<!-- .slide: data-background-video="https://webstatic.mihoyo.com/upload/op-public/2020/01/16/198e7665eab6282ce0cbc4117a098359_6177209616536484653.mp41" -->

# Quicksight

---

<!-- .slide: data-background="https://learnquicksight.workshop.aws/images/Business-Level-Up/qs-ppt-1.png" -->

<aside class="notes">
  这里是注释。
</aside>

---

<!-- .slide: data-background="https://learnquicksight.workshop.aws/images/Business-Level-Up/qs-ppt-2.png" -->

<aside class="notes">
  这里是注释。
</aside>

---

<!-- .slide: data-background="https://learnquicksight.workshop.aws/images/Business-Level-Up/qs-ppt-3.png" -->

<aside class="notes">
  这里是注释。
</aside>

---

<!-- .slide: data-background="https://learnquicksight.workshop.aws/images/Business-Level-Up/qs-ppt-4.png" -->

<aside class="notes">
  这里是注释。
</aside>

---

<!-- .slide: data-background="https://learnquicksight.workshop.aws/images/Business-Level-Up/qs-ppt-5.png" -->

<aside class="notes">
  这里是注释。
</aside>

---

<!-- .slide: data-background="https://learnquicksight.workshop.aws/images/Business-Level-Up/qs-ppt-6.png" -->

<aside class="notes">
  这里是注释。
</aside>

---

<!-- .slide: data-background="https://learnquicksight.workshop.aws/images/Business-Level-Up/qs-ppt-7.png" -->

<aside class="notes">
  这里是注释。
</aside>

---

<!-- .slide: data-background="https://learnquicksight.workshop.aws/images/Business-Level-Up/qs-ppt-8.png" -->

<aside class="notes">
  这里是注释。
</aside>

---

<!-- .slide: data-background="https://learnquicksight.workshop.aws/images/Business-Level-Up/qs-ppt-9.png" -->

<aside class="notes">
  这里是注释。
</aside>

---

<!-- .slide: data-background="https://learnquicksight.workshop.aws/images/Business-Level-Up/qs-ppt-10.png" -->

<aside class="notes">
  这里是注释。
</aside>

---

<!-- .slide: data-background="https://learnquicksight.workshop.aws/images/Business-Level-Up/qs-ppt-11.png" -->

<aside class="notes">
  这里是注释。
</aside>

---

<!-- .slide: data-background="https://learnquicksight.workshop.aws/images/Business-Level-Up/qs-ppt-12.png" -->

<aside class="notes">
  这里是注释。
</aside>

---

<!-- .slide: data-background="https://learnquicksight.workshop.aws/images/Business-Level-Up/qs-ppt-13.png" -->

<aside class="notes">
  这里是注释。
</aside>

---

<!-- .slide: data-background="https://learnquicksight.workshop.aws/images/Business-Level-Up/qs-ppt-14.png" -->

<aside class="notes">
  这里是注释。
</aside>

---

<!-- .slide: data-background="https://learnquicksight.workshop.aws/images/Business-Level-Up/qs-ppt-15.png" -->

<aside class="notes">
  这里是注释。
</aside>

---

<!-- .slide: data-background="https://learnquicksight.workshop.aws/images/Business-Level-Up/qs-ppt-16.png" -->

<aside class="notes">
  这里是注释。
</aside>

---

<!-- .slide: data-background="https://learnquicksight.workshop.aws/images/Business-Level-Up/qs-ppt-17.png" -->

<aside class="notes">
  这里是注释。
</aside>

---

<!-- .slide: data-background="https://learnquicksight.workshop.aws/images/Admin-Level-Up/qs-slide1.png" -->

<aside class="notes">
  这里是注释。
</aside>

---
<!-- .slide: data-background="https://learnquicksight.workshop.aws/images/Admin-Level-Up/qs-slide2.png" -->

<aside class="notes">
  这里是注释。
</aside>

---<!-- .slide: data-background="https://learnquicksight.workshop.aws/images/Admin-Level-Up/qs-slide3.png" -->

<aside class="notes">
  这里是注释。
</aside>

---<!-- .slide: data-background="https://learnquicksight.workshop.aws/images/Admin-Level-Up/qs-slide4.png" -->

<aside class="notes">
  这里是注释。
</aside>

---<!-- .slide: data-background="https://learnquicksight.workshop.aws/images/Admin-Level-Up/qs-slide5.png" -->

<aside class="notes">
  这里是注释。
</aside>

---<!-- .slide: data-background="https://learnquicksight.workshop.aws/images/Admin-Level-Up/qs-slide6.png" -->

<aside class="notes">
  这里是注释。
</aside>

---<!-- .slide: data-background="https://learnquicksight.workshop.aws/images/Admin-Level-Up/qs-slide7.png" -->

<aside class="notes">
  这里是注释。
</aside>

---<!-- .slide: data-background="https://learnquicksight.workshop.aws/images/Admin-Level-Up/qs-slide8.png" -->

<aside class="notes">
  这里是注释。
</aside>

---<!-- .slide: data-background="https://learnquicksight.workshop.aws/images/Admin-Level-Up/qs-slide9.png" -->

<aside class="notes">
  这里是注释。
</aside>

---


<pre><code data-trim>
  console.log('hello reveal.js!');
</code></pre>

---

<img src="" 
width="100%" 
height="100%"
/>

---

title: Foobar
separator: <!--s-->
verticalSeparator: <!--v-->
theme: solarized
revealOptions:
transition: 'fade'

---

Foo

Note: test note

<!--s-->

# Bar

<!--v-->
