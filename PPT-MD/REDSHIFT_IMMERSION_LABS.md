

##  [REDSHIFT IMMERSION LABS](https://redshift-immersion.workshop.aws/)

<aside class="notes">
  这里是注释。本ppt最新版本地址
  https://amazon.awsapps.com/workdocs/index.html#/login?redirectPath=folder%2F2f047905387d2808b2fe2eb34e240a7f6242fac3b31ce3918aeae1fa100fcb36
</aside>

---
## Agenda
1. CREATING A CLUSTER 
2. DATA LOADING
3. TABLE DESIGN AND QUERY TUNING
4. FEDERATION & SPECTRUM
5. Redshift ML

---
### 1. [CREATING A CLUSTER](https://ap-northeast-1.console.aws.amazon.com/cloudformation/home?region=ap-northeast-1#/stacks/create/template?stackName=ImmersionLab&templateURL=https://redshift-demos.s3.amazonaws.com/machine.yaml) 
- Configure Security VPC
- InternetGateway
- Subnets
- Route Table
- Security Group
- Subnet Group
- Create IAM Role
- Modify IAM Role for Scheduler access
- Launch Redshift Cluster

---

#### cloudFormation.yaml
```yaml
AWSTemplateFormatVersion: '2010-09-09'
Parameters:
  ClusterIdentifier:
    Description: The name of the redshift cluster
    Type: String
    Default: redshift-ml-demo
  NodeType:
    Description: The type of node to be provisioned
    Type: String
    Default: dc2.large
    AllowedValues:
    - ds2.xlarge
    - ds2.8xlarge
    - dc2.large
    - dc2.8xlarge
    - ra3.xlplus
    - ra3.4xlarge
    - ra3.16xlarge
  NumberOfNodes:
    Description: The number of compute nodes in the cluster. For multi-node clusters,
      the NumberOfNodes parameter must be greater than 1
    Type: Number
    Default: 2
    MinValue: 2
  DatabaseName:
    Description: The name of the first database to be created when the cluster is
      created
    Type: String
    Default: dev
    AllowedPattern: "([a-z]|[0-9])+"
  InboundTraffic:
    Description: The IP address CIDR range (x.x.x.x/x) to connect from your local machine.  FYI, you may get your CIDR using http://www.whatismyip.com.
    Type: String
    MinLength: '9'
    MaxLength: '18'
    Default: 72.21.0.0/16
    AllowedPattern: "(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})\\.(\\d{1,3})/(\\d{1,2})"
    ConstraintDescription: Must be a valid CIDR range of the form x.x.x.x/x.
  PortNumber:
    Description: The port number on which the cluster accepts incoming connections.
    Type: Number
    Default: '5439'
  MasterUsername:
    Description: The user name that is associated with the master user account for the cluster that is being created
    Type: String
    Default: awsuser
    AllowedPattern: "([a-z])([a-z]|[0-9])*"
  MasterUserPassword:
    Description: The password that is associated with the master user account for the cluster that is being created. Default is Awsuser123. Must have a length of 8-64 characters, contain one uppercase letter, one lowercase letter, and one number. Also only contain printable ASCII characters except for '/', '@', '"', ' ', '\' and '\'.
    Type: String
    Default: Awsuser123
    NoEcho: 'true'
Resources:
  S3BucketRedshiftMLDemo:
    Type: 'AWS::S3::Bucket'
    Properties:
      BucketEncryption:
        ServerSideEncryptionConfiguration:
          - ServerSideEncryptionByDefault:
              SSEAlgorithm: AES256
      Tags:
        - Key: Name
          Value:
            !Join [
              '-',
              [
                !Ref 'AWS::StackName',
                'S3BucketRedshiftMLDemo',
              ],
            ]
  RedshiftRole:
    Type: AWS::IAM::Role
    Properties :
      AssumeRolePolicyDocument:
        Version : 2012-10-17
        Statement :
          -
            Effect : Allow
            Principal :
              Service :
                - redshift.amazonaws.com
                - sagemaker.amazonaws.com
            Action :
              - sts:AssumeRole
      Path : /
      ManagedPolicyArns:
        - arn:aws:iam::aws:policy/AmazonS3FullAccess
        - arn:aws:iam::aws:policy/AWSGlueConsoleFullAccess
        - arn:aws:iam::aws:policy/AmazonSageMakerFullAccess
        - arn:aws:iam::aws:policy/AmazonRedshiftFullAccess
  RedshiftClusterParameterGroup:
    Properties:
      Description:
        Fn::Sub: "cluster cluster parameter group"
      ParameterGroupFamily: "redshift-1.0"
      Parameters:
        - ParameterName: max_concurrency_scaling_clusters
          ParameterValue: '1'
        -
          ParameterName: "wlm_json_configuration"
          ParameterValue:
            "[{\"user_group\":[\"data_scientist_group\"],\"query_group\":[],\"auto_wlm\":true,\"queue_type\":\"auto\",\"name\":\"data_scientist_queue\",\"concurrency_scaling\":\"off\",\"priority\":\"low\",\"rules\":[{\"rule_name\":\"log_long_running_query\",\"predicate\":[{\"metric_name\":\"query_execution_time\",\"operator\":\">\",\"value\":15}],\"action\":\"log\"},{\"rule_name\":\"kill_long_running_query\",\"predicate\":[{\"metric_name\":\"query_execution_time\",\"operator\":\">\",\"value\":45}],\"action\":\"abort\"},{\"rule_name\":\"log_disk_based_query\",\"predicate\":[{\"metric_name\":\"query_temp_blocks_to_disk\",\"operator\":\">\",\"value\":1}],\"action\":\"log\"}]},{\"user_group\":[\"etl_group\"],\"query_group\":[],\"auto_wlm\":true,\"queue_type\":\"auto\",\"name\":\"etl_queue\",\"priority\":\"highest\"},{\"auto_wlm\":true,\"user_group\":[],\"query_group\":[],\"name\":\"Defaultqueue\"},{\"short_query_queue\":true}]"
    Type: AWS::Redshift::ClusterParameterGroup
  RedshiftClusterSubnetGroup:
    Type: AWS::Redshift::ClusterSubnetGroup
    Properties:
      Description: Cluster subnet group
      SubnetIds:
      - Ref: SubnetAPublic
  VPC:
    Type: AWS::EC2::VPC
    Properties:
      CidrBlock: 192.168.0.0/16
      EnableDnsHostnames: true
      EnableDnsSupport: true
  SubnetAPublic:
    Type: AWS::EC2::Subnet
    Properties:
      CidrBlock: 192.168.0.0/24
      AvailabilityZone: !Select [0, !GetAZs ""]
      VpcId:
        Ref: VPC
  SubnetBPublic:
    Type: AWS::EC2::Subnet
    Properties:
      CidrBlock: 192.168.1.0/24
      AvailabilityZone: !Select [1, !GetAZs ""]
      VpcId:
        Ref: VPC
  SecurityGroup:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupDescription: Security group
      SecurityGroupIngress:
      - CidrIp:
          Ref: InboundTraffic
        FromPort: !Ref PortNumber
        ToPort: !Ref PortNumber
        IpProtocol: tcp
      VpcId:
        Ref: VPC
  SecurityGroupSelfReference:
    Type: AWS::EC2::SecurityGroupIngress
    Properties:
      Description: Self Referencing Rule
      FromPort: -1
      IpProtocol: -1
      GroupId: !GetAtt [SecurityGroup, GroupId]
      SourceSecurityGroupId: !GetAtt [SecurityGroup, GroupId]
      ToPort: -1
  InternetGateway:
    Type: AWS::EC2::InternetGateway
  AttachGateway:
    Type: AWS::EC2::VPCGatewayAttachment
    Properties:
      VpcId:
        Ref: VPC
      InternetGatewayId:
        Ref: InternetGateway
  RouteTable:
    Type: AWS::EC2::RouteTable
    Properties:
      VpcId:
        Ref: VPC
  Route:
    Type: AWS::EC2::Route
    DependsOn: AttachGateway
    Properties:
      RouteTableId:
        Ref: RouteTable
      DestinationCidrBlock: 0.0.0.0/0
      GatewayId:
        Ref: InternetGateway
  SubnetARouteTableAssociation:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties:
      SubnetId:
        Ref: SubnetAPublic
      RouteTableId:
        Ref: RouteTable
  SubnetBRouteTableAssociation:
    Type: AWS::EC2::SubnetRouteTableAssociation
    Properties:
      SubnetId:
        Ref: SubnetBPublic
      RouteTableId:
        Ref: RouteTable

  IamRoleLambdaCreateRedshiftCluster:
    Type: AWS::IAM::Role
    Properties:
      Description : IAM Role for lambda to Create Redshift Cluster
      AssumeRolePolicyDocument:
          Version: 2012-10-17
          Statement:
            -
              Effect: Allow
              Principal:
                Service:
                  - lambda.amazonaws.com
              Action:
                - sts:AssumeRole
      Path : /
      ManagedPolicyArns:
        - arn:aws:iam::aws:policy/AmazonS3FullAccess
      Policies:
          -
            PolicyName: LambdaInvokePolicy
            PolicyDocument :
              Version: 2012-10-17
              Statement:
                -
                  Effect: "Allow"
                  Action:
                  - logs:CreateLogGroup
                  - logs:CreateLogStream
                  - logs:PutLogEvents
                  - iam:PassRole
                  - redshift:DescribeClusters
                  - redshift:RestoreFromClusterSnapshot
                  - redshift:DescribeClusterSubnetGroups
                  - redshift:DescribeClusterSnapshots
                  - redshift:CreateClusterSnapshot
                  - redshift:CreateCluster
                  - ec2:Describe*
                  Resource: "*"
                -
                  Effect: "Allow"
                  Action:
                  - redshift:DeleteCluster
                  Resource:
                    - !Sub arn:aws:redshift:${AWS::Region}:${AWS::AccountId}:cluster:${ClusterIdentifier}
                -
                  Effect: Allow
                  Action: redshift:GetClusterCredentials
                  Resource:
                    - !Sub arn:aws:redshift:${AWS::Region}:${AWS::AccountId}:cluster:${ClusterIdentifier}
                    - !Sub arn:aws:redshift:${AWS::Region}:${AWS::AccountId}:dbname:${ClusterIdentifier}/${DatabaseName}
                    - !Sub arn:aws:redshift:${AWS::Region}:${AWS::AccountId}:dbuser:${ClusterIdentifier}/${MasterUsername}
                -
                  Effect: "Allow"
                  Action:
                  - redshift-data:ExecuteStatement
                  - redshift-data:ListStatements
                  - redshift-data:GetStatementResult
                  - redshift-data:DescribeStatement
                  Resource: "*"


  LambdaCreateRedshiftCluster:
    Type: AWS::Lambda::Function
    DependsOn:
      - RedshiftRole
      - RedshiftClusterParameterGroup
      - RedshiftClusterSubnetGroup
      - SecurityGroup   
    Properties:
      Description: lambda to add redshift cluster
      Handler: index.handler
      Runtime: python3.6
      Role: !GetAtt 'IamRoleLambdaCreateRedshiftCluster.Arn'
      Timeout: 900
      Code:
        ZipFile: |
          import boto3
          import boto3
          import botocore.exceptions as be
          import traceback
          import cfnresponse
          import time
          import hashlib

          def handler(event, context):
              print(event)
              client = boto3.client('redshift')
              response = ""
              msg = 'done'

              ClusterIdentifier = event['ResourceProperties'].get("ClusterIdentifier")
              md5 = hashlib.new('md5')
              md5.update(str(time.time()).encode('utf-8'))
              nodes = int(event['ResourceProperties'].get("NumberOfNodes"))
              NodeType = event['ResourceProperties'].get("NodeType")
              Port = int(event['ResourceProperties'].get("Port"))
              AvailabilityZone = event['ResourceProperties'].get("AvailabilityZone")
              ClusterSubnetGroupName = event['ResourceProperties'].get("ClusterSubnetGroupName")
              ClusterParameterGroupName = event['ResourceProperties'].get("ClusterParameterGroupName")
              MasterUsername = event['ResourceProperties'].get("MasterUsername")
              MasterUserPassword = event['ResourceProperties'].get("MasterUserPassword")
              DatabaseName=event['ResourceProperties'].get("DatabaseName")
              VpcSecurityGroupIds = [event['ResourceProperties'].get("VpcSecurityGroupIds")]
              iam = event['ResourceProperties'].get("IamRoles")
              IamRoles = [iam]
              if event['RequestType'] == 'Delete':
                  try:
                      response = client.delete_cluster(ClusterIdentifier=MlCluster, SkipFinalClusterSnapshot=True)
                  except:
                      error("error", context, event)
              else:
                  try:
                      response = client.create_cluster(DBName=DatabaseName,ClusterIdentifier=ClusterIdentifier,ClusterType='multi-node',NodeType=NodeType,MasterUsername=MasterUsername,MasterUserPassword=MasterUserPassword,VpcSecurityGroupIds=VpcSecurityGroupIds,ClusterSubnetGroupName=ClusterSubnetGroupName,AvailabilityZone=AvailabilityZone,ClusterParameterGroupName=ClusterParameterGroupName,Port=Port,NumberOfNodes=nodes,PubliclyAccessible=True,IamRoles=IamRoles,MaintenanceTrackName='sql_preview')
                      print(response)
                  except be.ClientError as e:
                      msg = e.response['Error']['Code']
                      if msg == 'ClusterAlreadyExists':
                          print(msg)
                      else:
                          error(msg, context, event)
              print(response)
              cfnresponse.send(event, context, cfnresponse.SUCCESS, {'Data': msg})


          def error(cfn_msg, context, event):
              print(traceback.format_exc())
              cfnresponse.send(event, context, cfnresponse.FAILED, {'Data': cfn_msg})
              raise


          def execute_redshift_sql(script, ClusterIdentifier, with_event=True):
              res = boto3.client("redshift-data").execute_statement(Database="dev", DbUser="awsuser", Sql=script,
                                                                    ClusterIdentifier=ClusterIdentifier, WithEvent=with_event)
              query_id = res["Id"]
              done = False
              while not done:
                  time.sleep(1)
                  status = redshift_sql_status_check(query_id)
                  if status in ("STARTED", "FAILED", "FINISHED"):
                      break
              return query_id


          def redshift_sql_status_check(query_id):
              res = boto3.client("redshift-data").describe_statement(Id=query_id)
              status = res["Status"]
              if status == "FAILED":
                  raise Exception('Error:' + param_value + ": " + res["Error"])
              return status.strip('"')


  CreateRedshiftCluster:
    Type: Custom::CreateRedshiftCluster
    Properties:
      ServiceToken: !GetAtt [LambdaCreateRedshiftCluster, Arn]
      ClusterIdentifier: !Ref ClusterIdentifier
      NumberOfNodes: !Ref NumberOfNodes
      NodeType: !Ref NodeType
      Port: !Ref PortNumber
      AvailabilityZone: !Select [0, !GetAZs ""]
      ClusterSubnetGroupName: !Ref RedshiftClusterSubnetGroup
      ClusterParameterGroupName: !Ref RedshiftClusterParameterGroup
      VpcSecurityGroupIds: !Ref SecurityGroup
      IamRoles: !GetAtt RedshiftRole.Arn
      MasterUsername: !Ref   MasterUsername
      MasterUserPassword: !Ref MasterUserPassword
      DatabaseName: !Ref DatabaseName
Outputs:
  RedshiftClusterRoleArn:
    Description: "Role ARN for SQL queries"
    Value:
      !GetAtt RedshiftRole.Arn
  RedshiftClusterUser:
    Description: "user to login to cluster"
    Value:
      !Ref MasterUsername
  RedshiftClusterPassword:
    Description: "Password to login to cluster"
    Value:
      !Ref MasterUserPassword
  RedshiftClusterIdentifier:
    Description: "Redshift cluster identifier"
    Value: 
      !Ref ClusterIdentifier
```

---

### [Redshift console](https://ap-northeast-1.console.aws.amazon.com/redshiftv2/home?region=ap-northeast-1#clusters)
Run Sample Query
```sql
select * from pg_user
```


---

### 2. DATA LOADING
<div align="left">
<font face="黑体" color=red size=1>ELAPSED TIME: 27 m 39 s获取所有行</font>
</div>

![](https://redshift-immersion.workshop.aws/images/Model.png)

---
<div align="left">
<font face="黑体" color=red size=1>查询编辑器只运行能够在10分钟内完成的短查询。 <br>
查询结果集按每页100行分页。</font>
</div>


```sql
DROP TABLE IF EXISTS partsupp; 
DROP TABLE IF EXISTS lineitem; 
DROP TABLE IF EXISTS supplier;
DROP TABLE IF EXISTS part;
DROP TABLE IF EXISTS orders;
DROP TABLE IF EXISTS customer;
DROP TABLE IF EXISTS nation;
DROP TABLE IF EXISTS region;

CREATE TABLE region (
  R_REGIONKEY bigint NOT NULL,
  R_NAME varchar(25),
  R_COMMENT varchar(152))
diststyle all;

CREATE TABLE nation (
  N_NATIONKEY bigint NOT NULL,
  N_NAME varchar(25),
  N_REGIONKEY bigint,
  N_COMMENT varchar(152))
diststyle all;

create table customer (
  C_CUSTKEY bigint NOT NULL,
  C_NAME varchar(25),
  C_ADDRESS varchar(40),
  C_NATIONKEY bigint,
  C_PHONE varchar(15),
  C_ACCTBAL decimal(18,4),
  C_MKTSEGMENT varchar(10),
  C_COMMENT varchar(117))
diststyle all;

create table orders (
  O_ORDERKEY bigint NOT NULL,
  O_CUSTKEY bigint,
  O_ORDERSTATUS varchar(1),
  O_TOTALPRICE decimal(18,4),
  O_ORDERDATE Date,
  O_ORDERPRIORITY varchar(15),
  O_CLERK varchar(15),
  O_SHIPPRIORITY Integer,
  O_COMMENT varchar(79))
distkey (O_ORDERKEY)
sortkey (O_ORDERDATE);

create table part (
  P_PARTKEY bigint NOT NULL,
  P_NAME varchar(55),
  P_MFGR  varchar(25),
  P_BRAND varchar(10),
  P_TYPE varchar(25),
  P_SIZE integer,
  P_CONTAINER varchar(10),
  P_RETAILPRICE decimal(18,4),
  P_COMMENT varchar(23))
diststyle all;

create table supplier (
  S_SUPPKEY bigint NOT NULL,
  S_NAME varchar(25),
  S_ADDRESS varchar(40),
  S_NATIONKEY bigint,
  S_PHONE varchar(15),
  S_ACCTBAL decimal(18,4),
  S_COMMENT varchar(101))
diststyle all;                                                              

create table lineitem (
  L_ORDERKEY bigint NOT NULL,
  L_PARTKEY bigint,
  L_SUPPKEY bigint,
  L_LINENUMBER integer NOT NULL,
  L_QUANTITY decimal(18,4),
  L_EXTENDEDPRICE decimal(18,4),
  L_DISCOUNT decimal(18,4),
  L_TAX decimal(18,4),
  L_RETURNFLAG varchar(1),
  L_LINESTATUS varchar(1),
  L_SHIPDATE date,
  L_COMMITDATE date,
  L_RECEIPTDATE date,
  L_SHIPINSTRUCT varchar(25),
  L_SHIPMODE varchar(10),
  L_COMMENT varchar(44))
distkey (L_ORDERKEY)
sortkey (L_RECEIPTDATE);

create table partsupp (
  PS_PARTKEY bigint NOT NULL,
  PS_SUPPKEY bigint NOT NULL,
  PS_AVAILQTY integer,
  PS_SUPPLYCOST decimal(18,4),
  PS_COMMENT varchar(199))
diststyle even;
```



---
### Key words
- diststyle
- distkey
- sortkey
  
---

diststyle
![](http://cdn.neo.pub/image/20210728124736.png)

---
distkey
![](http://cdn.neo.pub/image/20210728130456.png)

---
distkey
![](http://cdn.neo.pub/image/20210728130556.png)

---
sortkey
![](http://cdn.neo.pub/image/20210728130750.png)

---
sortkey
![](http://cdn.neo.pub/image/20210728131305.png)


---

### 2. [Loading Data](https://ap-northeast-1.console.aws.amazon.com/redshiftv2/home?region=ap-northeast-1#cluster-details?cluster=redshift-ml-demo&tab=properties)
```zsh
arn:aws:iam::835751346093:role/volvo-RedshiftRole-CQQY9JKGANG4
```

```zsh
COPY region FROM 's3://redshift-immersionday-labs/data/region/region.tbl.lzo'
iam_role 'arn:aws:iam::835751346093:role/volvo-RedshiftRole-CQQY9JKGANG4'
region 'us-west-2' lzop delimiter '|' COMPUPDATE PRESET;

COPY nation FROM 's3://redshift-immersionday-labs/data/nation/nation.tbl.'
iam_role 'arn:aws:iam::835751346093:role/volvo-RedshiftRole-CQQY9JKGANG4'
region 'us-west-2' lzop delimiter '|' COMPUPDATE PRESET;

copy customer from 's3://redshift-immersionday-labs/data/customer/customer.tbl.'
iam_role 'arn:aws:iam::835751346093:role/volvo-RedshiftRole-CQQY9JKGANG4'
region 'us-west-2' lzop delimiter '|' COMPUPDATE PRESET;

copy orders from 's3://redshift-immersionday-labs/data/orders/orders.tbl.'
iam_role 'arn:aws:iam::835751346093:role/volvo-RedshiftRole-CQQY9JKGANG4'
region 'us-west-2' lzop delimiter '|' COMPUPDATE PRESET;

copy part from 's3://redshift-immersionday-labs/data/part/part.tbl.'
iam_role 'arn:aws:iam::835751346093:role/volvo-RedshiftRole-CQQY9JKGANG4'
region 'us-west-2' lzop delimiter '|' COMPUPDATE PRESET;

copy supplier from 's3://redshift-immersionday-labs/data/supplier/supplier.json' manifest
iam_role 'arn:aws:iam::835751346093:role/volvo-RedshiftRole-CQQY9JKGANG4'
region 'us-west-2' lzop delimiter '|' COMPUPDATE PRESET;

copy lineitem from 's3://redshift-immersionday-labs/data/lineitem-part/l_orderyear=1992'
iam_role 'arn:aws:iam::835751346093:role/volvo-RedshiftRole-CQQY9JKGANG4'
region 'us-west-2' gzip delimiter '|' COMPUPDATE PRESET;

copy partsupp from 's3://redshift-immersionday-labs/data/partsupp/partsupp.tbl.'
iam_role 'arn:aws:iam::835751346093:role/volvo-RedshiftRole-CQQY9JKGANG4'
region 'us-west-2' lzop delimiter '|' COMPUPDATE PRESET;
```


---
#### 数据情况
- REGION (5 rows) - 20s
- NATION (25 rows) - 20s
- CUSTOMER (15M rows) – 3m
- ORDERS - (76M rows) - 1m
- PART - (20M rows) - 4m
- SUPPLIER - (1M rows) - 1m
- LINEITEM - (600M rows) - 13m
- PARTSUPPLIER - (80M rows) 3m


---
### Table Maintenance - Analyze
- 统计元数据
- 会自动Auto-Analyze，但大型的查询之前可以手动运行 `ANALYZE` 以达到性能最优

```zsh
analyze customer;
```
```sql
select query, rtrim(querytxt), starttime
from stl_query
where
querytxt like 'padb_fetch_sample%' and
querytxt like '%customer%'
order by query desc;
```

---
### Table Maintenance - VACUUM
- 清理标记delete数据
- 会自动Auto-VACUUM，应该在大量删除或更新之后运行 VACUUM 命令。
- 为了执行更新，Amazon Redshift 删除原始行并追加更新后的行，因此每次更新实际上就是一次删除和插入
```sql
vacuum delete only orders;
```

---
## 3. TABLE DESIGN AND QUERY TUNING
- 结果集缓存和执行计划重用
- 选择性过滤
- 压缩
- 加入策略

---

### 结果集缓存和执行计划重用
<div align="left">
<font face="黑体" color=red size=1>执行以下查询并记录查询执行时间。<br> 
因为这是该查询的第一次执行，所以需要编译查询并缓存结果集 35s</font>
</div>

```sql
SELECT c_mktsegment, o_orderpriority, sum(o_totalprice)
FROM customer c
JOIN orders o on c_custkey = o_custkey
GROUP BY c_mktsegment, o_orderpriority;
```

<div align="left">
<font face="黑体" color=red size=1>再次执行相同的查询，并记录查询执行时间。 3s<br> 
在第二次执行中，红移将利用结果集缓存并立即返回</font>
</div>

---
### 结果集缓存和执行计划重用
<div align="left">
<font face="黑体" color=red size=1>
更新表中的数据并再次运行查询。54s <br> 
当基础表中的数据发生更改时，Redshift 将意识到更改并使与查询关联的结果集缓存失效。  49s  <br> 
注意，执行时间不像步骤2那么快，但比步骤1快，因为它不能重用缓存，但可以重用已编译的计划 24s<br> 
</font>
</div>


```sql
UPDATE customer
SET c_mktsegment = 'Volvo'
WHERE c_mktsegment = 'MACHINERY';
```
```sql
VACUUM DELETE ONLY customer;
```
```sql
SELECT c_mktsegment, o_orderpriority, sum(o_totalprice)
FROM customer c
JOIN orders o on c_custkey = o_custkey
GROUP BY c_mktsegment, o_orderpriority;
```


---
### 结果集缓存和执行计划重用
<div align="left">
<font face="黑体" color=red size=1>
使用谓词执行新查询，并记录查询执行时间。9s<br> 
因为这是该查询的第一次执行，所以需要编译查询并缓存结果集 
</font>
</div>

```sql
SELECT c_mktsegment, count(1)
FROM Customer c
WHERE c_mktsegment = 'MACHINERY'
GROUP BY c_mktsegment;
```

<div align="left">
<font face="黑体" color=red size=1>
使用略有不同的谓词执行查询，并注意，尽管扫描和聚合的数据量非常相似，但执行时间比先前的执行更快。3s 4s<br> 
这种行为是由于重用了编译缓存，因为只有谓词发生了更改。<br> 
这种类型的模式在 BI 报告中很典型，其中 SQL 模式与不同用户检索与不同谓词关联的数据保持一致 
</font>
</div>

```sql
SELECT c_mktsegment, count(1)
FROM customer c
WHERE c_mktsegment = 'BUILDING'
GROUP BY c_mktsegment;
```


---
### 结果集缓存和执行计划重用
<div align="left">
<font face="黑体" color=red size=1>
关闭缓存
</font>
</div>

```sql
ALTER USER root set enable_result_cache_for_session to false;
```


---
### 选择性过滤
<div align="left">
<font face="黑体" color=red size=1>
当优化器知道过滤条件不匹配时，它可以跳过读取数据块。<br> 
对于 orders 表，因为我们已经在 o_orderdate 上定义了排序键，所以利用该字段作为谓词的查询将更快地返回。
</font>
</div>

```sql
SELECT count(1), sum(o_totalprice)
FROM orders
WHERE o_orderdate between '1992-07-05' and '1992-07-07';
```

```sql
SELECT count(1), sum(o_totalprice)
FROM orders
WHERE o_orderdate between '1992-07-05' and '1992-07-07';
```
<div align="left">
<font face="黑体" color=red size=1>
执行以上查询两次，记录第二次执行的执行时间。25s 5s<br> 
第一个执行是确保计划被编译。第二个是更具有代表性的终端用户体验
</font>
</div>

---
### 选择性过滤
<div align="left">
<font face="黑体" color=red size=1>
执行以下查询两次，记录第二次执行的执行时间。 <br> 
同样，第一个查询是为了确保计划被编译。 <br> 
注意: 与前面步骤中使用的查询相比，此查询具有不同的筛选条件，但扫描的数据行数相对相同
</font>
</div>


```sql
SELECT count(1), sum(o_totalprice)
FROM orders
where o_orderkey < 600001;
```
```sql
SELECT count(1), sum(o_totalprice)
FROM orders
where o_orderkey < 600001;
```


---
### 压缩
<div align="left">
<font face="黑体" color=red size=1>
Redshift 以分列方式运行。这使我们有机会实现可以对单列数据进行操作的算法，这些数据可以独立压缩。
</font>
</div>

```sql
SELECT tablename, "column", encoding
FROM pg_table_def
WHERE schemaname = 'public' AND tablename = 'lineitem'
```

---
### 执行计划

```sql
EXPLAIN
SELECT c_mktsegment,COUNT(o_orderkey) AS orders_count, sum(l_quantity) as quantity, sum (l_extendedprice) as extendedprice
FROM lineitem
JOIN orders on l_orderkey = o_orderkey
JOIN customer c on o_custkey = c_custkey
WHERE l_commitdate between '1992-01-01T00:00:00Z' and '1992-12-31T00:00:00Z'
GROUP BY c_mktsegment;
```


---
### FEDERATION & SPECTRUM
<div align="left">
<font face="黑体" color=red size=1>
创建外部表与谓词下推
</font>
</div>

```sql
DROP SCHEMA IF EXISTS apg;
-- 联合查询配置
CREATE EXTERNAL SCHEMA IF NOT EXISTS apg
    FROM POSTGRES
    DATABASE 'postgres' SCHEMA 'public'
    URI 'neo.cluster-ro-cjcyzs87nm4g.us-east-1.rds.amazonaws.com'
    PORT 5432
    IAM_ROLE 'arn:aws:iam::835751346093:role/aws-redshift-Federation=role'
    SECRET_ARN 'arn:aws:secretsmanager:us-east-1:835751346093:secret:aws-vieginia-aurora-finebi-v1-key-8ItDMm';
```

```sql
show enable_pg_federation_agg_pushdown;
set enable_pg_federation_agg_pushdown = true;
show enable_pg_federation_agg_pushdown;
```

```sql
explain
SELECT count(*)
FROM apg.adx_report_realtime;
```

---
### Redshift ML
<div align="left">
<font face="黑体" color=red size=1>
数据准备
</font>
</div>

```sql
CREATE TABLE IF NOT EXISTS pizza_deliveries_test_set
(
    delivery_date  VARCHAR(60),
    cust_number    INTEGER,
    store_number   SMALLINT,
    delivery_zip   CHAR(10),
    delivery_city  VARCHAR(90),
    delivery_state CHAR(2),
    order_amt      SMALLINT,
    offer_type     VARCHAR(20),
    location_type  VARCHAR(11),
    order_method   VARCHAR(10)
)
;

CREATE TABLE IF NOT EXISTS pizza_deliveries_training_set
(
    delivery_date  VARCHAR(60),
    cust_number    INTEGER,
    store_number   SMALLINT,
    delivery_zip   CHAR(10),
    delivery_city  VARCHAR(90),
    delivery_state CHAR(2),
    order_amt      SMALLINT,
    offer_type     VARCHAR(20),
    location_type  VARCHAR(11),
    order_method   VARCHAR(10),
    reason         VARCHAR(19),
    delivery_issue VARCHAR(1)
)
;

copy public.pizza_deliveries_test_set
    from 's3://redshift-demos/data/pizza_deliveries_testing.csv'
    iam_role 'arn:aws:iam::835751346093:role/volvo-RedshiftRole-CQQY9JKGANG4'
    delimiter ','
    IGNOREHEADER 1
    region 'us-east-1';

copy public.pizza_deliveries_training_set
    from 's3://redshift-demos/data/pizza_deliveries_training.csv'
    iam_role 'arn:aws:iam::835751346093:role/volvo-RedshiftRole-CQQY9JKGANG4'
    delimiter ','
    IGNOREHEADER 1
    region 'us-east-1';
```


---
### [Redshift ML](https://s3.console.aws.amazon.com/s3/home?region=ap-northeast-1)
<div align="left">
<font face="黑体" color=red size=1>
训练模型
</font>
</div>

```sql
CREATE
MODEL pizza_delivery_issue_model
FROM
  (
    SELECT
      cust_number,
      store_number,
      delivery_zip,
      delivery_state,
      offer_type,
      order_method,
      location_type,
      order_amt,
      delivery_issue
    FROM
     pizza_deliveries_training_set

  )  TARGET delivery_issue
FUNCTION predict_delivery_issue
IAM_ROLE 'arn:aws:iam::835751346093:role/volvo-RedshiftRole-CQQY9JKGANG4'
SETTINGS (
  S3_BUCKET 'volvo-s3bucketredshiftmldemo-1t8tnm96a4j97'
)
;
```

---
### [Redshift ML](https://ap-northeast-1.console.aws.amazon.com/sagemaker/home?region=ap-northeast-1#/dashboard)
<div align="left">
<font face="黑体" color=red size=1>
查看训练进度
</font>
</div>

```sql
show model pizza_delivery_issue_model;
```

![](http://cdn.neo.pub/image/20210728153709.png)

---
### Redshift ML
<div align="left">
<font face="黑体" color=red size=1>
模型精确度
</font>
</div>

```sql
select(t1.prediction_match*1.0)/(total_records*1.0) as accuracy_rate
from
    (
        Select
            sum (case when delivery_issue  = predict_delivery_issue(cust_number, store_number, delivery_zip, delivery_state,offer_type, order_method, location_type, order_amt) then 1
                      else 0 end) as prediction_match,
            count(*) as total_records
        FROM   pizza_deliveries_training_set
    ) t1
;
```



---
### Redshift ML
<div align="left">
<font face="黑体" color=red size=1>
sagemaker dashboard
</font>
</div>

![](http://cdn.neo.pub/image/20210728153914.png)


---
### Redshift ML
<div align="left">
<font face="黑体" color=red size=1>
模型推理
</font>
</div>


```sql
WITH term_data AS (SELECT func_model_bank_marketing(age, job, marital, education, "default", housing, loan, contact,
                                                    month, day_of_week, duration, campaign, pdays, previous, poutcome,
                                                    emp_var_rate, cons_price_idx, cons_conf_idx, euribor3m,
                                                    nr_employed) AS predicted
                   FROM bank_details_inference)
SELECT CASE
           WHEN predicted = 'Y' THEN 'Yes-will-do-a-term-deposit'
           WHEN predicted = 'N' THEN 'No-term-deposit'
           ELSE 'Neither' END as deposit_prediction,
       COUNT(1)               AS count
from term_data
GROUP BY 1;
```
![](http://cdn.neo.pub/image/20210728154457.png)