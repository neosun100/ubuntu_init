#!/bin/zsh -l

export TERM=linux
export TERMINFO=/etc/terminfo

sudo mkdir -p /data/logs/SSR-client && sudo chmod 777 /data/logs -R;

sddmd(){
  curl --silent \
       --request POST \
       --url "https://oapi.dingtalk.com/robot/send?access_token=813a36738938895e634ee516186fc0f8baec01d7792da5030dcc2a904bdc501b" \
       --header "content-type: application/json" \
       --data "{
            'msgtype': 'markdown',
            'markdown': {'title':'预警',
       'text':'# $1 \n> `date +"%Y-%m-%d %H:%M:%S"`  \n### `hostname`\n- $2'
            },
           'at': {
               'atMobiles': [
                   '1825718XXXX'
               ],
               'isAtAll': true
           }
        }"
}



rm_SSR_docker(){
[[ `sudo docker ps -a | grep SSR-client | wc -l` = 1 ]] && (sudo docker rm -f SSR-client && echo "SSR-client is removed") || echo "SSR-client is not exist";
}

check_local_socks5(){
curl --connect-timeout 4 \
--max-time 5 \
--location \
--silent \
--proxy socks5://127.0.0.1:12080 \
new.nginxs.net/ip.php
}


if [ `check_local_socks5 | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | wc -l` -eq 1 ]; then
echo "`date +'%Y-%m-%d %H:%M:%S'` SSR-client exist and success"
else
echo "`date +'%Y-%m-%d %H:%M:%S'` SSR-client is failure need reload" && \
rm_SSR_docker && \
sudo docker run \
-p 12080:2080 \
-p 18181:8181 \
--restart always \
-d \
--dns 1.1.1.1 \
--name SSR-client \
-v /etc/localtime:/etc/localtime:ro \
registry.cn-shanghai.aliyuncs.com/neo_docker/ssr-client:arm \
/bin/bash -c 'python3 main.py --setting-url https://request.appsmod.com/nodes/subscribe/44908/AorJte0Etj0Q && \
python3 main.py -u && \
python3 main.py -l && \
python3 main.py -c `echo $[$RANDOM%7+21]` -p 1080 && \
python3 main.py -S && \
python3 main.py -s && \
polipo -c /etc/polipo.conf && \
rinetd -c /etc/rinetd.conf && \
tail -f /dev/null';
# 传入容器的 `语句` 必须用单引号，否则在传入前就被执行了
sddmd "SSR-client status" "restart success"
fi # 判断结束，以fi结尾




# typeset STATUS="`python3 main.py -l | grep -v  ∞| grep -v +  | sort -g -k 6 |  tr -d \|  | awk "{print $1}" | wc -l`" && \
# typeset RESULT="`python3 main.py -l | grep -v  ∞| grep -v +  | sort -g -k 6 |  tr -d \|  | awk "{print $1}"  |head -n 2 | tail -n 1`" && \
