startTS="`date +%s.%N`";
cd ~/upload;


sudo mkdir /etc/apt/sources.list.d/
sh ~/upload/ubuntu_init/script/01.*.sh;
sh ~/upload/ubuntu_init/script/02.*.sh;
sh ~/upload/ubuntu_init/script/03.*.sh;
sh ~/upload/ubuntu_init/script/04.*.sh;
sh ~/upload/ubuntu_init/script/05.*.sh;
sh ~/upload/ubuntu_init/script/06.*.sh;
sh ~/upload/ubuntu_init/script/07.*.sh;
sh ~/upload/ubuntu_init/script/08.*.sh;
sh ~/upload/ubuntu_init/script/09.*.sh;
sh ~/upload/ubuntu_init/script/10.*.sh;
endTS="`date +%s.%N`";
result=`gawk -v x=$startTS -v y=$endTS 'BEGIN{printf "%.6f\n",y-x}'`; # 总秒数
_hour=`gawk -v x=$result -v y=3600 'BEGIN{printf "%d \n",x/y}'`; # 时数
_min=`gawk -v x=$result -v y=60 -v z=$_hour 'BEGIN{printf "%d \n",(x-z*3600)/y}'`; # 分数
_sec=`gawk -v x=$result -v y=60 -v z=$_hour -v w=$_min 'BEGIN{printf "%d \n", x - z*3600 - w*60 }'`; # 秒数
echo "`date +'%Y-%m-%d %H:%M:%S'` 全部自动化安装耗时 $_hour 小时 $_min 分钟 $_sec 秒 ";
