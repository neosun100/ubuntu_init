


# # 安装 brew 以及换源
# /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"


# # 若安装失败
# # 此时要做的就是去开发者网站
# # https://developer.apple.com/download/more/
# # 下载插件Command Line Tools，下载对应版本安装后就可以了

# # 替换brew.git：
# /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)" && \
# cd "$(brew --repo)" && \
# git remote set-url origin https://mirrors.ustc.edu.cn/brew.git && \
# cd "$(brew --repo)/Library/Taps/homebrew/homebrew-core" && \
# git remote set-url origin https://mirrors.ustc.edu.cn/homebrew-core.git && \
# export HOMEBREW_BOTTLE_DOMAIN=https://mirrors.ustc.edu.cn/homebrew-bottles && \
# brew update


# # 替换homebrew-core.git:
# cd "$(brew --repo)/Library/Taps/homebrew/homebrew-core"
# git remote set-url origin https://mirrors.ustc.edu.cn/homebrew-core.git


# cd "$(brew --repo)/Library/Taps/homebrew/homebrew-cask"
# git remote set-url origin https://mirrors.ustc.edu.cn/homebrew-cask.git
# # 执行有问题，
# # 可能需要先执行下 
# # brew install caskroom/cask/brew-cask
# # 报错
# # 在执行上面的命令 



# # 添加环境变量
# export HOMEBREW_BOTTLE_DOMAIN=https://mirrors.ustc.edu.cn/homebrew-bottles

/bin/zsh -c "$(curl -fsSL https://gitee.com/cunkai/HomebrewCN/raw/master/Homebrew.sh)"
brew update



# 安装 zsh
brew install zsh

# 安装 ohmyzsh
cd ~ && \
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

# 安装主题
echo "###########################################";
echo "##### 07. 安装 oh-my-zsh 高亮和推断插件#####";
echo "##########################################";

sudo chown -R   neo  /Users/neo/.config;  

git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting;
git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions;

brew install wget
666.reload_profile_no_tsocks

echo sudo 免密码
1.需要在/etc/sudoers中配置。 
这个文件的权限是r/r/n，配置之前需要加写权限。
sudo chmod u-w /etc/sudoers

2.打开命令窗口
sudo vi /etc/sudoers
将 #%admin ALL=(ALL) ALL
替换为 %admin ALL=(ALL) NOPASSWD: ALL

3.既然原来权限是r/r/,修改之后最好也改回来。
sudo chmod u-w /etc/sudoers
echo "####################################";
echo "##### 08. 生效 zsh  #####";
echo "####################################";

brew  install --cask anaconda;
pip install pygments -U;


echo $SHELL;
sudo chsh -s $(which zsh);

# sudo 免密码
# history + 700000
echo "####################################";
echo "##### 02. 优化 history数量和输出 ####";
echo "####################################";
echo "`date +'%Y-%m-%d %H:%M:%S'` 当前 /etc/profile文件";
ls -lah /etc/profile;
echo "`date +'%Y-%m-%d %H:%M:%S'` 覆盖备份 /etc/profile_bak 文件";
sudo cp -rf /etc/profile /etc/profile_bak;
echo  "`date +'%Y-%m-%d %H:%M:%S'`  /etc/profile 增加写权限";
sudo chmod +w /etc/profile;
echo "`date +'%Y-%m-%d %H:%M:%S'` 查看当前 /etc/profile 文件内容";
cat /etc/profile | grep -v "^#" | grep -v "^$";
echo "`date +'%Y-%m-%d %H:%M:%S'` 若不存在HISTSIZE=70000和whoami则添加 ";
sed -i '/^HISTSIZE=70000/d' /etc/profile;
sed -i '/^export HISTTIMEFORMAT="%F %T `whoami` "/d' /etc/profile;
echo "HISTSIZE=70000" >> /etc/profile;
echo 'export HISTTIMEFORMAT="%F %T `whoami` "' >> /etc/profile;
echo "`date +'%Y-%m-%d %H:%M:%S'` 处理完成，查看 /etc/profile 文件内容";
cat /etc/profile | grep -v "^#" | grep -v "^$";
echo  "`date +'%Y-%m-%d %H:%M:%S'`  /etc/profile 去除写权限";
sudo chmod -w /etc/profile;
echo "`date +'%Y-%m-%d %H:%M:%S'` source /etc/profile 在脚本执行结束后生效";


# mac 下的 ag命令安装
# https://github.com/ggreer/the_silver_searcher
brew install the_silver_searcher

brew install gcc

# 更好的 vim
# brew install neovim


# brew 是从下载源码解压然后./configure && make install,同时会包含相关依存库。并自动配置
# 好各种环境变量，而且易于卸载。
#
# 而brew cask是已经编译好了的应用包(.dmg/.pkg).
# 仅仅是下载解压，放在统一的目录中(/opt/homebrew-cask/Caskroom),
# 省掉了自己去下载、解压、拖拽(安装)等蛋疼的步骤，同样，卸载相当容易与干净。
# ---------------------
# 作者：野沐沐
# 来源：CSDN
# 原文：https://blog.csdn.net/yanxiaobo1991/article/details/78455908
# 版权声明：本文为博主原创文章，转载请附上博文链接！
# 通过brew安装cask
# brew install caskroom/cask/brew-cask
ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)" < /dev/null 2> /dev/null ; brew install caskroom/cask/brew-cask 2> /dev/null
# 使用cask安装Atom
# brew cask install atom



# pycharm汉化
# https://github.com/pingfangx/TranslatorX
# https://github.com/pingfangx/jetbrains-in-chinese
# https://blog.csdn.net/pingfangx/article/details/78826155
https://data-1251390522.cos.ap-shanghai.myqcloud.com/Sys/resources_zh_CN_PyCharm_2018.3_r1.jar



# mac 利用init.sh 666
# 解决 mac 下不存在此文件的问题，但是命令不可用哦，只是避免source 出错
# 需要自建2个文件和目录
# sudo mkdir /proc
# sudo touch /proc/cpuinfo


# tmux会报问题，在source 的时候，且没有用其配置，需要解决

# RedisDesktopManager-Mac
https://github.com/onewe/RedisDesktopManager-Mac/releases


# 破解datagrip
http://www.xue51.com/mac/5600.html
https://data-1251390522.cos.ap-shanghai.myqcloud.com/Sys/resources_cn_datagrip_test.jar
https://data-1251390522.cos.ap-shanghai.myqcloud.com/Sys/Jetbrains-Crack_all.jar 
https://data-1251390522.cos.ap-shanghai.myqcloud.com/Sys/Jetbrains-Crack.jar 


# 代码高亮
brew install highlight 

# dir高亮
brew install coreutils
# https://github.com/neosun100/dircolors



# 模糊查找
brew install fzf; # 模糊查找

# 全路径的 fzf 查找 代替 cd 
brew install zplug; #  z, 全目录；zz 当前目录下


mwget https://nodejs.org/dist/v10.15.3/node-v10.15.3.pkg

brew install node
# 预览扩展
# 预览代码高亮 空格操作
brew install --cask qlcolorcode
# md 预览
brew install --cask qlmarkdown
# 全套
brew install --cask qlcolorcode qlstephen qlmarkdown quicklook-json qlimagesize webpquicklook suspicious-package quicklookase qlvideo

# 10. PeovisionQL，预览iOS的 .ipa 安装包

brew install --cask provisionql

# 11. QuickLookAPK，预览Android的 .apk 安装包

brew install --cask quicklookapk​

# 流程图
brew install graphviz 


# tcping
brew install tcping 

# 若安装 qlimagesize 失败，请手动安装
# https://github.com/Nyx0uf/qlImageSize
# neo @ Neone in ~/upload [15:50:34] C:1
$ mv  ~/Downloads/qlImageSize.qlgenerator ~/Library/QuickLook/

# neo @ Neone in ~/upload [15:50:55] 
$ mv  ~/Downloads/mdImageSize.mdimporter ~/Library/QuickLook/



# mac 安装 sshpass
# 文档在这里 https://www.jianshu.com/p/2ce1bc682ac6

# ssh登陆不能在命令行中指定密码。sshpass的出现，解决了这一问题。sshpass用于非交互SSH的密码验证，一般用在sh脚本中，无须再次输入密码。

# 它允许你用 -p 参数指定明文密码，然后直接登录远程服务器，它支持密码从命令行、文件、环境变量中读取。

echo "`date +'%Y-%m-%d %H:%M:%S'` 编译安装 sshpass！用于ansible";
wget https://neone-club.sh1a.qingstor.com/Sys/sshpass-1.08.tar.gz;
tar -zxvf sshpass-1.08.tar.gz >/dev/null 2>&1;
cd ./sshpass-1.08/;
./configure; 
make && make install; 
sshpass -h;
echo "`date +'%Y-%m-%d %H:%M:%S'` 编译安装 sshpass 完成";



# osascript（AppleScript）python实现（MacOS）
# https://pypi.org/project/osascript/
pip install osascript;




# vim高亮
install_vim_draculatheme
# mv ~/upload/ubuntu_init/profile/.vimrc ~/.vimrc;
# sudo mv /Users/neo/upload/ubuntu_init/profile/molokai.vim  /usr/share/vim/vim*/colors/ ;

# sudo mv ~/upload/ubuntu_init/profile/molokai.vim  /usr/share/vim/vim*/colors/ ;

# molokai 2 ron
# 安装 go
brew info go;
brew install go;



# 避免macbook 休眠时 被鼠标唤醒 （放入包中，自启动了）:
brew install sleepwatcher 
# 下载蓝牙控制
# http://www.frederikseiffert.de/blueutil/
# wget http://www.frederikseiffert.de/blueutil/download/blueutil.zip
# Usage
# Print bluetooth status:
# $ blueutil status
# Status: on
# Switch bluetooth on:
# $ blueutil on
# Switch bluetooth off:
# $ blueutil off

brew install blueutil
brew link blueutil



# 打开把可执行文件移动到  /usr/local/bin/

brew services start sleepwatcher # 设置软件服务自启动
ps aux | grep sleepwatcher 

# 在 ~ 目录下创建文件 .sleep 和 .wakeup 并赋予权限 777

# > vi .sleep
 
# /usr/local/bin/blueutil off 
# networksetup -setairportpower en0 off
 
# > vi .wakeup
 
# /usr/local/bin/blueutil on
# networksetup -setairportpower en0 on
#  ———————————————— 
# 版权声明：本文为CSDN博主「remix_zhang」的原创文章，遵循CC 4.0 by-sa版权协议，转载请附上原文出处链接及本声明。
# 原文链接：https://blog.csdn.net/qq944417919/article/details/84297806



# 解压缩 rar
 brew install --cask rar



# k8sctl
brew install kubernetes-cli
brew upgrade kubernetes-cli


# 下载 docker 
https://download.docker.com/mac/edge/Docker.dmg

安装

check 版本

设置源

https://r9q0wroe.mirror.aliyuncs.com

{
  "debug": true,
  "experimental": true,
"registry-mirrors":["https://r9q0wroe.mirror.aliyuncs.com"]
}


cd ~/upload

git clone https://github.com/AliyunContainerService/k8s-for-docker-desktop

./load_images.sh

 开启 k8s


brew install goaccess; #  日志动态分析



brew install glances; # 最强版命令行系统监控



brew install frpc;
 # nohup frpc -c ~/upload/frp/frpc.ini > ~/upload/frp/frpc.log 2>&1 &



 brew install tmate;
 # https://tmate.io/
 # 


 brew install ffmpeg;



brew install aview # 字符画
brew cask install xquartz  # mplayer 的依赖
brew install mplayer  # shell 播放器


brew install bat #  带行号翻页的语法高亮阅读器

brew install fd # 更好的find

brew install nnn # 更好的du 和 ncdu类似


brew install guetzli # 谷歌图片压缩 


brew install sdl2 # pyboy 依赖


brew install wakeonlan # net 唤醒 ubuntu  需要主板支持


brew cask install rectangle # 桌面布局


brew install hstr # HSTR 是一款可以轻松查看、导航和搜索历史命令的小工具，它支持 Bash 和 Zsh

brew install groovy # 安装 groovy

echo "mac 画图图软件"
cd ~/upload;
wget http://file.neo.pub/mac/Paintbrush-2.5.0.zip



echo "music cli player"
brew install sox



echo "setup psql"
brew install postgresql
psql --help


echo "mac 两部验证"
brew install --cask goldenpassport

echo "等同linux的命令"
up_shell_all_proxy 
brew install cavaliercoder/dmidecode/dmidecode 

echo "mac pdf 工具集"
echo "https://www.jianshu.com/p/002a4bad9f9b"
brew install xpdf 
brew install mupdf-tools

# Tools
# The Xpdf open source project includes a PDF viewer along with a collection of command line tools which perform various functions on PDF files:

# xpdf: PDF viewer (click for a screenshot)
# pdftotext: converts PDF to text
# pdftops: converts PDF to PostScript
# pdftoppm: converts PDF pages to netpbm (PPM/PGM/PBM) image files
# pdftopng: converts PDF pages to PNG image files
# pdftohtml: converts PDF to HTML
# pdfinfo: extracts PDF metadata
# pdfimages: extracts raw images from PDF files
# pdffonts: lists fonts used in PDF files
# pdfdetach: extracts attached files from PDF files

# pdf分割
# pdf合并
# xpdf工具集
# 命令	描述
# pdfunite	合并pdf
# pdftotext	pdf转text
# pdftops	pdf转ops
# pdftoppm	pdf转ppm
# pdftohtml	pdf转html
# pdftocairo	pdf转cairo
# pdfsig	pdf签名
# pdfseparate	拆分pdf
# pdfinfo	pdf元数据
# pdfimages	pdf转图片
# pdffonts	提取pdf字体
# pdfdetach	删除pdf内置附件
# pdfattach	给pdf添加附件
# pdf2ps	pdf转ps
# pdf2dsc	pdf转dsc


echo "setup avro tools"
echo "https://github.com/miguno/avro-cli-examples"
brew install avro-tools



echo "pypy pypy3"
brew install pypy
brew install pypy3

echo "安装pypy pip"
wget https://bootstrap.pypa.io/get-pip.py
pypy3 get-pip.py
pypy3 -m pip install pip -U
pypy3 -m pip install datafaker
echo "/usr/local/share/pypy3"
echo "pypy cli"
echo "对比python pypy"
echo "https://mp.weixin.qq.com/s?__biz=MzAxMjUyNDQ5OA==&mid=2653565056&idx=1&sn=52948c80614bdc9a9ff5dcd107ea9a11&chksm=806e1e3db719972b484b4774b834b1a3c0ffc165b56143fba00e264c9af1023193e866875b7f&scene=21#wechat_redirect"