#!/bin/sh -e

# 钉钉消息
# sdd "标题" "内容"
sddmd(){
  curl --request POST \
       --url "https://oapi.dingtalk.com/robot/send?access_token=813a36738938895e634ee516186fc0f8baec01d7792da5030dcc2a904bdc501b" \
       --header "content-type: application/json" \
       --data "{
            'msgtype': 'markdown',
            'markdown': {'title':'预警',
       'text':'# $1 \n> `date +"%Y-%m-%d %H:%M:%S"`  \n### `hostname`\n- $2'
            },
           'at': {
               'atMobiles': [
                   '1825718XXXX'
               ],
               'isAtAll': true
           }
        }"
}

# mac版直接查看ip地址,            默认|比格云|腾讯云|qq云
alias inip='ip addr | egrep "192.168|10.100|172.27|172.30" | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | sed -n "1,1p"'


# 🍎 查看局域网ip,            默认|比格云|腾讯云|qq云
iip(){
# if [ -e /sbin/ifconfig ];then
#   #ifconfig | egrep "192.168|10.100|172.27|172.30" | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | sed -n "1,1p"
#   ifconfig | egrep "192.168|10.100|172.27|172.30" | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | head -n 1
# else
#   #ip addr | egrep "192.168|10.100|172.27|172.30" | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | sed -n "1,1p"
#   ip addr | egrep "192.168|10.100|172.27|172.30" | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | head -n 1
# fi


# [[ `uname` = "Darwin" ]] && \
# ifconfig | egrep "192.168|10.100|172.27|172.30" | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | head -n 1 || \
# ip addr |grep inet |grep -v inet6 | grep -v 127.0.0.1 | field 2 | line 1 1 | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}"

python3 -c "import socket;print([(s.connect(('8.8.8.8', 53)), s.getsockname()[0], s.close()) for s in [socket.socket(socket.AF_INET, socket.SOCK_DGRAM)]][0][1])"
}


# 每整分钟检测，启动时间 < 60 秒 则发送钉钉
# if [ `cat /proc/uptime | awk '{print int($0)}'` -le 180 ]; then sddmd "开机启动" "`iip`"; else echo "开机大于3分钟"; fi



sddmd "开机启动" "`iip`";

sudo mount /dev/nvme1n1 /pcie4.0data
sudo mount /dev/sdb /hdddata
sudo mount /dev/sda1 /ssddata
#sudo mount /dev/nvme1n1 /pcie4.0data
