echo "####################################################"
echo "################ 05.下载大安装并安装包 ###############"
echo "###################################################"

echo "$(date +'%Y-%m-%d %H:%M:%S') 开始下载!"

cd ~/upload
# # Anaconda
# https://repo.continuum.io/archive/
# wget https://repo.continuum.io/archive/Anaconda3-5.3.1-Linux-x86_64.sh;

wget --no-check-certificate "https://mirrors.ustc.edu.cn/anaconda/archive/$(curl -k --request GET \
  --url https://repo.anaconda.com/archive/ \
  --header 'cookie: __cfduid=d544b47fca998db431a01acbf20f47f8a1549107097' \
  --header 'user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36' \
  --cookie __cfduid=d544b47fca998db431a01acbf20f47f8a1549107097 | grep Anaconda3 | grep 2023 | grep Linux-x86_64.sh | tail -n 1 | awk -v head='sh\">' -v tail='</a></td>' '{print substr($0, index($0,head)+length(head),index($0,tail)-index($0,head)-length(head))}')"

#`chttp GET https://repo.anaconda.com/archive/ cookie:__cfduid=d544b47fca998db431a01acbf20f47f8a1549107097 user-agent:'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36' | grep Anaconda3 | grep Linux-x86_64.sh | head -n 1 | awk -v head="sh\">" -v tail="</a></td>" '{print substr($0, index($0,head)+length(head),index($0,tail)-index($0,head)-length(head))}'`

# # apache-maven
# http://maven.apache.org/download.cgi
wget --no-check-certificate https://data-1251390522.cos.ap-shanghai.myqcloud.com/Sys/apache-maven-3.6.0-bin.tar.gz
# # jdk
# http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html
#mwget http://101.44.1.123/files/216700000708070D/download.oracle.com/otn-pub/java/jdk/8u161-b12/2f38c3b165be4555a1fa6e98c45e0808/jdk-8u161-linux-x64.tar.gz;
wget --no-check-certificate https://data-1251390522.cos.ap-shanghai.myqcloud.com/Sys/jdk-8u161-linux-x64.tar.gz
# # node
# https://nodejs.org/en/download/
wget --no-check-certificate https://data-1251390522.cos.ap-shanghai.myqcloud.com/Sys/node-v10.14.1-linux-x64.tar.xz
#mwget https://nodejs.org/dist/v10.14.1/node-v10.14.1-linux-armv7l.tar.xz;
#mwget https://nodejs.org/dist/v10.14.1/node-v10.14.1.tar.gz;
# # sbt
# https://www.scala-sbt.org/download.html
# https://github.com/sbt/sbt/releases/download/v1.1.4/sbt-1.1.4.tgz
wget --no-check-certificate https://data-1251390522.cos.ap-shanghai.myqcloud.com/Sys/sbt-1.2.6.tgz
# # scala
# http://www.scala-lang.org/download/
wget --no-check-certificate https://data-1251390522.cos.ap-shanghai.myqcloud.com/Sys/scala-2.12.7.tgz
# https://data-1251390522.cos.ap-shanghai.myqcloud.com/Sys/scala-2.12.5.tgz
# https://studygolang.com/dl
wget --no-check-certificate https://dl.google.com/go/go1.11.linux-amd64.tar.gz

echo "$(date +'%Y-%m-%d %H:%M:%S') 下载完成!"

echo "$(date +'%Y-%m-%d %H:%M:%S') 解压二进制文件包!"
unzipDir=~/unzip   # 定义解压路径
sourceDir=~/upload # 定义源文件路径
tar -zxvf $sourceDir/jdk-*-linux-x64.tar.gz -C $unzipDir >/dev/null 2>&1
tar -zxvf $sourceDir/go*.linux-amd64.tar.gz -C $unzipDir >/dev/null 2>&1
tar -zxvf $sourceDir/scala-*.tgz -C $unzipDir >/dev/null 2>&1
tar -zxvf $sourceDir/apache-maven-*-bin.tar.gz -C $unzipDir >/dev/null 2>&1
tar -zxvf $sourceDir/sbt-*.tgz -C $unzipDir >/dev/null 2>&1
xz -d $sourceDir/node-*-linux-x64.tar.xz
tar -xvf $sourceDir/node-*-linux-x64.tar -C $unzipDir >/dev/null 2>&1
ls -lah ~/unzip/
echo "$(date +'%Y-%m-%d %H:%M:%S') 解压二进制文件包完成!"

echo "$(date +'%Y-%m-%d %H:%M:%S') 解压二进制文件移动到app目录!"
moveDir=/usr/local/app
mv -f $unzipDir/jdk* $moveDir/ >/dev/null 2>&1 # $moveDir/ 加/ 否则易移动的是子目录，就全乱了
mv -f $unzipDir/go* $moveDir/ >/dev/null 2>&1  # $moveDir/ 加/ 否则易移动的是子目录，就全乱了
mv -f $unzipDir/scala* $moveDir/ >/dev/null 2>&1
mv -f $unzipDir/apache-maven* $moveDir/ >/dev/null 2>&1
mv -f $unzipDir/sbt* $moveDir/ >/dev/null 2>&1
mv -f $unzipDir/node* $moveDir/ >/dev/null 2>&1
ls -lah $moveDir/
echo "$(date +'%Y-%m-%d %H:%M:%S') 解压二进制文件移动到app目录 完成!"

echo "$(date +'%Y-%m-%d %H:%M:%S') 建立软链接 arrow!"
arrowDir=/usr/local/arrow
ln -sf $moveDir/jdk* $arrowDir/jdk >/dev/null 2>&1
ln -sf $moveDir/go* $arrowDir/go >/dev/null 2>&1
ln -sf $moveDir/scala* $arrowDir/scala >/dev/null 2>&1
ln -sf $moveDir/apache-maven* $arrowDir/maven >/dev/null 2>&1
ln -sf $moveDir/sbt* $arrowDir/sbt >/dev/null 2>&1
ln -sf $moveDir/node* $arrowDir/node >/dev/null 2>&1
ls -lah $arrowDir/
echo "$(date +'%Y-%m-%d %H:%M:%S') 建立软链接 arrow 完成!"

echo "$(date +'%Y-%m-%d %H:%M:%S') 去手动安装 anaconda 吧! 骚年! "
echo "$(date +'%Y-%m-%d %H:%M:%S') 切忌不要安装VScode"
cd ~/upload
sudo chmod 777 Anaconda3-*-Linux-x86_64.sh && ./Anaconda3-*-Linux-x86_64.sh
echo "$(date +'%Y-%m-%d %H:%M:%S') anaconda 手动安装完成"

echo "$(date +'%Y-%m-%d %H:%M:%S') 去手动安装 tcpping "
echo y | sudo apt-get install tcptraceroute
cd ~/upload
wget --no-check-certificate http://www.vdberg.org/\~richard/tcpping
sudo mv tcpping /usr/bin/ && sudo chmod 777 /usr/bin/tcpping

echo "$(date +'%Y-%m-%d %H:%M:%S') 下载大安装并安装包完成"