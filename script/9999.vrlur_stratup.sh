sudo apt-get update
echo y | sudo apt-get upgrade



echo "`date +'%Y-%m-%d %H:%M:%S'` 检验上传目录是否创建" && \
if [ ! -d ~/upload  ];then mkdir ~/upload && chmod 777 ~/upload && echo "`date +'%Y-%m-%d %H:%M:%S'` 上传目录已创建并777！"; else echo "`date +'%Y-%m-%d %H:%M:%S'` 上传目录存在！"; fi && \
echo "`date +'%Y-%m-%d %H:%M:%S'` 下载安装 git and curl" && \
echo y | sudo apt-get install curl && \
echo y | sudo apt-get install git && \
cd ~ && rm -rf ~/upload/ubuntu_init && \
rm -rf ~/upload/ubuntu_init-master.tar.gz && \
cd ~/upload && \
wget https://gitlab.com/neosun100/ubuntu_init/-/archive/master/ubuntu_init-master.tar.gz && \
tar -zxvf ubuntu_init-master.tar.gz >/dev/null 2>&1 && \
mv ~/upload/ubuntu_init-master ~/upload/ubuntu_init && \
echo "set smtp-auth-password=Neosun100" >> ~/upload/ubuntu_init/profile/nail.rc && \
cd ~/upload && rm -rf ~/.bash_profile && \
cp ~/upload/ubuntu_init/profile/.bash_profile ~/.bash_profile && \
rm -rf ~/.zshrc && cp ~/upload/ubuntu_init/profile/.zshrc ~/.zshrc

/bin/cat ~/upload/ubuntu_init/source/bionic.ini >> /etc/apt/sources.list;
sudo apt-get update
echo y | sudo apt-get upgrade


echo "####################################";
echo "##### 00.修复source: not found错误 #####";
echo "####################################";
echo "`date +'%Y-%m-%d %H:%M:%S'` 当前 /bin/sh";
ls -l /bin/sh;
echo "dpkg-reconfigure dash 界面 选择 no";
sleep 5;
dpkg-reconfigure dash;
echo "`date +'%Y-%m-%d %H:%M:%S'` 修改后的 /bin/sh";




echo "####################################";
echo "##### 01.设置 sudo免密码 #####";
echo "####################################";
echo "`date +'%Y-%m-%d %H:%M:%S'` 当前 /etc/sudoers 文件";
ls -lah /etc/sudoers;
echo "`date +'%Y-%m-%d %H:%M:%S'` 覆盖备份 /etc/sudoers_bak 文件";
sudo cp -rf /etc/sudoers /etc/sudoers_bak;
echo "`date +'%Y-%m-%d %H:%M:%S'` 查看 当前 /etc/sudoers 文件";
cat /etc/sudoers | grep -v "^#" | grep -v "^$";
echo "`date +'%Y-%m-%d %H:%M:%S'` 若不存在neo sudo免密则添加";
sed -i '/^%sudo    ALL=(ALL:ALL) NOPASSWD: ALL/d' /etc/sudoers;
sed -i '/^neo      ALL=(ALL:ALL) NOPASSWD: ALL/d' /etc/sudoers;
echo "%sudo    ALL=(ALL:ALL) NOPASSWD: ALL" >> /etc/sudoers;
echo "neo      ALL=(ALL:ALL) NOPASSWD: ALL" >> /etc/sudoers;
echo "`date +'%Y-%m-%d %H:%M:%S'` 处理完成，查看 /etc/sudoers 文件";
cat /etc/sudoers | grep -v "^#" | grep -v "^$";



echo "#########################################";
echo "##### 01.5 添加 host 使得github 加速 #####";
echo "#########################################";
echo "`date +'%Y-%m-%d %H:%M:%S'` 当前 /etc/hosts 文件";
ls -lah /etc/hosts;
echo "`date +'%Y-%m-%d %H:%M:%S'` 覆盖备份 /etc/hosts_bak 文件";
sudo cp -rf /etc/hosts /etc/hosts_bak;
echo "`date +'%Y-%m-%d %H:%M:%S'` 查看 当前 /etc/hosts 文件";
cat /etc/hosts | grep -v "^#" | grep -v "^$";
echo "`date +'%Y-%m-%d %H:%M:%S'` 若不存在 github 加速 则增加 host ";
sed -i '/^151.101.185.194 github.global.ssl.fastly.net/d' /etc/hosts;
sed -i '/^192.30.253.112 github.com/d' /etc/hosts;
sed -i '/^151.101.72.133 assets-cdn.github.com/d' /etc/hosts;
echo "151.101.185.194 github.global.ssl.fastly.net" >> /etc/hosts;
echo "192.30.253.112 github.com" >> /etc/hosts;
echo "151.101.72.133 assets-cdn.github.com" >> /etc/hosts;
echo "`date +'%Y-%m-%d %H:%M:%S'` 处理完成，查看 /etc/hosts 文件";
cat /etc/hosts | grep -v "^#" | grep -v "^$";





echo "####################################";
echo "##### 02. 优化 history数量和输出 ####";
echo "####################################";
echo "`date +'%Y-%m-%d %H:%M:%S'` 当前 /etc/profile文件";
ls -lah /etc/profile;
echo "`date +'%Y-%m-%d %H:%M:%S'` 覆盖备份 /etc/profile_bak 文件";
sudo cp -rf /etc/profile /etc/profile_bak;
echo "`date +'%Y-%m-%d %H:%M:%S'` 查看当前 /etc/profile 文件内容";
cat /etc/profile | grep -v "^#" | grep -v "^$";
echo "`date +'%Y-%m-%d %H:%M:%S'` 若不存在HISTSIZE=70000和whoami则添加 ";
sed -i '/^HISTSIZE=70000/d' /etc/profile;
sed -i '/^export HISTTIMEFORMAT="%F %T `whoami` "/d' /etc/profile;
echo "HISTSIZE=70000" >> /etc/profile;
echo 'export HISTTIMEFORMAT="%F %T `whoami` "' >> /etc/profile;
echo "`date +'%Y-%m-%d %H:%M:%S'` 处理完成，查看 /etc/profile 文件内容";
cat /etc/profile | grep -v "^#" | grep -v "^$";
echo "`date +'%Y-%m-%d %H:%M:%S'` source /etc/profile 在脚本执行结束后生效";
# source /etc/profile;



echo "#####################################";
echo "###### 03. 确保 安装 lsb-core  #######";
echo "#####################################";
echo "`date +'%Y-%m-%d %H:%M:%S'` 目的是在更新源前，检测系统版本";
echo y | sudo apt-get install lsb-core;
echo "`date +'%Y-%m-%d %H:%M:%S'` lsb-core 安装完成";



echo "#####################################";
echo "###### 04. 替换为国内适合的源 ########";
echo "#####################################";
echo "`date +'%Y-%m-%d %H:%M:%S'` 当前/etc/apt/目录";
ls -lah /etc/apt/;
echo "`date +'%Y-%m-%d %H:%M:%S'` 删除/etc/apt/sources.list*";
sudo rm -rf /etc/apt/sources.list;
sudo rm -rf /etc/apt/sources.list.d;
if  [ `lsb_release -a | tail -n 1 | cut -f2` == "bionic" ]; then
echo "`date +'%Y-%m-%d %H:%M:%S'` /etc/apt/sources.list 写入 ubuntu 18 bionic 阿里源";
cat ~/upload/ubuntu_init/source/bionic.ini > /etc/apt/sources.list;
echo "`date +'%Y-%m-%d %H:%M:%S'` 写入 ubuntu 18 bionic 阿里源完成，查看当前/etc/apt/目录";
elif  [ `lsb_release -a | tail -n 1 | cut -f2` == "xenial" ]; then
echo "`date +'%Y-%m-%d %H:%M:%S'` /etc/apt/sources.list 写入 ubuntu 16 xenial 阿里源";
cat ~/upload/ubuntu_init/source/xenial.ini > /etc/apt/sources.list;
echo "`date +'%Y-%m-%d %H:%M:%S'` 写入 ubuntu 16 xenial 阿里源完成，查看当前/etc/apt/目录";
elif  [ `lsb_release -a | tail -n 1 | cut -f2` == "buster" ]; then
echo "`date +'%Y-%m-%d %H:%M:%S'` /etc/apt/sources.list 写入 raspbian buster 清华源";
cat ~/upload/ubuntu_init/source/raspbian.ini > /etc/apt/sources.list;
wget -O - http://pipplware.pplware.pt/pipplware/key.asc | sudo apt-key add -;
sudo apt-get update;
echo y | sudo apt-get install wget;
wget -O - http://pipplware.pplware.pt/pipplware/key.asc | sudo apt-key add -;
sudo apt-get update;
echo "`date +'%Y-%m-%d %H:%M:%S'` 写入 raspbian buster 清华源完成，查看当前/etc/apt/目录";
else echo "系统不在源范围，核实后加入源用于更新！";
  curl --request POST \
  --url "https://oapi.dingtalk.com/robot/send?access_token=813a36738938895e634ee516186fc0f8baec01d7792da5030dcc2a904bdc501b" \
  --header "content-type: application/json" \
  --data "{
     'msgtype': 'markdown',
     'markdown': {'title':'预警',
                  'text':'# init系统警报 \n> `date +"%Y-%m-%d %H:%M:%S"`  \n### `hostname`\n- `lsb_release -a | tail -n 1 | cut -f2`\n- 系统不在源范围，核实后加入源用于更新！'
     },
    'at': {
        'atMobiles': [
            '1825718XXXX'
        ],
        'isAtAll': true
    }
 }";
fi;

ls -lah /etc/apt/;
echo "`date +'%Y-%m-%d %H:%M:%S'` 查看 /etc/apt/sources.list 源";
cat /etc/apt/sources.list;
echo "`date +'%Y-%m-%d %H:%M:%S'` 刷新 /etc/apt/sources.list 源";
sudo apt-get update;
echo "`date +'%Y-%m-%d %H:%M:%S'` 刷新 /etc/apt/sources.list 源完成";



echo "#######################################################";
echo "##### 05. crontab 定时优化内存 and 开机发送ip到钉钉 #####";
echo "#######################################################";
echo "`date +'%Y-%m-%d %H:%M:%S'` 当前crontabs目录：";
sudo ls -lah  /var/spool/cron/crontabs;
echo "`date +'%Y-%m-%d %H:%M:%S'` 若不存在/var/spool/cron/crontabs/root文件则创建";
sudo echo " " >> /var/spool/cron/crontabs/root;
echo "`date +'%Y-%m-%d %H:%M:%S'` 改变/var/spool/cron/crontabs/root所属组为crontab";
sudo chgrp crontab /var/spool/cron/crontabs/root;
echo "`date +'%Y-%m-%d %H:%M:%S'` 当前crontabs目录：";
sudo ls -lah  /var/spool/cron/crontabs;
echo "`date +'%Y-%m-%d %H:%M:%S'` 当前/var/spool/cron/crontabs/root文件内容";
cat /var/spool/cron/crontabs/root | grep -v "^#" | grep -v "^$";
echo "`date +'%Y-%m-%d %H:%M:%S'` 若不存在定时清理内存脚本则添加";
sed -i '/echo 3/d' /var/spool/cron/crontabs/root;
sed -i '/sh /d' /var/spool/cron/crontabs/root ;
sed -i '/^ /d' /var/spool/cron/crontabs/root ;
echo "*/1 * * * * echo 3 >/proc/sys/vm/drop_caches" >> /var/spool/cron/crontabs/root;
echo "*/1 * * * * sh /home/neo/upload/ubuntu_init/script/801.startup.sh" >> /var/spool/cron/crontabs/root;
echo "`date +'%Y-%m-%d %H:%M:%S'` 当前/var/spool/cron/crontabs/root文件内容";
cat /var/spool/cron/crontabs/root | grep -v "^#" | grep -v "^$";
echo "`date +'%Y-%m-%d %H:%M:%S'` 生效需要进入一次crontab";
crontab -e;
echo "`date +'%Y-%m-%d %H:%M:%S'` crontab 定时优化内存完成";
echo "####################################";
echo "####################################";
echo "##### 06. 关闭笔记本盒盖休眠 #####";
echo "####################################";
sudo sed -i '/^HandleLidSwitch=ignore/d' /etc/systemd/logind.conf;
sudo echo "HandleLidSwitch=ignore" >> /etc/systemd/logind.conf;
echo "`date +'%Y-%m-%d %H:%M:%S'` 关闭笔记本盒盖休眠完成";
echo "####################################";
echo "##### 07. 清空 所有 root history #####";
echo "####################################";
echo "`date +'%Y-%m-%d %H:%M:%S'` 清空 所有 root history完成";


echo "####################################";
echo "##### 00.修复source: not found错误 #####";
echo "####################################";
echo "`date +'%Y-%m-%d %H:%M:%S'` 当前 /bin/sh";
ls -l /bin/sh;
echo "dpkg-reconfigure dash 界面 选择 no";
sleep 5;
dpkg-reconfigure dash;
echo "`date +'%Y-%m-%d %H:%M:%S'` 修改后的 /bin/sh";




echo "####################################";
echo "##### 01.设置 sudo免密码 #####";
echo "####################################";
echo "`date +'%Y-%m-%d %H:%M:%S'` 当前 /etc/sudoers 文件";
ls -lah /etc/sudoers;
echo "`date +'%Y-%m-%d %H:%M:%S'` 覆盖备份 /etc/sudoers_bak 文件";
sudo cp -rf /etc/sudoers /etc/sudoers_bak;
echo "`date +'%Y-%m-%d %H:%M:%S'` 查看 当前 /etc/sudoers 文件";
cat /etc/sudoers | grep -v "^#" | grep -v "^$";
echo "`date +'%Y-%m-%d %H:%M:%S'` 若不存在neo sudo免密则添加";
sed -i '/^%sudo    ALL=(ALL:ALL) NOPASSWD: ALL/d' /etc/sudoers;
sed -i '/^neo      ALL=(ALL:ALL) NOPASSWD: ALL/d' /etc/sudoers;
echo "%sudo    ALL=(ALL:ALL) NOPASSWD: ALL" >> /etc/sudoers;
echo "neo      ALL=(ALL:ALL) NOPASSWD: ALL" >> /etc/sudoers;
echo "`date +'%Y-%m-%d %H:%M:%S'` 处理完成，查看 /etc/sudoers 文件";
cat /etc/sudoers | grep -v "^#" | grep -v "^$";



echo "#########################################";
echo "##### 01.5 添加 host 使得github 加速 #####";
echo "#########################################";
echo "`date +'%Y-%m-%d %H:%M:%S'` 当前 /etc/hosts 文件";
ls -lah /etc/hosts;
echo "`date +'%Y-%m-%d %H:%M:%S'` 覆盖备份 /etc/hosts_bak 文件";
sudo cp -rf /etc/hosts /etc/hosts_bak;
echo "`date +'%Y-%m-%d %H:%M:%S'` 查看 当前 /etc/hosts 文件";
cat /etc/hosts | grep -v "^#" | grep -v "^$";
echo "`date +'%Y-%m-%d %H:%M:%S'` 若不存在 github 加速 则增加 host ";
sed -i '/^151.101.185.194 github.global.ssl.fastly.net/d' /etc/hosts;
sed -i '/^192.30.253.112 github.com/d' /etc/hosts;
sed -i '/^151.101.72.133 assets-cdn.github.com/d' /etc/hosts;
echo "151.101.185.194 github.global.ssl.fastly.net" >> /etc/hosts;
echo "192.30.253.112 github.com" >> /etc/hosts;
echo "151.101.72.133 assets-cdn.github.com" >> /etc/hosts;
echo "`date +'%Y-%m-%d %H:%M:%S'` 处理完成，查看 /etc/hosts 文件";
cat /etc/hosts | grep -v "^#" | grep -v "^$";





echo "####################################";
echo "##### 02. 优化 history数量和输出 ####";
echo "####################################";
echo "`date +'%Y-%m-%d %H:%M:%S'` 当前 /etc/profile文件";
ls -lah /etc/profile;
echo "`date +'%Y-%m-%d %H:%M:%S'` 覆盖备份 /etc/profile_bak 文件";
sudo cp -rf /etc/profile /etc/profile_bak;
echo "`date +'%Y-%m-%d %H:%M:%S'` 查看当前 /etc/profile 文件内容";
cat /etc/profile | grep -v "^#" | grep -v "^$";
echo "`date +'%Y-%m-%d %H:%M:%S'` 若不存在HISTSIZE=70000和whoami则添加 ";
sed -i '/^HISTSIZE=70000/d' /etc/profile;
sed -i '/^export HISTTIMEFORMAT="%F %T `whoami` "/d' /etc/profile;
echo "HISTSIZE=70000" >> /etc/profile;
echo 'export HISTTIMEFORMAT="%F %T `whoami` "' >> /etc/profile;
echo "`date +'%Y-%m-%d %H:%M:%S'` 处理完成，查看 /etc/profile 文件内容";
cat /etc/profile | grep -v "^#" | grep -v "^$";
echo "`date +'%Y-%m-%d %H:%M:%S'` source /etc/profile 在脚本执行结束后生效";
# source /etc/profile;



echo "#####################################";
echo "###### 03. 确保 安装 lsb-core  #######";
echo "#####################################";
echo "`date +'%Y-%m-%d %H:%M:%S'` 目的是在更新源前，检测系统版本";
echo y | sudo apt-get install lsb-core;
echo "`date +'%Y-%m-%d %H:%M:%S'` lsb-core 安装完成";



echo "#####################################";
echo "###### 04. 替换为国内适合的源 ########";
echo "#####################################";
echo "`date +'%Y-%m-%d %H:%M:%S'` 当前/etc/apt/目录";
ls -lah /etc/apt/;
echo "`date +'%Y-%m-%d %H:%M:%S'` 删除/etc/apt/sources.list*";
sudo rm -rf /etc/apt/sources.list;
sudo rm -rf /etc/apt/sources.list.d;
if  [ `lsb_release -a | tail -n 1 | cut -f2` == "bionic" ]; then
echo "`date +'%Y-%m-%d %H:%M:%S'` /etc/apt/sources.list 写入 ubuntu 18 bionic 阿里源";
cat ~/upload/ubuntu_init/source/bionic.ini > /etc/apt/sources.list;
echo "`date +'%Y-%m-%d %H:%M:%S'` 写入 ubuntu 18 bionic 阿里源完成，查看当前/etc/apt/目录";
elif  [ `lsb_release -a | tail -n 1 | cut -f2` == "xenial" ]; then
echo "`date +'%Y-%m-%d %H:%M:%S'` /etc/apt/sources.list 写入 ubuntu 16 xenial 阿里源";
cat ~/upload/ubuntu_init/source/xenial.ini > /etc/apt/sources.list;
echo "`date +'%Y-%m-%d %H:%M:%S'` 写入 ubuntu 16 xenial 阿里源完成，查看当前/etc/apt/目录";
elif  [ `lsb_release -a | tail -n 1 | cut -f2` == "buster" ]; then
echo "`date +'%Y-%m-%d %H:%M:%S'` /etc/apt/sources.list 写入 raspbian buster 清华源";
cat ~/upload/ubuntu_init/source/raspbian.ini > /etc/apt/sources.list;
wget -O - http://pipplware.pplware.pt/pipplware/key.asc | sudo apt-key add -;
sudo apt-get update;
echo y | sudo apt-get install wget;
wget -O - http://pipplware.pplware.pt/pipplware/key.asc | sudo apt-key add -;
sudo apt-get update;
echo "`date +'%Y-%m-%d %H:%M:%S'` 写入 raspbian buster 清华源完成，查看当前/etc/apt/目录";
else echo "系统不在源范围，核实后加入源用于更新！";
  curl --request POST \
  --url "https://oapi.dingtalk.com/robot/send?access_token=813a36738938895e634ee516186fc0f8baec01d7792da5030dcc2a904bdc501b" \
  --header "content-type: application/json" \
  --data "{
     'msgtype': 'markdown',
     'markdown': {'title':'预警',
                  'text':'# init系统警报 \n> `date +"%Y-%m-%d %H:%M:%S"`  \n### `hostname`\n- `lsb_release -a | tail -n 1 | cut -f2`\n- 系统不在源范围，核实后加入源用于更新！'
     },
    'at': {
        'atMobiles': [
            '1825718XXXX'
        ],
        'isAtAll': true
    }
 }";
fi;

ls -lah /etc/apt/;
echo "`date +'%Y-%m-%d %H:%M:%S'` 查看 /etc/apt/sources.list 源";
cat /etc/apt/sources.list;
echo "`date +'%Y-%m-%d %H:%M:%S'` 刷新 /etc/apt/sources.list 源";
sudo apt-get update;
echo "`date +'%Y-%m-%d %H:%M:%S'` 刷新 /etc/apt/sources.list 源完成";



echo "#######################################################";
echo "##### 05. crontab 定时优化内存 and 开机发送ip到钉钉 #####";
echo "#######################################################";
echo "`date +'%Y-%m-%d %H:%M:%S'` 当前crontabs目录：";
sudo ls -lah  /var/spool/cron/crontabs;
echo "`date +'%Y-%m-%d %H:%M:%S'` 若不存在/var/spool/cron/crontabs/root文件则创建";
sudo echo " " >> /var/spool/cron/crontabs/root;
echo "`date +'%Y-%m-%d %H:%M:%S'` 改变/var/spool/cron/crontabs/root所属组为crontab";
sudo chgrp crontab /var/spool/cron/crontabs/root;
echo "`date +'%Y-%m-%d %H:%M:%S'` 当前crontabs目录：";
sudo ls -lah  /var/spool/cron/crontabs;
echo "`date +'%Y-%m-%d %H:%M:%S'` 当前/var/spool/cron/crontabs/root文件内容";
cat /var/spool/cron/crontabs/root | grep -v "^#" | grep -v "^$";
echo "`date +'%Y-%m-%d %H:%M:%S'` 若不存在定时清理内存脚本则添加";
sed -i '/echo 3/d' /var/spool/cron/crontabs/root;
sed -i '/sh /d' /var/spool/cron/crontabs/root ;
sed -i '/^ /d' /var/spool/cron/crontabs/root ;
echo "*/1 * * * * echo 3 >/proc/sys/vm/drop_caches" >> /var/spool/cron/crontabs/root;
echo "*/1 * * * * sh /home/neo/upload/ubuntu_init/script/801.startup.sh" >> /var/spool/cron/crontabs/root;
echo "`date +'%Y-%m-%d %H:%M:%S'` 当前/var/spool/cron/crontabs/root文件内容";
cat /var/spool/cron/crontabs/root | grep -v "^#" | grep -v "^$";
echo "`date +'%Y-%m-%d %H:%M:%S'` 生效需要进入一次crontab";
crontab -e;
echo "`date +'%Y-%m-%d %H:%M:%S'` crontab 定时优化内存完成";
echo "####################################";
echo "####################################";
echo "##### 06. 关闭笔记本盒盖休眠 #####";
echo "####################################";
sudo sed -i '/^HandleLidSwitch=ignore/d' /etc/systemd/logind.conf;
sudo echo "HandleLidSwitch=ignore" >> /etc/systemd/logind.conf;
echo "`date +'%Y-%m-%d %H:%M:%S'` 关闭笔记本盒盖休眠完成";
echo "####################################";
echo "##### 07. 清空 所有 root history #####";
echo "####################################";
echo "`date +'%Y-%m-%d %H:%M:%S'` 清空 所有 root history完成";


sh ~/upload/ubuntu_init/init/02.ubuntu_init_user.sh

sh ~/upload/ubuntu_init/init/02.ubuntu_init_user.sh

sh ~/upload/ubuntu_init/init/03.ubuntu_init_user.sh

sh ~/upload/ubuntu_init/init/04.clear_env_init_user.sh


startTS="`date +%s.%N`";
cd ~/upload;
sh ~/upload/ubuntu_init/script/01.*.sh;
sh ~/upload/ubuntu_init/script/02.*.sh;
sh ~/upload/ubuntu_init/script/03.*.sh;
sh ~/upload/ubuntu_init/script/04.*.sh && \
sh ~/upload/ubuntu_init/script/05.*.sh && \
sh ~/upload/ubuntu_init/script/06.*.sh && \
sh ~/upload/ubuntu_init/script/07.*.sh && \
sh ~/upload/ubuntu_init/script/08.*.sh && \
sh ~/upload/ubuntu_init/script/09.*.sh && \
sh ~/upload/ubuntu_init/script/10.*.sh;
endTS="`date +%s.%N`";
result=`gawk -v x=$startTS -v y=$endTS 'BEGIN{printf "%.6f\n",y-x}'`; # 总秒数
_hour=`gawk -v x=$result -v y=3600 'BEGIN{printf "%d \n",x/y}'`; # 时数
_min=`gawk -v x=$result -v y=60 -v z=$_hour 'BEGIN{printf "%d \n",(x-z*3600)/y}'`; # 分数
_sec=`gawk -v x=$result -v y=60 -v z=$_hour -v w=$_min 'BEGIN{printf "%d \n", x - z*3600 - w*60 }'`; # 秒数
echo "`date +'%Y-%m-%d %H:%M:%S'` 全部自动化安装耗时 $_hour 小时 $_min 分钟 $_sec 秒 ";
