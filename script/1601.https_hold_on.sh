#!/bin/sh -e

# 这个脚本加在 neo_pub上面的

sddmd(){
  curl --silent \
       --request POST \
       --url "https://oapi.dingtalk.com/robot/send?access_token=813a36738938895e634ee516186fc0f8baec01d7792da5030dcc2a904bdc501b" \
       --header "content-type: application/json" \
       --data "{
            'msgtype': 'markdown',
            'markdown': {'title':'预警',
       'text':'# $1 \n> `date +"%Y-%m-%d %H:%M:%S"`  \n### `hostname`\n- $2'
            },
           'at': {
               'atMobiles': [
                   '1825718XXXX'
               ],
               'isAtAll': true
           }
        }"
}


crontab_gen_key(){
/home/neo/.acme.sh/acme.sh  \
--renew  \
-d ${1:-"*.neo.pub"} \
--server https://acme.freessl.cn/directory \
--yes-I-know-dns-manual-mode-enough-go-ahead-please \
--dns
}


check_https(){
if [ `crontab_gen_key ${1:-"*.neo.pub"} | wc -l` -eq 3 ] ; then 
echo "`date +'%Y-%m-%d %H:%M:%S'` https ${1:-"*.neo.pub"} hold on"
else
echo "`date +'%Y-%m-%d %H:%M:%S'` https ${1:-"*.neo.pub"} need gen new one"
crontab_gen_key ${1:-"*.neo.pub"};
echo "`date +'%Y-%m-%d %H:%M:%S'` https ${1:-"*.neo.pub"} gen key success"
cp -rf /home/neo/.acme.sh/${1:-"*.neo.pub"} /data/nginx/ssl
sddmd "https ${1:-"*.neo.pub"}" "https key gen and move success"
fi # 判断结束，以fi结尾
}


check_https "*.neo.pub";
check_https "*.frp.neo.pub";
check_https "neo.pub";
check_https "gaga.ink";





# # 这里的3
# # 程序本身 nohup  ~/upload/frp/frpc -c ~/upload/frp/frpc.ini > ~/upload/frp/frpc.log 2>&1 &
# # grep frpc 高亮
# # 这个脚本 含 frpc
# if [ `crontab_gen_key "*.neo.pub" | wc -l` -eq 3 ] ; then 
# echo "`date +'%Y-%m-%d %H:%M:%S'` https hold on"
# else
# echo "`date +'%Y-%m-%d %H:%M:%S'` https need gen new one"
# crontab_gen_key "*.neo.pub";
# echo "`date +'%Y-%m-%d %H:%M:%S'` https gen key success"
# cp -rf /home/neo/.acme.sh/*.neo.pub /data/nginx/ssl
# sddmd "https neo.pub" "https key gen and move success"
# fi # 判断结束，以fi结尾


# if [ `crontab_gen_key "*.frp.neo.pub" | wc -l` -eq 3 ] ; then 
# echo "`date +'%Y-%m-%d %H:%M:%S'` https hold on"
# else
# echo "`date +'%Y-%m-%d %H:%M:%S'` https need gen new one"
# crontab_gen_key "*.frp.neo.pub";
# echo "`date +'%Y-%m-%d %H:%M:%S'` https gen key success"
# cp -rf /home/neo/.acme.sh/*.frp.neo.pub /data/nginx/ssl
# sddmd "https frp.neo.pub" "https key gen and move success"
# fi # 判断结束，以fi结尾