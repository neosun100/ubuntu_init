
echo "####################################################"
echo "################ 09.npm 第三方包 ####################"
echo "###################################################"

echo "`date +'%Y-%m-%d %H:%M:%S'` npm 换源!";

npm config set registry https://registry.npm.taobao.org;
npm config get registry;
npm info express;
npm install -g cnpm --registry=https://registry.npm.taobao.org;
cnpm install express;
echo "`date +'%Y-%m-%d %H:%M:%S'` npm 换源 完成!";


echo "`date +'%Y-%m-%d %H:%M:%S'` npm 安装第三方模块!";
# 快速查询命令行工具 man2
npm install man2 -g;
cd ~/upload;
git clone https://github.com/letiantian/man2-docs.git ~/.man2-docs;


# 更好的 命令行速查工具 TLDR 页：Linux 手册页的简化替代品
npm install -g tldr;
tldr  --update; # 刷新缓存


# Easy and safe way to manage your crontab file
npm install -g crontab-ui;
#
# HOST=0.0.0.0 PORT=10000 crontab-ui --autosave

# nodejs 进程管理器
npm install pm2 -g;

# 13.cloc：代码统计工具，能够统计代码的空行数、注释行、编程语言。
npm install -g cloc;

# 安装代理抓取
npm install -g anyproxy;
echo "执行如下命令运行";
echo "anyproxy -i";
sleep 5;


# 静态文件服务器,就是http服务器
npm install http-server -g;


# 图片查看器 cishower
#简略，只是个样子
npm install cishower -g;



# json 2 table 神器 markdown-table-cli
# https://github.com/neosun100/markdown-table-cli
# md-table
npm install -g markdown-table-cli;

#reveal-md md 2 ppt
# github https://github.com/webpro/reveal-md
npm install -g reveal-md;
reveal-md -h;

# markdown 2 map
npm install markmap-lib -g;
markmap -h;


npm install -g zero;
# https://www.ctolib.com/remoteinterview-zero.html


npm install -g insomnia-inso

# Once you have installed Node.js, you can install Inso globally on your system by running the following code in your terminal:


npm install -g gnomon 
# 评估执行之间的命令


npm install wechat-format-cli -g
# 此处借用一个开源项目wechat-format-cli，可以做到使用命令行来转换Markdown到公众号的文章去。



#  npm install -g json2csv 
# 新版本有bug
 npm install -g json2csv@5.0.7
 # json2 csv  非常好的lib



npm install -g wscat
# ws 客户端，非常好用 CLI
# wscat -c ws://localhost:8080
# wscat --help


echo "`date +'%Y-%m-%d %H:%M:%S'` npm 安装第三方模块完成!";
