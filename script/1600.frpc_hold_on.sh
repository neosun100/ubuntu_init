#!/bin/sh -e
sddmd(){
  curl --silent \
       --request POST \
       --url "https://oapi.dingtalk.com/robot/send?access_token=813a36738938895e634ee516186fc0f8baec01d7792da5030dcc2a904bdc501b" \
       --header "content-type: application/json" \
       --data "{
            'msgtype': 'markdown',
            'markdown': {'title':'预警',
       'text':'# $1 \n> `date +"%Y-%m-%d %H:%M:%S"`  \n### `/bin/cat /proc/sys/kernel/hostname`\n- $2'
            },
           'at': {
               'atMobiles': [
                   '1825718XXXX'
               ],
               'isAtAll': true
           }
        }"
}

# 这里的3
# 程序本身 nohup  ~/upload/frp/frpc -c ~/upload/frp/frpc.ini > ~/upload/frp/frpc.log 2>&1 &
# grep frpc 高亮
# 这个脚本 含 frpc
if [ `ps -ef | grep frpc | grep -v ansible | wc -l` -gt 3 ] ; then 
echo "`date +'%Y-%m-%d %H:%M:%S'` frpc exist"
else
echo "`date +'%Y-%m-%d %H:%M:%S'` frpc is not exist"
[[ `uname` = "Linux" ]] && (nohup  ~/upload/frp/frpc -c ~/upload/frp/frpc.ini > ~/Logs/frpc.log 2>&1 &) || (nohup  frpc -c ~/upload/frp/frpc.ini > ~/Logs/frpc.log 2>&1 &)
echo "`date +'%Y-%m-%d %H:%M:%S'` frpc restart success"
sddmd "frpc status" "restart success"
fi # 判断结束，以fi结尾