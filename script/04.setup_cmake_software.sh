echo "####################################################"
echo "################ 04.编译 安装 ######################"
echo "###################################################"
cd ~/upload;
echo "`date +'%Y-%m-%d %H:%M:%S'` 编译安装 mwget！";
# wget https://data-1251390522.cos.ap-shanghai.myqcloud.com/Sys/mwget_0.1.0.orig.tar.bz2
# tar -xjvf mwget_0.1.0.orig.tar.bz2 >/dev/null 2>&1;
#wget http://jaist.dl.sourceforge.net/project/kmphpfm/mwget/0.1/mwget_0.1.0.orig.tar.bz2;
#tar -xjvf mwget_0.1.0.orig.tar.bz2 >/dev/null 2>&1;
# wget https://data-1251390522.cos.ap-shanghai.myqcloud.com/Sys/mwget_0.1.0.orig.tgz;
wget http://cdn.neo.pub/image/mwget_0.1.0.orig.tar.bz2
tar -jxvf mwget_0.1.0.orig.tar.bz2 >/dev/null 2>&1; 

cd ~/upload/mwget_0.1.0.orig/;

./configure >/dev/null 2>&1;
sudo make >/dev/null 2>&1;
sudo make install >/dev/null 2>&1;
mwget -h;
echo "`date +'%Y-%m-%d %H:%M:%S'` 编译安装 mwget 完成！";


cd ~/upload;
echo "`date +'%Y-%m-%d %H:%M:%S'` 编译安装 sshpass！用于ansible";
wget https://neone-club.sh1a.qingstor.com/Sys/sshpass-1.06.tar.gz;
tar -zxvf sshpass-1.06.tar.gz >/dev/null 2>&1;
cd ./sshpass-1.06/;
./configure;
sudo make >/dev/null 2>&1 && sudo make install >/dev/null 2>&1;
sshpass -h;
echo "`date +'%Y-%m-%d %H:%M:%S'` 编译安装 sshpass 完成";

cd ~/upload;
echo "`date +'%Y-%m-%d %H:%M:%S'` 编译安装 web压测工具——webbench 注意:不支持 post";
wget http://home.tiscali.cz/~cz210552/distfiles/webbench-1.5.tar.gz
tar -zxvf webbench-1.5.tar.gz >/dev/null 2>&1;
cd ./webbench-1.5/
sudo make >/dev/null 2>&1 && sudo make install >/dev/null 2>&1;
webbench -h;
echo "`date +'%Y-%m-%d %H:%M:%S'` 编译安装 web压测工具——webbench 完成";

cd ~/upload;
echo "`date +'%Y-%m-%d %H:%M:%S'` 编译安装 web压测工具 http_load";
wget http://cdn.neo.pub/http_load-09Mar2016.tar
tar -xf http_load-09Mar2016.tar >/dev/null 2>&1;
cd http_load-09Mar2016;
sudo make >/dev/null 2>&1 && sudo make install >/dev/null 2>&1;
http_load -h;
echo "`date +'%Y-%m-%d %H:%M:%S'` 编译安装 web压测工具 http_load 完成！";

# cd ~/upload;
# echo "`date +'%Y-%m-%d %H:%M:%S'` 编译安装 黑客工具 joy ！";
# git clone https://github.com/cisco/joy.git;
# cd joy/;
# echo y | sudo apt-get install build-essential libssl-dev libpcap-dev libcurl4-openssl-dev
# echo y | sudo apt-get install build-essential
# ./config # -l /usr/lib/x86_64-linux-gnu/
# sudo make;
# sudo make install;
# joy --help;
# whereis joy;
# echo "`date +'%Y-%m-%d %H:%M:%S'` 编译安装 黑客工具 joy 完成！";


cd ~/upload;
echo "`date +'%Y-%m-%d %H:%M:%S'` 编译安装 shell 下标准输出二维码生成工具：QRencode ！";
# 安装 libpng
wget --no-check-certificate https://data-1251390522.cos.ap-shanghai.myqcloud.com/Sys/libpng-1.6.35.tar.xz;
xz -d ***.tar.xz >/dev/null 2>&1
tar -xvf libpng-1.6.35.tar >/dev/null 2>&1
sudo chmod -R 777 libpng-1.6.35;
cd libpng-1.6.35/
./configure --prefix=/usr/local/source/libpng >/dev/null 2>&1
sudo make >/dev/null 2>&1;
sudo make install >/dev/null 2>&1;

# 安装使用 qrencode
echo y | sudo apt-get install qrencode;
qrencode -h;
echo "`date +'%Y-%m-%d %H:%M:%S'` 编译安装 shell 下标准输出二维码生成工具：QRencode 安装完成！";


# mac 下 安装 qrencode
# brew install qrencode


echo "`date +'%Y-%m-%d %H:%M:%S'` 编译安装 aria2";
cd ~/upload;
sudo apt-get install -y libcurl4-openssl-dev libevent-dev ca-certificates libssl-dev pkg-config build-essential intltool libgcrypt-dev libssl-dev libxml2-dev
wget --no-check-certificate https://data-1251390522.cos.ap-shanghai.myqcloud.com/Sys/aria2-1.34.0.tar.gz;
tar xzvf aria2-*.tar.gz >/dev/null 2>&1;
cd aria2-1.34.0;
./configure >/dev/null 2>&1;
sudo make >/dev/null 2>&1 && sudo make install >/dev/null 2>&1;
aria2c -h;
echo "`date +'%Y-%m-%d %H:%M:%S'` 编译安装 aria2 完成";



echo "`date +'%Y-%m-%d %H:%M:%S'` 编译安装 中文 man";
cd ~/upload;
wget --no-check-certificate https://data-1251390522.cos.ap-shanghai.myqcloud.com/Sys/manpages-zh-1.5.1.tar.gz;
# wget https://storage.googleapis.com/google-code-archive-downloads/v2/code.google.com/manpages-zh/manpages-zh-1.5.1.tar.gz --no-check-certificate;
# wget https://data-1251390522.cos.ap-shanghai.myqcloud.com/Sys/manpages-zh-1.6.2.1.tar.gz;
# https://www.cnblogs.com/fyc119/p/7116295.html
# https://src.fedoraproject.org/repo/pkgs/man-pages-zh-CN/
# 
tar xzvf manpages-*.tar.gz >/dev/null 2>&1;
cd manpages-zh-1.5.1;
./configure --disable-zhtw  --prefix=/usr/local/zhman;
sudo make >/dev/null 2>&1 && sudo make install >/dev/null 2>&1;
echo "`date +'%Y-%m-%d %H:%M:%S'` 编译安装 中文 man 完成";



echo "`date +'%Y-%m-%d %H:%M:%S'` 编译安装 best py top";
cd ~/upload;
git clone https://github.com/aristocratos/bpytop.git
cd bpytop
sudo make install  

echo "`date +'%Y-%m-%d %H:%M:%S'` 编译安装 best py top 完成";
cd ~/upload;

# echo "`date +'%Y-%m-%d %H:%M:%S'` 修复 中文 man乱码";
# groff -v;
# echo "版本小于1.22务必升级"
# cd ~/upload;
# wget https://data-1251390522.cos.ap-shanghai.myqcloud.com/Sys/groff-1.22.tar.gz;
# tar -zxvf groff-1.22.tar.gz >/dev/null 2>&1; 
# cd ~/upload/groff-1.22;
# ./configure >/dev/null 2>&1;
# sudo make  >/dev/null 2>&1;
# sudo make install  >/dev/null 2>&1;
echo "报错就报错吧，没关系";
sleep 3;
echo "# sudo sed -i '/preconv/d' /etc/man.conf;";
echo '# sudo echo "NROFF preconv -e UTF8 | /usr/local/bin/nroff -Tutf8 -mandoc -c" >> /etc/man.conf;';
echo "仅处理mac, linux没问题,手动处理吧";
sleep 3;
# # 安装go,要先安装go1.4,再再自举安装新版本的go,pass
# cd ~/upload;
# # https://golang.org/dl/
# echo "`date +'%Y-%m-%d %H:%M:%S'` 编译安装 go !";
#
# mwget https://dl.google.com/go/go1.11.2.src.tar.gz;
# tar -zxvf go1.11.2.src.tar.gz >/dev/null 2>&1;
# cd go/src/
# ./make.bash
# ls -lah /usr/local/go
# ls -lah /usr/local/go/bin/
# ls -lah /home/neo/upload/go
# /usr/local/go/bin/go env
# echo "`date +'%Y-%m-%d %H:%M:%S'` 编译安装 go 完成!";
