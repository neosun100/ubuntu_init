startTS="$(date +%s.%N)"
echo "################################################"
echo "#############自动化安装脚本开始执行！#############"
echo "################################################"
echo "$(date +'%Y-%m-%d %H:%M:%S') 检验上传目录是否创建"
if [ ! -d ~/upload ]; then
  mkdir ~/upload
  chmod 777 ~/upload
  echo "$(date +'%Y-%m-%d %H:%M:%S') 上传目录已创建并777！"
else
  echo "$(date +'%Y-%m-%d %H:%M:%S') 上传目录存在！"
fi

echo "$(date +'%Y-%m-%d %H:%M:%S') 检验app目录是否创建"
if [ ! -d /usr/local/app ]; then
  sudo mkdir /usr/local/app
  sudo chmod 777 /usr/local/app
  echo "$(date +'%Y-%m-%d %H:%M:%S') app目录已创建并777！"
else
  echo "$(date +'%Y-%m-%d %H:%M:%S') app目录存在！"
fi

echo "$(date +'%Y-%m-%d %H:%M:%S') 检验svg目录是否创建"
if [ ! -d ~/upload/svg ]; then
  sudo mkdir ~/upload/svg
  sudo chmod 777 ~/upload/svg
  echo "$(date +'%Y-%m-%d %H:%M:%S') svg目录已创建并777！"
else
  echo "$(date +'%Y-%m-%d %H:%M:%S') svg目录存在！"
fi

echo "$(date +'%Y-%m-%d %H:%M:%S') 检验~/unzip目录是否创建"
if [ ! -d ~/unzip ]; then
  mkdir ~/unzip
  chmod 777 ~/unzip
  echo "$(date +'%Y-%m-%d %H:%M:%S') ~/unzip目录已创建并777！"
else
  echo "$(date +'%Y-%m-%d %H:%M:%S') ~/unzip目录存在！"
fi

echo "$(date +'%Y-%m-%d %H:%M:%S') 检验arrow软链接目录是否创建"
if [ ! -d /usr/local/arrow ]; then
  sudo mkdir /usr/local/arrow
  sudo chmod 777 /usr/local/arrow
  echo "$(date +'%Y-%m-%d %H:%M:%S') arrow软链接目录已创建并777！"
else
  echo "$(date +'%Y-%m-%d %H:%M:%S') arrow软链接目录存在！"
fi

echo "##################################################"
echo "################# 01. apt-get 安装 ###############"
echo "##################################################"

echo "$(date +'%Y-%m-%d %H:%M:%S') 安装必须软件！"
echo y | sudo apt-get install smbclient
echo y | sudo apt-get install vim
# sudo add-apt-repository ppa:neovim-ppa/stable;
sudo apt-get update
sudo apt-get upgrade
# echo y | sudo apt-get install neovim;
echo y | sudo apt-get install git
echo y | sudo apt-get install tmux
echo y | sudo apt-get install tmuxinator    # tmux 管理工具
echo y | sudo apt-get install smartmontools # Linux硬盘的检测
echo y | sudo apt-get install powertop      # 电源管理
echo y | sudo apt-get install espeak        # 语音模块
echo y | sudo apt install gawk
echo y | sudo apt-get install p7zip-full
echo y | sudo apt install lolcat        # 彩虹 cat
echo y | sudo apt-get install dos2unix  # 文本转 unix dos2unix **.sh
echo y | sudo apt-get install bc        # 圆周率单核速度测试依赖
echo y | sudo apt-get install highlight # 代码高亮 比ccat好用
echo "$(date +'%Y-%m-%d %H:%M:%S') 必须软件安装完毕！"

echo "$(date +'%Y-%m-%d %H:%M:%S') 安装 snap"
echo "snap是一种全新的软件包管理方式，它类似一个容器拥有一个应用程序所有的文件和库，各个应用程序之间完全独立。所以使用snap包的好处就是它解决了应用程序之间的依赖问题，使应用程序之间更容易管理。但是由此带来的问题就是它占用更多的磁盘空间"
sleep 10
echo "#####################################################################"
echo "**************************功能介绍****************************"
echo "# 列出已经安装的snap包"
echo "sudo snap list"
echo "# 搜索要安装的snap包"
echo "sudo snap find <text to search>"
echo "# 安装一个snap包"
echo "sudo snap install <snap name>"
echo "# 更新一个snap包，如果你后面不加包的名字的话那就是更新所有的snap包"
echo "sudo snap refresh <snap name>"
echo "# 把一个包还原到以前安装的版本"
echo "sudo snap revert <snap name>"
echo "# 删除一个snap包"
echo "sudo snap remove <snap name>"
echo "#####################################################################"
echo "**************************例子展示****************************"
echo "sudo snap install clion # clion"
echo "sudo snap install pycharm # pycharm"
echo "sudo snap install netease-music --devmode --beta # 网易云音乐"
sleep 10

# udp ssh
echo y | sudo apt-get install mosh

# namp
echo y | sudo apt-get install nmap

# ack 命令 比肩grep
echo y | sudo apt-get install ack-grep
ack --help
# ag 命令 比肩 ack
echo y | sudo apt-get install silversearcher-ag
ag -h

echo y | sudo apt-get install snapd # ubuntu 16以后新增的更好的包管理器，后面的microk8s 依赖此环境

[[ $(lsb_release -a | tail -n 1 | cut -f2) == "focal" ]] && echo "ubuntu 20 无法安装 snapcraft pass" || (echo y | sudo apt-get install snapcraft)

echo y | sudo apt-get install snapcraft # snap 应用商店
echo "$(date +'%Y-%m-%d %H:%M:%S') 安装 snap完成"

# echo "`date +'%Y-%m-%d %H:%M:%S'` 安装 microk8s,可与 minikube 隔离";
# sudo snap install microk8s --classic;
# sudo snap info microk8s;
# echo "`date +'%Y-%m-%d %H:%M:%S'` 安装 microk8s 完成";

# echo "`date +'%Y-%m-%d %H:%M:%S'` 关闭 microk8s 节省内存，按需开启";
# sudo microk8s.stop;
# echo "`date +'%Y-%m-%d %H:%M:%S'` 关闭 microk8s 完成";

# echo "`date +'%Y-%m-%d %H:%M:%S'` 安装 kubectl";
# sudo snap install kubectl --classic;
# sudo snap info kubectl;
# echo "`date +'%Y-%m-%d %H:%M:%S'` 安装 kubectl 完成";
# echo "`date +'%Y-%m-%d %H:%M:%S'` 查看 kubectl 版本";
# sudo kubectl version;

echo "$(date +'%Y-%m-%d %H:%M:%S') 安装下载传输类软件!"
echo y | sudo apt install curl
echo y | sudo apt-get install axel  # 多线程HTTP/FTP下载工具 axel
echo y | sudo apt-get install lrzsz # 允许拖拽上传
echo y | sudo apt-get install aria2 # 据说是最好的下载器，支持所有
echo "$(date +'%Y-%m-%d %H:%M:%S') 下载传输类软件安装完成!"

echo "$(date +'%Y-%m-%d %H:%M:%S') 安装文件容量浏览类软件!"
echo y | sudo apt-get install ncdu   # 交互式 查询文件夹 Disk 占用情况 ncdu
echo y | sudo apt-get install ranger # 文件浏览 ranger
echo y | sudo apt-get install tree   # 文件结构浏览 tree
echo "$(date +'%Y-%m-%d %H:%M:%S') 文件容量浏览类软件安装完成!"

echo "$(date +'%Y-%m-%d %H:%M:%S') 安装文件查看软件!"
echo y | sudo apt-get install enca # 查看文件编码并转码 enca
echo y | sudo apt-get install jq   # 高亮json

echo y | sudo apt-get install aview      # shell 下图片/视频 转 ascii 查看 超赞
echo y | sudo apt install imagemagick    # 图像处理
echo y | sudo apt-get install caca-utils # 图片查看 cacaview 彩色ascii
echo "$(date +'%Y-%m-%d %H:%M:%S') 文件查看软件安装完成!"

echo "$(date +'%Y-%m-%d %H:%M:%S') 安装甜点类软件!"
echo y | sudo apt-get install toilet     # 实心艺术字
echo y | sudo apt-get install figlet     # 空心艺术字
echo y | sudo apt-get install sl         # 恶作剧
echo y | sudo apt-get install cmatrix    # 骇客帝国
echo y | sudo apt-get install fortune    # 名言警句
echo y | sudo apt-get install fortune-zh # 名言警句
echo y | sudo apt-get install rig        # 人名生成器
echo y | sudo apt-get install linuxlogo  # logo
echo y | sudo apt-get install graphviz   # xmind
echo y | sudo apt-get install goaccess   # 日志分析工具
echo "$(date +'%Y-%m-%d %H:%M:%S') 甜点类软件安装完成!"

echo "$(date +'%Y-%m-%d %H:%M:%S') 安装视频音频编码处理 FFmpeg!"
echo y | sudo apt-get install yasm
echo y | sudo apt-get install ffmpeg
echo "$(date +'%Y-%m-%d %H:%M:%S') 视频音频编码处理 FFmpeg 安装完成!"

echo "$(date +'%Y-%m-%d %H:%M:%S') 安装监控类软件!"
echo y | sudo apt-get install htop
echo y | sudo apt-get install dstat       # 磁盘监控
echo y | sudo apt-get install slurm       # 网卡监控，它会自动生成 ASCII 图形输出
echo y | sudo apt-get install nload       # 时间戳网速监控
echo y | sudo apt-get install iftop       # 时间戳 端口流量监控
echo y | sudo apt-get install iptraf      # IP局域网监控
echo y | sudo apt-get install nethogs     # 进程流量监控
echo y | sudo apt-get install speedometer # 可视化流量监控
echo y | sudo apt-get install lnav        # 神级日志查看
echo y | sudo apt-get install net-tools   # 端口查看 netstat

# cpu温度检测,不是所有的CPU都能检测，可能和架构有关系
echo y | sudo apt-get install lm-sensors
sudo modprobe coretemp
echo "$(date +'%Y-%m-%d %H:%M:%S') 监控类软件安装完成!"

echo "$(date +'%Y-%m-%d %H:%M:%S') 安装编译安装前提!"
echo y | sudo apt-get install intltool
echo y | sudo apt-get install libssl-dev
echo y | sudo apt-get install gcc
echo y | sudo apt-get install cmake
echo y | sudo apt-get install libgcrypt-dev
echo y | sudo apt-get install libxml2-dev
echo y | sudo apt-get install g++
echo "$(date +'%Y-%m-%d %H:%M:%S') 编译安装前提安装完成!"

# https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-docker-ce-1
echo "$(date +'%Y-%m-%d %H:%M:%S') 安装pip docker 和 compose!"
curl -fsSL https://get.docker.com | bash -s docker --mirror Aliyun
# sudo apt-get remove docker docker-engine docker.io
# sudo apt-get update
# echo y | sudo apt-get install \
#   apt-transport-https \
#   ca-certificates \
#   curl \
#   software-properties-common
# curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
# sudo apt-key fingerprint 0EBFCD88
# sudo add-apt-repository \
#   "deb [arch=$(dpkg --print-architecture)] https://download.docker.com/linux/ubuntu \
#    $(lsb_release -cs) \
#    stable"
# sudo apt-get update
# echo y | sudo apt-get install docker-ce
echo y | sudo apt-get install python-pip
echo y | sudo apt-get install python3-pip
# sudo pip install docker-compose
echo y | sudo apt-get install docker-compose

echo "$(date +'%Y-%m-%d %H:%M:%S') 安装pip docker 和 compose 完成!"

endTS="$(date +%s.%N)"
result=$(gawk -v x=$startTS -v y=$endTS 'BEGIN{printf "%.6f\n",y-x}')
echo "$(date +'%Y-%m-%d %H:%M:%S') 01. apt-get 安装 $result 秒，如有疑问请联系 Neo "

echo "$(date +'%Y-%m-%d %H:%M:%S') 安装 qperf，用于网速和延迟测试"
echo y | sudo apt-get install qperf
echo 'server端：qperf'
echo 'client端：qperf -t 60 --use_bits_per_sec {server_ip} tcp_bw tcp_lat'
sleep 5
echo "$(date +'%Y-%m-%d %H:%M:%S') 安装 qperf，用于网络带宽与网络延迟测试 完成"

echo "$(date +'%Y-%m-%d %H:%M:%S') 确保节点环境有必备软件"
# sudo snap remove microk8s && \
echo y | sudo apt-get install ebtables &&
  echo y | sudo apt-get install ethtool &&
  echo y | sudo apt-get install socat &&
  echo y | sudo apt-get install ipset

echo "$(date +'%Y-%m-%d %H:%M:%S') 安装超级压缩模块"
echo y | sudo apt-get install liblz4-tool

echo "$(date +'%Y-%m-%d %H:%M:%S') 安装cpu性能测试"
echo y | sudo apt-get install sysbench

echo "$(date +'%Y-%m-%d %H:%M:%S') 安装net性能测试"
echo y | sudo apt-get install iperf3

echo "$(date +'%Y-%m-%d %H:%M:%S') 安装最强htop"
echo y | sudo apt-get install glances

echo "$(date +'%Y-%m-%d %H:%M:%S') 安装至强htop"
echo y | sudo apt-get install bashtop


echo "$(date +'%Y-%m-%d %H:%M:%S') socks 代理工具"
echo y | sudo apt-get install tsocks

echo "$(date +'%Y-%m-%d %H:%M:%S') git 密码需求模块"
echo y | sudo apt install gnupg2 pass

echo "$(date +'%Y-%m-%d %H:%M:%S') 网络模块"
echo y | sudo apt-get install net-tools

echo y | sudo apt-get install iproute2

echo "envsubst 环境变量替换神器"
echo y | sudo apt-get install gettext-base

echo "pyboy 依赖"
echo y | sudo apt install libsdl2-dev

echo "wifi 测速 开启命令 iperf3 -s -D"
sudo apt-get install iperf3

echo "HSTR 是一款可以轻松查看、导航和搜索历史命令的小工具，它支持 Bash 和 Zsh。"
echo "快捷键 ctrl+r"
sudo apt-get install -y software-properties-common &&
  sudo add-apt-repository ppa:ultradvorka/ppa && sudo apt-get update && sudo apt-get install hstr

echo "端口转化"
echo y | sudo apt-get install polipo

echo "端口转发"
echo y | sudo apt-get install rinetd

echo "mysql client"
echo "mysqldump -u hqp_haitao  -phqp_haitao@123   -h 172.16.9.232 -P 3306 ods_zqp  --default-character-set=utf8mb4  >/exdata/ods_zqp-dump.sql"
echo y | sudo apt-get install mysql-client

echo "录音"
sudo add-apt-repository --remove ppa:osmoma/audio-recorder
sudo add-apt-repository ppa:audio-recorder/ppa
sudo apt-get -y update
echo y | sudo apt-get install --reinstall audio-recorder
sudo apt-get install alsa-utils alsa-tools alsa-tools-gui alsamixergui -y
sudo apt-get install portaudio19-dev python-all-dev python3-all-dev -y

echo "kafkacat"
sudo apt-get install kafkacat -y

# echo "`date +'%Y-%m-%d %H:%M:%S'` 安装 linuxbrew";

# # sudo mkdir -p /usr/local/Homebrew/Library/Taps/homebrew/homebrew-core;
# # cd /usr/local/Homebrew/Library/Taps/homebrew/homebrew-core;
# # sudo git clone https://mirrors.ustc.edu.cn/homebrew-core.git;
# # cd ~/upload;

# sh -c "$(curl -fsSL http://file.neo.pub/sh/brew_install.sh)";
# echo "`date +'%Y-%m-%d %H:%M:%S'` 安装 linuxbrew 完成";
# echo "http://linuxbrew.sh/";
# echo "https://docs.brew.sh/";
# echo "`date +'%Y-%m-%d %H:%M:%S'` 安装 brew app";
# sleep 5;
# /home/linuxbrew/.linuxbrew/bin/brew install fzf; # 模糊查找

# # |Token	|Match type	|Description
# # |sbtrkt	|fuzzy-match	|Items that match sbtrkt
# # |'wild	|exact-match (quoted)	|Items that include wild
# # |^music	|prefix-exact-match	|Items that start with music
# # |.mp3$	|suffix-exact-match	|Items that end with .mp3
# # |!fire	|inverse-exact-match	|Items that do not include fire
# # |!^music	|inverse-prefix-exact-match	|Items that do not start with music
# # |!.mp3$	|inverse-suffix-exact-match	|Items that do not end with .mp3

# /home/linuxbrew/.linuxbrew/bin/brew install zplug; # zsh_插件管理
# echo "https://github.com/zplug/zplug";

echo "postgresql-client"
sudo apt-get install postgresql-client -y

echo "ssd nvme 信息"
sudo apt install nvme-cli dstat sysstat glances smartmontools lm-sensors -y

echo " 行转列输出"
sudo apt-get install rs -y

echo "系统信息"
sudo apt-get install neofetch -y

echo "最强ping "
echo "hping是用于生成和解析TCPIP协议数据包的开源工具。创作者是Salvatore Sanfilippo。目前最新版是hping3，支持使用tcl脚本自动化地调用其API。hping是安全审计、防火墙测试等工作的标配工具。hping优势在于能够定制数据包的各个部分，因此用户可以灵活对目标机进行细致地探测。"
echo "hping命令是一个基于命令行的TCP/IP工具,它在UNIX上得到很好的应用,不过它并非仅仅一个ICMP请求/响应工具,它还支持TCP.UDP.ICMP,RAW-IP协议,以及一个路由模型HPING一直被用作安全工具,可以用来测试网络及主机的安全."
# hping3支持非常丰富的端口探测方式，nmap拥有的扫描方式hping3几乎都支持（除开connect方式，因为Hping3仅发送与接收包，不会维护连接，所以不支持connect方式探测）。而且Hping3能够对发送的探测进行更加精细的控制，方便用户微调探测结果。当然，Hping3的端口扫描性能及综合处理能力，无法与Nmap相比。一般使用它仅对少量主机的少量端口进行扫描。
sudo apt install hping3 -y

echo "pdftk是非常好用的PDF页面操作工具，能够切割、合并、提取指定页面等。"
echo "https://blog.csdn.net/weixin_33850015/article/details/88678877"
sudo apt-get install pdftk

echo "高级的ncdu du"
sudo apt-get install nnn

echo "htmlq"
curl https://sh.rustup.rs -sSf | sh
source ~/.cargo/env
cargo install htmlq
htmlq --help
curl -s https://kubesphere.com.cn | htmlq --attribute href a | head -n 3
echo "安装完成htmlq"



echo " perf 安装及使用"
sudo apt-get install linux-tools-common -y 
sudo apt-get install linux-tools-"$(uname -r)" -y 
sudo apt-get install linux-cloud-tools-"$(uname -r)" -y 
sudo apt-get install linux-tools-generic -y 
sudo apt-get install linux-cloud-tools-generic -y 
echo " perf 安装完成"
echo "https://www.jianshu.com/p/8d5a3fc035d5 "
echo "https://www.cnblogs.com/digdeep/p/4896235.html"