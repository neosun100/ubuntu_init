#  安装多版本 gcc
# 并开启切换器



sudo apt update

echo y | sudo apt install build-essential

gcc --version

echo y | sudo apt install software-properties-common

echo y | sudo add-apt-repository ppa:ubuntu-toolchain-r/test

# 键入以下命令安装所需的GCC和G ++版本： 


echo y | sudo apt install gcc-6 g++-6 gcc-7 g++-7 gcc-8 g++-8 gcc-9 g++-9


# 以下命令将为每个版本配置替代方案并将优先级与其关联。 默认版本是具有最高优先级的版本，在我们的例子中是gcc-9。 
echo y | sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-9 90 --slave /usr/bin/g++ g++ /usr/bin/g++-9

echo y | sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-8 80 --slave /usr/bin/g++ g++ /usr/bin/g++-8

echo y | sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-7 70 --slave /usr/bin/g++ g++ /usr/bin/g++-7

echo y | sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-6 60 --slave /usr/bin/g++ g++ /usr/bin/g++-6

# 稍后如果要更改默认版本，请使用update-alternatives命令：

sudo update-alternatives --config gcc



# xnr gpu 挖矿

sudo sysctl -w vm.nr_hugepages=128 # 消除警告


# eth 挖矿
# https://bminer.me/zh/releases/
# https://bminer.me/zh/examples/
wget https://www.bminercontent.com/releases/bminer-v15.8.2-7f347c7-amd64.tar.xz 
tar xvJf bminer-v15.8.2-7f347c7-amd64.tar.xz
# 打开miner.sh文件
