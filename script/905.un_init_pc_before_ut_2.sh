
# 多重判断参考，在 neo 账户下运行
# 末尾会因为 source 文件报错，没关系的
echo "####################################" && \
echo "##### 03. 安装 unzip 用于解压 #####" && \
echo "####################################" && \
if [ `lsb_release -a | grep "Distributor ID" | cut -f2` = "CentOS" ]; then echo `lsb_release -a | grep "Distributor ID" | cut -f2` && \
echo y | sudo yum install unzip && \
echo "`date +'%Y-%m-%d %H:%M:%S'` unzip 已安装！"; elif [ `lsb_release -a | grep "Distributor ID" | cut -f2` = "Ubuntu" ]; then echo `lsb_release -a | grep "Distributor ID" | cut -f2` && \
echo y | sudo apt-get install unzip && \
echo "`date +'%Y-%m-%d %H:%M:%S'` unzip 已安装！"; else echo "`date +'%Y-%m-%d %H:%M:%S'` `lsb_release -a | grep "Distributor ID" | cut -f2` Error 系统未知！"; fi && \
echo "####################################" && \
echo "##### 04. 检查 ~/upload 没有则创建 #####" && \
echo "####################################" && \
echo "`date +'%Y-%m-%d %H:%M:%S'` 检验上传目录是否创建" && \
if [ ! -d ~/upload ];then mkdir ~/upload && \
chmod 777 ~/upload && \
echo "`date +'%Y-%m-%d %H:%M:%S'` 上传目录已创建并777！"; else echo "`date +'%Y-%m-%d %H:%M:%S'` 上传目录存在！"; fi && \
cd ~/upload && \
echo "###############################################" && \
echo "##### 05. 下载 ubuntu_init 并刷新配置文件  #####" && \
echo "##############################################" && \
cd ~ && \
rm -rf ~/upload/ubuntu_init && \
rm -rf ~/upload/ubuntu_init-master.tar.gz && \
cd ~/upload && \
wget https://gitlab.com/neosun100/ubuntu_init/-/archive/master/ubuntu_init-master.tar.gz && \
tar -zxvf ubuntu_init-master.tar.gz >/dev/null 2>&1 && \
mv ~/upload/ubuntu_init-master ~/upload/ubuntu_init && \
echo "set smtp-auth-password=Neosun100" >> ~/upload/ubuntu_init/profile/nail.rc && \
cd ~/upload && \
rm -rf ~/.bash_profile && \
cp ~/upload/ubuntu_init/profile/.bash_profile ~/.bash_profile && \
rm -rf ~/.zshrc && \
cp ~/upload/ubuntu_init/profile/.zshrc ~/.zshrc && \
source ~/.bash_profile && \
source ~/.zshrc