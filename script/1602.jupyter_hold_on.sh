#!/bin/zsh -l

sddmd(){
  curl --silent \
       --request POST \
       --url "https://oapi.dingtalk.com/robot/send?access_token=813a36738938895e634ee516186fc0f8baec01d7792da5030dcc2a904bdc501b" \
       --header "content-type: application/json" \
       --data "{
            'msgtype': 'markdown',
            'markdown': {'title':'预警',
       'text':'# $1 \n> `date +"%Y-%m-%d %H:%M:%S"`  \n### `hostname`\n- $2'
            },
           'at': {
               'atMobiles': [
                   '1825718XXXX'
               ],
               'isAtAll': true
           }
        }"
}


# 忽略注释空行显示文本
# 增加 空格#类型的过滤
fcat(){
    sudo /bin/cat $1 | grep -v "^#" | grep -v "^$" |  grep -v "^[ ]*[#]"
}


# time_wait(){
# for i in $(seq 5 -1 1); do echo "$i 秒后执行"|lcat; sleep 1; done
# }

# jupyter_gen_config(){
# if [ `echo n | jupyter notebook --generate-config 2>&1 | grep Overwrite | wc -l` -eq 1 ] ; then 
# echo "`date +'%Y-%m-%d %H:%M:%S'` jupyter notebook 配置文件已存在"
# fcat ~/.jupyter/jupyter_notebook_config.py
# else
# echo "`date +'%Y-%m-%d %H:%M:%S'` jupyter notebook 配置文件 不存在需要生成"
# fcat ~/upload/ubuntu_init/config/jupyter.ini
# time_wait
# jupyter notebook --generate-config
# vim ~/.jupyter/jupyter_notebook_config.py
# echo "`date +'%Y-%m-%d %H:%M:%S'` jupyter notebook 配置文件 生成完成✅"
# fi # 判断结束，以fi结尾
# }

# jupyter_gen_config(){
# if [ `echo n | jupyter notebook --generate-config 2>&1 | grep Overwrite | wc -l` -eq 1 ] ; then 
# echo "`date +'%Y-%m-%d %H:%M:%S'` jupyter notebook 配置文件已存在"
# sudo /bin/cat ~/.jupyter/jupyter_notebook_config.py | grep -v "^#" | grep -v "^$" |  grep -v "^[ ]*[#]"
# else
# echo "`date +'%Y-%m-%d %H:%M:%S'` jupyter notebook 配置文件 不存在需要生成"
# sudo /bin/cat ~/upload/ubuntu_init/config/jupyter.ini | grep -v "^#" | grep -v "^$" |  grep -v "^[ ]*[#]"
# for i in $(seq 5 -1 1); do echo "$i 秒后执行"|lcat; sleep 1; done
# jupyter notebook --generate-config
# vim ~/.jupyter/jupyter_notebook_config.py
# echo "`date +'%Y-%m-%d %H:%M:%S'` jupyter notebook 配置文件 生成完成✅"
# fi # 判断结束，以fi结尾
# }
sudo mkdir -p /data/Ipynb && sudo chmod -R 777 /data/Ipynb
sudo mkdir -p /data/logs/jupyter && sudo chmod -R 777 /data/logs/jupyter
# mkdir -p ~/Logs && sudo chmod -R 777 /home/neo/Logs

if [ `ps -ef | grep jupyter | grep -v ansible | wc -l` -gt 3 ] ; then 
echo "`date +'%Y-%m-%d %H:%M:%S'` jupyter exist"
else
echo "`date +'%Y-%m-%d %H:%M:%S'` jupyter is not exist"
# jupyter_gen_config
[[ `uname -m` = "armv7l" && `uname` = "Linux" ]] && \
(nohup /usr/bin/python3 /usr/local/bin/jupyter-notebook --no-browser --notebook-dir=~/Resource/Ipynb  >  ~/Logs/jupyter.log 2>&1 &) || \
echo "`date +'%Y-%m-%d %H:%M:%S'` system is not arm"

[[ `uname -m` = "x86_64" && `uname` = "Linux" && `hostname` = "Archon" ]] && \
(nohup ~/anaconda3/bin/python ~/anaconda3/bin/jupyter-notebook  --no-browser --notebook-dir=/data/Ipynb  >  /data/logs/jupyter/jupyter.log 2>&1 &) || \
echo "`date +'%Y-%m-%d %H:%M:%S'` system is not Archon x86 Linux"

[[ `uname -m` = "x86_64" && `uname` = "Linux" && `hostname` != "z8750" ]] && \
(nohup ~/anaconda3/bin/python ~/anaconda3/bin/jupyter-notebook  --no-browser --notebook-dir=~/Resource/Ipynb  >  ~/Logs/jupyter.log 2>&1 &) || \
echo "`date +'%Y-%m-%d %H:%M:%S'` system is not z8750 x86 Linux"

[[ `uname -m` = "x86_64" && `uname` = "Linux" && `hostname` = "z8750" ]] && \
(nohup ~/anaconda3/bin/python ~/anaconda3/bin/jupyter-notebook  --no-browser --notebook-dir=/data/Ipynb  >  ~/Logs/jupyter.log 2>&1 &) || \
echo "`date +'%Y-%m-%d %H:%M:%S'` system is not x86 Linux"

[[ `uname -m` = "x86_64" && `uname` = "Darwin" ]] && \
(nohup ~/anaconda3/bin/python ~/anaconda3/bin/jupyter-notebook  --no-browser --notebook-dir=~/Resource/Ipynb  >  ~/Logs/jupyter.log 2>&1 &) || \
echo "`date +'%Y-%m-%d %H:%M:%S'` system is not mac"

echo "`date +'%Y-%m-%d %H:%M:%S'` jupyter restart success"
sddmd "jupyter status" "restart success"
fi # 判断结束，以fi结尾





# if [ `ps -ef | grep jupyter | grep -v color | wc -l` -eq 0 ] ; then 
# jupyter_gen_config
# (nohup jupyter notebook --no-browser --notebook-dir="~/Resource/Ipynb"  > ~/.jupyter/jupyter.log 2>&1 &) 
# echo "`date +'%Y-%m-%d %H:%M:%S'` jupyter restart success"
# sddmd "jupyter status" "restart success"
# else
# echo "`date +'%Y-%m-%d %H:%M:%S'` jupyter exist"
# fi # 判断结束，以fi结尾

# # 判断是否大于等于1，开启内核还会增加jupyter数量
# [[ `ps -ef | grep jupyter | grep -v color | wc -l` -eq 0 ]] && \
# (jupyter_gen_config && \
# echo "`date +'%Y-%m-%d %H:%M:%S'` jupyter restart success" &&\
# sddmd "jupyter status" "restart success" && \
# nohup jupyter notebook --no-browser --notebook-dir="~/Resource/Ipynb"  > ~/.jupyter/jupyter.log 2>&1 & ) || \
# echo "`date +'%Y-%m-%d %H:%M:%S'` jupyter exist"

