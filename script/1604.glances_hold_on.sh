#!/bin/zsh -l

export TERM=linux
export TERMINFO=/etc/terminfo

sddmd(){
  curl --silent \
       --request POST \
       --url "https://oapi.dingtalk.com/robot/send?access_token=813a36738938895e634ee516186fc0f8baec01d7792da5030dcc2a904bdc501b" \
       --header "content-type: application/json" \
       --data "{
            'msgtype': 'markdown',
            'markdown': {'title':'预警',
       'text':'# $1 \n> `date +"%Y-%m-%d %H:%M:%S"`  \n### `hostname`\n- $2'
            },
           'at': {
               'atMobiles': [
                   '1825718XXXX'
               ],
               'isAtAll': true
           }
        }"
}



if [ `ps -ef | grep glances | grep -v root | grep -v ansible | wc -l` -gt 3 ] ; then 
echo "`date +'%Y-%m-%d %H:%M:%S'` glances exist"
else
echo "`date +'%Y-%m-%d %H:%M:%S'` glances is not exist"
mkdir -p ~/.glances;
[[ `uname -m` = "armv7l" && `uname` = "Linux" ]] && \
(nohup ~/upload/gotty \
--credential=`hostname`:bilibili \
--reconnect \
--title-format=nsys-`hostname` \
--address=0.0.0.0 \
--port=48080 \
--ws-origin="https://*" \
/usr/bin/python3 /usr/bin/glances  >  ~/Logs/glances.log 2>&1 &) || \
echo "`date +'%Y-%m-%d %H:%M:%S'` system is not arm"

[[ `uname -m` = "x86_64" && `uname` = "Linux" ]] && \
(nohup ~/upload/gotty \
--credential=`hostname`:bilibili \
--reconnect \
--title-format=nsys-`hostname` \
--address=0.0.0.0 \
--port=48080 \
--ws-origin="https://*" \
~/anaconda3/bin/python ~/anaconda3/bin/glances >  ~/Logs/glances.log 2>&1 &) || \
echo "`date +'%Y-%m-%d %H:%M:%S'` system is not x86 Linux"

[[ `uname -m` = "x86_64" && `uname` = "Darwin" && `hostname` = "Smart.local" ]] && \
(nohup ~/upload/gotty \
--credential=`hostname`:bilibili \
--reconnect \
--title-format=nsys-`hostname` \
--address=0.0.0.0 \
--port=48080 \
--ws-origin="https://*" \
/usr/local/anaconda3/bin/python /usr/local/anaconda3/bin/glances  >  ~/Logs/glances.log 2>&1 &) || \
echo "`date +'%Y-%m-%d %H:%M:%S'` system is not mac"

echo "`date +'%Y-%m-%d %H:%M:%S'` nsys restart success"
sddmd "nsys status" "restart success"
fi # 判断结束，以fi结尾





# if [ `ps -ef | grep jupyter | grep -v color | wc -l` -eq 0 ] ; then 
# jupyter_gen_config
# (nohup jupyter notebook --no-browser --notebook-dir="~/Resource/Ipynb"  > ~/.jupyter/jupyter.log 2>&1 &) 
# echo "`date +'%Y-%m-%d %H:%M:%S'` jupyter restart success"
# sddmd "jupyter status" "restart success"
# else
# echo "`date +'%Y-%m-%d %H:%M:%S'` jupyter exist"
# fi # 判断结束，以fi结尾

# # 判断是否大于等于1，开启内核还会增加jupyter数量
# [[ `ps -ef | grep jupyter | grep -v color | wc -l` -eq 0 ]] && \
# (jupyter_gen_config && \
# echo "`date +'%Y-%m-%d %H:%M:%S'` jupyter restart success" &&\
# sddmd "jupyter status" "restart success" && \
# nohup jupyter notebook --no-browser --notebook-dir="~/Resource/Ipynb"  > ~/.jupyter/jupyter.log 2>&1 & ) || \
# echo "`date +'%Y-%m-%d %H:%M:%S'` jupyter exist"

