echo "####################################################"
echo "################ 08.pip 第三方包 ####################"
echo "###################################################"
# 改国内源，anaconda已经闭源了，只能用官方源
conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/free/
conda config --set show_channel_urls yes

sudo chmod -R 777 /home/neo/.cache

echo "消除IDE避免依赖问题"
echo y | pip uninstall spyder

echo "$(date +'%Y-%m-%d %H:%M:%S') pip 安装第三方模块!"
pip install --upgrade pip
pip install PyHamcrest # 自动化测试用的，匹配测试？ 极可能是 colorconsole 依赖
# 彩色输出 colorconsole
pip install colorconsole
# 简易彩色输出 colorprint 超赞！
pip install git+https://github.com/neosun100/colorprint

# 快速命令行查询工具 cheat
pip install cheat
cheat grep

# 警告相关去除
pip install --user --upgrade keyrings.alt
pip install Keyring
pip install Keyring -U
# pip install spyder;

# python版网络测速 speedtest-cli
pip install speedtest-cli
speedtest-cli --list | grep China
speedtest-cli --server=3633 --share
# pip install speedtest-cli && speedtest-cli --list | grep China && speedtest-cli --server=3633 --share

# 安装最新版 jupyter，避免mycli 冲突
# pip install -U ipython;
# pip install -U jupyter-console;
# mycli单独一个环境保险

# 安装机器学习
pip install deap update_checker tqdm stopit xgboost dask dask-ml scikit-mdr skrebate tpot

pip install vaex
# vaex是一个用处理、展示数据的数据表工具，类似pandas；
# vaex采取内存映射、惰性计算，不占用内存，适合处理大数据；
# vaex可以在百亿级数据集上进行秒级的统计分析和可视化展示；

# python版的多台服务器命令执行工具 pssh
pip install pssh

pip install arrow
pip install toolkity
pip install ansible
pip install sanic
pip install pendulum

# python系 专业的FTP服务器
pip install pyftpdlib

# httpie
pip install httpie

# mysql客户端 mycli
pip install mycli

#######################
#### fake 合集 ########
######################

# 测试数据生成器
pip install faker

# 数据直接入DB
pip install fake2db

# 测试数据直接入rdb，es，kafka，hbase
pip install datafaker
# 这是最好的 faker
echo "https://github.com/gangly/datafaker/blob/master/doc/zh_CN/%E6%B3%A8%E6%84%8F%E4%BA%8B%E9%A1%B9.md"

# 数据生成器 支持DB，df,excel 无命令行，与dataframe高度集成
pip install pydbgen

# 用户agent pass faker里也有
pip install fake-useragent

# 集成 faker
pip install factory_boy

# python输出代码高亮模块
pip install prettyprinter
echo "from prettyprinter import cpprint,set_default_style"
sleep 10

# xml 2 json
pip install xmljson

# 图像化的ping
pip install pinggraph
# gping host

# 另一个代码高亮备用
#pip install pygmentize; 没有这个包
pip install pygments -U
# alias pcat='pygmentize -g'
#### shell 录制
pip install asciinema

### shell 录制成 svg 文件
pip install termtosvg pyte python-xlib svgwrite

# 摘要抽取 sumy
pip install jieba
pip install git+git://github.com/miso-belica/sumy.git

# 远好于jieba的库
pip install pkuseg

# 安装 webp-converter
pip install webp-converter
webpc -h
2to3-3.7 -w -n ~/anaconda3/lib/python3.7/site-packages/webpc
webpc -h

#视频下载器 lulu 超越 you-get
#一个简单干净的视频/音乐/图像下载器
# pip install lulu

# youtube-dl
pip install youtube-dl -U

# 安装 PyGuetzli
# 谷歌图片极限压缩
pip install pyguetzli

# 命令行生成包
pip install fire

# 一个更简洁的操控浏览器（pk selenium） splinter 英 /'splɪntə/ 美 /'splɪntɚ/
pip install splinter

# 二维码生成 推荐模块 myqr
# 至尊款
# https://github.com/sylnsfar/qrcode/blob/master/README-cn.md
# 安装
pip install myqr

# json 输出更ping
pip install pingparsing==0.14

# nmap
pip install python-nmap

# 二维码识别模块
# pip install zbar;

# debug 模块
pip install pysnooper
pip install torchsnooper # pytorch debug
# 特有压缩模式，服务于 joblib
# mac 需要安装
# brew install lz4
# linux 下
# echo y | sudo apt-get install liblz4-tool;
pip install lz4

# 保存文件
pip install joblib

# 安装 mongo
pip install pymongo

# torch 基于深度学习的贝叶斯推断
pip install pyro-ppl

# 贝叶斯推断
pip install pymc3

# 自定义PyMC3模型构建于scikit-learn API之上
pip install pymc3_models

# 安装ta-lib
echo "$(date +'%Y-%m-%d %H:%M:%S') 安装 talib!"
cd ~/upload
wget --no-check-certificate https://data-1251390522.cos.ap-shanghai.myqcloud.com/Sys/ta-lib-0.4.0-src.tar.gz
tar -zxvf ta-lib-0.4.0-src.tar.gz >/dev/null 2>&1 # 解压
cd ta-lib                                         # 进入目录
./configure --prefix=/usr >/dev/null 2>&1
sudo make >/dev/null 2>&1
sudo make install >/dev/null 2>&1
pip install TA-Lib
echo "$(date +'%Y-%m-%d %H:%M:%S') 安装 talib 完成!"

# pip install pyopenssl==17.5.0;
pip install sanic-openapi
# echo "`date +'%Y-%m-%d %H:%M:%S'` 安装 sanic的微服务基础架构!";
# echo "github : https://github.com/songcser/sanic-ms"
# pip install git+https://github.com/songcser/sanic-ms;
# echo "`date +'%Y-%m-%d %H:%M:%S'` 安装 sanic的微服务基础架构完成";

# 安装kube-shell环境
# echo "`date +'%Y-%m-%d %H:%M:%S'` 创建虚拟环境安装 kube-shell ";
# if [ ! -d ~/anaconda3/envs/kubeshell ];then
# echo "`date +'%Y-%m-%d %H:%M:%S'` 不存在 kubeshell, 安装部署虚拟环境 "
# #conda update  conda;
# conda update -n base -c defaults conda;
# conda create -n kubeshell python=3.6;
# ~/anaconda3/envs/kubeshell/bin/pip install --upgrade pip;
# ~/anaconda3/envs/kubeshell/bin/pip install kube-shell;
# else echo "`date +'%Y-%m-%d %H:%M:%S'` kubeshell 虚拟环境已存在，请使用 kube-shell启动命令";
# fi;

pip install webssh
# wssh --address='0.0.0.0' --port=8000

# s-tui：在 Linux 中监控 CPU 温度、频率、功率和使用率的终端工具
sudo apt-get install stress
pip install s-tui

# https://www.akshare.xyz/zh_CN/latest/
# 金融数据接口
pip install akshare -U

# jq yq xq 牛逼的解析工具
pip install yq -U
# xq -y -x .qq

# pyautogui 这个库是一个模拟鼠标点击的好库
pip install PyAutoGUI

pip install -U dingtalk-sdk # 钉钉sdk 不是消息哦

pip install dash # web 可视化
pip install dash-auth
pip install streamlit # 机器学习 web 可视化

pip install rich # 高亮 表格 进度条 库

pip install langdetect # 谷歌语言检测库

# 编码检测
# Package	Accuracy	Mean per file (ns)	File per sec (est)
# chardet	93.5 %	126 081 168 ns	7.931 file/sec
# cchardet	97.0 %	1 668 145 ns	599.468 file/sec
# charset-normalizer	97.25 %	209 503 253 ns	4.773 file/sec
pip install charset_normalizer

pip install numerizer # numerizer[5]是一款将自然语言中数字转化成int或者float型数字的Python小工具。

pip install yfinance --upgrade --no-cache-dir
# yfinance[3]是一款Yahoo！金融数据下载工具。

# 数据对于从事数据分析、挖掘方向的同学一直都是一个巨大挑战。算法、编码都是非常成熟的。但是，如果没有数据，其余的都无从谈起。

# 金融，作为数据应用的一个典型的应用场景，受到很多同学的青睐，但是，从哪里获取到金融数据，却成了一个令人困扰的问题。

# yfinance提供一种可靠、线程化、Python化的方式，使得能够轻松从雅虎下载金融市场数据。

# bamboolib[5]是使得pandas DataFrames数据分析变得更加容易的一款Python库。

# 做数据相关工作的同学，对pandas肯定不会陌生。它很强大，甚至对于很多Python开发者具备着不可替代的位置，但是对于初学者却有时候让人难以理解。

# bamboolib使得pandas DataFrames数据分析变得更加简单容易，在以往需要上百行完成的工作，在bamboolib中只需要简短的一行即可。

# 通过bamboolib的使用，它可以提升你的工作效率，减少在无价值的事情上浪费过多精力。
pip install bamboolib
# Jupyter Notebook extensions
python3 -m bamboolib install_nbextensions
# JupyterLab extensions
python3 -m bamboolib install_labextensions

# 安装v百度 机器学习模块 paddlehub
pip install --upgrade paddlehub virtualenv visualdl tables astroid spyder rich jupyter-client qdarkstyle spyder-kernels pylint paddlehub qdarkstyle rich
pip install paddlepaddle -i https://mirror.baidu.com/pypi/simple

pip install emot --upgrade # emot可以用于从文本(字符串)中提取emojis和emoticon等，所有的表情符号。

pip install missingno # missingno是一款Python缺失数据的可视化工具。

pip install dabl # dabl是一款数据分析基准库。
# 这个项目试图使监督机器学习对于初学者变的更容易，并减少见任务的复杂度。

pip install autoviz # Automatically Visualize any dataset, any size with a single line of code.

pip install mitmproxy # 代理抓取

pip install natsort #  自然排序数字 解决 1 10 2 12 排序问题

pip install moviepy # 视频处理模块 ffmpeg

pip install invoke # 命令行构造神器

pip install torch torchvision -U
pip install pytorch-lightning -U
pip install fastHan # fastHan是基于fastNLP与pytorch实现的中文自然语言处理工具，像spacy一样调用方便。
# 其内核为基于BERT的联合模型，其在13个语料库中进行训练，可处理中文分词、词性标注、依存句法分析、命名实体识别四项任务。fastHan共有base与large两个版本，分别利用BERT的前四层与前八层。base版本在总参数量150MB的情况下各项任务均有不错表现，large版本则接近甚至超越SOTA模型。
pip install fastNLP -U

pip install 'pyhive[presto]'
# presto

pip install lac
# 基于百度开源项目LAC实现文本分词、词性标注和命名实体识别

# spark系
pip install pyspark           # python spark
pip install koalas            # spark pandas
pip install tensorflowonspark # spark on tensorflow
pip install sparktorch        # pytorch on spark
pip install joblibspark       # scikit-learn on spark
pip install spark_sklearn
pip install scikit-learn -U
pip install findspark

pip install cutecharts # 可爱的可视化绘图
# # 1. imoport this on the top.
# from cutecharts.globals import use_jupyter_lab; use_jupyter_lab()

# # 2. call the `load_javascript` function when you renders chart first time.
# chart.load_javascript()

pip install viztracer # 命令行 viztracer 工具 后接脚本 测试 程序速度分布支持win和多进程 比较新的包

pip install scikit-opt # 一个易用、易部署的Python 应该叫“进化算法”或者“智能优化算法 基于 scipy

pip install alive-progress # 最佳进度条

pip install dearpygui # 最佳GUI

pip install pyaudio # 音频相关

pip install PyExecJS # python 执行 JS

pip install fuzzywuzzy
# 这个库的名字听起来很奇怪，但是在字符串匹配方面，
# fuzzywuzzy 是一个非常有用的库。可以很方便地实现计算字符串匹配度、令牌匹配度等操作，也可以很方便地匹配保存在不同数据库中的记录。
echo "PyCaret是一个开源的、低代码率的Python机器学习库，它可以使机器学习工作流程自动化，同时，它也是一个端到端的机器学习和模型管理工具，可以缩短机器学习实验周期，将生产力提高10倍。"
echo "https://zhuanlan.zhihu.com/p/234702046?utm_source=com.alibaba.android.rimet&utm_medium=social&utm_oi=52708249698304"
echo y | conda create --name pycaret python=3.6 &&
    source activate pycaret &&
    pip install --upgrade pycaret &&
    conda deactivate

echo "安装clickhouse client 高亮客户端 chcli"
echo y | conda create --name chcli python=3.8 &&
    source activate chcli &&
    pip install git+https://github.com/long2ice/chcli &&
    pip install antlr4-python3-runtime==4.8 &&
    conda deactivate
~/anaconda3/envs/chcli/bin/chcli --help

pip install kinesis # kinesis 客户端

pip install pulsar-client # pulsar 客户端

pip install kazoo # kafka zookeeper?

echo "pdf合并分割"

pip install PyMuPDF
echo 'python -m fitz join -o "/Users/jiasunm/Downloads/Jason20Brownlee20-20XGBoost20with20Python.201.10_cn.pdf"  "/Users/jiasunm/Downloads/Jason20Brownlee20-20XGBoost20with20Python.201.10[001-058].en.zh-CN.pdf" "/Users/jiasunm/Downloads/Jason20Brownlee20-20XGBoost20with20Python.201.10[059-115].en.zh-CN.pdf"'

pip install esrally
echo "ES benchmark"

pip install minio
echo "minio client"

echo "安装 impala-shell "
echo y | conda create --name impala python=3.8 &&
    source activate impala &&
    pip install impala-shell &&
    conda deactivate
~/anaconda3/envs/impala/bin/impala-shell --help

echo "安装 catboost 机器学习框架"
pip install catboost
pip install ipywidgets
jupyter nbextension enable --py widgetsnbextension

echo "the best python repl ptpython"
pip install ptpython -U

echo "配置config"

if [ $(uname) = "Darwin" ]; then
    mkdir -p "/Users/$(whoami)/Library/Application Support/ptpython"
    cp -f ~/upload/ubuntu_init/pyScript/ptpython_conf.py "/Users/$(whoami)/Library/Application Support/ptpython/config.py"
elif [ $(uname) = "Linux" ]; then
    mkdir -p ~/.config/ptpython
    cp -f ~/upload/ubuntu_init/pyScript/ptpython_conf.py ~/.config/ptpython/config.py
elif [ $(uname) = "WindowsNT" ]; then
    echo "Windows pass"
else
    echo "$(uname) 系统未知！"
fi

# pyspark driver 高亮
export PYSPARK_DRIVER_PYTHON=ptpython

echo "shell下最专业的markdown view"
pip install mdv

echo "py hive presto trino"
echo "https://github.com/dropbox/PyHive"
pip install pyhive
pip install 'pyhive[hive]' -U
pip install 'pyhive[presto]' -U
pip install 'pyhive[trino]' -U

echo "clickhouse-cli 高亮"
echo "https://github.com/hatarist/clickhouse-cli"
pip install clickhouse-cli -U

echo "文本相似度的机器学习模块"

pip install text2vec
echo "https://github.com/shibing624/text2vec"

pip install sentence-transformers
echo "text2vec from sentence-transformers"
echo "https://github.com/UKPLab/sentence-transformers"
echo "https://mp.weixin.qq.com/s/avBvs7chURKJSn25OQiqbg"

pip install altair -U
echo "股票可视化模块"

pip install nanoid
echo "更好的UUID"

pip install 'papermill[all]'
echo "jupyter 执行器"
echo "造纸厂"

pip install 'luigi[toml]'
echo "Luigi是一个Python模块，可帮助您构建复杂的批处理作业管道。它处理依赖关系解析、工作流管理、可视化等。它还带有内置的Hadoop支持。"

pip install jupyter-vvp
echo "Jupyter的这一扩展允许您直接从Jupyter笔记本编写和执行Flink SQL语句。它得到了Ververica平台的SQL REST API的支持。"
echo "https://github.com/ververica/jupyter-vvp"

pip install git+https://github.com/scanny/python-pptx
echo "https://aws.amazon.com/cn/blogs/china/translating-presentation-files-with-amazon-translate/"
echo "PPT 翻译使用指南"
echo "https://python-pptx.readthedocs.io/en/latest/user/quickstart.html"
echo "https://python-pptx.readthedocs.io/en/latest/"




pip install pipx  -U 
echo "独立环境的pip，避免包冲突"
pipx install ansible


pipx install athena
pipx install athenacli
pipx install aws-shell
echo "AWS系列cli"


pipx install bypy
echo "baidupan 命令行"

pipx install ciphey
pipx install pycowsay
pipx install locust


pipx install clickhouse-cli
pipx install databricks-sql-cli #  dbsqlcli
pipx install litecli
pipx install mssql-cli
pipx install mycli
pipx install pgcli
pipx install odbc-cli

pipx install nuitka3
echo  "executor打包"


pipx install poetry
echo "Dependency Management for Python"
echo 'https://python-poetry.org/'
pipx install pdm
echo "包管理"





echo "$(date +'%Y-%m-%d %H:%M:%S') pip 安装第三方模块完成!"


