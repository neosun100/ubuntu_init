echo "####################################################"
echo "################ 06.二进制安装 ####################"
echo "###################################################"



echo "`date +'%Y-%m-%d %H:%M:%S'` 下载安装 dive docker image 分析器!";
cd ~/upload;
wget --no-check-certificate https://data-1251390522.cos.ap-shanghai.myqcloud.com/Sys/dive_0.4.0_linux_amd64.deb;
sudo chmod 777 dive_*_linux_amd64.deb;
sudo apt install ./dive_*_linux_amd64.deb;
sudo docker images;
echo "`date +'%Y-%m-%d %H:%M:%S'` 下载安装 dive docker image 分析器 安装完毕!";


# Webp转换与扫描 webp-converter
echo "`date +'%Y-%m-%d %H:%M:%S'` 下载安装 Webp转换与扫描 webp-converter!";
cd ~/upload;
wget --no-check-certificate https://data-1251390522.cos.ap-shanghai.myqcloud.com/Sys/libwebp-1.0.0-linux-x86-64.tar.gz;
tar -zxvf libwebp-*-linux-x86-64.tar.gz >/dev/null 2>&1;
chmod 777 libwebp-*-linux-x86-64;
moveDir=/usr/local/app;
mv -f ~/upload/libwebp-*  $moveDir/ >/dev/null 2>&1 # $moveDir/ 加/ 否则易移动的是子目录，就全乱了
rm -rf /usr/local/app/libwebp-1.0.0-linux-x86-64.tar.gz
ln -sf $moveDir/libwebp* $LOCAL_HOME/arrow/libwebp >/dev/null 2>&1
echo "`date +'%Y-%m-%d %H:%M:%S'` 下载安装 Webp转换与扫描 webp-converter 完成!";


cd ~/upload;
echo "`date +'%Y-%m-%d %H:%M:%S'` 下载安装 谷歌浏览器 & WebDriver!";
#axel -n 18 -a  https://data-1251390522.cos.ap-shanghai.myqcloud.com/Sys/google-chrome-stable_current_amd64.deb
wget --no-check-certificate https://data-1251390522.cos.ap-shanghai.myqcloud.com/Sys/google-chrome-stable_current_amd64.deb;
echo y | sudo dpkg -i google-chrome-stable_current_amd64.deb
sudo apt-get update # 更新-
echo y | sudo apt-get -f install # 解决依赖关系
echo y | sudo dpkg -i google-chrome-stable_current_amd64.deb



cd ~/upload;
# https://sites.google.com/a/chromium.org/chromedriver/downloads
# axel -n 18 -a https://data-1251390522.cos.ap-shanghai.myqcloud.com/Sys/chromedriver_linux64.zip;
wget --no-check-certificate https://data-1251390522.cos.ap-shanghai.myqcloud.com/Sys/chromedriver_linux64.zip;
chmod 777 chromedriver_linux64.zip;
unzip chromedriver_linux64.zip >/dev/null 2>&1;
chmod 777 chromedriver;
sudo cp ~/upload/chromedriver /usr/local/bin/chromedriver;

echo "`date +'%Y-%m-%d %H:%M:%S'` 下载安装 谷歌浏览器 & WebDriver 完成!";


echo "`date +'%Y-%m-%d %H:%M:%S'` 移动安装 谷歌图片压缩程序 guetzli!";
# mv ~/upload/ubuntu_init/file/guetzli.7z   ~/upload/guetzli.7z;
cd ~/upload;
wget --no-check-certificate https://data-1251390522.cos.ap-shanghai.myqcloud.com/Sys/guetzli.7z;
7za X ~/upload/guetzli.7z >/dev/null 2>&1;
chmod 777 ~/upload/guetzli;
sudo cp ~/upload/guetzli  /usr/local/bin/guetzli;

cd ~/upload;
wget --no-check-certificate https://data-1251390522.cos.ap-shanghai.myqcloud.com/Sys/libpng12.so.0;
chmod 777 ~/upload/libpng12.so.0;
sudo mv ~/upload/libpng12.so.0 /usr/lib/x86_64-linux-gnu/
guetzli --help;
echo "`date +'%Y-%m-%d %H:%M:%S'` 移动安装 谷歌图片压缩程序 guetzli 完成!";



echo "`date +'%Y-%m-%d %H:%M:%S'` 移动安装 qshell!";
cd ~/upload;
# mv ~/upload/ubuntu_init/file/qshell   ~/upload/qshell;
wget --no-check-certificate https://data-1251390522.cos.ap-shanghai.myqcloud.com/Sys/qshell;
chmod 777 ~/upload/qshell;
sudo cp ~/upload/qshell   $LOCAL_HOME/bin/qshell;
qshell -h;
echo "`date +'%Y-%m-%d %H:%M:%S'` 移动安装 qshell 完成!";



echo "`date +'%Y-%m-%d %H:%M:%S'` 移动安装 ccat !";
cd ~/upload;
wget --no-check-certificate https://data-1251390522.cos.ap-shanghai.myqcloud.com/Sys/ccat_linux-amd64-1.1.0.tar.gz;
tar -zxvf ccat_linux-amd64-1.1.0.tar.gz >/dev/null 2>&1;
cd linux-amd64-1.1.0/;
chmod 777 ccat;
sudo cp ccat /usr/bin/;

echo "`date +'%Y-%m-%d %H:%M:%S'` 移动安装 ccat 完成!";





echo "`date +'%Y-%m-%d %H:%M:%S'` 下载安装 邮件 mailx!";
cd ~/upload;
wget --no-check-certificate https://data-1251390522.cos.ap-shanghai.myqcloud.com/Sys/heirloom-mailx_12.5-4_amd64.deb;
sudo dpkg -i heirloom-mailx_12.5-4_amd64.deb;
echo y | sudo apt-get install sharutils;
echo "`date +'%Y-%m-%d %H:%M:%S'` 下载安装 邮件 mailx 完成!";


echo "`date +'%Y-%m-%d %H:%M:%S'` 下载安装  redis 测试工具" && \
cd ~/upload && \
wget --no-check-certificate https://data-1251390522.cos.ap-shanghai.myqcloud.com/Sys/redis-benchmark && \
sudo chmod 777 redis-benchmark && \
sudo cp redis-benchmark /usr/bin/ 

echo "`date +'%Y-%m-%d %H:%M:%S'` 下载安装 二进制文件全部完成!";
