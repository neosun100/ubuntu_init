
echo "####################################################";
echo "########  设置系统时间为local时间  ##############";
echo "####################################################";
# 安装ntpdate：
echo y | sudo apt-get install ntpdate;
# 设置校正服务器：
sudo ntpdate time.windows.com;
# 设置硬件时间为本地时间：
sudo hwclock --localtime --systohc;
# 然后创建时区软链
sudo ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime;
# 现在看下时间，是不是已经正常了呢。
date;
echo "`date +'%Y-%m-%d %H:%M:%S'` 设置本地时间完毕";