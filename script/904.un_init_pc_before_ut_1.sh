
# 免初始化 系统，也可UT的前提，在 root 执行，后会自动跳到 neo

echo "####################################" && \
echo "##### 01. 检查 neo账户是否存在 没有则创建 #####" && \
echo "####################################" && \
echo "`date +'%Y-%m-%d %H:%M:%S'` 检查 neo 账户是否存在" && \
if [ ! -d /home/neo ];then adduser neo && \
passwd neo && \
echo "`date +'%Y-%m-%d %H:%M:%S'` neo 账户已创建！"; else echo "`date +'%Y-%m-%d %H:%M:%S'` neo 账户 存在！"; fi && \
echo "####################################" && \
echo "##### 02. 检查 neo账户是否加入sudo组 没有则加入 #####" && \
echo "####################################" && \
cat /etc/sudoers | grep -v "^#" | grep -v "^$" && \
echo "`date +'%Y-%m-%d %H:%M:%S'` 若不存在neo sudo免密则添加" && \
sed -i '/^%sudo    ALL=(ALL:ALL) NOPASSWD: ALL/d' /etc/sudoers && \
sed -i '/^neo      ALL=(ALL:ALL) NOPASSWD: ALL/d' /etc/sudoers && \
echo "%sudo    ALL=(ALL:ALL) NOPASSWD: ALL" >> /etc/sudoers && \
echo "neo      ALL=(ALL:ALL) NOPASSWD: ALL" >> /etc/sudoers && \
echo "`date +'%Y-%m-%d %H:%M:%S'` 处理完成，查看 /etc/sudoers 文件" && \
cat /etc/sudoers | grep -v "^#" | grep -v "^$" && \
echo "`date +'%Y-%m-%d %H:%M:%S'` neo 账户 已加入sudo组！" && \
sudo su neo