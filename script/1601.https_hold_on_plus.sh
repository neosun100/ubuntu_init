# 该脚本部署在云端，每2个月执行一次 
# https://www.jianshu.com/p/8984bed9a784
# http://www.fly63.com/article/detial/2088
# https://www.cnblogs.com/datiangou/p/10173021.html
# https://ethanblog.com/tech/configure-and-anto-renew-let-s-encrypt-free-certificate-using-acme-sh-and-dnspod.html
# https://github.com/acmesh-official/acme.sh/wiki/dnsapi
# https://github.com/acmesh-official/acme.sh/wiki/%E8%AF%B4%E6%98%8E

acme_gen_key(){
echo "`date +'%Y-%m-%d %H:%M:%S'` 生成密钥对";
~/.acme.sh/acme.sh  \
--renew  \
-d ${1:-"*.neo.pub"} \
--server https://acme.freessl.cn/directory \
--yes-I-know-dns-manual-mode-enough-go-ahead-please \
--dns
echo "";
echo "";
echo "`date +'%Y-%m-%d %H:%M:%S'` 生成密钥对完成✅";
l ~/.acme.sh/;
l ~/.acme.sh/${1:-"*.neo.pub"};
}


acme_gen_key_neo(){
acme_gen_key "*.neo.pub";
acme_gen_key "*.frp.neo.pub";

acme_gen_key "*.gaga.ink";
acme_gen_key "*.i.gaga.ink";
}


acme_mv_key(){
echo "`date +'%Y-%m-%d %H:%M:%S'` 移动密钥对到nginx配置目录";
cp -rf ~/.acme.sh/*.neo.pub /data/nginx/ssl  
cp -rf ~/.acme.sh/*.frp.neo.pub /data/nginx/ssl  

cp -rf ~/.acme.sh/*.gaga.ink /data/nginx/ssl 
cp -rf ~/.acme.sh/*.i.gaga.ink /data/nginx/ssl 

# cp -rf /etc/letsencrypt/dhparam.pem /data/nginx/ssl
echo "`date +'%Y-%m-%d %H:%M:%S'` 覆盖移动密钥对到nginx配置目录完成✅";
}


acme_replace_neo(){
acme_gen_key_neo;
acme_mv_key;
echo "重启nginx容器";
sudo docker  restart -t 10 nginx ;
}


sddmd(){
  curl --silent \
       --request POST \
       --url "https://oapi.dingtalk.com/robot/send?access_token=813a36738938895e634ee516186fc0f8baec01d7792da5030dcc2a904bdc501b" \
       --header "content-type: application/json" \
       --data "{
            'msgtype': 'markdown',
            'markdown': {'title':'预警',
       'text':'# $1 \n> `date +"%Y-%m-%d %H:%M:%S"`  \n### `hostname`\n$2'
            },
           'at': {
               'atMobiles': [
                   '1825718XXXX'
               ],
               'isAtAll': true
           }
        }"
}




domain_https_deadline_TS(){
URL=${1:-www.neo.pub}
ENDTIME=`echo | openssl s_client -servername $URL -connect $URL:443 2>/dev/null | openssl x509 -noout -enddate | cut -c 10-`
ENDTS=`date +%s -d "$ENDTIME"`
echo $ENDTS
}

domain_https_deadline(){
URL=${1:-www.neo.pub}
ENDTIME=`echo | openssl s_client -servername $URL -connect $URL:443 2>/dev/null | openssl x509 -noout -enddate | cut -c 10-`
ENDTS=`date +%s -d "$ENDTIME"`
echo $ENDTIME
}


KEY_WORD="7 day"
STARTTS=`date --date="$KEY_WORD" +'%s'`
TIME_OUT_TS=`domain_https_deadline_TS`


if [ $TIME_OUT_TS -ge $STARTTS ] ; then 
echo "`date +'%Y-%m-%d %H:%M:%S'` https 过期时间为 `domain_https_deadline` 暂不需要更新"
else
echo "`date +'%Y-%m-%d %H:%M:%S'` https 过期时间为 `domain_https_deadline` 到达更新阙值，开始更新"
# ~/.acme.sh/acme.sh  --issue --dns dns_ali -d '*.gaga.ink' -d '*.i.gaga.ink' -d '*.neo.pub' -d '*.frp.neo.pub';
~/.acme.sh/acme.sh --issue --dns dns_ali -d '*.gaga.ink' --force
~/.acme.sh/acme.sh --issue --dns dns_ali -d '*.i.gaga.ink' --force
~/.acme.sh/acme.sh --issue --dns dns_ali -d '*.neo.pub' --force
~/.acme.sh/acme.sh --issue --dns dns_ali -d '*.frp.neo.pub' --force

# acme_replace_neo;
acme_mv_key;
echo "重启nginx容器";
sudo docker  restart -t 10 nginx ;
echo "`date +'%Y-%m-%d %H:%M:%S'` 更新程序完成✅"
echo  "`date +'%Y-%m-%d %H:%M:%S'` 当前过期时间为 `domain_https_deadline`"
sddmd "https更新完成" "当前https 过期时间为 `domain_https_deadline`"
fi # 判断结束，以fi结尾


