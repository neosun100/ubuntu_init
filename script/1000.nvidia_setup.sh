echo '🎃🎃🎃🎃🎃🎃🎃🎃🎃🎃🎃🎃🎃🎃🎃🎃🎃🎃🎃🎃🎃';
echo "#############   💳💳💳💳💳   ###############";
echo "#############   安装显卡驱动   ###############";
echo "#############   💳💳💳💳💳   ###############";
echo '🎃🎃🎃🎃🎃🎃🎃🎃🎃🎃🎃🎃🎃🎃🎃🎃🎃🎃🎃🎃🎃';
echo "";
echo "";
echo "#############   1. 检查是否添加了GPU源   ###############";
echo "`date +'%Y-%m-%d %H:%M:%S'` 添加Graphic Drivers PPA";

sudo add-apt-repository ppa:graphics-drivers/ppa;
sudo apt-get update;


sudo apt update
sudo apt install ubuntu-drivers-common

echo "#############   2. 查找并安装驱动   ###############";
echo "";
echo "";
echo "`date +'%Y-%m-%d %H:%M:%S'` 查看当前显卡型号与适配驱动";
ubuntu-drivers devices;
echo "`date +'%Y-%m-%d %H:%M:%S'` 安装推荐驱动";
echo y | sudo apt-get install `ubuntu-drivers devices | grep "recommended" | field 3` ;

echo "`date +'%Y-%m-%d %H:%M:%S'` 安装完成，检查安装的驱动信息";
nvidia-smi;
echo "`date +'%Y-%m-%d %H:%M:%S'` 查看当前所有GPU的型号";
nvidia-smi -L;
echo "";
echo "";
echo "#############   3. 安装CUDA   ###############";
echo "";
echo "";
echo "CUDA的下载地址\nhttps://developer.nvidia.com/cuda-90-download-archive?target_os=Linux&target_arch=x86_64&target_distro=Ubuntu&target_version=1704&target_type=runfilelocal"

echo "Docker 是在 Linux 上启用 TensorFlow GPU 支持的最简单方法，"
echo "因为只需在主机上安装 NVIDIA® GPU 驱动程序（无需安装 NVIDIA® CUDA® 工具包）。"

echo '🎃🎃🎃🎃🎃🎃🎃🎃🎃🎃🎃🎃🎃🎃🎃🎃🎃🎃🎃🎃🎃';
echo '安装教程参考 https://zhuanlan.zhihu.com/p/51373519';
echo '🎃🎃🎃🎃🎃🎃🎃🎃🎃🎃🎃🎃🎃🎃🎃🎃🎃🎃🎃🎃🎃';
echo '如果识别不了，请重启';

# 1  NVIDIA-SMI介绍
#         nvidia-smi简称NVSMI，提供监控GPU使用情况和更改GPU状态的功能，是一个跨平台工具，它支持所有标准的NVIDIA驱动程序支持的Linux发行版以及从WindowsServer 2008 R2开始的64位的系统。该工具是N卡驱动附带的，只要安装好驱动后就会有它。

#     Windows下程序位置：C:\Program Files\NVIDIACorporation\NVSMI\nvidia-smi.exe。Linux下程序位置：/usr/bin/nvidia-smi，由于所在位置已经加入PATH路径，可直接输入nvidia-smi运行。

# 2  NVIDIA-SMI命令系列详解
# 2.1  nvidia-smi
# 显示所有GPU的当前信息状态



# 显示的表格中：

# Fan：                     风扇转速（0%--100%），N/A表示没有风扇

# Temp：                 GPU温度（GPU温度过高会导致GPU频率下降）

# Perf：                    性能状态，从P0（最大性能）到P12（最小性能）

# Pwr：                     GPU功耗

# Persistence-M：   持续模式的状态（持续模式耗能大，但在新的GPU应用启动时花费时间更少）

# Bus-Id：               GPU总线，domain:bus:device.function

# Disp.A：                Display Active，表示GPU的显示是否初始化

# Memory-Usage：显存使用率

# Volatile GPU-Util：GPU使用率

# ECC：                   是否开启错误检查和纠正技术，0/DISABLED, 1/ENABLED

# Compute M.：     计算模式，0/DEFAULT,1/EXCLUSIVE_PROCESS,2/PROHIBITED

#  

# 附加选项：

# nvidia-smi –i xxx

# 指定某个GPU

# nvidia-smi –l xxx

# 动态刷新信息（默认5s刷新一次），按Ctrl+C停止，可指定刷新频率，以秒为单位

# nvidia-smi –f xxx

# 将查询的信息输出到具体的文件中，不在终端显示

# 2.2  nvidia-smi -q
# 查询所有GPU的当前详细信息



# 附加选项：

# nvidia-smi –q –u

# 显示单元而不是GPU的属性

# nvidia-smi –q –i xxx

# 指定具体的GPU或unit信息

# nvidia-smi –q –f xxx

# 将查询的信息输出到具体的文件中，不在终端显示

# nvidia-smi –q –x

# 将查询的信息以xml的形式输出

# nvidia-smi -q –d xxx

# 指定显示GPU卡某些信息，xxx参数可以为MEMORY, UTILIZATION, ECC, TEMPERATURE, POWER,CLOCK, COMPUTE, PIDS, PERFORMANCE, SUPPORTED_CLOCKS, PAGE_RETIREMENT,ACCOUNTING

# nvidia-smi –q –l xxx

# 动态刷新信息，按Ctrl+C停止，可指定刷新频率，以秒为单位

#  

#  

# nvidia-smi --query-gpu=gpu_name,gpu_bus_id,vbios_version--format=csv

# 选择性查询选项，可以指定显示的属性选项



# 可查看的属性有：timestamp，driver_version，pci.bus，pcie.link.width.current等。（可查看nvidia-smi--help-query–gpu来查看有哪些属性）

#  

# 2.3  设备修改选项
# 可以手动设置GPU卡设备的状态选项

# nvidia-smi –pm 0/1

# 设置持久模式：0/DISABLED,1/ENABLED

# nvidia-smi –e 0/1

# 切换ECC支持：0/DISABLED, 1/ENABLED

# nvidia-smi –p 0/1

# 重置ECC错误计数：0/VOLATILE, 1/AGGREGATE

# nvidia-smi –c

# 设置计算应用模式：0/DEFAULT,1/EXCLUSIVE_PROCESS,2/PROHIBITED

# nvidia-smi –r

# GPU复位

# nvidia-smi –vm

# 设置GPU虚拟化模式

# nvidia-smi –ac xxx,xxx

# 设置GPU运行的工作频率。e.g. nvidia-smi –ac2000,800

# nvidia-smi –rac

# 将时钟频率重置为默认值

# nvidia-smi –acp 0/1

# 切换-ac和-rac的权限要求，0/UNRESTRICTED, 1/RESTRICTED

# nvidia-smi –pl

# 指定最大电源管理限制（瓦特）

# nvidia-smi –am 0/1

# 启用或禁用计数模式，0/DISABLED,1/ENABLED

# nvidia-smi –caa

# 清除缓冲区中的所有已记录PID，0/DISABLED,1/ENABLED

#  

# 2.4  nvidia-smi dmon
# 设备监控命令，以滚动条形式显示GPU设备统计信息。

# GPU统计信息以一行的滚动格式显示，要监控的指标可以基于终端窗口的宽度进行调整。 监控最多4个GPU，如果没有指定任何GPU，则默认监控GPU0-GPU3（GPU索引从0开始）。



# 附加选项：

# nvidia-smi dmon –i xxx

# 用逗号分隔GPU索引，PCI总线ID或UUID

# nvidia-smi dmon –d xxx

# 指定刷新时间（默认为1秒）

# nvidia-smi dmon –c xxx

# 显示指定数目的统计信息并退出



# nvidia-smi dmon –s xxx

# 指定显示哪些监控指标（默认为puc），其中：

# p：电源使用情况和温度（pwr：功耗，temp：温度）

# u：GPU使用率（sm：流处理器，mem：显存，enc：编码资源，dec：解码资源）

# c：GPU处理器和GPU内存时钟频率（mclk：显存频率，pclk：处理器频率）

# v：电源和热力异常

# m：FB内存和Bar1内存

# e：ECC错误和PCIe重显错误个数

# t：PCIe读写带宽

# nvidia-smi dmon –o D/T

# 指定显示的时间格式D：YYYYMMDD，THH:MM:SS

# nvidia-smi dmon –f xxx

# 将查询的信息输出到具体的文件中，不在终端显示

#  

# 2.5   nvidia-smi pmon
# 进程监控命令，以滚动条形式显示GPU进程状态信息。

# GPU进程统计信息以一行的滚动格式显示，此工具列出了GPU所有进程的统计信息。要监控的指标可以基于终端窗口的宽度进行调整。 监控最多4个GPU，如果没有指定任何GPU，则默认监控GPU0-GPU3（GPU索引从0开始）。



# 附加选项：

# nvidia-smi pmon –i xxx

# 用逗号分隔GPU索引，PCI总线ID或UUID

# nvidia-smi pmon –d xxx

# 指定刷新时间（默认为1秒，最大为10秒）

# nvidia-smi pmon –c xxx

# 显示指定数目的统计信息并退出

# nvidia-smi pmon –s xxx

# 指定显示哪些监控指标（默认为u），其中：

# u：GPU使用率

# m：FB内存使用情况

# nvidia-smi pmon –o D/T

# 指定显示的时间格式D：YYYYMMDD，THH:MM:SS

# nvidia-smi pmon –f xxx

# 将查询的信息输出到具体的文件中，不在终端显示
# --------------------- 
# 作者：Michael丶Bear 
# 来源：CSDN 
# 原文：https://blog.csdn.net/handsome_bear/article/details/80903477 
# 版权声明：本文为博主原创文章，转载请附上博文链接！