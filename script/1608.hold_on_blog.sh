

# zsh *.sh

# hexo  部署 与最优化

#######################
# blog
# 宿主部署


# 进程id 与 端口号互查
# pid 查进程
# 一个参数为进程id
pid(){
ps -ef| head -n 1  && ps -ef | fgrepnum 2 "==" ${1:-2} 
}


# 进程名查进程id
pname(){
ps -ef| head -n 1  && ps -ef | grep ${1:-python} 
}

#  更好的pname
# 消除了包含 grep pname的问题
# one_pname
opname(){
ps -ef| head -n 1  && ps -ef | grep ${1:-python} | grep -v "grep" | lcat 
}

opname2id(){
sudo ps -ef | grep ${1:-hexo} | grep -v "grep" | sort -k 7 -r |  awk '{print $2}' | head -n 1
}

# 需要改成包含关系

# 端口 查 进程与pid
# 一个参数 端口
# 有个隐患，这里查的是全局可访问的端口
# to do 
# 需要改成 包含关系更好
port2pid(){
sudo netstat -plna | awk '{ gsub(/\//,"\t"); print $0 }' | head -n 2 && sudo  netstat -plna | awk '{ gsub(/\//,"\t"); print $0 }' | fgrepstr 4 "==" :::${1:-9000} 
}

pname2port(){
echo "解决包含关系的匹配 awk match 待解决"
}

# Pid查端口号
pid2port(){
sudo netstat -plna |awk '{ gsub(/\//,"\t"); print $0 }' |  head -n 2 && sudo  netstat -plna | awk '{ gsub(/\//,"\t"); print $0 }' | fgrepstr 7 "==" ${1:-2244} 
}

# pid kill
pidkill(){
sudo kill -9 $1
}

pnamekill(){
[[ `opname2id  ${1:-hexo} | wc -l` -eq 1 ]] && pidkill `opname2id  ${1:-hexo}` || echo "`date +'%Y-%m-%d %H:%M:%S'` ${1:-hexo} 进程不存在" 
}

git_pull_hexo_oneline(){
    cd ~/upload/neoBlog/source;
    git fetch --all && git reset --hard origin/master && git pull;
    cd ~/upload;
    echo "同步hexo md文件完毕"
}


hexo_run(){
git_pull_hexo_oneline;
echo "`date +'%Y-%m-%d %H:%M:%S'` hexo sys blog success" ;
pnamekill hexo;
echo "`date +'%Y-%m-%d %H:%M:%S'` hexo kill success" ;
cd ~/upload/neoBlog;
hexo clear && hexo g && hexo d;
echo "`date +'%Y-%m-%d %H:%M:%S'` hexo reload success" ;
nohup hexo server -i 0.0.0.0 -p 4000 > blog.log 2>&1 &;
cd ~/upload;
echo "`date +'%Y-%m-%d %H:%M:%S'` hexo start success" ;
}


# 只有一个参数
# 是否完全初始化博客，默认为0，不完全初始化，neoBlog重新生成
hexo_deploy(){
if [ ${1-:0} = "0" ] ; then 
hexo_theme_init && hexo_config_init 
else
hexo_init_blog && hexo_theme_init && hexo_config_init 
fi # 判断结束，以fi结尾
}






[[ `http https://blog.neo.pub | grep 502 | wc -l ` = 2 ]] && ( echo "`date +'%Y-%m-%d %H:%M:%S'` blog服务不通。需要重启blog服务" && hexo_run )