#!/bin/sh -e

# 钉钉消息
# sdd "标题" "内容"
sddmd(){
  curl --request POST \
       --url "https://oapi.dingtalk.com/robot/send?access_token=813a36738938895e634ee516186fc0f8baec01d7792da5030dcc2a904bdc501b" \
       --header "content-type: application/json" \
       --data "{
            'msgtype': 'markdown',
            'markdown': {'title':'预警',
       'text':'# $1 \n> `date +"%Y-%m-%d %H:%M:%S"`  \n### `hostname`\n- $2'
            },
           'at': {
               'atMobiles': [
                   '1825718XXXX'
               ],
               'isAtAll': true
           }
        }"
}





iip(){
# if [ -e /sbin/ifconfig ];then
#   #ifconfig | egrep "192.168|10.100|172.27|172.30" | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | sed -n "1,1p"
#   ifconfig | egrep "192.168|10.100|172.27|172.30" | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | head -n 1
# else
#   #ip addr | egrep "192.168|10.100|172.27|172.30" | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | sed -n "1,1p"
#   ip addr | egrep "192.168|10.100|172.27|172.30" | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | head -n 1
# fi


# [[ `uname` = "Darwin" ]] && \
# ifconfig | egrep "192.168|10.100|172.27|172.30" | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | head -n 1 || \
# ip addr |grep inet |grep -v inet6 | grep -v 127.0.0.1 | field 2 | line 1 1 | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}"

python -c "import socket;print([(s.connect(('8.8.8.8', 53)), s.getsockname()[0], s.close()) for s in [socket.socket(socket.AF_INET, socket.SOCK_DGRAM)]][0][1])"
}

# 删除容器
nrm(){
sudo docker rm -f $* 
}

docker_restart_rpi_mongo(){
echo "一个参数 默认rest 可选auth"
sudo rm -rf /data/mongo/db/mongod.lock && \
nrm mongo;
sudo docker run -d \
--name mongo \
--restart unless-stopped \
-v /data/mongo/db:/data/db \
-v /etc/localtime:/etc/localtime:ro \
-v /data/mongo/configdb:/data/configdb -v /data/logs/mongo:/var/log/mongodb  \
-p 27017:27017 \
-p 28017:28017 \
andresvidal/rpi3-mongodb3:latest \
mongod --${1:-rest} --bind_ip 0.0.0.0 --httpinterface --dbpath /data/db --logpath /var/log/mongodb/mongod.log --logappend
}


# 每整分钟检测，启动时间 < 180 秒 则发送钉钉
if [ `sudo /bin/cat /proc/uptime | awk '{print int($0)}'` -le 180 ] && [ `http 127.0.0.1:28017 | wc -l` = 0 ]; then
    sddmd "开机启动需要重启mongo" "`iip`"
    docker_restart_rpi_mongo;
else echo "开机大于3分钟"; fi
