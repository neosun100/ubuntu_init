# 在此步之前，手动安装完 anaconda
echo "####################################################"
echo "################ 07.测试 各安装软件 ####################"
echo "###################################################"


# echo "`date +'%Y-%m-%d %H:%M:%S'` source ~/.bash_profile,source ~/.zshrc";
# source ~/.bash_profile;
# source ~/.zshrc;
# echo "`date +'%Y-%m-%d %H:%M:%S'` 刷新配置文件完成";


echo "`date +'%Y-%m-%d %H:%M:%S'` 测试 各安装软件";
python -V;
npm -v;
mvn -v;
sbt version;
scala -version ;
java -version;
go version;
sudo docker version;
compose -h;
#pcinfo;
# 邮件测试
# echo "`date +'%Y-%m-%d %H:%M:%S'` 下载安装 邮件 mailx 测试!";
# echo "neo，这是来自尼奥未来的问候" | mailx -v \
#                                          -a ~/upload/ubuntu_init/pic/core.png \
#                                          -s "伟大的起航" \
#                                          yiqidangqian@foxmail.com;

echo "`date +'%Y-%m-%d %H:%M:%S'` 测试 各安装软件 完成";
