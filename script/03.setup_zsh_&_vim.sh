echo "########################################################"
echo "################ 03. pip vim nail.rc 安装 与配置#################"
echo "########################################################"

cd ~/upload;
# echo "`date +'%Y-%m-%d %H:%M:%S'` 安装 zsh!";
# echo y | sudo apt-get install zsh; # 安装 zsh
# echo $SHELL; # 查看默认shell
#
#
# if [ ! -d ~/.oh-my-zsh  ];then
#   sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)";
#
#   echo "`date +'%Y-%m-%d %H:%M:%S'` oh-my-zsh 已 安装！"
# else
#   echo "`date +'%Y-%m-%d %H:%M:%S'` oh-my-zsh 目录存在,不操作！"
# fi
#
#
# chsh -s $(which zsh);
#
# if [ ! -d /home/neo/.oh-my-zsh/custom/plugins/zsh-syntax-highlighting  ];then
#   git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting;
#
#   echo "`date +'%Y-%m-%d %H:%M:%S'` zsh-syntax-highlighting 已 clone！"
# else
#   echo "`date +'%Y-%m-%d %H:%M:%S'` /home/neo/.oh-my-zsh/custom/plugins/zsh-syntax-highlighting 目录存在,不操作！"
# fi
#
#
#
# echo "`date +'%Y-%m-%d %H:%M:%S'` zsh 安装完毕!";



echo "`date +'%Y-%m-%d %H:%M:%S'` 安装 vim!";
echo y | sudo apt-get install vim;
ls -lah /usr/share/vim/vim*/colors/; # 查看现有主题
rm -rf ~/upload/molokai-master; # 避免解压问题
cd ~/upload;
#wget -P ~/upload/ubuntu_init/file/ https://data-1251390522.cos.ap-shanghai.myqcloud.com/Sys/molokai.vim;
#unzip -o ~/upload/ubuntu_init/file/molokai-master.zip;
sudo rm -rf /usr/share/vim/vim*/colors/molokai.vim;
sudo cp   /home/neo/upload/ubuntu_init/profile/molokai.vim /usr/share/vim/vim*/colors/;
ls -lah /usr/share/vim/vim*/colors/ | grep molokai;
sudo rm -rf  ~/.vimrc;
cp  ~/upload/ubuntu_init/profile/.vimrc  ~/.vimrc;
sudo chmod 777 ~/.vimrc;
ls -lah | grep vimrc;
source ~/.vimrc;
echo "`date +'%Y-%m-%d %H:%M:%S'` vim 安装完毕!";


echo "`date +'%Y-%m-%d %H:%M:%S'` 提前配置 pip!";
cd ~/upload;

echo "`date +'%Y-%m-%d %H:%M:%S'` 检验pip目录是否创建";
if [ ! -d ~/.pip  ];then
  mkdir ~/.pip;
  chmod 777 ~/.pip;
  echo "`date +'%Y-%m-%d %H:%M:%S'` pip目录已创建并777！"
else
  echo "`date +'%Y-%m-%d %H:%M:%S'` pip目录存在！"
fi

sudo rm -rf ~/.pip/pip.conf;
cp ~/upload/ubuntu_init/profile/pip.conf ~/.pip/pip.conf;
echo "`date +'%Y-%m-%d %H:%M:%S'` 提前配置 pip 完成!";

# echo "`date +'%Y-%m-%d %H:%M:%S'` 提前配置 .bash_profile .zshrc !";
# cd ~/upload;
# rm -rf ~/.bash_profile;
# cp ~/upload/ubuntu_init/profile/.bash_profile ~/.bash_profile;
# rm -rf ~/.zshrc;
# cp ~/upload/ubuntu_init/profile/.zshrc ~/.zshrc;
# echo "`date +'%Y-%m-%d %H:%M:%S'` 提前配置 .bash_profile .zshrc 完成!";



echo "`date +'%Y-%m-%d %H:%M:%S'` 提前配置 nail.rc !";
cd ~/upload;
sudo rm -rf /etc/nail.rc;
sudo cp ~/upload/ubuntu_init/profile/nail.rc /etc/nail.rc;
sudo chmod 777 /etc/nail.rc;
echo "`date +'%Y-%m-%d %H:%M:%S'` 提前配置 nail.rc 完成!";
