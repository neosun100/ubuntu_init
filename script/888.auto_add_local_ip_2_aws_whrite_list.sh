alias grepip='grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}"'

alias webip='http "https://www.taobao.com/help/getip.php" | grepip'

alias nows='date +"%Y-%m-%d %H:%M:%S"'



field() {
  awk "{print \$${1:-1}}"
}

check_aws_white_list() {
    [[ `type jq | grep not | wc -l  | field 1` == 1 ]] && brew install jq || sleep 1
    cat ~/.aws/config
    echo "使用说明：该函数共两个参数，第一个参数为你的安全组id，第二参数默认值为你的local ip"
    SECURITY_GROUP_ID=${1:-sg-0708b2efbc03b7d46}
    IP=${2:-$(webip)}

    WHITE_IP_LIST=$(aws ec2 describe-security-groups --group-ids $SECURITY_GROUP_ID | jq '.SecurityGroups[].IpPermissions[] | select(.IpProtocol  == "-1") | .IpRanges[].CidrIp' | grepip)
    if [[ $WHITE_IP_LIST[@] =~ $IP ]]; then
        echo "$(date +'%Y-%m-%d %H:%M:%S') 🍀  白名单包含本地ip: $IP"
    else
        echo "$(date +'%Y-%m-%d %H:%M:%S') ❌  白名单不包含本地ip: $IP"
        TAG_STRING="$(nows) Script add entry"
        aws ec2 authorize-security-group-ingress --group-id $SECURITY_GROUP_ID --ip-permissions IpProtocol=-1,FromPort=-1,IpRanges="[{CidrIp=$(webip)/32,Description=$TAG_STRING}]" | jq
        echo "$(date +'%Y-%m-%d %H:%M:%S') ✅  白名单添加本地ip: $IP完成"
    fi

}

# 修改第一个参数为你的安全组id
check_aws_white_list sg-0708b2efbc03b7d46
