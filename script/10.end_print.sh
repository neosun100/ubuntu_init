echo "####################################################";
echo "########  如有疑问，请联系 Neo 微信 ↓   ##############";
echo "####################################################";
echo "`date +'%Y-%m-%d %H:%M:%S'` 下载安装 邮件 mailx 测试!";
echo "neo，这是来自尼奥未来的问候" | mailx -v \
                                         -a ~/upload/ubuntu_init/pic/core.png \
                                         -s "伟大的起航" \
                                         yiqidangqian@foxmail.com;
qrencode -o - -t ANSI "https://u.wechat.com/EK4YMwsrFvtGJDdI0dPNLrA";
# pcinfo;


sudo journalctl --vacuum-size=10M;
sudo journalctl --vacuum-time=1w;

# 查询 journalctl 消耗了多少磁盘空间
sudo journalctl --disk-usage;

# 删除旧有日志
# 如果大家打算对journal记录进行清理，则可使用两种不同方式（适用于systemd 218及更高版本）。

# 如果使用–vacuum-size选项，则可硬性指定日志的总体体积，意味着其会不断删除旧有记录直到所占容量符合要求：

# sudo journalctl --vacuum-size=1G
# 1
# 另一种方式则是使用–vacuum-time选项。任何早于这一时间点的条目都将被删除。

# 例如，去年之后的条目才能保留：

# sudo journalctl --vacuum-time=1years

# https://blog.csdn.net/zstack_org/article/details/56274966