
:: https://github.com/n-riesco/ijavascript


conda install nodejs &^
::最新版的nodejs后续步骤有问题，10.13
npm config set registry https://registry.npm.taobao.org  &^
npm config get registry &^

npm install -g cnpm --registry=https://registry.npm.taobao.org &^
::cnpm install express &^
cnpm install dayjs --save &^
cnpm install -g ijavascript &^
ijsinstall 

::安装 vue脚手架
::全局安装
cnpm install --global vue-cli &^

::创建一个基于 webpack 模版的新项目
vue init webpack neo-project  &^

:: 安装依赖，走你
cd neo-project   &^
cnpm install   &^
cnpm run dev