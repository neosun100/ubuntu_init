echo "####################################"
echo "####### 01.界面美化 ################"
echo "####################################"
apm install file-icons # 文件图标美化
apm install minimap # 小code地图
apm install minimap-git-diff # 小地图 git 差异标记
apm install simplified-chinese-menu # 汉化
apm install atom-clock # 右下角加时间
apm install activate-power-mode # 动感代码combo



echo "####################################"
echo "####### 02.操作优化 ################"
echo "####################################"
apm install autocomplete-paths # path自动补全
apm install autoclose-html # html 标签自动补全




echo "####################################"
echo "####### 03.功能扩展 ################"
echo "####################################"
apm install language-ini
apm install atom-beautify # code format
apm install atom-html-preview # html 预览
apm install python-autopep8 # python format
apm install python-tools # 效率
