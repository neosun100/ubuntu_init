echo "`date +'%Y-%m-%d %H:%M:%S'` 确保节点环境有必备软件";
sudo snap remove microk8s && \
echo y | sudo apt-get install ebtables && \
echo y | sudo apt-get install ethtool && \
echo y | sudo apt-get install socat  && \
echo y | sudo apt-get install ipset;
echo "`date +'%Y-%m-%d %H:%M:%S'` 查看 当前 /etc/ssh/sshd_config 文件";
cat /etc/ssh/sshd_config | grep -v "^#" | grep -v "^$";
echo "`date +'%Y-%m-%d %H:%M:%S'` 若不允许 root 登陆 则添加";
sed -i '/^LoginGraceTime 120/d' /etc/ssh/sshd_config;
sed -i '/^PermitRootLogin yes/d' /etc/ssh/sshd_config;
sed -i '/^StrictModes yes/d' /etc/ssh/sshd_config;
echo "LoginGraceTime 120" >> /etc/ssh/sshd_config;
echo "PermitRootLogin yes" >> /etc/ssh/sshd_config;
echo "StrictModes yes" >> /etc/ssh/sshd_config;
echo "`date +'%Y-%m-%d %H:%M:%S'` 处理完成，查看 /etc/ssh/sshd_config 文件";
cat /etc/ssh/sshd_config | grep -v "^#" | grep -v "^$";
echo "`date +'%Y-%m-%d %H:%M:%S'` 重启 sshd 服务 ";
/etc/init.d/ssh restart;
echo "`date +'%Y-%m-%d %H:%M:%S'` 修改 root 密码 ";
passwd root;
echo "`date +'%Y-%m-%d %H:%M:%S'`  禁用 swap";
swapoff -a;