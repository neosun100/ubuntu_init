# 安装nvidia源
# https://github.com/NVIDIA/nvidia-docker


# 查看gpu
lspci | grep -i nvidia;


curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | sudo apt-key add - 

distribution=$(. /etc/os-release;echo $ID$VERSION_ID)  

curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | \
  sudo tee /etc/apt/sources.list.d/nvidia-docker.list


sudo apt-get update 


sudo apt-get install -y nvidia-docker2


sudo tee /etc/docker/daemon.json <<EOF                                                    
{                                                                                                                        
    "runtimes": {                                                                             
        "nvidia": {                                                         
            "path": "/usr/bin/nvidia-container-runtime",                            
            "runtimeArgs": []                                                                                            
        }                                                                         
    }                                                                                    
}                                                                                             
EOF 

sudo pkill -SIGHUP dockerd


sudo docker run --runtime=nvidia --rm nvidia/cuda:9.0-base nvidia-smi







# 测试程序
sudo docker run --runtime=nvidia -it --rm tensorflow/tensorflow:1.12.0-gpu-py3 \
python -c "import tensorflow as tf; tf.enable_eager_execution(); print(tf.reduce_sum(tf.random_normal([100000, 100000])))"  


# 开启jupyter
sudo docker run --runtime=nvidia -it --rm tensorflow/tensorflow:latest-gpu-py3-jupyter --name gpu-jupyter


sudo docker run -it --rm -v $(realpath ~/notebooks):/tf/notebooks -p 8888:8888 tensorflow/tensorflow:latest-py3-jupyter


sudo docker run --runtime=nvidia \
                -it --rm -v $(realpath ~/upload/notebooks/):/tf/notebooks \
                -p 8888:8888 --name gpu-jupyter \
                tensorflow/tensorflow:latest-gpu-py3-jupyter 



sudo docker run --runtime=nvidia \
                -it --rm -v $(realpath /Ipynb/):/tf/notebooks \
                -p 8888:8888 --name gpu-jupyter \
                tensorflow/tensorflow:latest-gpu-py3-jupyter 



sudo docker run --runtime=nvidia \
                -it \
                --rm \
                -v $(realpath /Ipynb):/tf \
                -p 8888:8888 \
                --name gpu-jupyter \
                tensorflow/tensorflow:latest-gpu-py3-jupyter
                