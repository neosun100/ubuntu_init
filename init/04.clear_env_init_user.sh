echo "####################################";
echo "##### 01. 按需添加 user crontab #####";
echo "####################################";
echo "比如单位机，输入如下 crontab ";
echo "---------------------------------";
echo '01 20 * * 1-5 sh /home/neo/upload/ubuntu_init/script/901.ut_open.sh';
echo '10 9 * * 1-5  sh /home/neo/upload/ubuntu_init/script/902.ut_close.sh';
echo "---------------------------------";
sleep 10;
crontab -e;
cd ~;



echo "####################################";
echo "##### 02. 清除所有 init 目录环境 #####";
echo "####################################";
sudo rm -rf /home/neo/unzip/;
sudo rm -rf /usr/local/arrow/;
sudo rm -rf /usr/local/app/;
sudo rm -rf /home/neo/anaconda3;
sudo rm -rf /home/neo/berryconda3;
sudo rm -rf /home/neo/upload;



echo "####################################";
echo "##### 03. 重新创建 upload 并授权 777 #####";
echo "####################################";
mkdir ~/upload;
chmod 777 ~/upload;
cd ~/upload;



echo "####################################";
echo "##### 04. clone ubuntu_init #####";
echo "####################################";
wget --no-check-certificate https://gitlab.com/neosun100/ubuntu_init/-/archive/master/ubuntu_init-master.tar.gz;
tar -zxvf ubuntu_init-master.tar.gz >/dev/null 2>&1;
mv ~/upload/ubuntu_init-master ~/upload/ubuntu_init;
echo "set smtp-auth-password=Neosun100" >> /home/neo/upload/ubuntu_init/profile/nail.rc;
cd ~/upload;



echo "#######################################";
echo "##### 05. 替换 bash_profile zshrc #####";
echo "######################################";
rm -rf ~/.bash_profile;
cp ~/upload/ubuntu_init/profile/.bash_profile ~/.bash_profile;
rm -rf ~/.zshrc;
cp ~/upload/ubuntu_init/profile/.zshrc ~/.zshrc;
#source ~/.bash_profile;
#source ~/.zshrc;


echo "###############################################";
echo "##### 06. 安装 tmux 重启复原插件 ################";
echo "##############################################";
echo "`date +'%Y-%m-%d %H:%M:%S'` 检验 ~/.tmux 是否创建";
if [ ! -d ~/.tmux  ];then
  mkdir ~/.tmux;
  chmod 777 ~/.tmux;
  echo "`date +'%Y-%m-%d %H:%M:%S'` ~/.tmux 已创建并777！"
else
  echo "`date +'%Y-%m-%d %H:%M:%S'` ~/.tmux 目录存在！"
fi

cd ~/.tmux/;
# git clone https://github.com/tmux-plugins/tmux-resurrect.git;
wget http://file.neo.pub/linux/tmux-continuum.zip && \
wget http://file.neo.pub/linux/tmux-resurrect.zip && \
unzip tmux-continuum.zip && \
unzip tmux-resurrect.zip;

# git clone https://github.com/tmux-plugins/tmux-continuum.git;
echo "`date +'%Y-%m-%d %H:%M:%S'` tmux 重启复原插件 安装完成";








echo "###############################################";
echo "##### 07. 安装 tmux 防止 init 过程断线尴尬 #####";
echo "##############################################";
echo y | sudo apt-get install tmux;
cd ~/upload;
rm -rf ~/.tmux.conf;
cp ~/upload/ubuntu_init/profile/.tmux.conf ~/.tmux.conf;
tmux
