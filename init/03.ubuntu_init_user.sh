echo "###########################################";
echo "##### 07. 安装 oh-my-zsh 高亮和推断插件#####";
echo "##########################################";
echo y | sudo apt-get install unzip;
# git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting;
cd ~/upload && wget http://file.neo.pub/linux/zsh-syntax-highlighting.zip && unzip zsh-syntax-highlighting.zip && mv zsh-syntax-highlighting  ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting;
cd ~/upload && wget http://file.neo.pub/linux/zsh-autosuggestions.zip && unzip  zsh-autosuggestions.zip  && mv zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions;

# git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions;


echo "####################################";
echo "##### 08. 生效 zsh  #####";
echo "####################################";
echo $SHELL;
sudo chsh -s $(which zsh);



# echo "####################################";
# echo "##### 08. 安装 fish 和 oh-my-fish #####";
# echo "####################################";
# echo y | sudo apt-get install fish;
# sudo rm -rf /home/neo/.local/share/omf;
# curl -L http://file.neo.pub/linux/setup_fish.sh | fish
