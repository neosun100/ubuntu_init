echo "####################################";
echo "##### 01. 检查 ~/upload 没有则创建 #####";
echo "####################################";
echo "`date +'%Y-%m-%d %H:%M:%S'` 检验上传目录是否创建";
if [ ! -d ~/upload  ];then
mkdir ~/upload;
chmod 777 ~/upload;
echo "`date +'%Y-%m-%d %H:%M:%S'` 上传目录已创建并777！";
else echo "`date +'%Y-%m-%d %H:%M:%S'` 上传目录存在！";
fi;

sudo dpkg --configure -a;
echo "####################################";
echo "##### 02. 保证升级进程未启动 #####";
echo "####################################";
sudo kill -9 `ps -A | grep apt-get | awk '{print $1}'`;
sudo rm -rf  /var/cache/apt/archives/lock;
sudo rm -rf /var/lib/apt/lists/lock;
sudo rm -rf /var/lib/dpkg/lock;
# sudo rm /var/lib/dpkg/updates/*
sudo dpkg --configure -a;



echo "####################################";
echo "##### 03. 刷新源并升级          #####";
echo "####################################";
sudo apt-get update;
echo y | sudo apt-get upgrade;
echo y | sudo apt-get upgrade --fix-missing;




echo "####################################";
echo "##### 04. 安装并配置中文 #####";
echo "####################################";
if  [ `dpkg --print-architecture` == "amd64" ]; then
echo y | sudo apt-get install language-pack-zh-hant language-pack-zh-hans;
elif  [ `dpkg --print-architecture` == "armhf" ]; then
echo "`date +'%Y-%m-%d %H:%M:%S'` 树莓派不需安装中文包，直接配置即可";
else echo "CPU架构不在已知范围，需要核实！" && curl --request POST \
  --url "https://oapi.dingtalk.com/robot/send?access_token=813a36738938895e634ee516186fc0f8baec01d7792da5030dcc2a904bdc501b" \
  --header "content-type: application/json" \
  --data "{
     'msgtype': 'markdown',
     'markdown': {'title':'预警',
                  'text':'# init系统警报 \n> `date +"%Y-%m-%d %H:%M:%S"`  \n### `hostname`\n- `dpkg --print-architecture`\n- CPU架构不在已知范围，需要核实！'
     },
    'at': {
        'atMobiles': [
            '1825718XXXX'
        ],
        'isAtAll': true
    }
 }";
fi;
sudo dpkg-reconfigure locales;



echo "####################################";
echo "##### 05. 关闭图形界面 #####";
echo "####################################";
sudo systemctl set-default multi-user.target;



echo "####################################";
echo "##### 06. 安装 zsh 和 oh-my-zsh #####";
echo "####################################";
git config --global http.sslverify false
echo y | sudo apt-get install zsh;
echo y | sudo apt-get install curl;
sudo rm -rf /home/neo/.oh-my-zsh;
# sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
# sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
cd ~/upload;
# sh -c "$(curl http://file.neo.pub/sh/ohmyzsh_install.sh)"
sh -c "$(curl -kfsSL https://gitee.com/shmhlsy/oh-my-zsh-install.sh/raw/master/install.sh)"