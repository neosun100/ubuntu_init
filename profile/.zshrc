# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
  export ZSH=~/.oh-my-zsh

# 一下两处用到if 有隐患，不一定生效，有可能需要放下外面做脚本 source script处理
# 生效了，完美！
# dir 高亮
if [ `uname` = "Linux" ]; then
    eval `dircolors ~/upload/ubuntu_init/profile/dircolors.256dark`
elif [ `uname` = "Darwin" ]; then
    eval $(gdircolors ~/upload/ubuntu_init/profile/dircolors.256dark)
else
    echo " `uname` 系统未知！"
fi


ZSH_DISABLE_COMPFIX=true

# export HOMEBREW_BOTTLE_DOMAIN=https://mirrors.cloud.tencent.com/homebrew-bottles

if [ $(uname) = "Darwin" ]; then
  export HOMEBREW_BOTTLE_DOMAIN=https://mirrors.cloud.tencent.com/homebrew-bottles
elif [ $(uname) = "Linux" ]; then
  echo "" # linux
elif [ $(uname) = "WindowsNT" ]; then
  echo "" # windows
else
  echo "$(uname) 系统未知！"
fi


# 命令行高亮 重开terminus 生效
source ~/upload/ubuntu_init/profile/zsh-syntax-highlighting-filetypes.zsh
# Set name of the theme to load. Optionally, if you set this to "random"
# it'll load a random theme each time that oh-my-zsh is loaded.
# See https://github.com/robbyrussell/oh-my-zsh/wiki/Themes
ZSH_THEME="ys"
HIST_STAMPS="yyyy-mm-dd"

DISABLE_UPDATE_PROMPT=true

if [ -e /lib/terminfo/x/xterm-256color ]; then # linux
    export TERM='xterm-256color'
elif [ -e /usr/share/terminfo/78/xterm-256color ]; then # mac
    export TERM='xterm-256color'
else
    export TERM='xterm-color'
fi

# Set list of themes to load
# Setting this variable when ZSH_THEME=random
# cause zsh load theme from this variable instead of
# looking in ~/.oh-my-zsh/themes/
# An empty array have no effect
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
# sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# The optional three formats: "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
#plugins=(
#  git
#)
plugins=(gitfast
         wd
         web-search
         history
         history-substring-search
         last-working-dir
         catimg
         encode64
         urltools
         zsh-syntax-highlighting
         zsh-autosuggestions)
         
source $ZSH/oh-my-zsh.sh
# source ~/upload/ubuntu_init/profile/incr*.zsh # zsh自动补全
# 自动补全太卡了！去掉！
# 之前务必安装好fzf
source ~/upload/ubuntu_init/profile/zsh-interactive-cd.plugin.zsh # cd 自动补全

# source ~/upload/ubuntu_init/profile/zplug_init.zsh # 激活zsh 插件 管理器
source ~/.bash_profile
# zplug "changyuheng/fz", defer:1
# zplug "rupa/z", use:z.sh

# source ~/.bash_profile
# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/rsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"
if [  ]; then source <(kubectl completion zsh); fi

# 修复zsh粘贴卡顿
pasteinit() {
  OLD_SELF_INSERT=${${(s.:.)widgets[self-insert]}[2,3]}
  zle -N self-insert url-quote-magic # I wonder if you'd need `.url-quote-magic`?
}

pastefinish() {
  zle -N self-insert $OLD_SELF_INSERT
}
zstyle :bracketed-paste-magic paste-init pasteinit
zstyle :bracketed-paste-magic paste-finish pastefinish



# hstr --show-configuration

alias hh=hstr                    # hh to be alias for hstr
setopt histignorespace           # skip cmds w/ leading space from history
export HSTR_CONFIG=hicolor       # get more colors
bindkey -s "\C-r" "\C-a hstr -- \C-j"     # bind hstr to Ctrl-r (for Vi mode check doc)

export NVM_DIR="$HOME/.nvm"
[ -s "/usr/local/opt/nvm/nvm.sh" ] && . "/usr/local/opt/nvm/nvm.sh"  # This loads nvm
[ -s "/usr/local/opt/nvm/etc/bash_completion.d/nvm" ] && . "/usr/local/opt/nvm/etc/bash_completion.d/nvm"  # This loads nvm bash_completion