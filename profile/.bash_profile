##########################
###### 00.标签说明  ########
##########################
# 🍎 命令表示支持 mac 系统

# [[ `whereis yum | wc -l` == 1 ]] && type alias apt-get="yum"  || echo "非centos"

# export HOMEBREW_BOTTLE_DOMAIN=https://mirrors.cloud.tencent.com/homebrew-bottles

if [ $(uname) = "Darwin" ]; then
  export HOMEBREW_BOTTLE_DOMAIN=https://mirrors.cloud.tencent.com/homebrew-bottles
elif [ $(uname) = "Linux" ]; then
  echo "" # linux
  # export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:$HOME/.local/bin:$HOME/.local/share/pipx/bin:$PATH
elif [ $(uname) = "WindowsNT" ]; then
  echo "" # windows
else
  echo "$(uname) 1系统未知！"
fi

# 修复mac下中文乱码

export APACHE_HOST=https://mirrors.tuna.tsinghua.edu.cn
# https://www.apache.org/mirrors/#cn

export LANG=en_US.UTF-8

# dart 国内源
export PUB_HOSTED_URL=https://pub.flutter-io.cn
export FLUTTER_STORAGE_BASE_URL=https://storage.flutter-io.cn

# pyspark driver 高亮
export PYSPARK_DRIVER_PYTHON=ptpython

# 修复第三方软件损坏bug
alias fixmac='sudo spctl --master-disable'
# http://pygments.org/docs/cmdline/
# 🍎 代码高亮 效果一般use the ccat to instread cat for syntax highlighting.
# cat 终极解决方案 pygmentiz
#alias cat="ccat"
# alias cat="pygmentize -O full,style=monokai"
alias bcat="pygmentize -O full,style=dracula -l python -s"
alias pcat="pygmentize -O full,style=dracula -l python -s"
alias jcat="pygmentize -O full,style=dracula -l json -s"
alias lcat="pygmentize -O full,style=dracula -l go -s"
alias rcat="pygmentize -O full,style=dracula -l R -s"
alias hcat="pygmentize -O full,style=dracula -l haskell -s"
alias sqlcat="pygmentize -O full,style=dracula -l SQL -s"
lat() {
  pygmentize -O full,style=dracula -l nginx -s
}
# -s 刷新亦可输出高亮
# tail cat
tcat() {
  tail -f ${1:-/data/logs/jupyter/jupyter.log} | bcat
}

alias dat="bat --theme Dracula"
# 德古拉cat

alias cdu="ncdu --color dark -rr -x --exclude .git --exclude node_modules"

alias nhelp='tldr --theme base16'

source ~/upload/ubuntu_init/class/table.sh
# 拆分bash_profile
source ~/upload/ubuntu_init/class/cut.sh
source ~/upload/ubuntu_init/class/replace.sh
source ~/upload/ubuntu_init/class/fzf.sh

alias ctb="crontab -e"

# # 刷新 dns 可能要等一等
udns() {
  if [ $(uname) = "Darwin" ]; then
    sudo killall -HUP mDNSResponder # mac
  elif [ $(uname) = "Linux" ]; then
    sudo /etc/init.d/dns-clean start # linux
  elif [ $(uname) = "WindowsNT" ]; then
    ipconfig /flushdns # windows
  else
    echo "$(uname) 2系统未知！"
  fi
}

if [ $(uname) = "Darwin" ]; then
  # sudo launchctl limit maxfiles 65536 200000 # mac
  #💣 m2下不成功先注释了
  # Could not set resource limits: 150: Operation not permitted while System Integrity Protection is engaged

elif [ $(uname) = "Linux" ]; then
  echo "" # linux
elif [ $(uname) = "WindowsNT" ]; then
  echo "" # windows
else
  echo "$(uname) 3系统未知！"
fi

# 重启网卡
alias unet='sudo /etc/init.d/networking restart'

source ~/upload/ubuntu_init/class/time.sh

# alias nfzf='fzf --multi --preview "cat {}"'
# alias nfzf='fzf --multi --preview "pygmentize -O full,style=dracula -l go  {}"'

cfzf() {
  fzf --multi --preview "pygmentize -O full,style=dracula -l ${1:-xml}  {}"
}

#########################
##### 00. 环境变量 #######
#########################

if [ $(uname) = "Darwin" ]; then
  export GROOVY_HOME=/usr/local/opt/groovy/libexec
  # export ZPLUG_HOME=/usr/local/opt/zplug
  export PATH="$PATH:/usr/local/share/scala/bin"
  export PATH="$PATH:/usr/local/share/sbt/bin"
  export PATH=$PATH:~/.cargo/bin
  # source $ZPLUG_HOME/init.zsh
  export ZPLUG_HOME=/opt/homebrew/opt/zplug
  source $ZPLUG_HOME/init.zsh  
  source ~/.cargo/env
  export PATH=$PATH:~/.local/bin
  export PATH="/opt/homebrew/opt/coreutils/libexec/gnubin:$PATH"
elif [ $(uname) = "Linux" ]; then
  export ZPLUG_HOME=/home/linuxbrew/.linuxbrew/opt/zplug
  source $ZPLUG_HOME/init.zsh
  export PATH=$PATH:~/.local/bin
else
  echo "$(uname) 4系统未知！"
fi

# export ZPLUG_HOME=/usr/local/opt/zplug
# source $ZPLUG_HOME/init.zsh

# export ZPLUG_HOME=/home/linuxbrew/.linuxbrew/opt/zplug
# source $ZPLUG_HOME/init.zsh
if [ $(uname) = "Darwin" ] && [ -d /usr/local/spark ]; then
  export SPARK_HOME=/usr/local/spark
  export PATH=$PATH:$SPARK_HOME/bin
elif [ $(uname) = "Linux" ] && [ -d /usr/local/spark ]; then
  export SPARK_HOME=/usr/local/spark
  export PATH=$PATH:$SPARK_HOME/bin
else
  echo "$(uname)  5系统未知 或 还未添加 spark bin"
fi

if [ $(uname) = "Darwin" ] && [ -d /usr/local/kafka ]; then
  export KAFKA_HOME=/usr/local/kafka
  export PATH=$PATH:$KAFKA_HOME/bin
elif [ $(uname) = "Linux" ] && [ -d /usr/local/kafka ]; then
  export KAFKA_HOME=/usr/local/kafka
  export PATH=$PATH:$KAFKA_HOME/bin
else
  echo "$(uname)  6系统未知 或 还未添加 kafka bin"
fi

if [ $(uname) = "Darwin" ] && [ -d /usr/local/flink ]; then
  export FLINK_HOME=/usr/local/flink
  export FLINK_CONF_DIR=/usr/local/flink/conf
  export PATH=$PATH:$FLINK_HOME/bin
elif [ $(uname) = "Linux" ] && [ -d /usr/local/flink ]; then
  export FLINK_HOME=/usr/local/flink
  export FLINK_CONF_DIR=/usr/local/flink/conf
  export PATH=$PATH:$FLINK_HOME/bin
else
  echo "$(uname)  7系统未知 或 还未添加 flink bin"
fi

if [ $(uname) = "Darwin" ] && [ -d /usr/local/hadoop ]; then
  export HADOOP_HOME=/usr/local/hadoop
  export HADOOP_CONF_DIR=/usr/local/hadoop/etc/hadoop
  export PATH=$PATH:$HADOOP_HOME/bin
elif [ $(uname) = "Linux" ] && [ -d /usr/local/hadoop ]; then
  export HADOOP_HOME=/usr/local/hadoop
  export HADOOP_CONF_DIR=/usr/local/hadoop/etc/hadoop
  export PATH=$PATH:$HADOOP_HOME/bin
else
  echo "$(uname)  8系统未知 或 还未添加 hadoop bin"
fi

# if [ `uname` = "Darwin" ] && [ -d /usr/local/Cellar/pssh ] ; then
# export PSSH_HOME=/usr/local/Cellar/pssh
# export PATH=$PATH:$KAFKA_HOME/*/bin
# elif [ `uname` = "Linux" ] && [ -d /usr/local/kafka ] ; then
# export KAFKA_HOME=/usr/local/kafka
# export PATH=$PATH:$KAFKA_HOME/bin
# else
# echo "`uname`  系统未知 或 还未添加 kafka bin"
# fi

export EDITOR=/usr/bin/vim

export HISTTIMEFORMAT="%F %T $(whoami) "

export LOCAL_HOME=/usr/local

# cheat语法高亮
export CHEATCOLORS=true

export HISTCONTROL=ignorespace
# 然后你输入的命令前加个空格，就会被history过滤。

# python
field() {
  awk "{print \$${1:-1}}"
}

if [ $(uname) = "Linux" ] && [ $(file /bin/bash | field 7) = "ARM" ]; then
  export PYTHON_HOME=~/miniconda3 && export PATH=$PYTHON_HOME/bin:$PATH
elif [ $(uname -m) = "x86_64" ] && [ $(hostname) = "Smart.local" ]; then
  export PYTHON_HOME=/usr/local/anaconda3 && export PATH=$PYTHON_HOME/bin:$PATH
elif [ $(uname -m) = "x86_64" ] && [ $(hostname) = "Beta.local" ]; then
  export PYTHON_HOME=/usr/local/anaconda3 && export PATH=$PYTHON_HOME/bin:$PATH
elif [ $(uname) = "Darwin" ]; then
  export PYTHON_HOME=~/anaconda3 && export PATH=$PYTHON_HOME/bin:$PATH
else
  export PYTHON_HOME=~/anaconda3 && export PATH=$PYTHON_HOME/bin:$PATH
fi
# export PYTHON_HOME=~/anaconda3
# export PATH=$PYTHON_HOME/bin:$PATH

if [ $(uname) = "Darwin" ]; then
  # mojo
  export MODULAR_HOME="/Users/jiasunm/.modular"
  export PATH="/Users/jiasunm/.modular/pkg/packages.modular.com_mojo/bin:$PATH"

elif
  [ $(uname) = "Linux" ]
then
  # mojo
else
  echo "$(uname) 9系统未知！1"
fi



if [ $(uname) = "Darwin" ]; then
  # go
  export GOROOT=/usr/local/go
  export GOPATH=$HOME/go
  export PATH=$PATH:$GOROOT/bin
  # export GO_HOME=/usr/local/Cellar/go/1.14.6 # 这里是个深坑，还是安装包安装，再软连接方便
  # export GOROOT=$GO_HOME
  # export PATH=$GO_HOME/bin:$PATH
  # # export PATH=/bin:/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
  # # export PATH="/usr/bin:/bin:/usr/sbin:/sbin:/usr/local/bin:/usr/X11/bin"
  # # export GOROOT=/usr/local/opt/go/bin/
  # export GOPATH=/Users/neo/Code/golang/ # 这是我的项目路径  /Users/neo/Code/golang
  # export GOBIN=$GOPATH/bin
  # export PATH=GOBIN
  # export JAVA_8_HOME=$(/usr/libexec/java_home -v1.8)
  # export JAVA_8_HOME=$(/Library/Java/JavaVirtualMachines/jdk1.8.0_301.jdk/Contents/Home)
  # export JAVA_11_HOME=$(/usr/libexec/java_home -v11)

  # alias java8='export JAVA_HOME=$JAVA_8_HOME'
  # alias java11='export JAVA_HOME=$JAVA_11_HOME'
  # export JAVA_HOME=$JAVA_8_HOME

  # export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk1.8.0_301.jdk/Contents/Home
  # export PATH=$JAVA_HOME/bin:$PATH

elif
  [ $(uname) = "Linux" ]
then
  # brew
  export BREW_HOME=/home/linuxbrew/.linuxbrew/
  export PATH=$BREW_HOME/bin:$PATH

  # java
  export JAVA_HOME=$LOCAL_HOME/arrow/jdk
  export PATH=$JAVA_HOME/bin:$PATH

  # scala
  export SCALA_HOME=$LOCAL_HOME/arrow/scala
  export PATH=$SCALA_HOME/bin:$PATH

  # maven
  export MAVEN_HOME=$LOCAL_HOME/arrow/maven
  export PATH=$MAVEN_HOME/bin:$PATH

  # sbt
  export SBT_HOME=$LOCAL_HOME/arrow/sbt
  export PATH=$SBT_HOME/bin:$PATH

  # node
  export NODE_HOME=$LOCAL_HOME/arrow/node
  export PATH=$PATH:$NODE_HOME/bin
  export NODE_PATH=$NODE_HOME/lib/node_modules

  # go
  export GOROOT=$LOCAL_HOME/arrow/go
  export GOPATH=$HOME/go
  export PATH=$GOROOT/bin:$GOPATH/bin:$PATH

  # libwebp
  # 谷歌的图片转化
  export WEBP_HOME=$LOCAL_HOME/arrow/libwebp
  export PATH=$WEBP_HOME/bin:$PATH

  # chromedriver
  export PATH=/usr/local/bin/chromedriver:$PATH
else
  echo "$(uname) 10系统未知！1"
fi

# if [ `uname` = "Linux" and `dpkg --print-architecture` = 'armhf' ]; then

# elif [ `dpkg --print-architecture` = 'amd64' ]; then

########################
### 中文 man ############
########################

if [ $(uname) = "Darwin" ]; then
  alias cman='man -M /usr/local/share/man/zh_CN'
elif [ $(uname) = "Linux" ]; then
  alias cman='man -M /usr/local/zhman/share/man/zh_CN'
else
  echo "$(uname) 11系统未知！2"
fi

# alias cman='man -M /usr/local/zhman/share/man/zh_CN'

# 🍎 高亮 http
alias chttp='http -b -s dracula'

nhttp() {
  http -b -s dracula ${1:-"https://webstatic.mihoyo.com/admin/mi18n/plat_cn/m202004281054311/m202004281054311-zh-cn.json?"}
}

#########################
##### 01. 命令简写 #######
#########################
# 🍎
nls() {
  if [ $(uname) = "Darwin" ]; then
    gls --color=auto
  elif [ $(uname) = "Linux" ]; then
    ls
  else
    echo "$(uname) 12系统未知！3"
  fi
}

alias nl="nls -lah"

alias htop='sudo htop'
alias h='htop'
alias ll='ls -lah'
alias l='ll'
alias c='clear'

#### 更新 升级 安装
alias u='sudo apt-get update'
alias g='echo y | sudo apt-get upgrade'
alias i='sudo apt-get install'
alias ug='u && g'
# 消除升级文件被占用的bug
alias fixg='sudo rm /var/cache/apt/archives/lock && sudo rm /var/lib/apt/lists/lock && sudo rm /var/lib/dpkg/lock'
# 修复 dpkg
alias fixd="sudo rm /var/lib/dpkg/updates/*"
# 修复 zsh history
alias fixz="cp -f ~/.zsh_history ~/zsh_history && rm -f ~/.zsh_history && echo $(nows) 👌 'zsh_history 问题已修复' | lcat"
# 🍎 修复 prompt-toolkit 相关依赖问题
alias fixp="pip install --upgrade pip && \
            pip install pygments -U && \
            pip install mycli -U  && \
            pip install jupyter-console -U && \
            pip install ipython -U"

alias fix_pip="python -m pip install --upgrade pip"

# 关机
alias shutdown='sudo shutdown -h now'
alias reboot='sudo reboot'

# 升级 conda 版本
alias fixconda="conda update -n base -c defaults conda"

# 查看 python 虚拟环境
alias pe='conda env list | lcat'

# 切换python虚拟环境  source python env
# 默认切换回系统环境
spe() {
  conda activate ${1:-base}
}

# python退出虚拟环境到系统默认环境
# back system env
# alias bse="conda deactivate `conda env list | grep \* | field 1 `"

alias bse="conda deactivate"

# 显示系统磁盘位置
alias sysdisk="sudo fdisk -l | grep Linux | awk '{print $1}'"

sdisk() {
  sudo fdisk -l | grep Linux | awk '{print $1}'
}

# 系统磁盘健康状况 check sysdisk
checksdisk() {
  sudo smartctl -H $(sdisk) | lcat
}

checkdisk() {
  sudo smartctl -H $1 | lcat
}

# 🍎 快速修改配置文件
alias vimb='vim ~/.bash_profile'
alias sourceb='source ~/.bash_profile'

# 🍎 history 最后 28条
alias ht='history | tail -n 28 | lcat'

# 忽略首行
alias unhead='tail -n +2'

# 从大到小排序,第一个字段是排序的字段默认值1，第二字段是分隔符，
# ns neo sort
ns() {
  sort -nrk ${1:-1} -t ${2:-$'\t'}
}

# neo history
# https://cloud.tencent.com/info/31b4a11158ecb2fbdd7477675ebc3d18.html
nh() {
  #当变量为null或为空字符串时则var=b
  history | tail -n ${1:-28} | lcat
}

# vim history
vimh() {
  vim ~/.zsh_history
}

# neo history only cmd  函数添加默认值
nhoc() {
  history | tail -n ${1:-28} | cut -c 24- | lcat
}

source ~/upload/ubuntu_init/class/df.sh

# 🐧 显示所有磁盘路径，含物理，逻辑
alias ndf='df -halT | lcat'
# 🐧 仅显示物理磁盘
alias sndf='df -halT | head -n 1 | bcat && ndf | grep -v "loop\|cgroup" | grep "dev" | grep  "%" | grep -v "udev\|tmpfs" '

# 🐧 仅显示非 loop 磁盘 logic neo df
alias lndf='df -halT | head -n 1 | bcat && ndf | tail -n +2  | grep "%" | grep -v "loop\|cgroup\|docker" | ns '

# 🍎 history 最后 28条 只显示命令
alias htoc='history | tail -n 28 | cut -c 24- | lcat ' # history tail only cmd

# 🍎 扩展mkdir 补全上级目录并设置权限 777
alias mkdir7='mkdir -p -m 777'

# w 最佳设置，查看当前在线用户
alias w='w --from --ip-addr --no-current'
# 监控当前docker.containers
alias watchc='watch --differences --interval 1 sudo docker ps -a'
# 监控当前登录账户
alias watchw='watch --differences --interval 1 w'

# 🍎 指定目录递归全777 权限
alias chmod7='sudo chmod -R 777 '

# cpu温度
alias cputem='sensors | lcat'

# alias hcat="highlight -O ansi" # pass
#alias pyhcat='hcat --syntax python'
# 🍎 代码高亮 效果略好
# pip install pygmentize
# pip install pygments -U
#alias pcat='pygmentize -g'

# 🍎 彩色cat mac 效果很差
alias p="lolcat"

# 🍎 马赛克 mosaic 显示图片
alias msc="cishower -v -w 110"

open_window() {
  echo "📺📺📺📺📺📺📺📺📺📺📺"
  echo "打开图形界面"
  echo "📺📺📺📺📺📺📺📺📺📺📺"
  sudo systemctl set-default graphical.target
  echo "需要重启"
}

close_window() {
  echo "📄📄📄📄📄📄📄📄📄📄📄📄📄"
  echo "关闭图形界面"
  echo "📄📄📄📄📄📄📄📄📄📄📄📄📄"
  sudo systemctl set-default multi-user.target
  echo "需要重启"
}
# json格式化高亮，用于后输出
#alias bcat="xargs python ~/upload/ubuntu_init/pyScript/jcat.py --s"

# json高亮显示，否则普通 pprint
alias ncat="python ~/upload/ubuntu_init/pyScript/jcat.py"
# 案例
# $ ncat \{"剩余流量":"`flow|kab`","查询时间":"`_nows |kab`","续费日期":"`flowEnd |kab`"\}
# {
#     '剩余流量': '197.42GB',
#     '查询时间': '2019-01-22🚀17_27_56',
#     '续费日期': '2019-10-18'
# }

# 🍎 开启ftp
alias nftp="python -m pyftpdlib -p 1210 -u neo -P bilibili -w -d ~/upload/"

dftp() {
  if [ $(uname) = "Linux" ]; then
    python -m pyftpdlib -p 1210 -u neo -P bilibili -w -d ~/upload/
  elif [ $(uname) = "Darwin" ]; then
    python -m pyftpdlib -p 1210 -u neo -P bilibili -w -d ~/Downloads/
  else
    echo " $(uname) unknow sys"
  fi
}

# 🍎 彩色艺术字
alias color="toilet  -f mono12 -F gay"

# 网络时间戳监控
# nload

# 监控每一个套接字连接传输的数据
alias sload='sudo iftop -n'

# 高亮交互IP局域网流量监控
alias ipload='sudo iptraf'

# 进程流量监控
alias pload='sudo nethogs'

# 网卡可视化监控
alias vload='speedometer -r ens33 -t ens33'

# shell录制
# $ pip install asciinema
alias live='asciinema'
# svg录像存储在svg目录，需要提前生成
alias svglive="termtosvg ~/upload/svg/$(faker ean8).svg"

# 二进制文件转十六进制查看
# 对于多引号嵌套的解决办法
# hexdump -e '16/1 "%02X " "  |  "' -e '16/1 "%_p" "\n"'
hexstring1='16/1 "%02X " "  |  "'
hexstring2='16/1 "%_p" "\n"'
alias hexsee='hexdump -e $hexstring1 -e $hexstring2'

# 声明变量 清华 豆瓣
typeset douban='http://pypi.douban.com/simple/'
typeset qinghua='https://pypi.tuna.tsinghua.edu.cn/simple'

# 配套的别名命令
alias pipd-install='pip install -i $douban'
alias pipq-install='pip install -i $qinghua'

# 快速显示最后 几条 python 相关的 命令
alias hgp='history | grep python | tail'

# 快捷键 字符串转二维码
alias qr="qrencode -o - -t ANSI"

# md web 呈现
alias md2web='reveal-md --highlight-theme monokai --theme white --host "0.0.0.0" --port=6789'

# crontab-ui
# 切忌务必在局域网使用，外网慎用！
alias nctb='HOST=0.0.0.0 PORT=9000 crontab-ui --autosave'

alias ndu='sudo du -ah --max-depth=1'

#新快速开启 tmux
alias tnn='tmux new -s neo'

# 快速 attach tmux
alias tan='tmux attach -t neo'

fixtmux() {
  echo "###############################################"
  echo "##### 06. 安装 tmux 重启复原插件 ################"
  echo "##############################################"
  echo "$(date +'%Y-%m-%d %H:%M:%S') 检验 ~/.tmux 是否创建"
  if [ ! -d ~/.tmux ]; then
    mkdir ~/.tmux
    chmod 777 ~/.tmux
    echo "$(date +'%Y-%m-%d %H:%M:%S') ~/.tmux 已创建并777！"
  else
    echo "$(date +'%Y-%m-%d %H:%M:%S') ~/.tmux 目录存在！"
  fi

  cd ~/.tmux/
  git clone https://github.com/tmux-plugins/tmux-resurrect.git
  git clone https://github.com/tmux-plugins/tmux-continuum.git
  echo "$(date +'%Y-%m-%d %H:%M:%S') tmux 重启复原插件 安装完成"
  cd ~
}

source ~/upload/ubuntu_init/class/ip.sh
source ~/upload/ubuntu_init/class/proxy.sh
source ~/upload/ubuntu_init/class/flow.sh
# # 代理流量查询
# flow(){
# http GET 'https://e.2345.to/clientarea.php?action=productdetails&id=44908' \
#   accept:'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8' \
#   accept-encoding:'gzip, deflate, br' \
#   accept-language:'zh-CN,zh;q=0.9' \
#   cache-control:max-age=0 \
#   cookie:'UM_distinctid=168e56e1a74115-0ff1d9def8c757-b781636-144000-168e56e1a75225; CNZZDATA1273869891=1066208325-1550034757-%7C1550034757; WHMCSbxdqZID5HE7n=lts3kavqjreuiqmrbr2n30f7v0; WHMCSUser=43615%3A04281d526702850354fd8fb45b55505dfc6d748f' \
#   upgrade-insecure-requests:1 \
#   user-agent:'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36' |  grep "剩余流量" | keycut "剩余流量  : " "</p>"
# }

# flownum(){
# http GET 'https://e.2345.to/clientarea.php?action=productdetails&id=44908' \
#   accept:'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8' \
#   accept-encoding:'gzip, deflate, br' \
#   accept-language:'zh-CN,zh;q=0.9' \
#   cache-control:max-age=0 \
#   cookie:'UM_distinctid=168e56e1a74115-0ff1d9def8c757-b781636-144000-168e56e1a75225; CNZZDATA1273869891=1066208325-1550034757-%7C1550034757; WHMCSbxdqZID5HE7n=lts3kavqjreuiqmrbr2n30f7v0; WHMCSUser=43615%3A04281d526702850354fd8fb45b55505dfc6d748f' \
#   upgrade-insecure-requests:1 \
#   user-agent:'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36' |  grep "日后" | keycut "<strong>" "</strong>日后</div>"
# }

# flowEnd(){
# http GET 'https://e.2345.to/index.php?m=renewal' \
#   accept:'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8' \
#   accept-encoding:'gzip, deflate, br' \
#   accept-language:'zh-CN,zh;q=0.9' \
#   cache-control:max-age=0 \
#   cookie:'UM_distinctid=1664ee9c0781b2-0e7f22640fdc71-8383268-144000-1664ee9c0796c8; WHMCSUser=43615%3A04281d526702850354fd8fb45b55505dfc6d748f; WHMCSbxdqZID5HE7n=m27nv7sojd9ejrg4lav7qf31k3; CNZZDATA1273869891=57145790-1538920400-%7C1548124951' \
#   referer:https://e.2345.tn/clientarea.php \
#   upgrade-insecure-requests:1 \
#   user-agent:'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36' | grep -E -o "([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3})-(((0[13578]|1[02])-(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)-(0[1-9]|[12][0-9]|30))|(02-(0[1-9]|[1][0-9]|2[0-8])))"
# }

# cflow(){
#   echo "剩余流量:\t `flow`";
#   echo "重置天数:\t `flownum`";
#   echo "查询时间:\t `nows`";
#   echo "续费日期:\t `flowEnd`";
# }

# alias nflow="cflow | lcat" # 高亮代理流量

# jflow(){
#   ncat \{"剩余流量":"`flow|kab`","重置天数":"`flownum|kab`","查询时间":"`_nows |kab`","续费日期":"`flowEnd |kab`"\}
# }

ipwhere() {
  chttp -b GET "https://dm-81.data.aliyun.com/rest/160601/ip/getIpInfo.json?ip=$1" \
    authorization:'APPCODE c3ef1dd6767d42aca7b5deed29d376b2'
}

# UT挖矿
nut() {
  (/home/neo/upload/ulord/ulordrig \
    -o stratum+tcp://ulord.bi-chi.com:7100 \
    -O UYxN8zKYWHkUAMED2EbUXb2kvLz1X1i77p:x \
    -t $1 \
    -a cryptohello \
    -l /home/neo/upload/ulord/ulord.log &)
}

# 最大线程数计算
# alias nutm="nut `sudo cat /proc/cpuinfo |grep 'processor'|wc -l`"
alias nutm="nut $([[ -f /proc/cpuinfo ]] && sudo cat /proc/cpuinfo | grep 'processor' | wc -l || echo 'mac 系统无 /proc/cpuinfo 文件')"

# nut日志
alias nul='tail -f ~/upload/ulord/ulord.log | lcat'

# 进程名关键词到进程id ,只id
Pname2id() {
  Pname2Pid $1 | awk '{print $2}' | head -n 1
}

# 杀死nut
#alias knut="kill -9 `sudo ps -ef | grep ulord | sort -k 7 -r | awk '{print $2}' | head -n 1`"

killnut() {
  kill -9 $(sudo ps -ef | grep ulord | sort -k 7 -r | awk '{print $2}' | head -n 1)
}

# xmr 挖矿
nxmr() {
  ~/upload/xmrig/xmrig -B
}

#  杀死xmr
killnxmr() {
  kill -9 $(sudo ps -ef | grep xmrig | sort -k 7 -r | awk '{print $2}' | head -n 1)
}

# 短信模块
# sms 标题 内容，统一其后两个功能
sms() {
  chttp GET "http://m.toutiao.utan.com/?requestMethod=crawler.SendSms&mobile=18321021827&message=$(date +"%Y-%m-%d %H:%M:%S") $(hostname) $1 $2" \
    cookie:"PHPSESSID=b34mhboj3d410bcht9g3smqs80; yagdt=b34mhboj3d410bcht9g3smqs80.1544437282.1544437282"
}

# 邮件体系
# semail "标题" "内容"
semail() {
  echo "$(date +"%Y-%m-%d %H:%M:%S") \n$(hostname)\n$2" |
    mailx -v -s $1 yiqidangqian@foxmail.com
}

# 钉钉消息
# sdd "标题" "内容"
sddmd() {
  echo "{
     'msgtype': 'markdown',
     'markdown': {'title':'预警',
'text':'# $1 \n> $(date +"%Y-%m-%d %H:%M:%S")  \n### $(hostname)\n $2'
     },
    'at': {
        'atMobiles': [
            '1825718XXXX'
        ],
        'isAtAll': true
    }
 }" |
    chttp POST "https://oapi.dingtalk.com/robot/send?access_token=813a36738938895e634ee516186fc0f8baec01d7792da5030dcc2a904bdc501b" \
      content-type:application/json
}

# 钉钉消息 多个
sddmmd() {
  echo "{
     'msgtype': 'markdown',
     'markdown': {'title':'预警',
'text':'# $1 \n> $(date +"%Y-%m-%d %H:%M:%S")  \n### $(hostname)\n- $2 \n- $3 \n- $4 \n- $5 \n- $6'
     },
    'at': {
        'atMobiles': [
            '1825718XXXX'
        ],
        'isAtAll': true
    }
 }" |
    chttp POST "https://oapi.dingtalk.com/robot/send?access_token=5e5bc1f91c6a272faa48d719c811d4654665c3d59123dbdca41fb0139cc7c6a3" \
      content-type:application/json
}

# 整合邮件钉钉短信
sall() {
  sms $1 $2
  semail $1 $2
  sddmd $1 $2
}

# 直接打开 带通知的 UT
open_ut() {
  sh ~/upload/ubuntu_init/script/901.ut_open.sh
}

# 直接关闭 带通知的 UT
close_ut() {
  sh ~/upload/ubuntu_init/script/902.ut_close.sh
}

# 开机启动脚本ip钉钉webhook
startup() {
  sddmd "开机启动" "$(iip)"
}

# 回到第一个tmux 会话  btmux back tmux
alias btmux="tmux attach -t $(tmux ls | awk '{print $1}' | cut -d ':' -f 1)"

###########################
##### 02. 自定义函数 #######
###########################

# aria2c 支持 种子，磁力，文件
ndl() {
  aria2c -x 16 $1
}



ngpu() {
  echo "🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌"
  echo "📺\t显卡信息"
  echo "🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌"
  nvidia-smi -L 
  echo ""
  echo ""
  echo "🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒"
  echo "📺\t显卡进程与功耗"
  echo "🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒"
  nvidia-smi
}
# 显示gpu信息
ngpu() {
  echo "🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌"
  echo "📺\t显卡信息" | lcat
  echo "🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌"
  nvidia-smi -L | lcat
  echo ""
  echo ""
  echo "🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒"
  echo "📺\t显卡进程与功耗" | lcat
  echo "🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒"
  nvidia-smi | lcat
}

# gpu的top 不高亮，不推荐
gtop() {
  watch -n 1 -d nvidia-smi
}

# gpu 资源监控
# pip install gpustat
sngpu() {
  gpustat -cpu | lcat
}
#################################################################

# 截取文件中间几行
mid() {
  sed -n '$1,$2p' $3
}

# 函数合集
# json格式化输出函数
# sudo apt-get install jq
jsona() {
  sudo cat $1 | jq
}

# 忽略注释空行显示文本
# 增加 空格#类型的过滤
fcat() {
  sudo /bin/cat $1 | grep -v "^#" | grep -v "^$" | grep -v "^[ ]*[#]"
}

# 去字符串空格系列
# kill all blank 去除字符串所有空格
alias kab="tr -d '[ \t]'"
# kill head blank 去除字符串首空格
alias khb="sed -e 's/^[ \t]*//g'"
# kill tail blank 去除字符串尾空格
alias ktb="sed  's/[ \t]*$//g'"
# 删除前、后空格，不删除中间空格
alias khtb="sed -e 's/^[ \t]*//g' -e 's/[ \t]*$//g'"

# grep 高亮
alias grep='grep --color=auto'

# 批量 删除 docker 容器 正则
mdelDockers() {
  # cmd_result=($(sudo docker ps -a| grep -o -E "$1"))
  cmd_result=($(sudo docker ps -a | grep -E "$1" | field 1))
  for i in ${cmd_result[@]}; do sudo docker rm -f $i; done
}

# 批量 重启 docker 容器 正则
mrestartDockers() {
  # cmd_result=($(sudo docker ps -a| grep -o -E "$1"))
  cmd_result=($(sudo docker ps -a | grep -E "$1" | field 1))
  for i in ${cmd_result[@]}; do sudo docker restart $i; done
}

# 批量 停止 docker 容器 正则
mstopDockers() {
  # cmd_result=($(sudo docker ps -a| grep -o -E "$1"))
  cmd_result=($(sudo docker ps -a | grep -E "$1" | field 1))
  for i in ${cmd_result[@]}; do sudo docker stop $i; done
}

setup_kube-shell() {
  # 安装kube-shell环境
  echo "$(date +'%Y-%m-%d %H:%M:%S') 创建虚拟环境安装 kube-shell "
  if [ ! -d ~/anaconda3/envs/kubeshell ]; then
    echo "$(date +'%Y-%m-%d %H:%M:%S') 不存在 kubeshell, 安装部署虚拟环境 "
    conda update -n base -c defaults conda
    conda create -n kubeshell python=3.6
    ~/anaconda3/envs/kubeshell/bin/pip install --upgrade pip
    ~/anaconda3/envs/kubeshell/bin/pip install kube-shell
  else
    echo "$(date +'%Y-%m-%d %H:%M:%S') kubeshell 虚拟环境已存在，请使用 kube-shell启动命令"
  fi
}

alias kube-shell="~/anaconda3/envs/kubeshell/bin/kube-shell"

alias jping="pingparsing -c 3"
# # json 输出更ping
# pip install pingparsing==0.14;

# 快速进入localhost mysql
mysqlUser=root
mysqlPasswd=123456
mysqlLocalIp=127.0.0.1
mysqlLocalPort=3306
mysqlDatabase=ftnn
alias mycli='mycli --myclirc ~/upload/ubuntu_init/profile/myclirc'
alias myclih='mycli mysql://$mysqlUser:$mysqlPasswd@$mysqlLocalIp:$mysqlLocalPort/$mysqlDatabase'

alias pgcli='pgcli --pgclirc ~/upload/ubuntu_init/profile/pgclirc'

# 快速进入redis

alias rcli-loop="redis-cli -h r-uf6cd27bd64d64d4.redis.rds.aliyuncs.com -a LOOP2themoon --no-auth-warning"

alias sysname="lsb_release -a | grep 'Distributor ID' | cut -f2"
#######################################
############### 电脑信息 ###############
# 系统信息
alias sysinfo='lsb_release -a'

# cpu 型号
alias cpuinfo='[[ -f /proc/cpuinfo ]] && sudo cat /proc/cpuinfo  | grep "model name"  | uniq | cut -f2 | cut -d ":" -f2 | sed "s/^[ \t]*//g" || echo "mac 系统无 /proc/cpuinfo 文件" '

# 物理核心数
alias phynum="grep 'physical id' /proc/cpuinfo | sort | uniq | wc -l"

# 逻辑核心数
alias logicnum='[[ -f /proc/cpuinfo ]] && sudo cat /proc/cpuinfo |grep "cores"|uniq | cut -d ":" -f2 | sed "s/^[ \t]*//g" || echo "mac 系统无 /proc/cpuinfo 文件" '
# 去行首空格
# CPU多线程数
alias threadnum="[[ -f /proc/cpuinfo ]] && sudo cat /proc/cpuinfo |grep 'processor'|wc -l || echo 'mac 系统无 /proc/cpuinfo 文件'  "

# Linux 查看内存的插槽数,已经使用多少插槽.每条内存多大：
alias ramnum='sudo dmidecode|grep -A5 "Memory Device"|grep Size|grep -v Range | grep -v "No Module Installed"'

# linux下内存速度
alias ramspeed='sudo dmidecode|grep -A16 "Memory Device"|grep "Speed" | grep -v "Unknown"'

# 系统最大内存支持
alias sysmaxram='sudo dmidecode|grep -P "Maximum\s+Capacity" | cut -d ":" -f2'

# Linux下查看主机品牌
alias pcbrand='sudo dmidecode|grep "System Information" -A9|egrep  "Manufacturer|Product|Version"'

# linux 下查看主板品牌
alias boardbrand='sudo dmidecode|grep "Base Board Information" -A9|egrep  "Manufacturer|Product|Version"'

# 硬盘容量
# alias rominfo='sudo fdisk -l | grep Linux -b2'
alias rominfo='sudo fdisk -l | grep /dev'
# cpu速度
alias cpuspeed='[[ -f /proc/cpuinfo ]] && sudo cat /proc/cpuinfo |grep MHz|uniq | cut -d ":" -f2 | sed "s/^[ \t]*//g" | head -n 1 || echo "mac 系统无 /proc/cpuinfo 文件"  '

# 正则截取两字符串之间的内容
strmid() {
  awk -v head=$1 -v tail=$2 '{print substr($0, index($0,head)+length(head),index($0,tail)-index($0,head)-length(head))}'
}

# 万不得已，做不好连接字符串，换行加
persecond() {
  s2="/秒"
  echo ${1}${s2} # 当然这样写 $s1$s2 也行，但最好加上大括号
}

# 定义一个函数统一输出系统信息
pcinfo() {
  cd ~/upload
  startTS=$(nowtns)
  echo "====================SYS info====================" | lcat
  # linuxlogo;
  neofetch
  sysinfo | lcat
  echo "系统架构:\t$(uname -m)" | lcat
  echo "====================PC info====================" | lcat
  echo "主机品牌" | lcat
  pcbrand | lcat
  echo "主板品牌" | lcat
  boardbrand | lcat
  echo "====================CPU info====================" | lcat
  echo "cpu架构:\t$(dpkg --print-architecture)" | lcat
  echo "cpu型号:\t$(cpuinfo)" | lcat
  echo "物理核心数:\t$(phynum)" | lcat
  echo "逻辑核心数:\t$(logicnum)" | lcat
  echo "CPU多线程数:\t$(threadnum)" | lcat
  echo "cpu当前频率:\t$(cpuspeed)" | lcat

  echo "====================RAM info====================" | lcat
  echo "插槽及当前内存条容量:" | lcat
  ramnum | lcat
  echo "内存速度" | lcat
  ramspeed | lcat
  echo "系统支持最大内存:\t$(sysmaxram)" | lcat
  echo "====================ROM info====================" | lcat
  lndf
  echo "====================Disk info====================" | lcat
  echo "已挂在磁盘" | lcat
  sndf
  echo "-------------------------------------------------"
  echo "所有磁盘" | lcat
  sudo fdisk -l | grep "/dev/s\|/dev/nvme" | grep -v "EFI\|*" | grep "Disk" | lat
  # echo "主硬盘块大小"  | lcat;
  # sudo tune2fs -l `sudo fdisk -l | grep Linux | awk '{print $1}'`  | grep Block | lcat;
  # alias readhd="sudo hdparm -tT `sudo fdisk -l | grep Linux | awk '{print $1}'`";
  # alias readhd="sudo hdparm -tT `sudo fdisk -l | grep Linux | sort -k 4  | head -n 1 | awk '{print $1}'`";
  # echo "主硬盘读速度:\t`readhd | strmid "seconds = " "/sec" |sed -n "4,4p"`/s" | lcat ;
  echo "硬盘读速度:" | lcat
  sudo hdparm -t $(sudo fdisk -l | grep "Disk" | grep "/dev/s\|/dev/nvme" | field 2 | pass_mao)
  echo "-------------------------------------------------"

  echo "硬盘写速度" | lcat
  # sudo dd if=/dev/zero bs=1024 count=1000000 of=/1Gb.file;
  # sudo rm -rf /1Gb.file;
  for i in $(sudo fdisk -l | grep "Disk" | grep "/dev/s\|/dev/nvme" | field 2 | pass_mao); do
    echo $i | lat
    sync
    /usr/bin/time -p bash -c "(sudo dd if=$i of=test.dd  bs=1M count=2000 | grep 'MB');"
    sleep 1
  done

  echo "====================IP info====================" | lcat
  nip
  echo "====================Port info====================" | lcat
  port
  echo "===================CPU SPEED==================" | lcat
  echo "单核计算圆周率5000位耗时" | lcat

  #`time echo "scale=5000; 4*a(1)" | bc -l -q >/dev/null 2>&1 ` |sed  '/^#.*\|^$/d';
  sts=$(nowtns) && echo "scale=5000; 4*a(1)" | bc -l -q >/dev/null 2>&1 && ets=$(nowtns) && rst=$(gawk -v x=$sts -v y=$ets 'BEGIN{printf "%.6f\n",y-x}') && echo "5000位单核耗时 $rst 秒 " | lcat
  echo "多核心分数:$(test_cpu | grep "total number" | field 5)" | lcat
  echo "=================================================" | lcat
  echo "=================================================" | lcat
  endTS=$(nowtns)
  result=$(gawk -v x=$startTS -v y=$endTS 'BEGIN{printf "%.6f\n",y-x}')
  echo "检测总用时 $result 秒，如有疑问请联系 Neo 微信 ↓ " | lcat
  qrencode -o - -t ANSI "https://u.wechat.com/EK4YMwsrFvtGJDdI0dPNLrA"
}

# 网络带宽与网络延迟测试
# 不支持苹果
# ws web speed server
alias ws_server='nip && qperf'
# 家里的qperf节点ip
typeset home_qperf_server_ip="192.168.254.173"
# ws web speed client
ws_client() {
  qperf -t 60 --use_bits_per_sec ${1:-$home_qperf_server_ip} tcp_bw tcp_lat
}
source ~/upload/ubuntu_init/class/random.sh
source ~/upload/ubuntu_init/class/format.sh
source ~/upload/ubuntu_init/class/table.sh
source ~/upload/ubuntu_init/class/chrome.sh
source ~/upload/ubuntu_init/class/jq.sh
source ~/upload/ubuntu_init/class/docker.sh
source ~/upload/ubuntu_init/class/k8sctl.sh
source ~/upload/ubuntu_init/class/dir_clear.sh
source ~/upload/ubuntu_init/class/xmind.sh
source ~/upload/ubuntu_init/class/jupyter.sh
source ~/upload/ubuntu_init/class/fix.sh
source ~/upload/ubuntu_init/class/mount.sh
source ~/upload/ubuntu_init/class/host_info.sh
source ~/upload/ubuntu_init/class/qs.sh
source ~/upload/ubuntu_init/class/git.sh
source ~/upload/ubuntu_init/class/ipfs.sh
source ~/upload/ubuntu_init/class/loop.sh
source ~/upload/ubuntu_init/class/htmlq.sh
source ~/upload/ubuntu_init/class/tar.sh
source ~/upload/ubuntu_init/class/ansible.sh
source ~/upload/ubuntu_init/class/pelican.sh
source ~/upload/ubuntu_init/class/bug.sh
source ~/upload/ubuntu_init/class/notify.sh
source ~/upload/ubuntu_init/class/crack.sh
source ~/upload/ubuntu_init/class/beep.sh
source ~/upload/ubuntu_init/class/demo.sh
source ~/upload/ubuntu_init/class/http.sh
source ~/upload/ubuntu_init/class/raspberry.sh
source ~/upload/ubuntu_init/class/service.sh
source ~/upload/ubuntu_init/class/create.sh
source ~/upload/ubuntu_init/class/mysql.sh
source ~/upload/ubuntu_init/class/pip_git.sh
source ~/upload/ubuntu_init/class/log.sh
source ~/upload/ubuntu_init/class/net.sh
source ~/upload/ubuntu_init/class/pid.sh
source ~/upload/ubuntu_init/class/samba.sh
source ~/upload/ubuntu_init/class/mining.sh
source ~/upload/ubuntu_init/class/frp.sh
source ~/upload/ubuntu_init/class/hexo.sh
source ~/upload/ubuntu_init/class/acme.sh
source ~/upload/ubuntu_init/class/webssh.sh
source ~/upload/ubuntu_init/class/bpan.sh
source ~/upload/ubuntu_init/class/autopep8.sh
source ~/upload/ubuntu_init/class/ttyrec.sh
source ~/upload/ubuntu_init/class/gotty.sh
source ~/upload/ubuntu_init/class/hold_on.sh
source ~/upload/ubuntu_init/class/version.sh
source ~/upload/ubuntu_init/class/youtube-dl.sh
source ~/upload/ubuntu_init/class/socks.sh
source ~/upload/ubuntu_init/class/port.sh
source ~/upload/ubuntu_init/class/github.sh
source ~/upload/ubuntu_init/class/kcp.sh
source ~/upload/ubuntu_init/class/zignis.sh
source ~/upload/ubuntu_init/class/rclone.sh
source ~/upload/ubuntu_init/class/qrcp.sh
source ~/upload/ubuntu_init/class/pic.sh
source ~/upload/ubuntu_init/class/yh.sh
source ~/upload/ubuntu_init/class/service_power.sh
source ~/upload/ubuntu_init/class/mosh.sh
source ~/upload/ubuntu_init/class/ocr.sh
source ~/upload/ubuntu_init/class/adb.sh
source ~/upload/ubuntu_init/class/adb_py.sh
source ~/upload/ubuntu_init/class/video.sh
source ~/upload/ubuntu_init/class/encode.sh
source ~/upload/ubuntu_init/class/brew.sh
source ~/upload/ubuntu_init/class/rich.sh
source ~/upload/ubuntu_init/class/onedrive.sh
source ~/upload/ubuntu_init/class/rclone.sh
source ~/upload/ubuntu_init/class/stock.sh
source ~/upload/ubuntu_init/class/test.sh
source ~/upload/ubuntu_init/class/aws.sh
source ~/upload/ubuntu_init/class/aws-qs.sh
source ~/upload/ubuntu_init/class/aws-ec2.sh
source ~/upload/ubuntu_init/class/clickhouse.sh
source ~/upload/ubuntu_init/class/spark.sh
source ~/upload/ubuntu_init/class/kafka.sh
source ~/upload/ubuntu_init/class/flink.sh
source ~/upload/ubuntu_init/class/nc.sh
source ~/upload/ubuntu_init/class/chrome_aws.sh
source ~/upload/ubuntu_init/class/spider.sh
source ~/upload/ubuntu_init/class/maxwell.sh
source ~/upload/ubuntu_init/class/fake.sh
source ~/upload/ubuntu_init/class/rsync.sh
source ~/upload/ubuntu_init/class/dracula.sh
source ~/upload/ubuntu_init/class/cpu.sh
source ~/upload/ubuntu_init/class/memory.sh
source ~/upload/ubuntu_init/class/prom.sh
source ~/upload/ubuntu_init/class/wake_on_lan.sh
source ~/upload/ubuntu_init/class/eks.sh
source ~/upload/ubuntu_init/class/mc.sh
source ~/upload/ubuntu_init/class/rust.sh
source ~/upload/ubuntu_init/class/monetdb.sh
source ~/upload/ubuntu_init/class/jdbc.sh
source ~/upload/ubuntu_init/class/kompose.sh
source ~/upload/ubuntu_init/class/impala.sh
source ~/upload/ubuntu_init/class/juju.sh
source ~/upload/ubuntu_init/class/opensearch.sh
source ~/upload/ubuntu_init/class/benchmark.sh
source ~/upload/ubuntu_init/class/hadoop.sh
source ~/upload/ubuntu_init/class/tccli.sh
source ~/upload/ubuntu_init/class/ffmpeg.sh
source ~/upload/ubuntu_init/class/terraform.sh
source ~/upload/ubuntu_init/class/sdk.sh
source ~/upload/ubuntu_init/class/vim.sh
source ~/.sdkman/bin/sdkman-init.sh
source ~/upload/ubuntu_init/class/func/airflow-cli.sh
source ~/upload/ubuntu_init/class/node.sh # node.js func
source ~/upload/ubuntu_init/class/seatunnel.sh
source ~/upload/ubuntu_init/class/taskfile.sh # yaml执行任务工具
source ~/upload/ubuntu_init/class/redshift.sh # Redshift 客户端cli 安装以及benchamrk
source ~/upload/ubuntu_init/class/csvq.sh # csvq 
source ~/upload/ubuntu_init/class/zerotier.sh
# source ~/upload/ubuntu_init/class/closeai.sh
source ~/upload/ubuntu_init/class/openai.sh
source ~/upload/ubuntu_init/class/azure.sh
source ~/upload/ubuntu_init/class/ollama.sh
source ~/upload/ubuntu_init/class/aliyun.sh
source ~/upload/ubuntu_init/class/genai.sh
source ~/upload/ubuntu_init/class/md.sh
source ~/upload/ubuntu_init/class/pdf.sh
# source ~/upload/ubuntu_init/class/cuda.sh
# Add this to your .bashrc, .zshrc or equivalent.
# Run 'fff' with 'f' or whatever you decide to name the function.
f() {
  fff "$@"
  cd "$(cat "${XDG_CACHE_HOME:=${HOME}/.cache}/fff/.fff_d")"
}

if [ $(uname) = "Darwin" ]; then
  source ~/upload/ubuntu_init/class/mac.sh
  source ~/.env
elif [ $(uname) = "Linux" ]; then
  source ~/upload/ubuntu_init/class/linux.sh
else
  echo "$(uname) 13系统未知！4"
fi

field() {
  awk "{print \$${1:-1}}"
}

if [ $(uname) = "Linux" ] && [ $(file /bin/bash | field 7) = "ARM" ]; then
  # sed -i ‘’ '1d' $dirName/$targetName;
  source ~/upload/ubuntu_init/class/raspberry_init.sh &&
    source ~/upload/ubuntu_init/class/docker_arm.sh &&
    source ~/upload/ubuntu_init/class/k3sctl.sh
  export PATH=$PATH:~/.local/bin

elif [ $(uname -m) = "x86_64" ]; then
# echo "x64无需设置";
else
  # echo "$(uname) 系统未知！"

fi

[[ $(uname) = "Linux" ]] && bse
