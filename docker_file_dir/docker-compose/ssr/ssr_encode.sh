server=`curl -s ifconfig.me`
port=`/bin/cat /data/ssr/server/shadowsocks.json | jq ".server_port"`
protocol=`/bin/cat /data/ssr/server/shadowsocks.json | jq ".protocol" | sed  's/"//g' `
method=`/bin/cat /data/ssr/server/shadowsocks.json | jq ".method" | sed  's/"//g'`
obfs=`/bin/cat /data/ssr/server/shadowsocks.json | jq ".obfs"| sed  's/"//g'`
password=`/bin/cat /data/ssr/server/shadowsocks.json | jq ".password"| sed  's/"//g'`
# obfsparam=`/bin/cat /data/ssr/server/shadowsocks.json | jq ".obfs_param" | sed  's/"//g'`
# protoparam=`/bin/cat /data/ssr/server/shadowsocks.json | jq ".protocol_param" | sed  's/"//g'`
# remarks=`hostname`
# group="Neo"

password_base64=`echo $password | base64`
# obfsparam_base64=`echo $obfsparam | base64`
# protoparam_base64=`echo $protoparam | base64`
# remarks_base64=`echo $remarks | base64`
# group_base64=`echo $group | base64`


msg="$server:$port:$protocol:$method:$obfs:$password_base64"
original_link="ssr://$msg"
ssr_link="ssr://`echo $msg | base64`"
subscribe_msg=`echo $ssr_link | base64`


echo $msg
echo $original_link
echo $ssr_link
echo $subscribe_msg

# 有问题 ❌