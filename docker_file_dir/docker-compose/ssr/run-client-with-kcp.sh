

# 判断当前ip在境外则搭建server
# ip为国内
# 再判断系统
# 使用arm还是x86 client
# 在判断环境，
# home 使用自己的 SSR
# company 使用公司的 SSR 


# 文件检查 
# 第一个参数 目标文件路径
# 第二个参数 目标文件不存在需要的源文件路径
check_file(){
TARGET=${1:-/data/spug/spug_api/db.sqlite3}
SOURCE=${2:-`echo ~`/upload/ubuntu_init/docker_file_dir/docker-compose/spug/db.sqlite3}
TARGET_PATH=$(dirname ${1:-/data/spug/spug_api/db.sqlite3})

[ -f $TARGET ] && \
echo "`date +'%Y-%m-%d %H:%M:%S'` $TARGET exist ✅" || \
( echo "`date +'%Y-%m-%d %H:%M:%S'` $TARGET is not exist ❌" && \
sudo mkdir -p $TARGET_PATH && \
sudo chmod 777 $TARGET_PATH && \
cp $SOURCE $TARGET && \
sudo chmod 777 $TARGET && \
echo "`date +'%Y-%m-%d %H:%M:%S'` $TARGET is moved completed ✅" )

# [ -f ${1:-/data/spug/spug_api/db.sqlite3} ] && \
# echo "`date +'%Y-%m-%d %H:%M:%S'` ${1:-/data/spug/spug_api/db.sqlite3} exist ✅" || \
# ( echo "`date +'%Y-%m-%d %H:%M:%S'` ${1:-/data/spug/spug_api/db.sqlite3} is not exist ❌" && \
# sudo mkdir -p $(dirname ${1:-/data/spug/spug_api/db.sqlite3})   && \
# sudo chmod 777 $(dirname ${1:-/data/spug/spug_api/db.sqlite3})   && \
# cp ${2:-~/upload/ubuntu_init/docker_file_dir/docker-compose/spug/db.sqlite3} ${1:-/data/spug/spug_api/db.sqlite3} && \
# sudo chmod 777  ${1:-/data/spug/spug_api/db.sqlite3} && \
# echo "`date +'%Y-%m-%d %H:%M:%S'` ${1:-/data/spug/spug_api/db.sqlite3} is moved completed ✅" )
}


restart_compose(){
YAML=${1:-`echo ~`/upload/ubuntu_init/docker_file_dir/docker-compose/spug/x86-spug.yml}

sudo docker-compose -f $YAML stop && \
sudo docker-compose -f $YAML rm -f && \
sudo docker-compose -f $YAML up
}


restart_compose_d(){
YAML=${1:-`echo ~`/upload/ubuntu_init/docker_file_dir/docker-compose/spug/x86-spug.yml}

sudo docker-compose -f $YAML stop && \
sudo docker-compose -f $YAML rm -f && \
sudo docker-compose -f $YAML up -d
}


# user 执行
sudo mkdir -p /data/logs/kcp/client && \
sudo mkdir -p /data/kcptun/client && \
sudo chmod -R 777 /data/kcptun && \
sudo chmod -R 777 /data/logs && \
[[ -e /data/logs/kcp/client/kcp-client.log ]] && echo "/data/logs/kcp/client/kcp-client.log 存在 ✅" || sudo touch /data/logs/kcp/client/kcp-client.log 

check_file /data/kcptun/client/kcp-client-config.json `echo ~`/upload/ubuntu_init/docker_file_dir/docker-compose/ssr/kcp-client-config.json

echo "局域网永久在线主机部署";
echo "阿里云 kcp-server.* .* 指向 海外服务器"
# echo "需要提前更新好ssr和kcp配置文件";
# echo "需要根据本地的kcp接口（55555？）和本地ip（192.168.2.88？）组合为ssr链接（在小飞机中生成），进而 base64  生成订阅地址给ssr-client用";
echo "/data/kcptun/server/kcp-client-config.json"
echo "这个修改局域网ip，关闭🔐 因为加密在ssr已经做了 可忽略";
echo "若还未配置请立即退出";
sleep 3;

field(){
  awk "{print \$${1:-1}}"
}

if [ `file /bin/bash | field 7` = "ARM" ] && [ `uname` = "Linux" ]; then
  echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为arm Linux"
  restart_compose_d `echo ~`/upload/ubuntu_init/docker_file_dir/docker-compose/ssr/arm-ssr-client-with-kcp.yml

elif [ `uname -m` = "x86_64" ] && [ `uname` = "Linux" ]; then
  echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为X86 Linux"
  restart_compose_d `echo ~`/upload/ubuntu_init/docker_file_dir/docker-compose/ssr/x86-ssr-client-with-kcp.yml

elif [ `uname -m` = "x86_64" ] && [ `uname` = "Darwin" ]; then
  echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为X86 Mac" 
  echo "`date +'%Y-%m-%d %H:%M:%S'` 没有开发 ⌛️"

else
  echo "`uname` 系统未知！"
fi

# 发送钉钉 ssr

