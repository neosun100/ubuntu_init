# 文件检查
# 第一个参数 目标文件路径
# 第二个参数 目标文件不存在需要的源文件路径
check_file() {
  TARGET=${1:-/data/spug/spug_api/db.sqlite3}
  SOURCE=${2:-$(echo ~)/upload/ubuntu_init/docker_file_dir/docker-compose/spug/db.sqlite3}
  TARGET_PATH=$(dirname ${1:-/data/spug/spug_api/db.sqlite3})

  [ -f $TARGET ] &&
    echo "$(date +'%Y-%m-%d %H:%M:%S') $TARGET exist ✅" ||
    (echo "$(date +'%Y-%m-%d %H:%M:%S') $TARGET is not exist ❌" &&
      sudo mkdir -p $TARGET_PATH &&
      sudo chmod 777 $TARGET_PATH &&
      cp $SOURCE $TARGET &&
      sudo chmod 777 $TARGET &&
      echo "$(date +'%Y-%m-%d %H:%M:%S') $TARGET is moved completed ✅")

  # [ -f ${1:-/data/spug/spug_api/db.sqlite3} ] && \
  # echo "`date +'%Y-%m-%d %H:%M:%S'` ${1:-/data/spug/spug_api/db.sqlite3} exist ✅" || \
  # ( echo "`date +'%Y-%m-%d %H:%M:%S'` ${1:-/data/spug/spug_api/db.sqlite3} is not exist ❌" && \
  # sudo mkdir -p $(dirname ${1:-/data/spug/spug_api/db.sqlite3})   && \
  # sudo chmod 777 $(dirname ${1:-/data/spug/spug_api/db.sqlite3})   && \
  # cp ${2:-~/upload/ubuntu_init/docker_file_dir/docker-compose/spug/db.sqlite3} ${1:-/data/spug/spug_api/db.sqlite3} && \
  # sudo chmod 777  ${1:-/data/spug/spug_api/db.sqlite3} && \
  # echo "`date +'%Y-%m-%d %H:%M:%S'` ${1:-/data/spug/spug_api/db.sqlite3} is moved completed ✅" )
}

restart_compose() {
  YAML=${1:-$(echo ~)/upload/ubuntu_init/docker_file_dir/docker-compose/spug/x86-spug.yml}

  sudo docker-compose -f $YAML stop &&
    sudo docker-compose -f $YAML rm -f &&
    sudo docker-compose -f $YAML up
}

restart_compose_d() {
  YAML=${1:-$(echo ~)/upload/ubuntu_init/docker_file_dir/docker-compose/spug/x86-spug.yml}

  sudo docker-compose -f $YAML stop &&
    sudo docker-compose -f $YAML rm -f &&
    sudo docker-compose -f $YAML up -d
}

time_wait() {
    NUM=${1:-5}
    for i in $(seq $NUM -1 1); do
        echo "$i 秒后执行" 
        sleep 1
    done
}




check_file /data/spug/spug_api/db.sqlite3 ~/upload/ubuntu_init/docker_file_dir/docker-compose/spug/db.sqlite3

if [ $(uname -m) = "armv7l" ] && [ $(uname) = "Linux" ]; then
  echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为arm Linux"
  restart_compose_d $(echo ~)/upload/ubuntu_init/docker_file_dir/docker-compose/spug/arm-spug.yml

elif [ $(uname -m) = "x86_64" ] && [ $(uname) = "Linux" ]; then
  echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为X86 Linux"
  restart_compose_d $(echo ~)/upload/ubuntu_init/docker_file_dir/docker-compose/spug/x86-spug.yml

  time_wait 10
  sudo docker exec spug init_spug neo AWS2themoon
  sudo docker restart spug

elif [ $(uname -m) = "x86_64" ] && [ $(uname) = "Darwin" ]; then
  echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为X86 Mac"
  echo "$(date +'%Y-%m-%d %H:%M:%S') 没有开发 ⌛️"

else
  echo "$(uname) 系统未知！"
fi
