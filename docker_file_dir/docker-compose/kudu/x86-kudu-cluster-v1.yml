# Licensed to the Apache Software Foundation (ASF) under one
# or more contributor license agreements.  See the NOTICE file
# distributed with this work for additional information
# regarding copyright ownership.  The ASF licenses this file
# to you under the Apache License, Version 2.0 (the
# "License"); you may not use this file except in compliance
# with the License.  You may obtain a copy of the License at
#
#   http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing,
# software distributed under the License is distributed on an
# "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
# KIND, either express or implied.  See the License for the
# specific language governing permissions and limitations
# under the License.
version: "3"
services:
  kudu-master-1:
    image: apache/kudu:${KUDU_QUICKSTART_VERSION:-latest}
    container_name: kudu-master-1
    ports:
      - "0.0.0.0:7051:7051"
      - "0.0.0.0:8051:8051"
    command: ["master"]
    volumes:
      - /etc/localtime:/etc/localtime:ro
      - ${KUDU_DIR}/kudu-master-1:/var/lib/kudu
    environment:
      - KUDU_MASTERS=kudu-master-1:7051,kudu-master-2:7151,kudu-master-3:7251
      # TODO: Use `host.docker.internal` instead of KUDU_QUICKSTART_IP when it
      # works on Linux (https://github.com/docker/for-linux/issues/264)
      - >
        MASTER_ARGS=--fs_wal_dir=/var/lib/kudu/master
        --rpc_bind_addresses=0.0.0.0:7051
        --rpc_advertised_addresses=${KUDU_QUICKSTART_IP}:7051
        --webserver_port=8051
        --webserver_advertised_addresses=${KUDU_QUICKSTART_IP}:8051
        --webserver_doc_root=/opt/kudu/www
        --stderrthreshold=0
        --use_hybrid_clock=false
        --memory_limit_hard_bytes=1073741824
    networks:
      - quickstart-network
  kudu-master-2:
    image: apache/kudu:${KUDU_QUICKSTART_VERSION:-latest}
    container_name: kudu-master-2
    ports:
      - "7151:7151"
      - "8151:8151"
    command: ["master"]
    volumes:
      - /etc/localtime:/etc/localtime:ro
      - ${KUDU_DIR}/kudu-master-2:/var/lib/kudu
    environment:
      - KUDU_MASTERS=kudu-master-1:7051,kudu-master-2:7151,kudu-master-3:7251
      - >
        MASTER_ARGS=--fs_wal_dir=/var/lib/kudu/master
        --rpc_bind_addresses=0.0.0.0:7151
        --rpc_advertised_addresses=${KUDU_QUICKSTART_IP}:7151
        --webserver_port=8151
        --webserver_advertised_addresses=${KUDU_QUICKSTART_IP}:8151
        --webserver_doc_root=/opt/kudu/www
        --stderrthreshold=0
        --use_hybrid_clock=false
        --memory_limit_hard_bytes=1073741824
    networks:
      - quickstart-network
  kudu-master-3:
    image: apache/kudu:${KUDU_QUICKSTART_VERSION:-latest}
    container_name: kudu-master-3
    ports:
      - "7251:7251"
      - "8251:8251"
    command: ["master"]
    volumes:
      - /etc/localtime:/etc/localtime:ro
      - ${KUDU_DIR}/kudu-master-3:/var/lib/kudu
    environment:
      - KUDU_MASTERS=kudu-master-1:7051,kudu-master-2:7151,kudu-master-3:7251
      - >
        MASTER_ARGS=--fs_wal_dir=/var/lib/kudu/master
        --rpc_bind_addresses=0.0.0.0:7251
        --rpc_advertised_addresses=${KUDU_QUICKSTART_IP}:7251
        --webserver_port=8251
        --webserver_advertised_addresses=${KUDU_QUICKSTART_IP}:8251
        --webserver_doc_root=/opt/kudu/www
        --stderrthreshold=0
        --use_hybrid_clock=false
        --memory_limit_hard_bytes=1073741824
    networks:
      - quickstart-network
  kudu-tserver-1:
    image: apache/kudu:${KUDU_QUICKSTART_VERSION:-latest}
    container_name: kudu-tserver-1
    depends_on:
      - kudu-master-1
      - kudu-master-2
      - kudu-master-3
    ports:
      - "7050:7050"
      - "8050:8050"
    command: ["tserver"]
    volumes:
      - /etc/localtime:/etc/localtime:ro
      - ${KUDU_DIR}/kudu-tserver-1:/var/lib/kudu
    environment:
      - KUDU_MASTERS=kudu-master-1:7051,kudu-master-2:7151,kudu-master-3:7251
      - >
        TSERVER_ARGS=--fs_wal_dir=/var/lib/kudu/tserver
        --rpc_bind_addresses=0.0.0.0:7050
        --rpc_advertised_addresses=${KUDU_QUICKSTART_IP}:7050
        --webserver_port=8050
        --webserver_advertised_addresses=${KUDU_QUICKSTART_IP}:8050
        --webserver_doc_root=/opt/kudu/www
        --stderrthreshold=0
        --use_hybrid_clock=false
        --memory_limit_hard_bytes=1073741824
    networks:
      - quickstart-network
  kudu-tserver-2:
    image: apache/kudu:${KUDU_QUICKSTART_VERSION:-latest}
    container_name: kudu-tserver-2
    depends_on:
      - kudu-master-1
      - kudu-master-2
      - kudu-master-3
    ports:
      - "7150:7150"
      - "8150:8150"
    command: ["tserver"]
    volumes:
      - /etc/localtime:/etc/localtime:ro
      - ${KUDU_DIR}/kudu-tserver-2:/var/lib/kudu
    environment:
      - KUDU_MASTERS=kudu-master-1:7051,kudu-master-2:7151,kudu-master-3:7251
      - >
        TSERVER_ARGS=--fs_wal_dir=/var/lib/kudu/tserver
        --rpc_bind_addresses=0.0.0.0:7150
        --rpc_advertised_addresses=${KUDU_QUICKSTART_IP}:7150
        --webserver_port=8150
        --webserver_advertised_addresses=${KUDU_QUICKSTART_IP}:8150
        --webserver_doc_root=/opt/kudu/www
        --stderrthreshold=0
        --use_hybrid_clock=false
        --memory_limit_hard_bytes=1073741824
    networks:
      - quickstart-network
  kudu-tserver-3:
    image: apache/kudu:${KUDU_QUICKSTART_VERSION:-latest}
    container_name: kudu-tserver-3
    depends_on:
      - kudu-master-1
      - kudu-master-2
      - kudu-master-3
    ports:
      - "7250:7250"
      - "8250:8250"
    command: ["tserver"]
    volumes:
      - /etc/localtime:/etc/localtime:ro
      - ${KUDU_DIR}/kudu-tserver-3:/var/lib/kudu
    environment:
      - KUDU_MASTERS=kudu-master-1:7051,kudu-master-2:7151,kudu-master-3:7251
      - >
        TSERVER_ARGS=--fs_wal_dir=/var/lib/kudu/tserver
        --rpc_bind_addresses=0.0.0.0:7250
        --rpc_advertised_addresses=${KUDU_QUICKSTART_IP}:7250
        --webserver_port=8250
        --webserver_advertised_addresses=${KUDU_QUICKSTART_IP}:8250
        --webserver_doc_root=/opt/kudu/www
        --stderrthreshold=0
        --use_hybrid_clock=false
        --memory_limit_hard_bytes=1073741824
    networks:
      - quickstart-network
  kudu-tserver-4:
    image: apache/kudu:${KUDU_QUICKSTART_VERSION:-latest}
    container_name: kudu-tserver-4
    depends_on:
      - kudu-master-1
      - kudu-master-2
      - kudu-master-3
    ports:
      - "7350:7350"
      - "8350:8350"
    command: ["tserver"]
    volumes:
      - /etc/localtime:/etc/localtime:ro
      - ${KUDU_DIR}/kudu-tserver-4:/var/lib/kudu
    environment:
      - KUDU_MASTERS=kudu-master-1:7051,kudu-master-2:7151,kudu-master-3:7251
      - >
        TSERVER_ARGS=--fs_wal_dir=/var/lib/kudu/tserver
        --rpc_bind_addresses=0.0.0.0:7350
        --rpc_advertised_addresses=${KUDU_QUICKSTART_IP}:7350
        --webserver_port=8350
        --webserver_advertised_addresses=${KUDU_QUICKSTART_IP}:8350
        --webserver_doc_root=/opt/kudu/www
        --stderrthreshold=0
        --use_hybrid_clock=false
        --memory_limit_hard_bytes=1073741824
    networks:
      - quickstart-network
  kudu-tserver-5:
    image: apache/kudu:${KUDU_QUICKSTART_VERSION:-latest}
    container_name: kudu-tserver-5
    depends_on:
      - kudu-master-1
      - kudu-master-2
      - kudu-master-3
    ports:
      - "7450:7450"
      - "8450:8450"
    command: ["tserver"]
    volumes:
      - /etc/localtime:/etc/localtime:ro
      - ${KUDU_DIR}/kudu-tserver-5:/var/lib/kudu
    environment:
      - KUDU_MASTERS=kudu-master-1:7051,kudu-master-2:7151,kudu-master-3:7251
      - >
        TSERVER_ARGS=--fs_wal_dir=/var/lib/kudu/tserver
        --rpc_bind_addresses=0.0.0.0:7450
        --rpc_advertised_addresses=${KUDU_QUICKSTART_IP}:7450
        --webserver_port=8450
        --webserver_advertised_addresses=${KUDU_QUICKSTART_IP}:8450
        --webserver_doc_root=/opt/kudu/www
        --stderrthreshold=0
        --use_hybrid_clock=false
        --memory_limit_hard_bytes=1073741824
    networks:
      - quickstart-network

# volumes:
#   kudu-master-1:
#   kudu-master-2:
#   kudu-master-3:
#   kudu-tserver-1:
#   kudu-tserver-2:
#   kudu-tserver-3:
#   kudu-tserver-4:
#   kudu-tserver-5:


  nginx-tserver:
    image: nginx:1.19.2-alpine
    volumes:
      - /etc/localtime:/etc/localtime:ro
      - ./conf/nginx.conf:/etc/nginx/nginx.conf:ro
      - ${KUDU_DIR}/nginx-log:/var/log/nginx
    ports:
      - "8550:8550"
    depends_on:
      - kudu-tserver-1
      - kudu-tserver-2
      - kudu-tserver-3
      - kudu-tserver-4
      - kudu-tserver-5
    networks:
      - quickstart-network


  nginx-kudu-master:
    image: nginx:1.19.2-alpine
    volumes:
      - /etc/localtime:/etc/localtime:ro
      - ./conf/nginx-master.conf:/etc/nginx/nginx.conf:ro
      - ${KUDU_DIR}/nginx-master-log:/var/log/nginx
    ports:
      - "8351:8351"
    depends_on:
      - kudu-master-1
      - kudu-master-2
      - kudu-master-3
    networks:
      - quickstart-network


networks:
  quickstart-network:
    external: true