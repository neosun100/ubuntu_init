
# 文件检查 
# 第一个参数 目标文件路径
# 第二个参数 目标文件不存在需要的源文件路径
check_file(){
TARGET=${1:-/data/spug/spug_api/db.sqlite3}
SOURCE=${2:-`echo ~`/upload/ubuntu_init/docker_file_dir/docker-compose/spug/db.sqlite3}
TARGET_PATH=$(dirname ${1:-/data/spug/spug_api/db.sqlite3})
# echo $TARGET
# echo $SOURCE
# echo $TARGET_PATH
[ -f $TARGET ] && \
echo "`date +'%Y-%m-%d %H:%M:%S'` $TARGET exist ✅" || \
( echo "`date +'%Y-%m-%d %H:%M:%S'` $TARGET is not exist ❌" && \
sudo mkdir -p $TARGET_PATH && \
sudo chmod 777 $TARGET_PATH && \
cp $SOURCE $TARGET && \
sudo chmod 777 $TARGET && \
echo "`date +'%Y-%m-%d %H:%M:%S'` $TARGET is moved completed ✅" )
}

restart_compose(){
YAML=${1:-`echo ~`/upload/ubuntu_init/docker_file_dir/docker-compose/spug/x86-spug.yml}

sudo docker-compose -f $YAML stop && \
sudo docker-compose -f $YAML rm -f && \
sudo docker-compose -f $YAML up
}


restart_compose_d(){
YAML=${1:-`echo ~`/upload/ubuntu_init/docker_file_dir/docker-compose/spug/x86-spug.yml}

sudo docker-compose -f $YAML stop && \
sudo docker-compose -f $YAML rm -f && \
sudo docker-compose -f $YAML up -d
}


check_file /data/futuopend/FutuOpenD.xml `echo ~`/upload/ubuntu_init/docker_file_dir/docker-compose/futuOpenD/FutuOpenD.html
check_file /data/futuopend/FutuOpenD-song.xml `echo ~`/upload/ubuntu_init/docker_file_dir/docker-compose/futuOpenD/FutuOpenD-song.html
check_file /data/futuopend/FutuOpenD-gum.xml `echo ~`/upload/ubuntu_init/docker_file_dir/docker-compose/futuOpenD/FutuOpenD-gum.html
check_file /data/futuopend/FutuOpenD-yang.xml `echo ~`/upload/ubuntu_init/docker_file_dir/docker-compose/futuOpenD/FutuOpenD-yang.html
# check_file /data/futuopend/nginx.conf `echo ~`/upload/ubuntu_init/docker_file_dir/docker-compose/futuOpenD/nginx.conf

mkdir -p /data/futuopend/haproxy;
check_file /data/futuopend/haproxy/haproxy.cfg `echo ~`/upload/ubuntu_init/docker_file_dir/docker-compose/futuOpenD/haproxy.cfg
check_file /data/futuopend/haproxy/haproxy.pid `echo ~`/upload/ubuntu_init/docker_file_dir/docker-compose/futuOpenD/haproxy.pid


# mkdir -p /data/logs/nginx;
sudo chmod -R 777  /data/futuopend;



field(){
  awk "{print \$${1:-1}}"
}

if [ `file /bin/bash | field 7` = "ARM" ] && [ `uname` = "Linux" ]; then
  echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为arm Linux"
  echo "`date +'%Y-%m-%d %H:%M:%S'` 没有开发 ⌛️"

elif [ `uname -m` = "x86_64" ] && [ `uname` = "Linux" ]; then
  echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为X86 Linux"
  restart_compose_d `echo ~`/upload/ubuntu_init/docker_file_dir/docker-compose/futuOpenD/x86-futuOpenD-blance.yml
  echo "input_phone_verify_code -code=123456"
  sleep 10;
  sudo docker attach futu-neo;
  sudo docker attach futu-gum;
  sudo docker attach futu-song;
  sudo docker attach futu-yang;


elif [ `uname -m` = "x86_64" ] && [ `uname` = "Darwin" ]; then
  echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为X86 Mac" 
  echo "`date +'%Y-%m-%d %H:%M:%S'` 没有开发 ⌛️"

else
  echo "`uname` 系统未知！"
fi


