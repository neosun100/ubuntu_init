# 文件检查
# 第一个参数 目标文件路径
# 第二个参数 目标文件不存在需要的源文件路径
check_file() {
    TARGET=${1:-/data/spug/spug_api/db.sqlite3}
    SOURCE=${2:-$(echo ~)/upload/ubuntu_init/docker_file_dir/docker-compose/spug/db.sqlite3}
    TARGET_PATH=$(dirname ${1:-/data/spug/spug_api/db.sqlite3})

    [ -f $TARGET ] &&
        echo "$(date +'%Y-%m-%d %H:%M:%S') $TARGET exist ✅" ||
        (echo "$(date +'%Y-%m-%d %H:%M:%S') $TARGET is not exist ❌" &&
            sudo mkdir -p $TARGET_PATH &&
            sudo chmod -R 777 $TARGET_PATH &&
            cp $SOURCE $TARGET &&
            sudo chmod -R 777 $TARGET &&
            echo "$(date +'%Y-%m-%d %H:%M:%S') $TARGET is moved completed ✅")
}

restart_compose() {
    YAML=${1:-$(echo ~)/upload/ubuntu_init/docker_file_dir/docker-compose/spug/x86-spug.yml}

    sudo docker-compose -f $YAML stop &&
        sudo docker-compose -f $YAML rm -f &&
        sudo docker-compose -f $YAML up
}

restart_compose_d() {
    YAML=${1:-$(echo ~)/upload/ubuntu_init/docker_file_dir/docker-compose/spug/x86-spug.yml}

    sudo docker-compose -f $YAML stop &&
        sudo docker-compose -f $YAML rm -f &&
        sudo docker-compose -f $YAML up -d
}

sudo mkdir -p /data/monetdb
sudo chmod -R 777 /data/monetdb

field() {
    awk "{print \$${1:-1}}"
}



install_monetdb_client_latest() {
    mkdir -p ~/upload/bin/monetdb
    cd ~/upload/bin/monetdb
    wget https://www.monetdb.org/downloads/Java/jdbcclient.jre8.jar -O jdbcclient.jre8.jar
    java -jar ~/upload/bin/monetdb/jdbcclient.jre8.jar --help
    cd ~/upload
}


if [ $(file /bin/bash | field 7) = "ARM" ] && [ $(uname) = "Linux" ]; then
    echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为arm Linux"
    echo "$(date +'%Y-%m-%d %H:%M:%S') 没有开发 ⌛️"

elif

    [ $(uname -m) = "x86_64" ] && [ $(uname) = "Linux" ]
then
    echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为X86 Linux"
    restart_compose_d $(echo ~)/upload/ubuntu_init/docker_file_dir/docker-compose/monetdb/x86-monetdb.yml
    echo "java -jar /Users/jiasunm/Downloads/jdbcclient.jre8.jar --host 192.168.100.142 --port 50000 --user monetdb  -d public "


elif
    [ $(uname -m) = "x86_64" ] && [ $(uname) = "Darwin" ]
then
    echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为X86 Mac"
    echo "$(date +'%Y-%m-%d %H:%M:%S') 没有开发 ⌛️"

else
    echo "$(uname) 系统未知！"
fi


echo "检察客户端"
 [[ -f ~/upload/bin/monetdb/jdbcclient.jre8.jar ]] && echo "已安装好 mclient " || ( echo "未安装客户段，需要进行客户端部署" &&  install_monetdb_client_latest )