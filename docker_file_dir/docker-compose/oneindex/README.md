# 特殊文件实现功能  
` README.md `、`HEAD.md` 、 `.password`特殊文件使用  

**在文件夹底部添加说明:**  
>在 OneDrive 的文件夹中添加` README.md `文件，使用 Markdown 语法。  

**加密文件夹:**  
>在 OneDrive 的文件夹中添加`.password`文件，填入密码，密码不能为空。  

**直接输出网页:**  
>在 OneDrive 的文件夹中添加`index.html` 文件，程序会直接输出网页而不列目录。  
>配合 文件展示设置-直接输出 效果更佳。  


**上传文件:**  

> 推荐使用系统自带的OneDrive程序客户端或者使用RaiDrive进行文件的修改、上传、删除操作。

enjoy it!
----

![荣光](https://uploadstatic.mihoyo.com/contentweb/20200304/2020030418573058495.png)
