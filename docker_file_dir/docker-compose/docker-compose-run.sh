# 🐝🐝🐝🐝🐝🐝🐝🐝🐝🐝🐝🐝🐝🐝🐝🐝🐝🐝🐝🐝🐝🐝🐝🐝
# 🐝🐝🐝🐝🐝🐝🐝🐝🐝     arm     🐝🐝🐝🐝🐝🐝🐝🐝🐝
# 🐝🐝🐝🐝🐝🐝🐝🐝🐝🐝🐝🐝🐝🐝🐝🐝🐝🐝🐝🐝🐝🐝🐝🐝
# mysql 单节点
create_dir /data/mysql && \
sudo docker-compose -f /home/neo/upload/ubuntu_init/docker_file_dir/docker-compose/arm-mysql.yml up -d && \
sudo docker-compose -f /home/neo/upload/ubuntu_init/docker_file_dir/docker-compose/arm-mysql.yml ps -a 



# mysql 主从
# 当重启主从之后，配置信息丢失。需要重新对master_log_file文件及master_log_pos进行配置。
# https://www.jianshu.com/p/40e8b7548f03
sudo docker-compose -f /home/neo/upload/ubuntu_init/docker_file_dir/docker-compose/arm-mysql-master-slave.yml rm && \
create_dir && \
sudo rm -rf /data/mysql_* && \
sudo docker-compose -f /home/neo/upload/ubuntu_init/docker_file_dir/docker-compose/arm-mysql-master-slave.yml up  && \
sudo docker-compose -f /home/neo/upload/ubuntu_init/docker_file_dir/docker-compose/arm-mysql-master-slave.yml ps -a && \
echo "🐛🐛🐛🐛🐛🐛🐛🐛🐛🐛🐛🐛🐛🐛🐛" && \
echo "mysql-master执行以下命令" && \
mycli mysql://root:LOOP2themoon@127.0.0.1:33306 \
-e "flush tables with read lock; " -t && \
mycli mysql://root:LOOP2themoon@127.0.0.1:33306 \
-e "show master status; " -t && \
mycli mysql://root:LOOP2themoon@127.0.0.1:33306 \
-e "grant replication slave on *.* to 'root'@'172.25.0.102' identified by 'LOOP2themoon'; " -t && \
mycli mysql://root:LOOP2themoon@127.0.0.1:33306 \
-e "grant replication slave on *.* to 'root'@'172.25.0.103' identified by 'LOOP2themoon'; " -t && \
mycli mysql://root:LOOP2themoon@127.0.0.1:33306 \
-e "grant replication slave on *.* to 'slave'@'%' identified by 'LOOP2themoon'; " -t && \
mycli mysql://root:LOOP2themoon@127.0.0.1:33306 \
-e "UNLOCK TABLE; " -t && \
echo "" && \
echo "" && \
echo "🐛🐛🐛🐛🐛🐛🐛🐛🐛🐛🐛🐛🐛🐛🐛" && \
mycli mysql://root:LOOP2themoon@127.0.0.1:33307 \
-e "show slave status; " -t && \
mycli mysql://root:LOOP2themoon@127.0.0.1:33307 \
-e "reset slave; " -t && \
mycli mysql://root:LOOP2themoon@127.0.0.1:33307 \
-e "change master to master_host='172.25.0.101',master_user='root',master_password='LOOP2themoon',master_log_file='mysql-bin.000006',master_log_pos=454; " -t && \
mycli mysql://root:LOOP2themoon@127.0.0.1:33307 \
-e "start slave; " -t && \
mycli mysql://root:LOOP2themoon@127.0.0.1:33307 \
-e "show slave status; " -t && \
echo "注意查看Slave_IO_Running Slave_SQL_Running 这两列必须为yes" && \
echo "" && \
echo "" && \
echo "🐛🐛🐛🐛🐛🐛🐛🐛🐛🐛🐛🐛🐛🐛🐛" && \
mycli mysql://root:LOOP2themoon@127.0.0.1:33308 \
-e "show slave status; " -t && \
mycli mysql://root:LOOP2themoon@127.0.0.1:33308 \
-e "reset slave; " -t && \
mycli mysql://root:LOOP2themoon@127.0.0.1:33308 \
-e "change master to master_host='172.25.0.101',master_user='root',master_password='LOOP2themoon',master_log_file='mysql-bin.000006',master_log_pos=454; " -t && \
mycli mysql://root:LOOP2themoon@127.0.0.1:33308 \
-e "start slave; " -t && \
mycli mysql://root:LOOP2themoon@127.0.0.1:33308 \
-e "show slave status; " -t && \
echo "注意查看Slave_IO_Running Slave_SQL_Running 这两列必须为yes" && \
echo "注意，当重启主从之后，配置信息丢失。需要重新对master_log_file文件及master_log_pos进行配置。"


# mysql 集群



# redis 集群 哨兵模式
# https://www.cnblogs.com/hckblogs/p/11186311.html
echo "/home/neo/upload/ubuntu_init/config/arm_redis_sentinel.conf" && \
echo "/home/neo/upload/ubuntu_init/docker_file_dir/docker-compose/arm-redis-cluster.yml" && \
echo "确认已经修改 以上 2个文件 中的 ip 地址" && \
sleep 10 && \
sudo docker-compose -f /home/neo/upload/ubuntu_init/docker_file_dir/docker-compose/arm-redis-cluster.yml rm && \
sudo rm -rf /data/redis* && \
create_dir /data/redis_sentinel && \
scat /home/neo/upload/ubuntu_init/config/arm_redis_sentinel.conf > /data/redis_sentinel/sentinel-{1,2,3}.conf && \
l /data/redis_sentinel && \
sudo docker-compose -f /home/neo/upload/ubuntu_init/docker_file_dir/docker-compose/arm-redis-cluster.yml up -d && \
sudo docker-compose -f /home/neo/upload/ubuntu_init/docker_file_dir/docker-compose/arm-redis-cluster.yml ps -a



# redis 高可用集群模式
# https://blog.csdn.net/x3499633/article/details/88637819



# 🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸
# 🐸🐸🐸🐸🐸🐸🐸🐸🐸     x86     🐸🐸🐸🐸🐸🐸🐸🐸🐸
# 🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸


# redis 集群
# https://www.cnblogs.com/hckblogs/p/11186311.html

echo "/home/neo/upload/ubuntu_init/config/redis_sentinel.conf" && \
echo "/home/neo/upload/ubuntu_init/docker_file_dir/docker-compose/x86-redis-cluster.yml" && \
echo "确认已经修改 以上 2个文件 中的 ip 地址" && \
sleep 10 && \
sudo docker-compose -f /home/neo/upload/ubuntu_init/docker_file_dir/docker-compose/x86-redis-cluster.yml rm && \
sudo rm -rf /data/redis* && \
create_dir /data/redis_sentinel && \
scat /home/neo/upload/ubuntu_init/config/redis_sentinel.conf > /data/redis_sentinel/sentinel-{1,2,3}.conf && \
l /data/redis_sentinel && \
sudo docker-compose -f /home/neo/upload/ubuntu_init/docker_file_dir/docker-compose/x86-redis-cluster.yml up -d && \
sudo docker-compose -f /home/neo/upload/ubuntu_init/docker_file_dir/docker-compose/x86-redis-cluster.yml ps -a







#######################
# blog
宿主机和docker环境2套hexo
# 宿主部署
sudo rm -rf  ~/upload/neoBlog;
npm install -g hexo-cli;
cd ~/upload;
hexo init neoBlog;

# next主题
git clone https://github.com/neosun100/hexo-theme-next ~/upload/neoBlog/themes/next && \
rm -rf ~/upload/neoBlog/themes/next/_config.yml && \
curl https://raw.githubusercontent.com/neosun100/hexo-theme-next/neosun100-patch-1/_config.yml -o ~/upload/neoBlog/themes/next/_config.yml && \
echo "https://github.com/neosun100/hexo-theme-next/blob/neosun100-patch-1/_config.yml";


# 配置 hexo and admin
cd ~/upload/neoBlog;
npm install --save hexo-admin;
mkdir ~/upload/neoBlog/admin_script;
touch ~/upload/neoBlog/admin_script/hexo-d.sh;
echo 'hexo g && hexo d' > ~/upload/neoBlog/admin_script/hexo-d.sh;
chmod +x ~/upload/neoBlog/admin_script/hexo-d.sh;
rm -rf ~/upload/neoBlog/_config.yml;
curl https://raw.githubusercontent.com/neosun100/hexo-theme-next/neosun100-patch-2/hexo_config.yml -o ~/upload/neoBlog/_config.yml;
echo "https://github.com/neosun100/hexo-theme-next/blob/neosun100-patch-2/hexo_config.yml"

# $font-size-base           = 14px
# $font-size-base           = 18px



# $font-size-headings-base    = 24px
# $font-size-headings-base    = 30px
# 字符串替换
# 增加字体
sed 's/$font-size-base           = 14px/$font-size-base           = 18px/'  ~/upload/neoBlog/themes/next/source/css/_variables/base.styl;
sed 's/$font-size-headings-base    = 24px/$font-size-headings-base    = 30px/'  ~/upload/neoBlog/themes/next/source/css/_variables/base.styl;



# 同步md
source下面的所有文件
走 gitlab cicd
# 略


 hexo clear && hexo g && hexo d && hexo server -i 0.0.0.0 -p 4000

cd ~/upload/neoBlog;
hexo clear && hexo g && hexo d;
nohup hexo server -i 0.0.0.0 -p 4000 > blog.log 2>&1 &;




# docker下
# 和nginx放一起了
sudo docker-compose -f /home/neo/upload/ubuntu_init/docker_file_dir/docker-compose/x86-nginx.yml up -d



# 启动 frpc
cd ~/upload && \
wget https://github.com/fatedier/frp/releases/download/v0.30.0/frp_0.30.0_linux_amd64.tar.gz && \
tar zxvf frp_0.30.0_linux_amd64.tar.gz && \
mkdir ~/upload/frp;
mv frp_*_linux_amd64 frp && \
nohup  ~/upload/frp/frpc -c ~/upload/frp/frpc.ini > ~/upload/frp/frpc.log 2>&1 &


sudo docker-compose -f /home/neo/upload/ubuntu_init/docker_file_dir/docker-compose/x86-neo_pub.yml stop && \
sudo docker-compose -f /home/neo/upload/ubuntu_init/docker_file_dir/docker-compose/x86-neo_pub.yml rm -f && \
sudo docker-compose -f /home/neo/upload/ubuntu_init/docker_file_dir/docker-compose/x86-neo_pub.yml up -d