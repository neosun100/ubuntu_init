

# 文件检查
# 第一个参数 目标文件路径
# 第二个参数 目标文件不存在需要的源文件路径
check_file() {
    TARGET=${1:-/data/spug/spug_api/db.sqlite3}
    SOURCE=${2:-$(echo ~)/upload/ubuntu_init/docker_file_dir/docker-compose/spug/db.sqlite3}
    TARGET_PATH=$(dirname ${1:-/data/spug/spug_api/db.sqlite3})

    [ -f $TARGET ] &&
        echo "$(date +'%Y-%m-%d %H:%M:%S') $TARGET exist ✅" ||
        (echo "$(date +'%Y-%m-%d %H:%M:%S') $TARGET is not exist ❌" &&
            sudo mkdir -p $TARGET_PATH &&
            sudo chmod 777 $TARGET_PATH &&
            cp $SOURCE $TARGET &&
            sudo chmod 777 $TARGET &&
            echo "$(date +'%Y-%m-%d %H:%M:%S') $TARGET is moved completed ✅")

    # [ -f ${1:-/data/spug/spug_api/db.sqlite3} ] && \
    # echo "`date +'%Y-%m-%d %H:%M:%S'` ${1:-/data/spug/spug_api/db.sqlite3} exist ✅" || \
    # ( echo "`date +'%Y-%m-%d %H:%M:%S'` ${1:-/data/spug/spug_api/db.sqlite3} is not exist ❌" && \
    # sudo mkdir -p $(dirname ${1:-/data/spug/spug_api/db.sqlite3})   && \
    # sudo chmod 777 $(dirname ${1:-/data/spug/spug_api/db.sqlite3})   && \
    # cp ${2:-~/upload/ubuntu_init/docker_file_dir/docker-compose/spug/db.sqlite3} ${1:-/data/spug/spug_api/db.sqlite3} && \
    # sudo chmod 777  ${1:-/data/spug/spug_api/db.sqlite3} && \
    # echo "`date +'%Y-%m-%d %H:%M:%S'` ${1:-/data/spug/spug_api/db.sqlite3} is moved completed ✅" )
}

restart_compose() {
    YAML=${1:-$(echo ~)/upload/ubuntu_init/docker_file_dir/docker-compose/spug/x86-spug.yml}

    sudo docker-compose -f $YAML stop &&
        sudo docker-compose -f $YAML rm -f &&
        sudo docker-compose -f $YAML up
}

restart_compose_d() {
    YAML=${1:-$(echo ~)/upload/ubuntu_init/docker_file_dir/docker-compose/spug/x86-spug.yml}

    sudo docker-compose -f $YAML stop &&
        sudo docker-compose -f $YAML rm -f &&
        sudo docker-compose -f $YAML up -d
}


iip(){
# if [ -e /sbin/ifconfig ];then
#   #ifconfig | egrep "192.168|10.100|172.27|172.30" | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | sed -n "1,1p"
#   ifconfig | egrep "192.168|10.100|172.27|172.30" | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | head -n 1
# else
#   #ip addr | egrep "192.168|10.100|172.27|172.30" | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | sed -n "1,1p"
#   ip addr | egrep "192.168|10.100|172.27|172.30" | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | head -n 1
# fi


# [[ `uname` = "Darwin" ]] && \
# ifconfig | egrep "192.168|10.100|172.27|172.30" | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | head -n 1 || \
# ip addr |grep inet |grep -v inet6 | grep -v 127.0.0.1 | field 2 | line 1 1 | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}"

python3 -c "import socket;print([(s.connect(('8.8.8.8', 53)), s.getsockname()[0], s.close()) for s in [socket.socket(socket.AF_INET, socket.SOCK_DGRAM)]][0][1])"
}


if [ $(uname -m) = "armv7l" ] && [ $(uname) = "Linux" ]; then
    echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为arm Linux"
    echo "$(date +'%Y-%m-%d %H:%M:%S') 没有开发 ⌛️"

elif [ $(uname -m) = "x86_64" ] && [ $(uname) = "Linux" ]; then
    echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为X86 Linux"
    echo "非Archon机器需要修改run.sh和.env"

    sudo mkdir -p /data/opensearch/{data1,data2} && sudo chmod -R 777  /data/opensearch


    restart_compose_d $(echo ~)/upload/ubuntu_init/docker_file_dir/docker-compose/opensearch/x86-opensearch-cluster-v1.yml
                     # /home/neo/upload/ubuntu_init/docker_file_dir/docker-compose/opensearch/x86-opensearh-cluster-v1.yml

    echo "`iip`:59200"
    echo "`iip`:59600"
    echo "Login with the default username (admin) and password (admin)"

elif [ $(uname -m) = "x86_64" ] && [ $(uname) = "Darwin" ]; then
    echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为X86 Mac"
    echo "$(date +'%Y-%m-%d %H:%M:%S') 没有开发 ⌛️"

else
    echo "$(uname) 系统未知！"
fi






