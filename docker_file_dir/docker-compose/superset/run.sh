
setup_compose(){ 
echo y | sudo apt-get install python3-pip;  
sudo pip3 install docker-compose;  
}

restart_compose(){
YAML=${1:-`echo ~`/upload/ubuntu_init/docker_file_dir/docker-compose/x86-neo_pub.yml}

sudo docker-compose -f $YAML stop && \
sudo docker-compose -f $YAML rm -f && \
sudo docker-compose -f $YAML up
}


restart_compose_d(){
YAML=${1:-`echo ~`/upload/ubuntu_init/docker_file_dir/docker-compose/x86-neo_pub.yml}

sudo docker-compose -f $YAML stop && \
sudo docker-compose -f $YAML rm -f && \
sudo docker-compose -f $YAML up -d
}



alias grepip='grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}"'


# 🍎 查看局域网ip,            默认|比格云|腾讯云|qq云
iip(){
# if [ -e /sbin/ifconfig ];then
#   #ifconfig | egrep "192.168|10.100|172.27|172.30" | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | sed -n "1,1p"
#   ifconfig | egrep "192.168|10.100|172.27|172.30" | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | head -n 1
# else
#   #ip addr | egrep "192.168|10.100|172.27|172.30" | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | sed -n "1,1p"
#   ip addr | egrep "192.168|10.100|172.27|172.30" | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | head -n 1
# fi


# [[ `uname` = "Darwin" ]] && \
# ifconfig | egrep "192.168|10.100|172.27|172.30" | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | head -n 1 || \
# ip addr |grep inet |grep -v inet6 | grep -v 127.0.0.1 | field 2 | line 1 1 | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}"

python3 -c "import socket;print([(s.connect(('8.8.8.8', 53)), s.getsockname()[0], s.close()) for s in [socket.socket(socket.AF_INET, socket.SOCK_DGRAM)]][0][1])"
}




wip(){
# curl -s "https://www.taobao.com/help/getip.php" | grepip
curl -s https://www.taobao.com/help/getip.php | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}"
}







deploy_superset(){
# setup_compose
cd ~
pwd
echo "https://superset.apache.org/docs/databases/installing-database-drivers"
# tsocks git clone https://github.com/apache/incubator-superset.git
# git clone https://hub.fastgit.org/apache/incubator-superset 
git clone https://gitee.com/mirrors/Superset
cd incubator-superset
rm -rf ./docker/requirements-local.txt && \
touch ./docker/requirements-local.txt && \
echo "sqlalchemy-clickhouse" >> ./docker/requirements-local.txt && \
echo "PyAthena" >> ./docker/requirements-local.txt && \
echo "sqlalchemy-redshift" >> ./docker/requirements-local.txt && \
#echo "sqlalchemy-drill" >> ./docker/requirements-local.txt
echo "pydruid" >> ./docker/requirements-local.txt && \
echo "pyhive" >> ./docker/requirements-local.txt && \
#echo "impala" >> ./docker/requirements-local.txt
echo "kylinpy" >> ./docker/requirements-local.txt && \
#echo "pinotdb" >> ./docker/requirements-local.txt
echo "pybigquery" >> ./docker/requirements-local.txt && \
#echo "cockroachdb" >> ./docker/requirements-local.txt
#echo "sqlalchemy_dremio" >> ./docker/requirements-local.txt
echo "elasticsearch-dbapi" >> ./docker/requirements-local.txt && \
#echo "sqlalchemy-exasol" >> ./docker/requirements-local.txt
#echo "gsheetsdb" >> ./docker/requirements-local.txt
#echo "ibm_db_sa" >> ./docker/requirements-local.txt
echo "mysqlclient" >> ./docker/requirements-local.txt && \
echo "cx_Oracle" >> ./docker/requirements-local.txt && \
echo "psycopg2" >> ./docker/requirements-local.txt && \
echo "pymssql" >> ./docker/requirements-local.txt
#echo "snowflake-sqlalchemy" >> ./docker/requirements-local.txt
#echo "sqlalchemy-teradata" >> ./docker/requirements-local.txt
#echo "sqlalchemy-vertica-python" >> ./docker/requirements-local.txt
sudo tsocks docker-compose build --force-rm
restart_compose_d docker-compose.yml
sudo docker ps -a
echo "lan web url: `iip`:8088" && \
echo "wan web url: `wip`:8088"
echo "add clickhouse DB"
echo "http://`wip`:8088/databaseview/list/"
echo 'clickhouse://{username}:{password}@{hostname}:{port}/{database}'
echo "clickhouse://root:XXXXXXXXXX@13.59.180.229:8123/default"
}


# -----------------


# sudo apt-get update
# echo y | sudo apt  install docker.io 
# sudo docker ps -a
# deploy_superset