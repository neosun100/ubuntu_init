
# 文件检查 
# 第一个参数 目标文件路径
# 第二个参数 目标文件不存在需要的源文件路径
check_file(){
TARGET=${1:-/data/spug/spug_api/db.sqlite3}
SOURCE=${2:-`echo ~`/upload/ubuntu_init/docker_file_dir/docker-compose/spug/db.sqlite3}
TARGET_PATH=$(dirname ${1:-/data/spug/spug_api/db.sqlite3})

[ -f $TARGET ] && \
echo "`date +'%Y-%m-%d %H:%M:%S'` $TARGET exist ✅" || \
( echo "`date +'%Y-%m-%d %H:%M:%S'` $TARGET is not exist ❌" && \
sudo mkdir -p $TARGET_PATH && \
sudo chmod -R 777 $TARGET_PATH && \
cp $SOURCE $TARGET && \
sudo chmod -R 777 $TARGET && \
echo "`date +'%Y-%m-%d %H:%M:%S'` $TARGET is moved completed ✅" )
}

restart_compose(){
YAML=${1:-`echo ~`/upload/ubuntu_init/docker_file_dir/docker-compose/spug/x86-spug.yml}

sudo docker-compose -f $YAML stop && \
sudo docker-compose -f $YAML rm -f && \
sudo docker-compose -f $YAML up
}


restart_compose_d(){
YAML=${1:-`echo ~`/upload/ubuntu_init/docker_file_dir/docker-compose/spug/x86-spug.yml}

sudo docker-compose -f $YAML stop && \
sudo docker-compose -f $YAML rm -f && \
sudo docker-compose -f $YAML up -d
}


sudo mkdir -p /data/zk/zoo/data && \
sudo mkdir -p /data/logs/zk/zoo/datalog && \
sudo mkdir -p /data/kafka/data && \
sudo mkdir -p /data/logs/kafka/kafka-logs 

sudo chmod -R 777 /data

field(){
  awk "{print \$${1:-1}}"
}

# 🍎 查看局域网ip,            默认|比格云|腾讯云|qq云
iip(){
# if [ -e /sbin/ifconfig ];then
#   #ifconfig | egrep "192.168|10.100|172.27|172.30" | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | sed -n "1,1p"
#   ifconfig | egrep "192.168|10.100|172.27|172.30" | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | head -n 1
# else
#   #ip addr | egrep "192.168|10.100|172.27|172.30" | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | sed -n "1,1p"
#   ip addr | egrep "192.168|10.100|172.27|172.30" | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | head -n 1
# fi


# [[ `uname` = "Darwin" ]] && \
# ifconfig | egrep "192.168|10.100|172.27|172.30" | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | head -n 1 || \
# ip addr |grep inet |grep -v inet6 | grep -v 127.0.0.1 | field 2 | line 1 1 | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}"

python3 -c "import socket;print([(s.connect(('8.8.8.8', 53)), s.getsockname()[0], s.close()) for s in [socket.socket(socket.AF_INET, socket.SOCK_DGRAM)]][0][1])"
}


if [ `file /bin/bash | field 7` = "ARM" ] && [ `uname` = "Linux" ]; then
  echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为arm Linux"
  echo "`date +'%Y-%m-%d %H:%M:%S'` 没有开发 ⌛️"


elif [ `uname -m` = "x86_64" ] && [ `uname` = "Linux" ]; then
  echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为X86 Linux"
  cd `echo ~`/upload/ubuntu_init/docker_file_dir/docker-compose/kafka/
  echo "LOCAL_IP=`iip`" > `echo ~`/upload/ubuntu_init/docker_file_dir/docker-compose/kafka/.env  #  添加环境变量 yml直接用
  restart_compose_d `echo ~`/upload/ubuntu_init/docker_file_dir/docker-compose/kafka/x86-kafka-single.yml
  cd ~/upload;


elif [ `uname -m` = "x86_64" ] && [ `uname` = "Darwin" ]; then
  echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为X86 Mac" 
  echo "`date +'%Y-%m-%d %H:%M:%S'` 没有开发 ⌛️"

else
  echo "`uname` 系统未知！"
fi


