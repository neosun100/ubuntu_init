import itertools
from requests_html import HTMLSession

session = HTMLSession()

urls = [
    f"https://www.confluent.io/hub/kafka-connectors-{i}" for i in range(1, 50)]


pluginUrl_original = [[pluginUrl for pluginUrl in session.get(
    url).html.absolute_links if "https://www.confluent.io/hub/" in pluginUrl and pluginUrl not in urls] for url in urls]


pluginUrls = list(itertools.chain.from_iterable(pluginUrl_original))

results = [session.get(purl).html.xpath(
    '//*[@id="gatsby-focus-wrapper"]/div/main/div/div[2]/div/div[2]/div[1]/div/div/div/div[1]/div/text()') for purl in pluginUrls]


fileList = [y.replace("confluent-hub install", "") for y in list(itertools.chain.from_iterable(results))
            if "advantco/kafka-adapter" not in y
            and "symetryml/kafka-connector" not in y
            and "camunda/kafka-connect-zeebe" not in y]

with open("/Users/jiasunm/Code/ubuntu_init/docker_file_dir/docker-compose/kafka/kafka-connect-all/Dockerfile", 'w+') as f:
    f.write("FROM rueedlinger/kafka-connect:3.0.0"+'\n')
    f.write("RUN confluent-hub install ")
    for line in fileList:
        f.write(line)
    f.write(" --component-dir $CONNECT_HOME/plugins --worker-configs $CONNECT_WORKER_CONFIG --no-prompt")
