
# 文件检查 
# 第一个参数 目标文件路径
# 第二个参数 目标文件不存在需要的源文件路径
check_file(){
TARGET=${1:-/data/spug/spug_api/db.sqlite3}
SOURCE=${2:-`echo ~`/upload/ubuntu_init/docker_file_dir/docker-compose/spug/db.sqlite3}
TARGET_PATH=$(dirname ${1:-/data/spug/spug_api/db.sqlite3})

[ -f $TARGET ] && \
echo "`date +'%Y-%m-%d %H:%M:%S'` $TARGET exist ✅" || \
( echo "`date +'%Y-%m-%d %H:%M:%S'` $TARGET is not exist ❌" && \
sudo mkdir -p $TARGET_PATH && \
sudo chmod -R 777 $TARGET_PATH && \
cp $SOURCE $TARGET && \
sudo chmod -R 777 $TARGET && \
echo "`date +'%Y-%m-%d %H:%M:%S'` $TARGET is moved completed ✅" )
}

restart_compose(){
YAML=${1:-`echo ~`/upload/ubuntu_init/docker_file_dir/docker-compose/spug/x86-spug.yml}

sudo docker-compose -f $YAML stop && \
sudo docker-compose -f $YAML rm -f && \
sudo docker-compose -f $YAML up
}


restart_compose_d(){
YAML=${1:-`echo ~`/upload/ubuntu_init/docker_file_dir/docker-compose/spug/x86-spug.yml}

sudo docker-compose -f $YAML stop && \
sudo docker-compose -f $YAML rm -f && \
sudo docker-compose -f $YAML up -d
}


sudo mkdir -p /data/zk/zoo/data && \
sudo mkdir -p /data/logs/zk/zoo/datalog && \
sudo mkdir -p /data/kafka/data && \
sudo mkdir -p /data/logs/kafka/kafka-logs 

sudo chmod -R 777 /data

field(){
  awk "{print \$${1:-1}}"
}

# 🍎 查看局域网ip,            默认|比格云|腾讯云|qq云
iip(){
# if [ -e /sbin/ifconfig ];then
#   #ifconfig | egrep "192.168|10.100|172.27|172.30" | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | sed -n "1,1p"
#   ifconfig | egrep "192.168|10.100|172.27|172.30" | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | head -n 1
# else
#   #ip addr | egrep "192.168|10.100|172.27|172.30" | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | sed -n "1,1p"
#   ip addr | egrep "192.168|10.100|172.27|172.30" | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | head -n 1
# fi


# [[ `uname` = "Darwin" ]] && \
# ifconfig | egrep "192.168|10.100|172.27|172.30" | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | head -n 1 || \
# ip addr |grep inet |grep -v inet6 | grep -v 127.0.0.1 | field 2 | line 1 1 | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}"

python3 -c "import socket;print([(s.connect(('8.8.8.8', 53)), s.getsockname()[0], s.close()) for s in [socket.socket(socket.AF_INET, socket.SOCK_DGRAM)]][0][1])"
}



sudo mkdir -p /data/kafka-cluster/zookeeper_data
sudo mkdir -p /data/kafka-cluster/kafka_0_data
sudo mkdir -p /data/kafka-cluster/kafka_1_data
sudo mkdir -p /data/kafka-cluster/kafka_2_data
sudo mkdir -p /data/kafka-cluster/mysql-data
sudo mkdir -p /data/kafka-cluster/mysql-init_sql
sudo mkdir -p /data/kafka-cluster/kafka-manager
sudo chmod 777 -R /data/kafka-cluster

sudo rm -rf /data/kafka-cluster/kafka-manager/*
cp ~/upload/ubuntu_init/docker_file_dir/docker-compose/kafka/Logi-KafkaManager/Logi-KafkaManager-manager/application.yml /data/kafka-cluster/kafka-manager/application.yml
sudo rm -rf /data/kafka-cluster/mysql-init_sql/*
cp ~/upload/ubuntu_init/docker_file_dir/docker-compose/kafka/Logi-KafkaManager/Logi-KafkaManager-mysql/create_mysql_table.sql /data/kafka-cluster/mysql-init_sql/create_mysql_table.sql

sudo chmod 777 -R /data/kafka-cluster


if [ `file /bin/bash | field 7` = "ARM" ] && [ `uname` = "Linux" ]; then
  echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为arm Linux"
  echo "`date +'%Y-%m-%d %H:%M:%S'` 没有开发 ⌛️"


elif [ `uname -m` = "x86_64" ] && [ `uname` = "Linux" ]; then
  echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为X86 Linux"
  restart_compose_d `echo ~`/upload/ubuntu_init/docker_file_dir/docker-compose/kafka/x86-kafka-cluster-with-confluent-and-lenses.yml


elif [ `uname -m` = "x86_64" ] && [ `uname` = "Darwin" ]; then
  echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为X86 Mac" 
  echo "`date +'%Y-%m-%d %H:%M:%S'` 没有开发 ⌛️"

else
  echo "`uname` 系统未知！"
fi


echo "`iip`:18080 kafka-manager admin:admin"
echo "`iip`:13306 kafka-mysql admin:123456"
echo "`iip`:13306 kafka-mysql root:123456"
echo "`iip`:18080 kafka root:123456"
echo "`iip`:2181 zk"
echo "`iip`:9092 kafka-0 kafka:AWS2themoon broker kafka:AWS2themoon client"
echo "`iip`:19092 kafka-1 kafka:AWS2themoon broker kafka:AWS2themoon client"
echo "`iip`:29092 kafka-2 kafka:AWS2themoon broker kafka:AWS2themoon client"
echo "`iip`:"
echo "忽略密码"
# echo "初始化 mysql init 第一次启动时，执行即可，脚本如下"
# echo "mysql -h `iip` -P13306 -uroot -p123456 < /data/kafka-cluster/mysql-init_sql/create_mysql_table.sql"
# init 生效了，以上脚本注释就不要了
echo "kafka-manager 操作手册 https://github.com/didi/Logi-KafkaManager/blob/master/docs/user_guide/user_guide_cn.md"
echo "https://github.com/bitnami/bitnami-docker-kafka"