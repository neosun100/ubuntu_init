

[ClickHouse管理工具ckman说明文档](https://segmentfault.com/a/1190000039346699)
[Doc](https://github.com/housepower/ckman/blob/main/docs/Ckman_Document_zh.md)

https://github.com/housepower/ckman


```zsh
sudo docker run -itd \
-p 8808:8808 \
--restart unless-stopped \
--name ckman \
quay.io/housepower/ckman:latest
```