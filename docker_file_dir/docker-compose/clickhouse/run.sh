
setup_compose(){ 
echo y | sudo apt-get install python3-pip;  
sudo pip3 install docker-compose;  
}

restart_compose(){
YAML=${1:-`echo ~`/upload/ubuntu_init/docker_file_dir/docker-compose/x86-neo_pub.yml}

sudo docker-compose -f $YAML stop && \
sudo docker-compose -f $YAML rm -f && \
sudo docker-compose -f $YAML up
}


restart_compose_d(){
YAML=${1:-`echo ~`/upload/ubuntu_init/docker_file_dir/docker-compose/x86-neo_pub.yml}

sudo docker-compose -f $YAML stop && \
sudo docker-compose -f $YAML rm -f && \
sudo docker-compose -f $YAML up -d
}



alias grepip='grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}"'


# 🍎 查看局域网ip,            默认|比格云|腾讯云|qq云
iip(){
# if [ -e /sbin/ifconfig ];then
#   #ifconfig | egrep "192.168|10.100|172.27|172.30" | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | sed -n "1,1p"
#   ifconfig | egrep "192.168|10.100|172.27|172.30" | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | head -n 1
# else
#   #ip addr | egrep "192.168|10.100|172.27|172.30" | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | sed -n "1,1p"
#   ip addr | egrep "192.168|10.100|172.27|172.30" | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | head -n 1
# fi


# [[ `uname` = "Darwin" ]] && \
# ifconfig | egrep "192.168|10.100|172.27|172.30" | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | head -n 1 || \
# ip addr |grep inet |grep -v inet6 | grep -v 127.0.0.1 | field 2 | line 1 1 | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}"

python3 -c "import socket;print([(s.connect(('8.8.8.8', 53)), s.getsockname()[0], s.close()) for s in [socket.socket(socket.AF_INET, socket.SOCK_DGRAM)]][0][1])"
}




wip(){
# curl -s "https://www.taobao.com/help/getip.php" | grepip
curl -s https://www.taobao.com/help/getip.php | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}"
}







deploy_clickhouse(){
setup_compose
cd ~/clickhouse
pwd
restart_compose_d ./x86-clickhouse.yml
sudo docker ps -a
}



# -----------------


sudo apt-get update
echo y | sudo apt  install docker.io 
sudo docker ps -a
deploy_clickhouse

python3 ./clickhouse_d/python_script/create_table.py
echo "create table order_detail_with_weather,order_detail_with_weather_2019_2020,order_detail_with_weather_all successfully."

# 0- 100 个文件，内存较小， 可拆分多条命令，如下：
#python3 ./clickhouse_d/create_table.py 0 50
#python3 ./clickhouse_d/create_table.py 50 100
python3 ./clickhouse_d/python_script/aws2clickhouse.py 0 100
echo "insert data order_detail_with_weather ok"

python3 ./clickhouse_d/python_script/aws2clickhouse_2019_2020.py 0 20
echo "insert data order_detail_with_weather_2019_2020 ok."

python3 ./clickhouse_d/python_script/aws2clickhouse_all.py 0 130
echo "insert data order_detail_with_weather_all ok."



