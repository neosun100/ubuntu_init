import pyarrow.parquet as pq
from clickhouse_driver import Client



def create_table(path,table):

    table = pq.read_table(path)
    typedict = {'int64':'Int64', 'timestamp[ns]':'DateTime', 'string':'String', 'int32':'Int32', 'double':'Float64'}
    r = [f"{table.field(i).name} {typedict[str(table.field(i).type)]}" for i in range(len(table.columns))]
    r = ",".join(r)
    create_sql= f"create table {table}({r})ENGINE MergeTree() ORDER BY ({table.field(0).name});"
    create_sql = create_sql.replace("date String", "date Date")
    client = Client(host='127.0.0.1', port=9000, user='root', database='default', password='loop123')
    try:
        # 导入数据
        client.execute(create_sql)
        print("创表成功")
    except Exception as e:
        print(str(e))
    client.disconnect()

if __name__ == '__main__':
    path1 = "s3://aws-task-abc/df_green_clean_2_parquet/part-00000-c72985c0-7106-4523-b33c-ebf9e1099d98-c000.snappy.parquet"
    path2 = f"s3://aws-task-abc/df_green_clean_2_parquet_2019_2020/part-00000-2c526efa-c9cc-45b6-874b-8f7f75a3a5bf-c000.snappy.parquet"
    path3 = f"s3://aws-task-abc/df_green_clean_2_parquet_all/part-00000-17c8d803-8e3c-4f2f-bfd5-78c8208ef1ed-c000.snappy.parquet"
    create_table(path1, "order_detail_with_weather")
    create_table(path2, "order_detail_with_weather_2019_2020")
    create_table(path3, "order_detail_with_weather_all")