# -*- coding: utf-8 -*-
from datetime import datetime
import pyarrow.parquet as pq
import sys
from clickhouse_driver import Client
from concurrent.futures import ThreadPoolExecutor
import threading

class MyThread(threading.Thread):

    def __init__(self, func, args=()):
        super(MyThread, self).__init__()
        self.func = func
        self.args = args

    def run(self):
        self.result = self.func(*self.args)

    def get_result(self):
        try:
            return self.result  # 如果子线程不使用join方法，此处可能会报没有self.result的错误
        except Exception:
            return None


def get_thread_csv(s, filenum):
    s = int(s)
    filenum = int(filenum)
    def dwd_by_hour(fid=0):
        result = []
        try:
            print("thread by hour" + str(fid) + " running......")
            fids = f'00000{str(fid)}'[-5:]
            path = f"s3://aws-task-abc/df_green_clean_2_parquet_all/part-{fids}-17c8d803-8e3c-4f2f-bfd5-78c8208ef1ed-c000.snappy.parquet"
            table = pq.read_table(path)
            # print(len(table))
            df = table.to_pandas()
            # df = df.dropna()
            df = df.fillna("")
            result = list(df.to_records())
        except Exception as e:
            print(f"读取数据失败：{str(e)}")
        return result
    thread_pool = []
    results = []
    for i in range(s, filenum):
        # t = threading.Thread(target=dwd_by_hour, args=(i,))
        t = MyThread(dwd_by_hour, args=(i,))
        thread_pool.append(t)
        t.start()

    for t in thread_pool:
        t.join()
        results += t.get_result()
    print(datetime.now())
    return results

def parser_int(record):
    intcolumns = [0, 3, 4, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 24, 25, 26, 28, 29, 30, 31]
    for i in intcolumns:
        try:
            int(record[i])
        except:
            return False
    return True

def task_into_clickhouse(sp):
    res = []
    columns = ['VendorID','lpep_pickup_datetime','lpep_dropoff_datetime','passenger_count','trip_distance','fare_amount','extra','mta_tax','tip_amount','tolls_amount','ehail_fee','total_amount','payment_type','Pickup_longitude','Pickup_latitude','Dropoff_longitude','Dropoff_latitude','month','year','hour','quarter','useTime','useMinute','date','daysOfWeek','Average_speed','wind_level','weather','max_tem','low_tem','avg_tem','diff_tem']

    thread_name = threading.current_thread().getName()

    print('线程【%s】正在处理任务【%d】：do something...' % (thread_name, sp))
    for record in results[sp: sp+step]:
        record = list(record)
        if None in record:
            pass
        # elif parser_int(record):
        else:
            i=1
            v = f"({record[i + 0]},toDateTime('{str(record[i + 1]).split('.')[0]}'),toDateTime('{str(record[i + 2]).split('.')[0]}'),{record[i + 3]},{record[i + 4]},'{record[i + 5]}','{record[i + 6]}','{record[i + 7]}','{record[i + 8]}',{record[i + 9]},{record[i + 10]},{record[i + 11]},{record[i + 12]},{record[i + 13]},{record[i + 14]},{record[i + 15]},{record[i + 16]},{record[i + 17]},{record[i + 18]},{record[i + 19]},{record[i + 20]},{record[i + 21]},{record[i + 22]},toDate('{str(record[i + 23]).split('.')[0]}'),{record[i + 24]},{record[i + 25]},{record[i + 26]},'{record[i + 27]}',{record[i + 28]},{record[i + 29]},{record[i + 30]},{record[i + 31]})"
            res.append(v)
    sql = f"insert into order_detail_with_weather_all({','.join(columns)}) VALUES {','.join(res)}"
    client = Client(host='127.0.0.1', port=9000, user='root', database='default', password='loop123')
    try:
        # 导入数据
        client.execute(sql)
        print(f"写入ok{sp}")
    except Exception as e:
        print(str(e))
    client.disconnect()




if __name__ == '__main__':
    print(datetime.now())
    print(sys.argv[1], sys.argv[2])
    results = get_thread_csv(sys.argv[1], sys.argv[2])
    print(f"获取：{len(results)}条数据待入库")
    # 初始化线程池，定义好池里最多有几个线程同时运行
    pool = ThreadPoolExecutor(max_workers=20, thread_name_prefix='Thread')
    # 每10万条数据启动一个写库线程，提交到线程池
    step = 100000
    for i in range(0, len(results), step):
        pool.submit(task_into_clickhouse, i)
        # task_into_clickhouse(i)
    print(datetime.now())