# 文件检查
# 第一个参数 目标文件路径
# 第二个参数 目标文件不存在需要的源文件路径
check_file() {
  TARGET=${1:-/data/spug/spug_api/db.sqlite3}
  SOURCE=${2:-$(echo ~)/upload/ubuntu_init/docker_file_dir/docker-compose/spug/db.sqlite3}
  TARGET_PATH=$(dirname ${1:-/data/spug/spug_api/db.sqlite3})

  [ -f $TARGET ] &&
    echo "$(date +'%Y-%m-%d %H:%M:%S') $TARGET exist ✅" ||
    (echo "$(date +'%Y-%m-%d %H:%M:%S') $TARGET is not exist ❌" &&
      sudo mkdir -p $TARGET_PATH &&
      sudo chmod 777 $TARGET_PATH &&
      cp $SOURCE $TARGET &&
      sudo chmod 777 $TARGET &&
      echo "$(date +'%Y-%m-%d %H:%M:%S') $TARGET is moved completed ✅")

  # [ -f ${1:-/data/spug/spug_api/db.sqlite3} ] && \
  # echo "`date +'%Y-%m-%d %H:%M:%S'` ${1:-/data/spug/spug_api/db.sqlite3} exist ✅" || \
  # ( echo "`date +'%Y-%m-%d %H:%M:%S'` ${1:-/data/spug/spug_api/db.sqlite3} is not exist ❌" && \
  # sudo mkdir -p $(dirname ${1:-/data/spug/spug_api/db.sqlite3})   && \
  # sudo chmod 777 $(dirname ${1:-/data/spug/spug_api/db.sqlite3})   && \
  # cp ${2:-~/upload/ubuntu_init/docker_file_dir/docker-compose/spug/db.sqlite3} ${1:-/data/spug/spug_api/db.sqlite3} && \
  # sudo chmod 777  ${1:-/data/spug/spug_api/db.sqlite3} && \
  # echo "`date +'%Y-%m-%d %H:%M:%S'` ${1:-/data/spug/spug_api/db.sqlite3} is moved completed ✅" )
}

restart_compose() {
  YAML=${1:-$(echo ~)/upload/ubuntu_init/docker_file_dir/docker-compose/spug/x86-spug.yml}

  sudo docker-compose -f $YAML stop &&
    sudo docker-compose -f $YAML rm -f &&
    sudo docker-compose -f $YAML up --scale spark-worker=3
}

restart_compose_d() {
  YAML=${1:-$(echo ~)/upload/ubuntu_init/docker_file_dir/docker-compose/spug/x86-spug.yml}

  sudo docker-compose -f $YAML stop &&
    sudo docker-compose -f $YAML rm -f &&
    sudo docker-compose -f $YAML up --scale spark-worker=3 -d
}

# 🍎 查看局域网ip,            默认|比格云|腾讯云|qq云
iip() {
  # if [ -e /sbin/ifconfig ];then
  #   #ifconfig | egrep "192.168|10.100|172.27|172.30" | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | sed -n "1,1p"
  #   ifconfig | egrep "192.168|10.100|172.27|172.30" | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | head -n 1
  # else
  #   #ip addr | egrep "192.168|10.100|172.27|172.30" | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | sed -n "1,1p"
  #   ip addr | egrep "192.168|10.100|172.27|172.30" | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | head -n 1
  # fi

  # [[ `uname` = "Darwin" ]] && \
  # ifconfig | egrep "192.168|10.100|172.27|172.30" | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | head -n 1 || \
  # ip addr |grep inet |grep -v inet6 | grep -v 127.0.0.1 | field 2 | line 1 1 | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}"

  python3 -c "import socket;print([(s.connect(('8.8.8.8', 53)), s.getsockname()[0], s.close()) for s in [socket.socket(socket.AF_INET, socket.SOCK_DGRAM)]][0][1])"
}

time_wait() {
  NUM=${1:-5}
  for i in $(seq $NUM -1 1); do
    echo "$i 秒后执行"
    sleep 1
  done
}

if [ $(uname -m) = "armv7l" ] && [ $(uname) = "Linux" ]; then
  echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为arm Linux"
  echo "$(date +'%Y-%m-%d %H:%M:%S') 没有开发 ⌛️"

elif [ $(uname -m) = "x86_64" ] && [ $(uname) = "Linux" ]; then
  echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为X86 Linux"
  #   sed '3s/.*/SPARK_VERSION=3.2.0/g' `echo ~`/upload/ubuntu_init/docker_file_dir/docker-compose/spark/.env
  restart_compose_d $(echo ~)/upload/ubuntu_init/docker_file_dir/docker-compose/hadoop/hadoop3.2.1/x86-hadoop3.2.1.yml
  echo "http://$(iip):9870 Hadoop hdfs namenode UI"
  echo "http://$(iip):9990 Hadoop hdfs port"
  echo "http://$(iip):19864 Hadoop hdfs datanode-1 UI"
  echo "http://$(iip):29864 Hadoop hdfs datanode-2 UI"
  echo "http://$(iip):39864 Hadoop hdfs datanode-3 UI"
  echo "http://$(iip):28088 Hadoop Yarn resourcemanager UI"
  echo "http://$(iip):8042 Hadoop Yarn nodemanager-1 UI"
  echo "http://$(iip):8043 Hadoop Yarn nodemanager-2 UI"
  echo "http://$(iip):8044 Hadoop Yarn nodemanager-3 UI"
  echo "http://$(iip):8188 Hadoop Yarn historyserver UI"
  echo "http://$(iip):39083 Hadoop Hive metastore"
  echo "http://$(iip):10003 Hadoop Hive thrift server JDBC"

elif
  [ $(uname -m) = "x86_64" ] && [ $(uname) = "Darwin" ]
then
  echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为X86 Mac"
  echo "$(date +'%Y-%m-%d %H:%M:%S') 没有开发 ⌛️"

else
  echo "$(uname) 系统未知！"
fi
