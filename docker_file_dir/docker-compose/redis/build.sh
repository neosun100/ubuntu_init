

cd ~/upload && \   
git clone https://github.com/Grokzen/docker-redis-cluster && \
cd docker-redis-cluster && \
sudo tsocks docker-compose build --build-arg "redis_version=6.0-rc1" redis-cluster && \
cd ~/upload
