# [core]
# default_timezone=system

# https://airflow.apache.org/docs/apache-airflow/stable/start/docker.html

# install_docker-compose_lastest() {
#     cd ~/upload
#     # sudo curl -L "https://github.com/docker/compose/releases/download/1.29.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
#     # sudo curl -L  http://cdn.neo.pub/docker-compose-Linux-x86_64   -o /usr/local/bin/docker-compose
#     download_fast_github_best_file_url 'https://github.com/docker/compose/releases' '/usr/local/bin/docker-compose'

#     sudo chmod +x /usr/local/bin/docker-compose

#     if [ -f /usr/local/bin/docker-compose ]; then
#         echo "软连接已存在"
#     else
#         sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
#     fi

#     # 如果存在pip版本的，就卸载掉
#     # 字段非空写法的判断
#     # if [ -z `sudo pip list | grep docker-compose` ] ; then
#     if [ $(sudo pip list | grep docker-compose) ]; then
#         sudo pip uninstall docker-compose -y
#     fi

#     docker-compose version
#     sudo docker-compose version
# }

# 删除容器
nrm() {
  sudo docker rm -f $* 
}


restart_compose() {
    YAML=${1:-$(echo ~)/upload/ubuntu_init/docker_file_dir/docker-compose/spug/x86-spug.yml}

    sudo docker-compose -f $YAML stop &&
        sudo docker-compose -f $YAML rm -f &&
        sudo docker-compose -f $YAML up
}

restart_compose_d() {
    YAML=${1:-$(echo ~)/upload/ubuntu_init/docker_file_dir/docker-compose/spug/x86-spug.yml}

    sudo docker-compose -f $YAML stop &&
        sudo docker-compose -f $YAML rm -f &&
        sudo docker-compose -f $YAML up -d $2 $3 $4 $5 $6 $7
}

echo "保证 docker0compose 是最新版"
# install_docker-compose_lastest

sudo mkdir -p /data/airflow
sudo chmod 777 -R /data/airflow
cd /data/airflow
curl -LfO 'https://airflow.apache.org/docs/apache-airflow/stable/docker-compose.yaml'
mkdir ./dags ./logs ./plugins
sudo chmod 777 -R /data/airflow
echo -e "AIRFLOW_UID=1000\nAIRFLOW_GID=0" >.env

echo "初始化DB"
cp  -f  ~/upload/ubuntu_init/docker_file_dir/docker-compose/airflow/x86-airflow.yml /data/airflow/docker-compose.yml
# 目前只改了一点，就是cfg的外围访问权限
# auth_backend = airflow.api.auth.backend.deny_all
# auth_backend = airflow.api.auth.backend.default
# auth_backend = airflow.api.auth.backend.basic_auth
cat ~/upload/ubuntu_init/docker_file_dir/docker-compose/airflow/conf/airflow.cfg > /data/airflow/airflow.cfg

sudo chmod 777 -R /data/airflow
sudo docker-compose up airflow-init
cd /data/airflow && \
restart_compose_d docker-compose.yml scale worker=5
cd ~/upload
# 配置文件
# https://airflow.apache.org/docs/apache-airflow/stable/security/api.html 家一条身份免验证 可以供 api

echo "清理 init 容器"
nrm airflow_airflow-init_1


# ncp airflow_airflow-webserver_1:/opt/airflow/airflow.cfg ./airflow.cfg

# TODO
# 改成真正一键启动的

# 扩展包装满 从image开始搞了
