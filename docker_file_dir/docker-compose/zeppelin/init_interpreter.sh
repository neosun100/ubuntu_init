ncp() {
  sudo docker container cp $1 $2
}



sudo docker run -p 8080:8080 --rm -d --name zeppelin apache/zeppelin:0.10.0
mkdir -p /data/zeppelin/notebook/archon
sudo chmod 777 -R /data/zeppelin
ncp zeppelin:/opt/zeppelin/interpreter /data/zeppelin/ 
ncp zeppelin:/opt/zeppelin/notebook/ /data/zeppelin/archon/
ls -lah  /data/zeppelin/interpreter
sudo docker rm -f zeppelin 
sudo chmod -R 777  /data/zeppelin


[[ -d /data/zeppelin/spark ]] && echo "存在 /data/zeppelin/spark" || ( mkdir -p /data/zeppelin/{spark,flink} && cp -r /usr/local/spark/* /data/zeppelin/spark )

[[ -d /data/zeppelin/flink ]] && echo "存在 /data/zeppelin/flink" || ( mkdir -p /data/zeppelin/{spark,flink} && cp -r /usr/local/flink/* /data/zeppelin/flink )
# cp -r /usr/local/spark/* /data/zeppelin/spark                                                                      
[[ -d /data/zeppelin/flink-1.13.2 ]] && echo "存在 /data/zeppelin/flink-1.13.2 目前zeppelin 0.10.0 只支持 flink 1.13.2以下版本" || ( mkdir -p /data/zeppelin/flink-1.13.2 && cd /data/zeppelin && wget https://mirrors.tuna.tsinghua.edu.cn/apache/flink/flink-1.13.2/flink-1.13.2-bin-scala_2.12.tgz && tar -zxvf flink-1.13.2-bin-scala_2.12.tgz &&  sudo rm -rf flink-1.13.2-bin-scala_2.12.tgz && cd ~/upload )

# cp -r /usr/local/flink/* /data/zeppelin/flink

sudo chmod -R 777  /data/zeppelin