# https://dolphinscheduler.apache.org/zh-cn/docs/1.3.4/user_doc/cluster-deployment.html


# https://dolphinscheduler.apache.org/zh-cn/docs/1.3.4/user_doc/quick-start.html

# https://dolphinscheduler.apache.org/zh-cn/docs/latest/user_doc/guide/installation/docker.html




## ----------------------------------------------------------- ✅
echo "清volume和container"
sudo rm -rf /data/dolphinscheduler/* &&  \
sudo mkdir -p /data/dolphinscheduler/{postgresql,zookeeper,worker-data,logs,shared-local,resource-local} && \
sudo chmod 777 -R /data/dolphinscheduler && \
cd /data/dolphinscheduler/ && \
VERSION=1.3.9 && \
wget https://mirrors.tuna.tsinghua.edu.cn/apache/dolphinscheduler/$VERSION/apache-dolphinscheduler-$VERSION-src.tar.gz && \
tar -zxvf apache-dolphinscheduler-$VERSION-src.tar.gz >/dev/null 2>&1 && \
cd apache-dolphinscheduler-$VERSION-src/docker/docker-swarm && \
sudo docker pull dolphinscheduler.docker.scarf.sh/apache/dolphinscheduler:latest && \
restart_compose_d docker-compose.yml && \
sleep 30 && \
sudo docker-compose up -d --scale dolphinscheduler-master=2 dolphinscheduler-master && \
sleep 30 && \
sudo docker-compose up -d --scale dolphinscheduler-worker=3 dolphinscheduler-worker && \
echo "访问前端页面： http://`iip`:12345/dolphinscheduler " && \
echo "默认的用户是admin，默认的密码是dolphinscheduler123"

## ----------------------------------------------------------- ❌ 2.0.0 有 bug
sudo rm -rf /data/dolphinscheduler/* &&  \
sudo mkdir -p /data/dolphinscheduler/{postgresql,zookeeper,worker-data,logs,shared-local,resource-local} && \
sudo chmod 777 -R /data/dolphinscheduler && \
cd /data/dolphinscheduler/ && \
VERSION=2.0.0 && \
wget https://mirrors.tuna.tsinghua.edu.cn/apache/dolphinscheduler/$VERSION/apache-dolphinscheduler-$VERSION-src.tar.gz && \
tar -zxvf apache-dolphinscheduler-$VERSION-src.tar.gz >/dev/null 2>&1 && \
cd apache-dolphinscheduler-$VERSION-src/docker/docker-swarm && \
sudo docker pull dolphinscheduler.docker.scarf.sh/apache/dolphinscheduler:latest && \
restart_compose_d docker-compose.yml && \
sleep 30 && \
sudo docker-compose up -d --scale dolphinscheduler-master=2 dolphinscheduler-master && \
sleep 30 && \
sudo docker-compose up -d --scale dolphinscheduler-worker=3 dolphinscheduler-worker && \
echo "访问前端页面： http://`iip`:12345/dolphinscheduler " && \
echo "默认的用户是admin，默认的密码是dolphinscheduler123"


## ----------------------------------------------------------- ✅
echo "清volume和container"
sudo rm -rf /data/dolphinscheduler/* &&  \
sudo mkdir -p /data/dolphinscheduler/{postgresql,zookeeper,worker-data,logs,shared-local,resource-local} && \
sudo chmod 777 -R /data/dolphinscheduler && \
cd /data/dolphinscheduler/ && \
VERSION=2.0.1 && \
wget https://mirrors.tuna.tsinghua.edu.cn/apache/dolphinscheduler/$VERSION/apache-dolphinscheduler-$VERSION-src.tar.gz && \
tar -zxvf apache-dolphinscheduler-$VERSION-src.tar.gz >/dev/null 2>&1 && \
cd apache-dolphinscheduler-$VERSION-src/docker/docker-swarm && \
sudo docker pull dolphinscheduler.docker.scarf.sh/apache/dolphinscheduler:latest && \
restart_compose_d docker-compose.yml && \
sleep 30 && \
sudo docker-compose up -d --scale dolphinscheduler-master=2 dolphinscheduler-master && \
sleep 30 && \
sudo docker-compose up -d --scale dolphinscheduler-worker=3 dolphinscheduler-worker && \
echo "访问前端页面： http://`iip`:12345/dolphinscheduler " && \
echo "默认的用户是admin，默认的密码是dolphinscheduler123"



# 扩缩容 master 至 2 个实例:

sudo docker-compose up -d --scale dolphinscheduler-master=2 dolphinscheduler-master
# 扩缩容 worker 至 3 个实例:

sudo docker-compose up -d --scale dolphinscheduler-worker=3 dolphinscheduler-worker