#!/bin/bash
# GREMLINVER=3.4.12
GREMLINVER=3.5.1
cd /opt/apache-tinkerpop-gremlin-server-$GREMLINVER
./bin/gremlin-server.sh conf/gremlin-server-atlas.yaml
