# https://github.com/pandio-com/pulsar-docker-compose

# 删除容器
nrm() {
    sudo docker rm -f $*
}

# 文件检查
# 第一个参数 目标文件路径
# 第二个参数 目标文件不存在需要的源文件路径
check_file() {
    TARGET=${1:-/data/spug/spug_api/db.sqlite3}
    SOURCE=${2:-$(echo ~)/upload/ubuntu_init/docker_file_dir/docker-compose/spug/db.sqlite3}
    TARGET_PATH=$(dirname ${1:-/data/spug/spug_api/db.sqlite3})

    [ -f $TARGET ] &&
        echo "$(date +'%Y-%m-%d %H:%M:%S') $TARGET exist ✅" ||
        (echo "$(date +'%Y-%m-%d %H:%M:%S') $TARGET is not exist ❌" &&
            sudo mkdir -p $TARGET_PATH &&
            sudo chmod -R 777 $TARGET_PATH &&
            cp $SOURCE $TARGET &&
            sudo chmod -R 777 $TARGET &&
            echo "$(date +'%Y-%m-%d %H:%M:%S') $TARGET is moved completed ✅")
}

restart_compose() {
    YAML=${1:-$(echo ~)/upload/ubuntu_init/docker_file_dir/docker-compose/spug/x86-spug.yml}

    sudo docker-compose -f $YAML stop &&
        sudo docker-compose -f $YAML rm -f &&
        sudo docker-compose -f $YAML up
}

restart_compose_d() {
    YAML=${1:-$(echo ~)/upload/ubuntu_init/docker_file_dir/docker-compose/spug/x86-spug.yml}

    sudo docker-compose -f $YAML stop &&
        sudo docker-compose -f $YAML rm -f &&
        sudo docker-compose -f $YAML up -d
}

field() {
    awk "{print \$${1:-1}}"
}

# 🍎 查看局域网ip,            默认|比格云|腾讯云|qq云
iip() {
    # if [ -e /sbin/ifconfig ];then
    #   #ifconfig | egrep "192.168|10.100|172.27|172.30" | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | sed -n "1,1p"
    #   ifconfig | egrep "192.168|10.100|172.27|172.30" | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | head -n 1
    # else
    #   #ip addr | egrep "192.168|10.100|172.27|172.30" | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | sed -n "1,1p"
    #   ip addr | egrep "192.168|10.100|172.27|172.30" | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | head -n 1
    # fi

    # [[ `uname` = "Darwin" ]] && \
    # ifconfig | egrep "192.168|10.100|172.27|172.30" | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | head -n 1 || \
    # ip addr |grep inet |grep -v inet6 | grep -v 127.0.0.1 | field 2 | line 1 1 | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}"

    python3 -c "import socket;print([(s.connect(('8.8.8.8', 53)), s.getsockname()[0], s.close()) for s in [socket.socket(socket.AF_INET, socket.SOCK_DGRAM)]][0][1])"
}

sudo mkdir -p /data/pulsar/zoo1/data
sudo mkdir -p /data/pulsar/zoo2/data
sudo mkdir -p /data/pulsar/zoo3/data

sudo mkdir -p /data/pulsar/lzk1/data

sudo mkdir -p /data/logs/pulsar/zoo1/log
sudo mkdir -p /data/logs/pulsar/zoo2/log
sudo mkdir -p /data/logs/pulsar/zoo3/log

sudo mkdir -p /data/logs/pulsar/lzk1/log

sudo mkdir -p /data/pulsar/bookie1/data
sudo mkdir -p /data/pulsar/bookie2/data
sudo mkdir -p /data/pulsar/bookie3/data

sudo mkdir -p /data/logs/pulsar/bookie1/log
sudo mkdir -p /data/logs/pulsar/bookie2/log
sudo mkdir -p /data/logs/pulsar/bookie3/log

sudo mkdir -p /data/pulsar/broker1/data
sudo mkdir -p /data/pulsar/broker2/data

sudo mkdir -p /data/logs/pulsar/broker1/log
sudo mkdir -p /data/logs/pulsar/broker2/log

sudo mkdir -p /data/logs/pulsar/proxy/log

sudo mkdir -p /data/pulsar/conf
sudo mkdir -p /data/pulsar/scripts

sudo mkdir -p /data/pulsar/manager/data

sudo mkdir -p /data/pulsar/pulsar-manager/data

sudo chmod 777 -R /data/pulsar
sudo chmod 777 -R /data/logs/pulsar

sudo rm -rf /data/pulsar/conf/*
sudo rm -rf /data/pulsar/scripts/*

cp ~/upload/ubuntu_init/docker_file_dir/docker-compose/pulsar/pulsar-cluster/* /data/pulsar/scripts

sudo chmod 777 -R /data/pulsar
sudo chmod 777 -R /data/logs/pulsar

if [ $(file /bin/bash | field 7) = "ARM" ] && [ $(uname) = "Linux" ]; then
    echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为arm Linux"
    echo "$(date +'%Y-%m-%d %H:%M:%S') 没有开发 ⌛️"

elif
    [ $(uname -m) = "x86_64" ] && [ $(uname) = "Linux" ]
then
    echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为X86 Linux"
    restart_compose_d $(echo ~)/upload/ubuntu_init/docker_file_dir/docker-compose/pulsar/pulsar-cluster/x86_pulsar_all_in_one.yml

    echo "清理 init 容器"
    sleep 30
    nrm pulsar-init


    CSRF_TOKEN=$(curl -s http://$(iip):7750/pulsar-manager/csrf-token)
    curl \
        -H 'X-XSRF-TOKEN: $CSRF_TOKEN' \
        -H 'Cookie: XSRF-TOKEN=$CSRF_TOKEN;' \
        -H "Content-Type: application/json" \
        -X PUT "http://$(iip):7750/pulsar-manager/users/superuser" \
        -d '{"name": "admin", "password": "apachepulsar", "description": "admin", "email": "jiasunm@amazon.com"}'

elif
    [ $(uname -m) = "x86_64" ] && [ $(uname) = "Darwin" ]
then
    echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为X86 Mac"
    echo "$(date +'%Y-%m-%d %H:%M:%S') 没有开发 ⌛️"

else
    echo "$(uname) 系统未知！"
fi
echo ""
echo "  http://$(iip):8888 - proxy web service"
echo "  pulsar://$(iip):6650 - proxy pulsar service"
echo "  http://$(iip):1080 - pulsar manager"
echo "admin:apachepulsar"
echo "add env http://172.123.4.77:8080"
echo "python ~/upload/ubuntu_init/docker_file_dir/docker-compose/pulsar/pulsar-cluster/consumer.py"
echo "python ~/upload/ubuntu_init/docker_file_dir/docker-compose/pulsar/pulsar-cluster/producer.py"
echo "http://$(iip):8888/admin/v2/persistent/public/default/my-topic/stats" # 主题统计信息

