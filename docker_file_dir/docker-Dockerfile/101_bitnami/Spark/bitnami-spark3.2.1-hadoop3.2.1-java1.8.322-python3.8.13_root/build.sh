login_neo
SPARK_VERSION=3.2.1
HADOOP_VERSION=3.2.1
JAVA_VERSION=1.8.322
PYTHON_VERSION=3.8.13

docker_build_push \
/home/neo/upload/ubuntu_init/docker_file_dir/docker-Dockerfile/101_bitnami/Spark/bitnami-spark${SPARK_VERSION}-hadoop${HADOOP_VERSION}-java${JAVA_VERSION}-python${PYTHON_VERSION}_root/Dockerfile \
registry.cn-shanghai.aliyuncs.com/neo_docker/spark:bitnami-spark${SPARK_VERSION}-hadoop${HADOOP_VERSION}-java${JAVA_VERSION}-python${PYTHON_VERSION}_root
