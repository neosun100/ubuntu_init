source ~/.bash_profile

VERSION=$(curl -s https://www.monetdb.org/downloads/sources/archive/\?C\=M\;O\=D  | htmlq a --attribute  href  | grep MonetDB  | head -n 1 | cutnum | head -n 1 | pformat  | rformat  | t2pformat )



ENV MONETDB_VERSION 11.39.17

