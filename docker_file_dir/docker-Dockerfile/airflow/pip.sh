RUN pip install --no-cache-dir --upgrade -i https://pypi.tuna.tsinghua.edu.cn/simple requests_html
RUN pip install --no-cache-dir --upgrade -i https://pypi.tuna.tsinghua.edu.cn/simple speedtest-cli;
RUN pip install --no-cache-dir --upgrade -i https://pypi.tuna.tsinghua.edu.cn/simple tqdm ;
RUN pip install --no-cache-dir --upgrade -i https://pypi.tuna.tsinghua.edu.cn/simple pssh;
RUN pip install --no-cache-dir --upgrade -i https://pypi.tuna.tsinghua.edu.cn/simple arrow;
RUN pip install --no-cache-dir --upgrade -i https://pypi.tuna.tsinghua.edu.cn/simple toolkity;
RUN pip install --no-cache-dir --upgrade -i https://pypi.tuna.tsinghua.edu.cn/simple ansible;
RUN pip install --no-cache-dir --upgrade -i https://pypi.tuna.tsinghua.edu.cn/simple pendulum;
RUN pip install --no-cache-dir --upgrade -i https://pypi.tuna.tsinghua.edu.cn/simple httpie;
RUN pip install --no-cache-dir --upgrade -i https://pypi.tuna.tsinghua.edu.cn/simple faker;
RUN pip install --no-cache-dir --upgrade -i https://pypi.tuna.tsinghua.edu.cn/simple fake2db;
RUN pip install --no-cache-dir --upgrade -i https://pypi.tuna.tsinghua.edu.cn/simple pydbgen;
RUN pip install --no-cache-dir --upgrade -i https://pypi.tuna.tsinghua.edu.cn/simple factory_boy;
RUN pip install --no-cache-dir --upgrade -i https://pypi.tuna.tsinghua.edu.cn/simple xmljson;
RUN pip install --no-cache-dir --upgrade -i https://pypi.tuna.tsinghua.edu.cn/simple pkuseg;
RUN pip install --no-cache-dir --upgrade -i https://pypi.tuna.tsinghua.edu.cn/simple youtube-dl -U;
RUN pip install --no-cache-dir --upgrade -i https://pypi.tuna.tsinghua.edu.cn/simple pyguetzli;
RUN pip install --no-cache-dir --upgrade -i https://pypi.tuna.tsinghua.edu.cn/simple  myqr;
RUN pip install --no-cache-dir --upgrade -i https://pypi.tuna.tsinghua.edu.cn/simple pingparsing==0.14;
RUN pip install --no-cache-dir --upgrade -i https://pypi.tuna.tsinghua.edu.cn/simple python-nmap;
RUN pip install --no-cache-dir --upgrade -i https://pypi.tuna.tsinghua.edu.cn/simple pysnooper;
RUN pip install --no-cache-dir --upgrade -i https://pypi.tuna.tsinghua.edu.cn/simple lz4;
RUN pip install --no-cache-dir --upgrade -i https://pypi.tuna.tsinghua.edu.cn/simple joblib;
RUN pip install --no-cache-dir --upgrade -i https://pypi.tuna.tsinghua.edu.cn/simple dingtalk-sdk # 钉钉sdk 不是消息哦
RUN pip install --no-cache-dir --upgrade -i https://pypi.tuna.tsinghua.edu.cn/simple rich # 高亮 表格 进度条 库
RUN pip install --no-cache-dir --upgrade -i https://pypi.tuna.tsinghua.edu.cn/simple langdetect # 谷歌语言检测库
RUN pip install --no-cache-dir --upgrade -i https://pypi.tuna.tsinghua.edu.cn/simple charset_normalizer
RUN pip install --no-cache-dir --upgrade -i https://pypi.tuna.tsinghua.edu.cn/simple numerizer # numerizer[5]是一款将自然语言中数字转化成int或者float型数字的Python小工具。
RUN pip install --no-cache-dir --upgrade -i https://pypi.tuna.tsinghua.edu.cn/simple emot # # emot可以用于从文本(字符串)中提取emojis和emoticon等，所有的表情符号。
RUN pip install --no-cache-dir --upgrade -i https://pypi.tuna.tsinghua.edu.cn/simple natsort #  自然排序数字 解决 1 10 2 12 排序问题
RUN pip install --no-cache-dir --upgrade -i https://pypi.tuna.tsinghua.edu.cn/simple fuzzywuzzy


