
cd ~/upload && \
rm -rf shorter && \
tsocks git clone  https://github.com/icowan/shorter && \
if [ `uname -m` = "armv7l" ] && [ `uname` = "Linux" ]; then
  echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为arm Linux"
  docker_build_push `echo ~`/upload/shorter/Dockerfile registry.cn-shanghai.aliyuncs.com/neo_docker/shorter:arm
elif [ `uname -m` = "x86_64" ] && [ `uname` = "Linux" ]; then
  echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为X86 Linux"
  docker_build_push `echo ~`/upload/shorter/Dockerfile registry.cn-shanghai.aliyuncs.com/neo_docker/shorter:v2
elif [ `uname -m` = "x86_64" ] && [ `uname` = "Darwin" ]; then
  echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为X86 Mac" 
  echo "`date +'%Y-%m-%d %H:%M:%S'` 没有开发 ⌛️"
else
  echo "`uname` 系统未知！"
fi


