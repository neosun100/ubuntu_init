# 树莓派涉及的核心流程和shell

# 🧲🧲🧲🧲🧲🧲🧲🧲🧲🧲🧲🧲🧲🧲🧲🧲🧲🧲🧲🧲🧲
# mac 下执行

# 烧录tf卡

# 1. mac的磁盘工具，选中tf卡，点抹掉，格式化选择MS-DOS(FAT)
# 2. ndf-check
# 3. iso2disk
# 3.5 检查ssh文件是否生成
# 4. 磁盘工具卸载tf
ndf_check(){
    echo "显示已挂在的磁盘";
    df -lh | lcat;
    echo "显示所有磁盘及其分区情况"
    diskutil list | lcat;
    echo "请根据以上数据确认需要烧录的磁盘名称";
}

# 参数1: iso路径
# 参数2: 磁盘名称
iso2disk(){
echo "卸载需要烧录的磁盘分区";
diskutil unmount /dev/${1:-disk2}s1;  
# diskutil unmount /dev/${2:-disk4}s2;  

echo "烧录";
# sudo dd bs=4m if=${1:-/Volumes/MacData/ISO/2019-07-10-raspbian-buster-lite.img} of=/dev/r${2:-disk4};  
sudo dd bs=4m if=${1:-/Users/neo/Downloads/2019-09-26-raspbian-buster-lite.zip/2019-09-26-raspbian-buster-lite.img} of=/dev/r${2:-disk2};
echo "添加ssh文件";
sudo touch /Volumes/boot/ssh;

echo "添加wifi";
echo "https://blog.csdn.net/u011055198/article/details/87951583";
echo "复制如下";
echo "🐝 🐝 🐝 🐝 🐝 🐝 🐝 🐝 🐝 🐝 🐝 🐝 🐝 🐝 🐝 🐝 🐝 🐝 "
echo 'country=CN';
echo 'ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev';
echo 'update_config=1';
echo "";
echo 'network={';
echo 'ssid="phoenix-2.4G"';
echo 'psk="neosun100"';
echo 'key_mgmt=WPA-PSK';
echo 'priority=1';
echo '}';

sudo touch /Volumes/boot/wpa_supplicant.conf;


echo "卸载磁盘";
diskutil unmountDisk /dev/${1:-disk2};

echo "请拔出磁盘";
}


pi_passwd(){
echo "pi";
echo "raspberry";
}

fix_super(){
cp -r /Volumes/boot\ 1/*  /Volumes/boot/ 
}


# 判断系统是树莓派加
# # export LC_ALL="en_US.UTF-8"
# export LC_ALL="zh_CN.UTF-8"; 
# TZ='Asia/Shanghai';
# export TZ; 

if [ `uname -m` = "armv7l" ]; then
# sed -i ‘’ '1d' $dirName/$targetName;
export LC_ALL="zh_CN.UTF-8" && TZ='Asia/Shanghai' && export TZ && export PATH=$PATH:/sbin ; 

elif [ `uname -m` = "aarch64" ]; then
# sed -i ‘’ '1d' $dirName/$targetName;
export LC_ALL="zh_CN.UTF-8" && TZ='Asia/Shanghai' && export TZ && export PATH=$PATH:/sbin ; 

elif [ `uname -m` = "x86_64" ]; then
# echo "x64无需设置";
else
    echo "`uname` 系统未知！"
fi




# 交换空间初始最佳化
add_swp(){
echo "SWP：这个文件是linux中当内存不够用时启动的一个内存替代品";
echo "有时候Htop会出现SWP满值的情况，这个时候我们需要扩展一下SWP文件了";

echo "";
echo "";
echo "01. 先创建一个普通文件 预计要 3 分钟";
sudo dd if=/dev/zero of=/swapfile bs=1M count=4096;
echo " bs单区块大小，count区块数量。bs*count等于swapfile的大小";
echo "在创建的过程中可能会出现swapfile在使用中，正忙。那说明当前系统有交换空间，换个名字就行";

echo "";
echo "";
echo "02. 设置为swap文件";
sudo mkswap /swapfile;

echo "";
echo "";
echo "03. 修改一下权限";
sudo chmod 600 /swapfile;

echo "";
echo "";
echo "04. 激活交换空间";
sudo swapon /swapfile;

echo "";
echo "";
echo "04. 设置开机自动加载";
echo " 在 /etc/fstab 末尾添加";
echo "/swapfile none swap sw 0 0";
sleep 10;
sudo  vim /etc/fstab;
}

# 激活交换空间
actitate_swp(){
[[ `free -m | grep Swap | field 2` -lt 4000 ]] && \
sudo swapon /swapfile || \
echo "swap 已激活" 
}

vim_boot_config(){
echo "当前磁盘信息";
sdf;
[[ -f /Volumes/boot/config.txt ]] && sudo vim /Volumes/boot/config.txt || echo "不存在 /Volumes/boot/config.txt 文件"
}