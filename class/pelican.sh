
# pelican 安装与初始化
pelican_init(){
# pip install pelican markdown typogrify -U
# mkdir ~/upload/blog
# cd blog
# pelican-quickstart
# vim upload/blog/pelicanconf.py 
#     
}

# pelican config
# alias vimpc="vim ~/upload/blog/pelicanconf.py"

pelican_head(){
echo "Title: Pelican init";
echo "Date: `date +'%Y-%m-%d %H:%M:%S'`";
echo "Modified: `date +'%Y-%m-%d %H:%M:%S'`";
echo "Category: Python";
echo "Tags: pelican, init";
echo "Slug: my-awesome-pelican";
echo "Authors: Neo";
echo "Summary: pelican部署流程";
echo "";
echo "-----";
}

# 更新 modified
# pelican_update_modified(){

# }

# pelican ipynb 2 used md
# 参数为需要转化的ipynb的绝对路径
jupyter2blog(){
#     "# 0 cd 到工作目录"
# # 1. 可解析的路径
# resolvablePath=`echo "${1:-~/Resource/Ipynb/[Apps]Project/012.[Blog]Pelican.ipynb}" | replace_all '\[' '\\\['   | replace_all '\]' '\\\]'`     
# # 2. 拆分dirName 与 fileName
# dirName=`dirname $resolvablePath`
# fileName=`basename $resolvablePath`
# # 3. cd 到 路径下
# echo 📍📍📍📍📍📍📍📍📍📍📍📍📍📍📍
# echo "cd $dirName"
# echo "cp $"
echo 📍📍📍📍📍📍📍📍📍📍📍📍📍📍📍📍📍📍📍📍📍📍📍📍📍📍
echo "`date +'%Y-%m-%d %H:%M:%S'` 获取 dirName fileName targetName" | lcat;
dirName=`dirname ${1:-~/Resource/Ipynb/1【Apps】Project/012.【Blog】Pelican.ipynb}`
fileName=`basename ${1:-~/Resource/Ipynb/1【Apps】Project/012.【Blog】Pelican.ipynb}`
targetName=`echo $fileName | replace_one .ipynb .md`
echo "$dirName \n $fileName \n $targetName";
echo "`date +'%Y-%m-%d %H:%M:%S'` 将 $fileName 转化为 $targetName" | lcat;
jupyter nbconvert --to markdown ${1:-~/Resource/Ipynb/1【Apps】Project/012.【Blog】Pelican.ipynb}

echo "`date +'%Y-%m-%d %H:%M:%S'` 去除 jupyter 生成的 $targetName 文件首行空白" | lcat;
echo "分linux还是mac清洗掉第一行的空白";
echo 溯源问题 https://blog.csdn.net/sanbingyutuoniao123/article/details/72929801;
if [ `uname` = "Darwin" ]; then
# sed -i ‘’ '1d' $dirName/$targetName;
sed -i "" '1d' $dirName/$targetName;
elif [ `uname` = "Linux" ]; then
sed -i '1d' $dirName/$targetName;
else
    echo "`uname` 系统未知！"
fi

echo "`date +'%Y-%m-%d %H:%M:%S'` 更新 $targetName 文件刷新时间" | lcat;
nowTime=`date +'%Y-%m-%d %H:%M:%S'`
# echo "Modified: 2019-07-20 13:09:56" | sed -e "1s/^.*$/Modified: $nowTime/"  # -e 不覆盖源文件
if [ `uname` = "Darwin" ]; then
# sed -i ‘’ '1d' $dirName/$targetName;
sed -i ""  "3s/^.*$/Modified: $nowTime/" $dirName/$targetName # 将文件的第三行替换为指定字符串 
elif [ `uname` = "Linux" ]; then
sed -i "3s/^.*$/Modified: $nowTime/" $dirName/$targetName # 将文件的第三行替换为指定字符串
else
    echo "`uname` 系统未知！"
fi


echo "`date +'%Y-%m-%d %H:%M:%S'` 将 $targetName 文件 移动到 blog/content" | lcat;
mv -f $dirName/$targetName ~/upload/blog/content/  

echo "`date +'%Y-%m-%d %H:%M:%S'` all mds retry to htmls" | lcat;
cd ~/upload/blog;
pelican content;
cd -;
}

#  查看 pelican 完整配置
pelican_setting(){
    pelican --print-settings | lcat
}



# pelican run
pelican_run(){
    python -m pelican.server --path ~/upload/blog/output 
}

# 参数为需要发布的 绝对路径的 ipynb文件
pelican_debug(){
    jupyter2blog ${1:-~/Resource/Ipynb/1【Apps】Project/012.【Blog】Pelican.ipynb} && pelican_run;
}
