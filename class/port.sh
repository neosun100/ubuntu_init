# 接口检测

# udp接口测试
ping_udp() {
    nc -vuz ${1:-47.56.10.118} ${2:-44444}
}

# tcp
ping_tcp() {
    nc -vz ${1:-47.56.10.118} ${2:-33333}
}

# 向UDP端口发送消息: nc -vu x.x.x.x xxxx

install_prettyping() {
    brew install prettyping
    echo "curl -O https://raw.githubusercontent.com/denilsonsa/prettyping/master/prettyping"
    echo "无法使用，则自行下载"
}

install_hping3() {
    brew install hping
}

alias pping="prettyping"

# 端口转发
# 编辑 vimr
# 安装
# 重启

# 端口转化
# 编辑 vimr
# 安装
# 重启

install_ping_all() {

    install_prettyping
    echo "pping"

    brew install fping # 多 ping
    echo "fping -A -u -c 4 192.168.1.1 192.168.1.74 192.168.1.20"

    brew install hping
    echo "hacker ping"

    brew install tcping
    echo "http://devinz.org/install-tcping-for-macosx.html"

    if [ $(uname -m) = "armv7l" ] && [ $(uname) = "Linux" ]; then
        echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为arm Linux" | lcat
        echo y | sudo apt-get install tcptraceroute
        echo y | sudo apt-get install traceroute
        cd /usr/bin
        sudo wget http://www.vdberg.org/~richard/tcpping
        sudo chmod 755 tcpping
        #   typeset sysname="_linux_arm"
        # 执行代码块
    elif [ $(uname -m) = "x86_64" ] && [ $(uname) = "Linux" ]; then
        echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为X86 Linux" | lcat
        echo y | sudo apt-get install tcptraceroute
        echo y | sudo apt-get install traceroute
        cd /usr/bin
        sudo wget http://www.vdberg.org/~richard/tcpping
        sudo chmod 755 tcpping
        #   typeset sysname="_linux_amd64"
        # 执行代码块
    elif [ $(uname -m) = "x86_64" ] && [ $(uname) = "Darwin" ]; then
        echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为X86 Mac" | lcat

        #   typeset sysname="_darwin_amd64"
        # 执行代码块
    else
        echo "$(uname) 系统未知！"
    fi

    # $ cd /usr/bin
    # $ sudo wget http://www.vdberg.org/~richard/tcpping
    # $ sudo chmod 755 tcpping

}


# tcp ing
ntcp(){
    tcpping $* | lcat
}