# 此文件是青云对象存储的快捷操作

# 安装
qsctl_install(){
    pip install qsctl -U
}

# ls 系列
qsctl_ls(){
    qsctl ls qs://neone-club/$1 -c '~/Conf/qs_access_key.csv'  | lcat
}

qsctl_ls_sys(){
    qsctl ls qs://neone-club/Sys/ -c '~/Conf/qs_access_key.csv'  | lcat
}

qsctl_ls_pic(){
    qsctl ls qs://neone-club/dd-pic/ -c '~/Conf/qs_access_key.csv'  | lcat
}

qsctl_ls_voice(){
    qsctl ls qs://neone-club/dd-voice/   -c '~/Conf/qs_access_key.csv'  | lcat
}

qsctl_ls_json(){
    qsctl ls qs://neone-club/dd-json/   -c '~/Conf/qs_access_key.csv'  | lcat
}

# cp系列
qsctl_cp(){
    qsctl cp $1 qs://neone-club/$2   -c '~/Conf/qs_access_key.csv'  | lcat
}

qsctl_cp_sys(){
    qsctl cp $1 qs://neone-club/Sys/   -c '~/Conf/qs_access_key.csv'  | lcat
}

qsctl_cp_pic(){
    qsctl cp $1 qs://neone-club/dd-pic/   -c '~/Conf/qs_access_key.csv'  | lcat
}

qsctl_cp_voice(){
    qsctl cp $1 qs://neone-club/dd-voice/   -c '~/Conf/qs_access_key.csv'  | lcat
}

qsctl_cp_json(){
    qsctl cp $1 qs://neone-club/dd-json/   -c '~/Conf/qs_access_key.csv'  | lcat
}

# rm 系列
qsctl_rm(){
    qsctl rm qs://neone-club/$1   -c '~/Conf/qs_access_key.csv'  | lcat
}

qsctl_rm_sys(){
    qsctl rm qs://neone-club/Sys/$1   -c '~/Conf/qs_access_key.csv'  | lcat
}

qsctl_rm_pic(){
    qsctl rm qs://neone-club/dd-pic/$1 -c '~/Conf/qs_access_key.csv'  | lcat
}

qsctl_rm_voice(){
    qsctl rm qs://neone-club/dd-voice/$1   -c '~/Conf/qs_access_key.csv'  | lcat
}

qsctl_rm_json(){
    qsctl rm qs://neone-club/dd-json/$1   -c '~/Conf/qs_access_key.csv'  | lcat
}

# 获取文件url
qsctl_url(){
    qsctl presign qs://neone-club/$1   -c '~/Conf/qs_access_key.csv'  | lcat
}


# 同步系列  local2qs
qsctl_sync_up(){
    qsctl $1 qs://neone-club/$2 -c '~/Conf/qs_access_key.csv'  | lcat
}


# 同步系列  qs2local
qsctl_sync_down(){
    qsctl qs://neone-club/$1 $2  -c '~/Conf/qs_access_key.csv'  | lcat
}

# 大规模移动系列
qsctl_mul(){
~/upload/bin/qscamel -h
}