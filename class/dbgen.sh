


setup_dbgen(){
mac
git clone https://github.com/greenlion/ssb-dbgen
make

dbgen
gendata




linux
git clone https://github.com/vadimtk/ssb-dbgen
make

dbgen
qgen

}


gen_demo(){
num=${1:-100}
./dbgen -s $num -T c
./dbgen -s $num -T l
./dbgen -s $num -T p
./dbgen -s $num -T s
./dbgen -s $num -T d
}


flog(){
    ~/upload/bin/flog $*
}