# termius app 的命令行工具


termius_init(){
echo "`date +'%Y-%m-%d %H:%M:%S'` 安装并升级 termius" | lcat
pip install -U termius
echo "`date +'%Y-%m-%d %H:%M:%S'` 初始化配置文件" | lcat
[[ -f /home/neo/.ssh/config ]] && \
echo "`date +'%Y-%m-%d %H:%M:%S'` 已存在配置文件" || \
(mkdir -p /home/neo/.ssh && touch /home/neo/.ssh/config && echo "`date +'%Y-%m-%d %H:%M:%S'` 初始化配置文件已完成" )
echo "`date +'%Y-%m-%d %H:%M:%S'` 同步云配置" | lcat
termius init
echo "`date +'%Y-%m-%d %H:%M:%S'` 显示资源" | lcat
termius hosts -c address  -c label    | lcat
echo "`date +'%Y-%m-%d %H:%M:%S'` 连接命令解析"
echo "连接命令 termius connect label" | lcat
}