# md 相关处理


install_md2map(){
echo "安装md转map"
npm install markmap-lib -g
markmap -h
}

md2map(){
markmap --enable-mathjax --enable-prism $*
}


# 函数功能：安装并演示最好的 Markdown CLI 工具 Glow
# 注意：需要先安装 Homebrew 包管理器

install_md() {
    # 使用 Homebrew 安装 Glow markdown 
    brew install glow
    echo "glow output"
    glow /Users/jiasunm/Code/ubuntu_init/PPT-MD/SA_launch.md 


    # 演示 Glow 命令行工具的帮助信息
    glow -h || echo "Glow 工具安装失败，请检查相关配置和网络连接。"


    brew install mdcat  # 如果你使用 Homebrew
    echo "mdcat output"
    mdcat /Users/jiasunm/Code/ubuntu_init/PPT-MD/SA_launch.md

}


f2md() {
    log() {
        echo "$(date '+%Y-%m-%d %H:%M:%S') - $1"
    }

    # 支持的文件格式
    supported_formats="pdf|ppt|pptx|doc|docx|xls|xlsx|jpg|jpeg|png|gif|mp3|wav|html|csv|json|xml|zip"

    # 检查是否提供了文件名
    if [ $# -eq 0 ]; then
        log "错误：请提供文件名"
        echo "用法: 2md <文件名>"
        echo "支持的格式: pdf, ppt, pptx, doc, docx, xls, xlsx, jpg, jpeg, png, gif, mp3, wav, html, csv, json, xml, zip"
        return 1
    fi

    input_file="$1"
    
    # 检查文件是否存在
    if [ ! -f "$input_file" ]; then
        log "错误：文件 '$input_file' 不存在"
        return 1
    fi

    # 检查文件格式是否支持
    if ! echo "$input_file" | grep -E "\.(${supported_formats})$" > /dev/null; then
        log "错误：不支持的文件格式 '${input_file##*.}'"
        echo "支持的格式: pdf, ppt, pptx, doc, docx, xls, xlsx, jpg, jpeg, png, gif, mp3, wav, html, csv, json, xml, zip"
        return 1
    fi

    # 生成输出文件名
    output_file="${input_file%.*}.md"

    # 检查输出文件是否已存在
    if [ -e "$output_file" ]; then
        log "警告：输出文件 '$output_file' 已存在"
        read -p "是否覆盖？(y/n) " -n 1 -r
        echo
        if [[ ! $REPLY =~ ^[Yy]$ ]]; then
            log "用户取消了操作，因为输出文件已存在"
            return 1
        fi
    fi

    # 执行转换
    log "正在转换文件: $input_file"
    if markitdown "$input_file" > "$output_file" 2>&1; then
        log "成功：已将 $input_file 转换为 $output_file"

        # 自动将生成的 Markdown 内容复制到剪贴板（使用 njq 命令）
        if command -v njq >/dev/null 2>&1; then
            cat "$output_file" | njq
            log "已将 $output_file 内容复制到剪贴板"
        else
            log "警告：njq 命令不可用，无法将内容复制到剪贴板"
            log "提示：如果需要复制内容，可执行: cat $output_file | njq"
        fi
    else
        log "错误：转换 $input_file 失败"
        return 1
    fi
}


