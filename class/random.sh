rand_str(){
LENGTH=${1:-1}
echo "`tr -dc a-z < /dev/urandom | head -c$LENGTH`"
}


rand_int(){
# bash默认有一个$RANDOM的变量, 默认范围是0~32767
# https://www.cnblogs.com/nulisaonian/p/5829772.html
# https://www.cnblogs.com/golinux/p/10831061.html
BOTTOM=${1:-0}
TOP=${2:-3}
NUM=`expr $TOP - $BOTTOM + 1`

echo $[$RANDOM%$NUM+$BOTTOM]
}


# 随机数