#  日志浏览器，多文件监控
install_log_look(){
if [ `uname` = "Darwin" ]; then
brew install lnav;
elif [ `uname` = "Linux" ]; then
echo y | sudo apt install lnav; 
else
    echo "`uname` 系统未知！"
    echo "https://github.com/tstack/lnav/releases"
    echo "日志高亮"
fi
}



# sudo lnav /var/log/kern.log.1 /var/log/kern.log 
log_look(){
echo "可以一次打开多个日志文件"
sudo lnav $*;
}

# 滚动日志tail查询
log_look_tail(){
sudo tail -F  ${1:-/var/log/kern.log} | hcat;
}


