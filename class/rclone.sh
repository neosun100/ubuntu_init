

# 挂载同步网盘

typeset romotePan="neopub"


install_rclone(){
echo "`date +'%Y-%m-%d %H:%M:%S'` 检查是否已经安装"
if [ -e /usr/bin/rclone ]; then
echo "`date +'%Y-%m-%d %H:%M:%S'` rclone 已安装"
else 
echo "`date +'%Y-%m-%d %H:%M:%S'` 安装全能挂载同步工具 googleDrive oneDrive";
tsocks curl https://rclone.org/install.sh | sudo bash 
echo "`date +'%Y-%m-%d %H:%M:%S'` 还原配置文件"
echo "在mac剪切板中寻找 rclone.conf 进行粘贴复制"
time_wait;
mkdir -p ~/.config/rclone
vim ~/.config/rclone/rclone.conf
nat ~/.config/rclone/rclone.conf
echo "`date +'%Y-%m-%d %H:%M:%S'` 允许所有用户操作磁盘"
echo "user_allow_other"
time_wait;
sudo vim /etc/fuse.conf
echo "`date +'%Y-%m-%d %H:%M:%S'` 安装完成 添加网盘需要验证端口frp映射出来"
fi
}


up_rclone(){
echo "部署网盘"
sourcedir=${1:-neopub:/}
sinkdir=${2:-~/OneDrive}
if [ -d $sinkdir ]; then
rclone mount $sourcedir $sinkdir --copy-links --no-gzip-encoding --no-check-certificate  --allow-other --allow-non-empty --umask 000
else
mkdir -p $sinkdir
rclone mount $sourcedir $sinkdir --copy-links --no-gzip-encoding --no-check-certificate  --allow-other --allow-non-empty --umask 000
fi
}

# 卸载网盘
down_rclone(){
echo "卸载网盘"
fusermount -qzu ${1:-~/OneDrive}
}


stratup_rclone(){
sourcedir=${1:-neopub:/}
sinkdir=${2:-~/OneDrive}
serviceName=${3:-rclone}

#将后面修改成你上面手动运行命令中，除了rclone的全部参数
command="mount $sourcedir $sinkdir --copy-links --no-gzip-encoding --no-check-certificate --allow-other --allow-non-empty --umask 000"
#以下是一整条命令，一起复制到SSH客户端运行

scat > ~/upload/$serviceName.service <<EOF
[Unit]
Description=Rclone
After=network-online.target

[Service]
Type=simple
ExecStart=$(command -v rclone) ${command}
Restart=on-abort
User=root

[Install]
WantedBy=default.target
EOF

scat ~/upload/$serviceName.service
sleep 5;
sudo vim /etc/systemd/system/$serviceName.service
sleep 3;
echo "开始启动"
sudo systemctl start $serviceName
sleep 3;
echo "设置开机自启"
sudo systemctl enable $serviceName
echo "其他命令"
echo "重启：sudo systemctl restart $serviceName\n停止：sudo systemctl stop $serviceName\n状态：sudo systemctl status $serviceName"
rm -rf ~/upload/$serviceName.service
}

