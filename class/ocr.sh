# 百度的 ocr
# https://cloud.baidu.com/doc/OCR/s/Dk3h7yf8m

install_ocr(){
pip install baidu-aip -U
pip install fire -U

}


ocr(){
python ~/upload/ubuntu_init/pyScript/baidu_ocr.py --pic ${1:-~/upload/ubuntu_init/pic/ocr.jpg} --mod ${2:-1}
}

ocr_text(){
python ~/upload/ubuntu_init/pyScript/baidu_ocr.py --pic ${1:-~/upload/ubuntu_init/pic/ocr.jpg} --mod ${2:-1} | sed 's/'"'"/'"''/g'  | jq ".words_result | .[] | .words" |  xargs 
}

ocr_help(){
echo "mod=1 普通模式 也是默认模式 50000次/day" | lcat
echo "mod=2 高精模式 500次/day"  | lcat
echo "mod=3 生僻字模式 500次/day"  | lcat
echo "mod=4 网络上背景复杂，特殊字体模式 500次/day" | lcat 
}


ocr_path_json(){

startTS="`date +%s.%N`";


# 🐙🐙🐙🐙🐙🐙🐙
# 🐙执行逻辑代码块🐙
# 🐙🐙🐙🐙🐙🐙🐙
clean_file  ~/upload/pics.txt;
echo "`date +'%Y-%m-%d %H:%M:%S'` 开始pic转化json 🚀" | lcat
cd ${1:-~/Downloads/附件：中国人民银行调查统计司关于开展单位贷款基础数据试点采集工作的通知_IMG};
for file in `l ${1:-~/Downloads/附件：中国人民银行调查统计司关于开展单位贷款基础数据试点采集工作的通知_IMG}  | grep -E "jpg|png" | field 9`; do ocr $file >> ~/upload/pics.txt; done 
echo "`date +'%Y-%m-%d %H:%M:%S'` pic转化json ✅" | lcat
echo "~/upload/pics.txt";
cd ~/upload;

endTS="`date +%s.%N`";
result=`gawk -v x=$startTS -v y=$endTS 'BEGIN{printf "%.6f\n",y-x}'`; # 总秒数
_hour=`gawk -v x=$result -v y=3600 'BEGIN{printf "%d \n",x/y}'`; # 时数
_min=`gawk -v x=$result -v y=60 -v z=$_hour 'BEGIN{printf "%d \n",(x-z*3600)/y}'`; # 分数
_sec=`gawk -v x=$result -v y=60 -v z=$_hour -v w=$_min 'BEGIN{printf "%d \n", x - z*3600 - w*60 }'`; # 秒数
echo "`date +'%Y-%m-%d %H:%M:%S'` 耗时 $_hour 小时 $_min 分钟 $_sec 秒 " | lcat;
echo "~/upload/pics.txt"
}


# 将路径下的图片转换为纯文本
# 参数1 图片路径
# 参数2 jq 过滤语法
ocr_path_text(){
startTS="`date +%s.%N`";


# 🐙🐙🐙🐙🐙🐙🐙
# 🐙执行逻辑代码块🐙
# 🐙🐙🐙🐙🐙🐙🐙

# for file in $( l ~/Downloads/附件：中国人民银行调查统计司关于开展单位贷款基础数据试点采集工作的通知_IMG  | grep jpg | field 9); do ocr $file >> ~/upload/gagaPDF.txt; done 
# for file in $( l ~/Downloads/附件：中国人民银行调查统计司关于开展单位贷款基础数据试点采集工作的通知_IMG  | grep jpg | field 9); do ocr $file | sed 's/'"'"/'"''/g' | jq ${2:-".words_result | .[] | .words"} | xargs  | pass_black >> ~/upload/pics.txt; done 
# array=`l ${1:-~/Downloads/附件：中国人民银行调查统计司关于开展单位贷款基础数据试点采集工作的通知_IMG}  | grep -E "jpg|png" | field 9`
# for file in $array; do ocr $file | sed 's/'"'"/'"''/g' | jq ${2:-".words_result | .[] | .words"} | xargs  | pass_black >> ~/upload/pics.txt; done 
clean_file  ~/upload/pics.txt;
echo "`date +'%Y-%m-%d %H:%M:%S'` 开始pic转化txt 🚀" | lcat
cd ${1:-~/Downloads/附件：中国人民银行调查统计司关于开展单位贷款基础数据试点采集工作的通知_IMG};
for file in `l ${1:-~/Downloads/附件：中国人民银行调查统计司关于开展单位贷款基础数据试点采集工作的通知_IMG}  | grep -E "jpg|png" | field 9`; do ocr $file | sed 's/'"'"/'"''/g' | jq ${2:-".words_result | .[] | .words"} | xargs  | pass_black >> ~/upload/pics.txt; done 
echo "`date +'%Y-%m-%d %H:%M:%S'` pic转化txt ✅" | lcat
echo "~/upload/pics.txt"
cd ~/upload;

endTS="`date +%s.%N`";
result=`gawk -v x=$startTS -v y=$endTS 'BEGIN{printf "%.6f\n",y-x}'`; # 总秒数
_hour=`gawk -v x=$result -v y=3600 'BEGIN{printf "%d \n",x/y}'`; # 时数
_min=`gawk -v x=$result -v y=60 -v z=$_hour 'BEGIN{printf "%d \n",(x-z*3600)/y}'`; # 分数
_sec=`gawk -v x=$result -v y=60 -v z=$_hour -v w=$_min 'BEGIN{printf "%d \n", x - z*3600 - w*60 }'`; # 秒数
echo "`date +'%Y-%m-%d %H:%M:%S'` 耗时 $_hour 小时 $_min 分钟 $_sec 秒 " | lcat;
echo "~/upload/pics.txt"
}



#####################
#####################
#####################

install_google_ocr(){  
if [ `uname` = "Darwin" ] ; then # 如果是linux话输出linux字符串
echo "Mac os"
brew install tesseract
elif [ `uname` = "Linux" ] ; then
echo "Linux"
echo y | sudo apt install tesseract-ocr
echo Y | sudo apt install libtesseract-dev
else
echo "What?"
fi # 判断结束，以fi结尾

echo "####################################"
tesseract -h 
}


gocr(){
tesseract $*
}