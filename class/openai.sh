# 我自己的openai命令，指向私有endpoint

neoai() {
    # 函数功能：调用 OpenAI 客户端接口执行请求
    # 参数：
    #   $1: OpenAI 请求参数 (必填)，例如："completion" 或 "classification"
    #   $2: 其他请求参数 (可选)，例如请求输入的文本等
    if [ -z "$1" ]; then
        echo "缺少必要的 OpenAI 请求参数。"
        return 1
    fi
    openai --api-base https://gpt.neo.xin/v1/ "$@"
}

# openai_cost() {
#     echo "https://www.yuque.com/u889918/mxayrf/giyz2lxghtyg3awl"
#     echo "https://platform.openai.com/account/usage"
#     http -b --form GET "http://aws.xin:9898/dashboard/billing/usage?end_date=$(date +%Y-%m-%d)&start_date=$(date --date='-23 days' +%Y-%m-%d)" \
#         Authorization:"Bearer $OPENAI_SESSION_KEY" \
#         Content-Type:"application/json" >/tmp/cost.json && python ~/upload/ubuntu_init/pyScript/json_total_to_csv_v7.py /tmp/cost.json /tmp/output.csv && cat /tmp/output.csv | csvq "select * where Tot !=0"
# }

# 函数功能：获取 OpenAI 的费用使用情况并输出到 CSV 文件
# 注意：需要安装 http、python、csvq 工具。
openai_cost() {
    # 输出费用使用情况查询链接
    echo "费用使用情况查询链接：https://www.yuque.com/u889918/mxayrf/giyz2lxghtyg3awl"
    echo "OpenAI 账户使用情况：https://platform.openai.com/account/usage"

    # 调用 AWS API 获取费用使用情况，并输出到 CSV 文件
    http -b --form GET "https://gpt.neo.xin/dashboard/billing/usage?end_date=$(date +%Y-%m-%d)&start_date=$(date --date='-23 days' +%Y-%m-%d)" \
        Authorization:"Bearer $OPENAI_SESSION_KEY" \
        Content-Type:"application/json" >/tmp/cost.json
    if [ $? -ne 0 ]; then
        echo "获取费用使用情况失败，请检查相关配置和网络连接。"
        return 1
    fi

    # 将 JSON 文件转换为 CSV 格式
    python ~/upload/ubuntu_init/pyScript/json_total_to_csv_v7.py /tmp/cost.json /tmp/output.csv
    if [ $? -ne 0 ]; then
        echo "JSON 转换为 CSV 失败。"
        return 1
    fi

    # 使用 csvq 进行数据查询，只输出非零费用项目
    cat /tmp/output.csv | csvq "select * where Tot !=0"
}

# openai_models_list() {
#     neoai api models.list | jq -c . | json2csv | csvq "SELECT id, NANO_TO_DATETIME(created*1000*1000*1000) as time, object, owned_by FROM stdin ORDER BY id"
# }

# 函数功能：列出所有 OpenAI 模型
# 注意：需要安装 neoai、jq、json2csv、csvq 工具。
openai_models_list() {
    # 调用 neoai API 获取模型列表，并使用 jq 和 json2csv 进行格式转换
    neoai api models.list 2>/dev/null | jq -c . 2>/dev/null | json2csv 2>/dev/null | csvq "SELECT id, NANO_TO_DATETIME(created*1000*1000*1000) as time, object, owned_by FROM stdin ORDER BY id" 2>/dev/null
    if [ $? -ne 0 ]; then
        echo "获取模型列表失败，请确保安装了 neoai、jq、json2csv、csvq 工具，并且已正确配置 API。"
    fi
}

# 函数功能：安装 pyspark-ai 及相关依赖库
# 参数：无
# 注意：
#   - 需要先安装 pip 工具。
#   - 根据不同操作系统执行相应的代理设置。
install_pyspark-ai() {
    if [[ $(uname) == "Darwin" ]]; then # 如果是 macOS 系统
        up_shell_all_proxy              # 执行代理设置
    fi

    pip install "pyspark-ai[all]"
    pip install git+https://github.com/neosun100/pyspark-ai
    pip install langchain --upgrade
    pip install openai --upgrade

    if [[ $(uname) == "Darwin" ]]; then # 如果是 macOS 系统
        down_shell_all_proxy            # 关闭代理设置
    fi
}

# install_chatgpt_cli() {
#     echo "https://github.com/j178/chatgpt"
#     echo "An elegant interactive CLI for ChatGPT"
#     cd ~/Downloads
#     wget https://github.com/j178/chatgpt/releases/download/v1.3.1/chatgpt_Darwin_arm64.tar.gz
#     tar -zxvf chatgpt_Darwin_arm64.tar.gz
#     sudo cp /Users/jiasunm/Downloads/chatgpt_Darwin_arm64/chatgpt /usr/local/bin/
#     chatgpt -h
#     cd ~/upload
#     echo "使用前,请设定好OPENAI_API_KEY以及配置文件： ~/.config/chatgpt/config.json"
# }


# 函数功能：安装 ChatGPT CLI（仅支持 macOS）
# 
# 注意：
#   - 该脚本仅适用于 macOS 系统。
#   - 在使用前，请确保已经配置好 OPENAI_API_KEY 和 ~/.config/chatgpt/config.json 文件。
#   - 该脚本将下载 ChatGPT CLI 客户端，解压并添加到系统路径中，以便在命令行中全局调用。
#   - 安装完成后，用户需要配置 OPENAI_API_KEY 和 ~/.config/chatgpt/config.json 文件，然后即可使用 chatgpt 进行交互式对话。
#   - 如需获取更多帮助，请参阅 ChatGPT CLI GitHub 仓库链接：https://github.com/j178/chatgpt
#   - 如果下载或安装过程中出现错误，请确保网络连接畅通，并检查相关配置和权限。

install_chatgpt_cli() {
    echo "正在安装 ChatGPT CLI..."
    echo "ChatGPT CLI 仅适用于 macOS。"
    echo "ChatGPT CLI GitHub 仓库链接：https://github.com/j178/chatgpt"
    echo "ChatGPT CLI(交互式命令行界面)安装完成后，可以通过命令 chatgpt 进行交互式使用。"

    # 下载 ChatGPT CLI 客户端压缩包
    cd ~/Downloads || { echo "无法进入 Downloads 文件夹。"; return 1; }
    wget https://github.com/j178/chatgpt/releases/download/v1.3.1/chatgpt_Darwin_arm64.tar.gz || { echo "下载 ChatGPT CLI 失败。"; return 1; }
    tar -zxvf chatgpt_Darwin_arm64.tar.gz || { echo "解压 ChatGPT CLI 失败。"; return 1; }

    # 将 chatgpt 可执行文件添加到系统路径中
    sudo cp /Users/jiasunm/Downloads/chatgpt_Darwin_arm64/chatgpt /usr/local/bin/ || { echo "添加 ChatGPT CLI 到系统路径失败。"; return 1; }

    # 显示 ChatGPT CLI 帮助信息
    chatgpt -h || { echo "ChatGPT CLI 启动失败。"; return 1; }

    echo "ChatGPT CLI 安装完成。"
    echo "安装完成后，请配置 OPENAI_API_KEY 和 ~/.config/chatgpt/config.json 文件，然后即可使用 chatgpt 进行交互式对话。"
}


# chatgpt_help(){
#     glow ~/upload/ubuntu_init/PPT-MD/chatgpt_cli_readme.md
# }

# 函数功能：查看 ChatGPT CLI 的帮助文档
# 参数：
#   $1: ChatGPT CLI 的帮助文档文件路径 (可选，默认为 '~/upload/ubuntu_init/PPT-MD/chatgpt_cli_readme.md')

chatgpt_help() {
    # 使用 Glow 查看 ChatGPT CLI 的帮助文档
    glow ~/upload/ubuntu_init/PPT-MD/chatgpt_cli_readme.md || echo "无法找到 ChatGPT CLI 的帮助文档，请检查文件路径。"
}



# https://github.com/janlay/openai-cli 
# bash  构建的 cli项目