# 网络相关的命令

# 网卡名
net_card() {
    # sudo  cat /proc/net/dev | awk '{i++; if(i>2){print $1}}' | sed 's/^[\t]*//g' | sed 's/[:]*$//g' | grep en
    # ifconfig | grep  -B 1  `iip`  | head -n 1 | field | pass_mao
    ifconfig | grep -B 5 $(iip) | field | grep 'en' | pass_mao
    # 一般网卡就是 en开头
}

# 网卡网速监控
nnet() {
    sudo nload -u H devices $(net_card) #-u m
}

nnet_gateway() {
    IP_CARD=$(ifconfig | grep -B 5 ${1:-127.0.0.1} | field | grep 'en\|lo\|br' | pass_mao)
    sudo nload -u H devices $IP_CARD
}

# 进程网络监控
npnet() {
    sudo nethogs $(net_card)
}

npnet_gateway() {
    IP_CARD=$(ifconfig | grep -B 5 ${1:-127.0.0.1} | field | grep 'en\|lo\|br' | pass_mao)
    sudo nethogs $IP_CARD
}

nwifi_net() {
    echo "显示网卡信息"
    echo "其中Ethernet Controller表明你的电脑有以太网卡，Network controller表明你的电脑有无线网卡"
    lspci -nnk | grep -iA2 net
}

net_speed() {
    echo "网速测试"
    curl -s https://raw.githubusercontent.com/sivel/speedtest-cli/master/speedtest.py | python -
}
