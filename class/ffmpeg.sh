cut_video() {
    echo "视频剪切"
    SOURCE_VIDEO=${1:-"/Users/jiasunm/Documents/AWS产品/2021-reinvent/reCap_2021_highlight_for_internal_field_HKT.mp4"}
    START_TIME=${2:-"01:06:40"}
    END_TIME=${3:-"01:44:08"}
    TARGET_VIDEO=${4:-"/Users/jiasunm/Documents/AWS产品/2021-reinvent/recap_analytics.mp4"}

    ffmpeg -i $SOURCE_VIDEO \
        -vcodec copy \
        -acodec copy \
        -ss $START_TIME \
        -to $END_TIME \
        $TARGET_VIDEO \
        -y
}

m4a_2_mp3() {
    # 保持原码率转换音频
    INPUT=${1:-'/Users/jiasunm/录音/Lee.m4a'}
    OUTPUT=${2:-'/Users/jiasunm/录音/Lee.mp3'}
    ffmpeg -i $INPUT \
        -acodec libmp3lame \
        -ab "$(ffprobe -v error -select_streams a:0 -show_entries stream=bit_rate -of default=noprint_wrappers=1:nokey=1 $INPUT)" \
        $OUTPUT

}

merge_video() {
    # 函数功能：合并两个视频文件为一个视频文件，支持左右和上下合并模式。
    # 参数：
    #   $1: 第一个输入视频文件路径 (可选，默认为 '/Users/jiasunm/Documents/Pictures/重要图片/35221_1696684949(原视频).mp4')
    #   $2: 第二个输入视频文件路径 (可选，默认为 '/Users/jiasunm/Documents/Pictures/重要图片/00308-2034834098-35221_1696684949原视频.mp4')
    #   $3: 合并后的输出视频文件路径 (可选，默认为 '/Users/jiasunm/Downloads/merge-output.mp4')
    #   $4: 合并模式 (可选，默认为 "上下")，可选值为 "左右" 或 "上下"
    # 注意：
    #   - 请确保已经安装了 FFmpeg 工具。
    #   - 如果某个参数未提供，将使用默认值。
    #   - 如果输入的视频文件路径中包含特殊字符，要记得用引号将其括起来。
    input1=${1:-'/Users/jiasunm/Documents/Pictures/重要图片/35221_1696684949(原视频).mp4'}                # 第一个输入视频
    input2=${2:-'/Users/jiasunm/Documents/Pictures/重要图片/00308-2034834098-35221_1696684949原视频.mp4'} # 第二个输入视频
    output=${3:-'/Users/jiasunm/Downloads/merge-output.mp4'}                                       # 合并后的输出视频
    mode=${4:-"左右"}                                                                                # 合并模式，默认为 "上下"

    if [ "$mode" = "左右" ]; then
        filter="hstack"
    elif [ "$mode" = "上下" ]; then
        filter="vstack"
    else
        echo "无效的合并模式。请选择 \"左右\" 或 \"上下\"。"
        return 1
    fi

    # 执行视频合并命令
    ffmpeg \
        -i "$input1" \
        -i "$input2" \
        -filter_complex "[0:v][1:v]$filter=inputs=2[v]" \
        -map "[v]" \
        -map 0:a \
        "$output"
}

ytmp3() {
    # 设定下载目录
    DOWNLOAD_DIR="/Users/jiasunm/录屏/Youtube"

    # 检查是否提供了 URL
    if [ -z "$1" ]; then
        echo "❌ 请提供 YouTube 视频 URL！"
        echo "用法: ytmp3 <YouTube_URL>"
        return 1
    fi

    # 设定 cookies 路径
    COOKIES_PATH="/Users/jiasunm/upload/cookies.txt"

    # 执行 yt-dlp 命令
    yt-dlp --cookies "$COOKIES_PATH" \
        -x --audio-format mp3 \
        -o "$DOWNLOAD_DIR/%(upload_date)s_%(title)s_%(id)s.%(ext)s" \
        --extractor-args "youtube:client=web" \
        "$1"

    echo "✅ 下载完成: $1"
} 


ytmp4() {
    # 设定下载目录
    DOWNLOAD_DIR="/Users/jiasunm/录屏/Youtube"

    # 检查是否提供了 URL
    if [ -z "$1" ]; then
        echo "❌ 请提供 YouTube 视频 URL！"
        echo "用法: ytmp4 <YouTube_URL>"
        return 1
    fi

    # 设定 cookies 路径
    COOKIES_PATH="/Users/jiasunm/upload/cookies.txt"

    # 执行 yt-dlp 命令
    yt-dlp --cookies "$COOKIES_PATH" \
        -f "bestvideo[ext=mp4]+bestaudio[ext=m4a]/best[ext=mp4]" \
        -o "$DOWNLOAD_DIR/%(upload_date)s/%(title)s_%(id)s.%(ext)s" \
        --merge-output-format mp4 \
        --extractor-args "youtube:client=web" \
        "$1"

    echo "✅ 下载完成: $1"
}

