install_atlas_latest() {
    URL=${1:-https://mirrors.tuna.tsinghua.edu.cn/apache/atlas/}
    NAME=$(echo $URL | gformat | xargs | last_field)                                      # atlas
    VERSION_LATEST=$(http $URL | htmlq --attribute href a | cutnum | ns | head -n 1)      # 2.2.0
    PAKAGE_LIST_URL=$URL$VERSION_LATEST/                                                  # https://mirrors.tuna.tsinghua.edu.cn/apache/atlas/2.2.0
    PAKAGE_NAME=$(http $PAKAGE_LIST_URL | htmlq --attribute href a | grep 'gz\|tar\|zip') # apache-atlas-2.2.0-sources.tar.gz

    PAKAGE_URL=$PAKAGE_LIST_URL$PAKAGE_NAME
    echo $PAKAGE_URL
    mwget $PAKAGE_URL -d /tmp
    l /tmp/$PAKAGE_NAME
    cd /tmp
    tar xvfz $PAKAGE_NAME >/dev/null 2>&1
    FILE_PATH_NAME=$(ls -d * | grep $NAME | grep -v 'gz\|tar\|zip')
    cd $FILE_PATH_NAME
    mvn clean -DskipTests install
    mvn clean -DskipTests package -Pdist,embedded-hbase-solr
    

}
