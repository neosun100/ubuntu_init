install_minio_cli() {
    brew install minio/stable/mc
    mc --help

    wget https://dl.min.io/client/mc/release/linux-amd64/mc
    chmod +x mc
    ./mc --help

    if [ $(uname -m) = "armv7l" ] && [ $(uname) = "Linux" ]; then
        echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为arm Linux" | lcat
        echo "https://dl.min.io/client/mc/release/"
        #   typeset sysname="_linux_arm"
        # 执行代码块
    elif [ $(uname -m) = "x86_64" ] && [ $(uname) = "Linux" ]; then
        echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为X86 Linux" | lcat
        proxy_run brew install minio/stable/mc
        mc --help
        echo "https://dl.min.io/client/mc/release/linux-amd64/mc"
        echo "失败的话，自行下载移动到 /usr/local/bin/"
        echo "chmod +x mc"

        #   typeset sysname="_linux_amd64"
        # 执行代码块
    elif [ $(uname -m) = "x86_64" ] && [ $(uname) = "Darwin" ]; then
        echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为X86 Mac" | lcat
        brew install minio/stable/mc
        mc --help
    else
        echo "$(uname) 系统未知！"
        echo "https://dl.min.io/client/mc/release/"
    fi
}

minio_init() {
    echo "初始化管理账户和mc命令"
}
