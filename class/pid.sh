# 进程id 与 端口号互查
# pid 查进程
# 一个参数为进程id
pid() {
    ps -ef | head -n 1 | cat && ps -ef | fgrepnum 2 "==" ${1:-2} | cat
}

# 进程名查进程id
pname() {
    ps -ef | head -n 1 | cat && ps -ef | grep ${1:-python} | cat
}

#  更好的pname
# 消除了包含 grep pname的问题
# one_pname
opname() {
    ps -ef | head -n 1 | cat && ps -ef | grep ${1:-python} | grep -v "grep" | cat
}

opname2id() {
    sudo ps -ef | grep ${1:-hexo} | grep -v "grep" | sort -k 7 -r | awk '{print $2}' | head -n 1
}

# 需要改成包含关系

# 端口 查 进程与pid
# 一个参数 端口
# 有个隐患，这里查的是全局可访问的端口
# to do
# 需要改成 包含关系更好
port2pid() {
    sudo netstat -plna | awk '{ gsub(/\//,"\t"); print $0 }' | head -n 2 | lcat && sudo netstat -plna | awk '{ gsub(/\//,"\t"); print $0 }' | fgrepstr 4 "==" :::${1:-9000} | cat
}

pname2port() {
    echo "解决包含关系的匹配 awk match 待解决"
}

# Pid查端口号
pid2port() {
    sudo netstat -plna | awk '{ gsub(/\//,"\t"); print $0 }' | head -n 2 | lcat && sudo netstat -plna | awk '{ gsub(/\//,"\t"); print $0 }' | fgrepstr 7 "==" ${1:-2244} | cat
}

# pid kill
pidkill() {
    sudo kill -9 $1
}

pnamekill() {
    [[ $(opname2id ${1:-hexo} | wc -l) -eq 1 ]] && pidkill $(opname2id ${1:-hexo}) || echo "$(date +'%Y-%m-%d %H:%M:%S') ${1:-hexo} 进程不存在" | cat
}

pnamekill_all() {
    for i in seq $(pname ${1:-hexo} | wc -l); do pnamekill ${1:-hexo}; done
}

restart_mac_pinyin() {
    pnamekill "SCIM_Extension -AppleLanguages"
}
