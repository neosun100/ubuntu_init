# 同步cli

install_stow(){
echo "使用 Stow 管理多台机器配置"
echo "https://linux.cn/article-11796-1.html#使用指南"
echo "https://zhuanlan.zhihu.com/p/145281980?utm_source=ZHShareTargetIDMore&utm_medium=social&utm_oi=52708249698304"
echo "https://www.gnu.org/software/stow/"
cd ~/upload
nget http://ftp.gnu.org/gnu/stow/stow-latest.tar.gz
tar -zxvf stow-latest.tar.gz
rm -rf ~/upload/stow-latest.tar.gz
cd ~/upload/stow-*
./configure
sudo make 
sudo make install
stow -h 
cd ~/upload
rm -rf ~/upload/stow-*
}

install_syncthing(){
echo "多设备同步"
echo "https://syncthing.net/downloads/"
# Add the release PGP keys:
curl -s https://syncthing.net/release-key.txt | sudo apt-key add -

# Add the "stable" channel to your APT sources:
echo "deb https://apt.syncthing.net/ syncthing stable" | sudo tee /etc/apt/sources.list.d/syncthing.list

# Update and install syncthing:
sudo apt-get update
sudo apt-get install syncthing
}




