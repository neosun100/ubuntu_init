

# 解压与压缩命令帮助
tar_help(){
    ccat ~/upload/ubuntu_init/class/func/tar.conf | lcat 
}


tar_zip_xz(){
echo "第一个参数为目标dir"
tar -Jvcf "$1.tar.xz" "$1/"  
}

tar_unzip_xz(){
tar -Jvxf $1   
}



xz_help(){
echo -z	强制执行压缩, 默认不保留源文件。压缩后的文件名为源文件.xz | lcat
echo -d	强制执行解压缩 | lcat
echo -l	列出压缩文件的信息 | lcat
echo -k	保留源文件不要删除 | lcat
echo -f	强制覆盖输出文件和压缩链接 | lcat
echo -c	写入到标准输出，输入文件不要删除 | lcat
echo -0..-9	压缩比例，默认为6 | lcat
echo -e  使用更多的 CPU time 来进行压缩，提高压缩率。不会影响解压时所需要的内存。 | lcat
echo -T  指定线程数，默认是 1 ，当设置为 0 时使用和机器核心一样多的线程。 | lcat
echo --format=  指定压缩输出格式，可以是 raw、xz、lzma  | lcat
echo -v	显示更详细的信息 | lcat
}

# 最有压缩命令
nxz(){
num=`sudo cat /proc/cpuinfo |grep 'processor'|wc -l` # 线程数
xz -T `sudo cat /proc/cpuinfo |grep 'processor'|wc -l`  -e  -f --format xz $*
}

# 解压用 unxz 即可


# 依次压缩当前目录下所有文件
n1by1xz(){
for i in $(l | grep tbl | field 9); do nxz $i; echo $i 压缩完成✅; sleep 1; done 
}