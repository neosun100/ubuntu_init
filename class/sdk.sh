install_sdk_cli() {
    echo "for the future give a try to sdkman, is better than brew"
    echo "https://sdkman.io/install"
    curl -s "https://get.sdkman.io" | bash
    source "$HOME/.sdkman/bin/sdkman-init.sh"
    sdk version
    sdk list java
    echo "sdk install java 8.0.392-amzn"

    echo "sdk use java 11.0.14.10.1-amzn"
    echo "cd ~/ && sdk env init"
}

# 显示当前的 Java 版本:
# 要显示当前的默认 Java 版本，运行以下命令：

# bash
# Copy code
# sdk current java
# 显示当前的 Scala 版本:
# 要显示当前的默认 Scala 版本，运行以下命令：

# bash
# Copy code
# sdk current scala