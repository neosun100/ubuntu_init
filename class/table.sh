# 调用func输出表格

# table  无依赖，置顶 profile

alias table="sh $(echo ~)/upload/ubuntu_init/class/func/draw_table.sh -1 -green,-red,-green"

table_help() {
    echo -e "class\tscript\ninput data file\t'table < table.txt'\necho data\t'echo -e \"A\\\tB\\\na\\\tb\"|table'" | table
}

mdtable2csv() {
    # https://github.com/tomroy/mdtable2csv
    python -m ~/upload/ubuntu_init/class/func/mdtable2csv.py ${1:-md_table文件所在路径}
}

# 管道符对接上游 json 列表输出 转 markdown table
# tccli lighthouse DescribeFirewallRules --cli-unfold-argument --region ap-shanghai --InstanceId lhins-rfr3mwti  | jq .FirewallRuleSet | md2f && md2v
# https://github.com/axiros/terminal_markdown_viewer
# mdt() {
#     md-table >/tmp/md-table.md && mdv -A /tmp/md-table.md | lcat
# }

# md 文件流转临时文件
md2f() {
    md-table >/tmp/md-table.md
}

# md 读取临时文件并自定义高亮
md2v() {
    mdv -A /tmp/md-table.md | hcat
}

# 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 
# 🚀 md2f && md2v 🚀 🚀 
# 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 

nmdv() {
    mdv -t 884.0134 ${1:-~/upload/ubuntu_init/README.md}
}
