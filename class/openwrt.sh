# init openwrt.docker.frpc

echo "编辑配置文件 frpc.ini"
mkdir -p /mnt/loop0/frpc
vim /mnt/loop0/frpc/frpc.ini

echo "docker 启动 frpc"
docker run  --restart=always \
--network host \
-d \
-v /mnt/loop0/frpc/frpc.ini:/etc/frp/frpc.ini \
-e TZ=Asia/Shanghai \
--name frpc snowdreamtech/frpc




# init docker manager
docker_manager(){
echo "";
echo "";
echo "🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 ";
echo "`date +'%Y-%m-%d %H:%M:%S'` 检查并创建volume";
docker volume create portainer_data;

echo "";
echo "";
echo "🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 ";
echo "`date +'%Y-%m-%d %H:%M:%S'` 启动 docker 图形管理界面";
docker run -d -p 9000:9000 \
--name portainer \
--restart always \
-v /var/run/docker.sock:/var/run/docker.sock \
-v portainer_data:/data portainer/portainer;

echo "";
echo "";
echo "🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 ";
echo "`date +'%Y-%m-%d %H:%M:%S'`    docker 图形管理web建立完成";
echo "请访问： `iip`:9000";
}