ipfs_start(){
    sh `echo ~`/upload/ubuntu_init/class/func/ipfs_start.sh;
}


ipfs_help(){
    sudo docker exec ipfs_host ipfs -h;
}

ipfs_add(){
cd upload;
cp -r $1 ~/upload/ipfs_stag/  ;
l ~/upload/ipfs_stag/ ;
sudo docker exec ipfs_host ipfs add -r /export/$1 
}

ipfs_get(){

}

ipfs_log(){
sudo docker logs -f ipfs_host;
}


ipfs_close(){
    sudo docker rm 

}