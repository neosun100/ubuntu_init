aws-emr_create-cluster() {
    aws emr create-cluster \
        --auto-scaling-role EMR_AutoScaling_DefaultRole \
        --applications \
        Name=Hadoop \
        Name=Hive \
        Name=Flink \
        Name=Spark \
        Name=Livy \
        Name=Ganglia \
        Name=JupyterEnterpriseGateway \
        Name=Tez \
        Name=Sqoop \
        Name=Trino \
        Name=ZooKeeper \
        --ebs-root-volume-size 100 \
        --ec2-attributes '{"KeyName":"visit-virginia-key","InstanceProfile":"EMR_EC2_DefaultRole","SubnetId":"subnet-019c68e2f66977e0e","EmrManagedSlaveSecurityGroup":"sg-0708b2efbc03b7d46","EmrManagedMasterSecurityGroup":"sg-0708b2efbc03b7d46"}' \
        --service-role EMR_DefaultRole \
        --enable-debugging \
        --release-label emr-6.5.0 \
        --log-uri 's3n://aws-logs-835751346093-us-east-1/elasticmapreduce/' \
        --name 'emr-6.5-tesla-8xlarge-4node' \
        --instance-groups '[{"InstanceCount":1,"EbsConfiguration":{"EbsBlockDeviceConfigs":[{"VolumeSpecification":{"SizeInGB":32,"VolumeType":"gp2"},"VolumesPerInstance":2}]},"InstanceGroupType":"MASTER","InstanceType":"m5.xlarge","Name":"主实例组 - 1"},{"InstanceCount":4,"EbsConfiguration":{"EbsBlockDeviceConfigs":[{"VolumeSpecification":{"SizeInGB":32,"VolumeType":"gp2"},"VolumesPerInstance":2}]},"InstanceGroupType":"CORE","InstanceType":"m5.8xlarge","Name":"核心实例组 - 2"}]' \
        --configurations '[{"Classification":"hive-site","Properties":{"hive.metastore.client.factory.class":"com.amazonaws.glue.catalog.metastore.AWSGlueDataCatalogHiveClientFactory"}},{"Classification":"spark-hive-site","Properties":{"hive.metastore.client.factory.class":"com.amazonaws.glue.catalog.metastore.AWSGlueDataCatalogHiveClientFactory"}}]' \
        --scale-down-behavior TERMINATE_AT_TASK_COMPLETION \
        --region us-east-1

}
