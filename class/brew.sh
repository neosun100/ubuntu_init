# brew 安装与使用
# linux mac

# install_best_brew() {
#     echo "一键解决：自动脚本(全部国内地址)（在Mac os终端中复制粘贴回车下面这句话)"

#     /bin/zsh -c "$(curl -fsSL https://gitee.com/cunkai/HomebrewCN/raw/master/Homebrew.sh)"
# }

# install_linuxbrew() {
#     sh -c "$(curl -fsSL https://raw.githubusercontent.com/Linuxbrew/install/master/install.sh)"
# }

# install_brew() (
#     echo "$(date +'%Y-%m-%d %H:%M:%S') 安装 linuxbrew"
#     sh -c "$(curl -fsSL http://file.neo.pub/sh/brew_install.sh)"
#     echo "$(date +'%Y-%m-%d %H:%M:%S') 安装 linuxbrew 完成"
#     echo "http://linuxbrew.sh/"
#     echo "https://docs.brew.sh/"
#     echo "$(date +'%Y-%m-%d %H:%M:%S') 安装 brew app"
#     sleep 5
#     /home/linuxbrew/.linuxbrew/bin/brew install fzf   # 模糊查找
#     /home/linuxbrew/.linuxbrew/bin/brew install zplug # zsh_插件管理
#     echo "https://github.com/zplug/zplug"
# )

# install_brew_by_tsocks() {
#     echo "$(date +'%Y-%m-%d %H:%M:%S') tsocks 安装 linuxbrew"
#     # sh -c "$(curl -fsSL http://file.neo.pub/sh/brew_install.sh)";
#     cd ~/upload
#     wget http://file.neo.pub/sh/brew_install.sh
#     sudo chmod 777 ./brew_install.sh
#     tsocks ./brew_install.sh
#     echo "$(date +'%Y-%m-%d %H:%M:%S') 安装 linuxbrew 完成"
#     echo "http://linuxbrew.sh/"
#     echo "https://docs.brew.sh/"
#     echo "$(date +'%Y-%m-%d %H:%M:%S') 安装 brew app"
#     sleep 5
#     /home/linuxbrew/.linuxbrew/bin/brew install fzf   # 模糊查找
#     /home/linuxbrew/.linuxbrew/bin/brew install zplug # zsh_插件管理
#     echo "https://github.com/zplug/zplug"
# }

# install_brew_by_cn() {
#     echo "https://gitee.com/cunkai/HomebrewCN"
#     echo "Homebrew 国内自动安装脚本"
#     echo "$(date +'%Y-%m-%d %H:%M:%S') 安装 linuxbrew"
#     /bin/zsh -c "$(curl -fsSL https://gitee.com/cunkai/HomebrewCN/raw/master/Homebrew.sh)"
#     echo "http://linuxbrew.sh/"
#     echo "https://docs.brew.sh/"
#     echo "$(date +'%Y-%m-%d %H:%M:%S') 安装 brew app"
#     sleep 5
#     /home/linuxbrew/.linuxbrew/bin/brew install fzf   # 模糊查找
#     /home/linuxbrew/.linuxbrew/bin/brew install zplug # zsh_插件管理
#     echo "https://github.com/zplug/zplug"
# }

# install_brew_2() {
#     echo "https://www.cnblogs.com/jackey2015/p/10033072.html"
#     ruby -e "$(tsocks curl -fsSL https://raw.githubusercontent.com/Linuxbrew/install/master/install)"
#     tsocks brew tap athrunsun/linuxbinary
# }

fzf_help() {
    echo '# |Token	|Match type	|Description'
    echo '# |sbtrkt	|fuzzy-match	|Items that match sbtrkt'
    echo "# |'wild	|exact-match (quoted)	|Items that include wild"
    echo '# |^music	|prefix-exact-match	|Items that start with music'
    echo '# |.mp3$	|suffix-exact-match	|Items that end with .mp3'
    echo '# |!fire	|inverse-exact-match	|Items that do not include fire'
    echo '# |!^music	|inverse-prefix-exact-match	|Items that do not start with music'
    echo '# |!.mp3$	|inverse-suffix-exact-match	|Items that do not end with .mp3'
}

# brew ug
# mac_ug 同款
bug() {
    tsocks brew update
    tsocks brew upgrade
}

install_brew_pakage() {
    echo "更好的 find"
    brew install fd
    echo "更好的du ncdu"
    brew install nnn
    echo "超厉害的shell编辑器由众多插件"
    echo "https://github.com/jarun/nnn/tree/master/plugins#nnn-plugins"
    brew install diff-so-fancy
    echo "diff -u file_a file_b | diff-so-fancy"
    echo "https://github.com/so-fancy/diff-so-fancy"
}

# $ mysql.server restart
#  ERROR! MySQL server PID file could not be found!
# Starting MySQL
# . SUCCESS!

# brew install nginx // 安装nginx

# brew services list  // 查看服务列表

# brew services restart  nginx // 重启nginx

# brew services stop nginx // 关闭nginx

# brew outdated //查看需要更新的 服务

# brew update // 更新所有服务

# brew uninstall  *** // 卸载 ***

# brew info tmux

# install_brew_2021() {
#     /bin/zsh -c "$(curl -fsSL https://gitee.com/cunkai/HomebrewCN/raw/master/Homebrew.sh)"
# }

install_git-lfs() {
    echo "git 大文件传输工具"
    brew install git-lfs
}

install_jq() {
    brew install jq
    # C
    # https://github.com/jqlang/jq
    brew install gojq
    # golang
    # https://github.com/itchyny/gojq
    brew install jaq
    # rust
    # https://github.com/01mf02/jaq
    brew install jql
    # rust
    # https://github.com/yamafaktory/jql
}

install_macport() {
    cd ~/Downloads
    echo "https://guide.macports.org/#installing.macports"
    mwget https://github.com/macports/macports-base/releases/download/v2.8.1/MacPorts-2.8.1-13-Ventura.pkg
    echo "安装macport"
    echo "https://guide.macports.org/#installing.macports"
    open /Users/jiasunm/Downloads/MacPorts-2.8.1-13-Ventura.pkg
}




install_spack() {
    echo "https://github.com/spack/spack"
}

install_exa() {
    echo "https://bbs.huaweicloud.com/blogs/341701"
    echo "https://github.com/ogham/exa"
    echo "更好的 ls"
    brew install exa
    exa --long --header --git
}

# install_brew_cn_by_sh() {
#     echo "https://zhuanlan.zhihu.com/p/111014448"
#     rm Homebrew.sh
#     wget https://gitee.com/cunkai/HomebrewCN/raw/master/Homebrew.sh
#     bash Homebrew.sh
# }

# uninstall_brew_cn_by_sh() {
#     rm HomebrewUninstall.sh
#     wget https://gitee.com/cunkai/HomebrewCN/raw/master/HomebrewUninstall.sh
#     bash HomebrewUninstall.sh
# }

install_brew_cn_by_sh() {
    echo "https://zhuanlan.zhihu.com/p/111014448"
    rm Homebrew.sh
    curl -o Homebrew.sh https://gitee.com/cunkai/HomebrewCN/raw/master/Homebrew.sh
    bash Homebrew.sh
}

uninstall_brew_cn_by_sh() {
    rm HomebrewUninstall.sh
    curl -o HomebrewUninstall.sh https://gitee.com/cunkai/HomebrewCN/raw/master/HomebrewUninstall.sh
    bash HomebrewUninstall.sh
}

install_terraform() {
    brew tap hashicorp/tap
    brew install hashicorp/tap/terraform
}
