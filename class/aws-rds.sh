aws rds create-db-instance \
    --db-instance-identifier my-test-rds-mysql-instance \
    --db-instance-class db.m6g.4xlarge \
    --engine mysql \
    --master-username root \
    --master-user-password AWS2themoon \
    --allocated-storage 50

aws_rds_remove_cluster() {
    DB_CLUSTER_NAME=${1:-mysql-coin}
    aws rds delete-db-cluster \
        --db-cluster-identifier $DB_CLUSTER_NAME \
        --no-skip-final-snapshot \
        --final-db-snapshot-identifier $DB_CLUSTER_NAME-final-snapshot
}

aws rds delete-db-instance --db-instance-identifier mysql-coin-instance-1 --skip-final-snapshot

删除DB instance
0. aws rds describe-db-instances | jcat
2. aws rds delete-db-cluster --db-cluster-identifier database-2 --skip-final-snapshot

删除DB集群
0. aws rds describe-db-instances | jcat
1. aws rds delete-db-instance --db-instance-identifier mysql-coin-instance-1 --skip-final-snapshot
2. aws rds delete-db-cluster --db-cluster-identifier database-2 --skip-final-snapshot

# aws rds create-db-instance
# [--db-name <value>]
# --db-instance-identifier <value>
# [--allocated-storage <value>]
# --db-instance-class <value>
# --engine <value>
# [--master-username <value>]
# [--master-user-password <value>]
# [--db-security-groups <value>]
# [--vpc-security-group-ids <value>]
# [--availability-zone <value>]
# [--db-subnet-group-name <value>]
# [--preferred-maintenance-window <value>]
# [--db-parameter-group-name <value>]
# [--backup-retention-period <value>]
# [--preferred-backup-window <value>]
# [--port <value>]
# [--multi-az | --no-multi-az]
# [--engine-version <value>]
# [--auto-minor-version-upgrade | --no-auto-minor-version-upgrade]
# [--license-model <value>]
# [--iops <value>]
# [--option-group-name <value>]
# [--character-set-name <value>]
# [--nchar-character-set-name <value>]
# [--publicly-accessible | --no-publicly-accessible]
# [--tags <value>]
# [--db-cluster-identifier <value>]
# [--storage-type <value>]
# [--tde-credential-arn <value>]
# [--tde-credential-password <value>]
# [--storage-encrypted | --no-storage-encrypted]
# [--kms-key-id <value>]
# [--domain <value>]
# [--copy-tags-to-snapshot | --no-copy-tags-to-snapshot]
# [--monitoring-interval <value>]
# [--monitoring-role-arn <value>]
# [--domain-iam-role-name <value>]
# [--promotion-tier <value>]
# [--timezone <value>]
# [--enable-iam-database-authentication | --no-enable-iam-database-authentication]
# [--enable-performance-insights | --no-enable-performance-insights]
# [--performance-insights-kms-key-id <value>]
# [--performance-insights-retention-period <value>]
# [--enable-cloudwatch-logs-exports <value>]
# [--processor-features <value>]
# [--deletion-protection | --no-deletion-protection]
# [--max-allocated-storage <value>]
# [--enable-customer-owned-ip | --no-enable-customer-owned-ip]
# [--custom-iam-instance-profile <value>]
# [--backup-target <value>]
# [--cli-input-json | --cli-input-yaml]
# [--generate-cli-skeleton <value>]
