# hexo  部署 与最优化

#######################
# blog
# 宿主部署

hexo_init_blog(){
sudo rm -rf  ~/upload/neoBlog;
npm install -g hexo-cli;
cd ~/upload;
hexo init neoBlog;
}


hexo_theme_init(){
echo "`date +'%Y-%m-%d %H:%M:%S'` theme next 安装" | lcat;
cd ~/upload;
git clone https://github.com/theme-next/hexo-theme-next ~/upload/neoBlog/themes/next && \
rm -rf ~/upload/neoBlog/themes/next/_config.yml && \
curl https://raw.githubusercontent.com/neosun100/hexo-theme-next/neosun100-patch-1/_config.yml -o ~/upload/neoBlog/themes/next/_config.yml && \
echo "`date +'%Y-%m-%d %H:%M:%S'` hexo config 主题配置文件地址" | lcat;
echo "https://github.com/neosun100/hexo-theme-next/blob/neosun100-patch-1/_config.yml" | lcat;
echo "";
echo "";
echo "`date +'%Y-%m-%d %H:%M:%S'` hexo theme init success" | lcat;
}

hexo_config_init(){
echo "`date +'%Y-%m-%d %H:%M:%S'` admin 安装" | lcat;
cd ~/upload/neoBlog;
npm install --save hexo-admin;
mkdir ~/upload/neoBlog/admin_script;
touch ~/upload/neoBlog/admin_script/hexo-d.sh;
echo 'hexo g && hexo d' > ~/upload/neoBlog/admin_script/hexo-d.sh;
chmod +x ~/upload/neoBlog/admin_script/hexo-d.sh;
echo "";
echo "";
echo "`date +'%Y-%m-%d %H:%M:%S'` 配置文件最优化" | lcat;
rm -rf ~/upload/neoBlog/_config.yml;
curl https://raw.githubusercontent.com/neosun100/hexo-theme-next/neosun100-patch-2/hexo_config.yml -o ~/upload/neoBlog/_config.yml;
echo "`date +'%Y-%m-%d %H:%M:%S'` hexo config 系统配置文件地址" | lcat;
echo "https://github.com/neosun100/hexo-theme-next/blob/neosun100-patch-2/hexo_config.yml" | lcat;
echo "";
echo "";
echo "`date +'%Y-%m-%d %H:%M:%S'` 字体最优化" | lcat;
# 增加字体
sed 's/$font-size-base           = 14px/$font-size-base           = 18px/'  ~/upload/neoBlog/themes/next/source/css/_variables/base.styl;
sed 's/$font-size-headings-base    = 24px/$font-size-headings-base    = 30px/'  ~/upload/neoBlog/themes/next/source/css/_variables/base.styl;
echo "";
echo "";
echo "`date +'%Y-%m-%d %H:%M:%S'` hexo config init success" | lcat;
}



hexo_run(){
git_pull_hexo_oneline;
echo "`date +'%Y-%m-%d %H:%M:%S'` hexo sys blog success" | lcat;
pnamekill hexo;
echo "`date +'%Y-%m-%d %H:%M:%S'` hexo kill success" | lcat;
cd ~/upload/neoBlog;
hexo clear && hexo g && hexo d;
echo "`date +'%Y-%m-%d %H:%M:%S'` hexo reload success" | lcat;
nohup hexo server -i 0.0.0.0 -p 4000 > blog.log 2>&1 &;
cd ~/upload;
echo "`date +'%Y-%m-%d %H:%M:%S'` hexo start success" | lcat;
}


# 只有一个参数
# 是否完全初始化博客，默认为0，不完全初始化，neoBlog重新生成
hexo_deploy(){
if [ ${1-:0} = "0" ] ; then 
hexo_theme_init && hexo_config_init 
else
hexo_init_blog && hexo_theme_init && hexo_config_init 
fi # 判断结束，以fi结尾
}




# cicd模块
hexo_git_gen(){
# git md
# hexo clear && hexo g && hexo d;
}


# 初次执行
# pnamekill hexo && hexo_deploy 1 && hexo_run
# 重启执行
# pnamekill hexo && hexo_run
# 配置更新
# pnamekill hexo && hexo_deploy && hexo_run
# 运行中 文章push
# hexo_git_gen


hexo_head(){
(echo "---" && \
echo "title: ${1:-这里是标题}" && \
echo "mathjax: true" && \
echo "date: `nows`" && \
echo "comments: true #是否可评论" && \
echo "toc: true #是否显示文章目录" && \
echo "categories:  #分类" && \
echo "    - 云服务器" && \
echo "tags:   #标签" && \
echo "    - 转载" && \
echo "    - tomcat" && \
echo "---" && \
echo '<!-- toc -->' && \
echo "# 这里是正文摘要" && \
echo '<!-- more -->' && \
echo "# 这里是正文内容") | bcat
}

hexo_head_no_color(){
(echo "---" && \
echo "title: ${1:-这里是标题}" && \
echo "mathjax: true" && \
echo "date: `nows`" && \
echo "comments: true #是否可评论" && \
echo "toc: true #是否显示文章目录" && \
echo "categories:  #分类" && \
echo "    - 云服务器" && \
echo "tags:   #标签" && \
echo "    - 转载" && \
echo "    - tomcat" && \
echo "---" && \
echo '<!-- toc -->' && \
echo "# 这里是正文摘要" && \
echo '<!-- more -->')
}

hexo_fix_MathJax(){
echo "修复公式显示";
cd ~/upload/neoBlog;
npm uninstall hexo-renderer-marked --save;
npm install hexo-renderer-kramed --save;
time_wait;
echo "详细教程";
echo "https://blog.csdn.net/yexiaohhjk/article/details/82526604";
echo 'vim ~/upload/neoBlog/node_modules/kramed/lib/rules/inline.js' | lcat;
echo '第11行修改如下';
echo '//escape: /^\\([\\`*{}\[\]()#$+\-.!_>])/,' | lcat;
echo 'escape: /^\\([`*\[\]()#$+\-.!_>])/,' | lcat;
echo '第20行修改如下';
echo '//em: /^\b_((?:__|[\s\S])+?)_\b|^\*((?:\*\*|[\s\S])+?)\*(?!\*)/,' | lcat;
echo ' em: /^\*((?:\*\*|[\s\S])+?)\*(?!\*)/,' | lcat;
vim ~/upload/neoBlog/node_modules/kramed/lib/rules/inline.js;

}