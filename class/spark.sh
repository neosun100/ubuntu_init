install_mac_spark() {
  cd ~/upload
  SPARK_VERSION=${1:-3.0.1}
  HADOOP_VERSION=${2:-3.2}
  wget https://downloads.apache.org/spark/spark-${SPARK_VERSION}/spark-${SPARK_VERSION}-bin-hadoop${HADOOP_VERSION}.tgz

  tar -zxvf spark-${SPARK_VERSION}-bin-hadoop${HADOOP_VERSION}.tgz >/dev/null 2>&1
  rm -rf /usr/local/spark
  cp spark-${SPARK_VERSION}-bin-hadoop${HADOOP_VERSION}/* /usr/local/spark
  spark-submit --version
}

# mac linux 通用
install_spark_lastest() {
  cd ~/upload

  # kafka最新版本
  SPARK_VERSION=$(curl $APACHE_HOST'/apache/spark/' \
    -H 'Connection: keep-alive' \
    -H 'sec-ch-ua: "Chromium";v="88", "Google Chrome";v="88", ";Not A Brand";v="99"' \
    -H 'sec-ch-ua-mobile: ?0' \
    -H 'Upgrade-Insecure-Requests: 1' \
    -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 11_2_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.192 Safari/537.36' \
    -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9' \
    -H 'Sec-Fetch-Site: same-origin' \
    -H 'Sec-Fetch-Mode: navigate' \
    -H 'Sec-Fetch-User: ?1' \
    -H 'Sec-Fetch-Dest: document' \
    -H 'Referer: https://mirror-hk.koddos.net/apache/' \
    -H 'Accept-Language: zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7' \
    -s \
    --compressed | grep '20[2-9][0-9]-[0-1][0-9]-[0-3][0-9]' | keycut '<a href="' '/">' | sort -r | head -n 1)
  # KAFKA_VERSION=`http https://mirror-hk.koddos.net/apache/kafka/ | grep   '20[2-9][0-9]-[0-1][1-9]-[0-3][0-9]' | keycut '<a href="' '/">' | sort -r  | head -n 1`

  SPARK_PACKAGE_NAME=$(curl $APACHE_HOST"/apache/spark/$SPARK_VERSION/" \
    -H 'Connection: keep-alive' \
    -H 'sec-ch-ua: "Chromium";v="88", "Google Chrome";v="88", ";Not A Brand";v="99"' \
    -H 'sec-ch-ua-mobile: ?0' \
    -H 'Upgrade-Insecure-Requests: 1' \
    -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 11_2_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.192 Safari/537.36' \
    -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9' \
    -H 'Sec-Fetch-Site: same-origin' \
    -H 'Sec-Fetch-Mode: navigate' \
    -H 'Sec-Fetch-User: ?1' \
    -H 'Sec-Fetch-Dest: document' \
    -H 'Referer: https://mirror-hk.koddos.net/apache/spark/' \
    -H 'Accept-Language: zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7' \
    -s \
    --compressed | grep 'bin' | grep -v 'without' | keycut 'href="' '">spark' | sort -r | head -n 1)

  mwget $APACHE_HOST"/apache/spark/${SPARK_VERSION}/${SPARK_PACKAGE_NAME}"

  tar -zxvf $SPARK_PACKAGE_NAME >/dev/null 2>&1

  [[ -d /usr/local/spark ]] &&
    (sudo rm -rf /usr/local/spark && sudo mkdir /usr/local/spark && sudo chmod -R 777 /usr/local/spark) ||
    (sudo mkdir /usr/local/spark && sudo chmod -R 777 /usr/local/spark)

  SPARK_DIR=$(ls ~/upload | grep spark | grep "bin" | grep -v 'tgz' | sort -r | head -n 1)

  cp -r $SPARK_DIR/* /usr/local/spark
  l /usr/local/spark

  if [ $(uname) = "Darwin" ] && [ -d /usr/local/spark ]; then
    export SPARK_HOME=/usr/local/spark
    export SPARK_CONF_DIR=/usr/local/spark/conf
    export PATH=$PATH:$SPARK_HOME/bin
  elif [ $(uname) = "Linux" ] && [ -d /usr/local/spark ]; then
    export SPARK_HOME=/usr/local/spark
    export PATH=$PATH:$SPARK_HOME/bin
  else
    echo "$(uname)  系统未知 或 还未添加 spark bin"
  fi

  echo "修改 spark-env.sh 解决 WARN Utils"
  l $SPARK_HOME/conf/
  cp -rf $SPARK_HOME/conf/spark-env.sh.template $SPARK_HOME/conf/spark-env.sh
  echo "SPARK_LOCAL_IP=localhost" >>$SPARK_HOME/conf/spark-env.sh

  sourceb
  spark-submit --version

  rm -rf $SPARK_DIR
  rm -rf $SPARK_PACKAGE_NAME

  echo "在pyspark中查看当前的java和scala版本"
  echo ">>> sc._jvm.System.getProperty('java.version')
'17.0.9'
>>> sc._jvm.scala.util.Properties.versionString()
'version 2.12.18'"

}

# archon 下的 spark
nspark-sql() {
  spark-sql \
    --master ${1:-"spark://192.168.100.142:7077"} \
    --name "$(hostname)-$(whoami)-$(nowts)" \
    --driver-memory 2G \
    --driver-cores 2 \
    --total-executor-cores 12 \
    --num-executors 6 \
    --executor-cores 2 \
    --executor-memory 4G
}

npyspark() {
  pyspark \
    --master ${1:-"spark://192.168.100.142:7077"} \
    --name "$(hostname)-$(whoami)-$(nowts)" \
    --driver-memory 2G \
    --driver-cores 2 \
    --total-executor-cores 12 \
    --num-executors 6 \
    --executor-cores 2 \
    --executor-memory 4G
}

nspark-shell() {
  spark-shell \
    --master ${1:-"spark://192.168.100.142:7077"} \
    --name "$(hostname)-$(whoami)-$(nowts)" \
    --driver-memory 2G \
    --driver-cores 2 \
    --total-executor-cores 12 \
    --num-executors 6 \
    --executor-cores 2 \
    --executor-memory 4G
}

nspark-submit() {
  spark-submit \
    --master ${1:-"spark://192.168.100.142:7077"} \
    --name "$(hostname)-$(whoami)-$(nowts)" \
    --driver-memory 2G \
    --driver-cores 2 \
    --total-executor-cores 12 \
    --num-executors 6 \
    --executor-cores 2 \
    --executor-memory 4G \
    $*

}
