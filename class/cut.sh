# 行字段截取
# 默认截取第1行到第9999行
line() {
  #  sed -n "$1,$2p"
  sed -n "${1:-1},${2:-9999}p"
}

# 统计当前文件夹下的文件数
# file num
fn() {
  ls -l | grep "^-" | wc -l
}

# 按字段截取，默认截取第1个字段
field() {
  awk "{print \$${1:-1}}"
}

# 一次获取多个字段  '$1,$2,$3' 作为参数，代表字段1，2，3  切记用单引号
fields() {
  awk "{print ${1:-\$1}}"
}

# 按位置索引截取
ncut() {
  cut -c ${1:-1}-${2:-1000}
}

# 按前后关键字截取
keycut() {
  awk -v head=$1 -v tail=$2 '{print substr($0, index($0,head)+length(head),index($0,tail)-index($0,head)-length(head))}'
}

# 按字段位置批量截取
# 如截取第一个到第五个字段
# 如截取第五个字段到末尾
#  解决了
fcut_help() {
  echo "🎹🎹🎹🎹🎹🎹🎹🎹🎹🎹🎹🎹🎹🎹🎹🎹🎹🎹🎹🎹🎹🎹"
  echo "转换不成函数了，写个 help 吧，用的时候，先打 fcut ,然后改写" | lcat
  echo awk '{for(i=5;i<=100;i++)printf $i" ";printf "\\n"}' | lcat
  echo "awk 的参数 🔑大括号🔑 两边 ➕ 🍙单引号🍙后，方可食用🍲" | lcat
  echo "🎹🎹🎹🎹🎹🎹🎹🎹🎹🎹🎹🎹🎹🎹🎹🎹🎹🎹🎹🎹🎹🎹"
}

# 截取范围内的字段 第一个字段为 1
fcut() {
  awk "{for (i=${1:-1};i<=${2:-100};i++) printf \$i\" \";printf \"\\n\"}"
}

# 文本替换
# awk用法之：文本替换

# awk的sub/gsub函数用来替换字符串，其语法格式是：

# sub(/regexp/, replacement, target)
# 注意第三个参数target，如果忽略则使用$0作为参数，即整行文本。

# 将换行符替换为空格
rformat() {
  xargs
}

# 将空格转化为换行符，格式化输出
tformat() {
  awk '{ gsub(/ /,"\t"); print $0 }'
}

# 将空格转化为逗号，格式化输出
t2dformat() {
  awk '{ gsub(/ /,","); print $0 }'
}

# 将|转化为空，格式化输出
s2kformat() {
  awk '{ gsub(/\|/,""); print $0 }'
}

# 将空格转化为逗号，格式化输出 p point
t2pformat() {
  awk '{ gsub(/ /,"."); print $0 }'
}

# 将空格转化为冒，格式化输出 p point
t2mformat() {
  awk '{ gsub(/ /,":"); print $0 }'
}

# 将斜杠替换成换行符
#
gformat() {
  awk '{ gsub(/\//,"\t"); print $0 }'
}

# 将=替换成换行符
#
dformat() {
  awk '{ gsub(/\=/,"\t"); print $0 }'
}

# 将:替换成换行符
#
mformat() {
  awk '{ gsub(/\\:/,"\t"); print $0 }'
}

# 将.替换成换行符
#
pformat() {
  awk '{ gsub(/\./,"\t"); print $0 }'
}

nformat() {
  awk '{ gsub(/\n/," "); print $0 }'
}

# jq 用 单引号2双引号
# https://www.cnblogs.com/emanlee/p/3620858.html
jqformat() {
  sed 's/'"'"/'"''/g'
}

# jq 用 True False 加 ""
trueformat() {
  sed 's/True/"True"/g'
}

falseformat() {
  sed 's/False/"False"/g'
}

# 过滤所有空格
pass_black() {
  sed 's/ //g'
}

# 过滤所有,
pass_dou() {
  sed 's/,//g'
}

# 过滤所有
pass_dot() {
  sed 's/.//g'
}

# 过滤所有引号
pass_quotes() {
  sed 's/"//g'
}

pass_mao() {
  sed 's/://g'
}

# 将换行md再换行
md_format() {
  tr "\n" "#" | awk '{ gsub(/#/,"\n\n"); print $0 }'
}

# xml 2 json
# 前提安装 pip install xmljson
x2j() {
  if [ $(uname -m) = "armv7l" ]; then
    sudo python3 -m xmljson
  elif [ $(uname -m) = "aarch64" ]; then
    sudo python3 -m xmljson
  elif [ $(uname -m) = "x86_64" ]; then
    python -m xmljson
  else
    echo "$(uname) 系统未知！"
  fi
}

# field grep num 按字段内容 比较 grep 数值信息
# 第一个参数是要查询的字段位置：1，2，3，4，5
# 第二个参数是字段符号 == => != <= < >
# 第三个参数是字段对比值
# ps -ef | fgrepnum 2 "==" 2

fgrepnum() {
  awk "{if(\$${1:-2}+0 ${2:-==} \"${3:-19330}\"+0){print \$0}}"
}

# field grep str 按字段内容 等于或不等于 grep 字符串信息
# 第一个参数是要查询的字段位置：1，2，3，4，5
# 第二个参数是字段符号 == !=
# 第三个参数是字段对比值
#  ps -ef | fgrepstr 8 "==" bash

fgrepstr() {
  awk "{if(\$${1:-8} ${2:-==} \"${3:-bash}\"){print \$0}}"
}

# https://www.cnblogs.com/Aiapple/p/6475265.html
# 截取文件名或截取路经
# 参数1为全路经
cutfname() {
  echo $(basename ${1:-http://gitlab.9tong.com/iquant/eniac})
}
# enaic

# 截取路经名称
cutpname() {
  echo $(dirname ${1:-http://gitlab.9tong.com/iquant/eniac})
}
# http://gitlab.9tong.com/iquant

# 截取数字
cutnum() {
  # grep -Eo "\d+\.\d+"
  grep -Eo "[0-9.]+"
}

cutint() {
  awk '{print int($0)}'
}

# 截取主域名
# -o 只输出符合条件的部分
cuthost() {
  grep -Eo '(http|https)://(www.)?(\w+(\.)?)+'
}
# echo 'https://github.com/xmrig/xmrig/releases' |  grep -P '(http|https)://(www.)?(\w+(\.)?)+' -o
# https://github.com

# 获取指定网址，包含特定信息的文件地址
cut_file_url() {
  url=${1:-https://github.com/xmrig/xmrig/releases} # 二进制项目地址
  # part0="https://github.com"  # 主域名
  part0=$(echo $url | cuthost)                # 主域名
  part1=${2:-/xmrig/xmrig/releases/download/} # 主域名后内容
  # part2=${3:--xenial-x64.tar.gz} # 文件末尾内容
  part2=${3:--bionic-x64.tar.gz}

  partKey=$(http $url | grep -e $part1 | grep -e $part2 | head -n 1 | pass_black | keycut $part1 $part2) # 文件核心部分

  echo "$part0$part1$partKey$part2" # 拼接输出
}

cut_ip() {
  grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}"
}

# 小写转大写
az_2_AZ() {
  tr 'a-z' 'A-Z'
}

# 大写转小写
AZ_2_az() {
  tr 'A-Z' 'a-z'
}

# 🎆 🎆 🎆 🎆 🎆 🎆 🎆 🎆 🎆 🎆 🎆 🎆 🎆 🎆 🎆 🎆 🎆 🎆 🎆 🎆 🎆 🎆 🎆 🎆 🎆 🎆 🎆
# 🎆 🎆 🎆 🎆 🎆 🎆 🎆 🎆 🎆 🎆 🎆 🎆 🎆 🎆 🎆 🎆 🎆 🎆 🎆 🎆 🎆 🎆 🎆 🎆 🎆 🎆 🎆
# 🎆 🎆 🎆 🎆 🎆 🎆 🎆 🎆 🎆 🎆 🎆 🎆 🎆 🎆 🎆 🎆 🎆 🎆 🎆 🎆 🎆 🎆 🎆 🎆 🎆 🎆 🎆

# cut_github_best_file_url 已完成 对以下链接的测试
# https://github.com//docker/compose/releases
# https://github.com/fatedier/frp/releases
# https://github.com/xmrig/xmrig/releases
# https://github.com/Chia-Network/chia-blockchain/releases
# https://github.com/didi/Logi-KafkaManager/releases
# https://github.com/prometheus/prom2json/releases
# https://github.com/vmware-tanzu/velero/releases
# https://github.com/helm/helm/releases

cut_github_best_file_url() {
  FILE_URL=${1:-https://github.com//docker/compose/releases} # 二进制项目地址
  GITHUB_HOST=$(echo $FILE_URL | cuthost)                    # 主域名

  VERSION_LATEST=$(curl -s $FILE_URL | grep '/releases/tag/' | head -n 1 | keycut '">' '</' | field 1)

  if [ $FILE_URL = "https://github.com/prometheus/prom2json/releases" ]; then
    VERSION_LATEST="v$VERSION_LATEST"
  else
    echo ""
  fi
  if [ -f /etc/lsb-release ] && [ $(uname) = "Linux" ] && [ $FILE_URL != "https://github.com/didi/Logi-KafkaManager/releases" ]; then
    FILE_BASE=$(curl -s $FILE_URL | grep "releases/download/$VERSION_LATEST" | grep -v 'sha256\|arm\|386\|freebsd\|mips' | grep "$(lsb_release -c | field 2)\|$(uname -s)\|$(uname -s | AZ_2_az)\|.deb" | keycut ' href="' '" rel' | head -n 1)
  elif [ $(uname -m) = "x86_64" ] && [ $(uname) = "Darwin" ]; then
    FILE_BASE=$(curl -s $FILE_URL | grep "releases/download/$VERSION_LATEST" | grep -v 'sha256\|arm\|386\|freebsd\|mips' | grep "$(uname -s)\|$(uname -s | AZ_2_az)\|.dmg" | keycut ' href="' '" rel' | head -n 1)
  elif [ -f /etc/redhat-release ] && [ $(uname) = "Linux" ]; then
    FILE_BASE=$(curl -s $FILE_URL | grep "releases/download/$VERSION_LATEST" | grep -v 'sha256\|arm\|386\|freebsd\|mips' | grep "$(lsb_release -c | field 2)\|$(uname -s)\|$(uname -s | AZ_2_az)\|.rpm" | keycut ' href="' '" rel' | head -n 1)
  else
    # FILE_BASE=$(curl -s $FILE_URL | grep "releases/download/$VERSION_LATEST" | grep -v 'sha256\|arm\|386\|freebsd\|mips' | grep "$(uname -s)\|$(uname -s | AZ_2_az)" | keycut ' href="' '" rel' | head -n 1)
    FILE_BASE=$(curl -s $FILE_URL | grep "releases/download/$VERSION_LATEST" | grep -v 'sha256\|arm\|386\|freebsd\|mips' | grep "$(uname -s)\|tar.gz\|$(uname -s | AZ_2_az)" | keycut ' href="' '" rel' | head -n 1)
  fi

  echo "$GITHUB_HOST$FILE_BASE" | grep -v '^$'

}

get_fast_github_best_file_url() {
  FILE_URL=${1:-https://github.com//docker/compose/releases} # 二进制项目地址
  GITHUB_FILE_URL=$(cut_github_best_file_url $FILE_URL | grep -v '^$')
  echo "https://github.91chifun.workers.dev/$GITHUB_FILE_URL" | tail -n 1
}

download_fast_github_best_file_url() {
  FILE_URL=${1:-https://github.com//docker/compose/releases} # 二进制项目地址
  OUTPUT_NAME=${2:-./docker-compose}
  sudo curl -L $(get_fast_github_best_file_url $FILE_URL) -o $OUTPUT_NAME
  sudo chmod 777 -R $OUTPUT_NAME
  l $OUTPUT_NAME
}

# 文件检查
# 第一个参数 目标文件路径
# 第二个参数 目标文件不存在需要的源文件路径
check_file() {
  TARGET=${1:-/data/spug/spug_api/db.sqlite3}
  SOURCE=${2:-$(echo ~)/upload/ubuntu_init/docker_file_dir/docker-compose/spug/db.sqlite3}
  TARGET_PATH=$(dirname ${1:-/data/spug/spug_api/db.sqlite3})

  [ -f $TARGET ] &&
    echo "$(date +'%Y-%m-%d %H:%M:%S') $TARGET exist ✅" ||
    (echo "$(date +'%Y-%m-%d %H:%M:%S') $TARGET is not exist ❌" &&
      sudo mkdir -p $TARGET_PATH &&
      sudo chmod 777 $TARGET_PATH &&
      cp $SOURCE $TARGET &&
      sudo chmod 777 $TARGET &&
      echo "$(date +'%Y-%m-%d %H:%M:%S') $TARGET is moved completed ✅")

  # [ -f ${1:-/data/spug/spug_api/db.sqlite3} ] && \
  # echo "`date +'%Y-%m-%d %H:%M:%S'` ${1:-/data/spug/spug_api/db.sqlite3} exist ✅" || \
  # ( echo "`date +'%Y-%m-%d %H:%M:%S'` ${1:-/data/spug/spug_api/db.sqlite3} is not exist ❌" && \
  # sudo mkdir -p $(dirname ${1:-/data/spug/spug_api/db.sqlite3})   && \
  # sudo chmod 777 $(dirname ${1:-/data/spug/spug_api/db.sqlite3})   && \
  # cp ${2:-~/upload/ubuntu_init/docker_file_dir/docker-compose/spug/db.sqlite3} ${1:-/data/spug/spug_api/db.sqlite3} && \
  # sudo chmod 777  ${1:-/data/spug/spug_api/db.sqlite3} && \
  # echo "`date +'%Y-%m-%d %H:%M:%S'` ${1:-/data/spug/spug_api/db.sqlite3} is moved completed ✅" )
}

# 清理文件到指定大小，默认清空
clean_file() {
  sudo truncate -s ${2:-0} $1
  # sudo echo -n "" > access.log
  # sudo dd if=/dev/null of=access.log
  # sudo cat /dev/null > access.log
}

# neo find
# 参数1 路径
# 参数2 关键词通配
# 参数3 深度
nf() {
  sudo find ${1:-/} -maxdepth ${3:-100} -name ${2:-"*-json.log"}
}

#  nf + ls
nfl() {
  sudo ls -lh $(nf $*)
}

# 检查ip是否gg
# gg 为1
ip_dead() {
  echo "$(ping -c 1 -t 1 ${1:-192.168.2.19} | grep "100.0% packet loss" | wc -l | pass_black)"
}

str2list() {
  echo "$(date +'%Y-%m-%d %H:%M:%S')  demo str 2 list"
  echo "🐤🐤🐤🐤🐤🐤🐤🐤🐤🐤🐤🐤🐤🐤🐤🐤🐤🐤🐤🐤🐤"
  echo 'logfiles=($(nfl /var/lib/docker/containers "*-json.log" | field 9 | rformat)) # 文本转数组 文本 分隔符转空格' | lcat
}

nt2ts() {
  python ~/upload/ubuntu_init/pyScript/arrow.s2ts.py --time ${1:-2020/10/11}
}

# 数字取整数
integer() {
  num=${1:-"3.14"}
  echo ${num1%.*}
}

# 数字取小数
decimal() {
  num=${1:-3.14}
  echo ${num1#*.}
}

# 🈷️时间进度函数
# progress rate
# $1 deadline
# $2 num
# $3 total num
mrate() {
  start_seconds="$(nowts)"
  end_seconds=${1:-1602374400}
  num=${2:-40}
  total=${3:-200}
  dateRage=$((end_seconds - start_seconds))
  datestart=$((end_seconds - 2592000))

  # num/total * (dt - t1 / 2592000)

  echo "scale=2; $num/( $total * ($start_seconds - $datestart) /2592000)*100" | bc
}

# neo table
nt() {
  column -t
}

# neo weather
nwt() {
  curl -H "Accept-Language: zh" "http://wttr.in/${1-:shanghai}?m&${2:-3}"
}

# neo dirs num
# 统计文件夹下的目录数
ndn() {
  ls -l ./ | grep "^d" | wc -l | field
}

# 统计文件夹下的目录数 含子目录
nadn() {
  ls -lR | grep "^d" | wc -l | field
}

# neo files num
# 统计文件夹下的文件数
nfn() {
  ls -l ./ | grep "^-" | wc -l | field
}

# 统计 统计文件夹下文件个数，包括子文件
# neo all files num
nafn() {
  ls -lR | grep "^-" | wc -l | field
}

# 列加总
total_sum() {
  awk '{sum+=$1}END{print sum}'
}

# 分隔符等宽
function print_x_axis() {
  local Line= Title= Bytes= Xlength=

  Title=""
  Line=${1:-*}

  if [ -n "${Title}" ]; then
    Bytes=$(echo "${Title}" | wc -c)
  else
    Bytes=1
  fi

  Xlength=$(($(stty size | awk '{print $2}') - ${Bytes}))
  printf '%s ' "${Title}"
  printf "%${Xlength}s\n" "${Line}" | sed "s/ /${Line}/g"
}

# https://blog.csdn.net/ycwyong/article/details/80320070  字段对其

# 行列转换  rs -T

# 矩阵行列转换
Row2column() {
  awk '{for(i=1;i<=NF;i=i+1){a[NR,i]=$i}}END{for(j=1;j<=NF;j++){str=a[1,j];for(i=2;i<=NR;i++){str=str " " a[i,j]}print str}}'
}

get_SSD_data_units_written() {
  for i in $(sudo nvme list | line 3 100 | field 1); do echo $(($(sudo nvme smart-log $i | grep data_units_written | field 3 | awk '{ gsub(/,/,""); print $0 }') / 1024 / 1024))T; done
}

get_SSD_data_units_written_G() {
  for i in $(sudo nvme list | line 3 100 | field 1); do echo $(($(sudo nvme smart-log $i | grep data_units_written | field 3 | awk '{ gsub(/,/,""); print $0 }') / 1024))G; done
}

get_SSD_temperature() {
  for i in $(sudo nvme list | line 3 100 | field 1); do echo $(($(sudo nvme smart-log $i | grep temperature | field 3 | awk '{ gsub(/,/,""); print $0 }')))C; done
}

get_SSD_power_on_hours() {
  for i in $(sudo nvme list | line 3 100 | field 1); do echo $(($(sudo nvme smart-log $i | grep power_on_hours | field 3 | awk '{ gsub(/,/,""); print $0 }')))h; done
}

ssd_top() {
  if [ $(uname) = "Linux" ]; then
    ( 
      (sudo nvme list | grep "Node\|dev" | field 1) | rs -T &&
        (echo Temp && get_SSD_temperature) | rs -T &&
        (echo Written && get_SSD_data_units_written) | rs -T &&
        (echo Used && get_SSD_power_on_hours) | rs -T
    ) | nt | Row2column | nt | lat
  elif [ $(uname) = "Darwin" ]; then
    smartctl -a disk0 | grep "Data\|Temperature\|Available\|Power" | grep -v "Maximum\|Errors\|States" | lat
  else
    echo "$(uname) 系统未知！"
  fi
}

ssd_top_G() {
  ( 
    (sudo nvme list | grep "Node\|dev" | field 1) | rs -T &&
      (echo Temp && get_SSD_temperature) | rs -T &&
      (echo Written && get_SSD_data_units_written_G) | rs -T &&
      (echo Used && get_SSD_power_on_hours) | rs -T
  ) | nt | Row2column | nt | lat
}

# 文件编码转化
change_encode_help() {
  echo "# 文件编码转化"
  echo "cat ko.txt | iconv -f GBK -t UTF-8" | lcat
}

# 行转列
h2l() {
  awk '{for(i=1;i<=NF;i=i+1){a[NR,i]=$i}}END{for(j=1;j<=NF;j++){str=a[1,j];for(i=2;i<=NR;i++){str=str " " a[i,j]}print str}}'
}

# 最后一列

last_field() {
  rev | cut -d' ' -f 1 | rev
}

# 批量字符串首尾添加前后缀

# sed 's/^/<string>/;s/$/<\/string>/'  ip.txt     | njq
# https://blog.csdn.net/qianlong4526888/article/details/8669935
add_head_and_tail() {
  echo '/需要转译 为 \/'

  FILE=$1
  HEAD=$2
  TAIL=$3
  sed "s/^/$HEAD/;s/$/$TAIL/" $FILE
}

avro-tools() {
  java -jar /Users/jiasunm/upload/bin/jars/avro-tools-1.11.0.jar $*
}

# 文件整行替换，已知要替换的行位置
replace_row() {
  FILE=${1:-~/upload/teslaMeta.txt}
  ROW_NUM=${2:-1}
  REPLCAE_RESULT=${3:-id||int||自增id[:inc(id,111)]}
  sed -i "${ROW_NUM}s/.*/${REPLCAE_RESULT}/" ${FILE}
}

cut_filename() {
  basename ${1:-/docker/compose/releases/tag/v2.2.3}
}

pptx_clear_note(){
  python /Users/jiasunm/Code/ubuntu_init/class/func/aws_SSA/remove_ppt_notes.py $*
}

