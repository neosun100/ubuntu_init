# 树莓派系统需要执行

proxy_host="192.168.2.31"
proxy_port="8181"
#alias curl="curl -x $proxy_host:8181"

# alias pip="sudo python3 -m pip --proxy $proxy_host:8181"


# sudo apt-get install pi-bluetooth;
# 🍥🍥🍥🍥🍥🍥🍥🍥🍥🍥🍥🍥🍥🍥🍥🍥🍥🍥🍥🍥🍥
# pi 下执行



# 增加一个 neo 账户
# sudo adduser neo
# sudo usermod -a -G sudo neo
# sudo passwd neo # 改密码
# sudo passwd pi # 改密码
# %o&B9tFhKvpRvdv6UrkSoflN


# 执行初始化脚本
# terminal里


# 安装 docker 包含在 上面了👆
# raspberry_docker.sh




# 解决 manpath: can't set the locale; make sure $LC_* and $LANG are correct
# https://blog.csdn.net/everblog/article/details/79765313




	
# locale




# 超频
# http://wiki.wenyinos.org/index.php/2019/08/02/how-to-overclock-raspberry-pi-4-to-2-0ghz/
# http://tieba.baidu.com/p/6225973772






#  树莓派温度命令
# arm cpu temp
actem(){
sudo /opt/vc/bin/vcgencmd measure_temp;
}


arm_time(){
echo "修改时区";
sudo dpkg-reconfigure tzdata;   
sudo cp /usr/share/zoneinfo/Asia/Shanghai   /etc/localtime; 
}

check_time(){
echo "校准时间";
sudo apt-get install -y ntpdate;
sudo ntpdate us.pool.ntp.org;
}

# 增加字符集
add_locales(){
echo "增加字符集中文"; 
sudo dpkg-reconfigure locales;
}


change_hostname(){
hostname;
sudo vim /etc/hostname;
sudo vim  /etc/hosts
}







# docker 镜像
# https://hub.docker.com/u/arm32v7/



# 测cpu
test_cpu(){
threads_num=`[[ -f /proc/cpuinfo ]] && sudo cat /proc/cpuinfo |grep 'processor'| wc -l  || echo "mac 系统无 /proc/cpuinfo 文件"`;    
# sysbench --test=cpu --cpu-max-prime=10000 --max-time=10 --num-threads=${1:-$threads_num} run;
sysbench cpu --cpu-max-prime=10000 --threads=${1:-$threads_num} run;
}

# 测网速

test_net_server(){
    iperf3 -s;
}

test_net_client(){
echo "https://www.cnblogs.com/hjc4025/p/10438028.html";
iperf3 -c ${1:-192.168.2.24} -p ${2:-5201} -t 20;
# iperf3 -c 192.168.88.248 -p 12345 -i 1 -t 10 -w 100K
}





# 测硬盘
# sudo hdparm -Tt /dev/sda

# http://www.52pi.net/archives/1890#测试


# 超频
# http://wiki.wenyinos.org/index.php/2019/08/02/how-to-overclock-raspberry-pi-4-to-2-0ghz/

update_firmware(){
    echo "固件升级需10分钟";
    echo y | sudo rpi-update;
    echo y | sudo apt dist-upgrade;
}

# 修改  /boot/config.txt 文件 
# 将最大频率提升到2.0 GHz
super_pi(){
echo "固件升级";
update_firmware;
echo "sudo 修改 /boot/config.txt 文件"
sudo vim /boot/config.txt;
# force_turbo=0
# arm_freq=2200
# over_voltage=6

# 重启
sleep 10;
}

test_super(){
echo "当前频率";
sudo vcgencmd get_config int | grep "arm\|over";
echo "当前时钟频率";
sudo vcgencmd measure_clock arm;
echo "当前电压";
sudo vcgencmd measure_volts
echo "多核测试";
test_cpu;
}

#######################################################

# 🧬编译安装python3.8
cmake_python(){
echo "更新系统";
sudo  apt-get  update;
echo y | sudo  apt-get  upgrade;
echo y | sudo apt-get dist-upgrade;
echo "安装python依赖环境";
echo y | sudo apt-get install build-essential \
                              libsqlite3-dev \
                              sqlite3 \
                              bzip2 \
                              libbz2-dev \
                              build-essential \
                              python-dev \
                              python-setuptools \
                              python-pip \
                              python-smbus \
                              build-essential \
                              libncursesw5-dev \
                              libgdbm-dev \
                              libc6-dev \
                              zlib1g-dev \
                              libsqlite3-dev \
                              tk-dev \
                              libssl-dev \
                              openssl \
                              libffi-dev;
echo "下载源码并解压";
cd ~/upload;
mwget https://www.python.org/ftp/python/3.8.0/Python-3.8.0.tar.xz;
tar -Jvxf Python-3.8.0.tar.xz;
echo "编译安装";
cd Python-3.8.0
sudo ./configure
sudo make
sudo make install
echo "检查安装";
ls -al /usr/local/bin/python*;
# 需要加入环境变量，在 raspberry_init.sh
}

root_pip_config(){
echo '[global]';
echo 'trusted-host=pypi.douban.com';
echo 'index-url=http://pypi.douban.com/simple';
echo '[list]';
echo 'format=columns';
sleep 10;
vim ~/.pip/pip.conf;
}


root_install(){
sh /home/neo/upload/ubuntu_init/script/08.*.sh;
}

fix_dpkg_warning(){
    echo "https://blog.csdn.net/u010426270/article/details/52504991";
sudo dpkg --configure -a;
sudo apt-get --reinstall install `dpkg --get-selections | grep '[[:space:]]install' | cut -f1`;
}


root_install_pip_pakages(){
whereis python;
sudo rm /usr/bin/python;
echo $PATH;
sudo ln -s /usr/bin/python3.8 /usr/bin/python;
# sh /home/neo/upload/ubuntu_init/script/08.*.sh;

sudo apt-get install build-essential git cmake pkg-config -y;
sudo apt-get install libjpeg8-dev -y;
sudo apt-get install libtiff5-dev -y;
sudo apt-get install libjasper-dev -y;
sudo apt-get install libpng12-dev -y;
sudo apt-get install libgtk2.0-dev -y;
sudo apt-get install libatlas-base-dev gfortran -y;

sudo apt-get install python3-scipy;

sudo apt-get install python3-opencv;
sudo apt-get install gfortran libatlas-dev;

sudo apt install libatlas-base-dev;
pip install tensorflow;
pip install jupyter;
# https://blog.csdn.net/zhouxzh123/article/details/88652312
# 安装pytorch
}

root_install_pytorch(){
echo "https://github.com/pytorch/pytorch/blob/master/scripts/build_raspbian.sh"; 
export NO_DISTRIBUTED=1;
#!/bin/bash
##############################################################################
# Example command to build the Raspbian target.
##############################################################################
# 
# This script shows how one can build a Caffe2 binary for raspbian. The build
# is essentially much similar to a host build, with one additional change
# which is to specify -mfpu=neon for optimized speed.

CAFFE2_ROOT="$( cd "$(dirname -- "$0")"/.. ; pwd -P)"
echo "Caffe2 codebase root is: $CAFFE2_ROOT"
BUILD_ROOT=${BUILD_ROOT:-"$CAFFE2_ROOT/build"}
mkdir -p $BUILD_ROOT
echo "Build Caffe2 raspbian into: $BUILD_ROOT"

# obtain dependencies.
echo "Installing dependencies."
sudo apt-get install \
  cmake \
  libgflags-dev \
  libgoogle-glog-dev \
  libprotobuf-dev \
  libpython-dev \
  python-pip \
  python-numpy \
  protobuf-compiler \
  python-protobuf
# python dependencies
# sudo pip install hypothesis
pip install hypothesis

# Now, actually build the raspbian target.
echo "Building caffe2"
cd $BUILD_ROOT

# Note: you can add more dependencies above if you need libraries such as
# leveldb, lmdb, etc.
cmake "$CAFFE2_ROOT" \
    -DCMAKE_VERBOSE_MAKEFILE=1 \
    -DCAFFE2_CPU_FLAGS="-mfpu=neon -mfloat-abi=hard" \
    || exit 1

# Note: while Raspberry pi has 4 cores, running too many builds in parallel may
# cause out of memory errors so we will simply run -j 2 only.
make -j 2 || exit 1
}



vim_colorful(){
mv ~/upload/ubuntu_init/profile/.vimrc ~/.vimrc;  
sudo mv ~/upload/ubuntu_init/profile/molokai.vim  /usr/share/vim/vim*/colors/ ;
}


install_miniconda(){
cd ~/upload;
rm -rf ~/miniconda3;
rm -rf ./Miniconda3-latest-Linux-armv7l.sh;
echo "下载miniconda";
mwget http://repo.continuum.io/miniconda/Miniconda3-latest-Linux-armv7l.sh;
echo " 安装miniconda";
sh ./Miniconda3-latest-Linux-armv7l.sh;
echo "环境变量添加miniconda";
export PATH="/home/neo/miniconda3/bin:$PATH";
echo "做优化conda 升级conda";
# conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/free/;
# conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/main/;
conda config --set show_channel_urls yes;
conda update conda;
}


search_wifi(){
sudo iwlist wlan0 scan | grep ESSID | pass_black  ;
}

set_wifi(){
echo "set wifi id and passwd" && \
echo "🐼 🐼 🐼 🐼 🐼 🐼 🐼 🐼 🐼 🐼 🐼 🐼 🐼 🐼 🐼 🐼 " && \
echo "" && \
echo "" && \
echo 'network = {' && \
echo 'ssid ="Neo-iPhone"' && \
echo 'psk ="bilibili"' && \
echo '}';

sleep 10;
sudo vim /etc/wpa_supplicant/wpa_supplicant.conf;
}


# Sub-process /usr/bin/dpkg returned an error code 
fix_Sub-process_dpkg(){
echo "sudo apt-get remove nginx-full && sudo apt-get clean && sudo apt-get install -f  "
}

chrome_driver_path(){
sudo dpkg -L chromium-chromedriver;
}

reinstall_tensorflow(){
echo 'sudo pip3 uninstall tensorflow';
echo 'sudo pip3 install --upgrade tensorflow-1.8.0-cp35-none-linux_armv7l.whl';
}


kill_www-data(){
echo "此账户下起8个进程，消耗资源，怀疑是安装python包导致，强行改名，避免消耗资源，之后重启";
sleep 15;
sudo service apache2 stop;
sudo /etc/init.d/php7.2-fpm stop;
sudo pkill -9 nginx;
sudo pkill -u www-data;
sudo pkill -9 -u www-data;
sudo mv /usr/share/mailman3-web/manage.py /usr/share/mailman3-web/manage.py-neo;
sudo mv /usr/share/unattended-upgrades/unattended-upgrade-shutdown /usr/share/unattended-upgrades/unattended-upgrade-shutdown-neo;
reboot;
}


nctem(){
loop actem
}


color_w(){
    c && \
    w
}

# 循环高亮当前谁在线，执行什么
nw(){
    loop color_w
}


color_cpu(){
    echo $((`sudo vcgencmd measure_clock arm | awk '{ gsub(/\=/,"\t"); print $0 }' | field 2`/1000000)) mhz
}


ncpu(){
    loop color_cpu
}


# 这是个 别名命令 后期改成函数
aport(){
sudo netstat -ntlp tcp
}


sdf(){
df -halT | head -n 1 && df -halT | grep -v "loop\|cgroup" | grep "dev" | grep  "%" | grep -v "udev\|tmpfs"
}




#   jupyter最佳设置
arm_jt_best(){
echo "`date +'%Y-%m-%d %H:%M:%S'` jupyter 主题模块并设置最有配色";
sudo pip3 install jupyterthemes;
# jt -t grade3 -nf georgiaserif -tf droidsans;
# 最佳设置
jt -t monokai -nf georgiaserif -tf droidsans -cellw 90%;
echo "`date +'%Y-%m-%d %H:%M:%S'` jupyter 并行节点模块安装";
sudo pip3 install ipyparallel;

echo "`date +'%Y-%m-%d %H:%M:%S'` jupyter 插件安装";
sudo pip3 install jupyter_contrib_nbextensions;
sudo pip3 install jupyter_nbextensions_configurator;

echo "`date +'%Y-%m-%d %H:%M:%S'` jupyter 插件初始化";
jupyter contrib nbextension install --user --skip-running-check;

echo "`date +'%Y-%m-%d %H:%M:%S'` jupyter 插件初始化 完成 开启 jupyter 进行 设置 参考下图";


if [ `uname` = "Darwin" ]; then
echo "https://gitlab.com/neosun100/ubuntu_init/raw/master/pic/jupyter-config-nbextensions.png" && \
open https://gitlab.com/neosun100/ubuntu_init/raw/master/pic/jupyter-config-nbextensions.png;
elif [ `uname` = "Linux" ]; then
echo "https://gitlab.com/neosun100/ubuntu_init/raw/master/pic/jupyter-config-nbextensions.png";
else
    echo "`uname` 系统未知！"
fi

}



rjupyter(){
    create_dir ~/Resource/Ipynb;
    jupyter notebook --no-browser \
                     --NotebookApp.ip=0.0.0.0 \
                     --port 7777 \
                     --notebook-dir="~/Resource/Ipynb" \
                     --NotebookApp.token=zq \
                     --NotebookApp.iopub_data_rate_limit=99999999999999 \
                     --NotebookApp.allow_origin='*'
}

# 进程名查进程id
pname(){
ps -ef| head -n 1 && ps -ef | grep ${1:-python}
}

opname(){
ps -ef| head -n 1 && ps -ef | grep ${1:-python} | grep -v "grep" 
}

pnamekill(){
[[ `opname2id  ${1:-hexo} | wc -l` -eq 1 ]] && pidkill `opname2id  ${1:-hexo}` || echo "`date +'%Y-%m-%d %H:%M:%S'` ${1:-hexo} 进程不存在"
}

hold_ctb(){
echo "*/1 * * * * sh ~/upload/ubuntu_init/script/1600.frpc_hold_on.sh"   && \
echo "*/1 * * * * sh ~/upload/ubuntu_init/script/1602.jupyter_hold_on.sh"   && \
echo "*/1 * * * * sh ~/upload/ubuntu_init/script/1603.wssh_hold_on.sh"   && \
echo "*/1 * * * * sh ~/upload/ubuntu_init/script/1604.glances_hold_on.sh"  ;

time_wait;
ctb;
}


hold_start(){
sh ~/upload/ubuntu_init/script/1604.glances_hold_on.sh && \
sh ~/upload/ubuntu_init/script/1600.frpc_hold_on.sh && \
sh ~/upload/ubuntu_init/script/1602.jupyter_hold_on.sh && \
sh ~/upload/ubuntu_init/script/1603.wssh_hold_on.sh
}


hold_config(){
mkdir -p ~/Logs && \
chmod -R 777 ~/Logs;

echo "`date +'%Y-%m-%d %H:%M:%S'` jupyter config"  ;
mkdir -p ~/Resource/Ipynb;
/home/neo/.jupyter;
jt_best;
jupyter_gen_config;
echo "`date +'%Y-%m-%d %H:%M:%S'` jupyter config 完成 ✅"  ;
echo "";
echo "";
echo "`date +'%Y-%m-%d %H:%M:%S'` webssh config"  ;
pip install webssh;

echo "`date +'%Y-%m-%d %H:%M:%S'`webssh config 完成 ✅"  ;
echo "";
echo "";
echo "`date +'%Y-%m-%d %H:%M:%S'` frpc config"  ;
frpc_init;
echo "";
echo "";
echo "`date +'%Y-%m-%d %H:%M:%S'` frpc config 完成 ✅"  ;


echo "`date +'%Y-%m-%d %H:%M:%S'` glances config"  ;

if [ `uname -m` = "armv7l" ] && [ `uname` = "Linux" ]; then
  echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为arm Linux"  
  sudo apt-get install glances
#   typeset sysname="_linux_arm"
  # 执行代码块
elif [ `uname -m` = "aarch64" ] && [ `uname` = "Linux" ]; then
  echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为arm64 Linux"  
  sudo apt-get install glances

elif [ `uname -m` = "x86_64" ] && [ `uname` = "Linux" ]; then
  echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为X86 Linux"  
  pip install glances
#   typeset sysname="_linux_amd64"
  # 执行代码块
elif [ `uname -m` = "x86_64" ] && [ `uname` = "Darwin" ]; then
  echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为X86 Mac"  
  pip install glances
#   typeset sysname="_darwin_amd64"
  # 执行代码块
else
  echo "`uname` 系统未知！"
fi
deploy_gotty;
echo "";
echo "";
echo "`date +'%Y-%m-%d %H:%M:%S'` glances config 完成 ✅"  ;
}


hold_status(){
echo "🐼🐼🐼🐼🐼🐼🐼🐼🐼 frpc 🐼🐼🐼🐼🐼🐼🐼🐼🐼"   && \
opname frpc && \
echo "" && \
echo "🐼🐼🐼🐼🐼🐼🐼🐼🐼 glances 🐼🐼🐼🐼🐼🐼🐼🐼🐼"    && \
opname glances && \
echo "" && \
echo "🐼🐼🐼🐼🐼🐼🐼🐼🐼 jupyter 🐼🐼🐼🐼🐼🐼🐼🐼🐼"    && \
opname jupyter && \
echo "" && \
echo "🐼🐼🐼🐼🐼🐼🐼🐼🐼 wssh 🐼🐼🐼🐼🐼🐼🐼🐼🐼"    && \
opname wssh
}


hold_kill(){
pnamekill frpc && pnamekill frpc && pnamekill frpc && \
pnamekill glances && pnamekill glances && pnamekill glances && \
pnamekill jupyter && pnamekill jupyter && pnamekill jupyter && \
pnamekill wssh && pnamekill wssh && pnamekill wssh && \
echo "`date +'%Y-%m-%d %H:%M:%S'` kill all hold on "  ;
}



install_nodejs(){
curl -sL https://deb.nodesource.com/setup_14.x  | sudo -E bash - ;


sudo apt-get install -y nodejs;

sudo apt-get install gcc g++ make ;

curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | sudo apt-key add -;

echo "deb https://dl.yarnpkg.com/debian/ stable main" | sudo tee /etc/apt/sources.list.d/yarn.list;

sudo apt-get update && sudo apt-get install yarn ;
}