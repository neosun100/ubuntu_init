
# 创建目录
# 参数是目录绝对路径
create_dir(){
[[ -d ${1:-/data} ]] && \
( echo "`date +'%Y-%m-%d %H:%M:%S'` 已存在 ${1:-/data} 目录" && l ${1:-/data} ) || \
( echo "`date +'%Y-%m-%d %H:%M:%S'` 不存在 ${1:-/data} 目录 " && sudo mkdir -p ${1:-/data} && sudo chmod 777 ${1:-/data}  && echo "`date +'%Y-%m-%d %H:%M:%S'` ${1:-/data} 目录创建完成" ) 
}


# 创建文本文件
# 略