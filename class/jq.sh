# 嵌套拍扁 嵌套展开 json Flatten a JSON document using jq

jq_flatten() {
    jq --arg delim '.' 'reduce (tostream|select(length==2)) as $i ({};
    [$i[0][]|tostring] as $path_as_strings
        | ($path_as_strings|join($delim)) as $key
        | $i[1] as $value
        | .[$key] = $value
)'
}

# 将 多个json合并
# {"a":1}
# {"a":2}
# ⬇️
# [
#     {"a":1},
#     {"a":2}
# ]

jq_merge() {
    jq -s
}

## jq回填
# https://www.jianshu.com/p/e05a5940f833


so() {
    # smart output 的缩写
    output="$(cat)"
    if echo "$output" | jq empty >/dev/null 2>&1; then
        # 输出是有效的 JSON，使用默认主题
        echo "$output" | jq . | bat --style=plain -l json
    else
        # 输出不是 JSON，使用 Dracula 主题
        echo "$output" | bat --style=plain --theme="Dracula" -l auto
    fi
}