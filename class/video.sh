# ffmpeg 命令行大全

# 查看视频音频文件的播放时长
howlong() {
    ffmpeg -i $1 2>&1 | grep 'Duration' | cut -d ' ' -f 4 | sed s/,//
}

howlong_s() {
    ffmpeg -i $1 2>&1 | grep "Duration" | cut -d ' ' -f 4 | sed s/,// | sed 's@\..*@@g' | awk '{ split($1, A, ":"); split(A[3], B, "."); print 3600*A[1] + 60*A[2] + B[1] }'
}

all_sum() {
    awk '{sum += $1};END {print sum}'
}

howlong_dir_s() {
    for i in $(l | grep '.mp4\|.flv\|.wmv\|.mp3' | field 9); do howlong_s $i; done | all_sum
}

howlong_dir() {
    result=$(howlong_dir_s)                                                                              # 总秒数
    _hour=$(gawk -v x=$result -v y=3600 'BEGIN{printf "%d \n",x/y}')                                     # 时数
    _min=$(gawk -v x=$result -v y=60 -v z=$_hour 'BEGIN{printf "%d \n",(x-z*3600)/y}')                   # 分数
    _sec=$(gawk -v x=$result -v y=60 -v z=$_hour -v w=$_min 'BEGIN{printf "%d \n", x - z*3600 - w*60 }') # 秒数
    echo "文件夹下视频/音频总时长为 $_hour 小时 $_min 分钟 $_sec 秒 " | lcat
}

# 视频音频洞察
insight() {
    ffprobe -v quiet -print_format json -show_format -show_streams ${1:-"/Users/neo/upload/Please Who Is She-ph5ba1083aa2578.mp4"}
}

# colorful 视频音频洞察
cinsight() {
    ffprobe -v quiet -print_format json -show_format -show_streams ${1:-"/Users/neo/upload/Please Who Is She-ph5ba1083aa2578.mp4"} | lcat
}

video_size() {
    ffprobe -v quiet -print_format json -show_format -show_streams ${1:-"/Users/jiasunm/upload/output1.mp4"} | jq -r '.streams[] | select(.codec_type=="video") | "\(.width)x\(.height)"'
}

video_compress(){
    # 将视频压缩为原1/2
    ffmpeg -i ${1:-"/Users/jiasunm/upload/output1.mp4"} -vf scale=iw/2:-1 ${2:-"/Users/jiasunm/upload/output2.mp4"}
}
