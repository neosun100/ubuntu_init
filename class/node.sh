# node js function

install_js_copy-role() {
    cd ~/Code/AWS
    [ -d ~/Code/AWS/aws-iam-copy-role ] && (echo "已安装js module copy role") || (git clone https://github.com/maximivanov/aws-iam-copy-role && cd ~/Code/AWS/aws-iam-copy-role && npm install -g && echo "js module copy role 安装完成" && node ~/Code/AWS/aws-iam-copy-role/copy-role.js -h)
}

# 复制aws role 
copy-role() {
    SOURCE_ROLE_NAME=${1:-SOURCE_ROLE_NAME}
    TARGET_ROLE_NAME${2:-TARGET_ROLE_NAME}
    node ~/Code/AWS/aws-iam-copy-role/copy-role.js $SOURCE_ROLE_NAME $TARGET_ROLE_NAME
}
