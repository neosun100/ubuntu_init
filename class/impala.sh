install_impala-shell() {
    echo "安装 impala-shell "
    echo y | conda create --name impala python=3.8 &&
        source activate impala &&
        pip install impala-shell &&
        conda deactivate
    ~/anaconda3/envs/impala/bin/impala-shell --help
}

impala() {
    ~/anaconda3/envs/impala/bin/impala-shell $*
}
