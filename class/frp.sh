
# ⚽️ ⚽️ ⚽️ ⚽️ ⚽️ ⚽️ ⚽️ ⚽️ ⚽️ ⚽️ ⚽️ ⚽️ ⚽️ ⚽️ ⚽️ ⚽️ ⚽️ ⚽️ ⚽️ ⚽️ ⚽️ ⚽️ ⚽️ 
# server端 
# neo_pub  部署
# paste key 里找 frps.ini
# nginx
# frp docker-compose
# 配置文件是关键，还要加https呢
# 还有blog 整理，头大
# 以上完成 docker 启动








# 🏈 🏈 🏈 🏈 🏈 🏈 🏈 🏈 🏈 🏈 🏈 🏈 🏈 🏈 🏈 🏈 🏈 🏈 🏈 🏈 🏈 🏈 🏈 
# client端 
# phoenix   等 部署
# paste key 里找 frpc.ini
deploy_frpc(){
# 启动 frpc
cd ~/upload;
echo "`date +'%Y-%m-%d %H:%M:%S'` 获取最新下载链接" | lcat;

if [ `uname -m` = "armv7l" ] && [ `uname` = "Linux" ]; then
  echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为arm Linux" | lcat
  typeset subfile="_linux_arm.tar.gz"
#   typeset sysname="_linux_arm"
  # 执行代码块
elif [ `uname -m` = "aarch64" ] && [ `uname` = "Linux" ]; then
  echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为arm64 Linux" | lcat
  typeset subfile="_linux_arm64.tar.gz"

elif [ `uname -m` = "x86_64" ] && [ `uname` = "Linux" ]; then
  echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为X86 Linux" | lcat
  typeset subfile="_linux_amd64.tar.gz"
#   typeset sysname="_linux_amd64"
  # 执行代码块
elif [ `uname -m` = "x86_64" ] && [ `uname` = "Darwin" ]; then
  echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为X86 Mac" | lcat
  typeset subfile="_darwin_amd64.tar.gz"
#   typeset sysname="_darwin_amd64"
  # 执行代码块
else
  echo "`uname` 系统未知！"
fi

file=`cut_file_url https://github.com/fatedier/frp/releases  /fatedier/frp/releases/download/   $subfile`;
echo "";
echo "";
echo "`date +'%Y-%m-%d %H:%M:%S'` 获取链接成功开始下载文件" | lcat;
wget $file;
echo "";
echo "";
echo "`date +'%Y-%m-%d %H:%M:%S'` 下载完成解压缩并重命名" | lcat;
tar zxvf frp_*$subfile;
mkdir ~/upload/frp;
mv ~/upload/frp_*/* ~/upload/frp;
echo "";
echo "";
echo "`date +'%Y-%m-%d %H:%M:%S'` 编辑 ~/upload/frp/frpc.ini文件 模版见 paste" | lcat;
time_wait; #  等待 5秒
vim ~/upload/frp/frpc.ini;
echo "";
echo "";
echo "`date +'%Y-%m-%d %H:%M:%S'` 编辑 ～/upload/frp/frpc.ini文件完成✅" | lcat;
echo "";
echo "";
echo "`date +'%Y-%m-%d %H:%M:%S'` 部署frpc完成✅" | lcat;
}



crontab_add_frpc(){
echo "`date +'%Y-%m-%d %H:%M:%S'` crontab 添加如下" | lcat;
echo "*/2 * * * * sh ~/upload/ubuntu_init/script/1600.frpc_hold_on.sh" | lcat;
time_wait; #  等待 5秒
crontab -e;
sh ~/upload/ubuntu_init/script/1600.frpc_hold_on.sh | lcat;
echo "";
echo "";
echo "🎉 🎉 🎉 🎉 🎉 🎉 🎉 🎉 🎉 🎉 🎉 🎉 🎉 🎉 🎉 🎉 "
echo "`date +'%Y-%m-%d %H:%M:%S'` frpc 部署执行完成 开机状态监控frpc保证运行" | lcat;
}



frpc_init(){
echo "`date +'%Y-%m-%d %H:%M:%S'` 前置条件"  | lcat;
echo "🐣 🐣 🐣 🐣 🐣 🐣 🐣 🐣 🐣 🐣 🐣 🐣 🐣 🐣 ";
echo "1. neo_pub frp-ssh-host 端口打开"    | lcat;
echo "2. client 主机的 ssh ➕ 密钥登陆  执行 ssh_auth"    | lcat;
echo "以上2步❎，先执行 control ➕ c"    | lcat;
time_wait; #  等待 5秒
[[ -d ~/upload/frp ]] && crontab_add_frpc  || (deploy_frpc && crontab_add_frpc)
}