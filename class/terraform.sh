install_terraform() {
    echo "https://www.terraform.io/downloads"

    if [ $(uname) = "Darwin" ]; then
        brew tap hashicorp/tap
        brew install hashicorp/tap/terraform
    elif [ $(uname) = "Linux" ]; then
        curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
        sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
        sudo apt-get update && sudo apt-get install terraform
    else
        echo "$(uname) 系统未知！"
        brew tap hashicorp/tap
        brew install hashicorp/tap/terraform
    fi

    terraform -version              # 查看安装版本
    terraform -install-autocomplete # 可选，如果需要自动补充terraform命令，则执行该命令安装，退出终端再重新进入生效
    npm install -g cdktf-cli --force
    pip install python-terraform -U
    echo "https://github.com/hashicorp/terraform-cdk/blob/main/docs/getting-started/python.md"
}
