#  汇总 demo 的 脚本合集

# 如何汉化jb系列软件？
pychram_set_cn() {
    w_han
    echo 'mv ~/Conf/resources_zh_CN_PyCharm_2019.2_r1.jar /Applications/PyCharm.app/Contents/lib/' | lcat
    echo '🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸'
}

idea_set_cn() {
    w_han
    echo 'mv ~/Conf/resources_zh_CN_IntelliJIDEA_2019.2_r1.jar /Applications/IntelliJ\ IDEA.app/Contents/lib/' | lcat
    echo '🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸'
}

datagrip_set_cn() {
    w_han
    echo 'mv ~/Conf/resources_zh_CN_DataGrip_2019.2_r1.jar /Applications/DataGrip.app/Contents/lib/' | lcat
    echo '🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸'
}

goload_set_cn() {
    w_han
    echo 'mv ~/Conf/resources_zh_CN_GoLand_2019.2_r1.jar /Applications/GoLand.app/Contents/lib/' | lcat
    echo '🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸🐸'
}

# 楞严咒
lyz() {
    cat ~/upload/ubuntu_init/class/func/lengyanzhou.ini
}

cpu() {
    python ~/upload/ubuntu_init/class/func/cpuinfo.py | lcat
}

# 打印copt的机器码
mac_addr() {
    if [ $(uname) = "Darwin" ]; then
        ~/upload/ubuntu_init/class/func/coptmac_mac
    elif [ $(uname) = "Linux" ]; then
        ~/upload/ubuntu_init/class/func/coptmac_linux
    else
        echo "$(uname) 系统未知！"
    fi
}

# 包装下，美化输出
nmac() {
    echo y | mac_addr | lcat
}

# gerapy 高亮显示
# cd ~/upload
# 是否存在 gerapy 目录
# 如果不存在 就  gerapy init 生成目录

# cd gerapy
# 是否存在 sqlite数据库
# 如果不存在 就 gerapy migrate

# gerapy runserver

# 后台运行

# 关闭 后台

install_gerapy() {
    pip install gerapy
    cd ~/upload
    gerapy init # 生成目录
    cd gerapy
    gerapy migrate #   创建数据库
}

open_gerapy() {
    if [ $(pip list | grep gerapy | field) = "gerapy" ]; then
        cd ~/upload/gerapy && (gerapy runserver &) && chrome "http://127.0.0.1:8000"
    else
        echo "尚未安装 gerapy 请执行 install_gerapy" | lcat
    fi
}

close_gerapy() {
    kill -9 $(sudo ps -ef | grep gerapy | sort -k 7 -r | awk '{print $2}' | head -n 1)
}

# 升级 oh-my-zsh
ug_zsh() {
    echo "升级oh-my-zsh"
    zsh ~/.oh-my-zsh/tools/check_for_upgrade.sh
}

# 查询自身或出口ip是否是isp
check_isp() {
    curl -s 'https://ipinfo.io/widget' \
        -H 'authority: ipinfo.io' \
        -H 'sec-ch-ua: "Chromium";v="86", "\"Not\\A;Brand";v="99", "Google Chrome";v="86"' \
        -H 'sec-ch-ua-mobile: ?0' \
        -H 'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 11_0_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36' \
        -H 'accept: */*' \
        -H 'sec-fetch-site: same-origin' \
        -H 'sec-fetch-mode: cors' \
        -H 'sec-fetch-dest: empty' \
        -H 'referer: https://ipinfo.io/' \
        -H 'accept-language: zh-CN,zh;q=0.9,en;q=0.8' \
        --compressed | lcat
}

install_pyspark_highlight() {
    echo "the best python repl ptpython"
    pip install ptpython -U

    echo "配置config"

    if [ $(uname) = "Darwin" ]; then
        mkdir -p "/Users/$(whoami)/Library/Application Support/ptpython"
        cp -f ~/upload/ubuntu_init/pyScript/ptpython_conf.py "/Users/$(whoami)/Library/Application Support/ptpython/config.py"
    elif [ $(uname) = "Linux" ]; then
        mkdir -p ~/.config/ptpython
        cp -f ~/upload/ubuntu_init/pyScript/ptpython_conf.py ~/.config/ptpython/config.py
    elif [ $(uname) = "WindowsNT" ]; then
        echo "Windows pass"
    else
        echo "$(uname) 系统未知！"
    fi

    # pyspark driver 高亮
    export PYSPARK_DRIVER_PYTHON=ptpython
}

gen_SHA256() {
    echo "密码转SHA256"
    echo "https://www.jianshu.com/p/146187593a1a"
    PASSWORD=${1:-$(base64 </dev/urandom | head -c8)}
    echo "$PASSWORD"
    echo -n "$PASSWORD" | sha256sum | tr -d '-'
}

fix_pyspark_highlight_copy() {
    if [ $(uname) = "Darwin" ]; then
        mkdir -p "/Users/$(whoami)/Library/Application Support/ptpython"
        cp -f ~/upload/ubuntu_init/pyScript/ptpython_conf.py "/Users/$(whoami)/Library/Application Support/ptpython/config.py"
    elif [ $(uname) = "Linux" ]; then
        mkdir -p ~/.config/ptpython
        cp -f ~/upload/ubuntu_init/pyScript/ptpython_conf.py ~/.config/ptpython/config.py
    elif [ $(uname) = "WindowsNT" ]; then
        echo "Windows pass"
    else
        echo "$(uname) 系统未知！"
    fi
}
