# 语音模块
# https://blog.csdn.net/MaoshiYIHAO/article/details/72287817
echo y | sudo apt-get install sox # 播放器
echo y | sudo apt-get install libsox-fmt-mp3 #  添mp3支持
echo y | sudo apt-get install sox libsox-fmt-all # all
play -h


sudo apt-get install moc
mocp -h


sudo apt-get install cmus
cmus --help



# Ubuntu使用flite文字转语音 （TTS）
# 需要安装 pulseaudio、libpulse-dev、osspd，不然会报‘failed to open audio device /dev/dsp’ 错误
echo y | sudo apt-get install pulseaudio  && \
echo y | sudo apt-get install libpulse-dev  && \
echo y | sudo apt-get install osspd

# 下载源码并且编译安装
cd ~/upload && \
wget http://file.neo.pub/source/flite-1.4-release.tar.bz2 && \
tar jxvf flite-1.4-release.tar.bz2 && \
cd flite-1.4-release && \
./configure && \
make && \
sudo make install && \
cd ~/upload && 
flite -t "hello neo" 


# 读单词hello
flite -t hello
# 读文本hello.txt
flite hello
# 换女声语音库
flite -voice slt -f hello.txt



# espeak 另一个语音

espeak -h 
# eSpeak text-to-speech: 1.48.03  04.Mar.14  Data at: /usr/lib/x86_64-linux-gnu/espeak-data



# https://linuxtoy.org/archives/festival_on_ubuntu.html
# Ubuntu 之玩转语音合成（Festival）
# 2004 过时


# https://blog.csdn.net/uunubt/article/details/81180596
# 中文语音对话 机器人 在 ubuntu 上的 安装
# https://github.com/wzpan/dingdang-robot


