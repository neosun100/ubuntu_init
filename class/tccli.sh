# 安装腾讯云命令行工具
install_tccli() {
    pipx install tccli
    tccli version
    tccli configure
}

# web端的cli生成器
tccli_web_help() {
    echo "https://console.cloud.tencent.com/api/explorer?Product=lighthouse&Version=2020-03-24&Action=DescribeInstancesTrafficPackages&SignVersion="
    chrome "https://console.cloud.tencent.com/api/explorer?Product=lighthouse&Version=2020-03-24&Action=DescribeInstancesTrafficPackages&SignVersion="
}

w_tencent_console() {
    echo "SH-2core4G8M console"
    chrome "https://console.cloud.tencent.com/lighthouse/instance/detail?rid=4&id=lhins-rfr3mwti"
}

#  aws.xin 当前流量消耗
tc_traffic() {
    tccli lighthouse DescribeInstancesTrafficPackages \
        --cli-unfold-argument \
        --region ap-shanghai | jq '.InstanceTrafficPackageSet[] | .TrafficPackageSet[]' | jcat
}

#  aws.xin 流量已使用百分比
tc_traffic_rate() {
    tccli lighthouse DescribeInstancesTrafficPackages \
        --cli-unfold-argument \
        --region ap-shanghai | jq '.InstanceTrafficPackageSet[] | .TrafficPackageSet[] | .TrafficUsed / .TrafficPackageTotal * 100 | nearbyint '
}

# aws.xin 显示当前安全组列表
tc_firewall_rules() {
    tccli lighthouse DescribeFirewallRules \
        --cli-unfold-argument \
        --region ap-shanghai \
        --InstanceId lhins-rfr3mwti |
        jq .FirewallRuleSet | md2f && md2v
}

tc_firewall_rules_add() {

    tccli lighthouse CreateFirewallRules \
        --cli-unfold-argument \
        --region ap-shanghai \
        --InstanceId lhins-rfr3mwti \
        --FirewallRules.0.Protocol ALL \
        --FirewallRules.0.Port ALL \
        --FirewallRules.0.CidrBlock ${1:-101.88.126.251} \
        --FirewallRules.0.Action ACCEPT \
        --FirewallRules.0.FirewallRuleDescription ${2:-备注信息}
}

check_tc_white_list() {
    echo "目前只支持 aws.xin 轻量级应用服务器"
    InstanceId=${1:-lhins-rfr3mwti}
    IP="$(wip)"

    WHITE_IP_LIST=$(tccli lighthouse DescribeFirewallRules \
        --cli-unfold-argument \
        --region ap-shanghai \
        --InstanceId $InstanceId |
        jq '.FirewallRuleSet[] | select(.Protocol == "ALL") | .CidrBlock' | cut_ip)
    if [[ $WHITE_IP_LIST[@] =~ $IP ]]; then
        echo "$(date +'%Y-%m-%d %H:%M:%S') 🍀  白名单包含本地ip: $IP" | lcat
    else
        echo "$(date +'%Y-%m-%d %H:%M:%S') ❌  白名单不包含本地ip: $IP" | lcat
        LOCATION=$(curl -s http://ip-api.com/json/$IP\?lang\=zh-CN | jq '[del(.status,.zip,.lat,.lon,.timezone,.query,.country,.regionName,.city)]' | md-table | line 3 3 | pass_black | s2kformat | pass_dou)
        TAG_STRING="$(echo $(whoami)_$(nows)_$LOCATION | ncut 1 64)"
        tc_firewall_rules_add $IP $TAG_STRING
        echo "$(date +'%Y-%m-%d %H:%M:%S') ✅  白名单添加本地ip: $IP完成" | lcat
        sddmd "check_aws_white_list" "白名单添加本地ip:$IP完成✅"
    fi

}


# tccli lighthouse CreateFirewallRules \
#     --cli-unfold-argument \
#     --region ap-shanghai \
#     --InstanceId lhins-rfr3mwti \
#     --FirewallRules.0.Protocol TCP \
#     --FirewallRules.0.Port 12889 \
#     --FirewallRules.0.CidrBlock ${1:-0.0.0.0/0} \
#     --FirewallRules.0.Action ACCEPT \
#     --FirewallRules.0.FirewallRuleDescription ${2:-全网12889}

