# 🍎  现在的时间戳与时间
alias nowts='date +"%s"'
# 精确到纳秒
alias nowtns='date +%s.%N'
# 🍎
alias nows='date +"%Y-%m-%d %H:%M:%S"'
# 特殊情况下，不能输出：，则用_代替
alias _nows='date +"%Y-%m-%d🚀%H_%M_%S"'

nowjq() {
    echo $(date +"%Y-%m-%dT%H.%M.%S") | njq
}

# 🍎 时间戳转时间函数
ts2s() {
    if [ $(uname) = "Darwin" ]; then
        sudo killall -HUP mDNSResponder # mac
        date -r$1 +"%Y-%m-%d %H:%M:%S"
    elif [ $(uname) = "Linux" ]; then
        date -d @$1 +"%Y-%m-%d %H:%M:%S" # linux
    else
        echo "$(uname) 系统未知！"
    fi
}

# 🍎 时间转时间戳函数
s2ts() {
    if [ $(uname) = "Darwin" ]; then
        echo "mac 不支持此命令，等待 Neo 把 pendulum 整合 CLI"
    elif [ $(uname) = "Linux" ]; then
        # date -d $1 +"%s" # linux
        date --date=$1 +%s
    else
        echo "$(uname) 系统未知！"
    fi
}

#$ s2ts  "2018-02-26 15:41:45"
#1519630905

#$ ts2s 1519630905
# 2018-02-26 15:41:45

# need ts
# 按需获取指定距当前时间的时间戳
nts() {
    date --date=${1:-"1 minute ago"} +'%s'
}

ntns() {
    date --date=${1:-"1 minute ago"} +'%s.%N'
}

# $ nts "2 day ago"
# $ nts '2 day ago'
# 1583949600

install_cost() {
    npm install -g gnomon
}

# alias cost=gnomon

# 显示开机启动耗时
startup_time() {
    systemd-analyze blame | lat
}
