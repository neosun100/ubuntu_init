# 🐧 显示所有磁盘路径，含物理，逻辑
# alias ndf='df -halT | lcat'

adf() {
    if [ $(uname) = "Darwin" ]; then
        df -ha | lcat
    elif [ $(uname) = "Linux" ]; then
        # df -halT | lcat;
        df -ha | lcat
    else
        echo "$(uname) 系统未知！"
    fi
}

# 🐧 仅显示物理磁盘
# alias sndf='df -halT | head -n 1 | bcat && ndf | grep -v "loop\|cgroup" | grep "dev" | grep  "%" | grep -v "udev\|tmpfs" '

sdf() {
    if [ $(uname) = "Darwin" ]; then
        df -ha | head -n 1 | bcat && adf | grep -v "loop\|cgroup" | grep "dev\|neo" | grep "%" | grep -v "udev\|tmpfs" | grep -v 'efi'
    elif [ $(uname) = "Linux" ]; then
        df -ha | head -n 1 | bcat && adf | grep -v "loop\|cgroup" | grep "dev\|neo" | grep "%" | grep -v "udev\|tmpfs" | grep -v 'efi'
    else
        echo "$(uname) 系统未知！"
    fi
}

# 🐧 仅显示非 loop 磁盘 logic neo df
# alias lndf='df -halT | head -n 1 | bcat && ndf | tail -n +2  | grep "%" | grep -v "loop\|cgroup" | ns '

ldf() {
    if [ $(uname) = "Darwin" ]; then
        df -ha | head -n 1 | bcat && adf | tail -n +2 | grep "%" | grep -v "loop\|cgroup\|disk" | ns
    elif [ $(uname) = "Linux" ]; then
        df -halT | head -n 1 | bcat && adf | tail -n +2 | grep "%" | grep -v "loop\|cgroup" | ns
    else
        echo "$(uname) 系统未知！"
    fi
}

grep_ip() {
    grep -E "([0-9]{1,3}[\.]){3}[0-9]{1,3}"
}

# 仅显示网络挂载磁盘
netdf() {
    if [ $(uname) = "Darwin" ]; then
        df -ha | head -n 1 | bcat && df -ha | tail -n +2 | grep -E "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | ns | bcat
    elif [ $(uname) = "Linux" ]; then
        df -halT | head -n 1 | bcat && df -halT | tail -n +2 | grep -E "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | ns | bcat
    else
        echo "$(uname) 系统未知！"
    fi
}


# 关闭交换空间
rm_swap() {
    free -h
    sudo swapoff -a
    free -h
}
