# npm 的网页格式转化神器
# https://developer.aliyun.com/mirror/npm/package/zignis-plugin-read

install_zignis(){
cd ~/upload;
npm i -g zignis 
npm i -g  puppeteer # 会安装谷歌无头浏览器  Chromium 
cnpm install -g zignis-plugin-read
}

# html 2 markdown
h2m(){
cd ~/upload/html_MD;
URL=${1:-'https://blog.csdn.net/weixin_41732074/article/details/88747400'}
zignis read $URL --format=markdown;
# MD=`l -r | line 2 2 | field 9`
# MD=`ls -lt |  line 2 2 | field 9` 
MD=`ls -t | head -n 1 ` 
pbcopy < $MD;
TITLE=`basename $MD .md`
hexo_head $TITLE
# echo "tfidf 提取关键词";
# python -c "import jieba.analyse;file=$MD;text = open(file,'r').read();print(jieba.analyse.extract_tags(text,topK=23,allowPOS=['ns', 'n', 'vn', 'v','nr']))"
cd ~/upload;
}


format_title(){
echo $1 | awk '{ gsub(/[,。，/ !！?？&\t\n%#=&，:：“‘～`]/,"_"); print $0 }' 
}


h2b(){
cd ~/upload/html_MD;
URL=${1:-'https://blog.csdn.net/weixin_41732074/article/details/88747400'}
zignis read $URL --format=markdown;
# MD=`l -r | line 2 2 | field 9`
# MD=`ls -lt |  line 2 2 | field 9` 
MD=`ls -t | head -n 1 ` 
# pbcopy < $MD;
echo "`date +'%Y-%m-%d %H:%M:%S'` 原文件名 $MD";
NMD=`format_title $MD`
echo "`date +'%Y-%m-%d %H:%M:%S'` 新文件名 $NMD";

python ~/upload/ubuntu_init/pyScript/deal_md.py --source ~/upload/html_MD/$MD;

scat test.txt > $MD;
rm -rf ./test.txt;

[[ -e ~/Code/source/_posts/转载_`date +"%s"`_$NMD ]] && echo "`date +'%Y-%m-%d %H:%M:%S'` blog $NMD 已存在"  || touch ~/Code/source/_posts/转载_`date +"%s"`_$NMD
TITLE=`basename $MD .md`
HEAD=`hexo_head_no_color $TITLE`
# echo "清空当前博客内容文本"
# echo "" > /Users/neo/Code/source/_posts/$MD
echo "`date +'%Y-%m-%d %H:%M:%S'` 写入blog head"
echo $HEAD > ~/Code/source/_posts/转载_`date +"%s"`_$NMD
echo "`date +'%Y-%m-%d %H:%M:%S'` 写入文章主体"
scat $MD | line 5 >> ~/Code/source/_posts/转载_`date +"%s"`_$NMD
echo "`date +'%Y-%m-%d %H:%M:%S'` 转化存储 html2blog 完成✅"

if [ ${2:-1} -eq "1" ] ; then 
echo "`date +'%Y-%m-%d %H:%M:%S'` 转化当前文件图片为七牛云照片"
python ~/upload/script/replace_md_pic.py ~/Code/source/_posts/转载_`date +"%s"`_$NMD 1
rm -rf ~/Code/source/_posts/转载_*_$NMD.bak
echo "`date +'%Y-%m-%d %H:%M:%S'` 转化当前文件图片为七牛云照片完成✅"
else
echo "`date +'%Y-%m-%d %H:%M:%S'` 不进行图片迁移"
fi



# echo "tfidf 提取关键词";
# python -c "import jieba.analyse;file=$MD;text = open(file,'r').read();print(jieba.analyse.extract_tags(text,topK=23,allowPOS=['ns', 'n', 'vn', 'v','nr']))"
cd ~/upload;
}