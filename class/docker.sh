# 前提
# 必须在 cut.sh 之后

# 登录loop 阿里云 docker
login_loop() {
  sudo docker login --username=loop-dev@1284210417798820 \
    registry.cn-shanghai.aliyuncs.com
}

# 补充登陆 自己的账户
login_neo() {
  # sudo docker login --username=xinwuchen@vip.qq.com \
  #                   registry.cn-hangzhou.aliyuncs.com
  sudo docker login --username=xinwuchen@vip.qq.com registry.cn-shanghai.aliyuncs.com
}

login_neosun() {
  sudo docker login -u neosun
}

w_ali_docker() {
  echo " 登陆阿里云镜像仓库web端，用于check镜像和创建新仓库"
  chrome "https://cr.console.aliyun.com/cn-shanghai/instances/repositories"
}

init_docker_gpu() {
  sh ~/upload/ubuntu_init/class/func/docker_gpu_init.sh
}

# 第一个参数是镜像id
# 第二个参数是镜像名称和版本

ntag() {
  sudo docker tag ${1:-镜像id} registry.cn-shanghai.aliyuncs.com/neo_docker/${2:-镜像名称\:标签版本}
}

npush() {
  echo " 推送镜像"
  # $ sudo docker login --username=xinwuchen@vip.qq.com registry.cn-shanghai.aliyuncs.com
  # $ sudo docker tag [ImageId] registry.cn-shanghai.aliyuncs.com/neo_docker/neo-gpu-jupyter:[镜像版本号]
  # $ sudo docker push registry.cn-shanghai.aliyuncs.com/neo_docker/neo-gpu-jupyter:[镜像版本号]
  sudo docker tag ${1:-6ee3dcbde96602c052426d69bdc8964c1994af7393fc9b549b6cf9829a979ac6} registry.cn-shanghai.aliyuncs.com/neo_docker/${2:-neo-gpu-jupyter}
  sudo docker login --username=xinwuchen@vip.qq.com registry.cn-shanghai.aliyuncs.com
  sudo docker push registry.cn-shanghai.aliyuncs.com/neo_docker/${2:-neo-gpu-jupyter}

}

npull() {
  echo "拉取镜像"
  # sudo docker pull registry.cn-shanghai.aliyuncs.com/neo_docker/neo-gpu-jupyter:[镜像版本号]
  sudo docker pull registry.cn-shanghai.aliyuncs.com/neo_docker/${1:-neo-gpu-jupyter\:latest}
}

# 不要执行，只是缺少执行 1001.docker_gpu
# fix_docker_error_Unknown_runtime_specified_nvidia(){
# echo "https://blog.csdn.net/weixin_32820767/article/details/80538510";
# echo "nvidia-docker 没有注册";
# echo "";
# echo "";
# echo "Systemd drop-in file";
# sudo mkdir -p /etc/systemd/system/docker.service.d;
# sudo tee /etc/systemd/system/docker.service.d/override.conf <<EOF
# [Service]
# ExecStart=
# ExecStart=/usr/bin/dockerd --host=fd:// --add-runtime=nvidia=/usr/bin/nvidia-container-runtime
# EOF;
# sudo systemctl daemon-reload;
# sudo systemctl restart docker;

# echo "";
# echo "";
# echo "Daemon configuration file";
# sudo tee /etc/docker/daemon.json <<EOF
# {
#     "runtimes": {
#         "nvidia": {
#             "path": "/usr/bin/nvidia-container-runtime",
#             "runtimeArgs": []
#         }
#     }
# }
# EOF;
# sudo pkill -SIGHUP dockerd;
# }

# 清理 docker 日志
clear_docker_log() {
  echo "======== start clean docker containers logs ========"

  logs=$(find /var/lib/docker/containers/ -name *-json.log)

  for log in $logs; do
    echo "clean logs : $log"
    cat /dev/null >$log
  done

  echo "======== end clean docker containers logs ========"
}

# 进入运行态容器 container
# 如果没有指定 container name，则进入第一个 container
ninto() {
  sudo docker exec -it ${1:-$(sudo docker ps -la | field | line 2)} /bin/sh
}

nexec() {
  sudo docker exec -it $*
}

# 指定容器运行任务
nrun() {
  sudo docker exec $*
}

# 展示 指定 container 的 标准输出
# 默认现实第一个 container 的 最近 30 条 标准输出
nlog() {
  sudo docker logs --tail ${2:-30} -ft ${1:-$(sudo docker ps -la | field | line 2)}
}

# 显示当前所有 container 状态
nlist() {
  sudo docker ps -a | lcat
}

# 显示所有 volume 信息
nvlist() {
  sudo docker volume ls | lcat
}

# 显示所有 network 信息
nnlist() {
  sudo docker network ls | lcat
}

nclean() {
  echo "清理未使用的 volume"
  echo y | sudo docker volume prune
  echo "清理未使用的 network"
  echo y | sudo docker network prune
  echo "删除空镜像"
  clear_images
  echo "删除死亡的容器"
  clear_containers
}

nvrm() {
  echo "删除volume"
  sudo docker volume rm $*
}

nnrm() {
  echo "删除network"
  sudo docker network rm $*
}

# 显示具体的容器组件明细
# id 名称 短id🆔
nid() {
  sudo docker inspect $1
}

# 显示当前所有 image 信息
nilist() {
  sudo docker images --all --no-trunc | lcat
}

# 删除容器
nrm() {
  sudo docker rm -f $*
}

# 删除image
nrmi() {
  sudo docker rmi -f $*
}

nrm_by_name() {
  sudo docker rm -f $(sudo docker ps -a | grep $1 | field 1)
}

nrmi_by_name() {
  sudo docker rmi -f $(sudo docker images --all | grep $1 | field 1)
}

# 复制容器中的目录到宿主目录
# 第一个参数为容器路径
# 第二个参数为宿主路径
ncp() {
  sudo docker container cp $1 $2
}

# 删除死容器
clear_containers() {
  exited_containers_num=$(sudo docker ps -a | grep "Exited" | awk "{print ${1:-\$1}}" | wc -l)
  # 目标 containers 数量

  if [ $exited_containers_num -eq 0 ]; then
    echo "不存在 Exited container"
  else
    sudo docker rm -f $(sudo docker ps -a | grep "Exited" | awk "{print ${1:-\$1}}")
  fi
}

# 删除空images
# 删除空镜像
clear_images() {
  clear_containers
  # none_images_num=$(sudo docker images --all | grep "^<none>" | awk "{print $3}"  | wc -l)
  none_images_num=$(sudo docker images --all | grep "^<none>" | fields '$3' | wc -l)
  # 目标 images 数量

  if [ $none_images_num -eq 0 ]; then
    echo "不存在 <none> image"
  else
    # sudo docker rmi $(sudo docker images --all  | grep "^<none>" | awk "{print $3}")
    sudo docker rmi $(sudo docker images --all | grep "^<none>" | fields '$3')
  fi
}

# docker 里的gpu jupyter 相当于完美gpu镜像
gjupyter() {
  sudo docker run --runtime=nvidia \
    -it \
    --rm \
    -v $(realpath /Ipynb):/tf \
    -v /etc/localtime:/etc/localtime:ro \
    -p 7777:7777 \
    --name neo-gpu-jupyter \
    registry.cn-shanghai.aliyuncs.com/neo_docker/neo-gpu-jupyter \
    /bin/bash -c "/opt/conda/bin/jupyter notebook --allow-root --notebook-dir=/tf --NotebookApp.ip=0.0.0.0 --port=7777 --no-browser --NotebookApp.token=bilibili --NotebookApp.iopub_data_rate_limit=99999999999999 --NotebookApp.allow_origin='*' "
}

# official docker 镜像
official_jupyter() {
  sudo docker run --runtime=nvidia \
    -it \
    --rm \
    -v $(realpath /Ipynb):/tf \
    -v /etc/localtime:/etc/localtime:ro \
    -p 8888:8888 \
    --name gpu-jupyter \
    tensorflow/tensorflow:latest-gpu-py3-jupyter \
    /bin/bash -c "jupyter notebook --allow-root --notebook-dir=/tf --NotebookApp.ip=0.0.0.0 --port=8888 --no-browser --NotebookApp.token=bilibili --NotebookApp.iopub_data_rate_limit=99999999999999 --NotebookApp.allow_origin='*' "
}

docker_volume() {
  echo "/var/lib/docker/volumes"
  l /var/lib/docker/volumes
}

# 在mac上用docker会在mac上启动一个虚拟机运行docker，因此volume创建的directory并不在你的machine上，而是在虚拟机中。
docker_into_mac() {
  screen ~/Library/Containers/com.docker.docker/Data/vms/0/tty
  # 当你的docker version是18.06.0-ce-mac70 (26399)采用上面的指令，如果不是这个version请使用
  # screen ~/Library/Containers/com.docker.docker/Data/com.docker.driver.amd64-linux/tty
}

docker_manager() {
  echo ""
  echo ""
  echo "🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 "
  echo "$(date +'%Y-%m-%d %H:%M:%S') 检查并创建volume"
  [[ $(sudo docker volume ls | grep portainer_data | grep local | fields '$2') = "portainer_data" ]] && echo "$(date +'%Y-%m-%d %H:%M:%S') volume 已创建" || sudo docker volume create portainer_data

  echo ""
  echo ""
  echo "🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 "
  echo "$(date +'%Y-%m-%d %H:%M:%S') 启动 docker 图形管理界面"
  sudo docker run -d -p 8000:8000 -p 9000:9000 \
    --name portainer \
    --restart always \
    -v $(realpath /var/run/docker.sock):/var/run/docker.sock \
    -v $(realpath portainer_data):/data \
    portainer/portainer-ce

  sudo docker run -d -p 9001:9001 \
    --name portainer_agent \
    --restart=always \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v /var/lib/docker/volumes:/var/lib/docker/volumes \
    portainer/agent

  echo ""
  echo ""
  echo "🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 "
  echo "$(date +'%Y-%m-%d %H:%M:%S')    docker 图形管理web建立完成"
  echo "请访问： http://$(iip):9000"
}

docker_netdata() {
  sudo docker run -d --cap-add SYS_PTRACE \
    -v /proc:/host/proc:ro \
    -v /sys:/host/sys:ro \
    -p 19999:19999 \
    --restart unless-stopped \
    --name netdata \
    titpetric/netdata

  echo ""
  echo ""
  echo "🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 "
  echo "$(date +'%Y-%m-%d %H:%M:%S')    docker 图形管理web建立完成"
  echo "请访问： $(iip):19999"
}

mac_docker_manager() {
  echo ""
  echo ""

  echo "$(date +'%Y-%m-%d %H:%M:%S') 检查并创建volume"
  echo "🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 "
  [[ $(sudo docker volume ls | grep portainer_data | grep local | fields '$2') = "portainer_data" ]] && echo "$(date +'%Y-%m-%d %H:%M:%S') volume 已创建" || sudo docker volume create portainer_data
  # echo "mac Users 路经不用添加读写权限";
  # echo "docker 在 mac 里的 volume 是在 虚拟机里";
  # echo " 这里不用 volume  创建空间";
  # echo " 而同一创建在 ～/Dokcer下";
  # echo "`date +'%Y-%m-%d %H:%M:%S'` 检查并创建 ~/Docker/portainer_data ";
  # HOST_DIR="`echo ~`"
  # [[ -d $HOST_DIR/Docker/portainer_data ]] && \
  # echo "`date +'%Y-%m-%d %H:%M:%S'` ~/Docker/portainer_data 已创建" || \
  # ( echo "`date +'%Y-%m-%d %H:%M:%S'` ~/Docker/portainer_data 不存在" && mkdir -p $HOST_DIR/Docker/portainer_data && chmod  -R 777 $HOST_DIR/Docker && echo "`date +'%Y-%m-%d %H:%M:%S'` ~/Docker/portainer_data 已创建并777" );
  echo ""
  echo ""
  echo "🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 "
  echo "$(date +'%Y-%m-%d %H:%M:%S') 启动 docker 图形管理界面"
  sudo docker run -d -p 9000:9000 \
    --name portainer \
    --restart always \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v portainer_data:/data \
    portainer/portainer

  echo ""
  echo ""
  echo "🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 "
  echo "$(date +'%Y-%m-%d %H:%M:%S')    docker 图形管理web建立完成"
  echo "请访问： $(iip):9000"
}

docker_fastdfs() {
  # 启动哨兵容器
  sudo docker run -ti -d --name trakcer \ 
  -v /data/fastdfs/tracker_data:/fastdfs/tracker/data \ 
  --net=host \ 
  season/fastdfs \ 
  tracker

  # 启动存储容器
  sudo docker run -tid --name storage \ 
  -v /data/fastdfs/storage_data:/fastdfs/storage/data \ 
  -v /data/fastdfs/store_path:/fastdfs/store_path \ 
  --net=host \ 
  -e TRACKER_SERVER:192.168.111.110:22122 \ 
  season/fastdfs \ 
  storage

  # 更新存储容器节点，并重启服务生效
  # sudo docker cp storage:/fdfs_conf/storage.conf ~/
  # sudo vim ~/storage.conf
  sudo docker cp ~/storage.conf storage:/fdfs_conf/
  sudo docker restart storage

  #  查看存储配置文件
  # sudo docker exec -it storage bash
  ninto storage
  fdfs_monitor /fdfs_conf/storage.conf

  # 客户端
  sudo docker run -ti --name fdfs_sh --net=host season/fastdfs sh

  ninto fdfs_sh

  # 客户端命令列表
  ls -lah /usr/local/bin/

  # 上传文件 返回url
  fdfs_upload_file /fdfs_conf/storage.conf /neo.txt
  # group1/M00/00/00/wKgCDF2vIo-AA5lgAAAACWPMXSU337.txt
  # group1/M00/00/00/rBEAA12vMh-ACczkAAAACWPMXSU457.txt
  # 宿主查看路经
  ls -lah /data/fastdfs/store_path/data/00/00/wKgCDF2vIo-AA5lgAAAACWPMXSU337.txt

  # 必须经过nginx访问
}

docker_fastdfs_one() {
  sudo docker run -d --restart=always \
    --privileged=true \
    --net=host \
    --name=fastdfs \
    -e IP=47.95.234.255 \
    -e WEB_PORT=80 \
    -v ${HOME}/fastdfs:/var/local/fdfs
  registry.cn-beijing.aliyuncs.com/tianzuo/fastdfs

  # 其中-v ${HOME}/fastdfs:/var/local/fdfs是指：
  # 将${HOME}/fastdfs这个目录挂载到容器里的/var/local/fdfs这个目录里。
  # 所以上传的文件将被持久化到${HOME}/fastdfs/storage/data里，
  # IP 后面是自己的服务器公网ip或者虚拟机ip，
  # -e WEB_PORT=80 指定nginx端口
  # ————————————————
  # 版权声明：本文为CSDN博主「VicterTian」的原创文章，遵循 CC 4.0 BY-SA 版权协议，转载请附上原文出处链接及本声明。
  # 原文链接：https://blog.csdn.net/tttzzztttzzz/article/details/86709318
}

# ninto fastdfs

# echo "Neo : Hello FastDFS!" > index.html

# ash-4.4# fdfs_test /etc/fdfs/client.conf upload index.html
# This is FastDFS client test program v5.12

# Copyright (C) 2008, Happy Fish / YuQing

# FastDFS may be copied only under the terms of the GNU General
# Public License V3, which may be found in the FastDFS source kit.
# Please visit the FastDFS Home Page http://www.csource.org/
# for more detail.

# [2019-10-22 16:57:02] DEBUG - base_path=/var/local/fdfs/storage, connect_timeout=30, network_timeout=60, tracker_server_count=1, anti_steal_token=0, anti_steal_secret_key length=0, use_connection_pool=0, g_connection_pool_max_idle_time=3600s, use_storage_id=0, storage server id count: 0

# tracker_query_storage_store_list_without_group:
#         server 1. group_name=, ip_addr=192.168.2.12, port=23000

# group_name=group1, ip_addr=192.168.2.12, port=23000
# storage_upload_by_filename
# group_name=group1, remote_filename=M00/00/00/wKgCDF2vNN6ABx2TAAAAFd46oKA93.html
# source ip address: 192.168.2.12
# file timestamp=2019-10-22 16:57:02
# file size=21
# file crc32=3728384160
# example file url: http://192.168.2.12/group1/M00/00/00/wKgCDF2vNN6ABx2TAAAAFd46oKA93.html
# storage_upload_slave_by_filename
# group_name=group1, remote_filename=M00/00/00/wKgCDF2vNN6ABx2TAAAAFd46oKA93_big.html
# source ip address: 192.168.2.12
# file timestamp=2019-10-22 16:57:02
# file size=21
# file crc32=3728384160
# example file url: http://192.168.2.12/group1/M00/00/00/wKgCDF2vNN6ABx2TAAAAFd46oKA93_big.html

docker_git_build_push() {
  startTS="$(date +%s.%N)"
  git config --global credential.helper store
  # 保存账户密码
  echo "第一个参数 代码 git 地址  默认为 eniac 项目 http://gitlab.9tong.com/iquant/eniac" | lcat
  echo "第二个参数 docker 镜像仓库地址镜像➕标签 默认值为 registry.cn-shanghai.aliyuncs.com/iquant/factor-stack:v4.3" | lcat
  echo "第三个参数 是否使用 cache 默认值 为 0 使用缓存 使用缓存为 1" | lcat
  echo "loop-dev@loopbook" | lcat
  sleep 10

  if [ $(uname -m) = "armv7l" ] && [ $(uname) = "Linux" ]; then
    echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为arm Linux"
    typeset SYSTEM="linux"                        # 系统
    typeset ARCH="arm7"                           # 系统
    typeset TSOCKS_CONFIG_FILE='/etc/tsocks.conf' #  代理文件

  elif [ $(uname -m) = "aarch64" ] && [ $(uname) = "Linux" ]; then
    echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为arm64 Linux"
    typeset SYSTEM="linux"                        # 系统
    typeset ARCH="aarch64"                        # 系统
    typeset TSOCKS_CONFIG_FILE='/etc/tsocks.conf' #  代理文件

  elif [ $(uname -m) = "x86_64" ] && [ $(uname) = "Linux" ]; then
    echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为X86 Linux"
    typeset SYSTEM="linux"                        # 系统
    typeset ARCH="amd64"                          # 架构
    typeset TSOCKS_CONFIG_FILE='/etc/tsocks.conf' #  代理文件

  elif [ $(uname -m) = "x86_64" ] && [ $(uname) = "Darwin" ]; then
    echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为X86 Mac"
    typeset SYSTEM="darwin"                                 # 系统
    typeset ARCH="amd64"                                    # 架构
    typeset TSOCKS_CONFIG_FILE='/usr/local/etc/tsocks.conf' #  代理文件

  else
    echo "$(uname) 系统未知！"
  fi

  # 存在代理走代理，不存在走普通

  echo "获取关键信息" | lcat
  project=$(cutfname ${1:-http://gitlab.9tong.com/iquant/eniac})
  # eniac
  dockerHubStore=$(cutpname ${2:-registry.cn-shanghai.aliyuncs.com/iquant/factor-stack:v4.3})
  # registry.cn-shanghai.aliyuncs.com/iquant
  # 这里可能有bug 主域名提取失败
  dockerHub=$(cutpname $dockerHubStore)
  # registry.cn-shanghai.aliyuncs.com
  dockerProject=$(cutfname ${2:-registry.cn-shanghai.aliyuncs.com/iquant/factor-stack:v4.3})
  # factor-stack:v4.3
  dockerNoTag=$(echo ${2:-registry.cn-shanghai.aliyuncs.com/iquant/factor-stack:v4.3} | awk '{ gsub(/\:/,"\t"); print $0 }' | field 1)
  # registry.cn-shanghai.aliyuncs.com/iquant/factor-stack
  dockerTag=$(echo ${2:-registry.cn-shanghai.aliyuncs.com/iquant/factor-stack:v4.3} | awk '{ gsub(/\:/,"\t"); print $0 }' | field 2)
  # v4.3

  cd ~/upload
  [[ -d ~/upload/$project ]] && (sleep 3 && echo "$(date +'%Y-%m-%d %H:%M:%S') 已存在 ~/upload/$project 目录" && sudo rm -rf ~/upload/$project && echo "$(date +'%Y-%m-%d %H:%M:%S') 移除~/upload/$project 目录完成✅") || (echo "$(date +'%Y-%m-%d %H:%M:%S') 不存在 ~/upload/$project 目录 ")
  echo "🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 "
  echo "$(date +'%Y-%m-%d %H:%M:%S') 拉取 git 代码" | lcat
  echo "$(date +'%Y-%m-%d %H:%M:%S') 登陆 git" | lcat

  # if [ -e $TSOCKS_CONFIG_FILE ]; then
  # echo "`date +'%Y-%m-%d %H:%M:%S'`  存在代理 走代理加速下载";
  # tsocks git clone ${1:-http://gitlab.9tong.com/iquant/eniac};
  # else
  # echo "`date +'%Y-%m-%d %H:%M:%S'`  代理不存在";
  # git clone ${1:-http://gitlab.9tong.com/iquant/eniac};
  # fi

  git clone ${1:-http://gitlab.9tong.com/iquant/eniac}

  echo ""
  echo ""
  echo "$(date +'%Y-%m-%d %H:%M:%S')  拉取完成✅"
  l ~/upload/$project
  cd ~/upload/$project
  echo ""
  echo ""
  echo "🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 "
  echo "$(date +'%Y-%m-%d %H:%M:%S') 构造 $project 镜像" | lcat
  echo "$(date +'%Y-%m-%d %H:%M:%S') 登陆docker仓库" | lcat
  # sudo docker login --username=${2:-loop-dev@loopbook} ${3:-registry.cn-shanghai.aliyuncs.com};
  sudo docker login $dockerHub
  echo ""
  echo ""
  echo "$(date +'%Y-%m-%d %H:%M:%S') 构造仓库镜像" | lcat

  if [ ${3:-0} = "0" ]; then
    sudo docker build -t ${2:-registry.cn-shanghai.aliyuncs.com/iquant/factor-stack:v4.3} .
  else
    sudo docker build -t ${2:-registry.cn-shanghai.aliyuncs.com/iquant/factor-stack:v4.3} . --no-cache
  fi # 判断结束，以fi结尾

  echo "$(date +'%Y-%m-%d %H:%M:%S') 构造仓库镜像完成✅"
  echo ""
  echo ""
  echo "$(date +'%Y-%m-%d %H:%M:%S') 查看构造的镜像" | lcat
  sudo docker images | grep $dockerHubStore | lcat
  echo ""
  echo ""
  echo "$(date +'%Y-%m-%d %H:%M:%S') 检验是否构建成功" | lcat
  # sleep 3;
  success=$(sudo docker images | grep $dockerNoTag | grep $dockerTag | grep "second" | wc -l)
  # echo $success

  if [ $success = "1" ]; then
    echo "$(date +'%Y-%m-%d %H:%M:%S') 检验完成 \n🏅️🏅️🏅️🏅️🏅️🏅️🏅️🏅️\n🏅️🏅️🏅️🏅️🏅️🏅️🏅️🏅️\n----构建成功----\n🏅️🏅️🏅️🏅️🏅️🏅️🏅️🏅️\n🏅️🏅️🏅️🏅️🏅️🏅️🏅️🏅️\n🏅️🏅️🏅️🏅️🏅️🏅️🏅️🏅️" | lcat
    sgin="Success✅"
  else
    echo "$(date +'%Y-%m-%d %H:%M:%S') 检验完成 \n💣💣💣💣💣💣💣💣\n💣💣💣💣💣💣💣💣\n----构建失败----\n💣💣💣💣💣💣💣💣\n💣💣💣💣💣💣💣💣\n💣💣💣💣💣💣💣💣" | lcat
    sgin="Failure❌"
  fi # 判断结束，以fi结尾

  # [[ $success = "1" ]] && \
  # (echo "`date +'%Y-%m-%d %H:%M:%S'` 检验完成 \n🏅️🏅️🏅️🏅️🏅️🏅️🏅️🏅️\n🏅️🏅️🏅️🏅️🏅️🏅️🏅️🏅️\n----构建成功----\n🏅️🏅️🏅️🏅️🏅️🏅️🏅️🏅️\n🏅️🏅️🏅️🏅️🏅️🏅️🏅️🏅️\n🏅️🏅️🏅️🏅️🏅️🏅️🏅️🏅️" | lcat && typeset sgin="Success") || \
  # (echo "`date +'%Y-%m-%d %H:%M:%S'` 检验完成 \n💣💣💣💣💣💣💣💣\n💣💣💣💣💣💣💣💣\n----构建失败----\n💣💣💣💣💣💣💣💣\n💣💣💣💣💣💣💣💣\n💣💣💣💣💣💣💣💣" | lcat && typeset sgin="Failure");

  # echo $sgin;
  echo ""
  echo ""
  echo "🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 "
  echo "$(date +'%Y-%m-%d %H:%M:%S') 推送镜像到仓库" | lcat
  sudo docker push ${2:-registry.cn-shanghai.aliyuncs.com/iquant/factor-stack:v4.3}
  echo "$(date +'%Y-%m-%d %H:%M:%S') 推送镜像到仓库完成✅" | lcat
  cd ~/upload

  endTS="$(date +%s.%N)"
  result=$(gawk -v x=$startTS -v y=$endTS 'BEGIN{printf "%.6f\n",y-x}')                                # 总秒数
  _hour=$(gawk -v x=$result -v y=3600 'BEGIN{printf "%d \n",x/y}')                                     # 时数
  _min=$(gawk -v x=$result -v y=60 -v z=$_hour 'BEGIN{printf "%d \n",(x-z*3600)/y}')                   # 分数
  _sec=$(gawk -v x=$result -v y=60 -v z=$_hour -v w=$_min 'BEGIN{printf "%d \n", x - z*3600 - w*60 }') # 秒数
  echo "$(date +'%Y-%m-%d %H:%M:%S') 全部自动化构建推送耗时 $_hour 小时 $_min 分钟 $_sec 秒 " | lcat

  sddmd "git_build_push_$sgin" "$*\n耗时 $_hour 小时 $_min 分钟 $_sec 秒"
}

docker_web_k8sctl() {
  echo "https://github.com/webkubectl/webkubectl/blob/master/README.zh_CN.md"
  sudo docker run --restart=always --name="webkubectl" -p 8090:8080 -d --privileged webkubectl/webkubectl
  echo "0.0.0.0:8090"
}

docker_build_push() {

  startTS="$(date +%s.%N)"

  echo "第一个参数 Dockerfile 文件地址  默认为 ~/upload/ubuntu_init/docker_file_dir/hexo/Dockerfile " | lcat
  echo "第二个参数 docker 镜像仓库地址镜像➕标签 默认值为 registry.cn-shanghai.aliyuncs.com/iquant/factor-stack:v4.3" | lcat
  echo "第三个参数 是否使用 cache 默认值 为 0 使用缓存 不使用为 1" | lcat
  echo "loop-dev@loopbook" | lcat
  sleep 10

  echo "获取关键信息" | lcat
  project=$(cutpname ${1:-~/upload/ubuntu_init/docker_file_dir/hexo/Dockerfile})
  # eniac
  dockerHubStore=$(cutpname ${2:-registry.cn-shanghai.aliyuncs.com/iquant/factor-stack:v4.3})
  # registry.cn-shanghai.aliyuncs.com/iquant
  dockerHub=$(cutpname $dockerHubStore)
  # registry.cn-shanghai.aliyuncs.com
  dockerProject=$(cutfname ${2:-registry.cn-shanghai.aliyuncs.com/iquant/factor-stack:v4.3})
  # factor-stack:v4.3
  dockerNoTag=$(echo ${2:-registry.cn-shanghai.aliyuncs.com/iquant/factor-stack:v4.3} | awk '{ gsub(/\:/,"\t"); print $0 }' | field 1)
  # registry.cn-shanghai.aliyuncs.com/iquant/factor-stack
  dockerTag=$(echo ${2:-registry.cn-shanghai.aliyuncs.com/iquant/factor-stack:v4.3} | awk '{ gsub(/\:/,"\t"); print $0 }' | field 2)
  # v4.3

  echo "$(date +'%Y-%m-%d %H:%M:%S') neo的阿里云镜像地址"
  echo "https://cr.console.aliyun.com/cn-shanghai/instances/repositories"

  if [ $(uname -m) = "armv7l" ] && [ $(uname) = "Linux" ]; then
    echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为arm Linux" | lcat

  elif [ $(uname -m) = "aarch64" ] && [ $(uname) = "Linux" ]; then
    echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为arm64 Linux" | lcat

  elif [ $(uname -m) = "x86_64" ] && [ $(uname) = "Linux" ]; then
    echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为X86 Linux" | lcat
    #   typeset sysname="_linux_amd64"
    # 执行代码块
  elif [ $(uname -m) = "x86_64" ] && [ $(uname) = "Darwin" ]; then
    echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为X86 Mac" | lcat
    chrome "https://cr.console.aliyun.com/cn-shanghai/instances/repositories"
    #   typeset sysname="_darwin_amd64"
    # 执行代码块
  else
    echo "$(uname) 系统未知！"
  fi

  typecode=$(sudo docker pull $dockerNoTag 2>&1 | grep "repository does not exist" | wc -l)
  # 1 不存在仓库
  [[ $typecode = 1 ]] && (echo "$(date +'%Y-%m-%d %H:%M:%S') 仓库不存在请提前创建，请先❌掉程序" | lcat) || echo "$(date +'%Y-%m-%d %H:%M:%S') 镜像仓库存在"
  time_wait

  cd $project

  echo ""
  echo ""
  echo "🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 "
  echo "$(date +'%Y-%m-%d %H:%M:%S') 构造 $project 镜像" | lcat
  echo "$(date +'%Y-%m-%d %H:%M:%S') 登陆docker仓库" | lcat
  # sudo docker login --username=${2:-loop-dev@loopbook} ${3:-registry.cn-shanghai.aliyuncs.com};
  sudo docker login $dockerHub
  echo ""
  echo ""
  echo "$(date +'%Y-%m-%d %H:%M:%S') 构造仓库镜像" | lcat

  if [ ${3:-0} = "0" ]; then
    sudo docker build -t ${2:-registry.cn-shanghai.aliyuncs.com/iquant/factor-stack:v4.3} .
  else
    sudo docker build -t ${2:-registry.cn-shanghai.aliyuncs.com/iquant/factor-stack:v4.3} . --no-cache
  fi # 判断结束，以fi结尾

  echo "$(date +'%Y-%m-%d %H:%M:%S') 构造仓库镜像完成✅"
  echo ""
  echo ""
  echo "$(date +'%Y-%m-%d %H:%M:%S') 查看构造的镜像" | lcat
  sudo docker images | grep $dockerHubStore | lcat
  echo ""
  echo ""
  echo "$(date +'%Y-%m-%d %H:%M:%S') 检验是否构建成功" | lcat
  # sleep 3;
  success=$(sudo docker images | grep $dockerNoTag | grep $dockerTag | grep "second" | wc -l)
  # echo $success

  if [ $success = "1" ]; then
    echo "$(date +'%Y-%m-%d %H:%M:%S') 检验完成 \n🏅️🏅️🏅️🏅️🏅️🏅️🏅️🏅️\n🏅️🏅️🏅️🏅️🏅️🏅️🏅️🏅️\n----构建成功----\n🏅️🏅️🏅️🏅️🏅️🏅️🏅️🏅️\n🏅️🏅️🏅️🏅️🏅️🏅️🏅️🏅️\n🏅️🏅️🏅️🏅️🏅️🏅️🏅️🏅️" | lcat
    sgin="Success✅"
  else
    echo "$(date +'%Y-%m-%d %H:%M:%S') 检验完成 \n💣💣💣💣💣💣💣💣\n💣💣💣💣💣💣💣💣\n----构建失败----\n💣💣💣💣💣💣💣💣\n💣💣💣💣💣💣💣💣\n💣💣💣💣💣💣💣💣" | lcat
    sgin="Failure❌"
  fi # 判断结束，以fi结尾

  # [[ $success = "1" ]] && \
  # (echo "`date +'%Y-%m-%d %H:%M:%S'` 检验完成 \n🏅️🏅️🏅️🏅️🏅️🏅️🏅️🏅️\n🏅️🏅️🏅️🏅️🏅️🏅️🏅️🏅️\n----构建成功----\n🏅️🏅️🏅️🏅️🏅️🏅️🏅️🏅️\n🏅️🏅️🏅️🏅️🏅️🏅️🏅️🏅️\n🏅️🏅️🏅️🏅️🏅️🏅️🏅️🏅️" | lcat && typeset sgin="Success") || \
  # (echo "`date +'%Y-%m-%d %H:%M:%S'` 检验完成 \n💣💣💣💣💣💣💣💣\n💣💣💣💣💣💣💣💣\n----构建失败----\n💣💣💣💣💣💣💣💣\n💣💣💣💣💣💣💣💣\n💣💣💣💣💣💣💣💣" | lcat && typeset sgin="Failure");

  # echo $sgin;
  echo ""
  echo ""
  echo "🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 "
  echo "$(date +'%Y-%m-%d %H:%M:%S') 推送镜像到仓库" | lcat
  sudo docker push ${2:-registry.cn-shanghai.aliyuncs.com/iquant/factor-stack:v4.3}
  echo "$(date +'%Y-%m-%d %H:%M:%S') 推送镜像到仓库完成✅" | lcat
  cd ~/upload

  endTS="$(date +%s.%N)"
  result=$(gawk -v x=$startTS -v y=$endTS 'BEGIN{printf "%.6f\n",y-x}')                                # 总秒数
  _hour=$(gawk -v x=$result -v y=3600 'BEGIN{printf "%d \n",x/y}')                                     # 时数
  _min=$(gawk -v x=$result -v y=60 -v z=$_hour 'BEGIN{printf "%d \n",(x-z*3600)/y}')                   # 分数
  _sec=$(gawk -v x=$result -v y=60 -v z=$_hour -v w=$_min 'BEGIN{printf "%d \n", x - z*3600 - w*60 }') # 秒数
  echo "$(date +'%Y-%m-%d %H:%M:%S') 全部自动化构建推送耗时 $_hour 小时 $_min 分钟 $_sec 秒 " | lcat

  sddmd "git_build_push_$sgin" "$*\n耗时 $_hour 小时 $_min 分钟 $_sec 秒"
}

restart_compose() {
  YAML=${1:-$(echo ~)/upload/ubuntu_init/docker_file_dir/docker-compose/x86-neo_pub.yml}

  sudo docker-compose -f $YAML stop &&
    sudo docker-compose -f $YAML rm -f &&
    sudo docker-compose -f $YAML up
}

restart_compose_d() {
  YAML=${1:-$(echo ~)/upload/ubuntu_init/docker_file_dir/docker-compose/x86-neo_pub.yml}

  sudo docker-compose -f $YAML stop &&
    sudo docker-compose -f $YAML rm -f &&
    sudo docker-compose -f $YAML up -d
}

fast_dockerhub() {
  sudo mkdir -p /etc/docker
  sudo tee /etc/docker/daemon.json <<-'EOF'
{
  "registry-mirrors": ["https://r9q0wroe.mirror.aliyuncs.com"],
  "log-driver": "json-file",
  "log-opts": { "max-size": "10m", "max-file": "1" }
}
EOF
  sudo systemctl daemon-reload
  sudo systemctl restart docker
}

docker_restart_rpi_mongo() {
  echo "一个参数 默认rest 可选auth"
  time_wait
  sudo rm -rf /data/mongo/db/mongod.lock &&
    nrm mongo && sudo docker run -d \
    --name mongo \
    --restart unless-stopped \
    -v /data/mongo/db:/data/db \
    -v /etc/localtime:/etc/localtime:ro \
    -v /data/mongo/configdb:/data/configdb -v /data/logs/mongo:/var/log/mongodb \
    -p 27017:27017 \
    -p 28017:28017 \
    andresvidal/rpi3-mongodb3:latest \
    mongod --${1:-rest} --bind_ip 0.0.0.0 --httpinterface --dbpath /data/db --logpath /var/log/mongodb/mongod.log --logappend
}

nlog_ls() {
  nfl /var/lib/docker/containers "*-json.log" | lcat
}

nlog_name() {
  for id in $(nfl /var/lib/docker/containers "*-json.log" | field 9 | keycut "ontainers/" "-json" | gformat | field 2); do
    echo $(nfl /var/lib/docker/containers "*-json.log" | grep $id | fields '$1,$3,$5,$6,$7,$8') $id $(sudo docker inspect $id | jq ".[] | .Name") | lcat
  done
}

nlog_clean() {
  echo "$(date +'%Y-%m-%d %H:%M:%S') 为 /var/lib/docker/containers 增加修改权限"
  sudo chmod -R 777 /var/lib/docker/containers
  echo "$(date +'%Y-%m-%d %H:%M:%S') 显示当前docker log 情况"
  nlog_ls
  echo "$(date +'%Y-%m-%d %H:%M:%S') 清理开始"
  logfiles=($(nfl /var/lib/docker/containers "*-json.log" | field 9 | rformat)) # 文本转数组 文本 分隔符转空格
  for logfile in $logfiles; do
    sudo echo '' >$logfile
  done
  echo "$(date +'%Y-%m-%d %H:%M:%S') 清理完成✅"
  echo "$(date +'%Y-%m-%d %H:%M:%S') 显示当前docker log 情况"
  nlog_ls
}

# 安装最新的 docker-compose
# https://docs.docker.com/compose/install/
# http://cdn.neo.pub/docker-compose-Linux-x86_64

install_docker-compose_lastest() {
  cd ~/upload
  # sudo curl -L "https://github.com/docker/compose/releases/download/1.29.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
  # sudo curl -L  http://cdn.neo.pub/docker-compose-Linux-x86_64   -o /usr/local/bin/docker-compose

  # download_fast_github_best_file_url 'https://github.com/docker/compose/releases' '/usr/local/bin/docker-compose'

  VERSION="$(curl --connect-timeout 5 -m 10 -s https://github.com/docker/compose/releases | htmlq a --attribute href | grep compose | grep releases | grep tag | head -n 1 | xargs -0 basename)"

  # 执行查询版本号
  # xargs
  # typeset
  # $VERSION ok
  # ${VERSION} 失败
  [[ $VERSION ]] && echo $VERSION || typeset VERSION=v2.2.3

  curl -SL https://github.91chi.fun//https://github.com//docker/compose/releases/download/$VERSION/docker-compose-linux-x86_64 -o /usr/local/bin/docker-compose

  sudo chmod +x /usr/local/bin/docker-compose

  if [ -f /usr/local/bin/docker-compose ]; then
    echo "软连接已存在"
  else
    sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
  fi

  # 如果存在pip版本的，就卸载掉
  # 字段非空写法的判断
  # if [ -z `sudo pip list | grep docker-compose` ] ; then
  if [ $(sudo pip list | grep docker-compose) ]; then
    sudo pip uninstall docker-compose -y
  fi

  # docker-compose version
  sudo docker-compose version
}
