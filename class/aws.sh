# setup_() {
#     pip3 install -U
#     aws configure
# }

aws_set_ohio() {
    scat ~/.aws/credentials.ohio >~/.aws/credentials
    scat ~/.aws/config.ohio >~/.aws/config
    export ACCOUNT_ID=${1:-835751346093}
}

aws_set_singapore() {
    scat ~/.aws/credentials.ohio >~/.aws/credentials
    scat ~/.aws/config.singapore >~/.aws/config
    export ACCOUNT_ID=${1:-835751346093}
}

aws_set_bj() {
    scat ~/.aws/credentials.bj >~/.aws/credentials
    scat ~/.aws/config.bj >~/.aws/config
    export ACCOUNT_ID=${1:-032665272082}
}

aws_set_nx() {
    scat ~/.aws/credentials.bj >~/.aws/credentials
    scat ~/.aws/config.nx >~/.aws/config
    export ACCOUNT_ID=${1:-032665272082}
}

aws_set_virginia() {
    scat ~/.aws/credentials.ohio >~/.aws/credentials
    scat ~/.aws/config.virginia >~/.aws/config
    export ACCOUNT_ID=${1:-835751346093}
}

aws_set_virginia_sunny() {
    scat ~/.aws/credentials.virginia.sunny >~/.aws/credentials
    scat ~/.aws/config.virginia >~/.aws/config
    export ACCOUNT_ID=${1:-227401510565}
}

aws_set_tokyo() {
    scat ~/.aws/credentials.ohio >~/.aws/credentials
    scat ~/.aws/config.tokyo >~/.aws/config
    export ACCOUNT_ID=${1:-835751346093}
}

aws_set_ireland() {
    scat ~/.aws/credentials.ohio >~/.aws/credentials
    scat ~/.aws/config.ireland >~/.aws/config
    export ACCOUNT_ID=${1:-835751346093}
}

aws_set_tokyo_yangtan() {
    scat ~/.aws/credentials.tokyo_yangtan >~/.aws/credentials
    scat ~/.aws/config.tokyo_yangtan >~/.aws/config
    export ACCOUNT_ID=${1:-578945633952}
}

aws_set_global_ssa() {
    scat ~/.aws/credentials.global.ssa >~/.aws/credentials
    scat ~/.aws/config.virginia >~/.aws/config
    export ACCOUNT_ID=${1:-946277762357}
}

naws() {
    aws $* | jcat
}

aws_web_open() {
    pnamekill "compute.amazonaws.com"
    echo "开启本地web访问，没有输出，不要介意" | lat
    echo "切记在访问前，部署好 foxyProxy 及其配置文件" | lat
    server=${1:-ec2-18-166-59-193.ap-east-1.compute.amazonaws.com}
    pem=${2:-/Users/neo/Downloads/neohk.pem}
    aws_port=${3:-8157}

    (ssh -i $pem -ND $aws_port hadoop@$server &)

    echo "已开起本地web访问，端口为 8157" | lcat
    echo ""
    echo "🌟 🌟 🌟 🌟 🌟 🌟 🌟 🌟 🌟 🌟 🌟 🌟 🌟 🌟 🌟 🌟 🌟 🌟 "
    echo HDFS名称节点 http://$server:9870/ | lat
    echo Hue http://$server:8888 | lcat
    echo Spark_History_Server http://$server:18080/ | lat
    echo Tez_UI http://$server:8080/tez-UI | lat
    echo Zeppelin http://$server:8890/ | lat
    echo 资源管理器 http://$server:8088/ | lat

    echo "enjoy it！🌹 🌹 🌹"

    chrome http://$server:9870/
    chrome http://$server:8888
    chrome http://$server:18080/
    chrome http://$server:8080/tez-UI
    chrome http://$server:8890/
    chrome http://$server:8088/
}

aws_web_close() {

    pnamekill "compute.amazonaws.com"
}

aws_ip_bj() {

    echo "aws work neo 北京 ip"
    echo "52.81.30.213" | lcat
    echo "52.81.30.213" | njq
}

aws_ip_ohio() {

    echo "aws work neo 俄亥俄州 ip"
    echo "3.135.198.113" | lcat
    echo "3.135.198.113" | njq
}

aws_say() {
    # help
    # https://docs.aws.amazon.com/zh_cn/polly/latest/dg/what-is.html

    # tts
    aws polly synthesize-speech \
        --output-format mp3 \
        --voice-id Zhiyu \
        --text ${1:-'看破世间迷眼相，浩然正气贯长虹'} \
        ~/upload/aws_polly_voice.mp3 | jcat

    # speak
    play ~/upload/aws_polly_voice.mp3
}

aws_say_en() {
    # https://www.amazonaws.cn/polly/features/
    # https://docs.amazonaws.cn/en_us/polly/latest/dg/ssml-synthesize-speech-cli.html
    # https://docs.aws.amazon.com/zh_cn/polly/latest/dg/bilingual-voices.html
    aws polly synthesize-speech \
        --output-format mp3 \
        --voice-id Amy \
        --text-type ssml \
        --text ${1:-'看破世间迷眼相，浩然正气贯长虹'} \
        ~/upload/aws_polly_voice.mp3 | jcat

    # speak
    play ~/upload/aws_polly_voice.mp3
}

# 亚马逊安全必须安装
install_aws_ssm() {
    sudo snap switch --channel=candidate amazon-ssm-agent
    sudo snap services amazon-ssm-agent
    sudo systemctl status snap.amazon-ssm-agent.amazon-ssm-agent.service
    sudo systemctl restart snap.amazon-ssm-agent.amazon-ssm-agent.service
    sudo snap list amazon-ssm-agent
    sudo snap services amazon-ssm-agent
}

# SSA全区域
aws_ssa_region() {
    cd ~/upload/ubuntu_init/class/func/aws_SSA
    aws_set_ohio
    sh PVRE-SSM-onboarding.sh

    aws_set_bj
    sh PVRE-SSM-onboarding.sh

    cd ~/upload
}

install_aws_cli_mac() {
    cd ~/Downloads
    wget https://awscli.amazonaws.com/AWSCLIV2.pkg
    cd ~/upload/
}

install_aws_cli_latest() {

    curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "v2.zip"
    unzip v2.zip
    sudo ./aws/install

    # https://docs.aws.amazon.com/zh_cn/cli/latest/userguide/install-cliv2-mac.html

}

install_rsql() {
    echo "redshift sql client"
    brew install unixodbc openssl --build-from-source
    curl -s https://docs.aws.amazon.com/zh_cn/redshift/latest/mgmt/rsql-query-tool-getting-started.html | htmlq a --attribute href | grep ".rpm\|.dmg\|.msi"
}

install_aws-es-proxy() {
    echo "AWS-ES-Proxy是一个位于HTTP客户端(浏览器、cURL等)之间的小型Web服务器应用程序和亚马逊弹性搜索服务。在将请求发送到Amazon Elasticsearch之前，它将使用最新的AWS签名版本4对您的请求进行签名。当Amazon Elasticsearch返回响应时，此响应将发送回您的HTTP客户端。 Kibana请求也是自动签名的。"
    brew install aws-es-proxy

}

check_aws_white_list() {
    nat ~/.aws/config
    # echo "目前只支持virginia neo "

    SECURITY_GROUP_ID=${1:-sg-0708b2efbc03b7d46}
    # sg-0708b2efbc03b7d46 virginia
    # sg-06c09f11fb4b03111 k8s-virginia
    # sg-09dbcee77d647d169 ohio frp
    # sg-05788212725e51da8 beijing
    # sg-000e495cdac553f6f ningxia
    IP=${2:-$(wip)}

    WHITE_IP_LIST=$(aws ec2 describe-security-groups --group-ids $SECURITY_GROUP_ID | jq '.SecurityGroups[].IpPermissions[] | select(.IpProtocol  == "-1") | .IpRanges[].CidrIp' | cut_ip)
    if [[ $WHITE_IP_LIST[@] =~ $IP ]]; then
        echo "$(date +'%Y-%m-%d %H:%M:%S') 🍀  白名单包含本地ip: $IP" | lcat
    else
        echo "$(date +'%Y-%m-%d %H:%M:%S') ❌  白名单不包含本地ip: $IP" | lcat
        LOCATION=$(curl -s http://ip-api.com/json/$IP\?lang\=zh-CN | jq '[del(.status,.zip,.lat,.lon,.timezone,.query,.country,.regionName,.city)]' | md-table | line 3 3 | pass_black | s2kformat | pass_dou | ncut 1 20)
        # TAG_STRING="$(nows)$LOCATION"
        TAG_STRING="$(echo $(whoami)_$(nows)_$LOCATION | ncut 1 64)"
        aws ec2 authorize-security-group-ingress --group-id $SECURITY_GROUP_ID --ip-permissions IpProtocol=-1,FromPort=-1,IpRanges="[{CidrIp=$(wip)/32,Description=$TAG_STRING}]" | jq
        echo "$(date +'%Y-%m-%d %H:%M:%S') ✅  白名单添加本地ip: $IP完成" | lcat
        sddmd "check_aws_white_list" "白名单添加本地ip:$IP完成✅"
    fi

}

check_aws_white_list_all_region() {
    echo "所有region 的 专有 VPC 加入 本地白名单"
    echo "01. virginia"
    aws_set_virginia
    check_aws_white_list sg-0708b2efbc03b7d46
    check_aws_white_list sg-06c09f11fb4b03111
    echo "02. ohio"
    aws_set_ohio
    check_aws_white_list sg-09dbcee77d647d169

    echo "03. beijing"
    aws_set_bj
    check_aws_white_list sg-05788212725e51da8
    check_aws_white_list sg-00205fbf18d1fea56

    echo "04. ningxia"
    aws_set_nx
    check_aws_white_list sg-000e495cdac553f6f

    echo "05. ireland"
    aws_set_ireland
    check_aws_white_list sg-03f58abb72b64bbbf
    check_aws_white_list sg-07a489a8e7c47fee5
    check_aws_white_list sg-00b293dfb6e1918c5

    echo "06. tokyo"
    aws_set_tokyo
    check_aws_white_list sg-09a5ff2bc4c767b8a
    check_aws_white_list sg-04551bef3c569fb6d # emr-master

    # echo "07.SSA global"
    # aws_set_global_ssa
    # check_aws_white_list sg-0da5573d776ba567d

    echo "返回默认region"
    aws_eks_set_region virginia

}

aws_ip() {
    echo "亚马逊ip段"
    chrome "https://ip-ranges.amazonaws.com/ip-ranges.json"
}

install_awscli_v2() {
    echo "for Mac"
    echo "https://awscli.amazonaws.com/AWSCLIV2.pkg"
    echo "for Linux"
    echo "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip"
    echo "for Windows"
    echo "https://awscli.amazonaws.com/AWSCLIV2.msi"
}

aws_rm_bucket_prefix() {
    mylist=$(aws s3 ls | cut -d ' ' -f 3 | grep $1)
    # echo $mylist
    # for s in $(aws s3 ls | grep portal)
    # do
    #     echo $s
    # done
    for bucket in $mylist; do
        echo $bucket
        aws s3 rm s3://$bucket --recursive
        aws s3 rb s3://$bucket
    done
}

aws_workshop() {

    if [ -e "/usr/local/bin/aws-preview" ]; then
        echo "$(date +'%Y-%m-%d %H:%M:%S')aws-preview exists."
    else
        echo "$(date +'%Y-%m-%d %H:%M:%S') aws-preview not found. init aws-preview"
        mkdir ~/upload
        sudo curl -s -L -o ~/upload/aws-preview https://artifacts.us-east-1.prod.workshops.aws/v2/cli/osx/preview_build
        sudo chmod 777 ~/upload/aws-preview
        sudo mv ~/upload/aws-preview /usr/local/bin/aws-preview
    fi

    aws-preview \
        -debug \
        -input ${1:-~/Code/AWS/building-real-time-dashboard-applications-with-serverless} \
        -port ${2:-1234}

}

install_s3fs() {
    os_name=$(uname -s)
    if [[ "$os_name" == "Linux" ]]; then
        # Determine Linux distro
        if [ -f /etc/os-release ]; then
            . /etc/os-release
        fi

        case $ID in
        'amzn')
            sudo amazon-linux-extras install epel
            sudo yum install s3fs-fuse
            ;;
        'arch')
            sudo pacman -S s3fs-fuse
            ;;
        'debian' | 'ubuntu')
            sudo apt install s3fs
            ;;
        'fedora')
            sudo dnf install s3fs-fuse
            ;;
        'gentoo')
            sudo emerge net-fs/s3fs
            ;;
        'rhel' | 'centos')
            sudo yum install epel-release
            sudo yum install s3fs-fuse
            ;;
        'suse' | 'opensuse')
            sudo zypper install s3fs
            ;;
        *)
            echo "Unsupported Linux distro."
            ;;
        esac
    elif [[ "$os_name" == "Darwin" ]]; then
        brew install --cask macfuse
        brew install gromgit/fuse/s3fs-mac
    elif [[ "$os_name" == "FreeBSD" ]]; then
        pkg install fusefs-s3fs
    else
        echo "Unsupported OS."
    fi
}

# install_s3fs() {
#   os_name=$(uname -s)
#   sudo shopt -s -E

#   case $os_name in
#       Linux)
#           id_=$(echo $ID | tr '[:upper:]' '[:lower:]')
#           case $id_ in
#               amzn)
#                   sudo amazon-linux-extras install epel
#                   sudo yum install s3fs-fuse
#                   ;;
#               arch)
#                   sudo pacman -S s3fs-fuse
#                   ;;
#               debian|ubuntu)
#                   sudo apt install s3fs
#                   ;;
#               fedora)
#                   sudo dnf install s3fs-fuse
#                   ;;
#               gentoo)
#                   sudo emerge net-fs/s3fs
#                   ;;
#               rhel|centos)
#                   sudo yum install epel-release
#                   sudo yum install s3fs-fuse
#                   ;;
#               suse|opensuse)
#                   sudo zypper install s3fs
#                   ;;
#               *)
#                   echo "Unsupported Linux distro."
#                   exit 1
#                   ;;
#           esac
#           ;;
#       Darwin)
#           brew install --cask macfuse
#           brew install gromgit/fuse/s3fs-mac
#           ;;
#       FreeBSD)
#           pkg install fusefs-s3fs
#           ;;
#       *)
#           echo "Unsupported OS."
#           exit 1
#           ;;
#   esac
# }



aws_cases() {
    base_url="https://command-center.support.aws.a2z.com/case-console/cases/#/search/cases?"
    search_query=""

    if [ $# -eq 0 ]; then
        echo "请提供至少一个案例ID"
        return 1
    fi

    # 构建搜索查询
    for case_id in "$@"; do
        if [ -z "$search_query" ]; then
            search_query="search=(caseID%3D${case_id})"
        else
            search_query="${search_query}+%7C+(caseID%3D${case_id})"
        fi
    done

    full_url="${base_url}${search_query}"

    # 使用Chrome打开URL
    if [[ "$OSTYPE" == "darwin"* ]]; then
        # macOS
        /Applications/Google\ Chrome.app/Contents/MacOS/Google\ Chrome "$full_url"
    elif [[ "$OSTYPE" == "linux-gnu"* ]]; then
        # Linux
        google-chrome "$full_url" &>/dev/null &
    else
        echo "不支持的操作系统"
        return 1
    fi
}