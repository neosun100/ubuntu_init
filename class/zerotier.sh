# 45 2021-02-13 22:58 curl -s https://install.zerotier.com/ | sudo bash
# 46 2021-02-13 22:58 sudo chmod 777 -R /var/lib/zerotier-one/
# 47 2021-02-13 22:58 cd /var/lib/zerotier-one/
# 48 2021-02-13 22:59 zerotier-cli join 1c33c1ced00b399a
# 49 2021-02-13 22:59 zerotier-idtool initmoon identity.public >moon.json
# 50 2021-02-13 22:59 nat moon.json
# 51 2021-02-13 23:01 zerotier-idtool genmoon moon.json
# 52 2021-02-13 23:01 mkdir moons.d
# 53 2021-02-13 23:01 mv 000000*.moon moons.d
# 54 2021-02-13 23:02 sudo chmod 777 -R /var/lib/zerotier-one
# 55 2021-02-13 23:02 sctl_stop zerotier-one.service
# 56 2021-02-13 23:02 sctl_status zerotier-one.service
# 57 2021-02-13 23:02 ps -ef | grep zero
# 58 2021-02-13 23:02 sctl_restart zerotier-one.service
# 59 2021-02-13 23:02 ps -ef | grep zero
# 60 2021-02-13 23:03 sudo kill -9 4703
# 61 2021-02-13 23:03 sctl_restart zerotier-one.service
# 62 2021-02-13 23:03 ps -ef | grep zero
# 63 2021-02-13 23:03 l
# 64 2021-02-13 23:03 l moons.d

zero_init() {
   cd ~/upload
   curl -s https://install.zerotier.com/ | sudo bash
   sudo chmod 777 -R /var/lib/zerotier-one/
   cd /var/lib/zerotier-one/
   zerotier-cli join 1c33c1ced00b399a
   zerotier-idtool initmoon identity.public >moon.json
   zerotier-cli listpeers
}
# https://mp.weixin.qq.com/s/g3JFEaNGaLJCeDfr-Di0Hg
# http://einverne.github.io/post/2018/06/zerotier.html
# https://www.lingbaoboy.com/2019/03/vpszerotiermoon.html
# https://blog.csdn.net/dingjianjin/article/details/104235222

zerolist() {
   zerotier-cli listpeers
}

zero_add_moon_node() {
   zerotier-cli orbit bfd5c59fc5 bfd5c59fc5 # neo-pub
   zerotier-cli orbit eccb651069 eccb651069 # loop
   zerotier-cli orbit 3d15a81e76 3d15a81e76 # AWS-work-neo
}

# zerotier_table() {
#    curl -s -X GET -H "Authorization: token $ZEROTIER_TOKEN" 'https://api.zerotier.com/api/v1/network/1c33c1ced00b399a/member' |
#       jq '.[] | . + {"VirtualIP": .config.ipAssignments[]} | . + {"time": .lastOnline | todate}' |
#       json2csv |
#       csvq "select nodeId,name,lastOnline,physicalAddress,clientVersion,VirtualIP,NANO_TO_DATETIME(lastOnline*1000*1000) as time"
# }

zerotier_table() {
   current_time=$(date +%s)
   five_minutes_ago=$((current_time - 300))

   curl -s -X GET -H "Authorization: token $ZEROTIER_TOKEN" 'https://api.zerotier.com/api/v1/network/1c33c1ced00b399a/member' |
      jq '.[] | . + {"VirtualIP": .config.ipAssignments[]} | . + {"time": .lastOnline | todate}' |
      json2csv |
      csvq "SELECT nodeId,name,lastOnline,physicalAddress,clientVersion,VirtualIP,NANO_TO_DATETIME(lastOnline*1000*1000) as time ORDER BY lastOnline DESC" #|
   #awk -v five_minutes_ago="$five_minutes_ago" -F',' 'BEGIN {OFS=","} {if ($3 > five_minutes_ago) $0="\033[1;33m"$0"\033[0m"} 1'
}

zt() {
   zerotier_table
}
