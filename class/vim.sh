install_vim_theme_dracula() {
    echo "安装德古拉 vim 主题"
    echo "https://draculatheme.com/vim"
    mkdir -p ~/.vim/pack/themes/start
    cd ~/.vim/pack/themes/start
    git clone https://gitee.com/mirrors_dracula/vim.git dracula

    echo "input below CMD to ~/.vimrc"
    echo "🧛‍♂️ 🧛‍♂️ 🧛‍♂️ 🧛‍♂️ 🧛‍♂️ 🧛‍♂️ 🧛‍♂️ 🧛‍♂️ 🧛‍♂️ 🧛‍♂️ 🧛‍♂️ 🧛‍♂️ 🧛‍♂️ 🧛‍♂️ 🧛‍♂️ 🧛‍♂️ 🧛‍♂️ 🧛‍♂️ 🧛‍♂️ 🧛‍♂️ "
    echo 'packadd! dracula'
    echo 'syntax enable'
    echo 'colorscheme dracula'
    sleep 5
    vim ~/.vimrc

}
