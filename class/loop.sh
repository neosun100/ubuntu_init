# 等待函数 循环输出等待多少秒
# seq教程 https://www.cnblogs.com/ginvip/p/6351720.html
time_wait() {
    NUM=${1:-5}
    for i in $(seq $NUM -1 1); do
        echo "$i 秒后执行"
        sleep 1
    done
}

# 评估命令每步执行时间 消耗时间
# alias cost=gnomon
cost() {
    if ! command -v gnomon &> /dev/null; then
        echo "Error: gnomon is not installed or not in PATH" >&2
        return 1
    fi
    # cat | gnomon "$@"
    cat | gnomon --type=elapsed-line --high=10 --medium=5 --low=2 --format="Y-m-d H:i:s.u" --real-time=100 --ignore-blan
}

# echo -e "text1\ntext2\
# -e表示开启转移字符

loop_help() {
    echo '🥜🥜🥜🥜🥜🥜🥜🥜🥜🥜🥜🥜🥜🥜🥜🥜🥜🥜🥜🥜🥜🥜🥜🥜'
    echo '间隔n秒，执行一次命令'
    echo '循环执行ll 命令，间隔一秒'
    echo 'while : ;do ll --full-time; sleep 1;done;' | lcat
    echo ''
    echo ''
    echo '🥜🥜🥜🥜🥜🥜🥜🥜🥜🥜🥜🥜🥜🥜🥜🥜🥜🥜🥜🥜🥜🥜🥜🥜'
    echo 'loop 命令，每隔3秒执行1次'
    echo '命令 需要事先写好，不能是多命令链接格式'
    echo -e "Func\tDescription\nloop command\tdo command every 3 seconds\nngtop\tcolorful gpu top" | table
}

gen_demo() {
    head -n 5000 ~/upload/report_realtime_one_000 >~/upload/demo.txt &&
        /home/linuxbrew/.linuxbrew/Homebrew/Cellar/gawk/5.1.0/bin/gawk -i inplace -F '|' -v OFS="|" "\$1 = $(date +'%Y%m%d'), \$2 = $(nowts) " ~/upload/demo.txt &&
        tail -n 1 ~/upload/demo.txt && echo $(date +'%Y-%m-%d %H:%M:%S') $(nts)
}

insert_pg() {
    for i in $(seq 1 2000); do psql postgresql://root:AWS2themoon@database-pg.cluster-cokw0zyghprv.rds.cn-north-1.amazonaws.com.cn:5432/postgres -c "\copy public.adx_report_realtime FROM 'demo.txt' (DELIMITER '|') ;"; done | cost
    # sleep 525
}

insert_kinesis() {
    kinesis produce --stream mobvista -f ~/upload/demo.txt --repeat 2000
    sleep 400
}

loop_1() {
    while :; do
        $*
        sleep 1
    done
}

# 循环执行某命令
loop() {
    # while : ;do $@; sleep 3;done;
    while :; do
        ${1:-kubectl get pods|grep Running | wc -l}
        sleep 3
    done
}

loop_60() {
    while :; do
        $*
        sleep 60
    done
}

loop_30() {
    while :; do
        $*
        sleep 30
    done
}

loop_1h() {
    while :; do
        $*
        sleep 3600
    done
}

loop_2h() {
    while :; do
        $*
        sleep 7200
    done
}

loop_4h() {
    while :; do
        $*
        sleep 14400
    done
}

loop_6h() {
    while :; do
        $*
        sleep 21600
    done
}

loop_12h() {
    while :; do
        $*
        sleep 42300
    done
}

loop_1d() {
    while :; do
        $*
        sleep 86400
    done
}

loop_go() {
    # while : ;do $@; sleep 3;done;
    while :; do $*; done
}

# -------------- GPU --------------
color_gpu_top() {
    # echo 🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀 && \
    # echo 🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥🔥&& \
    c &&
        nvidia-smi | lcat
}

zero_sdf() {
    c && sdf | fields '$1,$2,$3,$4,$5,$6'
}

nsdf() {
    loop zero_sdf
}

# 循环高亮gpu统计输出
ngtop() {
    loop color_gpu_top
}

# --------------- CPU ------------

color_cputem() {
    c &&
        [[ $(hostname) = "z8750" ]] && (sensors | grep "Core" | lcat) ||
        sensors | grep "Package id" | lcat
}

# 循环高亮cpu温度输出 neo cpu tem
nctem() {
    loop color_cputem
}

color_w() {
    c &&
        w | lcat
}

# 循环高亮当前谁在线，执行什么
nw() {
    loop color_w
}

loop_ssd_top() {
    clear &&
        ssd_top
}

nssd_top() {
    loop_60 loop_ssd_top
}

color_cpu() {
    clear &&
        # sudo cat /proc/cpuinfo | grep MHz | grep -v 999 | ns 4 | line 1 16 |tr -d '[:blank:]'| lcat
        # sudo cat /proc/cpuinfo | grep MHz | grep -v 999 | ns 4 | line 1 16 | tr -d '[:blank:]' | cut -d. -f1
        sudo cat /proc/cpuinfo | grep MHz | grep -v 999 | ns 4 | line 1 16 | tr -d '[:blank:]' | cut -d. -f1 | sed 's/:/:\'$'\t''/g' 

    # sudo cat /proc/cpuinfo |grep MHz | lcat

}

ncpu() {
    loop color_cpu
}

ncpu_one() {
    while :; do
        sudo cat /proc/cpuinfo | grep MHz | head -n 1 | lcat
        sleep 3
    done
}

simple_cpu(){
    sudo cat /proc/cpuinfo | grep MHz | grep -v 999
}
