info_log() {
    echo -e "\033[1;32mINFO:\033[0m \033[0;37m$(date +'%Y-%m-%d %H:%M:%S')\033[0m \033[1;34m|\033[0m $*"
}

warning_log() {
    echo -e "\033[1;33mWARNING:\033[0m \033[0;37m$(date +'%Y-%m-%d %H:%M:%S')\033[0m \033[1;34m|\033[0m $*"
}

error_log() {
    echo -e "\033[1;31mERROR:\033[0m \033[0;37m$(date +'%Y-%m-%d %H:%M:%S')\033[0m \033[1;34m|\033[0m $*"
}

init_jupyterlab() {
    info_log 升级conda
    conda update -n base -c defaults conda
    if [ $? -eq 0 ]; then
        # $?表示上一个命令的退出状态。如果conda update命令成功执行，则$?将返回0
        info_log "Conda已成功更新。"
    else
        error_log "Conda更新失败。"
        # exit 1
    fi
    conda info --envs | grep 'jupyterlab' && warning_log '环境已存在' || conda create -n jupyterlab python=3.11
    conda activate jupyterlab
    conda install -c conda-forge jupyterlab
    conda list | grep jupyter
    jupyter labextension list
}


rjupyter() {
  jupyter lab --no-browser \
    --ip=0.0.0.0 \
    --port 1024 \
    --notebook-dir="~/Resource/Ipynb" \
    --LabApp.token=zq \
    --ServerApp.allow_origin='*'
}