aws-kafkaconnect_list_plugins() {
    aws kafkaconnect list-custom-plugins | jq '[.customPlugins[] | del(.customPluginArn)]' | md2f && md2v
}
