clear_init_dir(){
sudo rm -rf ~/upload/Anaconda3-*-Linux-x86_64.sh;
sudo rm -rf ~/upload/mwget_0.1.0.orig.tgz;
sudo rm -rf ~/upload/apache-maven-3.6.0-bin.tar.gz;
sudo rm -rf ~/upload/aria2-1.34.0;
sudo rm -rf ~/upload/aria2-1.34.0.tar.gz;
sudo rm -rf ~/upload/ccat_linux-amd64-1.1.0.tar.gz;
sudo rm -rf ~/upload/chromedriver;
sudo rm -rf ~/upload/chromedriver_linux64.zip;
sudo rm -rf ~/upload/content.md;
sudo rm -rf ~/upload/crontab_neo;
sudo rm -rf ~/upload/dircolors.256dark;
sudo rm -rf ~/upload/dircolors.ansi-dark;
sudo rm -rf ~/upload/dircolors.ansi-light;
sudo rm -rf ~/upload/dircolors-solarized;
sudo rm -rf ~/upload/dive_0.4.0_linux_amd64.deb;
sudo rm -rf ~/upload/electron-quick-start;
sudo rm -rf ~/upload/gnome-terminal-colors-solarized;
sudo rm -rf ~/upload/go1.11.linux-amd64.tar.gz;
sudo rm -rf ~/upload/google-chrome-stable_current_amd64.deb;
sudo rm -rf ~/upload/guetzli;
sudo rm -rf ~/upload/guetzli.7z;
sudo rm -rf ~/upload/heirloom-mailx_12.5-4_amd64.deb;
sudo rm -rf ~/upload/http_load-09Mar2016;
sudo rm -rf ~/upload/http_load-09Mar2016.tar.gz;
sudo rm -rf ~/upload/jcat.py;
sudo rm -rf ~/upload/jdk-8u161-linux-x64.tar.gz;
sudo rm -rf ~/upload/krs_linux;
sudo rm -rf ~/upload/libpng-1.6.35;
sudo rm -rf ~/upload/libpng-1.6.35.tar;
sudo rm -rf ~/upload/linux-amd64-1.1.0;
sudo rm -rf ~/upload/loop;
sudo rm -rf ~/upload/manpages-zh-1.5.1;
sudo rm -rf ~/upload/manpages-zh-1.5.1.tar.gz;
sudo rm -rf ~/upload/manpages-zh-1.5.2;
sudo rm -rf ~/upload/manpages-zh-1.5.2.tar.bz2;
sudo rm -rf ~/upload/antigen.zsh;
# sudo rm -rf ~/upload/masscan
sudo rm -rf ~/upload/materialshell;
sudo rm -rf ~/upload/materialshell-oceanic.terminal;
# sudo rm -rf ~/upload/movie
sudo rm -rf ~/upload/mwget;
sudo rm -rf ~/upload/mwget_0.1.0.orig;
sudo rm -rf ~/upload/mwget_0.1.0.orig.tar.bz2;
#sudo rm -rf ~/upload/node_modules;
sudo rm -rf ~/upload/node-v10.14.1-linux-x64.tar;
sudo rm -rf ~/upload/__pycache__;
sudo rm -rf ~/upload/qshell;
sudo rm -rf ~/upload/redis-stable;
sudo rm -rf ~/upload/redis-stable.tar.gz;
sudo rm -rf ~/upload/sbt-1.2.6.tgz;
sudo rm -rf ~/upload/scala-2.12.7.tgz;
sudo rm -rf ~/upload/spider;
# sudo rm -rf ~/upload/svg
sudo rm -rf ~/upload/ta-lib;
sudo rm -rf ~/upload/ta-lib-0.4.0-src.tar.gz;
# sudo rm -rf ~/upload/test.json
# sudo rm -rf ~/upload/test.svg
# sudo rm -rf ~/upload/ubuntu_init
# sudo rm -rf ~/upload/ubuntu_init-master.tar.gz
# sudo rm -rf ~/upload/ulord
sudo rm -rf ~/upload/UlordRig-Linux-V1.0.0.zip;
sudo rm -rf ~/upload/US_Cities.txt;
sudo rm -rf ~/upload/webbench-1.5;
sudo rm -rf ~/upload/webbench-1.5.tar.gz;
sudo rm -rf ~/upload/zsh-syntax-highlighting-filetypes.zsh;
}

