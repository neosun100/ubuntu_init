

#  终端实时操作同步
# http://www.axiaoxin.com/article/201/
# https://linux.cn/article-2221-1.html
# https://www.oschina.net/p/ttyrec
# 通过浏览器实时分享终端内容

install_tty_online(){
echo "通过浏览器实时分享终端内容";
brew install ttyrec;
npm install -g ttyrec ttycast;
}


tty_server(){
echo "`date +'%Y-%m-%d %H:%M:%S'`  开启 tty server" | lcat;
rm /tmp/outfile.tty &>/dev/null;rm /tmp/ttycast &>/dev/null;ttyreccast /tmp/outfile.tty
}


tty_client(){
echo "`date +'%Y-%m-%d %H:%M:%S'`  开启 tty client" | lcat;
reset && ttyrec /tmp/ttycast;
}


tty_web(){
echo "`date +'%Y-%m-%d %H:%M:%S'`  web 端播放地址" | lcat;
open http://`ipconfig getifaddr en0`:13377
}
