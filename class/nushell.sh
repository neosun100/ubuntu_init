install_nushell() {
    echo "https://github.com/nushell/nushell/releases"
    brew install nushell
}

nuc() {
    if [[ -z $1 ]]; then
        echo "No command provided to nuc function."
        return 1
    fi

    if ! nu -c "$1"; then
        echo "The nu command failed to execute."
        return 1
    fi
}
