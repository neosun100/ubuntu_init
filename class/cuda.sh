

cuda_paths=(
    "/usr/local/cuda"           # 標準安裝路徑
    "${CUDA_HOME:-}"            # 自定義環境變量
    "/usr/local/cuda-12.4"      # 具體版本路徑
)

found_cuda=0
for path in "${cuda_paths[@]}"; do
    if [[ -n "$path" && -d "${path}/bin" && -x "${path}/bin/nvcc" ]]; then
        # 避免路徑重複添加
        if [[ ":${PATH}:" != *":${path}/bin:"* ]]; then
            export PATH="${path}/bin:$PATH"
        fi
        if [[ ":${LD_LIBRARY_PATH}:" != *":${path}/lib64:"* ]]; then
            export LD_LIBRARY_PATH="${path}/lib64:$LD_LIBRARY_PATH"
        fi
        found_cuda=1
        break
    fi
done

if (( found_cuda == 0 )); then
    echo "[CUDA Not Found] GPU acceleration disabled" >&2
fi