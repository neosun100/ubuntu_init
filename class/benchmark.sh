install_es_benchmark() {
    pip install esrally
    cd ~/.rally/benchmarks/tracks/default
    wget http://file.neo.pub/pmc-neo.gz
    tar -zxvf pmc-neo.gz
    cd ~/upload
}

aes_import_data() {
    source ~/upload/ubuntu_init/class/benchmark.env.sh

    loop esrally race \
        --track=pmc-neo \
        --target-hosts=$AES_HOST \
        --client-options="use_ssl:true,verify_certs:true,basic_auth_user:$AES_USER,basic_auth_password:$AES_PASSWORD" \
        --track-params="bulk_size:$BULK_SIZE,number_of_replicas:$NUM_OF_REPLICAS,number_of_shards:$NUM_OF_SHARDS" \
        --challenge="append-no-conflicts" \
        --revision=$REVISION \
        --user-tag=$TAG \
        --pipeline=benchmark-only \
        --report-file=$REPORT_FILE \
        --report-format=$REPORT_FORMAT
}

aes_benchmark_run() {
    source ~/upload/ubuntu_init/class/benchmark.env.sh

    # benchmark_tag=${1:-hot}

    # esrally race \
    #     --track-path="~/upload/ubuntu_init/class/benchmark_aes.json" \
    #     --target-hosts=$AES_HOST \
    #     --client-options="use_ssl:true,verify_certs:true,basic_auth_user:$AES_USER,basic_auth_password:$AES_PASSWORD" \
    #     --revision=$REVISION \
    #     --user-tag=$benchmark_tag \
    #     --pipeline=benchmark-only \
    #     --report-file=$REPORT_FILE \
    #     --report-format=$REPORT_FORMAT

    esrally race \
        --track-path="~/upload/ubuntu_init/class/benchmark_aes.json" \
        --target-hosts=$AES_HOST \
        --client-options="use_ssl:true,verify_certs:true,basic_auth_user:$AES_USER,basic_auth_password:$AES_PASSWORD" \
        --revision=$REVISION \
        --user-tag=$TAG \
        --pipeline=benchmark-only \
        --report-file=$REPORT_FILE \
        --report-format=$REPORT_FORMAT

}

aes_benchmark_list() {
    esrally list races

}

aes_benchmark_compare() {
    esrally compare \
        --baseline=${1:-f3503f31-9aa1-40e1-9c23-33cf78991fe5} \
        --contender=${2:-696f72fa-39e6-4b8c-91bf-bea7addd6fc6} | grep "Baseline\|90th percentile service time"
}


install_ab(){
    sudo apt-get install apache2-utils  -y
    echo "ab -n 5000 -c 100 https://search-prd-opensearch-internet-rkg7a427rkyfrpgyt64kxm3huq.us-east-1.es.amazonaws.com/  "
}


install_siege(){
    brew install siege 
    siege -h
    siege https://www.bh3.com/_nuxt/img/loading-logo.f0c3ebe.png   -r2 -c25 --benchmark  -j | jq '[.]' | md2f && md2v
    siege https://www.bh3.com/_nuxt/img/loading-logo.f0c3ebe.png   -r2 -c25 --benchmark  -j | jq '[.]' | md-table
    siege https://www.bh3.com/_nuxt/img/loading-logo.f0c3ebe.png   -r2 -c25 --benchmark  -j | jq | jcat


}