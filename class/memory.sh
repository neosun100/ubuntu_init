

# 内存相关命令

mem_all(){
echo "https://blog.csdn.net/yongqingcloud/article/details/8489710"
echo "查看内存槽数、那个槽位插了内存，大小是多少"
echo "🐼 🐼 🐼 🐼 🐼 🐼 🐼 🐼 🐼 🐼 🐼 🐼 🐼 🐼 🐼 🐼 🐼 🐼 🐼 🐼 🐼 🐼 🐼"
sudo dmidecode|grep -P -A5 "Memory\s+Device"|grep Size|grep -v Range | lcat
sleep 3
sudo dmidecode -t 17 | lcat
sleep 1
echo ""
echo "查看最大支持内存数"
echo "🐼 🐼 🐼 🐼 🐼 🐼 🐼 🐼 🐼 🐼 🐼 🐼 🐼 🐼 🐼 🐼 🐼 🐼 🐼 🐼 🐼 🐼 🐼"
sudo dmidecode|grep -P 'Maximum\s+Capacity' | lcat
sleep 3
sudo dmidecode -t 16 | lcat
sleep 1
echo ""
echo "查看槽位上内存的速率，没插就是unknown。"
echo "🐼 🐼 🐼 🐼 🐼 🐼 🐼 🐼 🐼 🐼 🐼 🐼 🐼 🐼 🐼 🐼 🐼 🐼 🐼 🐼 🐼 🐼 🐼"
sudo dmidecode|grep -A16 "Memory Device"|grep 'Speed' | lcat
}