install_see_cpu() {
    cd ~/upload
    git clone https://github.com/Dr-Noob/cpufetch
    cd cpufetch/
    make
    sudo mv -f ./cpufetch /usr/local/bin/cpufetch
    sudo chmod 777 /usr/local/bin/cpufetch
    cd ~/upload
    cpufetch
    sudo rm -rf ~/upload/cpufetch
}

# 查询 CPU版本
see_cpu() {
    cpufetch $* 2>/dev/null 
}

# 系统版本
see_os() {
    neofetch $*
}
