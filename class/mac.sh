# mac  独有的 命令行

# 清理内存 clear memory
alias cm="sudo purge"

# mac 换 清华源
export HOMEBREW_BOTTLE_DOMAIN=https://mirrors.ustc.edu.cn/homebrew-bottles

# brew 源 优化

brew_zh_source() {
  cd "$(brew --repo)" &&
    git remote set-url origin https://mirrors.ustc.edu.cn/brew.git &&
    cd "$(brew --repo)/Library/Taps/homebrew/homebrew-core" &&
    git remote set-url origin https://mirrors.ustc.edu.cn/homebrew-core.git &&
    cd "$(brew --repo)/Library/Taps/homebrew/homebrew-cask" &&
    git remote set-url origin https://mirrors.ustc.edu.cn/homebrew-cask.git
  export HOMEBREW_BOTTLE_DOMAIN=https://mirrors.ustc.edu.cn/homebrew-bottles &&
    brew update
}

# 咖啡因，类似于 gui LUNGO

# 按⌚️秒，默认1个小时
cfi_t() {
  caffeinate -t ${1:-3600}
}

# 按进程🆔
cfi_p() {
  caffeinate -w
}

# 查看当前 mac 的 若睡眠的状态
stat_sleep() {
  echo "❄️❄️❄️❄️❄️❄️❄️❄️❄️❄️❄️❄️❄️❄️❄️"
  pmset -g
  echo "🌧️🌧️🌧️🌧️🌧️🌧️🌧️🌧️🌧️🌧️🌧️🌧️🌧️🌧️🌧️"
  pmset -g | grep hibernatemode
}

# 电脑合盖后，数据写入硬盘，清空内存，启动会较慢，3～5秒
deep_sleep() {
  sudo pmset -a hibernatemode 25
}

# 默认睡眠
default_sleep() {
  sudo pmset -a hibernatemode 3
}

# rjupyter
rjupyter() {
  jupyter notebook --no-browser \
    --NotebookApp.ip=0.0.0.0 \
    --port 1024 \
    --notebook-dir="~/Resource/Ipynb" \
    --NotebookApp.token=zq \
    --NotebookApp.iopub_data_rate_limit=99999999999999 \
    --NotebookApp.allow_origin='*'
}

# 上传开发的app到蒲公英
upload_app() {
  curl -F $1 -F '_api_key=28bda8dcb05eba43a841b26ea9cdfdf1' https://www.pgyer.com/apiv2/app/upload
}

# 设置hostname
set_hostname() {
  sudo scutil --set HostName ${1:-Neone}
}

# mac 系统更新
mac_ug() {
  brew update && brew upgrade
}

# fix mac
# dyld: Library not loaded: /usr/local/opt/readline/lib/libreadline.7.dylib
#   Referenced from: /usr/local/bin/awk
#   Reason: image not found
# dyld: Library not loaded: /usr/local/opt/readline/lib/libreadline.7.dylib
#   Referenced from: /usr/local/bin/awk
#   Reason: image not found
# error connecting to /private/tmp/tmux-501/default (No such file or directory)

fix_dylib() {
  echo '检查是否存在必须文件 libreadline.*.dylib'
  l /usr/local/opt/readline/lib/ | grep dylib
  echo "创建软连接"
  ln -s /usr/local/opt/readline/lib/libreadline.7.dylib /usr/local/opt/readline/lib/libreadline.8.0.dylib
  echo "重新打开shell 查看是否生效"
}

# 安装flutter mac 依赖
# 执行了2次
fix_flutter_depend_mac() {
  brew uninstall --ignore-dependencies libimobiledevice
  brew uninstall --ignore-dependencies usbmuxd
  brew install --HEAD usbmuxd
  brew unlink usbmuxd
  brew link usbmuxd
  brew install --HEAD libimobiledevice
  brew install ideviceinstaller ios-deploy cocoapods
  pod setup
}

# 安装完安卓studio -- 安卓sdk后
export_flutter_path() {
  export ANDROID_HOME=/Users/neo/Library/Android/sdk
  export PATH=${PATH}:${ANDROID_HOME}/tools
  export PATH=${PATH}:${ANDROID_HOME}/platform-tools
  export PUB_HOSTED_URL=https://pub.flutter-io.cn
  export FLUTTER_STORAGE_BASE_URL=https://storage.flutter-io.cn
  export PATH=~/flutter/bin:$PATH
}

export_flutter_path

# gem 换国内源
fix_gem_source() {
  gem sources --remove https://rubygems.org/
  gem source -a https://gems.ruby-china.com
  gem sources -l
}

# pod_setup 本质就是 git 资源，太慢了，自己迅雷下载解压一样
fix_pod_setup_over() {
  pod setup # 初始化生成目录，可能需要终止命令
  # pod repo remove master;
  # pod repo add master https://github.com/CocoaPods/Specs.git;
  cd /Users/neo/.cocoapods/repos
  wget https://github.com/CocoaPods/Specs/archive/master.zip
  unzip master.zip >/dev/null 2>&1
  mv ~/.cocoapods/repos/Specs-master ~/.cocoapods/repos/master
  flutter doctor
}

# dyld: Library not loaded: /usr/local/opt/readline/lib/libreadline.7.dylib
fix_dyld() {
  cd /usr/local/opt/readline/lib/
  ln -s libreadline.dylib libreadline.7.dylib
}

# 安装dart
install_dart() {
  brew tap dart-lang/dart
  brew install dart
  # brew install dart --with-content-shell --with-dartium;
  # 如果你需要开发 web 应用，则还需要安装 Dartium 和 Content Shell：
  echo "Dart SDK， 里面有 Dart VM、核心库、一些命令行工具，例如 dart、 dartanalyzer、 pub、 和 dartdoc。" | lcat
  dart --version
}

#  升级dart
update_dart() {
  echo "比较慢，预计 2 分钟" | lcat
  brew update
  brew upgrade dart
  brew cleanup dart
}

# brew  安装软件的目录
brew_path() {
  brew --prefix
}

# dart的安装目录 含sdk
dart_path() {
  echo "HOMEBREW_INSTALL: $(brew --prefix)" | lcat
  echo "SDK 目录: HOMEBREW_INSTALL/opt/dart/libexec" | lcat
  echo "Dartium: HOMEBREW_INSTALL/opt/dart/Chromium.app" | lcat
}

# https://blog.csdn.net/qq944417919/article/details/84297806
# 解决电脑休眠后蓝牙链接唤起mac的问题
init_sleepwatcher() {
  echo "🏵 🏵 🏵 🏵 🏵 🏵 🏵 🏵 🏵 🏵 🏵 🏵 🏵 🏵 🏵 🏵 🏵 🏵 🏵 🏵"
  echo "# https://blog.csdn.net/qq944417919/article/details/84297806" | lcat
  echo "# 解决电脑休眠后蓝牙链接唤起mac的问题" | lcat
  echo "🏵 🏵 🏵 🏵 🏵 🏵 🏵 🏵 🏵 🏵 🏵 🏵 🏵 🏵 🏵 🏵 🏵 🏵 🏵 🏵"
  echo ""
  echo "$(date +'%Y-%m-%d %H:%M:%S') 下载蓝牙控制软件" | lcat
  # cd ~/upload;
  # wget https://neone-club.sh1a.qingstor.com/Sys/blueutil.zip;
  # echo "把下载好的blueutil解压，把文件夹中的blueutil放在一个固定位置，" | lcat;
  # echo "本人放在/usr/local/bin/中，需要编辑 .sleep和 .wakeup文件分别增加对蓝牙的控制。 " | lcat;
  # echo "off为关闭，on 为打开" | lcat;
  # sleep 10;
  # echo "`date +'%Y-%m-%d %H:%M:%S'` 部署 blueutil" | lcat;
  # unzip blueutil.zip && \
  # chmod 777  ~/upload/blueutil/blueutil && \
  # mv ~/upload/blueutil/blueutil  /usr/local/bin/blueutil;
  brew install blueutil
  # brew link --overwrite blueutil;
  # echo "`date +'%Y-%m-%d %H:%M:%S'` 清理 blueutil遗留无用文件"| lcat;
  # rm -rf ~/upload/blueutil.zip && rm -rf ~/upload/blueutil;

  echo "$(date +'%Y-%m-%d %H:%M:%S') 安装 sleepwatcher" | lcat
  brew install sleepwatcher
  echo "$(date +'%Y-%m-%d %H:%M:%S') 设置软件服务自启动" | lcat
  brew services start sleepwatcher
  echo " $(date +'%Y-%m-%d %H:%M:%S') 查看进程是否启动,如果启动可以看到其中有两个内容。分别是 .sleep 和 .wakeup，分别对应睡眠和唤醒" | lcat
  echo "wakeup  进程" | lcat
  ps -ef | grep wakeup
  echo "sleep  进程" | lcat
  ps -ef | grep sleep
  echo "$(date +'%Y-%m-%d %H:%M:%S') 编写配置脚本 在 ~ 目录下创建文件 .sleep 和 .wakeup 并赋予权限 777" | lcat
  echo "" >~/.sleep
  echo "/usr/local/bin/blueutil off" >>~/.sleep
  echo "networksetup -setairportpower en0 off" >>~/.sleep

  echo "" >~/.wakeup
  echo "/usr/local/bin/blueutil on" >>~/.wakeup
  echo "networksetup -setairportpower en0 on" >>~/.wakeup

  echo "$(date +'%Y-%m-%d %H:%M:%S') 完成安装" | lcat
}

install_xgboost() {
  # 首先，gcc-8使用Homebrew（https://brew.sh/）获取以启用多线程（即，使用多个CPU线程进行训练）。
  # 默认的Apple Clang编译器不支持OpenMP，因此使用默认的编译器将禁用多线程。
  # https://xgboost.readthedocs.io/en/latest/build.html
  brew install gcc@8
  pip install xgboost
}

# mac 升级后的迁移文件和配置
mac_migrate_file() {
  echo "mac 升级后的迁移文件和配置目录"
  open "/Users/Shared/Relocated Items"
}

mac_pycharm_config() {
  echo "idea 系列软件的 配置文件"
  l /Users/neo/Conf/PyCharm-settings.zip
}

fix_ntfs_read_only() {
  echo "解决 mac 挂载的移动硬盘 Read-only file system"
  echo "https://blog.csdn.net/sunbiao0526/article/details/8566317"
  echo ""
  echo ""
  echo "查找磁盘名"
  diskutil info /Volumes/${1:-data} | grep Node
  echo " 弹出磁盘"
  real_data=$(diskutil info /Volumes/${1:-data} | grep Node | awk "{print $${1:-3}}")
  echo "hdiutil eject /Volumes/${1:-data}"
  echo "创建一个新的目录用于挂载"
  sudo mkdir /Volumes/${2:-raData}
  echo "将NTFS硬盘 挂载 mount 到mac"
  sudo mount_ntfs -o rw,nobrowse /dev/disk2s1 /Volumes/${2:-raData}/
  # /dev/disk2s1 就是 磁盘名
}

# 当行修复rabook nas
fix_rabook_nas() {
  real_data=$(diskutil info /Volumes/${1:-data} | grep Node | awk "{print \$${1:-3}}") &&
    hdiutil eject /Volumes/${1:-data} &&
    sudo mkdir /Volumes/${2:-raData} &&
    sudo mount_ntfs -o rw,nobrowse $real_data /Volumes/${2:-raData} &&
    open /Volumes/raData
}

# 打开rabook nas 文件夹
open_rabook_nas() {
  open /Volumes/raData
}

docker_desktop_log() {
  pred='process matches ".*(ocker|vpnkit).*"
  || (process in {"taskgated-helper", "launchservicesd", "kernel"} && eventMessage contains[c] "docker")'
  /usr/bin/log stream --style syslog --level=debug --color=always --predicate "$pred"
}

close_time_machine() {
  echo "关闭时间机器本地备份"
  sudo tmutil disable
}

remove_disk() {
  echo "卸载磁盘"
  sudo diskutil unmount ${1:-/Volumes/backup}
  # diskutil unmount ${1:-/Volumes/NeoneBackup};
  sdf
}

remove_time_machine_disk() {
  close_time_machine
  remove_disk "/Volumes/backup"
  sdf
}

# 简化命令版的 remove_disk
rmd() {
  echo "卸载磁盘" | lcat
  sudo diskutil unmount ${1:-/Volumes/backup} | lcat
  # diskutil unmount ${1:-/Volumes/NeoneBackup};
  sdf
}

nmail() {
  echo "查看用户系统日志"
  scat "/var/mail/${1:-$(whoami)}" | lcat
}

# 亮度调节
# http://www.github.com/kfix/ddcctl
# https://www.helplib.com/GitHub/article_127639

airdisk_up() {
  echo "$(date +'%Y-%m-%d %H:%M:%S') 装载 asmb 到 ~/upload/airdisk" | lcat
  mkdir ~/upload/airdisk
  mount_smbfs //AirDisk:neosun100@192.168.31.35/AirDisk ~/upload/airdisk
  l ~/upload/airdisk
  echo "$(date +'%Y-%m-%d %H:%M:%S') 装载 asmb 完成✅" | lcat
  open ~/upload/airdisk
}

airdisk_down() {
  diskutil umount ~/upload/airdisk
  echo "$(date +'%Y-%m-%d %H:%M:%S') 卸载 asmb 完成✅" | lcat
}

mac_tem() {
  # sudo gem install iStats
  istats --no-graphs | grep -v "Fan\|fans\|health\|count\|more\|0.0°C" | lcat
}

mac_tmux_help() {
  echo "python -m http.server 7878 --directory /Users/rabook/Resource/Ipynb/mp4/"
  echo "h"
  echo "nload"
  echo "loop mac_tem"
  echo "loop pnamekill cron"
  echo "loop_60 adb_pi_auto_up"
  echo "loop_60 adb_auto_power"
  echo "loop_60 adb_hold_on"
}

mwget() {
  axel -n 10 -a $*
}

brew_best_source() {
  #替换brew.git:
  cd "$(brew --repo)" &&
    git remote set-url origin https://mirrors.ustc.edu.cn/brew.git

  #替换homebrew-core.git:
  cd "$(brew --repo)/Library/Taps/homebrew/homebrew-core" &&
    git remote set-url origin https://mirrors.ustc.edu.cn/homebrew-core.git

  # 重置brew.git:
  # cd "$(brew --repo)"
  # git remote set-url origin https://github.com/Homebrew/brew.git

  # 重置homebrew-core.git:
  # cd "$(brew --repo)/Library/Taps/homebrew/homebrew-core"
  # git remote set-url origin https://github.com/Homebrew/homebrew-core.git

  export HOMEBREW_NO_AUTO_UPDATE=true

  brew update

  cd ~/upload
}

up_rabook_nfs() {
  echo "客户端查看nfs服务清单"
  select_nfs_list 192.168.2.31
  echo "客户端查看本地挂载的rabook nfs目录"
  select_nfs_local 192.168.2.31
  echo "挂载rabook nfs"
  mount_nfs 192.168.2.31:/Users/rabook/upload/nfs
  echo "打开rabook nfs"
  open_nfs
}

# mac服务
mac_only_local_login() {
  echo "打开系统共享管理——允许✅远程登陆"

  echo "限制ip登陆"
  echo 'AllowUsers neo@127.0.0.1'
  sleep 5
  sudo vim /etc/ssh/sshd_config
  fcat /etc/ssh/sshd_config

  echo "停止sshd服务"
  sudo launchctl unload -w /System/Library/LaunchDaemons/ssh.plist

  echo "启动sshd服务"
  sudo launchctl load -w /System/Library/LaunchDaemons/ssh.plist

  echo "查看是否启动"
  sudo launchctl list | grep ssh
}

check_ssh_local() {
  echo 'AllowUsers neo@127.0.0.1' | pbcopy
  [[ $(nat /etc/ssh/sshd_config | grep "AllowUsers neo@127.0.0.1" | wc -l | field) = 1 ]] &&
    (echo $(date +'%Y-%m-%d %H:%M:%S') 检查完成 🔐 仅可 127.0.0.1登陆 | lcat) ||
    (echo $(date +'%Y-%m-%d %H:%M:%S') 检查完成 💣 不安全 重新添加 🚫 ip登陆 | lcat && echo $(date +'%Y-%m-%d %H:%M:%S') "已粘贴入剪切板 进入文件直接粘贴即可" | lcat && time_wait && sudo vim /etc/ssh/sshd_config && echo $(date +'%Y-%m-%d %H:%M:%S') 设置完成✅ | lcat)
}

auth_code() {
  echo "get ${1:-Google} auth"
  echo "$(curl -s http://127.0.0.1:19877/code/${1:-Google})"
  curl -s http://127.0.0.1:19877/code/${1:-Google} | pbcopy
}

auth() {
  code=${1:-ag}
  if [ $code = "ok" ]; then
    auth_code OKEx
  elif [ $code = "ok2" ]; then
    auth_code OKEx.sub
  elif [ $code = "h" ]; then
    auth_code huobi
  elif [ $code = "f" ]; then
    auth_code fmz@Bot
  elif [ $code = "g" ]; then
    auth_code Google
  elif [ $code = "d" ]; then
    auth_code Discord
  elif [ $code = "ac" ]; then
    auth_code AWS-China
  elif [ $code = "ag" ]; then
    auth_code AWS-Global
  else
    echo "$code code未知！"
  fi
}

ntmux_yml_restore() {
  echo "还原yml文件"
  create_dir ~/.tmuxinator
  rm -rf ~/.tmuxinator/tmux_standard.yml &&
    cp ~/upload/ubuntu_init/profile/tmux_mac.yml ~/.tmuxinator/tmux_standard.yml &&
    rm -rf ~/.tmuxinator/tmux_big.yml &&
    cp ~/upload/ubuntu_init/profile/tmux_big.yml ~/.tmuxinator/tmux_big.yml

  echo "修复完成"
  l ~/.tmuxinator/
}

ntmux_first() {
  tmux new -s unused
}

ntmux() {
  echo "重启开机时执行"

  tmuxinator start tmux_standard

}

tmux_kill() {
  tmux kill-session -t unused
  tmux kill-session -t 0
}

ntmux_big() {
  echo "重启开机时执行"
  echo "针对超多核心的服务器"
  tmuxinator start tmux_big
}

tmux_size() {
  tmux list-windows | keycut "[layout " "] @1 (active)" | lcat
}

vimt() {
  echo "编辑tmux布局文件"
  vim ~/.tmuxinator/tmux_standard.yml
}

brew_all() {
  brew update && brew upgrade && brew cleanup
  brew doctor
}

install_redis-cli() {
  brew tap ringohub/redis-cli &&
    brew update && brew doctor &&
    brew install redis-cli
}

export PATH="$PATH":"$HOME/Flutter/flutter/.pub-cache/bin"

# cm trash
cmt() {
  echo "清空垃圾桶"
  osascript -e 'tell application "Finder" to empty trash'
}

#neo message
nm() {
  sqlite3 -line ~/Library/Messages/chat.db "SELECT m.ROWID, text, MAX(date) lastMessageDate, h.id FROM message m INNER JOIN handle h ON h.ROWID=m.handle_id GROUP BY h.ROWID ORDER BY  m.ROWID desc limit ${1:-5} "
}

# 高亮的 neo message
cnm() {
  nm | lat
}

# 复制 最新一条短信的验证码
# neo message code
nmc() {
  echo "$(nm | grep 验证码 | line 1 1)结尾" | keycut "验证码" "结尾" | cutnum | head -n 1
  echo "$(nm | grep 验证码 | line 1 1)结尾" | keycut "验证码" "结尾" | cutnum | head -n 1 | pbcopy
}

# 修复破解软件
fix_mac() {
  sudo spctl --master-disable
}

# 测cpu
test_cpu() {
  threads_num=$(sysctl machdep.cpu | grep machdep.cpu.thread_count | field 2)
  # sysbench --cpu-max-prime=10000 --num-threads=${1:-$threads_num} run;
  # sysbench cpu --cpu-max-prime=10000 --num-threads=${1:-$threads_num} run;
  sysbench cpu --cpu-max-prime=10000 --threads=${1:-$threads_num} run
}

# 系统版本
os_version() {
  sw_vers
}

install_fish() {
  brew install asciiquarium
}

nfish() {
  asciiquarium
}

idea_key() {
  echo "key网址如下"
  echo "http://idea.medeming.com/jets/"
  echo "$(date +'%Y-%m-%d %H:%M:%S') 下载key文件包" | lcat
  cd ~/upload
  mkdir -p ~/upload/my_idea_key
  nget http://idea.medeming.com/jets/images/jihuoma.zip
  echo "$(date +'%Y-%m-%d %H:%M:%S') 解压到 ~/upload/my_idea_key " | lcat
  # unzip -n -d ~/upload/my_idea_key jihuoma.zip
  7za x ~/upload/jihuoma.zip -r -o./my_idea_key
  # 7za x ~/upload/激活码.zip -r -o./my_idea_key
  echo "$(date +'%Y-%m-%d %H:%M:%S') 显示key 并粘贴入剪切板 " | lcat
  cd ~/upload/my_idea_key
  nat "$(ls ~/upload/my_idea_key | grep '20[1-9][0-9]' | sort -r | head -n 1)"
  scat "$(ls ~/upload/my_idea_key | grep '20[1-9][0-9]' | sort -r | head -n 1)" | pbcopy
  echo "$(date +'%Y-%m-%d %H:%M:%S')  处理完成，开始清理环境" | lcat
  cd ~/upload
  rm -rf ~/upload/jihuoma.zip
  rm -rf ~/upload/my_idea_key
  echo "$(date +'%Y-%m-%d %H:%M:%S') 清理完成✅" | lcat

}

# neo 剪切板
njq() {
  pbcopy
}

# mac 微信插件
install_wx_plugin() {
  echo "需要先安装微信"
  echo "https://github.com/neosun100/WeChatExtension-ForMac"
  curl -o- -L https://omw.limingkai.cn/install.sh | bash -s
}

install_mac_brew() {
  echo "真正的cn区速度源"
  /bin/zsh -c "$(curl -fsSL https://gitee.com/cunkai/HomebrewCN/raw/master/Homebrew.sh)"
}

ndate() {
  date | lcat
  date | njq
}

nblue_mouse() {
  ioreg -r -n AppleDeviceManagementHIDEventService -k BatteryPercent | perl -nE '/"BatteryPercent" = (\d++)/ && say $1'
  # ioreg -c AppleDeviceManagementHIDEventService -r -l | grep -i BatteryPercent | cutnum
}

nmac_battery() {
  ioreg -rn AppleSmartBattery
}

remove_ios_simulator() {
  echo "删除多余的ios虚拟器" | lcat
  l /Library/Developer/CoreSimulator/Profiles/Runtimes/
  echo "sudo rm -rf /Library/Developer/CoreSimulator/Profiles/Runtimes/'iOS 12.1.simruntime' " | lcat
}

n_openvpn() {
  echo "Viscosity for mac注册码：" | bcat
  echo "用户名: The Shark" | bcat
  echo "邮箱: deep@sea.com" | bcat
  echo "序列号: VM1U-JKJRW4-2NSXY4-G25K7Q-AU6LCU-I56CL4" | bcat
  echo ""
  echo "软件下载地址：" | bcat
  echo "https://xclient.info/s/viscosity.html#versions"

}

nvolume() {
  osascript -e "set volume output volume ${1:-25}"
}

nvolume_max() {
  nvolume 100
}

nvolume_min() {
  nvolume 1
}

unsudo_help() {
  echo "Mac/Ubuntu/Linux 配置sudo免密码只需要如下两部：" | lcat
  echo "1 打开命令窗口输入如下命令：" | lcat
  echo "sudo visudo 或者  sudo vi /etc/sudoers" | lcat
  echo "2 替换 #%admin ALL=(ALL) ALL 为" | lcat
  echo "%admin ALL=(ALL) NOPASSWD: NOPASSWD: ALL" | lcat
}

function git() {
  /usr/bin/git $*
}

install_istat() {
  echo "mac 下风扇 温度 电池损耗"
  sudo gem install iStats
  sudo istats all
  echo "⚠️ 则执行 sudo chmod go-w  /usr/local/kafka"
  sudo istats scan
}

mac_istat_scan() {
  echo " 扫描 mac 硬件 扩充输出"
  sudo istats scan
}

mac_istat() {
  sudo istats disable all
  sudo istats all | lat
}

mac_istat_all() {
  sudo istats enable all
  sudo istats all | lat
}

# 迅雷
xl() {
  /Applications/Thunder.app/Contents/MacOS/Thunder $*
}

# 给smart 迅雷用的下载方式
# 打开迅雷后，执行命令即刻剪切进下载器
mv_url() {
  scat ~/upload/dytt8/movies.csv | line ${1:-1} ${2:-20} | awk -F"," '{print $6}' | grep ftp | njq
}

install_mac_nvme() {
  brew install smartmontools
}

nvme() {
  smartctl -a disk0 | lat
}

# pdftk() {
#   cd ~/upload
#   wget http://file.neo.pub/pdftk_server-2.02-mac_osx-10.11-setup.pkg
# }

# mac cli
install_mcli() {
  echo "mac 精简 cli"
  brew install m-cli
  m help
  chrome "https://blog.csdn.net/culiuman3228/article/details/108788119"
}

maven_go() {
  /Users/jiasunm/Documents/Tool/nexus-3.34.0-01-mac/nexus-3.34.0-01/bin/nexus start
}

install_mac_mouse() {
  brew install cliclick
  echo "mac 下的鼠标 CLI工具"
  echo "cliclick p 当前鼠标位置"
  echo "cliclick c:3188,873 点击鼠标该位置"

}

install_mac_ip_cli() {
  echo "mac下的ip命令"
  echo "https://github.com/brona/iproute2mac"
  curl --remote-name -L https://github.com/brona/iproute2mac/raw/master/src/ip.py
  chmod +x ip.py
  mv ip.py /usr/local/bin/ip
  echo "ip help"
  echo "ip link help"
  echo "ip addr help"
  echo "ip route help"
  echo "ip neigh help"
}

#  显示max 系统时区
mac_time_zone() {
  sudo systemsetup -gettimezone
}

# 查看蓝牙设备
# to_entries 抽 key，value 独立
# 🎾 🎾 🎾 🎾 🎾 🎾 🎾
# from
# {
#   "尼奥未来的黑鼠标": {
#     "device_addr": "E0-EB-40-5C-89-69",
#     "device_AFHEnabled": "attrib_On",
#     "device_AFHMap": "00080FBFFFBFFFF7F",
#     "device_batteryPercent": "28%",
#     "device_classOfDevice": "0x05 0x20 0x0580",
#     "device_ConnectionMode": "attrib_sniff_mode",
#     "device_core_spec": "3.0",
#     "device_fw_version": "0x0170",
#     "device_interval": "11.25 ms",
#     "device_isconfigured": "attrib_Yes",
#     "device_isconnected": "attrib_Yes",
#     "device_isNormallyConnectable": "attrib_Yes",
#     "device_ispaired": "attrib_Yes",
#     "device_majorClassOfDevice_string": "Peripheral",
#     "device_manufacturer": "Broadcom (0x5, 0x240C)",
#     "device_minorClassOfDevice_string": "Mouse",
#     "device_productID": "0x0269",
#     "device_role": "attrib_master",
#     "device_RSSI": -48,
#     "device_services": "Magic Mouse",
#     "device_supportsEDR": "attrib_Yes",
#     "device_supportsESCO": "attrib_No",
#     "device_supportsSSP": "attrib_Yes",
#     "device_vendorID": "0x004C"
#   }
# },
# 🎾 🎾 🎾 🎾 🎾 🎾 🎾
# to
# {
#   "key": "尼奥未来的黑鼠标",
#   "value": {
#     "device_addr": "E0-EB-40-5C-89-69",
#     "device_AFHEnabled": "attrib_On",
#     "device_AFHMap": "7F8FFFEFF7FFFD1FF7F",
#     "device_batteryPercent": "28%",
#     "device_classOfDevice": "0x05 0x20 0x0580",
#     "device_ConnectionMode": "attrib_sniff_mode",
#     "device_core_spec": "3.0",
#     "device_fw_version": "0x0170",
#     "device_interval": "11.25 ms",
#     "device_isconfigured": "attrib_Yes",
#     "device_isconnected": "attrib_Yes",
#     "device_isNormallyConnectable": "attrib_Yes",
#     "device_ispaired": "attrib_Yes",
#     "device_majorClassOfDevice_string": "Peripheral",
#     "device_manufacturer": "Broadcom (0x5, 0x240C)",
#     "device_minorClassOfDevice_string": "Mouse",
#     "device_productID": "0x0269",
#     "device_role": "attrib_master",
#     "device_RSSI": -50,
#     "device_services": "Magic Mouse",
#     "device_supportsEDR": "attrib_Yes",
#     "device_supportsESCO": "attrib_No",
#     "device_supportsSSP": "attrib_Yes",
#     "device_vendorID": "0x004C"
#   }
# },

# 添加字段 .value + {Name: .key}

# https://unix.stackexchange.com/questions/663338/change-order-of-lines-position-in-json-file
# https://gist.github.com/ipbastola/2c955d8bf2e96f9b1077b15f995bdae3
# https://gist.github.com/joar/776b7d176196592ed5d8
# https://stackoverflow.com/questions/29389010/jq-parsing-get-value

# json2keyvalue
# https://codingdict.com/questions/89598
mac_bluetooth() {
  system_profiler SPBluetoothDataType -json |
    jq '[.SPBluetoothDataType[] | .device_title[] | to_entries[] | .value + {Name: .key} ]  ' |
    md2f && md2v
}

install_autojump() {
  echo "https://github.com/wting/autojump"
  echo "A cd command that learns - easily navigate directories from the command line"
  brew install autojump
}

mac_version() {
  sw_vers
}

install_mac_top() {
  brew install asitop
  echo ' $(asitop) 是一个专门为 Apple Silicon（如 M1 和 M2 芯片）设计的性能监控工具。它运行在命令行界面中，用于实时监控和展示 Apple Silicon Mac 上的 CPU、GPU、内存等硬件的性能数据。$(asitop) 的目标是提供一个简洁且高效的方式来观察和分析 Apple Silicon 硬件的性能表现。通过 $(brew install asitop) 命令，你可以在使用 Homebrew 的 macOS 系统上安装 $(asitop)。一旦安装，你可以在终端中运行 $(asitop) 命令来启动这个监控工具。它将显示各种性能指标，比如 CPU 和 GPU 的使用率、能耗、温度等，这对于性能调优和问题诊断非常有帮助。'
}
