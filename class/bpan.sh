


# 🏈 🏈 🏈 🏈 🏈 🏈 🏈 🏈 🏈 🏈 🏈 🏈 🏈 🏈 🏈 🏈 🏈 🏈 🏈 🏈 🏈 🏈 🏈 
#  部署百度客户端命令行工具
# web端有问题，先pass了

deploy_bpan(){
cd ~/upload;
echo "`date +'%Y-%m-%d %H:%M:%S'` 获取最新下载链接" | lcat;

if [ `uname -m` = "armv7l" ] && [ `uname` = "Linux" ]; then
  echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为arm Linux" | lcat
  typeset subfile="-linux-arm.zip"

elif [ `uname -m` = "aarch64" ] && [ `uname` = "Linux" ]; then
  echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为arm64 Linux" | lcat
  typeset subfile="-linux-arm64.zip"

  # 执行代码块
elif [ `uname -m` = "x86_64" ] && [ `uname` = "Linux" ]; then
  echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为X86 Linux" | lcat
  typeset subfile="-linux-amd64.zip"

  # 执行代码块
elif [ `uname -m` = "x86_64" ] && [ `uname` = "Darwin" ]; then
  echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为X86 Mac" | lcat
  typeset subfile="-mac-amd64.zip"

  # 执行代码块
else
  echo "`uname` 系统未知！"
fi

file=`cut_file_url https://github.com/qjfoidnh/BaiduPCS-Go/releases  /qjfoidnh/BaiduPCS-Go/releases/download/   $subfile`;
echo "";
echo "";
echo "`date +'%Y-%m-%d %H:%M:%S'` 获取链接成功开始下载文件" | lcat;
mwget $file;
echo "";
echo "";
echo "`date +'%Y-%m-%d %H:%M:%S'` 下载完成解压缩并重命名" | lcat;
# tar zxvf frp_*$subfile;
unzip BaiduPCS-*$subfile;
mkdir ~/upload/bpan;
mv ~/upload/BaiduPCS-Go-*/* ~/upload/bpan;
echo "";
echo "";
echo "`date +'%Y-%m-%d %H:%M:%S'` 登陆 baidupan 客户端" | lcat;
~/upload/bpan/BaiduPCS-Go  help | lcat;
time_wait; #  等待 5秒
~/upload/bpan/BaiduPCS-Go login;
~/upload/bpan/BaiduPCS-Go who;
~/upload/bpan/BaiduPCS-Go loglist;
~/upload/bpan/BaiduPCS-Go pwd;
~/upload/bpan/BaiduPCS-Go config;
echo "";
echo "";
echo "`date +'%Y-%m-%d %H:%M:%S'` 最优化 bpan 设置" | lcat;
~/upload/bpan/BaiduPCS-Go config set -appid=266719;
~/upload/bpan/BaiduPCS-Go config set -cache_size=256KB;
~/upload/bpan/BaiduPCS-Go config set -max_parallel=100;
~/upload/bpan/BaiduPCS-Go config set -max_upload_parallel=100;
~/upload/bpan/BaiduPCS-Go config set -max_download_load=5;
echo "";
echo "";
echo "`date +'%Y-%m-%d %H:%M:%S'` bpan默认下载地址就不变了" | lcat;
echo "需要修改可以执行如下命令" | lcat;
echo '~/upload/bpan/BaiduPCS-Go config set -savedir=/Volumes/data/baidu-pan' | lcat;
time_wait; #  等待 5秒
echo "";
echo "";
echo "`date +'%Y-%m-%d %H:%M:%S'` 部署 bpan 完成✅" | lcat;
}





bpan_init(){
[[ -d ~/upload/bpan ]] && echo "`date +'%Y-%m-%d %H:%M:%S'` bpan 已部署✅"  || deploy_bpan
}


bpan(){
~/upload/bpan/BaiduPCS-Go $*
}


cbpan(){
if [ `uname -m` = "armv7l" ] && [ `uname` = "Linux" ]; then
  # echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为arm Linux" | lcat
  ~/upload/bpan/BaiduPCS-Go $*
#   typeset sysname="_linux_arm"
  # 执行代码块
elif [ `uname -m` = "aarch64" ] && [ `uname` = "Linux" ]; then
  # echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为X86 Linux" | lcat
  ~/upload/bpan/BaiduPCS-Go $*
#   typeset sysname="_linux_amd64"
  # 执行代码块
elif [ `uname -m` = "x86_64" ] && [ `uname` = "Linux" ]; then
  # echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为X86 Linux" | lcat
  ~/upload/bpan/BaiduPCS-Go $* | lcat
#   typeset sysname="_linux_amd64"
  # 执行代码块
elif [ `uname -m` = "x86_64" ] && [ `uname` = "Darwin" ]; then
  # echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为X86 Mac" | lcat
  ~/upload/bpan/BaiduPCS-Go $* | lcat

#   typeset sysname="_darwin_amd64"
  # 执行代码块
else
  echo "`uname` 系统未知！"
fi
}