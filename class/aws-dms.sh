aws_dms_target_kinesis() {
    # https://docs.aws.amazon.com/zh_cn/dms/latest/userguide/CHAP_Target.Kinesis.html
    echo "提前创建好Kinesis data stream 和 DMS role"
    aws dms create-endpoint \
        --endpoint-identifier=${target_endpoint_name:-kinesis-cdc} \
        --engine-name kinesis \
        --endpoint-type target \
        --region us-east-1 \
        --kinesis-settings ServiceAccessRoleArn=arn:aws:iam::835751346093:role/DMS-all-policy,StreamArn=arn:aws:kinesis:us-east-1:835751346093:stream/KDS-On-demand,MessageFormat=json-unformatted,IncludeControlDetails=true,IncludeNullAndEmpty=false,IncludeTransactionDetails=true,IncludePartitionValue=true,PartitionIncludeSchemaTable=true,IncludeTableAlterOperations=true
}

aws_dms_target_msk() {
    # https://docs.aws.amazon.com/zh_cn/dms/latest/userguide/CHAP_Target.Kafka.html
    echo "提前创建好 MSK "
    aws dms create-endpoint \
        --endpoint-identifier=${target_endpoint_name:-kafka-cdc} \
        --engine-name kafka \
        --endpoint-type target \
        --region us-east-1 \
        --kafka-settings 'Broker="b-2.tesla.33lkxf.c19.kafka.us-east-1.amazonaws.com:9092,b-1.tesla.33lkxf.c19.kafka.us-east-1.amazonaws.com:9092,b-3.tesla.33lkxf.c19.kafka.us-east-1.amazonaws.com:9092",Topic=mysql-cdc,MessageFormat=json-unformatted,IncludeControlDetails=true,IncludeNullAndEmpty=false,IncludeTransactionDetails=true,IncludePartitionValue=true,PartitionIncludeSchemaTable=true,IncludeTableAlterOperations=true'
}

aws_dms_source_aurora() {
    # https://docs.aws.amazon.com/zh_cn/dms/latest/userguide/CHAP_Target.Kafka.html
    echo "提前创建好 Aurora 且只能使用写实例"
    aws dms create-endpoint \
        --endpoint-identifier=${target_endpoint_name:-mysql-cdc} \
        --engine-name mysql \
        --endpoint-type source \
        --region us-east-1 \
        --my-sql-settings EventsPollInterval=1,MaxFileSize=512,ParallelLoadThreads=8,Password=AWS2themoon,Port=3306,ServerName=aurora-tesla-instance-1-us-east-1a.cjcyzs87nm4g.us-east-1.rds.amazonaws.com,ServerTimezone=US/Pacific,Username=root
}

aws_dms_task() {
    mkdir -p ~/upload/aws_dms_task

    cat >~/upload/aws_dms_task/dms-task-aurora-mysql-2-kinesis.json <<EOF
{
    "rules": [
        {
            "rule-type": "selection",
            "rule-id": "1",
            "rule-name": "1",
            "object-locator": {
                "schema-name": "tesla",
                "table-name": "taxi_order"
            },
            "rule-action": "include",
            "filters": []
        }
    ]
}
EOF

    aws dms create-replication-task \
        --replication-task-identifier ${task_name:-aurora-mysql-2-kds} \
        --source-endpoint-arn arn:aws:dms:us-east-1:835751346093:endpoint:IR5XKDKN6VAUO3ZH4SVZXGN6WIVAFDN3WG46QYQ \
        --target-endpoint-arn arn:aws:dms:us-east-1:835751346093:endpoint:LHZBSI54DA6ZGCXSHB3WJHQBXK74MDQ3KPFTEWI \
        --replication-instance-arn arn:aws:dms:us-east-1:835751346093:rep:test \
        --migration-type full-load-and-cdc \
        --table-mappings file://~/upload/aws_dms_task/dms-task-aurora-mysql-2-kinesis.json

}

aws_dms_task_msk() {
    mkdir -p ~/upload/aws_dms_task

    cat >~/upload/aws_dms_task/dms-task-aurora-mysql-2-msk.json <<EOF
{
    "rules": [
        {
            "rule-type": "selection",
            "rule-id": "1",
            "rule-name": "1",
            "object-locator": {
                "schema-name": "tesla",
                "table-name": "taxi_order"
            },
            "rule-action": "include",
            "filters": []
        },
        {
            "rule-type": "object-mapping",
            "rule-id": "2",
            "rule-name": "MapToKafka1",
            "rule-action": "map-record-to-record",
            "kafka-target-topic": "mysql-cdc",
            "object-locator": {
                "schema-name": "tesla",
                "table-name": "taxi_order"
            },
            "partition-key": {"value": "id" }
        }
    ]
}
EOF

    aws dms create-replication-task \
        --replication-task-identifier ${task_name:-aurora-mysql-2-msk-v1} \
        --source-endpoint-arn arn:aws:dms:us-east-1:835751346093:endpoint:IR5XKDKN6VAUO3ZH4SVZXGN6WIVAFDN3WG46QYQ \
        --target-endpoint-arn arn:aws:dms:us-east-1:835751346093:endpoint:XEKDCYHIZVGURUEE6SCYEHDTBWTPPE3I5QTZYIQ \
        --replication-instance-arn arn:aws:dms:us-east-1:835751346093:rep:test \
        --migration-type full-load-and-cdc \
        --table-mappings file://~/upload/aws_dms_task/dms-task-aurora-mysql-2-msk.json

}

aws_dms_task_msk() {
    mkdir -p ~/upload/aws_dms_task

    cat >~/upload/aws_dms_task/dms-task-aurora-mysql-2-msk.json <<EOF
{
    "rules": [
        {
            "rule-type": "selection",
            "rule-id": "1",
            "rule-name": "1",
            "rule-action": "include",
            "object-locator": {
                "schema-name": "tesla",
                "table-name": "%"
            }
        },
        {
            "rule-type": "object-mapping",
            "rule-id": "2",
            "rule-name": "DefaultMapToKafka",
            "rule-action": "map-record-to-record",
            "object-locator": {
                "schema-name": "tesla",
                "table-name": "taxi_order"
            }
        }
    ]
}
EOF

    aws dms create-replication-task \
        --replication-task-identifier ${task_name:-aurora-mysql-2-msk-v2} \
        --source-endpoint-arn arn:aws:dms:us-east-1:835751346093:endpoint:IR5XKDKN6VAUO3ZH4SVZXGN6WIVAFDN3WG46QYQ \
        --target-endpoint-arn arn:aws:dms:us-east-1:835751346093:endpoint:XEKDCYHIZVGURUEE6SCYEHDTBWTPPE3I5QTZYIQ \
        --replication-instance-arn arn:aws:dms:us-east-1:835751346093:rep:test \
        --migration-type full-load-and-cdc \
        --table-mappings file://~/upload/aws_dms_task/dms-task-aurora-mysql-2-msk.json

}

aws_dms_task_msk() {
    mkdir -p ~/upload/aws_dms_task

    cat >~/upload/aws_dms_task/dms-task-aurora-mysql-2-msk.json <<EOF
{
    "rules": [
        {
            "rule-type": "selection",
            "rule-id": "1",
            "rule-name": "1",
            "object-locator": {
                "schema-name": "tesla",
                "table-name": "taxi_order"
            },
            "rule-action": "include",
            "filters": []
        },
        {
            "rule-type": "object-mapping",
            "rule-id": "2",
            "rule-name": "MapToKafka1",
            "rule-action": "map-record-to-record",
            "kafka-target-topic": "mysql-cdc-json",
            "object-locator": {
                "schema-name": "tesla",
                "table-name": "taxi_order"
            },
            "partition-key": {"value": "id" }
        }
    ]
}
EOF

    aws dms create-replication-task \
        --replication-task-identifier ${task_name:-aurora-mysql-2-msk-json} \
        --source-endpoint-arn arn:aws:dms:us-east-1:835751346093:endpoint:IR5XKDKN6VAUO3ZH4SVZXGN6WIVAFDN3WG46QYQ \
        --target-endpoint-arn arn:aws:dms:us-east-1:835751346093:endpoint:P7AIOFX5GOI74YJA7DDOOGGI7IDJGE5EPF5RAUQ \
        --replication-instance-arn arn:aws:dms:us-east-1:835751346093:rep:test \
        --migration-type full-load-and-cdc \
        --table-mappings file://~/upload/aws_dms_task/dms-task-aurora-mysql-2-msk.json

}
