install_mc() {
    brew install minio/stable/mc
    mc --help
    echo "https://docs.min.io/minio/baremetal/reference/minio-cli/minio-mc.html"
}


mcj(){
mc --json $*
}