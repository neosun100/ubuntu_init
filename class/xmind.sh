# 命令行下载 流程图工具
# https://graphviz.gitlab.io/documentation/
install_xmind(){
if [ `uname` = "Linux" ]; then                                                                                                                                                                 
      echo y | sudo apt-get install graphviz                                                                                                                            
elif [ `uname` = "Darwin" ]; then                                                                                                                                                              
      brew install graphviz                                                                                                                        
else                                                                                                                                                                                           
    echo " `uname` unknow sys"                                                                                                                                                                 
fi;  

echo "`date +'%Y-%m-%d %H:%M:%S'`  create work dir";
if [ ! -d ~/Pictures/xmind/  ];then
mkdir ~/Pictures/xmind/;
chmod 777 ~/Pictures/xmind/;
echo "`date +'%Y-%m-%d %H:%M:%S'` 工作目录已创建并777！";
else echo "`date +'%Y-%m-%d %H:%M:%S'` 工作目录存在！";
fi;
}

# 查看工作目录的文件
xmind_list(){
    l `echo ~`/Pictures/xmind/ | grep dot
}


# 编辑 dot 文件 并转换成 图片
xmind(){
    vim `echo ~`/Pictures/xmind/${1:-test}.dot;
    dot -T${2:-svg} -o `echo ~`/Pictures/xmind/${1:-test}.${2:-svg} `echo ~`/Pictures/xmind/${1:-test}.dot
}

# 打开 dot 文件
xmind_open(){
    open `echo ~`/Pictures/xmind/${1:-test}.${2:-svg}
}
 
# 打开文档
xmind_doc(){
    open `echo ~`/upload/ubuntu_init/file/dotguide.pdf
}

xmind_host(){
    chrome "https://graphviz.gitlab.io/"
}
