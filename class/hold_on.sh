

hold_ctb(){
echo "*/1 * * * * sh ~/upload/ubuntu_init/script/1600.frpc_hold_on.sh" | lcat && \
echo "*/1 * * * * sh ~/upload/ubuntu_init/script/1602.jupyter_hold_on.sh" | lcat && \
echo "*/1 * * * * sh ~/upload/ubuntu_init/script/1603.wssh_hold_on.sh" | lcat && \
echo "*/1 * * * * sh ~/upload/ubuntu_init/script/1604.glances_hold_on.sh" | lcat;

time_wait;
ctb;
}


hold_start(){
sh ~/upload/ubuntu_init/script/1604.glances_hold_on.sh && \
sh ~/upload/ubuntu_init/script/1600.frpc_hold_on.sh && \
sh ~/upload/ubuntu_init/script/1602.jupyter_hold_on.sh && \
sh ~/upload/ubuntu_init/script/1603.wssh_hold_on.sh
}


hold_config(){
mkdir -p ~/Logs && \
chmod -R 777 ~/Logs;

echo "`date +'%Y-%m-%d %H:%M:%S'` jupyter config" | lcat;
mkdir -p ~/Resource/Ipynb;
mkdir -p /home/neo/.jupyter;
jt_best;
jupyter_gen_config;
echo "`date +'%Y-%m-%d %H:%M:%S'` jupyter config 完成 ✅" | lcat;
echo "";
echo "";
echo "`date +'%Y-%m-%d %H:%M:%S'` webssh config" | lcat;
pip install webssh;

echo "`date +'%Y-%m-%d %H:%M:%S'`webssh config 完成 ✅" | lcat;
echo "";
echo "";
echo "`date +'%Y-%m-%d %H:%M:%S'` frpc config" | lcat;
frpc_init;
echo "";
echo "";
echo "`date +'%Y-%m-%d %H:%M:%S'` frpc config 完成 ✅" | lcat;


echo "`date +'%Y-%m-%d %H:%M:%S'` glances config" | lcat;

if [ `uname -m` = "armv7l" ] && [ `uname` = "Linux" ]; then
  echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为arm Linux" | lcat
  sudo apt-get install glances
#   typeset sysname="_linux_arm"
  # 执行代码块

elif [ `uname -m` = "aarch64" ] && [ `uname` = "Linux" ]; then
  echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为arm64 Linux" | lcat
  sudo apt-get install glances

elif [ `uname -m` = "x86_64" ] && [ `uname` = "Linux" ]; then
  echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为X86 Linux" | lcat
  pip install glances
#   typeset sysname="_linux_amd64"
  # 执行代码块
elif [ `uname -m` = "x86_64" ] && [ `uname` = "Darwin" ]; then
  echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为X86 Mac" | lcat
  pip install glances
#   typeset sysname="_darwin_amd64"
  # 执行代码块
else
  echo "`uname` 系统未知！"
fi
deploy_gotty;
echo "";
echo "";
echo "`date +'%Y-%m-%d %H:%M:%S'` glances config 完成 ✅" | lcat;
}


hold_status(){
echo "🐼🐼🐼🐼🐼🐼🐼🐼🐼 frpc 🐼🐼🐼🐼🐼🐼🐼🐼🐼" | lcat && \
opname frpc && \
echo "" && \
echo "🐼🐼🐼🐼🐼🐼🐼🐼🐼 glances 🐼🐼🐼🐼🐼🐼🐼🐼🐼"  | lcat && \
opname glances && \
echo "" && \
echo "🐼🐼🐼🐼🐼🐼🐼🐼🐼 jupyter 🐼🐼🐼🐼🐼🐼🐼🐼🐼"  | lcat && \
opname jupyter && \
echo "" && \
echo "🐼🐼🐼🐼🐼🐼🐼🐼🐼 wssh 🐼🐼🐼🐼🐼🐼🐼🐼🐼"  | lcat && \
opname wssh
}


hold_kill(){
pnamekill frpc && pnamekill frpc && pnamekill frpc && \
pnamekill glances && pnamekill glances && pnamekill glances && \
pnamekill jupyter && pnamekill jupyter && pnamekill jupyter && \
pnamekill wssh && pnamekill wssh && pnamekill wssh && \
echo "`date +'%Y-%m-%d %H:%M:%S'` kill all hold on " | lcat;
}