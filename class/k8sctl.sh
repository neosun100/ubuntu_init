# 🍎 本shell mac 下兼容

typeset k8s_master="192.168.81.236"

#  设置K8S 主机 IP，默认 loop
set_k8s_master_agent() {
    k8s_master=${1:-192.168.81.236}
}

set_k8s_master_ireland() {
    k8s_master=${1:-aws_ireland}
}

set_k8s_master_ohio() {
    k8s_master=${1:-aws_ohio}
}

set_k8s_master_virginia() {
    k8s_master=${1:-aws_virginia}
}

set_k8s_master_singapore() {
    k8s_master=${1:-aws_singapore}
}

set_k8s_master_bj() {
    k8s_master=${1:-aws_bj}
}

set_k8s_master_nx() {
    k8s_master=${1:-aws_nx}
}

#  设置K8S 主机 IP，默认 local  务必先注册  reg_k8s master_ip
set_k8s_master_local() {
    k8s_master=${1:-192.168.249.26}
}

set_k8s_master_807() {
    k8s_master=${1:-192.168.2.22}
}

# 设置 k8s 集群为 线上测试环境
set_k8s_master_loop() {
    k8s_master=${1:-192.168.255.218}
}

# 设置 k8s 集群为 pi 集群
set_k8s_master_archon() {
    k8s_master=${1:-192.168.100.124}
}

k8s_init_pkg() {
    echo "$(date +'%Y-%m-%d %H:%M:%S') 在任意linux节点下执行，修复ubuntu下的bug，并生成包分发链接"
    cd ~/upload
    rm -rf ~/upload/kube
    echo "$(date +'%Y-%m-%d %H:%M:%S') 下载指定包链接"
    wget ${1:-https://sealyun.oss-cn-beijing.aliyuncs.com/054f15a19f3c943bf387b3da25ef3de1-1.18.6/kube1.18.6.tar.gz}
    k8sversion=$(echo ${1:-https://sealyun.oss-cn-beijing.aliyuncs.com/054f15a19f3c943bf387b3da25ef3de1-1.18.6/kube1.18.6.tar.gz} | keycut "kube" ".tar.gz")
    echo "$(date +'%Y-%m-%d %H:%M:%S') 解压包"
    tar zxvf kube${k8sversion}.tar.gz
    echo "$(date +'%Y-%m-%d %H:%M:%S') fix ubuntu bug"
    sed -ie "s/setenforce 0/#setenforce 0/g" ~/upload/kube/bin/kubelet-pre-start.sh
    echo "$(date +'%Y-%m-%d %H:%M:%S') 重新打包"
    tar zcvf kube$k8sversion-neo-fix.tar.gz kube
    echo "$(date +'%Y-%m-%d %H:%M:%S') 生成http链接"
    echo "http://$(iip):12121/kube$k8sversion-neo-fix.tar.gz"
    python -m http.server 12121
}

#  返回当前k8s 的环境
k8s_env() {
    echo "当前k8s的环境ip : $k8s_master" | lcat
    if [ $(echo $k8s_master) = "192.168.81.236" ]; then
        echo "当前k8s环境为生产 代理 环境 💣" | lcat
    elif [ $(echo $k8s_master) = "192.168.249.26" ]; then
        echo "当前k8s环境为本地环境 🐳" | lcat
    elif [ $(echo $k8s_master) = "192.168.100.124" ]; then
        echo "当前k8s环境为 Home 环境 🤖️" | lcat
    elif [ $(echo $k8s_master) = "192.168.2.31" ]; then
        echo "当前k8s环境为rabook windows 环境 🔥 " | lcat
    else
        echo "环境未知！❓"
    fi
}

k8sctl_help() {
    echo "k8sctl 函数说明书"
}

k8s_add_node() {
    sudo kubeadm token create --print-join-command
}

k8s_delete_node() {
    sudo kubectl --kubeconfig=$(echo ~)/.kube/$k8s_master.config delete node ${1:-lenovo} | lcat
}

# 在 master 下 执行 获取 新 node 节点 加入 master 的 命令
k8s_add_node_help() {
    echo "在 新 node 节点上 root 下 执行如下命令" | lcat
    echo "kubeadm join 10.103.97.2:6443 --token $(k8s_add_node | field 5) --master ${1:-192.168.254.148}:6443 --master ${2:-192.168.254.202}:6443 --master ${2:-192.168.254.166}:6443 --discovery-token-ca-cert-hash $(k8s_add_node | field 7)" | lcat
}

# 设置master节点也可部署 pod
k8s_master_2_pod() {
    master_num=$(sudo kubectl --kubeconfig=$(echo ~)/.kube/$k8s_master.config get node -o wide | grep master | field 1 | line 2 | wc -l | sed 's/ //g')
    # 目标 master 数量

    if [ $master_num -eq 0 ]; then
        echo "集群 $k8s_master 不存在" | lcat
    else
        sudo kubectl --kubeconfig=$(echo ~)/.kube/$k8s_master.config taint node $(sudo kubectl --kubeconfig=$(echo ~)/.kube/$k8s_master.config get node -o wide | field 1 | line 2) node-role.kubernetes.io/master- | lcat
    fi
}

# # 注册 kubectl,只能用这个哦,mac 下有问题，手动实现了
# reg_k8s(){
# chmod7 ~/.kube;
#   if [ `uname` = "Darwin" ]; then
#     scp -r root@${1:-192.168.80.190}:/etc/kubernetes/admin.conf `echo ~`/.kube/${1:-192.168.80.190}.config; # 覆盖
#   elif [ `uname` = "Linux" ]; then
#     scp -r root@${1:-192.168.80.190}:/etc/kubernetes/admin.conf `echo ~`/.kube/${1:-192.168.80.190}.config; # 覆盖
#   else
#     echo "`uname` 系统未知！"
#   fi
# chmod7 ~/.kube;
# }

# 注册 kubectl,只能用这个哦,mac 可用，修改了ssh，sourcetree 可能会出现问题，针对公司的gitlab
reg_k8s() {
    if [ ! -d $(echo ~)/.kube ]; then
        mkdir $(echo ~)/.kube
        chmod7 $(echo ~)/.kube
        echo "$(date +'%Y-%m-%d %H:%M:%S') ~/.kube 已创建并777！"
    else
        echo "$(date +'%Y-%m-%d %H:%M:%S') ~/.kube 目录存在！"
    fi
    rm -rf ~/.kube/${1:-192.168.80.190}.config
    scp -r root@${1:-192.168.80.190}:/etc/kubernetes/admin.conf $(echo ~)/.kube/${1:-192.168.80.190}.config # 覆盖
    chmod7 $(echo ~)/.kube
    sed -ie "s/apiserver.cluster.local/${1:-192.168.80.190}/g" ~/.kube/${1:-192.168.80.190}.config
}

#  查看已注册的K8S集群 ip
ls_k8s() {
    l $(echo ~)/.kube | field 9 | line 4 100 | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}"
}

# # 编辑k8s 配置文件
# vim_k8s_config(){
# vim ~/.kube/${1:-192.168.80.190}.config;
# }

# 默认的kubectl 的配置文件是 ~/.kube/config
# 如果要自定义，每次需要指定 --kubeconfig=/path/to/kubeconfig

# 针对loop k8s 集群的控制,只能用绝对路径，奇怪
k8sctl() {
    sudo kubectl --kubeconfig=$(echo ~)/.kube/$k8s_master.config $* | lcat
}
jk8sctl() {
    sudo kubectl --kubeconfig=/$(echo ~)/.kube/$k8s_master.config $* -o json | jcat
}

k8sctl_version() {
    sudo kubectl --kubeconfig=$(echo ~)/.kube/$k8s_master.config version -o json | jq '.serverVersion.gitVersion' | sed 's/"//g' | lcat
}

# 针对loop k8s 集群的控制,只能用绝对路径，奇怪
# pod
# 一个参数 默认 default 命名空间
k8sctl_pod() {
    sudo kubectl --kubeconfig=$(echo ~)/.kube/$k8s_master.config get pod -o wide --namespace ${1:-default} | lcat
}

# deploy
# 一个参数 默认 default 命名空间
k8sctl_deploy() {
    sudo kubectl --kubeconfig=$(echo ~)/.kube/$k8s_master.config get deploy -o wide --namespace ${1:-default} | lcat
}

# 查看所有节点的labels
k8sctl_labels() {
    sudo kubectl --kubeconfig=$(echo ~)/.kube/$k8s_master.config get node --show-labels | lcat
}

# k8sctl get node  -l "airflow=master"
k8sctl_select_label() {
    sudo kubectl --kubeconfig=$(echo ~)/.kube/$k8s_master.config get node --show-labels -l ${1:-small} | fields '$1,$3,$6' | lcat
}

# daemonset
# 一个参数 默认 default 命名空间 每个节点一个，HA
k8sctl_daemonset() {
    sudo kubectl --kubeconfig=$(echo ~)/.kube/$k8s_master.config get daemonset -o wide --namespace ${1:-default} | lcat
}

# Kubernetes-持久化存储卷PersistentVolume
k8sctl_pv() {
    sudo kubectl --kubeconfig=$(echo ~)/.kube/$k8s_master.config get pv -o wide --namespace ${1:-default} | lcat
}

# Kubernetes-配置PersistentVolumeClaim 绑定pv 使用pv的限制相当于 硬盘上的软件空间要求
k8sctl_pvc() {
    sudo kubectl --kubeconfig=$(echo ~)/.kube/$k8s_master.config get pvc -o wide --namespace ${1:-default} | lcat
}

# configmap
k8sctl_configmap() {
    sudo kubectl --kubeconfig=$(echo ~)/.kube/$k8s_master.config get configmap -o wide --namespace ${1:-default} | lcat
}

# svc
k8sctl_svc() {
    sudo kubectl --kubeconfig=$(echo ~)/.kube/$k8s_master.config get svc -o wide --namespace ${1:-default} | lcat
}

# 查看 命名空间
k8sctl_namespaces() {
    sudo kubectl --kubeconfig=$(echo ~)/.kube/$k8s_master.config get namespaces -o wide | lcat
}

# 查看 node
k8sctl_node() {
    sudo kubectl --kubeconfig=$(echo ~)/.kube/$k8s_master.config get node -o wide | lcat
}

k8sctl_cluster() {
    sudo kubectl --kubeconfig=$(echo ~)/.kube/$k8s_master.config cluster-info
}

# K8S 集群 pod 资源占用
k8sctl_top_node() {
    sudo kubectl --kubeconfig=$(echo ~)/.kube/$k8s_master.config top node | lcat
}

k8sctl_top_pod() {
    sudo kubectl --kubeconfig=$(echo ~)/.kube/$k8s_master.config top pod | lcat
}

k8sctl_all() {
    echo "🐼 k8sctl_version"
    k8sctl_version
    echo ""

    echo "🐼 k8sctl_cluster"
    k8sctl_cluster
    echo ""

    echo "🐼 k8sctl_namespaces"
    k8sctl_namespaces
    echo ""

    echo "🐼 k8sctl_labels"
    k8sctl_labels
    echo ""

    echo "🐼 k8sctl_node"
    k8sctl_node
    echo ""

    echo "🐼 k8sctl_pod"
    k8sctl_pod ${1:-default}
    echo ""

    echo "🐼 k8sctl_deploy"
    k8sctl_deploy ${1:-default}
    echo ""

    echo "🐼 k8sctl_daemonset"
    k8sctl_daemonset ${1:-default}
    echo ""

    echo "🐼 k8sctl_svc"
    k8sctl_svc ${1:-default}
    echo ""

    echo "🐼 k8sctl_pv"
    k8sctl_pv ${1:-default}
    echo ""

    echo "🐼 k8sctl_pvc"
    k8sctl_pvc ${1:-default}
    echo ""

    echo "🐼 k8sctl_configmap"
    k8sctl_configmap ${1:-default}
    echo ""

    echo "🐼 k8sctl_top_node"
    k8sctl_top_node
    echo ""

    echo "🐼 k8sctl_top_pod"
    k8sctl_top_pod
    echo ""
}

# 设置 让master 节点 也可以 执行 pod 默认 neo-tank节点， 末尾的-，代表去除
# 参数为 master name
# 一次所有master2work
# 污点， 主节点也可部署服务
k8sctl_master_work() {
    # sudo kubectl --kubeconfig=`echo ~`/.kube/$k8s_master.config taint  nodes ${1:-neo-tank} node-role.kubernetes.io/master:NoSchedule-
    sudo kubectl --kubeconfig=$(echo ~)/.kube/$k8s_master.config taint nodes --all node-role.kubernetes.io/master-
}

# 按文件部署资源
# 参数为yaml文件和环境   -n test
k8sctl_create() {
    sudo kubectl --kubeconfig=$(echo ~)/.kube/$k8s_master.config create -f $* | lcat
}

# 优先用这个命令，保存执行的文件配置
k8sctl_create_save() {
    sudo kubectl --kubeconfig=$(echo ~)/.kube/$k8s_master.config create --save-config -f $* | lcat
}

# 增加配置文件
# kubectl create configmap mysql-config --from-file=mysqld.cnf
k8sctl_create_configmap() {
    sudo kubectl --kubeconfig=$(echo ~)/.kube/$k8s_master.config create configmap $1 --from-file=$2 | lcat
}

# 已经集成在了 k8s_init里
# k8sctl_dashboard(){
#     # sudo kubectl --kubeconfig=`echo ~`/.kube/$k8s_master.config apply -f `echo ~`/upload/ubuntu_init/k8s_run_dir/dashboard/kubernetes-dashboard.yaml | lcat;
#     # sudo kubectl --kubeconfig=`echo ~`/.kube/$k8s_master.config apply -f `echo ~`/upload/ubuntu_init/k8s_run_dir/dashboard/dashboard-admin.yaml | lcat;
#     chrome "https://github.com/kubernetes/dashboard/releases";
#     sleep 10;
#     sudo kubectl --kubeconfig=`echo ~`/.kube/$k8s_master.config delete ns kubernetes-dashboard | lcat;
#     sudo kubectl --kubeconfig=`echo ~`/.kube/$k8s_master.config apply -f  https://raw.githubusercontent.com/kubernetes/dashboard/v2.0.0-beta5/aio/deploy/recommended.yaml;
#     echo "https://$k8s_master:32000/#\!/overview?namespace=default"  | lcat;
#     chrome "https://$k8s_master:32000/#\!/overview?namespace=default";

# }

#  按文件删除资源
# 参数为yaml文件和环境   -n test
k8sctl_delete() {
    sudo kubectl --kubeconfig=$(echo ~)/.kube/$k8s_master.config delete -f $* | lcat
}

# 按资源类型和ID删除资源
k8sctl_delete_pod() {
    sudo kubectl --kubeconfig=$(echo ~)/.kube/$k8s_master.config delete pod $* | lcat
}

k8sctl_delete_pod_by_name() {
    # DELETE_ID_LIST=$(sudo kubectl --kubeconfig=$(echo ~)/.kube/$k8s_master.config get pod -o wide --namespace ${2:-default} | field 1 | line 2 100 | grep ${1:-scala} | rs )
    k8sctl_delete_pod --namespace ${2:-default} `sudo kubectl --kubeconfig=$(echo ~)/.kube/$k8s_master.config get pod -o wide --namespace ${2:-default} | field 1 | line 2 100 |  awk '{ gsub(/\n/," "); print $0 }' | grep ${1:-scala}` 
}

k8sctl_delete_rc() {
    sudo kubectl --kubeconfig=$(echo ~)/.kube/$k8s_master.config delete rc $* | lcat
}

k8sctl_delete_deploy() {
    sudo kubectl --kubeconfig=$(echo ~)/.kube/$k8s_master.config delete deploy $* | lcat
}

k8sctl_delete_daemonset() {
    sudo kubectl --kubeconfig=$(echo ~)/.kube/$k8s_master.config delete daemonset $* | lcat
}

k8sctl_delete_svc() {
    sudo kubectl --kubeconfig=$(echo ~)/.kube/$k8s_master.config delete svc $* | lcat
}

k8sctl_delete_configmap() {
    sudo kubectl --kubeconfig=$(echo ~)/.kube/$k8s_master.config delete configmap $* | lcat
}

k8sctl_delete_pv() {
    sudo kubectl --kubeconfig=$(echo ~)/.kube/$k8s_master.config delete pv $* | lcat
}

k8sctl_delete_pvc() {
    sudo kubectl --kubeconfig=$(echo ~)/.kube/$k8s_master.config delete pvc $* | lcat
}

# 调成资源
k8sctl_scale() {
    sudo kubectl --kubeconfig=$(echo ~)/.kube/$k8s_master.config scale $* | lcat
}

# 调整deployment资源数
k8sctl_adjust_dp() {
    sudo kubectl --kubeconfig=$(echo ~)/.kube/$k8s_master.config scale deployment $1 --replicas $2 -n ${3:-default} | lcat
}

# 调成动态资源
k8sctl_autoscale() {
    sudo kubectl --kubeconfig=$(echo ~)/.kube/$k8s_master.config autoscale $* | lcat
}

#  打标签 <node_name> loop=small
k8sctl_label() {
    sudo kubectl --kubeconfig=$(echo ~)/.kube/$k8s_master.config label node $* | lcat
}

# 查询信息 k8sctl describe node stalker
k8sctl_describe() {
    sudo kubectl --kubeconfig=$(echo ~)/.kube/$k8s_master.config describe $* | lcat
}

k8sctl_describe_node() {
    sudo kubectl --kubeconfig=$(echo ~)/.kube/$k8s_master.config describe node $* | lcat
}

k8sctl_describe_pod() {
    sudo kubectl --kubeconfig=$(echo ~)/.kube/$k8s_master.config describe pod $* | lcat
}

k8sctl_describe_svc() {
    sudo kubectl --kubeconfig=$(echo ~)/.kube/$k8s_master.config describe svc $* | lcat
}

k8sctl_describe_deploy() {
    sudo kubectl --kubeconfig=$(echo ~)/.kube/$k8s_master.config describe deploy $* | lcat
}

k8sctl_describe_daemonset() {
    sudo kubectl --kubeconfig=$(echo ~)/.kube/$k8s_master.config describe daemonset $* | lcat
}

k8sctl_describe_configmap() {
    sudo kubectl --kubeconfig=$(echo ~)/.kube/$k8s_master.config describe configmap $* | lcat
}

k8sctl_describe_pv() {
    sudo kubectl --kubeconfig=$(echo ~)/.kube/$k8s_master.config describe pv $* | lcat
}

k8sctl_describe_pvc() {
    sudo kubectl --kubeconfig=$(echo ~)/.kube/$k8s_master.config describe pvc $* | lcat
}

# 滚动更新资源
k8sctl_apply() {
    sudo kubectl --kubeconfig=$(echo ~)/.kube/$k8s_master.config apply --record -f $* | lcat
}

#  查看资源滚动更新历史
# kubectl rollout history deployment httpd
k8sctl_history() {
    sudo kubectl --kubeconfig=$(echo ~)/.kube/$k8s_master.config rollout history $* | lcat
}

#  查看资源滚动更新历史
k8sctl_history_pod() {
    sudo kubectl --kubeconfig=$(echo ~)/.kube/$k8s_master.config rollout history pod $* | lcat
}

#  查看资源滚动更新历史
k8sctl_history_daemonset() {
    sudo kubectl --kubeconfig=$(echo ~)/.kube/$k8s_master.config rollout history daemonset $* | lcat
}

k8sctl_history_deploy() {
    sudo kubectl --kubeconfig=$(echo ~)/.kube/$k8s_master.config rollout history deploy $* | lcat
}

# 回滚  kubectl rollout undo deployment httpd --to-revision=1
k8sctl_back_select() {
    sudo kubectl --kubeconfig=$(echo ~)/.kube/$k8s_master.config rollout undo $1 $2 --to-revision=$3 | lcat
}

k8sctl_back_pod_select() {
    sudo kubectl --kubeconfig=$(echo ~)/.kube/$k8s_master.config rollout undo pod $1 --to-revision=$2 | lcat
}

k8sctl_back_daemonset_select() {
    sudo kubectl --kubeconfig=$(echo ~)/.kube/$k8s_master.config rollout undo daemonset $1 --to-revision=$2 | lcat
}

k8sctl_back_deploy_select() {
    sudo kubectl --kubeconfig=$(echo ~)/.kube/$k8s_master.config rollout undo deploy $1 --to-revision=$2 | lcat
}

# 快速回归到上一个
k8sctl_back_fast() {
    sudo kubectl --kubeconfig=$(echo ~)/.kube/$k8s_master.config rollout undo $* | lcat
}

k8sctl_back_pod_fast() {
    sudo kubectl --kubeconfig=$(echo ~)/.kube/$k8s_master.config rollout undo pod $1 | lcat
}

k8sctl_back_daemonset_fast() {
    sudo kubectl --kubeconfig=$(echo ~)/.kube/$k8s_master.config rollout undo daemonset $1 | lcat
}

k8sctl_back_deploy_fast() {
    sudo kubectl --kubeconfig=$(echo ~)/.kube/$k8s_master.config rollout undo deploy $1 | lcat
}

# 需要重启的app
k8sctl_restart() {
    echo "🍥 🍥 🍥 🍥 🍥 🍥 🍥 🍥 🍥 🍥 🍥 🍥 🍥 🍥 🍥 🍥"
    echo "$(date +'%Y-%m-%d %H:%M:%S') k8s app restarting" | lcat
    k8sctl_delete $1
    sleep 49
    echo ""
    echo ""
    echo "🍥 🍥 🍥 🍥 🍥 🍥 🍥 🍥 🍥 🍥 🍥 🍥 🍥 🍥 🍥 🍥"
    k8sctl_create $1
    echo "🍣 🍣 🍣 🍣 🍣 🍣 🍣 🍣 🍣 🍣 🍣 🍣 🍣 🍣 🍣 🍣"
    echo "$(date +'%Y-%m-%d %H:%M:%S') k8s app restart finish" | lcat
}

# 安装完k8s,mac设置好
k8s_init() {
    echo "🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 "
    echo "请在 sealos 执行完成后执行,否则请立即 ctrl + c "
    sleep 10
    echo ""
    echo ""
    echo "🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 "
    echo "master 污点化"
    k8sctl_master_work
    echo ""
    echo ""
    echo "🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 "
    echo "创建全功能的 kubernetes-dashboard 账户"
    K8Sminor=$(sudo kubectl --kubeconfig=/Users/neo/.kube/$k8s_master.config version -o json | jq '.serverVersion.minor')

    if [ $K8Sminor = '"15"' ]; then
        k8sctl_apply /Users/neo/upload/ubuntu_init/k8s_run_dir/dashboard/kubernetes-dashboard.1.15.2.yaml
    elif [ $K8Sminor = '"16"' ]; then
        k8sctl_apply /Users/neo/upload/ubuntu_init/k8s_run_dir/dashboard/kubernetes-dashboard.1.16.2.yaml
    elif [ $K8Sminor = '"19"' ]; then
        k8sctl_apply /Users/neo/upload/ubuntu_init/k8s_run_dir/dashboard/kubernetes-dashboard.1.19.6.yaml

    else
        echo "kubernetes server 版本为 $K8Sminor 需要添加新的 kubernetes-dashboard.1.$K8Sminor.x.yaml"
    fi # 判断结束，以fi结尾

    echo ""
    echo ""
    echo "🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 "
    echo "查看所有 token"
    k8sctl get secret -n=kube-system

    echo ""
    echo ""
    echo "🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 "
    echo "创建一个dashboard管理用户"
    k8sctl create serviceaccount dashboard-admin -n kube-system

    echo ""
    echo ""
    echo "🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 "
    echo "绑定用户为集群管理用户"
    k8sctl create clusterrolebinding dashboard-cluster-admin --clusterrole=cluster-admin --serviceaccount=kube-system:dashboard-admin

    echo ""
    echo ""
    echo "🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 "
    echo "查看token name"
    # k8sctl get secret -n=kube-system | field | grep "token" | grep "dashboard" | grep "kubernetes";
    k8sctl get secret -n=kube-system | field | grep "dashboard" | grep "admin"

    DASHBOARDtoken=$(sudo kubectl --kubeconfig=/Users/neo/.kube/$k8s_master.config get secret -n=kube-system | field | grep "dashboard" | grep "admin")
    # 警告 ⚠️ 参数不可高亮在用，要用原始的输出
    echo ""
    echo ""
    echo "🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 "
    echo "获取token"
    k8sctl describe secret -n=kube-system $DASHBOARDtoken

    echo "https://$k8s_master:31001/#\!/overview?namespace=default" | lcat
    chrome "https://$k8s_master:31001/#\!/overview?namespace=default"
}

k8s_token() {
    DASHBOARDtoken=$(sudo kubectl --kubeconfig=/Users/neo/.kube/$k8s_master.config get secret -n=kube-system | field | grep "dashboard" | grep "admin")
    # 警告 ⚠️ 参数不可高亮在用，要用原始的输出
    echo ""
    echo ""
    echo "🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 🦁 "
    echo "获取token"
    k8sctl describe secret -n=kube-system $DASHBOARDtoken
}

k8s_clean_pod() {
    name=${1:-default}
    sudo kubectl -n $name --kubeconfig=$(echo ~)/.kube/$k8s_master.config get pods | grep Evicted | awk '{print $1}' | xargs sudo kubectl -n $name --kubeconfig=$(echo ~)/.kube/$k8s_master.config delete pod | lcat
}

install_velero_latest() {
    echo "备份和迁移 Kubernetes 应用程序及其持久卷 工具 velero"
    download_fast_github_best_file_url 'https://github.com/vmware-tanzu/velero/releases' '/tmp/velero.tar.gz'
    cd /tmp
    tar -zxvf /tmp/velero.tar.gz
    sudo mv /tmp/velero-v*-amd64/velero /usr/local/bin
    scat /tmp/velero-v*-amd64/examples/README.md
    velero help
    cd ~/upload
}

install_helm_latest() {
    cd ~/upload
    echo "支持MacOS Linux amd64 arm"

    if [ $(uname -m) = "armv7l" ] && [ $(uname) = "Linux" ]; then
        echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为arm Linux" | lcat
        URL=$(curl -s https://github.com/helm/helm/releases | htmlq a --attribute href | grep 'helm' | grep 'linux' | grep 'arm' | head -n 1)
        curl --silent --location $URL | tar xz -C /tmp
        mv /tmp/linux-arm/helm /usr/local/bin

    elif [ $(uname -m) = "x86_64" ] && [ $(uname) = "Linux" ]; then
        echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为X86 Linux" | lcat
        URL=$(curl -s https://github.com/helm/helm/releases | htmlq a --attribute href | grep 'helm' | grep 'linux' | grep 'amd64' | head -n 1)
        curl --silent --location $URL | tar xz -C /tmp
        mv /tmp/linux-amd64/helm /usr/local/bin

    elif [ $(uname -m) = "x86_64" ] && [ $(uname) = "Darwin" ]; then
        echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为X86 Mac" | lcat
        URL=$(curl -s https://github.com/helm/helm/releases | htmlq a --attribute href | grep 'helm' | grep 'darwin' | grep 'amd64' | head -n 1)
        curl --silent --location $URL | tar xz -C /tmp
        mv /tmp/darwin-amd64/helm /usr/local/bin
    elif [ $(uname -m) = "x86_64" ] && [ $(uname) = "Darwin" ]; then
        echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为X86 Mac" | lcat
        URL=$(curl -s https://github.com/helm/helm/releases | htmlq a --attribute href | grep 'helm' | grep 'darwin' | grep 'arm64' | head -n 1)
        curl --silent --location $URL | tar xz -C /tmp
        mv /tmp/darwin-arm64/helm /usr/local/bin
    else
        echo "$(uname) 系统未知！"
    fi

    helm help
    helm version
}
