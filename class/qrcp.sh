# https://github.com/claudiodangelis/qrcp/releases


install_qrcp(){
echo "下载地址"
echo "https://github.com/claudiodangelis/qrcp/releases"


# 标记 下载url
if [ `uname -m` = "armv7l" ] && [ `uname` = "Linux" ]; then
  echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为arm Linux" | lcat
  typeset url="http://file.neo.pub/linux/arm/qrcp"
#   typeset sysname="_linux_arm"
  # 执行代码块

elif [ `uname -m` = "aarch64" ] && [ `uname` = "Linux" ]; then
  echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为arm64 Linux" | lcat
  typeset url="http://file.neo.pub/linux/arm64/qrcp"

elif [ `uname -m` = "x86_64" ] && [ `uname` = "Linux" ]; then
  echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为X86 Linux" | lcat
  typeset url="http://file.neo.pub/linux/x86/qrcp"
#   typeset sysname="_linux_amd64"
  # 执行代码块
elif [ `uname -m` = "x86_64" ] && [ `uname` = "Darwin" ]; then
  echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为X86 Mac" | lcat
  typeset url="http://file.neo.pub/mac/qrcp"
#   typeset sysname="_darwin_amd64"
  # 执行代码块
else
  echo "`uname` 系统未知！"
  typeset url="http://file.neo.pub/win/qrcp"
fi



echo "`date +'%Y-%m-%d %H:%M:%S'` 判断是否已经存在qrcp"

mkdir -p ~/upload/bin;
if [ -x ~/upload/bin/qrcp ] ; then 
echo "`date +'%Y-%m-%d %H:%M:%S'` qrcp 已存在"
else
echo "`date +'%Y-%m-%d %H:%M:%S'` qrcp 不存在" 

cd ~/upload/bin && wget $url 
sudo chmod -R 777 ~/upload/bin
~/upload/bin/qrcp --help
fi # 判断结束，以fi结尾
}


qr_config(){
echo "配置默认设置"
~/upload/bin/qrcp config
}


# qr send
# 可以是多文件，可以是目录
qr_s(){
~/upload/bin/qrcp --port 8199 $*
}

# qr receive
qr_r(){
~/upload/bin/qrcp --port 8199 receive
}