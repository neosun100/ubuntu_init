install_github_file_latest_version() {
    if [ $(uname -m) = "armv7l" ] && [ $(uname) = "Linux" ]; then
        echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为arm Linux" | lcat
        SYS_NAME="linux"
        ARM_OR_AMD="arm"

    elif [ $(uname -m) = "x86_64" ] && [ $(uname) = "Linux" ]; then
        echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为X86 Linux" | lcat
        SYS_NAME="linux"
        ARM_OR_AMD="amd64"

    elif [ $(uname -m) = "x86_64" ] && [ $(uname) = "Darwin" ]; then
        echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为X86 Mac" | lcat
        SYS_NAME="darwin"
        ARM_OR_AMD="amd64"

    else
        echo "$(uname) 系统未知！"
        time_wait
    fi
    echo "$(date +'%Y-%m-%d %H:%M:%S') 当前环境系统名称:\n$SYS_NAME\n"

    echo "$(date +'%Y-%m-%d %H:%M:%S') 当前环境架构名称:\n$ARM_OR_AMD\n"

    RELEASES_URL=${1:-"https://github.com/kubernetes/kompose/releases/"}
    echo "$(date +'%Y-%m-%d %H:%M:%S') RELEASES_URL:\n$RELEASES_URL\n"

    PROJECT_NAME=$(echo $RELEASES_URL | awk '{ gsub(/\//,"\t"); print $0 }' | field 4)
    echo "$(date +'%Y-%m-%d %H:%M:%S') PROJECT_NAME:\n$PROJECT_NAME\n"

    LATEST_VERSION=$(curl -s $RELEASES_URL | htmlq a --attribute href | grep $PROJECT_NAME | grep "github" | grep "amd64\|arm\|linux\|windows\|darwin" | head -n 1 | cutnum | line 2 2)
    echo "$(date +'%Y-%m-%d %H:%M:%S') LATEST_VERSION:\n$LATEST_VERSION\n"

    MATCH_PAKAGES=$(curl -s $RELEASES_URL | htmlq a --attribute href | grep $PROJECT_NAME | grep "github" | grep "$LATEST_VERSION" | grep "$SYS_NAME" | grep "$ARM_OR_AMD")
    echo "$(date +'%Y-%m-%d %H:%M:%S') MATCH_PAKAGES:\n$MATCH_PAKAGES\n"

    UNTAR_PAKAGES=$(curl -s $RELEASES_URL | htmlq a --attribute href | grep $PROJECT_NAME | grep "github" | grep "$LATEST_VERSION" | grep "$SYS_NAME" | grep "$ARM_OR_AMD" | grep -v 'tar\|gz')
    echo "$(date +'%Y-%m-%d %H:%M:%S') 二进制文件:\n$UNTAR_PAKAGES\n"

    FAST_FIRST_UNTAR_PAKAGE=$(echo https://github.91chifun.workers.dev/$(curl -s $RELEASES_URL | htmlq a --attribute href | grep $PROJECT_NAME | grep "github" | grep "$LATEST_VERSION" | grep "$SYS_NAME" | grep "$ARM_OR_AMD" | grep -v 'tar\|gz' | head -n 1))
    echo "$(date +'%Y-%m-%d %H:%M:%S') 第一个fast二进制文件url:\n$FAST_FIRST_UNTAR_PAKAGE\n"

    TAR_PAKAGES=$(curl -s $RELEASES_URL | htmlq a --attribute href | grep $PROJECT_NAME | grep "github" | grep "$LATEST_VERSION" | grep "$SYS_NAME" | grep "$ARM_OR_AMD" | grep 'tar\|gz')
    echo "$(date +'%Y-%m-%d %H:%M:%S') tar包:\n$TAR_PAKAGES\n"

    FAST_FIRST_TAR_PAKAGE=$(echo https://github.91chifun.workers.dev/$(curl -s $RELEASES_URL | htmlq a --attribute href | grep $PROJECT_NAME | grep "github" | grep "$LATEST_VERSION" | grep "$SYS_NAME" | grep "$ARM_OR_AMD" | grep 'tar\|gz' | head -n 1))
    echo "$(date +'%Y-%m-%d %H:%M:%S') 第一个fast非二进制文件url:\n$FAST_FIRST_TAR_PAKAGE\n"

    # curl -s $RELEASES_URL | htmlq a --attribute href | grep $PROJECT_NAME | grep "github" | grep "$LATEST_VERSION" | grep "$SYS_NAME" | grep "$ARM_OR_AMD"

    if [ -n $UNTAR_PAKAGES ]; then
        echo "$(date +'%Y-%m-%d %H:%M:%S') 有二进制文件优先下载二进制:\n$UNTAR_PAKAGES\n$FAST_FIRST_UNTAR_PAKAGE\n"

        curl --silent --location $FAST_FIRST_UNTAR_PAKAGE -o /tmp/$PROJECT_NAME
        chmod +x /tmp/$PROJECT_NAME
        sudo mv /tmp/$PROJECT_NAME /usr/local/bin

        echo "$(date +'%Y-%m-%d %H:%M:%S') 验证 $PROJECT_NAME 版本与功能"
        eval $PROJECT_NAME
        eval $PROJECT_NAME version

        echo "$(date +'%Y-%m-%d %H:%M:%S') install $PROJECT_NAME success\n"

    elif

        [ -n $TAR_PAKAGES ]
    then
        echo "$(date +'%Y-%m-%d %H:%M:%S') 仅有tar包:\n$UNTAR_PAKAGES\n$FAST_FIRST_TAR_PAKAGE\n"
        curl --silent --location $FAST_FIRST_TAR_PAKAGE | tar xz -C /tmp
        chmod +x /tmp/$PROJECT_NAME
        sudo mv /tmp/$PROJECT_NAME /usr/local/bin

        echo "$(date +'%Y-%m-%d %H:%M:%S') 验证 $PROJECT_NAME 版本与功能"
        eval $PROJECT_NAME
        eval $PROJECT_NAME version

        echo "$(date +'%Y-%m-%d %H:%M:%S') install $PROJECT_NAME success\n"

    else

        echo "$(date +'%Y-%m-%d %H:%M:%S') 未找到任何包⚠️\n检查 $RELEASES_URL\n"

    fi

}

# curl --silent --location "https://github.91chifun.workers.dev/https://github.com/weaveworks/eksctl/releases/latest/download/eksctl_$(uname -s)_amd64.tar.gz" | tar xz -C /tmp
# sudo mv /tmp/eksctl /usr/local/bin
# eksctl help
# eksctl version
# echo "$(date +'%Y-%m-%d %H:%M:%S') setup or resetup latest eksctl success"

install_kompose_latest() {
    install_github_file_latest_version https://github.com/kubernetes/kompose/releases/
}

n_install_github_file_latest_version() {
    FAST_HOST="https://github.91chi.fun"

    if [ $(uname -m) = "armv7l" ] && [ $(uname) = "Linux" ]; then
        echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为arm Linux" | lcat
        SYS_NAME="linux"
        ARM_OR_AMD="arm"

    elif [ $(uname -m) = "x86_64" ] && [ $(uname) = "Linux" ]; then
        echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为X86 Linux" | lcat
        SYS_NAME="linux"
        ARM_OR_AMD="amd64"

    elif [ $(uname -m) = "x86_64" ] && [ $(uname) = "Darwin" ]; then
        echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为X86 Mac" | lcat
        SYS_NAME="darwin"
        ARM_OR_AMD="amd64"

    else
        echo "$(uname) 系统未知！"
        time_wait
    fi
    echo "$(date +'%Y-%m-%d %H:%M:%S') 当前环境系统名称:\n$SYS_NAME\n"

    echo "$(date +'%Y-%m-%d %H:%M:%S') 当前环境架构名称:\n$ARM_OR_AMD\n"

    RELEASES_URL=${1:-"https://github.com/iikira/tinypng/releases"}
    echo "$(date +'%Y-%m-%d %H:%M:%S') RELEASES_URL:\n$RELEASES_URL\n"

    PROJECT_NAME=$(echo $RELEASES_URL | awk '{ gsub(/\//,"\t"); print $0 }' | field 4)
    echo "$(date +'%Y-%m-%d %H:%M:%S') PROJECT_NAME:\n$PROJECT_NAME\n"

    LATEST_VERSION=$(curl -s $RELEASES_URL | htmlq a --attribute href | grep $PROJECT_NAME | grep "amd64\|arm\|linux\|windows\|darwin" | head -n 1 | cutnum | line 2 2)
    echo "$(date +'%Y-%m-%d %H:%M:%S') LATEST_VERSION:\n$LATEST_VERSION\n"

    MATCH_PAKAGES=$(curl -s $RELEASES_URL | htmlq a --attribute href | grep $PROJECT_NAME | grep "$LATEST_VERSION" | grep "$SYS_NAME" | grep "$ARM_OR_AMD")
    MATCH_PAKAGES=https://github.com$MATCH_PAKAGES
    echo "$(date +'%Y-%m-%d %H:%M:%S') MATCH_PAKAGES:\n$MATCH_PAKAGES\n"

    UNTAR_PAKAGES=$(curl -s $RELEASES_URL | htmlq a --attribute href | grep $PROJECT_NAME | grep "github" | grep "$LATEST_VERSION" | grep "$SYS_NAME" | grep "$ARM_OR_AMD" | grep -v 'tar\|gz\|zip')
    [[ -n $UNTAR_PAKAGES ]] && UNTAR_PAKAGES=https://github.com$UNTAR_PAKAGES || echo "未抓取到非压缩包"
    echo "$(date +'%Y-%m-%d %H:%M:%S') 二进制文件:\n$UNTAR_PAKAGES\n"

    FAST_FIRST_UNTAR_PAKAGE=$(echo $FAST_HOST//https://github.com$(curl -s $RELEASES_URL | htmlq a --attribute href | grep $PROJECT_NAME | grep "$LATEST_VERSION" | grep "$SYS_NAME" | grep "$ARM_OR_AMD" | grep -v 'tar\|gz\|zip' | head -n 1))
    [[ $FAST_FIRST_UNTAR_PAKAGE == "$FAST_HOST//https://github.com" ]] && echo "未抓取到非压缩包" || echo "$(date +'%Y-%m-%d %H:%M:%S') 第一个fast二进制文件url:\n$FAST_FIRST_UNTAR_PAKAGE\n"

    TAR_PAKAGES=$(curl -s $RELEASES_URL | htmlq a --attribute href | grep $PROJECT_NAME | grep "$LATEST_VERSION" | grep "$SYS_NAME" | grep "$ARM_OR_AMD" | grep 'tar\|gz\|zip')
    [[ -n $TAR_PAKAGES ]] && TAR_PAKAGES=https://github.com$TAR_PAKAGES || echo "未抓取到压缩包"
    echo "$(date +'%Y-%m-%d %H:%M:%S') tar包:\n$TAR_PAKAGES\n"

    FAST_FIRST_TAR_PAKAGE=$(echo $FAST_HOST//https://github.com$(curl -s $RELEASES_URL | htmlq a --attribute href | grep $PROJECT_NAME | grep "$LATEST_VERSION" | grep "$SYS_NAME" | grep "$ARM_OR_AMD" | grep 'tar\|gz\|zip' | head -n 1))

    [[ $FAST_FIRST_TAR_PAKAGE == "$FAST_HOST//https://github.com" ]] && echo "未抓取到压缩包" || echo "$(date +'%Y-%m-%d %H:%M:%S') 第一个非二进制文件url:\n$FAST_FIRST_TAR_PAKAGE\n"

    # curl -s $RELEASES_URL | htmlq a --attribute href | grep $PROJECT_NAME | grep "github" | grep "$LATEST_VERSION" | grep "$SYS_NAME" | grep "$ARM_OR_AMD"

    if [ $(echo $UNTAR_PAKAGES | grep "$PROJECT_NAME" | wc -l) -eq 1 ]; then
        echo "$(date +'%Y-%m-%d %H:%M:%S') 有二进制文件优先下载二进制:\n$UNTAR_PAKAGES\n$FAST_FIRST_UNTAR_PAKAGE\n"

        curl --silent --location $FAST_FIRST_UNTAR_PAKAGE -o /tmp/$PROJECT_NAME
        chmod +x /tmp/$PROJECT_NAME
        sudo mv /tmp/$PROJECT_NAME /usr/local/bin

        echo "$(date +'%Y-%m-%d %H:%M:%S') 验证 $PROJECT_NAME 版本与功能"
        eval $PROJECT_NAME
        eval $PROJECT_NAME version

        echo "$(date +'%Y-%m-%d %H:%M:%S') install $PROJECT_NAME success\n"

    elif

        [ $(echo $TAR_PAKAGES | grep "$PROJECT_NAME" | wc -l) -eq 1 ] && [ $(echo "$TAR_PAKAGES" | grep "gz" | wc -l) -eq 1 ]
    then
        echo "$(date +'%Y-%m-%d %H:%M:%S') 仅有tar包:\n$UNTAR_PAKAGES\n$FAST_FIRST_TAR_PAKAGE\n"
        curl --silent --location $FAST_FIRST_TAR_PAKAGE | tar xz -C /tmp
        chmod +x /tmp/$PROJECT_NAME
        sudo mv /tmp/$PROJECT_NAME /usr/local/bin

        echo "$(date +'%Y-%m-%d %H:%M:%S') 验证 $PROJECT_NAME 版本与功能"
        eval $PROJECT_NAME
        eval $PROJECT_NAME version

        echo "$(date +'%Y-%m-%d %H:%M:%S') install $PROJECT_NAME success\n"
    elif
        [ $(echo $TAR_PAKAGES | grep "$PROJECT_NAME" | wc -l) -eq 1 ] && [ $(echo "$TAR_PAKAGES" | grep "zip" | wc -l) -eq 1 ]
    then

        echo "$(date +'%Y-%m-%d %H:%M:%S') 仅有zip包:\n$UNTAR_PAKAGES\n$FAST_FIRST_TAR_PAKAGE\n"
        cd /tmp && curl --silent --location $FAST_FIRST_TAR_PAKAGE | unzip
        chmod +x /tmp/$PROJECT_NAME
        sudo mv /tmp/$PROJECT_NAME /usr/local/bin

        echo "$(date +'%Y-%m-%d %H:%M:%S') 验证 $PROJECT_NAME 版本与功能"
        eval $PROJECT_NAME
        eval $PROJECT_NAME version

        echo "$(date +'%Y-%m-%d %H:%M:%S') install $PROJECT_NAME success\n"

    else

        echo "$(date +'%Y-%m-%d %H:%M:%S') 未找到任何包⚠️\n检查 $RELEASES_URL\n"

    fi

}

n2_install_github_file_latest_version() {
    FAST_HOST="https://github.91chi.fun"

    if [ $(uname -m) = "armv7l" ] && [ $(uname) = "Linux" ]; then
        echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为arm Linux" | lcat
        SYS_NAME="linux"
        ARM_OR_AMD="arm"

    elif [ $(uname -m) = "x86_64" ] && [ $(uname) = "Linux" ]; then
        echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为X86 Linux" | lcat
        SYS_NAME="linux"
        ARM_OR_AMD="amd64"

    elif [ $(uname -m) = "x86_64" ] && [ $(uname) = "Darwin" ]; then
        echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为X86 Mac" | lcat
        SYS_NAME="darwin"
        ARM_OR_AMD="amd64"

    else
        echo "$(uname) 系统未知！"
        time_wait
    fi
    echo "$(date +'%Y-%m-%d %H:%M:%S') 当前环境系统名称:\n$SYS_NAME\n"

    echo "$(date +'%Y-%m-%d %H:%M:%S') 当前环境架构名称:\n$ARM_OR_AMD\n"

    RELEASES_URL=${1:-"https://github.com/suntong/html2md/releases"}
    echo "$(date +'%Y-%m-%d %H:%M:%S') RELEASES_URL:\n$RELEASES_URL\n"

    PROJECT_NAME=$(echo $RELEASES_URL | awk '{ gsub(/\//,"\t"); print $0 }' | field 4)
    echo "$(date +'%Y-%m-%d %H:%M:%S') PROJECT_NAME:\n$PROJECT_NAME\n"

    LATEST_VERSION=$(curl -s $RELEASES_URL | htmlq a --attribute href | grep $PROJECT_NAME | grep "amd64\|arm\|linux\|windows\|darwin" | head -n 1 | cutnum | line 2 2)
    echo "$(date +'%Y-%m-%d %H:%M:%S') LATEST_VERSION:\n$LATEST_VERSION\n"

    MATCH_PAKAGES=$(curl -s $RELEASES_URL | htmlq a --attribute href | grep $PROJECT_NAME | grep "$LATEST_VERSION" | grep "$SYS_NAME" | grep "$ARM_OR_AMD")
    MATCH_PAKAGES=https://github.com$MATCH_PAKAGES
    echo "$(date +'%Y-%m-%d %H:%M:%S') MATCH_PAKAGES:\n$MATCH_PAKAGES\n"

    UNTAR_PAKAGES=$(curl -s $RELEASES_URL | htmlq a --attribute href | grep $PROJECT_NAME | grep "github" | grep "$LATEST_VERSION" | grep "$SYS_NAME" | grep "$ARM_OR_AMD" | grep -v 'tar\|gz\|zip')
    [[ -n $UNTAR_PAKAGES ]] && UNTAR_PAKAGES=https://github.com$UNTAR_PAKAGES || echo "未抓取到非压缩包"
    echo "$(date +'%Y-%m-%d %H:%M:%S') 二进制文件:\n$UNTAR_PAKAGES\n"

    FAST_FIRST_UNTAR_PAKAGE=$(echo $FAST_HOST//https://github.com$(curl -s $RELEASES_URL | htmlq a --attribute href | grep $PROJECT_NAME | grep "$LATEST_VERSION" | grep "$SYS_NAME" | grep "$ARM_OR_AMD" | grep -v 'tar\|gz\|zip' | head -n 1))
    [[ $FAST_FIRST_UNTAR_PAKAGE == "$FAST_HOST//https://github.com" ]] && echo "未抓取到非压缩包" || echo "$(date +'%Y-%m-%d %H:%M:%S') 第一个fast二进制文件url:\n$FAST_FIRST_UNTAR_PAKAGE\n"

    TAR_PAKAGES=$(curl -s $RELEASES_URL | htmlq a --attribute href | grep $PROJECT_NAME | grep "$LATEST_VERSION" | grep "$SYS_NAME" | grep "$ARM_OR_AMD" | grep 'tar\|gz\|zip')
    [[ -n $TAR_PAKAGES ]] && TAR_PAKAGES=https://github.com$TAR_PAKAGES || echo "未抓取到压缩包"
    echo "$(date +'%Y-%m-%d %H:%M:%S') tar包:\n$TAR_PAKAGES\n"

    FAST_FIRST_TAR_PAKAGE=$(echo $FAST_HOST//https://github.com$(curl -s $RELEASES_URL | htmlq a --attribute href | grep $PROJECT_NAME | grep "$LATEST_VERSION" | grep "$SYS_NAME" | grep "$ARM_OR_AMD" | grep 'tar\|gz\|zip' | head -n 1))

    [[ $FAST_FIRST_TAR_PAKAGE == "$FAST_HOST//https://github.com" ]] && echo "未抓取到压缩包" || echo "$(date +'%Y-%m-%d %H:%M:%S') 第一个非二进制文件url:\n$FAST_FIRST_TAR_PAKAGE\n"

    # curl -s $RELEASES_URL | htmlq a --attribute href | grep $PROJECT_NAME | grep "github" | grep "$LATEST_VERSION" | grep "$SYS_NAME" | grep "$ARM_OR_AMD"

    if [ $(echo $UNTAR_PAKAGES | grep "$PROJECT_NAME" | wc -l) -eq 1 ]; then
        echo "$(date +'%Y-%m-%d %H:%M:%S') 有二进制文件优先下载二进制:\n$UNTAR_PAKAGES\n$FAST_FIRST_UNTAR_PAKAGE\n"

        curl --silent --location $(echo $FAST_FIRST_UNTAR_PAKAGE | awk '{ gsub("//","/"); print $0 }' | sed -e 's/\(......\)/\1\//') -o /tmp/$PROJECT_NAME
        [ -f $(ls /tmp/ | grep $PROJECT_NAME) ] && echo "tmp/$PROJECT_NAME is file" || mv /tmp/$PROJECT_NAME*/$PROJECT_NAME /tmp/$PROJECT_NAME
        chmod +x /tmp/$PROJECT_NAME
        sudo mv /tmp/$PROJECT_NAME /usr/local/bin

        echo "$(date +'%Y-%m-%d %H:%M:%S') 验证 $PROJECT_NAME 版本与功能"
        eval $PROJECT_NAME
        eval $PROJECT_NAME version

        echo "$(date +'%Y-%m-%d %H:%M:%S') install $PROJECT_NAME success\n"

    elif

        [ $(echo $TAR_PAKAGES | grep "$PROJECT_NAME" | wc -l) -eq 1 ] && [ $(echo "$TAR_PAKAGES" | grep "gz" | wc -l) -eq 1 ]
    then
        echo "$(date +'%Y-%m-%d %H:%M:%S') 仅有tar包:\n$UNTAR_PAKAGES\n$FAST_FIRST_TAR_PAKAGE\n"
        curl --silent --location $(echo $FAST_FIRST_TAR_PAKAGE | awk '{ gsub("//","/"); print $0 }' | sed -e 's/\(......\)/\1\//') | tar xz -C /tmp
        [ -f $(ls /tmp/ | grep $PROJECT_NAME) ] && echo "tmp/$PROJECT_NAME is file" || mv /tmp/$PROJECT_NAME*/$PROJECT_NAME /tmp/$PROJECT_NAME
        chmod +x /tmp/$PROJECT_NAME
        sudo mv /tmp/$PROJECT_NAME /usr/local/bin

        echo "$(date +'%Y-%m-%d %H:%M:%S') 验证 $PROJECT_NAME 版本与功能"
        eval $PROJECT_NAME
        eval $PROJECT_NAME version

        echo "$(date +'%Y-%m-%d %H:%M:%S') install $PROJECT_NAME success\n"
    elif
        [ $(echo $TAR_PAKAGES | grep "$PROJECT_NAME" | wc -l) -eq 1 ] && [ $(echo "$TAR_PAKAGES" | grep "zip" | wc -l) -eq 1 ]
    then

        echo "$(date +'%Y-%m-%d %H:%M:%S') 仅有zip包:\n$UNTAR_PAKAGES\n$FAST_FIRST_TAR_PAKAGE\n"
        cd /tmp && curl --silent --location $(echo $FAST_FIRST_TAR_PAKAGE | awk '{ gsub("//","/"); print $0 }' | sed -e 's/\(......\)/\1\//') | unzip
        [ -f $(ls /tmp/ | grep $PROJECT_NAME) ] && echo "tmp/$PROJECT_NAME is file" || mv /tmp/$PROJECT_NAME*/$PROJECT_NAME /tmp/$PROJECT_NAME
        chmod +x /tmp/$PROJECT_NAME
        sudo mv /tmp/$PROJECT_NAME /usr/local/bin

        echo "$(date +'%Y-%m-%d %H:%M:%S') 验证 $PROJECT_NAME 版本与功能"
        eval $PROJECT_NAME
        eval $PROJECT_NAME version

        echo "$(date +'%Y-%m-%d %H:%M:%S') install $PROJECT_NAME success\n"

    else

        echo "$(date +'%Y-%m-%d %H:%M:%S') 未找到任何包⚠️\n检查 $RELEASES_URL\n"

    fi

}

install_html2md_latest() {
    n2_install_github_file_latest_version https://github.com/suntong/html2md/releases
}

install_tinypng_latest() {
    "echo tinypng 是 jpg和png压缩程序"
    "Go语言编写的 TinyPNG 客户端 v1.0"
    n2_install_github_file_latest_version https://github.com/iikira/tinypng/releases/

}

# 支持mac标记的无压缩包文件
n3_install_github_file_latest_version() {
    FAST_HOST="https://github.91chi.fun"

    if [ $(uname -m) = "armv7l" ] && [ $(uname) = "Linux" ]; then
        echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为arm Linux" | lcat
        SYS_NAME="linux"
        ARM_OR_AMD="arm"

    elif [ $(uname -m) = "x86_64" ] && [ $(uname) = "Linux" ]; then
        echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为X86 Linux" | lcat
        SYS_NAME="linux"
        ARM_OR_AMD="amd64"

    elif [ $(uname -m) = "x86_64" ] && [ $(uname) = "Darwin" ]; then
        echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为X86 Mac" | lcat
        SYS_NAME="darwin\|mac"
        ARM_OR_AMD="amd64"

    else
        echo "$(uname) 系统未知！"
        time_wait
    fi
    echo "$(date +'%Y-%m-%d %H:%M:%S') 当前环境系统名称:\n$SYS_NAME\n"

    echo "$(date +'%Y-%m-%d %H:%M:%S') 当前环境架构名称:\n$ARM_OR_AMD\n"

    RELEASES_URL=${1:-"https://github.com/barnybug/s3/releases"}
    echo "$(date +'%Y-%m-%d %H:%M:%S') RELEASES_URL:\n$RELEASES_URL\n"

    PROJECT_NAME=$(echo $RELEASES_URL | awk '{ gsub(/\//,"\t"); print $0 }' | field 4)
    echo "$(date +'%Y-%m-%d %H:%M:%S') PROJECT_NAME:\n$PROJECT_NAME\n"

    LATEST_VERSION=$(curl -s $RELEASES_URL | htmlq a --attribute href | grep $PROJECT_NAME | grep "amd64\|arm\|linux\|windows\|darwin\|mac" | head -n 1 | cutnum | line 2 2)
    echo "$(date +'%Y-%m-%d %H:%M:%S') LATEST_VERSION:\n$LATEST_VERSION\n"

    MATCH_PAKAGES=$(curl -s $RELEASES_URL | htmlq a --attribute href | grep $PROJECT_NAME | grep "$LATEST_VERSION" | grep "$SYS_NAME" | grep "$ARM_OR_AMD")
    MATCH_PAKAGES=https://github.com$MATCH_PAKAGES
    echo "$(date +'%Y-%m-%d %H:%M:%S') MATCH_PAKAGES:\n$MATCH_PAKAGES\n"

    UNTAR_PAKAGES=$(curl -s $RELEASES_URL | htmlq a --attribute href | grep $PROJECT_NAME | grep "github" | grep "$LATEST_VERSION" | grep "$SYS_NAME" | grep "$ARM_OR_AMD" | grep -v 'tar\|gz\|zip')
    [[ -n $UNTAR_PAKAGES ]] && UNTAR_PAKAGES=https://github.com$UNTAR_PAKAGES || echo "未抓取到非压缩包"
    echo "$(date +'%Y-%m-%d %H:%M:%S') 二进制文件:\n$UNTAR_PAKAGES\n"

    FAST_FIRST_UNTAR_PAKAGE=$(echo $FAST_HOST//https://github.com$(curl -s $RELEASES_URL | htmlq a --attribute href | grep $PROJECT_NAME | grep "$LATEST_VERSION" | grep "$SYS_NAME" | grep "$ARM_OR_AMD" | grep -v 'tar\|gz\|zip' | head -n 1))
    [[ $FAST_FIRST_UNTAR_PAKAGE == "$FAST_HOST//https://github.com" ]] && echo "未抓取到非压缩包" || echo "$(date +'%Y-%m-%d %H:%M:%S') 第一个fast二进制文件url:\n$FAST_FIRST_UNTAR_PAKAGE\n"

    TAR_PAKAGES=$(curl -s $RELEASES_URL | htmlq a --attribute href | grep $PROJECT_NAME | grep "$LATEST_VERSION" | grep "$SYS_NAME" | grep "$ARM_OR_AMD" | grep 'tar\|gz\|zip')
    [[ -n $TAR_PAKAGES ]] && TAR_PAKAGES=https://github.com$TAR_PAKAGES || echo "未抓取到压缩包"
    echo "$(date +'%Y-%m-%d %H:%M:%S') tar包:\n$TAR_PAKAGES\n"

    FAST_FIRST_TAR_PAKAGE=$(echo $FAST_HOST//https://github.com$(curl -s $RELEASES_URL | htmlq a --attribute href | grep $PROJECT_NAME | grep "$LATEST_VERSION" | grep "$SYS_NAME" | grep "$ARM_OR_AMD" | grep 'tar\|gz\|zip' | head -n 1))

    [[ $FAST_FIRST_TAR_PAKAGE == "$FAST_HOST//https://github.com" ]] && echo "未抓取到压缩包" || echo "$(date +'%Y-%m-%d %H:%M:%S') 第一个非二进制文件url:\n$FAST_FIRST_TAR_PAKAGE\n"

    # curl -s $RELEASES_URL | htmlq a --attribute href | grep $PROJECT_NAME | grep "github" | grep "$LATEST_VERSION" | grep "$SYS_NAME" | grep "$ARM_OR_AMD"

    if [ $(echo $FAST_FIRST_UNTAR_PAKAGE | grep "$PROJECT_NAME" | wc -l) -eq 1 ]; then
        echo "$(date +'%Y-%m-%d %H:%M:%S') 有二进制文件优先下载二进制:\n$UNTAR_PAKAGES\n$FAST_FIRST_UNTAR_PAKAGE\n"

        curl --silent --location $(echo $FAST_FIRST_UNTAR_PAKAGE | awk '{ gsub("//","/"); print $0 }' | sed -e 's/\(......\)/\1\//') -o /tmp/$PROJECT_NAME
        [ -f $(ls /tmp/ | grep $PROJECT_NAME) ] && echo "tmp/$PROJECT_NAME is file" || mv /tmp/$PROJECT_NAME*/$PROJECT_NAME /tmp/$PROJECT_NAME
        chmod +x /tmp/$PROJECT_NAME
        sudo mv /tmp/$PROJECT_NAME /usr/local/bin

        echo "$(date +'%Y-%m-%d %H:%M:%S') 验证 $PROJECT_NAME 版本与功能"
        eval $PROJECT_NAME
        eval $PROJECT_NAME version

        echo "$(date +'%Y-%m-%d %H:%M:%S') install $PROJECT_NAME success\n"

    elif

        [ $(echo $TAR_PAKAGES | grep "$PROJECT_NAME" | wc -l) -eq 1 ] && [ $(echo "$TAR_PAKAGES" | grep "gz" | wc -l) -eq 1 ]
    then
        echo "$(date +'%Y-%m-%d %H:%M:%S') 仅有tar包:\n$UNTAR_PAKAGES\n$FAST_FIRST_TAR_PAKAGE\n"
        curl --silent --location $(echo $FAST_FIRST_TAR_PAKAGE | awk '{ gsub("//","/"); print $0 }' | sed -e 's/\(......\)/\1\//') | tar xz -C /tmp
        [ -f $(ls /tmp/ | grep $PROJECT_NAME) ] && echo "tmp/$PROJECT_NAME is file" || mv /tmp/$PROJECT_NAME*/$PROJECT_NAME /tmp/$PROJECT_NAME
        chmod +x /tmp/$PROJECT_NAME
        sudo mv /tmp/$PROJECT_NAME /usr/local/bin

        echo "$(date +'%Y-%m-%d %H:%M:%S') 验证 $PROJECT_NAME 版本与功能"
        eval $PROJECT_NAME
        eval $PROJECT_NAME version

        echo "$(date +'%Y-%m-%d %H:%M:%S') install $PROJECT_NAME success\n"
    elif
        [ $(echo $TAR_PAKAGES | grep "$PROJECT_NAME" | wc -l) -eq 1 ] && [ $(echo "$TAR_PAKAGES" | grep "zip" | wc -l) -eq 1 ]
    then

        echo "$(date +'%Y-%m-%d %H:%M:%S') 仅有zip包:\n$UNTAR_PAKAGES\n$FAST_FIRST_TAR_PAKAGE\n"
        cd /tmp && curl --silent --location $(echo $FAST_FIRST_TAR_PAKAGE | awk '{ gsub("//","/"); print $0 }' | sed -e 's/\(......\)/\1\//') | unzip
        [ -f $(ls /tmp/ | grep $PROJECT_NAME) ] && echo "tmp/$PROJECT_NAME is file" || mv /tmp/$PROJECT_NAME*/$PROJECT_NAME /tmp/$PROJECT_NAME
        chmod +x /tmp/$PROJECT_NAME
        sudo mv /tmp/$PROJECT_NAME /usr/local/bin

        echo "$(date +'%Y-%m-%d %H:%M:%S') 验证 $PROJECT_NAME 版本与功能"
        eval $PROJECT_NAME
        eval $PROJECT_NAME version

        echo "$(date +'%Y-%m-%d %H:%M:%S') install $PROJECT_NAME success\n"

    else

        echo "$(date +'%Y-%m-%d %H:%M:%S') 未找到任何包⚠️\n检查 $RELEASES_URL\n"

    fi

}

# 支持 gz  不含 tgz or tar.gz
n4_install_github_file_latest_version() {
    FAST_HOST="https://github.91chi.fun"

    if [ $(uname -m) = "armv7l" ] && [ $(uname) = "Linux" ]; then
        echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为arm Linux" | lcat
        SYS_NAME="linux"
        ARM_OR_AMD="arm"

    elif [ $(uname -m) = "x86_64" ] && [ $(uname) = "Linux" ]; then
        echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为X86 Linux" | lcat
        SYS_NAME="linux"
        ARM_OR_AMD="amd64"

    elif [ $(uname -m) = "x86_64" ] && [ $(uname) = "Darwin" ]; then
        echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为X86 Mac" | lcat
        SYS_NAME="darwin\|mac"
        ARM_OR_AMD="amd64"

    else
        echo "$(uname) 系统未知！"
        time_wait
    fi
    echo "$(date +'%Y-%m-%d %H:%M:%S') 当前环境系统名称:\n$SYS_NAME\n"

    echo "$(date +'%Y-%m-%d %H:%M:%S') 当前环境架构名称:\n$ARM_OR_AMD\n"

    RELEASES_URL=${1:-"https://github.com/barnybug/s3/releases"}
    echo "$(date +'%Y-%m-%d %H:%M:%S') RELEASES_URL:\n$RELEASES_URL\n"

    PROJECT_NAME=$(echo $RELEASES_URL | awk '{ gsub(/\//,"\t"); print $0 }' | field 4)
    echo "$(date +'%Y-%m-%d %H:%M:%S') PROJECT_NAME:\n$PROJECT_NAME\n"

    LATEST_VERSION=$(curl -s $RELEASES_URL | htmlq a --attribute href | grep $PROJECT_NAME | grep "amd64\|arm\|linux\|windows\|darwin\|mac" | head -n 1 | cutnum | line 2 2)
    echo "$(date +'%Y-%m-%d %H:%M:%S') LATEST_VERSION:\n$LATEST_VERSION\n"

    MATCH_PAKAGES=$(curl -s $RELEASES_URL | htmlq a --attribute href | grep $PROJECT_NAME | grep "$LATEST_VERSION" | grep "$SYS_NAME" | grep "$ARM_OR_AMD")
    MATCH_PAKAGES=https://github.com$MATCH_PAKAGES
    echo "$(date +'%Y-%m-%d %H:%M:%S') MATCH_PAKAGES:\n$MATCH_PAKAGES\n"

    UNTAR_PAKAGES=$(curl -s $RELEASES_URL | htmlq a --attribute href | grep $PROJECT_NAME | grep "github" | grep "$LATEST_VERSION" | grep "$SYS_NAME" | grep "$ARM_OR_AMD" | grep -v 'tar\|gz\|zip')
    [[ -n $UNTAR_PAKAGES ]] && UNTAR_PAKAGES=https://github.com$UNTAR_PAKAGES || echo "未抓取到非压缩包"
    echo "$(date +'%Y-%m-%d %H:%M:%S') 二进制文件:\n$UNTAR_PAKAGES\n"

    FAST_FIRST_UNTAR_PAKAGE=$(echo $FAST_HOST//https://github.com$(curl -s $RELEASES_URL | htmlq a --attribute href | grep $PROJECT_NAME | grep "$LATEST_VERSION" | grep "$SYS_NAME" | grep "$ARM_OR_AMD" | grep -v 'tar\|gz\|zip' | head -n 1))
    [[ $FAST_FIRST_UNTAR_PAKAGE == "$FAST_HOST//https://github.com" ]] && echo "未抓取到非压缩包" || echo "$(date +'%Y-%m-%d %H:%M:%S') 第一个fast二进制文件url:\n$FAST_FIRST_UNTAR_PAKAGE\n"

    TAR_PAKAGES=$(curl -s $RELEASES_URL | htmlq a --attribute href | grep $PROJECT_NAME | grep "$LATEST_VERSION" | grep "$SYS_NAME" | grep "$ARM_OR_AMD" | grep 'tar\|gz\|zip')
    [[ -n $TAR_PAKAGES ]] && TAR_PAKAGES=https://github.com$TAR_PAKAGES || echo "未抓取到压缩包"
    echo "$(date +'%Y-%m-%d %H:%M:%S') tar包:\n$TAR_PAKAGES\n"

    FAST_FIRST_TAR_PAKAGE=$(echo $FAST_HOST//https://github.com$(curl -s $RELEASES_URL | htmlq a --attribute href | grep $PROJECT_NAME | grep "$LATEST_VERSION" | grep "$SYS_NAME" | grep "$ARM_OR_AMD" | grep 'tar\|gz\|zip' | head -n 1))

    [[ $FAST_FIRST_TAR_PAKAGE == "$FAST_HOST//https://github.com" ]] && echo "未抓取到压缩包" || echo "$(date +'%Y-%m-%d %H:%M:%S') 第一个非二进制文件url:\n$FAST_FIRST_TAR_PAKAGE\n"

    # curl -s $RELEASES_URL | htmlq a --attribute href | grep $PROJECT_NAME | grep "github" | grep "$LATEST_VERSION" | grep "$SYS_NAME" | grep "$ARM_OR_AMD"

    if [ $(echo $FAST_FIRST_UNTAR_PAKAGE | grep "$PROJECT_NAME" | wc -l) -eq 1 ]; then
        echo "$(date +'%Y-%m-%d %H:%M:%S') 有二进制文件优先下载二进制:\n$UNTAR_PAKAGES\n$FAST_FIRST_UNTAR_PAKAGE\n"

        curl --silent --location $(echo $FAST_FIRST_UNTAR_PAKAGE | awk '{ gsub("//","/"); print $0 }' | sed -e 's/\(......\)/\1\//') -o /tmp/$PROJECT_NAME
        [ -f $(ls /tmp/ | grep $PROJECT_NAME) ] && echo "tmp/$PROJECT_NAME is file" || mv /tmp/$PROJECT_NAME*/$PROJECT_NAME /tmp/$PROJECT_NAME
        chmod +x /tmp/$PROJECT_NAME
        sudo mv /tmp/$PROJECT_NAME /usr/local/bin

        echo "$(date +'%Y-%m-%d %H:%M:%S') 验证 $PROJECT_NAME 版本与功能"
        eval $PROJECT_NAME
        eval $PROJECT_NAME version

        echo "$(date +'%Y-%m-%d %H:%M:%S') install $PROJECT_NAME success\n"

    elif

        [ $(echo "$TAR_PAKAGES" | grep -v "sha256" | grep -v "tar" | grep -v "tgz" | grep "gz" | wc -l | pass_black) -eq 1 ]
    then
        # echo "gogogogo"
        echo "$(date +'%Y-%m-%d %H:%M:%S') 仅有gz包:\n$UNTAR_PAKAGES\n$FAST_FIRST_TAR_PAKAGE\n"
        cd /tmp
        sudo rm -rf ./${PROJECT_NAME}-*
        sudo wget -N -t 0 -c -P /tmp $(echo $FAST_FIRST_TAR_PAKAGE | awk '{ gsub("//","/"); print $0 }' | sed -e 's/\(......\)/\1\//')
        DOWNLOAD_NAME=$(basename $FAST_FIRST_TAR_PAKAGE)

        gzip --uncompress $DOWNLOAD_NAME
        mv `ls  | grep $PROJECT_NAME | grep -v gz` $PROJECT_NAME
        sudo chmod +x /tmp/$PROJECT_NAME
        sudo mv /tmp/$PROJECT_NAME /usr/local/bin
        sudo rm -rf ./${PROJECT_NAME}-*
        sudo rm -rf ./${PROJECT_NAME}

        echo "$(date +'%Y-%m-%d %H:%M:%S') 验证 $PROJECT_NAME 版本与功能"
        # eval $PROJECT_NAME
        eval $PROJECT_NAME --version

        echo "$(date +'%Y-%m-%d %H:%M:%S') install $PROJECT_NAME success\n"

    elif

        [ $(echo $TAR_PAKAGES | grep "$PROJECT_NAME" | wc -l) -eq 1 ] && [ $(echo "$TAR_PAKAGES" | grep "gz" | wc -l) -eq 1 ]
    then
        echo "$(date +'%Y-%m-%d %H:%M:%S') 仅有tar包:\n$UNTAR_PAKAGES\n$FAST_FIRST_TAR_PAKAGE\n"
        curl --silent --location $(echo $FAST_FIRST_TAR_PAKAGE | awk '{ gsub("//","/"); print $0 }' | sed -e 's/\(......\)/\1\//') | tar xz -C /tmp
        [ -f $(ls /tmp/ | grep $PROJECT_NAME) ] && echo "tmp/$PROJECT_NAME is file" || mv /tmp/$PROJECT_NAME*/$PROJECT_NAME /tmp/$PROJECT_NAME
        chmod +x /tmp/$PROJECT_NAME
        sudo mv /tmp/$PROJECT_NAME /usr/local/bin

        echo "$(date +'%Y-%m-%d %H:%M:%S') 验证 $PROJECT_NAME 版本与功能"
        eval $PROJECT_NAME
        eval $PROJECT_NAME version

        echo "$(date +'%Y-%m-%d %H:%M:%S') install $PROJECT_NAME success\n"
    elif
        [ $(echo $TAR_PAKAGES | grep "$PROJECT_NAME" | wc -l) -eq 1 ] && [ $(echo "$TAR_PAKAGES" | grep "zip" | wc -l) -eq 1 ]
    then

        echo "$(date +'%Y-%m-%d %H:%M:%S') 仅有zip包:\n$UNTAR_PAKAGES\n$FAST_FIRST_TAR_PAKAGE\n"
        cd /tmp && curl --silent --location $(echo $FAST_FIRST_TAR_PAKAGE | awk '{ gsub("//","/"); print $0 }' | sed -e 's/\(......\)/\1\//') | unzip
        [ -f $(ls /tmp/ | grep $PROJECT_NAME) ] && echo "tmp/$PROJECT_NAME is file" || mv /tmp/$PROJECT_NAME*/$PROJECT_NAME /tmp/$PROJECT_NAME
        chmod +x /tmp/$PROJECT_NAME
        sudo mv /tmp/$PROJECT_NAME /usr/local/bin

        echo "$(date +'%Y-%m-%d %H:%M:%S') 验证 $PROJECT_NAME 版本与功能"
        eval $PROJECT_NAME
        eval $PROJECT_NAME version

        echo "$(date +'%Y-%m-%d %H:%M:%S') install $PROJECT_NAME success\n"

    else

        echo "$(date +'%Y-%m-%d %H:%M:%S') 未找到任何包⚠️\n检查 $RELEASES_URL\n"

    fi

}


install_nali_latest(){
    # ip地址解析 nali
    echo "# https://github.com/zu1k/nali/releases"
    echo "# 一个查询IP地理信息和CDN服务提供商的离线终端工具.An offline tool for querying IP geographic information and CDN provider."
    n4_install_github_file_latest_version https://github.com/zu1k/nali/releases 
    cd ~/upload
    ls ~/.nali/config.yaml
    cat ~/.nali/config.yaml
}

install_s3_lastest() {
    echo "s3 是 golang 的 s3 查看工具"
    echo "支持如下命令"
    echo "Swiss army pen-knife for Amazon S3."
    echo "ls: List buckets or keys"
    echo "get: Download keys"
    echo "cat: Cat keys"
    echo "grep: Search for key containing text"
    echo "sync: Synchronise local to s3, s3 to local or s3 to s3"
    echo "rm: Delete keys"
    echo "mb: Create buckets"
    echo "rb: Delete buckets"
    n3_install_github_file_latest_version https://github.com/barnybug/s3/releases
}

# apache general download

# mac linux 通用
install_apache_incubator_lastest() {
    PROJECT_NAME=${1:-seatunnel}
    cd ~/upload

    # pakages最新版本
    VERSION=$(curl $APACHE_HOST"/apache/incubator/$PROJECT_NAME/" \
        -H 'Connection: keep-alive' \
        -H 'sec-ch-ua: "Chromium";v="88", "Google Chrome";v="88", ";Not A Brand";v="99"' \
        -H 'sec-ch-ua-mobile: ?0' \
        -H 'Upgrade-Insecure-Requests: 1' \
        -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 11_2_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.192 Safari/537.36' \
        -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9' \
        -H 'Sec-Fetch-Site: same-origin' \
        -H 'Sec-Fetch-Mode: navigate' \
        -H 'Sec-Fetch-User: ?1' \
        -H 'Sec-Fetch-Dest: document' \
        -H 'Referer: https://mirror-hk.koddos.net/apache/' \
        -H 'Accept-Language: zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7' \
        -s \
        --compressed | grep '20[2-9][0-9]-[0-1][1-9]-[0-3][0-9]' | keycut '<a href="' '/">' | sort -r | head -n 1)

    echo "$(date +'%Y-%m-%d %H:%M:%S') $PROJECT_NAME Latest version: $VERSION"
    # KAFKA_VERSION=`http https://mirror-hk.koddos.net/apache/kafka/ | grep   '20[2-9][0-9]-[0-1][1-9]-[0-3][0-9]' | keycut '<a href="' '/">' | sort -r  | head -n 1`

    PACKAGE_NAME=$(curl $APACHE_HOST"/apache/incubator/$PROJECT_NAME/$VERSION/" \
        -H 'Connection: keep-alive' \
        -H 'sec-ch-ua: "Chromium";v="88", "Google Chrome";v="88", ";Not A Brand";v="99"' \
        -H 'sec-ch-ua-mobile: ?0' \
        -H 'Upgrade-Insecure-Requests: 1' \
        -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 11_2_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.192 Safari/537.36' \
        -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9' \
        -H 'Sec-Fetch-Site: same-origin' \
        -H 'Sec-Fetch-Mode: navigate' \
        -H 'Sec-Fetch-User: ?1' \
        -H 'Sec-Fetch-Dest: document' \
        -H 'Referer: https://mirror-hk.koddos.net/apache/spark/' \
        -H 'Accept-Language: zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7' \
        -s \
        --compressed | htmlq a --attribute href | grep "$VERSION\|$PROJECT_NAME" | grep -v 'src' | sort -r | head -n 1)

    echo "$(date +'%Y-%m-%d %H:%M:%S') PACKAGE_NAME latest: $PACKAGE_NAME"

    mwget $APACHE_HOST"/apache/incubator/$PROJECT_NAME/$VERSION/${PACKAGE_NAME}"

    tar -zxvf $PACKAGE_NAME >/dev/null 2>&1

    [[ -d /usr/local/$PROJECT_NAME ]] &&
        (sudo rm -rf /usr/local/$PROJECT_NAME && sudo mkdir /usr/local/$PROJECT_NAME && sudo chmod -R 777 /usr/local/$PROJECT_NAME) ||
        (sudo mkdir /usr/local/$PROJECT_NAME && sudo chmod -R 777 /usr/local/$PROJECT_NAME)

    PACKAGE_DIR=$(ls ~/upload | grep $PROJECT_NAME | grep -v 'tgz\|gz' | sort -r | head -n 1)

    cp -r $PACKAGE_DIR/* /usr/local/$PROJECT_NAME
    l /usr/local/$PROJECT_NAME

    if [ $(uname) = "Darwin" ] && [ -d /usr/local/$PROJECT_NAME ]; then
        export PROJECT_NAME_HOME=/usr/local/$PROJECT_NAME
        export PATH=$PATH:$PROJECT_NAME_HOME/bin
    elif [ $(uname) = "Linux" ] && [ -d /usr/local/$PROJECT_NAME ]; then
        export PROJECT_NAME_HOME=/usr/local/$PROJECT_NAME
        export PATH=$PATH:$PROJECT_NAME_HOME/bin
    else
        echo "$(uname)  系统未知 或 还未添加 $PROJECT_NAME bin"
    fi

    echo "$(date +'%Y-%m-%d %H:%M:%S') PACKAGE_NAME bin:"
    ls $PROJECT_NAME_HOME/bin

    rm -rf $PACKAGE_DIR
    rm -rf $PACKAGE_NAME

}

install_apache_lastest() {
    PROJECT_NAME=${1:-seatunnel}
    cd ~/upload

    # pakages最新版本
    VERSION=$(curl $APACHE_HOST"/apache/$PROJECT_NAME/" \
        -H 'Connection: keep-alive' \
        -H 'sec-ch-ua: "Chromium";v="88", "Google Chrome";v="88", ";Not A Brand";v="99"' \
        -H 'sec-ch-ua-mobile: ?0' \
        -H 'Upgrade-Insecure-Requests: 1' \
        -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 11_2_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.192 Safari/537.36' \
        -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9' \
        -H 'Sec-Fetch-Site: same-origin' \
        -H 'Sec-Fetch-Mode: navigate' \
        -H 'Sec-Fetch-User: ?1' \
        -H 'Sec-Fetch-Dest: document' \
        -H 'Referer: https://mirror-hk.koddos.net/apache/' \
        -H 'Accept-Language: zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7' \
        -s \
        --compressed | grep '20[2-9][0-9]-[0-1][1-9]-[0-3][0-9]' | keycut '<a href="' '/">' | sort -r | head -n 1)

    echo "$(date +'%Y-%m-%d %H:%M:%S') $PROJECT_NAME Latest version: $VERSION"
    # KAFKA_VERSION=`http https://mirror-hk.koddos.net/apache/kafka/ | grep   '20[2-9][0-9]-[0-1][1-9]-[0-3][0-9]' | keycut '<a href="' '/">' | sort -r  | head -n 1`

    PACKAGE_NAME=$(curl $APACHE_HOST"/apache/$PROJECT_NAME/$VERSION/" \
        -H 'Connection: keep-alive' \
        -H 'sec-ch-ua: "Chromium";v="88", "Google Chrome";v="88", ";Not A Brand";v="99"' \
        -H 'sec-ch-ua-mobile: ?0' \
        -H 'Upgrade-Insecure-Requests: 1' \
        -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 11_2_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.192 Safari/537.36' \
        -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9' \
        -H 'Sec-Fetch-Site: same-origin' \
        -H 'Sec-Fetch-Mode: navigate' \
        -H 'Sec-Fetch-User: ?1' \
        -H 'Sec-Fetch-Dest: document' \
        -H 'Referer: https://mirror-hk.koddos.net/apache/spark/' \
        -H 'Accept-Language: zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7' \
        -s \
        --compressed | htmlq a --attribute href | grep "$VERSION\|$PROJECT_NAME" | grep -v 'src' | sort -r | head -n 1)

    echo "$(date +'%Y-%m-%d %H:%M:%S') PACKAGE_NAME latest: $PACKAGE_NAME"

    mwget $APACHE_HOST"/apache/$PROJECT_NAME/$VERSION/${PACKAGE_NAME}"

    tar -zxvf $PACKAGE_NAME >/dev/null 2>&1

    [[ -d /usr/local/$PROJECT_NAME ]] &&
        (sudo rm -rf /usr/local/$PROJECT_NAME && sudo mkdir /usr/local/$PROJECT_NAME && sudo chmod -R 777 /usr/local/$PROJECT_NAME) ||
        (sudo mkdir /usr/local/$PROJECT_NAME && sudo chmod -R 777 /usr/local/$PROJECT_NAME)

    PACKAGE_DIR=$(ls ~/upload | grep $PROJECT_NAME | grep -v 'tgz\|gz' | sort -r | head -n 1)

    cp -r $PACKAGE_DIR/* /usr/local/$PROJECT_NAME
    l /usr/local/$PROJECT_NAME

    if [ $(uname) = "Darwin" ] && [ -d /usr/local/$PROJECT_NAME ]; then
        export PROJECT_NAME_HOME=/usr/local/$PROJECT_NAME
        export PATH=$PATH:$PROJECT_NAME_HOME/bin
    elif [ $(uname) = "Linux" ] && [ -d /usr/local/$PROJECT_NAME ]; then
        export PROJECT_NAME_HOME=/usr/local/$PROJECT_NAME
        export PATH=$PATH:$PROJECT_NAME_HOME/bin
    else
        echo "$(uname)  系统未知 或 还未添加 $PROJECT_NAME bin"
    fi

    echo "$(date +'%Y-%m-%d %H:%M:%S') PACKAGE_NAME bin:"
    ls $PROJECT_NAME_HOME/bin

    rm -rf $PACKAGE_DIR
    rm -rf $PACKAGE_NAME

}
