install_adb() {
    brew install git
    brew install android-platform-tools
}
# 默认🔗SUMSUNG
typeset MYDEVICES="192.168.1.3:40721"
# typeset MYDEVICES="ad6ac0190306"
#  设置K8S 主机 IP，默认 loop
set_driver_8notepro() {
    MYDEVICES=${1:-192.168.2.11:5556}
}

set_driver_12s() {
    MYDEVICES=${1:-192.168.1.11:41261}
}

#  设置K8S 主机 IP，默认 local  务必先注册  reg_k8s master_ip
set_driver_max2() {
    MYDEVICES=${1:-192.168.249.233:5557}
}

# 设置 k8s 集群为 线上测试环境
set_driver_7notepro() {
    MYDEVICES=${1:-192.168.255.218:5558}
}

# 设置 k8s 集群为 pi 集群
set_driver_8a() {
    MYDEVICES=${1:-192.168.100.109:5555}
}

# 设置 k8s 集群为 pi 集群
set_driver_samsung() {
    MYDEVICES=${1:-192.168.100.173:44021}
}

go_adb() {
    echo " 前提开启手机的调试模式"
    kill-server
    if [ $MYDEVICES = "192.168.100.109:5555" ]; then
        echo "redmi8a"
        PORT="$(echo $MYDEVICES | mformat | field 2)"
        adb tcpip $PORT
        adb connect $MYDEVICES
        adb devices

    elif [ $MYDEVICES = "192.168.2.11:5556" ]; then
        echo "redmi8notePro"
        PORT="$(echo $MYDEVICES | mformat | field 2)"
        adb tcpip $PORT
        # adb connect 192.168.2.13:5555
        adb connect $MYDEVICES
        adb devices

    elif [ $MYDEVICES = "192.168.100.173:44021" ]; then
        echo "sumsung 20"
        PORT="$(echo $MYDEVICES | mformat | field 2)"
        adb tcpip $PORT
        # adb connect 192.168.2.13:5555
        adb connect $MYDEVICES
        adb devices

    elif [ $MYDEVICES = "192.168.1.11:41261" ]; then
        echo "xiaomi 12s ultra"
        PORT="$(echo $MYDEVICES | mformat | field 2)"
        adb tcpip $PORT
        # adb connect 192.168.2.13:5555
        adb connect $MYDEVICES
        adb devices

    else
        echo "继续补充"
    fi
}

go_adb_samsung() {
    echo "MYDEVICES 查看手机的无线调试功能——IP地址和端口"
    set_driver_samsung
    go_adb
}

adb_reboot() {
    adb -s $MYDEVICES reboot
}

# 🐯🐯🐯🐯🐯🐯🐯🐯🐯🐯🐯🐯🐯🐯🐯🐯🐯🐯🐯🐯🐯🐯
# 🐯🐯🐯🐯🐯🐯🐯🐯🐯🐯🐯🐯🐯🐯🐯🐯🐯🐯🐯🐯🐯🐯
# 🐯🐯🐯🐯🐯🐯🐯🐯🐯🐯🐯🐯🐯🐯🐯🐯🐯🐯🐯🐯🐯🐯
adb_clean_button() {
    if [ $MYDEVICES = "192.168.100.109:5555" ]; then
        echo "redmi8a"
        adb_click 350 1400 # 点击清除
    elif [ $MYDEVICES = "ad6ac0190306" ]; then
        echo "redmi8a"
        adb_click 350 1400 # 点击清除
    elif [ $MYDEVICES = "192.168.2.11:5556" ]; then
        echo "redmi8notePro"
        adb_click 540 2222 # 点击清除
    else
        echo "$(date +'%Y-%m-%d %H:%M:%S') 设备位置需要添加"
    fi
}

adb_yes_button() {
    if [ $MYDEVICES = "192.168.100.109:5555" ]; then
        echo "redmi8a"
        adb_click 600 1320 # 点击✅
    elif [ $MYDEVICES = "192.168.2.11:5556" ]; then
        echo "redmi8notePro"
        adb_click 540 2222 # 需要补充
    else
        echo "$(date +'%Y-%m-%d %H:%M:%S') 设备位置需要添加"
    fi
}

install_adb() {
    adb 不存在则安装
    if [ $(adb version | grep version | wc -l | pass_black) = 1 ]; then
        echo "$(date +'%Y-%m-%d %H:%M:%S') adb 已安装"
    else
        # 判断系统分别安装
        brew cask install android-platform-tools
        pip install adbutils
    # https://github.com/openatx/adbutils
    fi
}

# 电池信息
adb_battery() {
    echo "显示电池信息"
    adb -s $MYDEVICES shell dumpsys battery
    # Current Battery Service state:   ####当前电池服务状态
    #   AC powered: false                   ####交流供电
    #   USB powered: true                  ####USB供电
    #   Wireless powered: false          ####无线供电
    #   status: 2                                  ####电池状态：2：充电状态 ，其他数字为非充电状态
    #   health: 2                                  ####电池健康状态：只有数字2表示good
    #   present: true                           ####电池是否安装在机身
    #   level: 82                                  ####电量: 百分比
    #   scale: 100                                ####规模
    #   voltage: 4500                          ####电池电压
    #   temperature: 378                    ####电池温度，单位是0.1摄氏度
    #   current now: -335232             #-------电流值，负数表示正在充电

    #   technology: Li-ion                   ####电池种类=
}

adb_battery_num() {
    adb_battery | grep level | cutnum

}

# app列表
adb_app() {
    echo "显示app列表"
    adb -s $MYDEVICES shell pm list packages -${1:-3} # s是系统
}

adb_code() {
    adb -s $MYDEVICES shell input keyevent ${1:-26} # 默认电源键 点亮屏幕
}

adb_light() {
    adb_code 224 # 点亮屏幕
}

adb_dark() {
    adb_code 224 # 点亮屏幕
    adb_code 26  # 电源键
}

# 向上翻页
adb_page_up() {
    adb_code 92
}

# 向下翻页
adb_page_down() {
    adb_code 93
}

adb_del() {
    adb_code 112
}

adb_head() {
    adb_code 122
}

adb_end() {
    adb_code 123
}

adb_center() {
    adb_code 23
}

adb_enter() {
    adb_code 66
}

adb_esc() {
    adb_code 111
}

adb_home() {
    adb_code 3
}

# 点击坐标
adb_click() {
    adb -s $MYDEVICES shell input tap ${1:-123} ${2:-456}
}

# 滑动
adb_swipe() {
    adb -s $MYDEVICES shell input swipe $*
}

# 音量 🔊
adb_up_sound() {
    adb -s $MYDEVICES shell input keyevent ${1:-24}
}

adb_down_sound() {
    adb -s $MYDEVICES shell input keyevent ${1:-25}
}

adb_close_sound() {
    adb -s $MYDEVICES shell input keyevent ${1:-164}
}

# 以上需要连续按才有效果

# 🖥亮度
adb_up_brightness() {
    adb -s $MYDEVICES shell input keyevent ${1:-221}
}

adb_down_brightness() {
    adb -s $MYDEVICES shell input keyevent ${1:-220}
}

adb_camera() {
    adb_code 224                                                                                                            # 点亮屏幕
    adb_code 3                                                                                                              # 回到桌面
    adb -s $MYDEVICES shell am start -a android.media.action.IMAGE_CAPTURE --ei android.intent.extras.CAMERA_FACING ${1:-1} # 1 为前摄像头 0 为后摄像头
    sleep 2
    adb -s $MYDEVICES shell input keyevent 27
    adb_code
}

adb_call() {
    echo "打电话☎️"
    adb -s $MYDEVICES shell am start -a android.intent.action.CALL tel:${1-18312345678}
}

adb_call_status() {
    echo "查询状态，来电时查询为1  "
    adb -s $MYDEVICES shell service call phone 11
}

adb_call_down() {
    adb -s $MYDEVICES shell service call phone 3
}

adb_msg() {
    adb -s $MYDEVICES shell am start -a android.intent.action.SENDTO -d sms:${1:-18312345678} --es sms_body ${2:-hello neo}
}

adb_text() {
    adb -s $MYDEVICES shell am broadcast -a ADB_INPUT_TEXT --es msg ${1:-neo_你好}
}

adb_pic() {
    adb -s $MYDEVICES exec-out screencap -p >/tmp/sc.png
}

adb_see() {
    cd ~/upload
    adb_light
    adb_pic && see /tmp/sc.png
}

adb_jsee() {
    cd ~/upload
    adb_light
    adb_pic && rsee /tmp/sc.png
}

adb_ps() {
    echo "查看进程"
    adb -s $MYDEVICES shell ps
}

adb_top() {
    adb -s $MYDEVICES shell top
}

adb_clean_app() {
    adb_code 224 # 点亮屏幕
    adb_code 3   # 回到桌面
    adb_code 82  # 菜单键
    sleep 5
    adb_clean_button
    sleep 5
    adb_code
}

adb_clean_msg() {
    adb_code 224             # 点亮屏幕
    adb_code 3               # 回到桌面
    adb_swipe 100 10 100 300 # 滑动管理栏
    sleep 5
    adb_clean_button
    sleep 5
    adb_code
}

adb_clean_all() {
    echo "$(date +'%Y-%m-%d %H:%M:%S') 清理app"
    adb_clean_app
    sleep 3
    echo "$(date +'%Y-%m-%d %H:%M:%S') 清理消息"
    adb_clean_msg
}

adb_dev() {
    adb_clean_all
    adb_light          # 点亮屏幕
    adb_home           # 回到桌面首页
    adb_click 115 1025 # 点击设置
    # sleep 1
    adb_page_down # 启动翻页
    # sleep 1
    adb_page_down # 翻一页
    # sleep 1
    adb_click 400 1055 #  点击更多设置
    # sleep 1
    adb_click 325 1355 #  点击开发者模式
    # sleep 1
    adb_page_down # 启动翻页
    # sleep 1
    adb_page_down # 翻一页
    # sleep 1
    adb_page_down # 翻一页
    # sleep 1
    adb_page_down # 翻一页
    # sleep 1
    adb_page_down # 翻一页
    # sleep 1
    adb_page_down # 翻一页
    # sleep 1
    adb_click 640 240 #  点击显示点按反馈
    # sleep 1
    adb_click 640 370 # 点击显示指针位置
    # sleep 1
    adb_click 640 800 # 点击显示布局边界
    # sleep 1
    adb_home # 回到桌面首页
    adb_code
    echo "$(date +'%Y-%m-%d %H:%M:%S') 开发者模式切换完成✅"
}

adb_now_app_activity() {
    echo "显示当前页面活动"
    adb -s $MYDEVICES shell dumpsys activity | grep "mFocusedActivity"
}

adb_now_app_page_name() {
    echo "显示当前打开页面的app页面代码"
    adb -s $MYDEVICES shell dumpsys window | grep mCurrentFocus | keycut "{" "}" | field 3
}

# adb_app_page(){
# # adb -s $MYDEVICES shell dumpsys package ${1:-com.xiaomi.smarthome} | grep "Activity" | grep "/\." | field 2  | uniq
# adb -s $MYDEVICES shell dumpsys package ${1:-com.xiaomi.smarthome} | grep "Activity" | field 2  | uniq
# }

# adb_app_main_page(){
# # adb -s $MYDEVICES shell dumpsys package ${1:-com.xiaomi.smarthome} | grep "Activity" | grep "/\." | field 2  | uniq | grep Main
# adb -s $MYDEVICES shell dumpsys package ${1:-com.xiaomi.smarthome} | grep "Activity" | field 2  | uniq | grep Main
# }

adb_now_app_page_view() {
    echo "显示当前页面的布局"
    cd ~/upload
    adb_light
    adb -s $MYDEVICES shell uiautomator dump /data/local/tmp/uidump.xml && adb pull /data/local/tmp/uidump.xml uidump.xml && scat uidump.xml | lcat
}

adb_now_app_page_view_json() {
    cd ~/upload
    adb_light
    adb -s $MYDEVICES shell uiautomator dump /data/local/tmp/uidump.xml && adb pull /data/local/tmp/uidump.xml uidump.xml && scat uidump.xml | xq .hierarchy
}

adb_open_app_page() {
    # adb -s $MYDEVICES shell am start -n  com.xiaomi.smarthome/.SmartHomeMainActivity
    # main_page=$(adb -s $MYDEVICES shell dumpsys package ${1:-com.xiaomi.smarthome} | grep "Activity" | grep "/\." | field 2  | uniq | grep Main)
    # main_page=$(adb -s $MYDEVICES shell dumpsys package ${1:-com.xiaomi.smarthome} | grep "Activity"  | field 2  | uniq | grep Main)
    cd ~/upload
    adb_light
    adb -s $MYDEVICES shell am start -n ${1:-com.xiaomi.smarthome/.SmartHomeMainActivity}
}

adb_close_app() {
    cd ~/upload
    adb_light
    adb -s $MYDEVICES shell am force-stop ${1:-com.xiaomi.smarthome}
}

adb_phone_name() {
    adb -s $MYDEVICES shell getprop ro.product.model
}

adb_auto_power() {
    echo "$(date +'%Y-%m-%d %H:%M:%S') 检查当前电量和充电状态"
    LIMIT=${1:-50} # 50电量🔋作为临界点
    if [ $(adb_battery | grep level | field 2) -lt $LIMIT ] && [ $(adb_battery | grep AC | field 3) = "false" ]; then
        echo "$(date +'%Y-%m-%d %H:%M:%S') 电量小于$LIMIT，且未在充电----需要充电"
        adb_code 224 # 点亮屏幕
        adb_code 3   # 回到桌面
        echo "打开米家app"
        sleep 6
        adb_open_app_page "com.xiaomi.smarthome/.SmartHomeMainActivity"
        sleep 6
        echo "打开开关"
        adb_click 650 650
        sleep 6
        echo "完成 关闭 米家 app"
        adb_close_app "com.xiaomi.smarthome"
        adb_code
        sddmd "$(adb_phone_name)" "电量小于$LIMIT,已开启充电🔋"
    elif [ $(adb_battery | grep level | field 2) -eq 100 ] && [ $(adb_battery | grep AC | field 3) = "true" ]; then
        echo "$(date +'%Y-%m-%d %H:%M:%S') 电量等于100，且还在充电----需要关闭电源"
        adb_code 224 # 点亮屏幕
        adb_code 3   # 回到桌面
        echo "打开米家app"
        adb_open_app_page "com.xiaomi.smarthome/.SmartHomeMainActivity"
        sleep 6
        echo "打开开关"
        adb_click 650 650
        sleep 6
        echo "完成 关闭 米家 app"
        adb_close_app "com.xiaomi.smarthome"
        adb_code
        sddmd "$(adb_phone_name)" "电池已充满,关闭充电完成✅"
    else
        echo "$(date +'%Y-%m-%d %H:%M:%S') 🔋$(adb_battery | grep level | field 2)，继续等待"
    fi
}

# 检查 MYDEVICES 是否 adb 在线
# 1 为 在线
# 0 为 离线
adb_on_line() {
    echo "$(adb devices | grep $MYDEVICES | wc -l | pass_black)"
}

adb_hold_on() {
    echo "检查连接状态"
    if [ $(adb_on_line) = 0 ]; then
        echo "$(date +'%Y-%m-%d %H:%M:%S') $MYDEVICES 已经脱机，需要立即重启 adb server "
        go_adb
    else
        echo "$(date +'%Y-%m-%d %H:%M:%S') $MYDEVICES online，wait"
    fi
}

adb_pi_auto_up() {
    echo "$(date +'%Y-%m-%d %H:%M:%S') 检查pi状态"
    if [ $(ip_dead 192.168.100.104) = 1 ]; then
        echo "$(date +'%Y-%m-%d %H:%M:%S') pi已经脱机，需要立即重启"
        adb_code 224 # 点亮屏幕
        adb_code 3   # 回到桌面
        echo "打开米家app"
        adb_open_app_page "com.xiaomi.smarthome/.SmartHomeMainActivity"
        sleep 6
        echo "关闭开关"
        adb_click 300 630
        sleep 6
        echo "打开开关"
        adb_click 300 630
        sleep 6
        echo "完成 关闭 米家 app"
        adb_close_app "com.xiaomi.smarthome"
        adb_code
        sddmd "Pi" "Pi电源重启完成✅"
    else
        echo "$(date +'%Y-%m-%d %H:%M:%S') pi在线，继续等待"
    fi
}

adb_tianyi_used() {
    echo "$(date +'%Y-%m-%d %H:%M:%S') 打开 点亮屏幕"
    adb_light
    sleep 2
    echo "$(date +'%Y-%m-%d %H:%M:%S') 打开 电信天翼 app"
    adb_open_app_page com.ct.client/.SplashActivity
    echo "$(date +'%Y-%m-%d %H:%M:%S') 打开 等待广告完成✅"
    sleep 10
    echo "$(date +'%Y-%m-%d %H:%M:%S') 点击流量页面"
    adb_click 100 480
    sleep 5
    echo "$(date +'%Y-%m-%d %H:%M:%S') 截图保存"
    adb_jsee
    sleep 2
    ocr_text $(cut_pic_point ~/upload/sc.png)
    sleep 2
    adb_code
}

# 需求开启点击登录 每日一次找到登陆的地方
adb_ssr_used() {
    # adb_clean_all
    adb_light
    echo "$(date +'%Y-%m-%d %H:%M:%S') 关闭浏览器"
    adb_close_app com.android.browser # 关闭浏览器
    echo "$(date +'%Y-%m-%d %H:%M:%S') 打开 点亮屏幕"
    sleep 2
    echo "$(date +'%Y-%m-%d %H:%M:%S') 打开 浏览器 app"
    adb_code 3 # 回到桌面首屏
    sleep 1
    adb_click 110 170 # 进入app
    sleep 7
    # adb_click 375 850
    adb_click 360 1000
    sleep 7
    echo "$(date +'%Y-%m-%d %H:%M:%S') 截图保存"
    adb_jsee
    sleep 2
    # msg=$(ocr_text  `cut_pic_point ~/upload/sc.png 180 635 590 1000`)
    msg=$(ocr_text ~/upload/sc.png)
    echo $msg
    used_ssr=$(echo $msg | keycut "已用流量:" ".")
    used_date=$(echo $msg | keycut "续费服务 " " 已用流量")
    today_D=$(date | field 3)
    end_D=$(echo $used_date | gformat | field 3)
    # real_date 是真实结算日期而非最后付款日期

    echo "向下翻页截取流量重置天数"
    adb_page_down
    adb_jsee
    sleep 2
    real_num=$(ocr_text $(cut_pic_point ~/upload/sc.png 255 800 356 857) 2 | cutint)

    real_date=$(echo "scale=2; $real_num * 86400 + $(nowts)" | bc)

    process=$(mrate $(nt2ts $real_date) $used_ssr 200)
    echo "消耗时间进度为 $process %"
    echo "下次续费日期为 $used_date"

    [[ $process -gt 100 ]] && sddmd "🚀ssr流量预警" "消耗时间进度为 $process %\n下次续费日期为 $used_date"

    sleep 2
    adb_code
}

# 微信获取短信
adb_green_bud() {
    adb_clean_all
    echo "$(date +'%Y-%m-%d %H:%M:%S') 打开 点亮屏幕"
    adb_light
    sleep 2
    echo "$(date +'%Y-%m-%d %H:%M:%S') 打开  微信"
    adb_open_app_page "com.tencent.mm/com.tencent.mm.ui.LauncherUI"
    sleep 10
    echo "$(date +'%Y-%m-%d %H:%M:%S') 打开  绿芽公众号"
    adb_click 264 236
    sleep 10
    echo "$(date +'%Y-%m-%d %H:%M:%S') 点击打开每日签到"
    adb_click 617 1463
    sleep 10
    echo "$(date +'%Y-%m-%d %H:%M:%S') 点击签到小程序"
    adb_click 356 1000
    sleep 10
    echo "$(date +'%Y-%m-%d %H:%M:%S') 点击第一次抽奖"
    adb_click 360 1077
    sleep 10
    echo "$(date +'%Y-%m-%d %H:%M:%S') 点击再抽一次"
    adb_click 338 908
    sleep 20 # 广告时间
    echo "$(date +'%Y-%m-%d %H:%M:%S') 点击返回"
    adb_click 53 115
    sleep 2
    echo "$(date +'%Y-%m-%d %H:%M:%S') 点击退出小程序"
    adb_click 665 117
    sleep 2
    echo "$(date +'%Y-%m-%d %H:%M:%S') 关闭微信"
    adb_close_app com.tencent.mm
    sleep 2
    echo "$(date +'%Y-%m-%d %H:%M:%S') 关闭屏幕"
    adb_code
    sleep 2
}

adb_job_6hour() {
    sleep 60
    adb_ssr_used
    adb_green_bud
}

# 微信接口

# 解决adbd cannot run as root in production builds问题
# 1，验证你的手机是否已经root了

# adb shell

# su

# 行命令后，$ 变为 # 即 表示root 成功

# 2，安装adbd-insecure.apk

# adb install adbd-insecure.apk

# 3，设置

# 打开应用将Enable insecure adbd 和 enable at boot 勾选上，设置好之后重进键入：adb root即可

# adb -s SH0A6PL00243 pull data/data/com.android.tencent/databases/AgendaDetails.db f:\test

# 3、将执行日志输出到电脑的某个位置

# adb shell monkey -p com.taobao.taobao -v 1000 >E:/test.txt

adb_key_word_point() {
    cd ~/upload
    adb_light
    KEY_WORD=${1:-三星}
    POINT_RANGE=$(adb_now_app_page_view_json | grep -A 16 -B 2 $KEY_WORD | sed 's/},/}/g' | jq '.["@bounds"]' | pass_quotes | awk '{ gsub(/\[/,"\t"); print $0 }' | awk '{ gsub(/\]/,"\t"); print $0 }' | awk '{ gsub(/,/,"\t"); print $0 }')
    # echo $POINT_RANGE
    POINT_X=$(echo "($(echo $POINT_RANGE | field 3)+$(echo $POINT_RANGE | field 1))/2" | bc)
    POINT_Y=$(echo "($(echo $POINT_RANGE | field 4)+$(echo $POINT_RANGE | field 2))/2" | bc)
    # echo "$POINT_X $POINT_Y"
    adb_click "$POINT_X $POINT_Y"
}

adb_click_center_precent() {
    echo "点击中轴百分比"
    PRECENT=${1:-30}
    width=$(adb shell wm size | cutnum | line 1 1)
    height=$(adb shell wm size | cutnum | line 2 2)
    POINT_X=$(echo "$width/2" | bc)
    POINT_Y=$(echo "$height/100*$PRECENT" | bc)
    adb_click "$POINT_X $POINT_Y"

}
