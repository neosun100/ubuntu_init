# rich 超级模块

# rich wget
# 多文件批量下载 进度条
nget(){
python3 ~/upload/ubuntu_init/pyScript/rich_downloader.py $*
}

# 过tsocks的下载
ntget(){
tsocks python3 ~/upload/ubuntu_init/pyScript/rich_downloader.py $*
}

# shell md
nmd(){
python3 ~/upload/ubuntu_init/pyScript/rich_md.py  --md ${1:-~/upload/ubuntu_init/README.md}
}

# 文本高亮
nat(){
python3 ~/upload/ubuntu_init/pyScript/rich_highlighting.py --file ${1:-~/upload/ubuntu_init/pyScript/rich_highlighting.py} --num=${2:-0}
}