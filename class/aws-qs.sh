################################ 以 ⬇️ 为账户相关 #################################
################################ 以 ⬇️ 为账户相关 #################################
################################ 以 ⬇️ 为账户相关 #################################
ACCOUNT_ID=${1:-835751346093}





qs-list-namespace() {
    aws quicksight list-namespaces --aws-account-id $ACCOUNT_ID | jq '.Namespaces' | md-table | lcat
}

qs-list-analyses() {
    aws quicksight list-analyses --aws-account-id $ACCOUNT_ID | jq '.Namespaces' | md-table | lcat
}

qs-list-groups() {
    NAMESPACE=${2:-default}
    aws quicksight list-groups --aws-account-id $ACCOUNT_ID --namespace $NAMESPACE | jq '.GroupList' | md-table | lcat
}

qs-list-users() {
    NAMESPACE=${2:-default}
    aws quicksight list-users --aws-account-id $ACCOUNT_ID --namespace $NAMESPACE | jq '.UserList' | md-table | lcat
}

################################ 以 ⬇️ 为内容相关 #################################
################################ 以 ⬇️ 为内容相关 #################################
################################ 以 ⬇️ 为内容相关 #################################

qs-list-data-sources() {
    aws quicksight list-data-sources --aws-account-id $ACCOUNT_ID |
        jq '[.DataSources[] | {Arn: .Arn, Name: .Name, Type: .Type, Status: .Status, CreatedTime : .CreatedTime , LastUpdatedTime: .LastUpdatedTime}]' | md-table | lcat
}

# for data-set
qs-list-ingestions() {
    NAMESPACE=${2:-default}
    aws quicksight list-ingestions --aws-account-id $ACCOUNT_ID --namespace $NAMESPACE | jq '.UserList' | md-table | lcat
}

qs-list-data-sets() {
    aws quicksight list-data-sets --aws-account-id $ACCOUNT_ID |
        jq '[.DataSetSummaries[] | {Arn: .Arn, Name: .Name, CreatedTime : .CreatedTime , LastUpdatedTime: .LastUpdatedTime,ImportMode: .ImportMode, ColRules: .ColumnLevelPermissionRulesApplied}]' | md-table | lcat
}

qs-list-analyses() {
    aws quicksight list-analyses --aws-account-id $ACCOUNT_ID |
        jq '[.AnalysisSummaryList[] | {Arn: .Arn, Name: .Name, Status: .Status, CreatedTime : .CreatedTime , LastUpdatedTime: .LastUpdatedTime}]' | md-table | lcat
}

qs-list-templates() {
    NAMESPACE=${2:-default}
    aws quicksight list-templates --aws-account-id $ACCOUNT_ID --namespace $NAMESPACE | jq '.UserList' | md-table | lcat
}

qs-list-dashboards() {
    aws quicksight list-dashboards --aws-account-id $ACCOUNT_ID |
        jq '[.DashboardSummaryList[] | {Arn: .Arn, Name: .Name, CreatedTime : .CreatedTime , LastUpdatedTime: .LastUpdatedTime, Version: .PublishedVersionNumber, LastPublishedTime:.LastPublishedTime}]' | md-table | lcat
}

## describe
qs-describe-data-source() {
    DATA_SOURCE_ID=${1:-39369a4e-5dda-4a0a-ab10-3f05ba92d1c2}
    aws quicksight describe-data-source --aws-account-id $ACCOUNT_ID --data-source-id $DATA_SOURCE_ID | jq '.DataSource' | jcat
}

## create

qs-create-namespace() {
    NAMESPACE=${2:-HILTON}
    aws quicksight create-namespace --aws-account-id $ACCOUNT_ID --namespace $NAMESPACE --identity-store QUICKSIGHT | jcat
}

## delete

qs-delete-namespace() {
    NAMESPACE=${2:-HILTON}
    aws quicksight delete-namespace --aws-account-id $ACCOUNT_ID --namespace $NAMESPACE | jcat
}
