# http://maxwells-daemon.io/quickstart

install_maxwell(){
brew install maxwell
}


# maxwell 前置条件 需要配置 mysql
maxwell_mysql_pre_condition(){
MYSQL_URL=${1:-root:LOOP2themoon@192.168.100.142:3306}
echo "登录 mysql 执行如下语句" | lcat
echo 'set global binlog_format=ROW;' | sqlcat
echo 'set global binlog_row_image=FULL;' | sqlcat
echo "CREATE USER 'maxwell'@'%' IDENTIFIED BY 'AWS2themoon';" | sqlcat
echo "GRANT ALL ON maxwell.* TO 'maxwell'@'%';" | sqlcat
echo "GRANT SELECT, REPLICATION CLIENT, REPLICATION SLAVE ON *.* TO 'maxwell'@'%';" | sqlcat
mycli mysql://$MYSQL_URL \
-e "set global binlog_format=ROW; set global binlog_row_image=FULL; CREATE USER 'maxwell'@'%' IDENTIFIED BY 'AWS2themoon'; GRANT ALL ON maxwell.* TO 'maxwell'@'%'; GRANT SELECT, REPLICATION CLIENT, REPLICATION SLAVE ON *.* TO 'maxwell'@'%'; " \
-t 
}





maxwell_stdout(){
url=${1:-maxwell:AWS2themoon@192.168.100.142:3306}
USER=`echo $url | awk '{ gsub(/:/,"\t"); print $0 }' | awk '{ gsub(/@/,"\t"); print $0 }' | field 1`
PASSWORD=`echo $url | awk '{ gsub(/:/,"\t"); print $0 }' | awk '{ gsub(/@/,"\t"); print $0 }' | field 2`
myHOST=`echo $url | awk '{ gsub(/:/,"\t"); print $0 }' | awk '{ gsub(/@/,"\t"); print $0 }' | field 3`
PORT=`echo $url | awk '{ gsub(/:/,"\t"); print $0 }' | awk '{ gsub(/@/,"\t"); print $0 }' | field 4`

maxwell --user=$USER --password=$PASSWORD --host=$myHOST --port=$PORT --producer=stdout | jcat 
}