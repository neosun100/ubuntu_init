
# oneDrive uploader help
opan_help(){
nmd ~/upload/ubuntu_init/file/OneDriveUploader_help.md
}

#  onedrive uploader
install_opan(){
echo "https://github.com/MoeClub/OneList/tree/master/OneDriveUploader"
echo "https://github.com/MoeClub/OneList/tree/master/Rewrite"

if [ `uname -m` = "armv7l" ] && [ `uname` = "Linux" ]; then
  echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为arm Linux" | lcat
  typeset url="http://file.neo.pub/linux/arm/OneDriveUploader"
#   typeset sysname="_linux_arm"
  # 执行代码块

elif [ `uname -m` = "aarch64" ] && [ `uname` = "Linux" ]; then
  echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为arm64 Linux" | lcat
  typeset url="http://file.neo.pub/linux/arm/OneDriveUploader"

elif [ `uname -m` = "x86_64" ] && [ `uname` = "Linux" ]; then
  echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为X86 Linux" | lcat
  typeset url="http://file.neo.pub/linux/x86/OneDriveUploader"
#   typeset sysname="_linux_amd64"
  # 执行代码块
elif [ `uname -m` = "x86_64" ] && [ `uname` = "Darwin" ]; then
  echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为X86 Mac" | lcat
  typeset url="http://file.neo.pub/mac/OneDriveUploader"
#   typeset sysname="_darwin_amd64"
  # 执行代码块
else
  echo "`uname` 系统未知！"
#   typeset url="http://file.neo.pub/java/jar/tiv.jar"
fi

echo "`date +'%Y-%m-%d %H:%M:%S'` 判断是否已经存在OneDriveUploader"


if [ -x ~/upload/bin/OneDrive/OneDriveUploader ] ; then 
echo "`date +'%Y-%m-%d %H:%M:%S'` OneDriveUploader 已存在"
else
echo "`date +'%Y-%m-%d %H:%M:%S'` OneDriveUploader 不存在" 
mkdir -p ~/upload/bin/OneDrive
cd ~/upload/bin/OneDrive && wget $url && cp ~/upload/ubuntu_init/file/auth.json ~/upload/bin/OneDrive/auth.json
cp ~/upload/ubuntu_init/
sudo chmod -R 777 ~/upload/bin/OneDrive
~/upload/bin/OneDrive/OneDriveUploader -h
fi # 判断结束，以fi结尾

opan_help;
cd ~/upload
}



# 打开本地onedrive 网盘 root
opan(){
ncdu /Users/neo/neopubDrive/OneDrive\ -\ 尼奥未来 
}

opan_up(){
~/upload/bin/OneDrive/OneDriveUploader -c ~/upload/bin/OneDrive/auth.json $*
}