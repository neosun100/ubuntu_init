
cd ~/upload;

# 从 FreeSSL---ACME管理 后台获取安装命令：
curl https://freessl.cn/api/get.acme.sh?token=91590e77-98d3-49e0-9bcf-40aca8a9d300 | sh

# 注册客户端
# 想要使用该脚本必须注册一个 ACME客户端，这是 ACME 协议所规定的。这里必须指定一个你在 FreeSSL 注册的邮箱，这样才能与 FreeSSL Web 端相关联。
/home/neo/.acme.sh/acme.sh  \
--register-account \
--accountemail yiqidangqian@foxmail.com \
--server https://acme.freessl.cn/directory



# http://s.tool.chinaz.com/https?url=neo.pub
# 升级 网站 HTTPS 安全评级服务
# 一、B 升 A
# /etc/letsencrypt 目录下查看是否有：ssl-dhparams.pem 文件
# 如果有则直接设置 nginx 配置：ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;
cd /etc/ssl/certs
sudo openssl dhparam -out dhparam.pem 4096
# 生成该参数十分消耗 CPU 资源，在线上服务执行一定要注意！

# 修改 nginx 配置


server {
    listen 443 ssl;
    server_name neo.pub www.neo.pub blog.neo.pub portainer.neo.pub sgin.neo.pub;
    add_header Strict-Transport-Security "max-age=63072000; includeSubdomains; preload";
    .....
}
server {
   listen 80;
   server_name www.hi-linux.com;
   return 301 https://www.hi-linux.com$request_uri;
...
}

service nginx restart
