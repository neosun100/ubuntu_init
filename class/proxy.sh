# 代理相关

install_mtproxy() {
  echo "https://github.com/neosun100/MTProxy-Bash"
  echo "MTProxy 一键搭建管理脚本。 MTProxy 是专门用于 Telegram 的代理软件。目前 Telegram 内部直接支持此代理软件。安装完成后生成的 tg:// 链接可在 Telegram 内随意分享使用。代理效果非常出色。"
  wget -N --no-check-certificate https://raw.githubusercontent.com/FunctionClub/MTProxy-Bash/master/install.sh && sudo bash install.sh
}

# 输出最合适的代理ip
# mac 127.0.0.1:1087
# home 192.168.2.12:8181
# 其他 122.51.108.4:8181
best_proxy() {
  [[ $(hostname) = 3c22fb80a423.ant.amazon.com ]] && echo 127.0.0.1:7890 ||
    ([[ $(iip | tr '.' ' ' | field 3) = 2 ]] && echo 192.168.2.19:18181 || echo 122.51.108.4:8181)
}

# 检测代理有效性
check_proxy() {
  echo "check ${1:-192.168.2.19} socks5 proxy"
  curl --connect-timeout 7 \
    --max-time 10 \
    --location \
    --silent \
    --proxy socks5://${1:-192.168.2.19}:12080 \
    new.nginxs.net/ip.php
  echo ""
  echo "check ${1:-192.168.2.19} http proxy"
  curl --connect-timeout 7 \
    --max-time 10 \
    --location \
    --silent \
    --proxy http://${1:-192.168.2.19}:18181 \
    new.nginxs.net/ip.php
}

# 代理调度器
# 神器

install_privoxy() {
  echo "强大的代理调度器代理Privoxy"

  if [ $(uname -m) = "armv7l" ] && [ $(uname) = "Linux" ]; then
    echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为arm Linux" | lcat
    sudo apt-get install privoxy
    #   typeset sysname="_linux_arm"
    # 执行代码块
  elif [ $(uname -m) = "aarch64" ] && [ $(uname) = "Linux" ]; then
    echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为arm64 Linux" | lcat
    sudo apt-get install privoxy

  elif [ $(uname -m) = "x86_64" ] && [ $(uname) = "Linux" ]; then
    echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为X86 Linux" | lcat
    sudo apt-get install privoxy
    #   typeset sysname="_linux_amd64"
    # 执行代码块
  elif [ $(uname -m) = "x86_64" ] && [ $(uname) = "Darwin" ]; then
    echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为X86 Mac" | lcat
    brew install privoxy
    #   typeset sysname="_darwin_amd64"
    # 执行代码块
  else
    echo "$(uname) 系统未知！"
  fi

  echo "privoxy教程📖"
  echo "https://www.cnblogs.com/hongdada/p/10787924.html"
}

