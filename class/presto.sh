install_presto-cli() {
    cd upload
    wget https://repo.maven.apache.org/maven2/io/prestosql/presto-cli/350/presto-cli-350-executable.jar
    mkdir -p ~/upload/bin
    mv presto-cli-350-executable.jar ~/upload/bin/presto-cli
    sudo chmod 777 ~/upload/bin/presto-cli
    ~/upload/bin/presto-cli --help
    echo "https://docs.starburst.io/latest/installation/cli.html"
}

# 创建presto 账户与密码
bcrypt_passwd() {
    htpasswd --help
    cd ~/upload
    touch password.db
    htpasswd -B -C 10 password.db root
    scat ~/upload/password.db
}
