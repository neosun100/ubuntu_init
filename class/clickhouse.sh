install_chcli() {
  echo "安装clickhouse client 高亮客户端 chcli"
  echo y | conda create --name chcli python=3.8 &&
    source activate chcli &&
    pip install git+https://github.com/long2ice/chcli &&
    pip install antlr4-python3-runtime==4.8 &&
    conda deactivate
  ~/anaconda3/envs/chcli/bin/chcli --help
}

chcli() {
  ~/anaconda3/envs/chcli/bin/chcli $*
}

chcli_url() {
  url=${1:-"root:AWS2themoon@192.168.100.142:9900"}
  USER=$(echo $url | awk '{ gsub(/:/,"\t"); print $0 }' | awk '{ gsub(/@/,"\t"); print $0 }' | field 1)
  PASSWORD=$(echo $url | awk '{ gsub(/:/,"\t"); print $0 }' | awk '{ gsub(/@/,"\t"); print $0 }' | field 2)
  myHOST=$(echo $url | awk '{ gsub(/:/,"\t"); print $0 }' | awk '{ gsub(/@/,"\t"); print $0 }' | field 3)
  PORT=$(echo $url | awk '{ gsub(/:/,"\t"); print $0 }' | awk '{ gsub(/@/,"\t"); print $0 }' | field 4)

  chcli --host $myHOST \
    --port $PORT \
    --user $USER \
    --password $PASSWORD

}

install_mac_clickhouse_client() {
  cd ~/upload
  tsocks curl -O 'https://builds.clickhouse.tech/master/macos/clickhouse' && chmod a+x ./clickhouse
  mv -i clickhouse ~/upload/bin/
  l ~/upload/bin
}

install_linux_clickhouse_client() {
  sudo apt-get install -y clickhouse-client
}

install_clickhouse_client() {
  if [ $(file /bin/bash | field 7) = "ARM" ] && [ $(uname) = "Linux" ]; then
    echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为arm Linux"
    echo "$(date +'%Y-%m-%d %H:%M:%S') 没有开发 ⌛️"

  elif [ $(uname -m) = "x86_64" ] && [ $(uname) = "Linux" ]; then
    echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为X86 Linux"
    install_linux_clickhouse_client

  elif [ $(uname -m) = "x86_64" ] && [ $(uname) = "Darwin" ]; then
    echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为X86 Mac"
    install_mac_clickhouse_client

  else
    echo "$(uname) 系统未知！"
  fi
}

fix_install_clickhouse() {
  sudo apt-get install apt-transport-https ca-certificates dirmngr
  sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv E0C56BD4

  echo "deb https://repo.clickhouse.tech/deb/stable/ main/" | sudo tee \
    /etc/apt/sources.list.d/clickhouse.list
  sudo apt-get update

  sudo apt-get install -y clickhouse-client # clickhouse-server

  # sudo service clickhouse-server start
  clickhouse-client --version
}

ck() {
  if [ $(file /bin/bash | field 7) = "ARM" ] && [ $(uname) = "Linux" ]; then
    echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为arm Linux"
    echo "$(date +'%Y-%m-%d %H:%M:%S') 没有开发 ⌛️"

  elif [ $(uname -m) = "x86_64" ] && [ $(uname) = "Linux" ]; then
    clickhouse $*

  elif [ $(uname -m) = "x86_64" ] && [ $(uname) = "Darwin" ]; then
    ~/upload/bin/clickhouse $*

  else
    echo "$(uname) 系统未知！"
  fi

}

ck-github() {
  ck client -m -h github.demo.trial.altinity.cloud --port 9440 -s --user=demo --password=demo $*
}

ck-archon() {
  url="root:AWS2themoon@192.168.100.142:9900"
  USER=$(echo $url | awk '{ gsub(/:/,"\t"); print $0 }' | awk '{ gsub(/@/,"\t"); print $0 }' | field 1)
  PASSWORD=$(echo $url | awk '{ gsub(/:/,"\t"); print $0 }' | awk '{ gsub(/@/,"\t"); print $0 }' | field 2)
  myHOST=$(echo $url | awk '{ gsub(/:/,"\t"); print $0 }' | awk '{ gsub(/@/,"\t"); print $0 }' | field 3)
  PORT=$(echo $url | awk '{ gsub(/:/,"\t"); print $0 }' | awk '{ gsub(/@/,"\t"); print $0 }' | field 4)

  ck client --host $myHOST \
    --port $PORT \
    --user $USER \
    --password $PASSWORD $*
}

ck-client() {
  url=${1:-"root:AWS2themoon@192.168.100.142:9900"}
  USER=$(echo $url | awk '{ gsub(/:/,"\t"); print $0 }' | awk '{ gsub(/@/,"\t"); print $0 }' | field 1)
  PASSWORD=$(echo $url | awk '{ gsub(/:/,"\t"); print $0 }' | awk '{ gsub(/@/,"\t"); print $0 }' | field 2)
  myHOST=$(echo $url | awk '{ gsub(/:/,"\t"); print $0 }' | awk '{ gsub(/@/,"\t"); print $0 }' | field 3)
  PORT=$(echo $url | awk '{ gsub(/:/,"\t"); print $0 }' | awk '{ gsub(/@/,"\t"); print $0 }' | field 4)

  ck client --host $myHOST \
    --port $PORT \
    --user $USER \
    --password $PASSWORD

}

ck-archon_query() {
  url="root:AWS2themoon@192.168.100.142:9900"
  USER=$(echo $url | awk '{ gsub(/:/,"\t"); print $0 }' | awk '{ gsub(/@/,"\t"); print $0 }' | field 1)
  PASSWORD=$(echo $url | awk '{ gsub(/:/,"\t"); print $0 }' | awk '{ gsub(/@/,"\t"); print $0 }' | field 2)
  myHOST=$(echo $url | awk '{ gsub(/:/,"\t"); print $0 }' | awk '{ gsub(/@/,"\t"); print $0 }' | field 3)
  PORT=$(echo $url | awk '{ gsub(/:/,"\t"); print $0 }' | awk '{ gsub(/@/,"\t"); print $0 }' | field 4)

  ck client --host $myHOST \
    --port $PORT \
    --user $USER \
    --password $PASSWORD \
    --multiquery \
    --time \
    --query $* | lcat
}

ck_query() {
  url=${1:-"root:AWS2themoon@192.168.100.142:9900"}
  QUERY=${2:-"show databases;"}
  FORMAT=${3:-Vertical} # Vertical JSON CSVWithNames CSV

  USER=$(echo $url | awk '{ gsub(/:/,"\t"); print $0 }' | awk '{ gsub(/@/,"\t"); print $0 }' | field 1)
  PASSWORD=$(echo $url | awk '{ gsub(/:/,"\t"); print $0 }' | awk '{ gsub(/@/,"\t"); print $0 }' | field 2)
  myHOST=$(echo $url | awk '{ gsub(/:/,"\t"); print $0 }' | awk '{ gsub(/@/,"\t"); print $0 }' | field 3)
  PORT=$(echo $url | awk '{ gsub(/:/,"\t"); print $0 }' | awk '{ gsub(/@/,"\t"); print $0 }' | field 4)

  ck client --host $myHOST \
    --port $PORT \
    --user $USER \
    --password $PASSWORD \
    --multiquery \
    --time \
    --stacktrace \
    --format=$FORMAT \
    --query $QUERY | lat

}

ck_pre_drop() {
  echo "clickhouse 删除表失败时，在之前执行"
  CONTAINER=${1:-clickhouse-server}
  nexec ${CONTAINER} touch /var/lib/clickhouse/flags/force_drop_table
  nexec ${CONTAINER} chmod 666 /var/lib/clickhouse/flags/force_drop_table
  nexec ${CONTAINER} ls /var/lib/clickhouse/flags/force_drop_table
  echo "clickhouse 删除前置完成✅ 执行 drop table if exists DB.table 后 文件消失"
}