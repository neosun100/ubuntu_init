# aws-ec2命令大全

## 创建实例

# aws ec2 run-instances \
# --image-id ami-0d5eff06f840b45e9 \
# --count 1 \
# --instance-type m5.xlarge \
# --key-name visit-virginia-key \
# --region us-east-1

create_emr_gateway() {
    # 输入一个需要创建endpoint的emr集群name
    EMR_CLUSTER_NAME=${1:-emr-6.5-tesla}
    echo "$(date +'%Y-%m-%d %H:%M:%S') 1. 获取EMR 相关信息"

    echo "EMR_CLUSTER_NAME:\t $EMR_CLUSTER_NAME"
    EMR_CLUSTER_ID=$(aws emr list-clusters --active | jq ".Clusters[] | select(.Name == \"$EMR_CLUSTER_NAME\") | .Id" | line 1 1 | pass_quotes)
    echo "EMR_CLUSTER_ID:\t $EMR_CLUSTER_ID"
    EMR_MASTER_ID=$(aws emr list-instances --cluster-id $EMR_CLUSTER_ID --instance-group-types MASTER | jq '.Instances[].Ec2InstanceId' | line 1 1 | pass_quotes)
    echo "EMR_MASTER_ID:\t $EMR_MASTER_ID"
    EMR_KEY_NAME=$(aws ec2 describe-key-pairs | jq '.KeyPairs[0].KeyName' | pass_quotes)

    echo "$(date +'%Y-%m-%d %H:%M:%S') 2. 创建EMR Master AMI"
    EMR_MASTER_AMI=$(aws ec2 describe-images --filters "Name=name,Values=$EMR_CLUSTER_NAME-AMI" | jq '.Images[].ImageId' | pass_quotes)

    if [ -z $EMR_MASTER_AMI ]; then
        echo "AMI不存在，重新创建"
        EMR_MASTER_AMI=$(aws ec2 create-image --no-dry-run --instance-id $EMR_MASTER_ID --name $EMR_CLUSTER_NAME-AMI | jq '.ImageId' | pass_quotes)
        time_wait 10
        echo "EMR_MASTER_AMI:\t $EMR_MASTER_AMI"
    else
        echo "EMR_MASTER_AMI:\t $EMR_MASTER_AMI"
    fi

    echo "$(date +'%Y-%m-%d %H:%M:%S') 3. 创建 EMR $EMR_CLUSTER_NAME Endpoint"
    aws ec2 run-instances \
        --image-id $EMR_MASTER_AMI \
        --count 1 \
        --instance-type m5.xlarge \
        --subnet-id subnet-019c68e2f66977e0e \
        --security-group-ids sg-0708b2efbc03b7d46 \
        --iam-instance-profile Name="EMR_EC2_DefaultRole" \
        --tag-specifications "ResourceType=instance,Tags=[{Key=Name,Value=$EMR_CLUSTER_NAME-gateway}]" \
        --key-name $EMR_KEY_NAME \
        --region us-east-1

    echo "EMR $EMR_CLUSTER_NAME Endpoint:\t "
    time_wait 15
    echo "$(date +'%Y-%m-%d %H:%M:%S') 4. 显示 $EMR_CLUSTER_NAME Endpoint"
    aws ec2 describe-instances \
        --filters "Name=instance-state-name,Values=running" \
        --filters "Name=tag:Name,Values=$EMR_CLUSTER_NAME-gateway" \
        --query "Reservations[].Instances[].[Tags[?Value=='Name'] |[0].Tags.Value,InstanceId,State.Name,InstanceType,Placement.AvailabilityZone,"PublicIpAddress","PublicDnsName"]" --output table | lcat

}

terminate_emr_gateway() {
    EMR_CLUSTER_NAME=${1:-emr-6.5-tesla}
    aws ec2 terminate-instances --instance-ids $(aws ec2 describe-instances --filters "Name=instance-state-name,Values=running" --filters "Name=tag:Name,Values=$EMR_CLUSTER_NAME-gateway" | jq '.Reservations[].Instances[].InstanceId' | pass_quotes)

}


start_ssa_gpu() {
    INSTANCE_ID=${1:-i-052727bcc1e0af980}
    aws_set_global_ssa
    aws ec2 start-instances --instance-ids $INSTANCE_ID | jq
    echo "$(date '+%Y-%m-%d %H:%M:%S') ⚡️ Starting EC2 instance $INSTANCE_ID..."
    while true; do
        ec2_status=$(aws ec2 describe-instances --instance-ids $INSTANCE_ID --query 'Reservations[0].Instances[0].State.Name' --output text)
        if [ $ec2_status = "running" ]; then
            echo "$(date '+%Y-%m-%d %H:%M:%S') ✅ EC2 instance $INSTANCE_ID is running"
            break
        else
            echo "$(date '+%Y-%m-%d %H:%M:%S') ⏳ EC2 instance $INSTANCE_ID is not running, waiting for 5 seconds..."
            sleep 5
        fi
    done
    aws_set_virginia
}



stop_ssa_gpu() {
    INSTANCE_ID=${1:-i-052727bcc1e0af980}
    aws_set_global_ssa
    aws ec2 stop-instances --instance-ids $INSTANCE_ID | jq
    echo "$(date '+%Y-%m-%d %H:%M:%S') 🛑 Stopping EC2 instance $INSTANCE_ID..."
    while true; do
        ec2_status=$(aws ec2 describe-instances --instance-ids $INSTANCE_ID --query 'Reservations[0].Instances[0].State.Name' --output text)
        if [ $ec2_status = "stopped" ]; then
            echo "$(date '+%Y-%m-%d %H:%M:%S') ✅ EC2 instance $INSTANCE_ID is stopped"
            break
        else
            echo "$(date '+%Y-%m-%d %H:%M:%S') ⏳ EC2 instance $INSTANCE_ID is not stopped, waiting for 5 seconds..."
            sleep 5
        fi
    done
    aws_set_virginia
}


start_ssa_cpu(){
    start_ssa_gpu i-0a82ab167aec52c1b
}


stop_ssa_cpu(){
    stop_ssa_gpu i-0a82ab167aec52c1b
}