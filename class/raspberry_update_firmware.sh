#!/bin/sh -e

update_firmware(){
    echo "固件升级需10分钟";
    echo y | sudo rpi-update;
    echo y | sudo apt dist-upgrade;
}


# 钉钉消息
# sdd "标题" "内容"
sddmd(){
  curl --request POST \
       --url "https://oapi.dingtalk.com/robot/send?access_token=813a36738938895e634ee516186fc0f8baec01d7792da5030dcc2a904bdc501b" \
       --header "content-type: application/json" \
       --data "{
            'msgtype': 'markdown',
            'markdown': {'title':'预警',
       'text':'# $1 \n> `date +"%Y-%m-%d %H:%M:%S"`  \n### `hostname`\n- $2'
            },
           'at': {
               'atMobiles': [
                   '1825718XXXX'
               ],
               'isAtAll': true
           }
        }"
}

echo "固件升级需10分钟";
echo y | sudo rpi-update;
if [$? -ne 0 ];then
    sddmd "树莓派pi4固件升级" "`date +'%Y-%m-%d %H:%M:%S'` 固件升级失败"
else
    sddmd "树莓派pi4固件升级" "`date +'%Y-%m-%d %H:%M:%S'` 固件升级成功"
fi;

echo y | sudo apt dist-upgrade;
if [$? -ne 0 ];then
    sddmd "树莓派pi4固件升级" "`date +'%Y-%m-%d %H:%M:%S'` dist-upgrade 失败"
else
    sddmd "树莓派pi4固件升级" "`date +'%Y-%m-%d %H:%M:%S'` dist-upgrade 成功"
fi;