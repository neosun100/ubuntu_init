install_convert_pakage() {
    echo "rmp 2 deb "
    echo "The conversion may not be successful."
    sudo add-apt-repository universe
    sudo apt-get update
    sudo apt-get install alien -y
}

install_rsql_latest() {
    echo "https://docs.aws.amazon.com/zh_cn/redshift/latest/mgmt/rsql-query-tool.html"
    if [ $(uname -m) = "armv7l" ] && [ $(uname) = "Linux" ]; then
        echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为arm Linux"
        [[ $(lsb_release -a | grep ID | awk "{print \$${1:-3}}") = "Ubuntu" ]] &&
            (echo Ubuntu && sudo apt install unixodbc openssl -y) ||
            (echo 非Ubuntu && sudo yum install unixODBC openssl -y)

        wget -N -t 0 -c -P /tmp/ https://s3.amazonaws.com/redshift-downloads/amazon-redshift-rsql/1.0.4/AmazonRedshiftRsql-1.0.4-1.x86_64.rpm
        [[ $(lsb_release -a | grep ID | awk "{print \$${1:-3}}") = "Ubuntu" ]] &&
            (echo Ubuntu && install_convert_pakage && ) ||
            (echo 非Ubuntu &&  sudo rpm -i /tmp/AmazonRedshiftRsql-1.0.4-1.x86_64.rpm)
       
    elif [ $(uname -m) = "x86_64" ] && [ $(uname) = "Linux" ]; then
        echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为X86 Linux"
        [[ $(lsb_release -a | grep ID | awk "{print \$${1:-3}}") = "Ubuntu" ]] &&
            (echo Ubuntu && sudo apt install unixodbc openssl -y) ||
            (echo 非Ubuntu && sudo yum install unixODBC openssl -y)

        wget -N -t 0 -c -P /tmp/ https://s3.amazonaws.com/redshift-downloads/amazon-redshift-rsql/1.0.4/AmazonRedshiftRsql-1.0.4-1.x86_64.rpm
        sudo rpm -i /tmp/AmazonRedshiftRsql-1.0.4-1.x86_64.rpm
    elif [ $(uname -m) = "x86_64" ] && [ $(uname) = "Darwin" ]; then
        echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为X86 Mac"
        # typeset subfile="_darwin_amd64.tar.gz"
        #   typeset sysname="_darwin_amd64"
        # 执行代码块
        brew install unixodbc openssl --build-from-source
        echo "https://docs.aws.amazon.com/zh_cn/redshift/latest/mgmt/configure-odbc-connection.html#install-odbc-driver-mac"
        wget -N -t 0 -c -P /tmp/ https://s3.amazonaws.com/redshift-downloads/drivers/odbc/1.4.53.1000/AmazonRedshiftODBC-1.4.53.1000.dmg
        echo "cli 安装 ODBC dmg"
        # https://www.likecs.com/show-397710.html
        hdiutil attach /tmp/AmazonRedshiftODBC-1.4.53.1000.dmg
        echo "双击 AmazonRedshiftODBC.dmg 以挂载磁盘映像。"
        echo "双击 AmazonRedshiftODBC.pkg 以运行安装程序。"

        echo "安装 rsql client"

        wget -N -t 0 -c -P /tmp/ https://s3.amazonaws.com/redshift-downloads/amazon-redshift-rsql/1.0.4/AmazonRedshiftRsql-1.0.4.dmg
        hdiutil attach /tmp/AmazonRedshiftRsql-1.0.4.dmg
        echo "init 客户端配置"
        nat ~/.odbc.ini
    else
        echo "$(uname) 系统未知！"
        echo "https://s3.amazonaws.com/redshift-downloads/amazon-redshift-rsql/1.0.4/AmazonRedshiftRsql-1.0.4.msi"
    fi
}

init_rsql() {
    if [ $(uname -m) = "armv7l" ] && [ $(uname) = "Linux" ]; then
        echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为arm Linux" 
        # cp -f /opt/amazon/redshiftodbc/Setup/odbc.ini ~/.odbc.ini
        export ODBCINI=~/.odbc.ini
        export ODBCSYSINI=/opt/amazon/redshiftodbc/Setup
        export AMAZONREDSHIFTODBCINI=/opt/amazon/redshiftodbc/lib/64/amazon.redshiftodbc.ini
        #   typeset sysname="_linux_arm"
        # 执行代码块
    elif [ $(uname -m) = "x86_64" ] && [ $(uname) = "Linux" ]; then
        echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为X86 Linux" 
        # cp -f /opt/amazon/redshiftodbc/Setup/odbc.ini ~/.odbc.ini
        export ODBCINI=~/.odbc.ini
        export ODBCSYSINI=/opt/amazon/redshiftodbc/Setup
        export AMAZONREDSHIFTODBCINI=/opt/amazon/redshiftodbc/lib/64/amazon.redshiftodbc.ini
        #   typeset sysname="_linux_amd64"
        # 执行代码块
    elif [ $(uname -m) = "x86_64" ] && [ $(uname) = "Darwin" ]; then
        echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为X86 Mac"
        # cp -f /opt/amazon/redshiftodbc/Setup/odbc.ini ~/.odbc.ini
        export ODBCINI=~/.odbc.ini
        export ODBCSYSINI=/opt/amazon/redshift/Setup
        export AMAZONREDSHIFTODBCINI=/opt/amazon/redshift/lib/amazon.redshiftodbc.ini
        export DYLD_LIBRARY_PATH=$DYLD_LIBRARY_PATH:/usr/local/lib
    else
        echo "$(uname) 系统未知！"
    fi

}

neo_rsql() {
    init_rsql
    rsql $*
}

init_rsql_benchmark_table() {
    rsql \
        -U root \
        -h redshift-cluster-finebi.ctn6v110psed.us-east-1.redshift.amazonaws.com \
        -d dev \
        -p 5439 \
        -t \
        -c "create table if not exists public.redshift_benchmark_statistical_table
(
    use_type                   varchar(255)   NOT NULL DEFAULT '0',
    sub_use_type               varchar(255)   NOT NULL DEFAULT '0',
    use_case                   varchar(255)   NOT NULL DEFAULT '0',
    running_Sequence           int            NOT NULL DEFAULT 0,
    query_id                   int            NOT NULL DEFAULT 0,
    exec_start_time            timestamp,
    exec_end_time              timestamp,
    total_exec_time            int8           NOT NULL DEFAULT 0,
    slot_count                 int            NOT NULL DEFAULT 0,
    final_state                varchar(255)   NOT NULL DEFAULT '0',
    query_priority             varchar(255)   NOT NULL DEFAULT '0',
    sqlquery                   varchar(10240) NOT NULL DEFAULT '0',
    concurrency_scaling_status int            NOT NULL DEFAULT 0

);"
}

# 参考链接 https://github.com/ClickHouse/ClickBench/tree/main/redshift
benchmark_redshift() {

    # 使用方式
    # 参数1 必选，需要跑的SQL文件
    # 参数2 可选，sql type
    # 参数3 可选，sql sub-type
    # 参数4 可选，sql use case
    # 🚀 写法注意需要双引号内加单引号，redshift 相关
    # benchmark_redshift
    # benchmark_redshift /tmp/redshift_demo.sql
    # benchmark_redshift /tmp/redshift_demo.sql "'this is type'"   "'this is sub_type'"  "'this is use_case'"

    TRIES=1
    # 💥 这个参数是需要单sql跑的次数 ，默认1次
    SQL_FILE=${1:-/tmp/redshift_demo.sql}
    SQL_NAME="$(basename $SQL_FILE)"
    USE_TYPE=${2:-\' \'}
    SUB_USE_TYPE=${3:-\' \'}
    USE_CASE=${4:-\' \'}
    # echo $SQL_FILE
    # echo $SQL_NAME
    # echo $USE_TYPE
    # echo $SUB_USE_TYPE
    # echo $USE_CASE

    for num in $(seq 1 $TRIES); do
        rsql \
            -U root \
            -h redshift-cluster-finebi.ctn6v110psed.us-east-1.redshift.amazonaws.com \
            -d dev \
            -p 5439 \
            -t \
            -c 'SET enable_result_cache_for_session = off' \
            -f $SQL_FILE \
            -c "INSERT INTO public.redshift_benchmark_statistical_table
(select
    $USE_TYPE as use_type,
    $SUB_USE_TYPE as sub_use_type,
    $USE_CASE as use_case,
    $num as running_Sequence,
    stl_wlm_query.query as query_id,
    stl_wlm_query.exec_start_time,
    stl_wlm_query.exec_end_time,
    stl_wlm_query.total_exec_time,
    stl_wlm_query.slot_count,
    stl_wlm_query.final_state,
    stl_wlm_query.query_priority,
    trim(stl_query.querytxt) as sqlquery,
    stl_query.concurrency_scaling_status
from stl_wlm_query,stl_query
where stl_wlm_query.query = stl_query.query and stl_query.query = pg_last_query_id());
" >/dev/null 2>&1

    done
}


create_redshift_role(){
aws iam delete-role --role-name redshift-all-admin-v1
aws iam create-role \
--role-name redshift-all-admin-v1 \
--assume-role-policy-document '{"Version":"2012-10-17","Statement":[{"Effect":"Allow","Principal":{"Service":["ec2.amazonaws.com","redshift.amazonaws.com","sagemaker.amazonaws.com","sns.amazonaws.com","events.amazonaws.com"]},"Action":"sts:AssumeRole"}]}' \
--description "Redshift all admin role" \
--max-session-duration 3600 && \
aws iam attach-role-policy --role-name redshift-all-admin-v1 --policy-arn arn:aws:iam::aws:policy/AdministratorAccess
}