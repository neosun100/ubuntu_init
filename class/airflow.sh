

# CLI

pip install apache-airflow -U 
register-python-argcomplete airflow


[cli]
api_client = airflow.api.client.json_client
endpoint_url = http://<WEBSERVER>:<PORT>