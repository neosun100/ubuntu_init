

查看无线设备：
sudo iw dev
检查无线设备情况（假设无线网卡是wlan0）：
sudo iw dev `sudo iw dev | grep Interface | cut -f 2 | cut -d " " -f 2` link

开启无线设备电源：
sudo ip link set `sudo iw dev | grep Interface | cut -f 2 | cut -d " " -f 2` up

扫描无线网络
# sudo iw dev `sudo iw dev | grep Interface | cut -f 2 | cut -d " " -f 2` scan | egrep "signal|SSID" | head -n 10
# sudo iw dev `sudo iw dev | grep Interface | cut -f 2 | cut -d " " -f 2` scan | egrep "SSID"

sudo iw dev `sudo iw dev | grep Interface | cut -f 2 | cut -d " " -f 2` scan | egrep "SSID" | cut -f 2 | cut -d : -f 2 | tr -s '\n'

连接wifi（根据essid连接就是无线名）：
sudo iw `sudo iw dev | grep Interface | cut -f 2 | cut -d " " -f 2` connect [essid]


当前连接的wifi信息
iwconfig


过滤wifi网络
sudo iw dev wlp1s0 scan | less  | grep "5G\|2.4G"


sudo iw dev wlp1s0 scan | less  | grep "SSID"

https://linux.cn/article-4015-1.html

https://www.cnblogs.com/chaobest/articles/6693161.html


signal

SSID

WPA
