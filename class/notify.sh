# 带语音的弹窗提醒程序

# 有声弹窗提醒
# 第一个参数是title，第二个参数是⏰提醒内容
notify(){
osascript -e "display notification \"${2:-美好的一天从早睡开始！}\" with title \"${1:-早睡提醒}\" subtitle \"`date +'%Y-%m-%d_%H:%M:%S'` \" " ;
say "${2:-美好的一天从早睡开始！}";
sddmd ${1:-早睡提醒} ${2:-美好的一天从早睡开始！};
}


