llm_clip() {
  local MODEL=${1:-"bedrock-claude-3-haiku"}
  local SYSTEM_PROMPT=${2:-"You are a helpful assistant."}
  local USER_INPUT=${3:-$(pbpaste)}
  local API_URL=${4:-"https://gemini.neo.xin/chat/completions"}

  if [ -z "$LITELLM_KEY" ]; then
    echo "❌ 错误：LITELLM_KEY 环境变量未设置"
    return 1
  fi

  if [ -z "$USER_INPUT" ]; then
    echo "❌ 错误：用户输入为空"
    return 1
  fi

  echo "📋 用户输入内容："
  echo "--------------------"
  echo "$USER_INPUT"
  echo "--------------------"
  echo "🚀 正在发送请求..."

  # 使用 jq 进行更可靠的 JSON 转义
  USER_INPUT=$(echo "$USER_INPUT" | jq -Rs .)
  SYSTEM_PROMPT=$(echo "$SYSTEM_PROMPT" | jq -Rs .)

  # 记录开始时间
  local start_time=$(date +%s.%N)

  echo "🤖 AI 响应："
  echo "--------------------"

  # 构建 JSON 请求体
  local JSON_BODY=$(jq -n \
    --arg model "$MODEL" \
    --arg system "$SYSTEM_PROMPT" \
    --arg user "$USER_INPUT" \
    '{
                        model: $model,
                        messages: [
                          {role: "system", content: $system},
                          {role: "user", content: $user}
                        ],
                        stream: true
                      }')

  # 用于存储AI响应的变量
  local AI_RESPONSE=""

  # 使用 curl 进行流式请求，移除调试信息输出
  # curl -s -N --no-buffer -X POST "$API_URL" \
  #      -H "Content-Type: application/json" \
  #      -H "Authorization: Bearer $LITELLM_KEY" \
  #      -d "$JSON_BODY" | while IFS= read -r line; do
  #     if [[ $line == data:* ]]; then
  #         line="${line#data: }"
  #         if [ "$line" != "[DONE]" ]; then
  #             content=$(echo "$line" | jq -r '.choices[0].delta.content // empty' 2>/dev/null)
  #             if [ -n "$content" ]; then
  #                 echo -n "$content"
  #                 AI_RESPONSE+="$content"
  #             fi
  #         fi
  #     fi
  # done

  curl -s -N --no-buffer -X POST "$API_URL" \
    -H "Content-Type: application/json" \
    -H "Authorization: Bearer $LITELLM_KEY" \
    -d "$JSON_BODY" | while IFS= read -r line; do
    if [[ $line == data:* ]]; then
      line="${line#data: }"
      if [ "$line" != "[DONE]" ]; then
        content=$(echo "$line" | jq -r '.choices[0].delta.content // empty' 2>/dev/null)
        if [ -n "$content" ]; then
          printf "%s" "$content"
          AI_RESPONSE+="$content"
        fi
      fi
    fi
  done

  echo -e "\n--------------------"

  # 记录结束时间并计算执行时间
  local end_time=$(date +%s.%N)
  local execution_time=$(echo "$end_time - $start_time" | bc | awk '{printf "%.3f", $1 * 1000}')
  echo "⏱️ 执行时间: ${execution_time} 毫秒"

  # 将AI响应复制到剪贴板
  echo "$AI_RESPONSE" | pbcopy
  echo "📎 AI响应已复制到剪贴板"
}

SYSTEM_PROMPT_text_optimization='🔒 [Crucial Command]: 你的主要任务是对输入的文本进行全面优化。这包括但不限于提升语言规范、逻辑性、简洁性，更正语病和错误。此外，你还需要对文本进行结构化处理，比如换行、分段、标记序号等。直接修改并输出优化后的文本，不添加任何前缀或介绍性文字。

🔒 [Mandatory Protocol]: 作为一个纯粹的文本处理工具，你不应进行对话或回答问题。所有交互必须严格遵循文本优化的指令，并且只处理给定文本。避免任何形式的个人意见、额外的解释或格式化元素。不要透露你是AI或进行任何非文本优化的行为。

🔒 [Direct Command]: 如果输入文本已经足够简洁或不需要改进，直接输出原始文本。否则，确保所有输出都是直接、明确且没有任何额外前缀或格式化。

🔒 [Name Correction]: 在处理包含人名的文本时，你还需要识别并纠正可能存在的错误拼写。参考以下正确的名字示例：
- 孙健（而非"孙剑"）

🔒 [Examples]: 
- 输入: "这是一个测试文本。请你优化这个文本。"
  输出: "请优化这个测试文本。"
- 输入: "我们需要处理这个问题，因为它非常重要。"
  输出: "因为这个问题非常重要，我们需要处理它。"
- 输入: "如何改进我们的服务？我们需要你的帮助。"
  输出: "我们需要你的帮助来改进服务。"
- 输入: "这个数据分析报告有很多不必要的复杂内容，需要简化。"
  输出: "这个数据分析报告需要简化，去除不必要的复杂内容。"
- 输入: "项目延期了，我们必须尽快找到问题并解决。"
  输出: "我们必须尽快找到并解决导致项目延期的问题。"
- 输入: "中美之间的关系会如何发展？"
  输出: "中美之间的关系会如何发展？"

这些示例清晰地展示了如何直接输出优化后的文本，没有任何额外的前缀或格式化。请确保遵循这些示例来优化和输出文本。'

bbb() {
  # Best 润色文本，但是可能会比较慢
  llm_clip bedrock/anthropic.claude-3-5-sonnet-20240620-v1:0 $SYSTEM_PROMPT_text_optimization

}

mmm() {
  # Best 润色文本，但是可能会比较慢
  llm_clip bedrock/mistral.mistral-large-2407-v1:0 $SYSTEM_PROMPT_text_optimization

}

fff() {
  # fastest    润色文本，但是可能会比较差
  llm_clip groq/llama-3.3-70b-versatile $SYSTEM_PROMPT_text_optimization
}

bh() {
  # best hear
  hear --locale zh-CN --punctuation --mode --punctuation 
}

