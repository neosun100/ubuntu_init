
#  安装 zbar 二维码识别模块  命令是 zbarimg

install_zbar(){
cd ~/upload;
if [ `uname` = "Darwin" ]; then
     brew install zbar && \
     wget https://neone-club.sh1a.qingstor.com/Sys/zbar-py-1.0.4.tar.gz && \
     tar -zxvf zbar-py-1.0.4.tar.gz && \
     cd zbar-py-1.0.4/ && \
     python setup.py install && \  
elif [ `uname` = "Linux" ]; then
    echo "`uname` 暂未找到可用的"
else
    echo "`uname` 系统未知！"
fi
cd ~/upload;
}

