# mac linux 通用

install_flink_lastest() {
    cd ~/upload

    # flink最新版本
    FLINK_VERSION=$(curl $APACHE_HOST'/apache/flink/' \
        -H 'Connection: keep-alive' \
        -H 'sec-ch-ua: "Chromium";v="88", "Google Chrome";v="88", ";Not A Brand";v="99"' \
        -H 'sec-ch-ua-mobile: ?0' \
        -H 'Upgrade-Insecure-Requests: 1' \
        -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 11_2_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.192 Safari/537.36' \
        -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9' \
        -H 'Sec-Fetch-Site: same-origin' \
        -H 'Sec-Fetch-Mode: navigate' \
        -H 'Sec-Fetch-User: ?1' \
        -H 'Sec-Fetch-Dest: document' \
        -H 'Referer: https://mirror-hk.koddos.net/apache/' \
        -H 'Accept-Language: zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7' \
        -s \
        --compressed | grep -v 'kubernetes\|statefun\|shaded\|ml' | grep '20[2-9][0-9]-[0-1][0-9]-[0-3][0-9]' | keycut '<a href="' '/">' | grep -v 'flink-table-store' | sort -r | head -n 1)
    # KAFKA_VERSION=`http https://mirror-hk.koddos.net/apache/kafka/ | grep   '20[2-9][0-9]-[0-1][1-9]-[0-3][0-9]' | keycut '<a href="' '/">' | sort -r  | head -n 1`

    FLINK_PACKAGE_NAME=$(curl $APACHE_HOST"/apache/flink/$FLINK_VERSION/" \
        -H 'Connection: keep-alive' \
        -H 'sec-ch-ua: "Chromium";v="88", "Google Chrome";v="88", ";Not A Brand";v="99"' \
        -H 'sec-ch-ua-mobile: ?0' \
        -H 'Upgrade-Insecure-Requests: 1' \
        -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 11_2_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.192 Safari/537.36' \
        -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9' \
        -H 'Sec-Fetch-Site: same-origin' \
        -H 'Sec-Fetch-Mode: navigate' \
        -H 'Sec-Fetch-User: ?1' \
        -H 'Sec-Fetch-Dest: document' \
        -H 'Referer: https://mirror-hk.koddos.net/apache/spark/' \
        -H 'Accept-Language: zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7' \
        -s \
        --compressed | grep 'bin' | grep -v 'without' | keycut 'href="' '">flink' | sort -r | head -n 1)

    mwget "$APACHE_HOST/apache/flink/${FLINK_VERSION}/${FLINK_PACKAGE_NAME}"

    tar -zxvf $FLINK_PACKAGE_NAME >/dev/null 2>&1

    [[ -d /usr/local/flink ]] && (sudo rm -rf /usr/local/flink && sudo mkdir /usr/local/flink && sudo chmod -R 777 /usr/local/flink) || (sudo mkdir /usr/local/flink && sudo chmod -R 777 /usr/local/flink)

    FLINK_DIR=$(ls ~/upload | grep flink | grep -v 'tgz' | sort -r | head -n 1)

    cp -r $FLINK_DIR/* /usr/local/flink
    l /usr/local/flink

    if [ $(uname) = "Darwin" ] && [ -d /usr/local/flink ]; then
        export FLINK_HOME=/usr/local/flink
        export FLINK_CONF_DIR=/usr/local/flink/conf
        export PATH=$PATH:$FLINK_HOME/bin
    elif [ $(uname) = "Linux" ] && [ -d /usr/local/flink ]; then
        export FLINK_HOME=/usr/local/flink
        export PATH=$PATH:$FLINK_HOME/bin
    else
        echo "$(uname)  系统未知 或 还未添加 flink bin"
    fi

    # echo "修改 spark-env.sh 解决 WARN Utils"
    # l $SPARK_HOME/conf/
    # cp -rf $SPARK_HOME/conf/spark-env.sh.template $SPARK_HOME/conf/spark-env.sh
    # echo "SPARK_LOCAL_IP=localhost" >>$SPARK_HOME/conf/spark-env.sh

    sourceb
    flink --version

    rm -rf $FLINK_DIR 
    rm -rf $FLINK_PACKAGE_NAME
}


install_flink_version_lastest() {
    cd ~/upload
    VERSION=${1:-1.14}
    # flink最新版本
    FLINK_VERSION=$(curl $APACHE_HOST'/apache/flink/' \
        -H 'Connection: keep-alive' \
        -H 'sec-ch-ua: "Chromium";v="88", "Google Chrome";v="88", ";Not A Brand";v="99"' \
        -H 'sec-ch-ua-mobile: ?0' \
        -H 'Upgrade-Insecure-Requests: 1' \
        -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 11_2_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.192 Safari/537.36' \
        -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9' \
        -H 'Sec-Fetch-Site: same-origin' \
        -H 'Sec-Fetch-Mode: navigate' \
        -H 'Sec-Fetch-User: ?1' \
        -H 'Sec-Fetch-Dest: document' \
        -H 'Referer: https://mirror-hk.koddos.net/apache/' \
        -H 'Accept-Language: zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7' \
        -s \
        --compressed | grep -v 'kubernetes\|statefun\|shaded\|ml' | grep '20[2-9][0-9]-[0-1][0-9]-[0-3][0-9]' | grep $VERSION | keycut '<a href="' '/">' | sort -r | head -n 1)
    # KAFKA_VERSION=`http https://mirror-hk.koddos.net/apache/kafka/ | grep   '20[2-9][0-9]-[0-1][1-9]-[0-3][0-9]' | keycut '<a href="' '/">' | sort -r  | head -n 1`

    FLINK_PACKAGE_NAME=$(curl $APACHE_HOST"/apache/flink/$FLINK_VERSION/" \
        -H 'Connection: keep-alive' \
        -H 'sec-ch-ua: "Chromium";v="88", "Google Chrome";v="88", ";Not A Brand";v="99"' \
        -H 'sec-ch-ua-mobile: ?0' \
        -H 'Upgrade-Insecure-Requests: 1' \
        -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 11_2_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.192 Safari/537.36' \
        -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9' \
        -H 'Sec-Fetch-Site: same-origin' \
        -H 'Sec-Fetch-Mode: navigate' \
        -H 'Sec-Fetch-User: ?1' \
        -H 'Sec-Fetch-Dest: document' \
        -H 'Referer: https://mirror-hk.koddos.net/apache/spark/' \
        -H 'Accept-Language: zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7' \
        -s \
        --compressed | grep 'bin' | grep -v 'without' | keycut 'href="' '">flink' | sort -r | head -n 1)

    mwget "$APACHE_HOST/apache/flink/${FLINK_VERSION}/${FLINK_PACKAGE_NAME}"

    tar -zxvf $FLINK_PACKAGE_NAME >/dev/null 2>&1

    [[ -d /usr/local/flink ]] && (sudo rm -rf /usr/local/flink && sudo mkdir /usr/local/flink && sudo chmod -R 777 /usr/local/flink) || (sudo mkdir /usr/local/flink && sudo chmod -R 777 /usr/local/flink)

    FLINK_DIR=$(ls ~/upload | grep flink | grep -v 'tgz' | sort -r | head -n 1)

    cp -r $FLINK_DIR/* /usr/local/flink
    l /usr/local/flink

    if [ $(uname) = "Darwin" ] && [ -d /usr/local/flink ]; then
        export FLINK_HOME=/usr/local/flink
        export FLINK_CONF_DIR=/usr/local/flink/conf
        export PATH=$PATH:$FLINK_HOME/bin
    elif [ $(uname) = "Linux" ] && [ -d /usr/local/flink ]; then
        export FLINK_HOME=/usr/local/flink
        export PATH=$PATH:$FLINK_HOME/bin
    else
        echo "$(uname)  系统未知 或 还未添加 flink bin"
    fi

    # echo "修改 spark-env.sh 解决 WARN Utils"
    # l $SPARK_HOME/conf/
    # cp -rf $SPARK_HOME/conf/spark-env.sh.template $SPARK_HOME/conf/spark-env.sh
    # echo "SPARK_LOCAL_IP=localhost" >>$SPARK_HOME/conf/spark-env.sh

    sourceb
    flink --version

    rm -rf $FLINK_DIR 
    rm -rf $FLINK_PACKAGE_NAME
}