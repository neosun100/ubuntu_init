# ubuntu下的各个service


# 安装初始化nfs
install_nfs(){
echo y | sudo apt-get install nfs-kernel-server ;
echo y | sudo apt-get install rpcbind;    
}

# 设置共享路经
init_nfs_path(){
echo "`date +'%Y-%m-%d %H:%M:%S'` 初始化 nfs 配置文件" | lcat;
sudo echo "" > /etc/exports;
sudo echo "/usr/local/k8s/redis/pv1 192.168.*.*(rw,sync,no_root_squash,no_all_squash,insecure)" >> /etc/exports;
sudo echo "/usr/local/k8s/redis/pv2 192.168.*.*(rw,sync,no_root_squash,no_all_squash,insecure)" >> /etc/exports;
sudo echo "/usr/local/k8s/redis/pv3 192.168.*.*(rw,sync,no_root_squash,no_all_squash,insecure)" >> /etc/exports;
sudo echo "/usr/local/k8s/redis/pv4 192.168.*.*(rw,sync,no_root_squash,no_all_squash,insecure)" >> /etc/exports;
sudo echo "/usr/local/k8s/redis/pv5 192.168.*.*(rw,sync,no_root_squash,no_all_squash,insecure)" >> /etc/exports;
sudo echo "/usr/local/k8s/redis/pv6 192.168.*.*(rw,sync,no_root_squash,no_all_squash,insecure)" >> /etc/exports;
/bin/cat  /etc/exports | lcat;
echo "`date +'%Y-%m-%d %H:%M:%S'` 创建 nfs 目录" | lcat;
sudo mkdir -p /usr/local/k8s/redis/pv{1..6}; # 创建对应目录
echo "`date +'%Y-%m-%d %H:%M:%S'` 显示 nfs 目录" | lcat;
l /usr/local/k8s/redis/;
}

# 开启nfs服务
start_nfs_service(){
echo "`date +'%Y-%m-%d %H:%M:%S'` 启动 nfs 服务" | lcat;
sudo systemctl restart nfs-server;
sudo systemctl restart rpcbind;
sudo systemctl enable nfs-server;
echo "`date +'%Y-%m-%d %H:%M:%S'` 显示 nfs 状态" | lcat;
sudo systemctl status nfs-server;
sudo systemctl status rpcbind;
echo "`date +'%Y-%m-%d %H:%M:%S'` 显示 nfs 设置" | lcat;
sudo exportfs -v;    
}


# systemctl 服务程序管理
sctl_start(){
sudo systemctl start $1;
}

sctl_status(){
sudo systemctl status $1;
}

sctl_enable(){
sudo systemctl enable $1;
}

sctl_disable(){
sudo systemctl disable $1;
}

sctl_stop(){
sudo systemctl stop $1;
}

sctl_restart(){
sudo systemctl restart $1;
}

sctl_list(){
sudo systemctl list-units --all --type=service;
}


sctl_list_grep(){
sudo systemctl list-units --all --type=service  | grep ${1:-nfs};   
}


# service 对比 sctl
# 文件目录在 /etc/init.d/
# 
# service foo start	systemctl start foo.service	启动服务
# service foo restart	systemctl restart foo.service	重启服务
# service foo stop	systemctl stop foo.service	停止服务
# service foo reload	systemctl reload foo.service	重新加载配置文件（不终止服务）
# service foo status	systemctl status foo.service	查看服务状态


# chkconfig foo on	systemctl enable foo.service	开机自动启动
# chkconfig foo off	systemctl disable foo.service	开机不自动启动
# chkconfig foo	systemctl is-enabled foo.service	查看特定服务是否为开机自动启动
# chkconfig --list	systemctl list-unit-files --type=service	查看各个级别下服务的启动与禁用情况






# 客户端查看nfs服务清单
select_nfs_list(){
if [ `uname` = "Darwin" ]; then
showmount -e  ${1:-192.168.255.168} | lcat;
elif [ `uname` = "Linux" ]; then
sudo showmount -e ${1:-192.168.249.26} | lcat;
else
    echo "`uname` 系统未知！"
fi     
}


# 客户端查看本地挂载的nfs目录
select_nfs_local(){
if [ `uname` = "Darwin" ]; then
showmount -a  ${1:-192.168.255.168} | lcat;
elif [ `uname` = "Linux" ]; then
sudo showmount -a  ${1:-192.168.249.26} | lcat;
else
    echo "`uname` 系统未知！"
fi       
}


# 挂载 nfs 第一个参数为nfs挂载路径，第二个参数为，本地挂载的位置
mount_nfs(){
echo "nfs文件目录: ${1:-192.168.255.168:/usr/local/k8s/redis/pv2}" | lcat;
echo "挂载文件目录: ${2:-~/upload/nfs/}" | lcat;
sudo  mount -o resvport  ${1:-192.168.255.168:/usr/local/k8s/redis/pv2}   ${2:-~/upload/nfs/};
}

# 打开挂载目录 只有一个参数，挂载目录
open_nfs(){
echo "打开挂载文件目录: ${1:-~/upload/nfs/}" | lcat;
open ${1:-~/upload/nfs/};
}

# 卸载nfs目录 只有一个参数 挂载的目录
umount_nfs(){
echo "卸载挂载文件目录: ${1:-~/upload/nfs/}" | lcat;
sudo umount ${1:-~/upload/nfs/};
}



# k8s nfs
# https://www.cnblogs.com/DaweiJ/articles/9131762.html