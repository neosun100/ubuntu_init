install_eks() {
    eksctl version
    echo "$(date +'%Y-%m-%d %H:%M:%S') setup or resetup latest eksctl"
    # eksctl_Darwin_arm64.tar.gz
    ARCH=$(uname -m)
    if [ "$ARCH" = "x86_64" ]; then
        ARCH="amd64"
    fi
    PLATFORM=$(uname -s)_$ARCH

    # curl -sLO "https://github.com/eksctl-io/eksctl/releases/latest/download/eksctl_$PLATFORM.tar.gz"
    curl -sLO "https://github.com/eksctl-io/eksctl/releases/latest/download/eksctl_$PLATFORM.tar.gz"

    # (Optional) Verify checksum
    curl -sL "https://github.com/eksctl-io/eksctl/releases/latest/download/eksctl_checksums.txt" | grep $PLATFORM | sha256sum --check

    tar -xzf eksctl_$PLATFORM.tar.gz -C /tmp && rm eksctl_$PLATFORM.tar.gz

    sudo mv /tmp/eksctl /usr/local/bin
    eksctl help
    eksctl version
    echo "$(date +'%Y-%m-%d %H:%M:%S') setup or resetup latest eksctl success"

    echo "$(date +'%Y-%m-%d %H:%M:%S') setup or resetup latest kubectl"
    brew install kubectl
    brew upgrade kubectl
    # brew upgrade kubernetes-cli
    kubectl help
    kubectl version
    echo "$(date +'%Y-%m-%d %H:%M:%S') setup or resetup latest kubectl success"

}

eks_local_env_init() {
    # local 环境检查
    EKSCTL_SETUP_STATUS=$(eksctl help | grep 'The official CLI for Amazon EKS' | wc -l | pass_black)
    # 1 0

    KUBECTL_SETUP_STATUS=$(kubectl help | grep "kubectl controls the Kubernetes cluster manager" | wc -l | pass_black)
    # 1 0

    AWS_SETUP_STATUS=$(aws help | grep "The  AWS  Command  Line  Interface" | wc -l | pass_black)
    # 1 0

    if [ $EKSCTL_SETUP_STATUS = 1 ] && [ $KUBECTL_SETUP_STATUS = 1 ] && [ $AWS_SETUP_STATUS = 1 ]; then
        echo 'success! local env has pass check'
    else
        install_eks && setup_awscli
    fi
}

aws_region_check() {
    echo "当前所在 region" && cat ~/.aws/config && echo "\n\n当前region ak/sk\n\n" && cat ~/.aws/credentials
}

# VIRTUAL_CLUSTER_NAME=$(aws emr-containers list-virtual-clusters | jq '.virtualClusters[].id' | pass_quotes)
# # ld74s94zfiqj4ll8qskxrhlp0

# VPC_NUM=$(aws ec2 describe-vpcs | jq '.Vpcs  | length ')
# # <5

# aws ec2 describe-vpcs | jq '.Vpcs' | md-table | lcat
# # vpc

# KEY_NUM=$(aws ec2 describe-key-pairs | jq '.KeyPairs  | length')
# # >= 1

# aws ec2 describe-key-pairs | jq '.KeyPairs' | md-table | lcat
# # key

# aws ec2 describe-key-pairs | jq '.KeyPairs[0].KeyName' | pass_quotes
# # neo-web-visit-aws-Singapore

# KEY_NAME=$(aws ec2 describe-key-pairs | jq '.KeyPairs[].KeyName' | pass_quotes)

# aws iam list-roles | jq '[.Roles[] | { Arn,date: .CreateDate[:10],Service: .AssumeRolePolicyDocument.Statement[].Principal.Service[0:3] }   ] ' | md-table | grep 'Arn\|all\|admin' | nt | lcat
# # need role

# if [[ $VPC_NUM < 5 ]]; then
#     echo success
# else
#     echo fail
# fi

# aws ec2 describe-availability-zones --region us-east-1 | jq '.AvailabilityZones[].ZoneName' | pass_quotes | Row2column | t2dformat
# # ap-southeast-1a,ap-southeast-1b,ap-southeast-1c

# aws emr-containers list-virtual-clusters | jq '[.virtualClusters[] | select(.state == "RUNNING")] | length'
# # 1 活跃虚拟集群数

# aws emr-containers list-virtual-clusters | jq '[.virtualClusters[] | select(.state == "RUNNING")][0].id' | pass_quotes
# # 40831w2j5r0w1yquw28kfnt0r 第一个活跃虚拟集群id

# aws_eks_set_region() {
#     EKS_REGION=${1:-"us-east-1"}
#     KEY_NAME=$(aws ec2 describe-key-pairs | jq '.KeyPairs[].KeyName' | pass_quotes)
#     ZONES_NAME=$(aws ec2 describe-availability-zones --region $EKS_REGION | jq '.AvailabilityZones[].ZoneName' | pass_quotes | Row2column | t2dformat)
#     VIRTUAL_CLUSTER_ID=$(aws emr-containers list-virtual-clusters | jq '.virtualClusters[].id' | pass_quotes)

#     export REGION=$EKS_REGION
#     export ZONES=$ZONES_NAME
#     export EKS_CLUSTER_NAME="neo-K8s-$EKS_REGION"
#     # export DATALAKE_NAMESPACE="datalake"
#     export DATALAKE_NAMESPACE="default" # 默认环境吧，好监控
#     export VIRTUAL_CLUSTER_NAME="emr-cirtual-cluster"
#     export SSH_PUBLIC_KEY=$KEY_NAME
#     if [ $EKS_REGION -eq "cn-north-1" ] || [ $EKS_REGION -eq "cn-northwest-1" ]; then
#         export EXECUTION_ROLE_ARN="eks-all-role"
#         export RUN_EXECUTION_ROLE_ARN="arn:aws-cn:iam::032665272082:role/eks-all-role"

#     else
#         export EXECUTION_ROLE_ARN="emr-on-eks-role-v1-neo-20-long"
#         export RUN_EXECUTION_ROLE_ARN="arn:aws:iam::835751346093:role/emr-on-eks-role-v1-neo-20-long"
#     fi

#     export VIRTUAL_CLUSTER_ID=$VIRTUAL_CLUSTER_ID
# }

### 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀
### 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀
### 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀
aws_eks_init() {

    echo "缺check cloudformation 检查，同名堆栈删除,有的AZ不支持EKS"
    echo "eksctl-neo-K8s-us-east-1-cluster"

    echo "0. 初始化全局环境变量"
    input_region_name=${1:-"virginia"}
    node_class=${2:-"m5.4xlarge"}
    node_num=${3:-5}

    aws_eks_set_region $input_region_name

    echo "1. 清空当前 /Users/jiasunm/.kube/config 并备份"
    scat /Users/jiasunm/.kube/config >>/Users/jiasunm/.kube/backup.config
    echo "" >/Users/jiasunm/.kube/config

    echo "2. 安装EKS"
    eksctl create cluster \
        --region $REGION \
        --name $EKS_CLUSTER_NAME \
        --zones $ZONES \
        --node-type $node_class \
        --nodes $node_num \
        --with-oidc \
        --ssh-access \
        --ssh-public-key $SSH_PUBLIC_KEY \
        --managed

    echo "3. 注册当前region EKS 到本地配置"
    scat /Users/jiasunm/.kube/config

    scat /Users/jiasunm/.kube/config >/Users/jiasunm/.kube/aws_$input_region_name.config

    echo "4. 更新 环境变量"
    aws_eks_set_region $input_region_name

    echo "5. 创建eks 配置"
    eksctl create iamidentitymapping \
        --region $REGION \
        --cluster $EKS_CLUSTER_NAME \
        --namespace $DATALAKE_NAMESPACE \
        --service-name "emr-containers"

    echo "6. 更新eks emr iam"
    aws emr-containers update-role-trust-policy \
        --cluster-name $EKS_CLUSTER_NAME \
        --namespace $DATALAKE_NAMESPACE \
        --role-name $EXECUTION_ROLE_ARN

    echo "7. 创建emr on eks 配置文件"
    tee $VIRTUAL_CLUSTER_NAME.json <<EOF
{
  "name": "$VIRTUAL_CLUSTER_NAME",
  "containerProvider": {
    "type": "EKS",
    "id": "$EKS_CLUSTER_NAME",
    "info": {
      "eksInfo": {
        "namespace": "$DATALAKE_NAMESPACE"
      }
    }
  }
}
EOF
    echo "8. 创建emr on eks 虚拟集群"
    aws emr-containers create-virtual-cluster --cli-input-json file://./$VIRTUAL_CLUSTER_NAME.json

    echo "9. 更新EMR 虚拟集群 ID"
    export VIRTUAL_CLUSTER_ID="$(aws emr-containers list-virtual-clusters | jq '.virtualClusters[].id' | pass_quotes)"

    echo "10. 运行一个测试任务"
    export EMR_ON_EKS_JOB_NAME="sample-job-$(hostname)"

    aws emr-containers start-job-run \
        --virtual-cluster-id $VIRTUAL_CLUSTER_ID \
        --name $EMR_ON_EKS_JOB_NAME \
        --execution-role-arn $RUN_EXECUTION_ROLE_ARN \
        --release-label emr-6.3.0-latest \
        --job-driver '{
    "sparkSubmitJobDriver": {
        "entryPoint": "s3://aws-data-analytics-workshops/emr-eks-workshop/scripts/pi.py",
        "sparkSubmitParameters": "--conf spark.executor.instances=2 --conf spark.executor.memory=2G --conf spark.executor.cores=2 --conf spark.driver.cores=1"
        }
    }'

    echo "11. 集群资源一览"
    k8sctl_all

}

aws_eks_token() {
    aws eks get-token --cluster-name $EKS_CLUSTER_NAME | jq -r '.status.token'
    aws eks get-token --cluster-name $EKS_CLUSTER_NAME | jq -r '.status.token' | njq

}

### 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀
### 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀
### 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀
aws_eks_set_region() {
    echo "建立集群后，每次执行eks或EMRonEKS前，执行此命令，固化环境变量"

    input_region_name=${1:-virginia}
    if [ $input_region_name = "singapore" ]; then
        aws_set_singapore
        set_k8s_master_singapore
    elif [ $input_region_name = "ohio" ]; then
        aws_set_ohio
        set_k8s_master_ohio
    elif [ $input_region_name = "bj" ]; then
        aws_set_bj
        set_k8s_master_bj
    elif [ $input_region_name = "nx" ]; then
        aws_set_nx
        set_k8s_master_nx
    elif [ $input_region_name = "ireland" ]; then
        aws_set_ireland
        set_k8s_master_ireland
    elif [ $input_region_name = "virginia" ]; then
        aws_set_virginia
        set_k8s_master_virginia
    else
        echo "输入region name 有误 或当前 region name 未加入到 CLI"
    fi

    echo "设定EKS region完成✅"
    EKS_REGION=$(scat ~/.aws/config | grep "region" | field 3)
    KEY_NAME=$(aws ec2 describe-key-pairs | jq '.KeyPairs[].KeyName' | pass_quotes)
    ZONES_NAME=$(aws ec2 describe-availability-zones --region $EKS_REGION | jq '.AvailabilityZones[].ZoneName' | head -n 3 | pass_quotes | Row2column | t2dformat)
    VIRTUAL_CLUSTER_ID=$(aws emr-containers list-virtual-clusters | jq '.virtualClusters[] | select(.state == "RUNNING") | .id' | pass_quotes)

    export REGION=$EKS_REGION
    export ZONES=$ZONES_NAME
    export EKS_CLUSTER_NAME="neo-K8s-$EKS_REGION"
    # export DATALAKE_NAMESPACE="datalake"
    export DATALAKE_NAMESPACE="default" # 默认环境吧，好监控
    export VIRTUAL_CLUSTER_NAME="emr-cirtual-cluster"
    export SSH_PUBLIC_KEY=$KEY_NAME
    if [ $EKS_REGION = "cn-north-1" ] || [ $EKS_REGION = "cn-northwest-1" ]; then
        export EXECUTION_ROLE_ARN="eks-all-role"
        export RUN_EXECUTION_ROLE_ARN="arn:aws-cn:iam::032665272082:role/eks-all-role"

    else
        export EXECUTION_ROLE_ARN="emr-on-eks-role-v1-neo-20-long"
        export RUN_EXECUTION_ROLE_ARN="arn:aws:iam::835751346093:role/emr-on-eks-role-v1-neo-20-long"
    fi

    export VIRTUAL_CLUSTER_ID=$VIRTUAL_CLUSTER_ID

    echo "当前EKS环境为 $EKS_REGION"

}

### 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀
### 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀
### 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀
aws_eks_clear() {
    echo "EKS环境清理，需要先执行 aws_eks_set_XXXXX"
    time_wait 5
    EKS_REGION=$(scat ~/.aws/config | grep "region" | field 3)
    VIRTUAL_CLUSTER_ID=$(aws emr-containers list-virtual-clusters | jq '.virtualClusters[].id' | pass_quotes)
    aws emr-containers delete-virtual-cluster --id $VIRTUAL_CLUSTER_ID
    eksctl delete cluster --region $EKS_REGION --name $EKS_CLUSTER_NAME

}

# aws_eks_set_ireland() {
#     aws_set_ireland
#     set_k8s_master_ireland

#     export REGION="eu-west-1"
#     export ZONES="eu-west-1a,eu-west-1b,eu-west-1c"
#     export EKS_CLUSTER_NAME="neo-K8s-infrastructure"
#     export DATALAKE_NAMESPACE="datalake"
#     export VIRTUAL_CLUSTER_NAME="emr-cluster-1"
#     export SSH_PUBLIC_KEY="visit-ireland-key"
#     export EXECUTION_ROLE_ARN="emr-on-eks-role-v1-neo-20-long"
#     export RUN_EXECUTION_ROLE_ARN="arn:aws:iam::835751346093:role/emr-on-eks-role-v1-neo-20-long"
#     export VIRTUAL_CLUSTER_ID="40831w2j5r0w1yquw28kfnt0r"
# }

# aws_eks_set_virginia() {
#     aws_set_virginia
#     set_k8s_master_virginia

#     export REGION="us-east-1"
#     export ZONES="us-east-1a,us-east-1b,us-east-1c"
#     export EKS_CLUSTER_NAME="virginia-k8s"
#     export DATALAKE_NAMESPACE="datalake"
#     export DATALAKE_NAMESPACE="default" # 默认环境吧，好监控
#     export VIRTUAL_CLUSTER_NAME="emr-cluster-virginia"
#     export SSH_PUBLIC_KEY="visit-virginia-key"
#     export EXECUTION_ROLE_ARN="emr-on-eks-role-v1-neo-20-long"
#     export RUN_EXECUTION_ROLE_ARN="arn:aws:iam::835751346093:role/emr-on-eks-role-v1-neo-20-long"
# }

# aws_eks_set_bj() {
#     aws_set_bj
#     set_k8s_master_bj

#     export REGION="cn-north-1"
#     export ZONES="cn-north-1a,cn-north-1b,cn-north-1d"
#     export EKS_CLUSTER_NAME="beijing-k8s"
#     export DATALAKE_NAMESPACE="datalake"
#     export DATALAKE_NAMESPACE="default" # 默认环境吧，好监控
#     export VIRTUAL_CLUSTER_NAME="emr-cluster-beijing"
#     export SSH_PUBLIC_KEY="neo-web-visit-aws-beijing"
#     export EXECUTION_ROLE_ARN="eks-all-role"
#     export RUN_EXECUTION_ROLE_ARN="arn:aws-cn:iam::032665272082:role/eks-all-role"

# }

# aws_eks_set_nx() {
#     # 待修改
#     aws_set_nx
#     set_k8s_master_nx

#     export REGION="cn-northwest-1"
#     export ZONES="cn-northwest-1a,cn-northwest-1b,cn-northwest-1c"
#     export EKS_CLUSTER_NAME="ningxia-k8s"
#     export DATALAKE_NAMESPACE="datalake"
#     export DATALAKE_NAMESPACE="default" # 默认环境吧，好监控
#     export VIRTUAL_CLUSTER_NAME="emr-cluster-ningxia"
#     export SSH_PUBLIC_KEY="AWS-ningxia-key"
#     export EXECUTION_ROLE_ARN="eks-all-role"
#     export RUN_EXECUTION_ROLE_ARN="arn:aws-cn:iam::032665272082:role/eks-all-role"

# }

# aws_init_emr_on_eks() {
#     # 参数为你需要init的region name，获取该region的AZ，以及初始化全部变量
#     # 判断该region是否已经存在EKS，若存在则pass
#     # 创建eks集群
#     # 构建config文件
# }

# eksctl create cluster \
#     --region $REGION \
#     --name $EKS_CLUSTER_NAME \
#     --zones $ZONES \
#     --node-type m5.4xlarge \
#     --nodes 5 \
#     --with-oidc \
#     --ssh-access \
#     --ssh-public-key $SSH_PUBLIC_KEY \
#     --managed

# scat ~/.kube/aws_ireland.config

# scat /Users/jiasunm/.kube/config >/Users/jiasunm/.kube/aws_virginia.config

# set_k8s_master_virginia

# eksctl create iamidentitymapping \
#     --region $REGION \
#     --cluster $EKS_CLUSTER_NAME \
#     --namespace $DATALAKE_NAMESPACE \
#     --service-name "emr-containers"

# aws_set_virginia
# set_k8s_master_virginia

# aws emr-containers update-role-trust-policy \
#     --cluster-name $EKS_CLUSTER_NAME \
#     --namespace $DATALAKE_NAMESPACE \
#     --role-name $EXECUTION_ROLE_ARN

# tee $VIRTUAL_CLUSTER_NAME.json <<EOF
# {
#   "name": "$VIRTUAL_CLUSTER_NAME",
#   "containerProvider": {
#     "type": "EKS",
#     "id": "$EKS_CLUSTER_NAME",
#     "info": {
#       "eksInfo": {
#         "namespace": "$DATALAKE_NAMESPACE"
#       }
#     }
#   }
# }
# EOF

# aws emr-containers create-virtual-cluster \
#     --region "cn-northwest-1" \
#     --name "emr-cluster-ningxia" \
#     --container-provider '{
# 	"id": "ningxia-k8s",
# 	"type": "EKS",
# 	"info": {
# 		"eksInfo": {
# 			"namespace": "default"
# 		}
# 	}
# }'

# aws emr-containers create-virtual-cluster \
#     --region "cn-northwest-1" \
#     --name "emr-cluster-ningxia" \
#     --container-provider '{
# "id": "ningxia-k8s", "type": "EKS", "info": {
# "eksInfo": {
# "namespace": "default"
# } }
# }'

# aws emr-containers create-virtual-cluster --cli-input-json file://./$VIRTUAL_CLUSTER_NAME.json
# # {
# #     "id": "qcjxx7g1z4pl1b98fcju5altf",
# #     "name": "emr-cluster-virginia",
# #     "arn": "arn:aws:emr-containers:us-east-1:835751346093:/virtualclusters/qcjxx7g1z4pl1b98fcju5altf"
# # }

# export VIRTUAL_CLUSTER_ID="$(aws emr-containers list-virtual-clusters | jq '.virtualClusters[].id' | pass_quotes)"

# export VIRTUAL_CLUSTER_ID='qcjxx7g1z4pl1b98fcju5altf'
# export EMR_ON_EKS_JOB_NAME='sample-job-emrONeks'

# aws emr-containers start-job-run \
#     --virtual-cluster-id $VIRTUAL_CLUSTER_ID \
#     --name $EMR_ON_EKS_JOB_NAME \
#     --execution-role-arn $RUN_EXECUTION_ROLE_ARN \
#     --release-label emr-6.3.0-latest \
#     --job-driver '{"sparkSubmitJobDriver": {"entryPoint": "local:///usr/lib/spark/examples/src/main/python/pi.py","sparkSubmitParameters": "--conf spark.executor.instances=2 --conf spark.executor.memory=2G --conf spark.executor.cores=2 --conf spark.driver.cores=1"}}' \
#     --configuration-overrides '{"monitoringConfiguration": {"cloudWatchMonitoringConfiguration": {"logGroupName": "log_group_name", "logStreamNamePrefix": "log_stream_prefix"}}}'

# aws emr-containers start-job-run \
#     --virtual-cluster-id $VIRTUAL_CLUSTER_ID \
#     --name demo-1 \
#     --execution-role-arn $RUN_EXECUTION_ROLE_ARN \
#     --release-label emr-6.3.0-latest \
#     --job-driver '{
#     "sparkSubmitJobDriver": {
#         "entryPoint": "s3://aws-data-analytics-workshops/emr-eks-workshop/scripts/pi.py",
#         "sparkSubmitParameters": "--conf spark.executor.instances=2 --conf spark.executor.memory=2G --conf spark.executor.cores=2 --conf spark.driver.cores=1"
#         }
#     }'

# aws emr-containers start-job-run \
#     --virtual-cluster-id $VIRTUAL_CLUSTER_ID \
#     --name pi-script-s3-neo \
#     --execution-role-arn $RUN_EXECUTION_ROLE_ARN \
#     --release-label emr-6.3.0-latest \
#     --job-driver '{
#     "sparkSubmitJobDriver": {
#         "entryPoint": "s3://aws-virginia-spline-jars/pi.py",
#         "sparkSubmitParameters": "--conf spark.executor.instances=2 --conf spark.executor.memory=2G --conf spark.executor.cores=2 --conf spark.driver.cores=1"
#         }
#     }'

# aws emr-containers start-job-run \
#     --virtual-cluster-id $VIRTUAL_CLUSTER_ID \
#     --name pi-script-s3-neo \
#     --execution-role-arn $RUN_EXECUTION_ROLE_ARN \
#     --release-label emr-6.3.0-latest \
#     --job-driver '{
#     "sparkSubmitJobDriver": {
#         "entryPoint": "s3://aws-virginia-spline-jars/pi.py",
#         "sparkSubmitParameters": "--conf spark.executor.instances=2 --conf spark.executor.memory=2G --conf spark.executor.cores=2 --conf spark.driver.cores=1"
#         }
#     }'

# aws emr-containers start-job-run \
#     --virtual-cluster-id $VIRTUAL_CLUSTER_ID \
#     --name etl-script-s3-neo \
#     --execution-role-arn $RUN_EXECUTION_ROLE_ARN \
#     --release-label emr-6.3.0-latest \
#     --job-driver '{
#     "sparkSubmitJobDriver": {
#         "entryPoint": "s3://aws-virginia-spline-jars/spark_etl.py",
#         "sparkSubmitParameters": "--conf spark.executor.instances=10 --conf spark.executor.memory=2G --conf spark.executor.cores=2 --conf spark.driver.cores=1"
#         }
#     }'

# aws emr-containers start-job-run \
#     --virtual-cluster-id $VIRTUAL_CLUSTER_ID \
#     --name spline-emr-on-eks \
#     --execution-role-arn arn:aws:iam::835751346093:role/emr-on-eks-role-v1-neo-20-long \
#     --release-label emr-6.3.0-latest \
#     --job-driver '{"sparkSubmitJobDriver": {"entryPoint": "s3://aws-virginia-spline-jars/demo_conf.py","sparkSubmitParameters": "--jars s3://aws-virginia-spline-jars/za.co.absa.spline.agent.spark_spark-3.1-spline-agent-bundle_2.12-0.6.2.jar"}}' \
#     --configuration-overrides '{"monitoringConfiguration": {"cloudWatchMonitoringConfiguration": {"logGroupName": "log_group_name", "logStreamNamePrefix": "log_stream_prefix"}}}'

# 集群删除
# # 1. list all jobs
# aws emr-containers list-job-runs --virtual-cluster-id $VIRTUAL_CLUSTER_ID

# # 2. cancel running jobs
# aws emr-containers cancel-job-run --id $VIRTUAL_CLUSTER_ID <job-run-id >--virtual-cluster-id

# # 3. delete virtual cluster
# aws emr-containers delete-virtual-cluster --id $VIRTUAL_CLUSTER_ID

# # 4. delete eks cluster
# eksctl delete cluster --region $REGION --name $EKS_CLUSTER_NAME
