# 磁盘挂载相关

# 找到磁盘
# alias real_disk='sudo fdisk -l | grep "/dev/" | grep "Linux\|HPFS\|exFAT\|NTFS"  | bcat '

alias real_disk='sudo fdisk -l | grep "Disk" | grep "/dev/s\|/dev/nvme"  | lat '
# 磁盘格式化
# 第一个参数为格式化的磁盘，只需要输入磁盘的名字
# 第二个参数为格式化的类型，默认格式化为 ext4格式
format_disk(){
sudo mkfs -t ${2:-ext4} /dev/$1
# sudo mkfs -t ext4 /dev/sdd1 
}



#  磁盘挂载
# sudo mount /dev/sdd1 /exdata 

# # 磁盘卸载
# sudo umount /exdata


up_disk_forever(){
echo '编辑  /etc/fstab，格式如下'
echo 🚚🚚🚚🚚🚚🚚🚚🚚🚚🚚🚚🚚🚚🚚🚚🚚🚚🚚🚚🚚🚚🚚🚚🚚🚚🚚🚚🚚🚚🚚🚚🚚🚚🚚🚚🚚🚚🚚🚚🚚🚚🚚🚚    
echo '/dev/sda2                                 /data_a         ext4    defaults        1         1'
sleep 5;
sudo vim /etc/fstab
}


disk_info(){
sudo dumpe2fs -h ${1:-/dev/sda} # real_disk 磁盘名
}

disk_uuid(){
sudo blkid ${1:-/dev/sda} 
}


# vim /etc/fstab 永久性挂载配置
# 在文件中添加如下：/dev/sdb1       /opt      ext3   defaults   1 1
# mount ：查看分区格式
#       -a 挂载信息立即生效
#       -t ext3 /dev/sdb1 /opt 临时挂载linux分区
#       -t vfat /dev/sdc1 //media/usb u盘挂载 window分区
#       -o loop docs.iso /media/iso 挂载镜像文件
# mount media/cdrom 光驱挂载
# umount /opt 卸载挂载

# 可以看到，fstab文件其实就是一个表格，表格各列的含意如下：
# 第一列：磁盘分区名/卷标，一般是/dev/sdaN（N表示正整数）
# 第二列：挂载点，我们在这里把/dev/sda1挂到/samba上。
# 第三列：缺省设置，一般用defautls。
# 第四列：是否备份：0——表示不做 dump 备份；1——表示要将整个 <fie sysytem> 里的内容备份；2 也表示要做 dump 备份，但该分区的重要性比1 小。
# 第五列：检测顺序：0——不进行检测；根分区/，必须填写1，其它的都不能填写1。如果有分区填写大于1 的话，则在检查完根分区后，从小到大依次检查下去。

# 作者：随心云
# 链接：https://www.jianshu.com/p/7dc8f2a01c27
# 来源：简书
# 简书著作权归作者所有，任何形式的转载都请联系作者获得授权并注明出处。