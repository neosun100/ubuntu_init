
# https://support.apple.com/zh-cn/HT201710

# https://lizhiyong2000.github.io/2019/03/30/mac%E4%B8%AD%E9%85%8D%E7%BD%AEnfs-server/
# mac nfs server

# sudo nfsd enable

# sudo nfsd disable

# sudo nfsd start

# sudo nfsd stop

# sudo nfsd restart

# sudo nfsd status

kickstart_help(){
sudo /System/Library/CoreServices/RemoteManagement/ARDAgent.app/Contents/Resources/kickstart -help
}



kickstart_start(){
sudo /System/Library/CoreServices/RemoteManagement/ARDAgent.app/Contents/Resources/kickstart -restart -agent && \
sudo /System/Library/CoreServices/RemoteManagement/ARDAgent.app/Contents/Resources/kickstart -activate \
-configure \
-allowAccessFor \
-allUsers \
-privs \
-all \
-clientopts \
-setmenuextra \
-menuextra yes;
}



kickstart_stop(){
sudo /System/Library/CoreServices/RemoteManagement/ARDAgent.app/Contents/Resources/kickstart -deactivate -configure -access -off
}