# curl -s https://mvnrepository.com/artifact/org.neo4j/neo4j-jdbc-driver/latest

get_pakage_latest() {
    Artifact_ID=${1:-kudu-client-tools}
    # curl -s "https://search.maven.org/solrsearch/select?q=a:${Artifact_ID}&start=0&rows=20" | jq '.response.docs[0] | .id, .latestVersion ' | pass_quotes | rs | awk '{ gsub(/  /," "); print $0 }' | t2mformat
    curl -s "https://search.maven.org/solrsearch/select?q=a:${Artifact_ID}&start=0&rows=20" | jq '.response.docs[0] | .id, .latestVersion ' | pass_quotes | h2l | awk '{ gsub(/  /," "); print $0 }' | t2mformat

}
