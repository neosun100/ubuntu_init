install_aliyun_cli() {
    # 检测操作系统和架构
    OS="$(uname -s)"
    ARCH="$(uname -m)"

    # 设置下载链接的前缀和版本
    URL_PREFIX="http://cdn.neo.pub/image"
    VERSION="3.0.198"

    # 根据操作系统和架构选择正确的下载链接
    if [ "$OS" = "Linux" ] && [ "$ARCH" = "x86_64" ]; then
        FILE_NAME="aliyun-cli-linux-$VERSION-amd64.tgz"
    elif [ "$OS" = "Darwin" ] && [ "$ARCH" = "arm64" ]; then
        FILE_NAME="aliyun-cli-macosx-$VERSION-arm64.tgz"
    else
        echo "Your system ($OS $ARCH) is not supported by this installer."
        return 1
    fi

    # 构建完整的下载URL
    DOWNLOAD_URL="$URL_PREFIX/$FILE_NAME"

    # 下载文件
    echo "Downloading $FILE_NAME..."
    curl -L "$DOWNLOAD_URL" -o "/tmp/$FILE_NAME"

    # 解压到/tmp目录
    tar -xzf "/tmp/$FILE_NAME" -C /tmp

    # 移动到/usr/local/bin
    sudo mv /tmp/aliyun /usr/local/bin

    # 验证安装
    if [ -x "$(command -v aliyun)" ]; then
        echo "Aliyun CLI installation successful."
        aliyun version
        aliyun configure
    else
        echo "Aliyun CLI installation failed."
        return 1
    fi
}
