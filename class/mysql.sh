# mysql 常规查询

# 内网连接

# 查询 auto_model_pipline_state
# neo table model pipline
# nt
ntmp() {
    mycli mysql://root:LOOP2themoon@rm-uf6rozhp0o3za68tt.mysql.rds.aliyuncs.com:3306/ac \
        -e "select oId,esindex,market_id,state,date_format(update_time,'%m-%d %H:%i') as update_time,substring(model_local,12) as model_local,train_score  from model_state; " \
        -t | lcat
}

# neo table trading log lastest
nttll() {
    mycli mysql://root:LOOP2themoon@rm-uf6rozhp0o3za68tt.mysql.rds.aliyuncs.com:3306/loop_coin \
        -e "select ut.name,ot.* from (select user_id,FROM_UNIXTIME(time_key) as trading_time,creat_time as insert_time, UNIX_TIMESTAMP(creat_time) - time_key as delay_time, exchange,symbol,side,size,avg_price from coin_operation order by _id desc limit ${1:-20}) as ot LEFT JOIN coin_user as ut on ot.user_id=ut.id;" \
        -t | lcat
}

create_user_demo() {
    echo "CREATE USER 'looper'@'%' IDENTIFIED BY 'password'; " | lcat &&
        echo "GRANT ALL PRIVILEGES ON *.* TO 'looper'@'%';" | lcat &&
        echo "FLUSH PRIVILEGES;" | lcat
    sleep 5
    echo "访问权限修改"
    sudo vim /usr/local/etc/my.cnf
}

set_mysql_aws_tesla() {
    export MYSQL_USER="root"
    export MYSQL_PASSWORD="AWS2themoon"
    export MYSQL_URL="aurora-mysql-8-tesla-instance-1.cjcyzs87nm4g.us-east-1.rds.amazonaws.com"
    export MYSQL_URL_READ_ONLY="aurora-tesla-instance-1-us-east-1a.cjcyzs87nm4g.us-east-1.rds.amazonaws.com"
}

mysql_tesla_upsert() {
    # 1. fix teslaMeta.txt 自增ID起始点
    # 1.1 ID起始点
    NUMBER_OFFSET="$(mycli mysql://${MYSQL_USER}:${MYSQL_PASSWORD}@${MYSQL_URL}:3306/tesla --execute "select max(id)+1 as max_id from tesla.taxi_order;" --csv | cutnum)"
    # 不存在则定义为1
    [[ $NUMBER_OFFSET ]] && echo $NUMBER_OFFSET || typeset NUMBER_OFFSET=1

    # 1.2 修改~/upload/teslaMeta.txt
    sed -i "1s/.*/id||int||自增id[:inc(id,${NUMBER_OFFSET})]/" ~/upload/teslaMeta.txt

    # 2. insert
    datafaker rdb \
        "mysql+mysqldb://${MYSQL_USER}:${MYSQL_PASSWORD}@${MYSQL_URL}:3306/tesla?charset=utf8mb4" \
        taxi_order \
        10 \
        --meta ~/upload/teslaMeta.txt \
        --interval 1 \
        --batch 5 \
        --worker 2

    # 3. update
    mycli "mysql://${MYSQL_USER}:${MYSQL_PASSWORD}@${MYSQL_URL}:3306/tesla" \
        --execute "UPDATE tesla.taxi_order
                   SET status  = status + 1,
                       eventTS = unix_timestamp()
                   WHERE createTS > unix_timestamp() - 5
                    and status < 3;"

}
