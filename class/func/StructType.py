# aws kinesis put-record --stream-name test --partition-key 1 --data 'Kinesis'
# aws kinesis put-record --stream-name test --partition-key 1 --data 'Connector'
# aws kinesis put-record --stream-name test --partition-key 1 --data 'for'
# aws kinesis put-record --stream-name test --partition-key 1 --data 'Apache'
# aws kinesis put-record --stream-name test --partition-key 1 --data 'Spark'


# export PYSPARK_SUBMIT_ARGS = --master local[2] --packages org.apache.spark:spark-streaming-kinesis-asl_2.11:2.1.0 pyspark-shell


import pyspark.sql.functions as fn
from pyspark.sql.types import *
from pyspark.sql.types import StructType
from pyspark.sql.functions import explode
from pyspark.sql.functions import split
from pyspark.sql.functions import from_json

spark.sql("SET spark.sql.streaming.metricsEnabled=true").show(10, False)
# 在启用此配置之后，所有在 SparkSession 中启动的查询将通过 Dropwizard 向任何已配置的接收器报告指标(例如 Ganglia、 Graphite、 JMX 等)。
spark.sql("SET spark.sql.streaming.forceDeleteTempCheckpointLocation=true").show(
    10, False)
spark.conf.set("spark.sql.streaming.schemaInference", True)

spark.conf.set("spark.sql.spark.sql.adaptive.enabled", True)
# 配置是否启用自适应执行功能。注意：AQE特性与DPP（动态分区裁剪）特性同时开启时，SparkSQL任务执行中会优先执行DPP特性，从而使得AQE特性不生效。默认值 false
spark.conf.set("spark.sql.optimizer.dynamicPartitionPruning.enabled", True)
# 动态分区裁剪功能的开关。


# Kafka 2 trigger once


kafkaDF.withColumn("json", col("value").cast(StringType())
                   ).select("json")\
                    .writeStream.option("checkpointLocation", "/home/hadoop/checkpoint-once/")\
                    .trigger(once=True)\
                    .foreachBatch(lambda batch_df, batch_id: batch_df.collect())\
                    .start().awaitTermination()
                                                                                                                                                                      batchDF.write.text(
                                                                                                                                                                          "/tmp/cdc-" + timeStamp)
                                                                                                                                                  }.start()
kafkaDF.withColumn("json", col("value").cast(StringType())\
                   ).select("json")\
                    .writeStream.option("checkpointLocation", "/home/hadoop/checkpoint-once/")\
                    .trigger(once=True)\
                    .foreachBatch(lambda batch_df, batch_id:print(batch_df.collect()))\
                    .start().awaitTermination()


kafkaDF.withColumn("json", col("value").cast(StringType())\
                   ).select("json")\
                    .writeStream.option("checkpointLocation", "/home/hadoop/checkpoint-once/")\
                    .trigger(once=True)\
                    .foreachBatch(lambda batch_df, batch_id:batch_df.show())\
                    .start().awaitTermination()



kafkaDF.withColumn("json", col("value").cast(StringType())\
                   ).select("json")\
                    .writeStream.option("checkpointLocation", "/home/hadoop/checkpoint-once/")\
                    .trigger(once=True)\
                    .foreachBatch(lambda batch_df, batch_id:print(batch_df.iat[0,0]))\
                    .start().awaitTermination()

kafkaDF.withColumn("json", col("value").cast(StringType())\
                   ).select("json")\
                    .writeStream.option("checkpointLocation", "/home/hadoop/checkpoint-once/")\
                    .trigger(once=True)\
                    .foreachBatch(lambda batch_df, batch_id:print(batch_df.collect()[0][0]))\
                    .start().awaitTermination()



kafkaDF.withColumn("json", col("value").cast(StringType())\
                   ).select("json")\
                    .writeStream.option("checkpointLocation", "/home/hadoop/checkpoint-once/")\
                    .trigger(once=True)\
                    .foreachBatch(lambda batch_df, batch_id:print(fn.schema_of_json(batch_df.collect()[0][0])))\
                    .start().awaitTermination()




def df_2_hudi_sigle_table(batch_df, batch_id):
    """
    同结构df写入hudi
    """
    df = spark.range(1)
    oneJson = batch_df.collect()[0][0]
    schema = df.select(fn.schema_of_json(fn.lit(f'{oneJson}')).alias("json")).collect()[0][0]

    batch_df.select(from_json("json", schema)).show()



kafkaDF.withColumn("json", col("value").cast(StringType())\
                   ).select("json")\
                    .writeStream\
                    .option("checkpointLocation", "/home/hadoop/checkpoint-all/")\
                    .trigger(processingTime='30 seconds')\
                    .foreachBatch(df_2_hudi_sigle_table)\
                    .start().awaitTermination()



tableName = "py_taxi_order"
basePath = f"s3a://app-util-hudi/spark/{tableName}" 
spark.read.format("hudi").load(basePath).createOrReplaceTempView("ods_tesla_taxi_order")
commits = list(map(lambda row: row[0], spark.sql("select distinct(_hoodie_commit_time) as commitTime from  ods_tesla_taxi_order order by commitTime").limit(50).collect()))

beginTime = "000" # Represents all commits > this time.
endTime = "2022-02-20 00:00:00.000"



spark.sql("select cardDate, count(id) as order_sum, sum(cast(money as double)) as money_sum from ods_tesla_taxi_order group by cardDate").createOrReplaceTempView("init_agg")



incremental_read_options = {
  'hoodie.datasource.query.type': 'incremental',
  'hoodie.datasource.read.begin.instanttime': beginTime,
  'hoodie.datasource.read.end.instanttime': endTime
}


odslDF = spark.read.format("hudi"). \
  options(**incremental_read_options). \
  load(basePath)
odslDF.createOrReplaceTempView("ods_tesla_taxi_order")


aggName = "py_taxi_order_agg"
aggPath = f"s3a://app-util-hudi/spark/{aggName}" # S3


aggDF.write.format("hudi")\
.option("hoodie.datasource.write.table.type", "COPY_ON_WRITE")\
.option("hoodie.table.name", aggName)\
.option("hoodie.datasource.write.operation", "upsert")\
.option("hoodie.datasource.write.recordkey.field", "cardDate")\
.option("hoodie.datasource.write.precombine.field", "ts")\
.option("hoodie.datasource.write.keygenerator.class", "org.apache.hudi.keygen.NonpartitionedKeyGenerator")\
.option("hoodie.clean.async", "true")\
.option("hoodie.clean.automatic", "true")\
.option("hoodie.cleaner.commits.retained", "2")\
.option("hoodie.keep.min.commits", "3")\
.option("hoodie.keep.max.commits", "4")\
.option("hoodie.datasource.hive_sync.enable", "true")\
.option("hoodie.datasource.hive_sync.mode", "hms")\
.option("hoodie.datasource.hive_sync.database", "tesla")\
.option("hoodie.datasource.hive_sync.table", aggName)\
.option("hoodie.datasource.hive_sync.partition_extractor_class", "org.apache.hudi.hive.NonPartitionedExtractor")\
.option("hoodie.datasource.write.payload.class","org.apache.hudi.common.model.DefaultHoodieRecordPayload")\
.mode("append")\
.save(aggPath)


increAggDF = ss.sql(
        """select i.cardDate, i.status, cast(unix_timestamp() as string) as ts,
          | (i.order_sum + s.order_sum) as order_sum, (i.money_sum + s.money_sum) as money_sum
          | from incre_agg i join taxi_order_agg s on i.cardDate = s.cardDate and i.status = s.status"""


select 
i.cardDate, 
cast(unix_timestamp() as string) as ts,
(i.order_sum + a.order_sum) as order_sum,
(i.money_sum + a.money_sum) as money_sum
from ads_tesla_agg_taxi_order_incremental i join ads_tesla_agg_taxi_order a on i.cardDate = a.cardDate