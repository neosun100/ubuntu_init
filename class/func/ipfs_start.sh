# 启动 ipfs 做准备
if [ ! -d ~/upload ];then
  mkdir ~/upload;
  chmod 777 ~/upload;
  echo "`date +'%Y-%m-%d %H:%M:%S'` 上传目录已创建并777！"
else
  echo "`date +'%Y-%m-%d %H:%M:%S'` 上传目录存在！"
fi


if [ ! -d ~/upload/ipfs_data ];then
  mkdir ~/upload/ipfs_data;
  chmod 777 ~/upload/ipfs_data;
  echo "`date +'%Y-%m-%d %H:%M:%S'` ipfs_data 已创建并777！"
else
  echo "`date +'%Y-%m-%d %H:%M:%S'` ipfs_data 存在！"
fi


if [ ! -d ~/upload/ipfs_stag ];then
  mkdir ~/upload/ipfs_stag;
  chmod 777 ~/upload/ipfs_stag;
  echo "`date +'%Y-%m-%d %H:%M:%S'` ipfs_stag 已创建并777！"
else
  echo "`date +'%Y-%m-%d %H:%M:%S'` ipfs_stag 存在！"
fi


# 开启ipfs
sudo docker run -d \
                --name ipfs_host \
                -v /home/neo/upload/ipfs_stag:/export \
                -v /home/neo/upload/ipfs_data:/data/ipfs \
                -p 0.0.0.0:4001:4001 \
                -p 0.0.0.0:8080:8080 \
                -p 0.0.0.0:5001:5001 \
                ipfs/go-ipfs:latest;

# 5. 加入网络
sudo docker exec ipfs_host ipfs swarm peers;


