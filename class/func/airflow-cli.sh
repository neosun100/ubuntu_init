airflow_cli() {
    # 默认第一个mwaa集群
    CLI=${1:-"dags list"}
    CLI_JSON=$(aws mwaa create-cli-token --name $(aws mwaa list-environments | jq '.Environments[1]' | sed 's/"//g')) &&
        CLI_TOKEN=$(echo $CLI_JSON | jq -r '.CliToken') &&
        WEB_SERVER_HOSTNAME=$(echo $CLI_JSON | jq -r '.WebServerHostname') &&
        echo $WEB_SERVER_HOSTNAME &&
        CLI_RESULTS=$(curl -s --request POST "https://$WEB_SERVER_HOSTNAME/aws_mwaa/cli" \
            --header "Authorization: Bearer $CLI_TOKEN" \
            --header "Content-Type: text/plain" \
            --data-raw $CLI) &&
        echo "Output:" &&
        echo $CLI_RESULTS | jq -r '.stdout' | base64 --decode &&
        echo "Errors:" &&
        echo $CLI_RESULTS | jq -r '.stderr' | base64 --decode
}


# CLI_JSON=$(aws mwaa create-cli-token --name $(aws mwaa list-environments | jq '.Environments[0]' | sed 's/"//g')) && \
# CLI_TOKEN=$(echo $CLI_JSON | jq -r '.CliToken') && \
# WEB_SERVER_HOSTNAME=$(echo $CLI_JSON | jq -r '.WebServerHostname') && \
# curl --request PUT \
#   --url https://$WEB_SERVER_HOSTNAME/api/v1/variables/my_var \
#   --header "Authorization: Bearer $CLI_TOKEN" \
#   --header 'Content-Type: text/plain' \
#   --data-raw '{
#     "key": "my_var",
#     "value": "my_value"
#   }'



# CLI_JSON=$(aws mwaa create-cli-token --name $(aws mwaa list-environments | jq '.Environments[0]' | sed 's/"//g')) && \
# CLI_TOKEN=$(echo $CLI_JSON | jq -r '.CliToken') && \
# WEB_SERVER_HOSTNAME=$(echo $CLI_JSON | jq -r '.WebServerHostname') && \
# curl --request POST \
#   --url https://$WEB_SERVER_HOSTNAME/aws_mwaa/cli \
#   --header "Authorization: Bearer $CLI_TOKEN" \
#   --header 'Content-Type: text/plain' \
#   --data-raw 'dags list'