source ~/.bash_profile
[[ -d /Volumes/backup ]] &&
    (remove_disk /Volumes/backup && notify "磁盘卸载" "Time machine disk has been uninstalled") ||
    (echo "$(date +'%Y-%m-%d %H:%M:%S') 当前环境不存在 Time machine disk " | lcat)
