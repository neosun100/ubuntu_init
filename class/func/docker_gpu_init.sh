lspci | grep -i nvidia

curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | sudo apt-key add -

distribution=$(
    . /etc/os-release
    echo $ID$VERSION_ID
)

curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | sudo tee /etc/apt/sources.list.d/nvidia-docker.list

sudo apt-get update

sudo apt-get install -y nvidia-docker2

sudo tee /etc/docker/daemon.json <<EOF
{                                                                                                                        
    "runtimes": {                                                                             
        "nvidia": {                                                         
            "path": "/usr/bin/nvidia-container-runtime",                            
            "runtimeArgs": []                                                                                            
        }                                                                         
    }                                                                                    
}                                                                                             
EOF

sudo pkill -SIGHUP dockerd

sudo docker run --runtime=nvidia --rm nvidia/cuda:9.0-base nvidia-smi
