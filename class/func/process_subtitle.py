import re
from datetime import datetime
import argparse

def clear_srt(filepath, max_duration, highlight_color):
    """
    处理字幕文件，去除空格，高亮文本，删除超长段落。

    Args:
        filepath: 字幕文件路径。
        max_duration: 最大段落持续时间（秒）。
        highlight_color: 高亮文本的颜色。
    """
    output_lines = []
    with open(filepath, 'r', encoding='utf-8') as f:
        lines = f.readlines()

    i = 0
    while i < len(lines):
        subtitle_number = lines[i].strip()
        i += 1
        time_range_line = lines[i].strip()
        i += 1
        subtitle_text_lines = []
        while i < len(lines) and lines[i].strip():
            subtitle_text_lines.append(lines[i].strip())
            i += 1
        i += 1

        try:
            start_time_str, end_time_str = time_range_line.split(' --> ')
            start_time = datetime.strptime(start_time_str.split(',')[0], '%H:%M:%S')
            end_time = datetime.strptime(end_time_str.split(',')[0], '%H:%M:%S')
            duration = (end_time - start_time).total_seconds()

            if duration <= max_duration:
                processed_text_lines = []
                for line in subtitle_text_lines:
                    text_no_space = line.replace(" ", "")
                    highlighted_text = re.sub(r'<u>(.*?)</u>', 
                                              f'<font color="{highlight_color}">'
                                              r'\1</font>', 
                                              text_no_space)
                    processed_text_lines.append(highlighted_text)

                output_lines.append(subtitle_number)
                output_lines.append(time_range_line)
                output_lines.extend(processed_text_lines)
                output_lines.append("")
            else:
                print(f"删除段落 {subtitle_number}，时间范围超过{max_duration}秒。")

        except ValueError as e:
            print(f"解析时间范围或字幕文本时出错: {e}")
            continue

    output_filepath = filepath
    with open(output_filepath, 'w', encoding='utf-8') as f_out:
        f_out.write('\n'.join(output_lines))

    print(f"处理完成，结果已保存到: {output_filepath}")


if __name__ == "__main__":
    # 定义颜色选项
    # https://www.sioe.cn/yingyong/yanse-rgb-16/
    color_choices = {
        'black': '黑色',
        'white': '白色',
        'red': '红色',
        'green': '绿色',
        'blue': '蓝色',
        'yellow': '黄色',
        'magenta': '洋红色',
        'cyan': '青色',
        'gray': '灰色',
        'darkred': '深红色',
        'darkgreen': '深绿色',
        'darkblue': '深蓝色',
        'darkyellow': '深黄色',
        'lime': '酸橙色',
        'darkcyan': '深青色',
        'darkgray': '深灰色',
        'lightgray': '浅灰色',
        'brown': '棕色',
        'orange': '橙色',
        'purple': '紫色',
        'pink': '粉色',
        'gold': '金色',
        'silver': '银色',
        'navy': '海军蓝',
        'teal': '青蓝色',
        'olive': '橄榄绿',
        'maroon': '栗色',
        'lime': '酸橙色',
        'aqua': '水绿色',
        'fuchsia': '紫红色'
    }

    parser = argparse.ArgumentParser(description='处理字幕文件，去除空格，高亮文本，删除超长段落。')
    parser.add_argument('filepath', type=str, help='字幕文件路径')
    parser.add_argument('-d', '--max_duration', type=float, default=2.0, 
                        help='最大段落持续时间（秒），默认为2秒')
    parser.add_argument('-c', '--highlight_color', type=str, default='lime', 
                        choices=list(color_choices.keys()),
                        help='高亮文本的颜色，默认为绿色')
    args = parser.parse_args()

    clear_srt(args.filepath, args.max_duration, args.highlight_color)
