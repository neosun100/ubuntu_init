import os
import argparse
from PIL import Image


def images_to_pdf(image_folder_path, pdf_file_path):
    image_files = os.listdir(image_folder_path)
    image_files.sort(key=lambda x: os.stat(
        os.path.join(image_folder_path, x)).st_mtime)  # 以文件修改时间排序

    with Image.open(os.path.join(image_folder_path, image_files[0])) as first_image:
        pdf_width, pdf_height = first_image.size
        pdf_image = Image.new(
            'RGB', (pdf_width, pdf_height * len(image_files)), (255, 255, 255))

    for idx, image_file in enumerate(image_files):
        with Image.open(os.path.join(image_folder_path, image_file)) as image:
            pdf_image.paste(image, (0, idx * pdf_height))

    pdf_image.save(pdf_file_path, "PDF", resolution=100.0)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Convert a folder of images to a PDF file.')
    parser.add_argument('input', help='The input image folder path.')
    parser.add_argument('output', help='The output PDF file path.')
    args = parser.parse_args()

    images_to_pdf(args.input, args.output)

# python images2pdf.py /Users/jiasunm/截屏/large-language-model output.pdf