from thrift.transport import TSocket
from thrift.protocol import TBinaryProtocol
from thrift.transport import TTransport
from hbase.ttypes import *
from hbase import THBaseService

transport = TTransport.TBufferedTransport(TSocket.TSocket('127.0.0.1', 9090))
protocol = TBinaryProtocol.TBinaryProtocolAccelerated(transport)
client = THBaseService.Client(protocol)
transport.open()

table = 'test_ns:test1'
row = 'row1'

put_columns = [
    TColumnValue('cf'.encode(), '1015'.encode(), '0.28'.encode()),
    TColumnValue('cf'.encode(), '1016'.encode(), '0.35'.encode()),
    TColumnValue('cf'.encode(), '1017'.encode(), '0.25'.encode()),
]
tput = TPut(row.encode(), put_columns)
client.put(table.encode(), tput)

get_columns = [
    TColumn('cf'.encode(), '1015'.encode()),
    TColumn('cf'.encode(), '1016'.encode()),
]
tget = TGet(row.encode(), get_columns)
tresult = client.get(table.encode(), tget)
print(tresult)

transport.close()
