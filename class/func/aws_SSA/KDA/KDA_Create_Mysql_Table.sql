%flink.ssql


create table t_mysql_taxi_order(
    id                 int,
    status             int,
    age                int,
    phone              int,
    email              String,
    ip                 String,
    cardDate           String,
    creditCardNumber   String,
    startAddress       String,
    endAddress         String,
    carNumber          String,
    carType            String,
    userName           String,
    userID             String,
    driverName         String,
    driverRegisterDate String,
    score              String,
    startLatitude      String,
    startLongitude     String,
    endLatitude        String,
    endLongitude       String,
    money              String,
    createTS           int,
    eventTS            int
) 
with ( 
'connector' = 'mysql-cdc',
'hostname' = 'aurora-tesla-instance-1-us-east-1a.cjcyzs87nm4g.us-east-1.rds.amazonaws.com',
'port' = '3306', 
'database-name' = 'tesla',
'table-name' = 'taxi_ordere',
'username' = 'root', 
'password' = 'AWS2themoon');