#!/bin/bash

#
# Derived heavily from the awesome VariaOnboardingScript by Brandon Schwartz (bsschwar@)
# Source: https://code.amazon.com/packages/VariaOnboardingScript/trees/mainline
#

set -Eeuo pipefail

function set_global_options() {
  LAMBDA_NAME="SSMOnboardingLambda"
  LAMBDA_ZIP_NAME="${LAMBDA_NAME}.zip"
  DEFAULT_S3_BUCKET_NAME="ssm-onboarding-bucket"
  S3_URL="s3://"
  CFN_STACK_NAME="PVRE"
  SCRIPT_OPTS=""
  REQUIRED_PARAMS=("REGIONS;r;Regions in the AWS account where you have EC2 instances, separated by a comma and without spaces. Eg: us-east-1,us-west-2"
  )
  HELP_PARAMS=("HELP;h;Display this help message."
  )

  for param in "${REQUIRED_PARAMS[@]}" "${HELP_PARAMS[@]}"; do
    rest=${param#*;}
    opts+=(${rest%%;*}:)
  done

  SCRIPT_OPTS=$(printf "%s" "${opts[@]}")

  for param in "${REQUIRED_PARAMS[@]}"; do
    key=${param%%;*}
    eval $key=""
  done

  # Colors
  RED='\033[1;31m'
  YELLOW='\033[1;33m'
  GREEN='\033[1;32m'
  NC='\033[0m' # No Color
}

function process_command_line() {
  #== Transform long options to short ones ==#)
  for arg in "$@"; do
    shift
    case "${arg}" in
    "--regions") set -- "$@" "-r" ;;
    "--help") set -- "$@" "-h" ;;
    "--"*)
      command_fail "${arg}"
      ;;
    *)
      set -- "$@" "${arg}"
      ;;
    esac
  done
  #== Parse command-line options ==#
  while getopts ${SCRIPT_OPTS} option; do
    case "${option}" in
    r)
      REGIONS=${OPTARG}
      ;;
    h)
      usage
      ;;
    :)
      command_fail "-${OPTARG} requires an argument"
      ;;
    ?)
      command_fail "-${OPTARG}"
      ;;
    *)
      usage
      ;;
    esac
  done
  shift $((OPTIND - 1))
}

function check_local_dependencies() {
  if command -v aws &>/dev/null; then
    AWS=$(command -v aws)
  else
    error "AWS CLI could not be found. Please install it and re-run this script."
  fi

  if command -v zip &>/dev/null; then
    ZIP=$(command -v zip)
  else
    error "ZIP tool is not installed. Please install it and re-run this script."
  fi
}

function test_aws_access() {
  ERROR=$(${AWS} s3 ls 2>&1 >/dev/null) || true
  if [[ -n $ERROR ]]; then
    error "$ERROR"
  fi
}

function validate_and_read_parameters() {
  local answer=""
  local key=""
  local key_lower=""
  local param=""
  AWS_ACCOUNT_ID=$(${AWS} sts get-caller-identity --query 'Account' | tr -d \")
  if ! valid_aws_account_id "$AWS_ACCOUNT_ID"; then
    error "Retrieved Account ID $AWS_ACCOUNT_ID is invalid"
  fi
  printf "Detected AWS Account ID: %s\n" "$(warn "$AWS_ACCOUNT_ID")"
  while [[ ! ${answer:-} =~ ^y && ! ${answer:-} =~ ^n ]]; do
    read -p "Are you sure you want to continue? (y/n): " answer
  done
  shopt -s nocasematch
  [[ ${answer:-} =~ ^y ]] || exit
  shopt -u nocasematch
  for param in "${REQUIRED_PARAMS[@]}"; do
    key=${param%%;*}
    value=${param##*;}
    key_lower=$(awk '{gsub("_", " ", $1); print tolower($0)}' <<<${key})
    while :; do
      if [[ -z ${!key} ]]; then
        read -p "Please enter in your ${key_lower} (enter '?' for description and example): " ${key}
        if [[ ${!key} == "?" ]]; then
          printf '%s\n' "${value}"
          eval $key=""
          continue
        fi
      fi
      if [[ ${key} == "REGIONS" ]]; then
        IFS=","
        read -a REGION_LIST <<<"${!key}"
        unset IFS
        for region in "${REGION_LIST[@]}"; do
          if ! valid_aws_region "${region}"; then
            warn "${region} is not a valid AWS Region. Please try again."
            eval $key=""
            break
          fi
        done
      fi
      [[ -n ${!key} ]] && break
    done
  done
}

function zip_lambda_code() {
  ${ZIP} -rj ${LAMBDA_ZIP_NAME} *.py >/dev/null
}

function create_s3_bucket() {
  ERROR=$(${AWS} s3api --region ${region} head-bucket --bucket "${S3_BUCKET_NAME}" 2>&1 >/dev/null) || true
  if [[ -n $ERROR ]]; then
    if [[ $ERROR == *"404"* ]]; then
      ${AWS} s3 mb "${S3_URL}${S3_BUCKET_NAME}" --region ${region} >/dev/null
      success "Created S3 bucket named ${S3_BUCKET_NAME} in region ${region}"
    else
      error "$ERROR"
    fi
  else
    success "S3 bucket named ${S3_BUCKET_NAME} already exists in region ${region}. Reusing it..."
  fi
}

function upload_lambda_code() {
  ${AWS} s3 cp ${LAMBDA_ZIP_NAME} "${S3_URL}${S3_BUCKET_NAME}" --region ${region} >/dev/null
}

function wait_for_stack_deployment() {
  action=""
  failure_message=""
  stack_name=""
  if [[ $# -eq 0 ]]; then
    action="stack-update-complete"
    failure_message="UPDATE_FAILED"
    stack_name=$CFN_STACK_NAME
  else
    action="stack-create-complete"
    failure_message="CREATE_FAILED"
    # Deleted stacks must be referenced by their stack ID
    stack_name=$1
  fi
  OUTPUT=$(${AWS} cloudformation wait ${action} --region ${region} --stack-name ${CFN_STACK_NAME} 2>&1 >/dev/null) || true
  if [[ $OUTPUT == *"terminal failure state"* ]]; then
    echo $failure_message
    ${AWS} cloudformation describe-stack-events --region ${region} --stack-name $stack_name --query "StackEvents[?ResourceStatus=='${failure_message}' &&  ResourceStatusReason!='Resource creation cancelled'].{Resource:LogicalResourceId, Reason:ResourceStatusReason}"
    exit 127
  fi
}

function deploy_cfn_stacks() {
  if ${AWS} cloudformation describe-stacks --stack-name ${CFN_STACK_NAME} --region ${region} >/dev/null 2>&1; then
    printf 'Cloudformation stack %s already exists in region %s. Trying to update it...\n' ${CFN_STACK_NAME} ${region}
    ERROR=$(${AWS} cloudformation update-stack --region ${region} --stack-name ${CFN_STACK_NAME} --template-body file://PVRE-SSM-onboarding.yaml --capabilities CAPABILITY_NAMED_IAM --parameters ParameterKey=PatchGroupName,ParameterValue=DEV ParameterKey=SSMOnboardingLambdaCodeS3Bucket,ParameterValue=${S3_BUCKET_NAME} 2>&1 >/dev/null) || true
    if [[ -n $ERROR ]]; then
      if [[ $ERROR == *"No updates"* ]]; then
        success "No updates need to be performed"
      else
        error "$ERROR"
      fi
    else
      printf 'Waiting for stack update to be complete (takes around 5 minutes)...\n'
      sleep 5
      wait_for_stack_deployment
      success "Stack successfully updated in region ${region}"
    fi
    printf 'Updating lambda function code...\n'
    ${AWS} lambda update-function-code --region ${region} --function-name ${LAMBDA_NAME} --zip-file "fileb://${LAMBDA_ZIP_NAME}" >/dev/null
    success "Lambda function code updated"
  else
    printf 'Deploying %s Cloudformation stack in region %s\n' ${CFN_STACK_NAME} ${region}
    STACK_ID=$(${AWS} cloudformation create-stack  --region ${region} --stack-name ${CFN_STACK_NAME} --template-body file://PVRE-SSM-onboarding.yaml --capabilities CAPABILITY_NAMED_IAM --on-failure DELETE --parameters ParameterKey=PatchGroupName,ParameterValue=DEV ParameterKey=SSMOnboardingLambdaCodeS3Bucket,ParameterValue=${S3_BUCKET_NAME} --query 'StackId' | tr -d \")
    printf 'Waiting for stack creation to be complete (takes around 5 minutes)...\n'
    sleep 5
    wait_for_stack_deployment $STACK_ID
    success "Stack successfully deployed in region ${region}"
  fi
}

function usage() {
  local desc=""
  local long_param=""
  local rest=""
  local short_param=""

  printf '    %-25s\n' "Usage: $(basename $0)" 1>&2
  printf '\n    %-27s\n' "Required Parameters:" 1>&2
  for param in "${REQUIRED_PARAMS[@]}" "${HELP_PARAMS[@]}"; do
    long_param=$(awk '{gsub("_", "-", $1); print tolower($0)}'<<< ${param%%;*})
    rest=${param#*;}
    short_param=${rest%%;*}
    desc=${rest##*;}
    printf '\t%-30s %-30s\n' "--${long_param}|-${short_param}" "${desc}" 1>&2
  done
  exit
}

function command_fail() {
  printf 'Invalid option: %s\n' ${1} 1>&2
  usage
}

function valid_aws_account_id() {
  [[ "$1" =~ ^[[:digit:]]{12}$ ]]
}

function valid_aws_region() {
  [[ "$1" =~ ^(([[:lower:]]){2})-(gov-)?(iso([[:lower:]])?-)?(central|(north|south)?(east|west)?)-[[:digit:]]+$ ]]
}

function success() {
  display_message "$1" "${GREEN}"
}

function warn() {
  display_message "$1" "${YELLOW}"
}

function error() {
  display_message "$1" "${RED}"
  exit 127
}

function display_message() {
  str=$1
  color=$2
  echo "${color}${str}${NC}"
}

function deploy_regional_resources() {
  for region in "${REGION_LIST[@]}"; do
    S3_BUCKET_NAME="${DEFAULT_S3_BUCKET_NAME}-${AWS_ACCOUNT_ID}-${region}"
    printf "Creating resources in region %s...\n" "$(warn "$region")"
    create_s3_bucket
    upload_lambda_code
    deploy_cfn_stacks
    printf -- '------------------------------------------------------\n'
    S3_BUCKET_NAME=""
  done
}

function main() {
  printf "Welcome to the PVRE-SSM Onboarding Script\n"
  printf -- '------------------------------------------------------\n'
  warn "Only intended for patching EC2 instances in developer and non-critical AWS accounts"
  warn "DO NOT use this script for Production accounts or for EC2 instances with long-running processes"
  warn "Patching will be attempted at 12 a.m UTC (5 p.m. PST) every day and if patches are installed, YOUR INSTANCES WILL BE REBOOTED"

  process_command_line "$@"
  check_local_dependencies
  test_aws_access
  validate_and_read_parameters
  zip_lambda_code
  deploy_regional_resources
  printf 'Done!\n'
}

set_global_options

main "$@"