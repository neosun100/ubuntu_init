# -*- coding: utf-8 -*-
# @Description:
# @Time    : 2022/5/30 12:01 下午}
# @Author  : jiasunm@ Neo Sun
# @Product : PyCharm
# @motto   : It is never too late to learn
# @File    : streamX_setup.sh
# 我已上传 s3 s3://app-util-hudi/scripts/streamX_setup.sh 自用

echo "🚀 🚀 🚀 🚀 🚀 🚀 🚀 "
echo "🚀 Streamx init 🚀 "
echo "🚀 🚀 🚀 🚀 🚀 🚀 🚀 "

echo "修改变量  部署不同版本"
STREAMX_VERSION=1.2.3
SCALA_VERSION=2.12
STREAMX_PATH=/opt/streamx_workspace
PORT=11111

echo "STREAMX_VERSION: $STREAMX_VERSION"
sleep 5

echo "$(date +'%Y-%m-%d %H:%M:%S') 1.安装依赖"
# 增加nodejs源
curl --silent --location https://rpm.nodesource.com/setup_lts.x | sudo bash -
# 安装nodejs
sudo yum install -y nodejs
#  检查nodejs版本
node --version
# 安装pm2
sudo npm install pm2 -g
#  安装express
sudo npm install express -g
# 安装 git
sudo sudo yum install -y git

echo "$(date +'%Y-%m-%d %H:%M:%S') 2.环境变量"
export HADOOP_HOME=/usr/lib/hadoop
export HADOOP_CONF_DIR=/etc/hadoop/conf
export HIVE_HOME=$HADOOP_HOME/../hive
export HBASE_HOME=$HADOOP_HOME/../hbase
export HADOOP_HDFS_HOME=$HADOOP_HOME/../hadoop-hdfs
export HADOOP_MAPRED_HOME=$HADOOP_HOME/../hadoop-mapreduce
export HADOOP_YARN_HOME=$HADOOP_HOME/../hadoop-yarn
export HADOOP_CLASSPATH=$(hadoop classpath)

echo "$(date +'%Y-%m-%d %H:%M:%S') 3.下载streamx bin"

sudo chmod a+w /opt
mkdir -p $STREAMX_PATH
cd $STREAMX_PATH
wget https://github.com/streamxhub/streamx/releases/download/v$STREAMX_VERSION/streamx-console-service_$SCALA_VERSION-$STREAMX_VERSION.tar.gz
tar xvf streamx-console-service_$SCALA_VERSION-$STREAMX_VERSION.tar.gz

echo "$(date +'%Y-%m-%d %H:%M:%S') 4.fix streamx final.sql"
# 修复
# 待验证
sed -i "s|\\\|''|g" $STREAMX_PATH/streamx-console-service-1.2.3/script/final.sql

# 修复建表
replace_row() {
    FILE=${1:-~/upload/teslaMeta.txt}
    ROW_NUM=${2:-1}
    REPLCAE_RESULT=${3:-id||int||自增id[:inc(id,111)]}
    sed -i "${ROW_NUM}s/.*/${REPLCAE_RESULT}/" ${FILE}
}

for i in $(cat $STREAMX_PATH/streamx-console-service-$STREAMX_VERSION/script/final.sql | grep -n "ENGINE=InnoDB AUTO_INCREMENT=100000 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;" | awk '{ gsub(/:/,"\t"); print $0 }' | awk "{print \$${1:-1}}"); do
    echo $i
    replace_row \
        $STREAMX_PATH/streamx-console-service-$STREAMX_VERSION/script/final.sql \
        $i \
        ") ENGINE=InnoDB AUTO_INCREMENT=100000 ROW_FORMAT=DYNAMIC DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;"
done

echo "$(date +'%Y-%m-%d %H:%M:%S') 5.mysql settings"
sudo mysql -uroot -s --prompt=nowarning -e 'set global innodb_large_prefix = 1'
sudo mysql -uroot -s --prompt=nowarning -e 'set global innodb_file_format=BARRACUDA'
sudo mysql -uroot -s --prompt=nowarning -e 'set global innodb_file_per_table = true'
# sudo mysql -uroot -s --prompt=nowarning -e 'drop database streamx'
sudo mysql -uroot -s --prompt=nowarning -e 'create database streamx default character set utf8'

sudo mysql -uroot -s --prompt=nowarning -e "create user 'admin'@'%' identified by '123456';"
sudo mysql -uroot -s --prompt=nowarning -e "grant all privileges on *.* to 'admin'@'%';"
sudo mysql -uroot -s --prompt=nowarning -e "flush privileges;"
sudo mysql -uroot -s --prompt=nowarning -e "show grants for 'admin';"

sudo mysql -uroot -s --prompt=nowarning -D streamx <$STREAMX_PATH/streamx-console-service-$STREAMX_VERSION/script/final.sql

echo "$(date +'%Y-%m-%d %H:%M:%S') 6.修改application.yml"
# vim $STREAMX_PATH/streamx-console-service-$STREAMX_VERSION/conf/application.yml

# $(cat $STREAMX_PATH/streamx-console-service-$STREAMX_VERSION/conf/application.yml | grep -n "  port" | awk '{ gsub(/:/,"\t"); print $0 }' | awk "{print \$${1:-1}}")

echo "6.1 修改端口"
replace_row \
    $STREAMX_PATH/streamx-console-service-$STREAMX_VERSION/conf/application.yml \
    $(cat $STREAMX_PATH/streamx-console-service-$STREAMX_VERSION/conf/application.yml | grep -n "  port" | awk '{ gsub(/:/,"\t"); print $0 }' | awk "{print \$${1:-1}}") \
    "  port: $PORT"

echo "6.2 修改mysql streamx user"
replace_row \
    $STREAMX_PATH/streamx-console-service-$STREAMX_VERSION/conf/application.yml \
    $(cat $STREAMX_PATH/streamx-console-service-$STREAMX_VERSION/conf/application.yml | grep -n "          username" | awk '{ gsub(/:/,"\t"); print $0 }' | awk "{print \$${1:-1}}") \
    "          username: admin"

echo "6.3 修改mysql streamx password"
replace_row \
    $STREAMX_PATH/streamx-console-service-$STREAMX_VERSION/conf/application.yml \
    $(cat $STREAMX_PATH/streamx-console-service-$STREAMX_VERSION/conf/application.yml | grep -n "          password" | awk '{ gsub(/:/,"\t"); print $0 }' | awk "{print \$${1:-1}}") \
    "          password: 123456"

echo "6.4 修改mysql jdbc url"
HOSTNAME=$(hostname)
NUMBER=$(cat $STREAMX_PATH/streamx-console-service-$STREAMX_VERSION/conf/application.yml | grep -n "          url: jdbc:mysql://" | awk '{ gsub(/:/,"\t"); print $0 }' | awk "{print \$${1:-1}}")
sed -i "${NUMBER}d" $STREAMX_PATH/streamx-console-service-$STREAMX_VERSION/conf/application.yml
sed -i "${NUMBER}i\          url: jdbc:mysql://$HOSTNAME:3306/streamx?useSSL=false&useUnicode=true&characterEncoding=UTF-8&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=GMT%2B8" $STREAMX_PATH/streamx-console-service-$STREAMX_VERSION/conf/application.yml

echo "$(date +'%Y-%m-%d %H:%M:%S') 7. 启动 streamx 后端"
cd $STREAMX_PATH/streamx-console-service-$STREAMX_VERSION/bin
bash startup.sh

echo "$(date +'%Y-%m-%d %H:%M:%S')8. jps check"
jps -l | grep streamx
sleep 3
jps -l | grep streamx
sleep 3
jps -l | grep streamx

echo "$(date +'%Y-%m-%d %H:%M:%S')9. 编译 streamx 前端"
cd /opt/streamx_workspace/streamx-console-service-$STREAMX_VERSION
git clone https://github.com/streamxhub/streamx.git
cd /opt/streamx_workspace/streamx-console-service-$STREAMX_VERSION/streamx/streamx-console/streamx-console-webapp
npm install
npm run build
cp -r dist ~/
cp streamx.js ~/

echo "$(date +'%Y-%m-%d %H:%M:%S')10. 启动 streamx 前端"
cd
pm2 start streamx.js
pm2 list

echo "$(date +'%Y-%m-%d %H:%M:%S')11. 登录信息"
echo "内网 domain: http://$(hostname):11111/"
echo "公网 domain http://`curl -s ifconfig.me`:11111 🔥前提有公网ip的话"
echo "初始用户名密码是 admin/streamx"
echo "enjoy it❕"
