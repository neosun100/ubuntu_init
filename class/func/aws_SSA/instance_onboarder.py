import boto3
from config import (LOGGER, SSM_CORE_POLICY_ARN_TO_ATTACH, INSTANCE_PROFILE_ARN_TO_ATTACH, PATCH_GROUP)


class InstanceOnboarder(object):

    def __init__(self):
        """
        Creates the clients
        """
        self.ec2_client = boto3.client('ec2')
        self.iam_client = boto3.client('iam')

    def onboard_instance(self, instance_id: str):
        LOGGER.info(f'Processing for instance {instance_id}')
        self.attach_permissions(instance_id)
        self.attach_tag(instance_id)

    def attach_permissions(self, instance_id: str):
        response = self.ec2_client.describe_iam_instance_profile_associations(
            Filters=[{
                'Name': 'instance-id',
                'Values': [instance_id]
            },
            {
                'Name': 'state',
                'Values': ['associated']
            }]
        )

        # If no instance profile is present, attach our profile
        if not response['IamInstanceProfileAssociations']:
            self.associate_instance_profile(INSTANCE_PROFILE_ARN_TO_ATTACH, instance_id)
            return

        profile_in_response = response['IamInstanceProfileAssociations'][0]
        # Handle case where an already attached instance profile ends up in a weird state after the resource is updated
        # by Cloudformation. The instance profile shows up as "associated" but actually isn't.
        # GetInstanceProfile query throws NoSuchEntity error
        current_instance_profile = self.get_current_instance_profile(profile_in_response)
        if not current_instance_profile:
            self.ec2_client.disassociate_iam_instance_profile(AssociationId=profile_in_response['AssociationId'])
            self.associate_instance_profile(INSTANCE_PROFILE_ARN_TO_ATTACH, instance_id)
            return

        # If policy already exists, do nothing.
        instance_profile_role_name = current_instance_profile['Roles'][0]['RoleName']
        for policy_arn in [SSM_CORE_POLICY_ARN_TO_ATTACH]:
            if not self.policy_exists(instance_profile_role_name, policy_arn):
                # Attach policy to instance role.
                self.iam_client.attach_role_policy(RoleName=instance_profile_role_name, PolicyArn=policy_arn)
                LOGGER.info(f'Attached policy {policy_arn.split(":")[-1].split("/")[-1]} to '
                            f'{instance_profile_role_name} role')

    def get_current_instance_profile(self, instance_profile_arn):
        instance_profile_name = instance_profile_arn['IamInstanceProfile']['Arn'].split("/")[-1]
        try:
            current_instance_profile = self.iam_client.get_instance_profile(InstanceProfileName=instance_profile_name)
            return current_instance_profile["InstanceProfile"]
        except self.iam_client.exceptions.NoSuchEntityException:
            return None

    def policy_exists(self, instance_profile_role_name, policy_arn):
        attached_policies = self.iam_client.list_attached_role_policies(RoleName=instance_profile_role_name)
        for attached_policy in attached_policies['AttachedPolicies']:
            if attached_policy['PolicyArn'] == policy_arn:
                # Policy already present. Do nothing
                LOGGER.info(f'Policy is already present. Nothing to be done here.')
                return True
        return False

    def associate_instance_profile(self, instance_profile, instance_id):
        LOGGER.info(f'No instance profiles associated with this instance. Creating one...')
        response = self.ec2_client.associate_iam_instance_profile(
            IamInstanceProfile={
                'Arn': INSTANCE_PROFILE_ARN_TO_ATTACH
            },
            InstanceId=instance_id
        )
        LOGGER.info(f'Attached instance profile {INSTANCE_PROFILE_ARN_TO_ATTACH.split("/")[-1]} to instance '
                    f'{instance_id}')

    def attach_tag(self, instance_id: str):
        attached_tag = self.ec2_client.describe_tags(
            Filters=[
                {
                    'Name': 'resource-id',
                    'Values': [instance_id]
                },
                {
                    'Name':  'tag:Patch Group',
                    'Values': [PATCH_GROUP]
                }
            ]
        )
        if attached_tag["Tags"]:
            LOGGER.info(f'Tag value {PATCH_GROUP} already attached to instance {instance_id}. Nothing to do here')
        else:
            response = self.ec2_client.create_tags(
                Resources=[instance_id],
                Tags=[
                    {
                        'Key': 'Patch Group',
                        'Value': PATCH_GROUP
                    },
                ]
            )
            LOGGER.info(f'Attached Tag value {PATCH_GROUP} to instance {instance_id}')
