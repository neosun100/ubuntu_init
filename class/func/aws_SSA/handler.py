from config import LOGGER
from instance_onboarder import InstanceOnboarder


def event_handler(event, context):
    """
    Handler for SSM Onboarding lambda
    Attaches policies required by SSM to instance roles
    """
    instance_onboarder = InstanceOnboarder()
    if 'instance_id' in event:
        # Event from PolicyAttacher SSM document invoking this lambda
        LOGGER.info(f'document event detected: {event}')
        instance_onboarder.onboard_instance(instance_id=event['instance_id'])
    elif 'detail' in event and 'instance-id' in event['detail']:
        # Event from EC2 instance launch detected by the Event rule
        LOGGER.info(f'event detected: {event}')
        instance_onboarder.onboard_instance(instance_id=event['detail']['instance-id'])
    else:
        LOGGER.error(f'Invalid event detected: {event}')