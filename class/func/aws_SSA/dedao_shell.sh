for i in $(seq 0 ${TOTAL_NUM}); do
    echo "$(date +'%Y-%m-%d %H:%M:%S') 当前下载的UID 为 $i"
    echo "$(date +'%Y-%m-%d %H:%M:%S') 查询下载的听书ID"
    DEDAO_ID=$(cat dedao_df.txt |
        sed '/+/d' |
        tr -s ' ' |
        head -n 99999 |
        awk '{ gsub(/》\| /,"》"); print $0 }' |
        awk '{ gsub(/\| 2023/,"neo | 2023"); print $0 }' |
        awk '{ gsub(/Pro版 \| /,"Pro版"); print $0 }' |
        awk '{ gsub(/\| # /,"| UID "); print $0 }' |
        sed 's/ //g' |
        jc --csv |
        json2csv | csvq -f json "select ID from stdin where UID = $i" | jq '.[].ID' | pass_quotes)
    echo "$(date +'%Y-%m-%d %H:%M:%S') 听书ID 为 $DEDAO_ID"
    echo "$(date +'%Y-%m-%d %H:%M:%S') 开始下载 $DEDAO_ID 音频"
    ./dedao-dl dlo $DEDAO_ID -t 1
    echo "$(date +'%Y-%m-%d %H:%M:%S') 开始下载 $DEDAO_ID Markdown"
    ./dedao-dl dlo $DEDAO_ID -t 3
    echo "$(date +'%Y-%m-%d %H:%M:%S') 开始下载 dedaoid=$DEDAO_ID UID=$i 完成完成 ✅"
    echo "目前进度 $(eva $i/${TOTAL_NUM})"
done

# ---
# 加强版，增加去重下载
for i in $(seq 0 ${TOTAL_NUM}); do
    echo "$(date +'%Y-%m-%d %H:%M:%S') 当前下载的UID 为 $i"
    echo "$(date +'%Y-%m-%d %H:%M:%S') 查询下载的听书ID"
    DEDAO_ID=$(cat dedao_df.txt |
        sed '/+/d' |
        tr -s ' ' |
        head -n 99999 |
        awk '{ gsub(/》\| /,"》"); print $0 }' |
        awk '{ gsub(/\| 2023/,"neo | 2023"); print $0 }' |
        awk '{ gsub(/Pro版 \| /,"Pro版"); print $0 }' |
        awk '{ gsub(/\| # /,"| UID "); print $0 }' |
        sed 's/ //g' |
        jc --csv |
        json2csv | csvq -f json "select ID from stdin where UID = $i" | jq '.[].ID' | pass_quotes)
    echo "$(date +'%Y-%m-%d %H:%M:%S') 听书ID 为 $DEDAO_ID"
    echo "$(date +'%Y-%m-%d %H:%M:%S') 检查是否已存在"
    STRING=$(l output/每天听本书/MD | grep $(cat dedao_df.txt | fields '$4,$5,$6,$7,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18' | grep "2170" | awk '{ gsub(/ \|/,"\t"); print $0 }' | field 2))
    if [ -z "$STRING" ]; then
        echo "STRING is empty"
        echo "$(date +'%Y-%m-%d %H:%M:%S') 听书ID $i 已下载 ✅"
    else
        echo "STRING not is empty"
        echo "$(date +'%Y-%m-%d %H:%M:%S') 听书ID $i 未下载 ❌"
        echo "$(date +'%Y-%m-%d %H:%M:%S') 开始下载 $DEDAO_ID 音频"
        ./dedao-dl dlo $DEDAO_ID -t 1
        echo "$(date +'%Y-%m-%d %H:%M:%S') 开始下载 $DEDAO_ID Markdown"
        ./dedao-dl dlo $DEDAO_ID -t 3
        echo "$(date +'%Y-%m-%d %H:%M:%S') 开始下载 dedaoid=$DEDAO_ID UID=$i 完成完成 ✅"
    fi
    echo "目前进度 $(eva $i/${TOTAL_NUM})"

done
