from datetime import timedelta
from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.sensors.external_task_sensor import ExternalTaskSensor
from airflow.utils.dates import days_ago

# DAG_hour
dag_hour = DAG(
    'DAG_hour',
    start_date=days_ago(3),
    schedule_interval=timedelta(hours=1),
    tags=['ExternalTaskSensor'],
)

start_task_hour = DummyOperator(
    task_id='start_task_hour',
    dag=dag_hour,
)

# Wait for the end of DAG_minute
wait_for_minute = ExternalTaskSensor(
    task_id='wait_for_minute',
    external_dag_id='DAG_minute',
    external_task_id='end_task_minute',
    check_existence=True,
    allowed_states=['success'],
    failed_states=['failed', 'skipped'],
    mode='reschedule',
    poke_interval=60,
    timeout=3600,
    dag=dag_hour
)

end_task_hour = DummyOperator(
    task_id='end_task_hour',
    dag=dag_hour,
)

start_task_hour >> wait_for_minute >> end_task_hour
