from datetime import timedelta
from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.sensors.external_task_sensor import ExternalTaskSensor
from airflow.utils.dates import days_ago

# DAG_day
dag_day = DAG(
    'DAG_day',
    start_date=days_ago(3),
    schedule_interval=timedelta(days=1),
    tags=['ExternalTaskSensor'],
)

start_task_day = DummyOperator(
    task_id='start_task_day',
    dag=dag_day,
)

# Wait for the end of DAG_hour
wait_for_hour = ExternalTaskSensor(
    task_id='wait_for_hour',
    external_dag_id='DAG_hour',
    external_task_id='end_task_hour',
    check_existence=True,
    allowed_states=['success'],
    failed_states=['failed', 'skipped'],
    mode='reschedule',
<<<<<<< HEAD
    poke_interval=60 * 60,  # poke every hour
=======
    #execution_delta=timedelta(days=1, hours=1),
    poke_interval=60,  # 60s
>>>>>>> da771213146e08dcc824d7dbe0c2817a6178cd64
    timeout=24 * 60 * 60,  # timeout after a day
    dag=dag_day
)

end_task_day = DummyOperator(
    task_id='end_task_day',
    dag=dag_day,
)

start_task_day >> wait_for_hour >> end_task_day