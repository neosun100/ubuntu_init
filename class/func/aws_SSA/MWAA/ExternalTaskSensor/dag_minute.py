from datetime import timedelta
from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.utils.dates import days_ago

# DAG_minute
dag_minute = DAG(
    'DAG_minute',
    start_date=days_ago(3),
    schedule_interval=timedelta(minutes=1),
    tags=['ExternalTaskSensor'],
)

start_task_minute = DummyOperator(
    task_id='start_task_minute',
    dag=dag_minute,
)

end_task_minute = DummyOperator(
    task_id='end_task_minute',
    dag=dag_minute,
)

start_task_minute >> end_task_minute
