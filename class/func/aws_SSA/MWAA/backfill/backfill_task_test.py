from datetime import datetime, timedelta
from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime(2023, 11, 6),
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
}

dag = DAG(
    'dw_common_day_dag',
    default_args=default_args,
    description='A simple DAG for backfill demo',
    schedule_interval=timedelta(days=1),
)

start_task = DummyOperator(
    task_id='start_task',
    dag=dag,
)

intermediate_task = DummyOperator(
    task_id='intermediate_task',
    dag=dag,
)

dummy_end_1 = DummyOperator(
    task_id='dummy_end_1',
    dag=dag,
)

dummy_end_2 = DummyOperator(
    task_id='dummy_end_2',
    dag=dag,
)

end_task = DummyOperator(
    task_id='end_task',
    dag=dag,
)

start_task >> intermediate_task >> [dummy_end_1, dummy_end_2] >> end_task
