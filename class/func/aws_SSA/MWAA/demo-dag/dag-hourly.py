from airflow import DAG
from airflow.operators.bash import BashOperator
from datetime import datetime, timedelta

default_args = {
    'owner': 'airflow',
    'start_date': datetime.now() - timedelta(days=3),
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
}

with DAG('dag-hourly',
         default_args=default_args,
         schedule_interval='15 * * * *',
         catchup=True,
        tags=["unexternal_task"]) as dag:

    task = BashOperator(
        task_id='bash_task',
        bash_command='sleep 3; date "+Execution time: %Y-%m-%d %H:%M:%S"; echo "Current task is completed."'
    )