from airflow import DAG
from airflow.operators.bash import BashOperator
from airflow.operators.python import ShortCircuitOperator
from airflow.operators.dummy_operator import DummyOperator
from airflow.api.common.experimental.get_dag_run_state import get_dag_run_state
from datetime import datetime, timedelta

default_args = {
    'owner': 'airflow',
    'start_date': datetime.now() - timedelta(days=3),
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
}

def check_dependent_dag_success(context):
    dag_id = 'dag-hourly'
    execution_date = context['execution_date'] - timedelta(minutes=45)
    state = get_dag_run_state(dag_id, execution_date)
    
    if state == 'success':
        return True
    else:
        return False

with DAG('dag-daily',
         default_args=default_args,
         schedule_interval='5 0 * * *',
         catchup=True,
         tags=["unexternal_task"]) as dag:

    start = DummyOperator(task_id='start')

    check_prev_dag = ShortCircuitOperator(
        task_id='check_prev_dag',
        python_callable=check_dependent_dag_success,
        provide_context=True,
        retries=240,
        retry_delay=timedelta(seconds=30),
    )

    task = BashOperator(
        task_id='bash_task',
        bash_command='echo "dag-daily task executed."'
    )

    start >> check_prev_dag >> task