from datetime import datetime, timedelta
from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python_operator import BranchPythonOperator, PythonOperator

def print_time(ti, **kwargs):
    print(f"Current execution date and time: {ti.execution_date}")

def decide_which_task(*args, **kwargs):
    current_hour = kwargs['execution_date'].hour
    return f'end_task_hour_{current_hour}'

# DAG_hour
dag_hour = DAG(
    'DAG_test_hour_v2',
    start_date=datetime(2023, 8, 3),
    schedule_interval='15 * * * *', # This is cron expression, which means every hour at minute 15.
    tags=['v2_ExternalTaskSensor'],
)

start_task_hour = PythonOperator(
    task_id='start_task_hour',
    python_callable=print_time,
    provide_context=True,
    dag=dag_hour,
)

branch_task = BranchPythonOperator(
    task_id='branch_task',
    python_callable=decide_which_task,
    provide_context=True,
    dag=dag_hour,
)

start_task_hour >> branch_task

for hour in range(24):
    end_task_hour = PythonOperator(
        task_id=f'end_task_hour_{hour}',
        python_callable=print_time,
        provide_context=True,
        dag=dag_hour,
    )
    branch_task >> end_task_hour