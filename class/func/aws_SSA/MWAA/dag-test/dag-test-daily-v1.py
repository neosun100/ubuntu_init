from datetime import datetime
from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.python_operator import PythonOperator
from airflow.sensors.external_task_sensor import ExternalTaskSensor



# Function to print current date and time
def print_datetime():
    print(datetime.now())

# DAG_day
dag_day = DAG(
    'DAG_test_day',
    start_date=datetime(2023, 8, 3),
    schedule_interval='15 0 * * *', # 这是cron表达式，表示每天的0点5分
    tags=['v1_ExternalTaskSensor'],
)

start_task_day = DummyOperator(
    task_id='start_task_day',
    dag=dag_day,
)


print_date_time = PythonOperator(
    task_id='print_date_time',
    python_callable=print_datetime,
    dag=dag_day,
)

# Wait for the end of DAG_hour
wait_for_hour = ExternalTaskSensor(
    task_id='wait_for_hour',
    external_dag_id='DAG_test_hour',
    external_task_id='end_task_hour',
    check_existence=True,
    allowed_states=['success'],
    failed_states=['failed', 'skipped'],
    mode='reschedule',
    execution_delta=timedelta(days=1),
    poke_interval=60,
    timeout=3600,
    dag=dag_day
)

end_task_day = DummyOperator(
    task_id='end_task_day',
    dag=dag_day,
)

start_task_day >> print_date_time >> wait_for_hour >> end_task_day