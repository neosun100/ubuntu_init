from datetime import datetime
from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator

# DAG_hour
dag_hour = DAG(
    'DAG_test_hour',
    start_date=datetime(2023, 8, 3),
    schedule_interval='15 * * * *', # 这是cron表达式，表示每小时的第15分钟
    tags=['v1_ExternalTaskSensor'],
)

start_task_hour = DummyOperator(
    task_id='start_task_hour',
    dag=dag_hour,
)

end_task_hour = DummyOperator(
    task_id='end_task_hour',
    dag=dag_hour,
)

start_task_hour >> end_task_hour