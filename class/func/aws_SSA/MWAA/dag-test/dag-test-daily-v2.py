from datetime import datetime, timedelta
from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.sensors.external_task_sensor import ExternalTaskSensor
from airflow.utils.dates import days_ago
# DAG_day
dag_day = DAG(
    'DAG_test_day_v2',
    start_date=datetime(2023, 8, 4),
    schedule_interval='15 0 * * *', 
    default_args={'retries': 3, 'retry_delay': timedelta(minutes=5)},
    tags=['v2_ExternalTaskSensor'],
)

start_task_day = DummyOperator(
    task_id='start_task_day',
    dag=dag_day,
)

end_task_day = DummyOperator(
    task_id='end_task_day',
    dag=dag_day,
)

start_task_day >> end_task_day

for hour in range(24):
    wait_for_task_hour = ExternalTaskSensor(
        task_id=f'wait_for_end_task_hour_{hour}',
        external_dag_id='DAG_test_hour_v2',
        external_task_id=f'end_task_hour_{hour}',
        execution_delta=timedelta(hours=24-hour), # execution_delta并不是用来检查特定时间段内所有的任务状态。它仅用来指定一个具体的单一时间点，以便查看在那个时间点运行的特定任务的状态
        check_existence=True, # 是否在任务开始时检查外部 DAG 和任务的存在。
        allowed_states=['success'],
        failed_states=['failed','skipped'],
        mode='reschedule', # 定义当传感器尝试时应该做什么。默认值是 "poke"，表示传感器将继续运行并尝试，"reschedule" 表示传感器会释放 worker 插槽。
        poke_interval=60, # 等待下一次尝试的时间间隔（秒）。
        timeout=3600, # 超时时间（秒），如果传感器在这段时间内没有成功，那么任务就会失败。
        dag=dag_day,
    )

    start_task_day >> wait_for_task_hour >> end_task_day