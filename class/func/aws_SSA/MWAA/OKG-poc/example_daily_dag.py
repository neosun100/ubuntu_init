from pendulum import datetime
from datetime import timedelta
from airflow import DAG
from airflow.operators.dummy import DummyOperator
from airflow.operators.bash import BashOperator

from airflow.sensors.external_task import ExternalTaskSensor

with DAG(
    dag_id="example_daily_dag",
    schedule="0 5 * * *",
    start_date=datetime(2023, 7, 11, tz="UTC"),
    catchup=False,
    tags=["external_task"],
) as dag:

    depends_on_hourly_23hr = ExternalTaskSensor(
        task_id="depends_on_hourly_23hr",
        external_dag_id="example_hourly_dag",
        external_task_id="hourly_job",
        execution_delta=timedelta(days=1),
        poke_interval=50*60, # 50min
        # execution_delta=timedelta(hours=-23),

        # poke_interval=MY_POKE_INTERVAL,
        # timeout=MY_TIMEOUT,
        # soft_fail=MY_SOFT_FAIL,
        # mode=MY_MODE,
        # exponential_backoff=MY_EXPONENTIAL_BACKOFF,
        # max_wait=MY_MAX_WAIT,
        # silent_fail=MY_SILENT_FAIL,

        # allowed_states=MY_ALLOWED_STATES,
        # skipped_states=MY_SKIPPED_STATES,
        # failed_states=MY_FAILED_STATES,
        # execution_delta=MY_EXECUTION_DELTA,
        # execution_date_fn=MY_EXECUTION_DATE_FN,
    )


    daily_job = BashOperator(
        task_id="daily_job",
        bash_command="echo 'run me hourly' && date"
    )



    depends_on_hourly_23hr >> daily_job
    
    
    # aws s3 cp example_hourly_dag.py  s3://okx-pri-va/oklogs/prod_dags/