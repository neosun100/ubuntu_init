from datetime import datetime, timedelta
from airflow import DAG
from airflow.operators.bash import BashOperator
from airflow.sensors.external_task import ExternalTaskSensor

with DAG(
    dag_id="example_daily_dag",
    schedule_interval="0 16 * * *",  # UTC时间的16点，相当于东八区的0点
    start_date=datetime(2023, 8, 2, 16),  # UTC时间的16点开始，相当于东八区的0点
    catchup=True,
    tags=["external_task"],
) as dag:

    depends_on_hourly_23hr = ExternalTaskSensor(
        task_id="depends_on_hourly_23hr",
        external_dag_id="example_hourly_dag",
        external_task_id="hourly_job",
        execution_delta=timedelta(days=1),
        poke_interval=10, # 10 seconds
        timeout=3*60*60,  # 3 hours in seconds
        mode="reschedule",
    )

    daily_job = BashOperator(
        task_id="daily_job",
        bash_command="echo 'daily dag执行完成' && date"
    )

    depends_on_hourly_23hr >> daily_job