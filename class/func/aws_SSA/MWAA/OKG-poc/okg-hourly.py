from datetime import datetime, timedelta
from airflow import DAG
from airflow.operators.dummy import DummyOperator
from airflow.operators.bash import BashOperator

with DAG(
    dag_id="example_hourly_dag",
    schedule_interval="0 15 * * *",  # UTC时间的15点，相当于东八区的23点
    start_date=datetime(2023, 8, 2, 15),  # UTC时间的15点开始，相当于东八区的23点
    catchup=True,
    tags=["external_task"],
) as dag:

    start = DummyOperator(
        task_id="start",
    )
    
    delay_60s = BashOperator(
        task_id="delay_60s",
        bash_command="sleep 60s"
    )

    hourly_job = BashOperator(
        task_id="hourly_job",
        bash_command="echo 'hourly dag执行完成' && date"
    )

    start >> delay_60s >> hourly_job