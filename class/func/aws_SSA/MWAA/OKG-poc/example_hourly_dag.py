from pendulum import datetime



from airflow import DAG
from airflow.operators.dummy import DummyOperator
from airflow.operators.bash import BashOperator

default_args = {"retries": 2}

with DAG(
    dag_id="example_hourly_dag",
    schedule="15 * * * *",
    start_date=datetime(2023, 7, 11, tz="UTC"),
    catchup=False,
    tags=["external_task"],
) as dag:

    start = DummyOperator(
        task_id="start",
    )
    
    delay_2h = BashOperator(
        task_id="delay_2h",
        bash_command="sleep 2h"
    )

    hourly_job = BashOperator(
        task_id="hourly_job",
        bash_command="echo 'run me hourly' && date"
    )

    start >> delay_2h >> hourly_job