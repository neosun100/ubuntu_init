from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.sensors.external_task_sensor import ExternalTaskSensor
from datetime import datetime, timedelta

default_args = {
    'owner': 'airflow',
    'start_date': datetime(2023, 1, 1),
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
}

daily_dag = DAG('daily_dag', default_args=default_args, schedule_interval='@daily')

with daily_dag:
    wait_for_hourly_dag = ExternalTaskSensor(
        task_id='wait_for_hourly_dag',
        external_dag_id='hourly_dag',
        external_task_id='run_this_first', # 指定小时级 DAG 的最后一个任务
        check_existence=True,
        mode='reschedule',
        execution_delta=timedelta(days=1), # 等待前一天的 hourly_dag 运行完成
    )

    run_this_after_hourly_dag = DummyOperator(
        task_id='run_this_after_hourly_dag',
    )

run_this_after_hourly_dag



from datetime import datetime, timedelta
from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from pytz import timezone

def print_execution_date(execution_date):
    print(f"The execution date is {execution_date}")

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime(2023, 7, 1, 12, tzinfo=timezone('Asia/Shanghai')), # 设置为东八区12点
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
}

dag = DAG(
    'test_dag',
    default_args=default_args,
    description='A simple test DAG',
    schedule_interval='0 12 * * *', # 每天的12点执行
    catchup=True, 
)

start = PythonOperator(
    task_id='run_this_first',
    python_callable=print_execution_date,
    provide_context=True, # 这是必要的，它将告诉Airflow将上下文变量（包括execution_date）传递给python_callable函数
    dag=dag,
)

end = PythonOperator(
    task_id='run_this_last',
    python_callable=print_execution_date,
    provide_context=True, # 这是必要的，它将告诉Airflow将上下文变量（包括execution_date）传递给python_callable函数
    dag=dag,
)

start >> end


airflow backfill -s 2023-07-01 -e 2023-07-06 test_dag


airflow backfill -s 2023-07-01T00:00 -e 2023-07-01T05:00 test_dag



from datetime import datetime, timedelta
from airflow import DAG
from airflow.operators.python_operator import PythonOperator
from pytz import timezone

def print_execution_date(execution_date, **kwargs):
    print(f"The execution date is {execution_date}")

default_args = {
    'owner': 'airflow',
    'depends_on_past': False,
    'start_date': datetime(2023, 7, 1, 0, tzinfo=timezone('Asia/Shanghai')), # 设置为东八区0点
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
    'retries': 1,
    'retry_delay': timedelta(minutes=5),
}

dag = DAG(
    'test_hourly_dag',
    default_args=default_args,
    description='A simple hourly test DAG',
    schedule_interval='0 * * * *', # 每小时的第0分钟执行
    catchup=False, 
)

task = PythonOperator(
    task_id='print_execution_date',
    python_callable=print_execution_date,
    provide_context=True,
    dag=dag,
)


# 注意⚠️
# execution_date 是以UTC时区表示的
# execution_date 的值代表的是每个任务实例的逻辑运行时间，也就是说，如果一个任务是处理数据的，那么这个日期通常会被用来确定需要处理哪个小时的数据。
# 每个任务实例都有一个 execution_date，这个日期是基于你的DAG的 schedule_interval 和 start_date 计算出来的。当你运行 Backfill 时，Airflow 会为你指定的每一个小时创建一个新的DAG Run，并将那个小时的日期和时间作为 execution_date 传递给你的任务。



# -*- coding: utf-8 -*-
# @Description:
# @Time    : 2022/2/17 2:50 下午}
# @Author  : jiasunm@ Neo Sun
# @Product : PyCharm
# @motto   : It is never too late to learn
# @File    : mwaa2_emr_ohio_pull_virginia_long_running.py.py

from airflow import DAG

# 最新版本需要，mwaa 2.0.2 目前不支持
# from airflow.providers.amazon.aws.operators.emr import EmrCreateJobFlowOperator
# from airflow.providers.amazon.aws.operators.emr import EmrAddStepsOperator
# from airflow.providers.amazon.aws.sensors.emr import EmrStepSensor
# from airflow.operators.dummy import DummyOperator

from airflow.providers.amazon.aws.operators.emr_create_job_flow import EmrCreateJobFlowOperator
from airflow.providers.amazon.aws.operators.emr_add_steps import EmrAddStepsOperator
from airflow.providers.amazon.aws.sensors.emr_step import EmrStepSensor
from airflow.operators.dummy_operator import DummyOperator

from airflow.utils.dates import days_ago
from datetime import timedelta
import os

DAG_ID = os.path.basename(__file__).replace('.py', '')

DEFAULT_ARGS = {
    'owner': 'airflow',
    'depends_on_past': False,
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
}

SPARK_STEPS = [
    {
        # "Name": "Spark-Program-neo-aws-etl",
        "Name": "spark-etl-demo",
        "ActionOnFailure": "CONTINUE",
        "HadoopJarStep": {
            "Jar": "command-runner.jar",
            "Args": [
                "spark-submit",
                "--deploy-mode",
                "cluster",
                "--master",
                "yarn",
                "--conf",
                "spark.yarn.submit.waitAppCompletion=true",
                "s3://neo-ohio-emr-script/spark_etl_simple.py"
            ]
        }
    }
]

SQOOP_STEPS = [
    {
        "Name": "Sqoop_version_check",
        "ActionOnFailure": "CONTINUE",
        "HadoopJarStep": {
            "Jar": "command-runner.jar",
            "Args": [
                "sqoop",
                "version"
            ]
        }
    }

]

HIVE_STEPS = [
    {
        "Name": "Hive ETL demo",
        "ActionOnFailure": "CONTINUE",
        "HadoopJarStep": {
            "Jar": "command-runner.jar",
            "Args": [
                "hive",
                "-e",
                "show databases;select count(*) as num from gaga.pwrd_ods_ipchina;"
            ]
        }
    }
]

clusterID = "j-2MGE9DCUR1UPZ"

with DAG(
        dag_id=DAG_ID,
        description='Run built-in Spark app on Amazon EMR',
        default_args=DEFAULT_ARGS,
        dagrun_timeout=timedelta(minutes=60),
        start_date=days_ago(1),
        schedule_interval="20 8 * * *",
        # schedule_interval='@once',
        tags=['emr', 'mwaa2', 'sqoop', 'hive'],
) as dag:
    start_operator = DummyOperator(task_id='begin_execution', dag=dag)
    end_operator = DummyOperator(task_id='stop_execution', dag=dag)

    step_1_adder_spark = EmrAddStepsOperator(
        task_id='step_1_adder_spark',
        job_flow_id=clusterID,
        # aws_conn_id='aws-virginia',
        steps=SPARK_STEPS,
    )

    wait_step_1_adder_spark = EmrStepSensor(
        task_id='wait_step_1_adder_spark',
        job_flow_id=clusterID,
        step_id="{{ task_instance.xcom_pull(task_ids='step_1_adder_spark', key='return_value')[0] }}",
        aws_conn_id='aws-virginia',
    )

    step_2_adder_aqoop = EmrAddStepsOperator(
        task_id='step_2_adder_sqoop',
        job_flow_id=clusterID,
        # aws_conn_id='aws-virginia',
        steps=SQOOP_STEPS,
    )

    wait_step_2_adder_aqoop = EmrStepSensor(
        task_id='wait_step_2_adder_aqoop',
        job_flow_id=clusterID,
        step_id="{{ task_instance.xcom_pull(task_ids='step_2_adder_sqoop', key='return_value')[0] }}",
        # aws_conn_id='aws-virginia',
    )

start_operator >> step_1_adder_spark >> wait_step_1_adder_spark >> step_2_adder_aqoop >> wait_step_2_adder_aqoop >> end_operator