# -*- coding: utf-8 -*-
# @Description:
# @Time    : 2022/2/17 2:50 下午}
# @Author  : jiasunm@ Neo Sun
# @Product : PyCharm
# @motto   : It is never too late to learn
# @File    : mwaa2_emr_ohio_pull_virginia_long_running.py.py

from airflow import DAG

# 最新版本需要，mwaa 2.0.2 目前不支持
# from airflow.providers.amazon.aws.operators.emr import EmrCreateJobFlowOperator
# from airflow.providers.amazon.aws.operators.emr import EmrAddStepsOperator
# from airflow.providers.amazon.aws.sensors.emr import EmrStepSensor
# from airflow.operators.dummy import DummyOperator

from airflow.providers.amazon.aws.operators.emr_create_job_flow import EmrCreateJobFlowOperator
from airflow.providers.amazon.aws.operators.emr_add_steps import EmrAddStepsOperator
from airflow.providers.amazon.aws.sensors.emr_step import EmrStepSensor
from airflow.operators.dummy_operator import DummyOperator

from airflow.utils.dates import days_ago
from datetime import timedelta
import os

DAG_ID = os.path.basename(__file__).replace('.py', '')

DEFAULT_ARGS = {
    'owner': 'airflow',
    'depends_on_past': False,
    'email': ['airflow@example.com'],
    'email_on_failure': False,
    'email_on_retry': False,
}

SPARK_STEPS = [
    {
        # "Name": "Spark-Program-neo-aws-etl",
        "Name": "spark-etl-demo",
        "ActionOnFailure": "CONTINUE",
        "HadoopJarStep": {
            "Jar": "command-runner.jar",
            "Args": [
                "spark-submit",
                "--deploy-mode",
                "cluster",
                "--master",
                "yarn",
                "--conf",
                "spark.yarn.submit.waitAppCompletion=true",
                "s3://neo-ohio-emr-script/spark_etl_simple.py"
            ]
        }
    }
]

SQOOP_STEPS = [
    {
        "Name": "Sqoop_version_check",
        "ActionOnFailure": "CONTINUE",
        "HadoopJarStep": {
            "Jar": "command-runner.jar",
            "Args": [
                "sqoop",
                "version"
            ]
        }
    }

]

HIVE_STEPS = [
    {
        "Name": "Hive ETL demo",
        "ActionOnFailure": "CONTINUE",
        "HadoopJarStep": {
            "Jar": "command-runner.jar",
            "Args": [
                "hive",
                "-e",
                "show databases;select count(*) as num from gaga.pwrd_ods_ipchina;"
            ]
        }
    }
]

clusterID = "j-2MGE9DCUR1UPZ"

with DAG(
        dag_id=DAG_ID,
        description='Run built-in Spark app on Amazon EMR',
        default_args=DEFAULT_ARGS,
        dagrun_timeout=timedelta(minutes=60),
        start_date=days_ago(1),
        schedule_interval="20 8 * * *",
        # schedule_interval='@once',
        tags=['emr', 'mwaa2', 'sqoop', 'hive'],
) as dag:
    start_operator = DummyOperator(task_id='begin_execution', dag=dag)
    end_operator = DummyOperator(task_id='stop_execution', dag=dag)

    step_1_adder_spark = EmrAddStepsOperator(
        task_id='step_1_adder_spark',
        job_flow_id=clusterID,
        # aws_conn_id='aws-virginia',
        steps=SPARK_STEPS,
    )

    wait_step_1_adder_spark = EmrStepSensor(
        task_id='wait_step_1_adder_spark',
        job_flow_id=clusterID,
        step_id="{{ task_instance.xcom_pull(task_ids='step_1_adder_spark', key='return_value')[0] }}",
        aws_conn_id='aws-virginia',
    )

    step_2_adder_aqoop = EmrAddStepsOperator(
        task_id='step_2_adder_sqoop',
        job_flow_id=clusterID,
        # aws_conn_id='aws-virginia',
        steps=SQOOP_STEPS,
    )

    wait_step_2_adder_aqoop = EmrStepSensor(
        task_id='wait_step_2_adder_aqoop',
        job_flow_id=clusterID,
        step_id="{{ task_instance.xcom_pull(task_ids='step_2_adder_sqoop', key='return_value')[0] }}",
        # aws_conn_id='aws-virginia',
    )

start_operator >> step_1_adder_spark >> wait_step_1_adder_spark >> step_2_adder_aqoop >> wait_step_2_adder_aqoop >> end_operator