import boto3
import json
import requests
import base64

def airflow_cli_boto3(cli_command="dags list"):
    try:
        # 创建MWAA客户端
        mwaa_client = boto3.client('mwaa')
        
        # 获取第一个环境名
        environments = mwaa_client.list_environments()
        environment_name = environments['Environments'][1]

        # 创建CLI令牌
        cli_token_response = mwaa_client.create_cli_token(Name=environment_name)
        cli_token = cli_token_response['CliToken']
        web_server_hostname = cli_token_response['WebServerHostname']

        # 设置请求头
        headers = {'Authorization': f'Bearer {cli_token}', 'Content-Type': 'text/plain'}
        
        # 发送CLI命令
        response = requests.post(f"https://{web_server_hostname}/aws_mwaa/cli", headers=headers, data=cli_command)
        
        if response.status_code == 200:
            # 解析响应
            result = json.loads(response.text)
            
            # 解码Base64编码的输出
            decoded_output = base64.b64decode(result['stdout']).decode('utf-8')
            return decoded_output
        else:
            return f"Error: {response.status_code}, {response.text}"
    except Exception as e:
        return f"An error occurred: {e}"

# 使用函数
# result = airflow_cli_boto3()
# print(result)

cli_command="dags list"
# https://docs.aws.amazon.com/zh_tw/mwaa/latest/userguide/airflow-cli-command-reference.html
# 使用函数，设置CLI命令为Airflow backfill
# cli_command = "dags backfill -s 2023-07-01T00:00 -e 2023-07-01T05:00 DAG_test_hour_v2"
#cli_command = "dags backfill -s 2023-07-01T00:00+08:00 -e 2023-07-01T05:00+08:00 DAG_test_hour_v2"
result = airflow_cli_boto3(cli_command)
print(result)
