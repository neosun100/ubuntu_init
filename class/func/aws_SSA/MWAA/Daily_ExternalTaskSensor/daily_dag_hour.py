from datetime import timedelta
from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.utils.dates import days_ago

# DAG_hour
dag_hour = DAG(
    'DAG_hour',
    start_date=days_ago(1),
    schedule_interval=timedelta(hours=1),
    tags=['ExternalTaskSensor'],
)

start_task_hour = DummyOperator(
    task_id='start_task_hour',
    dag=dag_hour,
)

end_task_hour = DummyOperator(
    task_id='end_task_hour',
    dag=dag_hour,
)

start_task_hour >> end_task_hour