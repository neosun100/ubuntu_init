from datetime import datetime, timedelta
from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.bash_operator import BashOperator
from airflow.utils.dates import days_ago
from airflow.sensors.external_task_sensor import ExternalTaskSensor

with DAG(
    dag_id="example_daily_dag",
    schedule_interval=timedelta(days=1),
    start_date=days_ago(2),
    catchup=False,
    tags=["external_task"],
) as dag:

    depends_on_hourly_23hr = ExternalTaskSensor(
        task_id="depends_on_hourly_23hr",
        external_dag_id="example_hourly_dag",
        external_task_id="hourly_job",
        execution_delta=timedelta(days=1),
        poke_interval=50*60,  # 50min
        timeout=60*60,  # 1hr
        mode='reschedule',
        allowed_states=['success'],
        failed_states=['failed', 'skipped'],
    )

    daily_job = BashOperator(
        task_id="daily_job",
        bash_command="echo 'run me daily' && date"
    )

    depends_on_hourly_23hr >> daily_job