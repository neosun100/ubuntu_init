from datetime import datetime, timedelta
from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.operators.bash_operator import BashOperator
from airflow.utils.dates import days_ago

default_args = {"retries": 2}

with DAG(
    dag_id="example_hourly_dag",
    schedule_interval=timedelta(hours=1),
    start_date=days_ago(2),
    catchup=False,
    default_args=default_args,
    tags=["external_task"],
) as dag:

    start = DummyOperator(
        task_id="start",
    )

    delay_2h = BashOperator(
        task_id="delay_2h",
        bash_command="sleep 3"  # sleep for 7200 seconds which is equivalent to 2 hours
    )

    hourly_job = BashOperator(
        task_id="hourly_job",
        bash_command="echo 'run me hourly' && date"
    )

    start >> delay_2h >> hourly_job