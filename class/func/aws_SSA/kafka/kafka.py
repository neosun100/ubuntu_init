def check_df_2_table(batch_df, batch_id):
    """
    同结构df写入hudi
    """
    tableList = batch_df.select('topic').distinct().rdd.flatMap(lambda x: x).collect()
    print(tableList)
    for table in tableList:
        tableName = table.split('.')[-1]
        print(tableName)
        one_batch_df = batch_df.filter(batch_df.topic == table).withColumn("json", col("value").cast(StringType()) \
                                                                           ).select("json")
        json_schema = spark.read.json(one_batch_df.select("json").rdd.map(lambda row: row.json)).schema
        # print(json_schema)
        df = one_batch_df.withColumn("jsonData", from_json("json", json_schema))
        # df.select("jsonData.*").show(1000)
        print(df.count())



### 4.1.6 构造eachBatch 函数

# 采用推断方式，解析schema，未来可以添加多表逻辑，实现整库多表——单流入湖——sink多hudi表


def df_2_hudi_sigle_table(batch_df, batch_id):
    """
    同结构df写入hudi
    """
    json_schema = spark.read.json(batch_df.select("json").rdd.map(lambda row: row.json)).schema
    #print(json_schema)
    df = batch_df.withColumn("jsonData", from_json("json", json_schema))
    #batch_df.show(3)
    df.select("jsonData.*").write.format("hudi"). \
    options(**hudi_options). \
    mode("append"). \
    save(basePath)



# 多个scehma根据topic拆分多张表
# 同步写法
def df_2_hudi_multiple_table(batch_df, batch_id):
    """
    同结构df写入hudi
    """
    tableList = batch_df.select('topic').distinct().rdd.flatMap(lambda x: x).collect()
    print(tableList)
    for table in tableList:
        tableName = table.split('.')[-1]
        #print(tableName)
        # basePath = "file:///tmp/hudi_trips_cow" # file
        # basePath = "s3a://hudi/main/hudi_trips_cow_v1/" # lakeFS
        # basePath = "s3a://aws-tools-kit/hudi_trips_cow_v1/" # minIO
        basePath = f"s3a://app-util-hudi/spark/{tableName}"  # S3
        hudi_options = {
            'hoodie.table.name': tableName,
            'hoodie.datasource.write.table.name': tableName,
            'hoodie.datasource.write.table.type': 'COPY_ON_WRITE',
            'hoodie.datasource.write.operation': 'upsert',
            'hoodie.datasource.write.recordkey.field': 'id',
            'hoodie.datasource.write.partitionpath.field': 'age',
            'hoodie.datasource.write.precombine.field': 'eventTS',
            'hoodie.upsert.shuffle.parallelism': 2,
            'hoodie.insert.shuffle.parallelism': 2,
            'hoodie.bulkinsert.shuffle.parallelism': 2,
            'hoodie.delete.shuffle.parallelism': 2,
            'hoodie.datasource.hive_sync.mode': 'hms',
            'hoodie.datasource.hive_sync.auto_create_database': True,
            'hoodie.datasource.hive_sync.database': 'lake-hudi',
            'hoodie.datasource.hive_sync.table': tableName,
            'hoodie.datasource.hive_sync.partition_fields': 'age',
            'hoodie.datasource.hive_sync.partition_extractor_class': "org.apache.hudi.hive.MultiPartKeysValueExtractor",
            'hoodie.datasource.write.payload.class': 'org.apache.hudi.common.model.DefaultHoodieRecordPayload',
            'hoodie.datasource.hive_sync.enable': "true",
        }
        one_batch_df = batch_df.filter(batch_df.topic == table).withColumn("json", col("value").cast(StringType()) \
                                                                           ).select("json")
        json_schema = spark.read.json(one_batch_df.select("json").rdd.map(lambda row: row.json)).schema
        #print(json_schema)
        df = one_batch_df.withColumn("jsonData", from_json("json", json_schema))
        #one_batch_df.show(3)
        df.select("jsonData.*").write.format("hudi"). \
            options(**hudi_options). \
            mode("append"). \
            save(basePath)  
        
kafkaDF.writeStream \
    .option("checkpointLocation", "/home/hadoop/checkpoint-all-multiple-2022-08-20-005/") \
    .trigger(processingTime='60 seconds') \
    .foreachBatch(df_2_hudi_multiple_table) \
    .start().awaitTermination()