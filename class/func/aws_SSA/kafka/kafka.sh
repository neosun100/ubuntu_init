kafka-topics --bootstrap-server <broker-list> --topic <topic-name> --describe


Topic: myTopic    PartitionCount: 3    ReplicationFactor: 2    Configs: segment.bytes=1073741824
    Topic: myTopic    Partition: 0    Leader: 1    Replicas: 1,2    Isr: 1,2
    Topic: myTopic    Partition: 1    Leader: 2    Replicas: 2,3    Isr: 2,3
    Topic: myTopic    Partition: 2    Leader: 3    Replicas: 3,1    Isr: 3,1



{
  "version": 1,
  "partitions": [
      {"topic": "my-topic", "partition": 0, "replicas": [1, 2, 3]},
      {"topic": "my-topic", "partition": 1, "replicas": [2, 3, 1]},
      {"topic": "my-topic", "partition": 2, "replicas": [3, 1, 2]}
  ]
}


kafka-reassign-partitions --bootstrap-server <broker-list> --reassignment-json-file increase-replication-factor.json --execute



kafka-reassign-partitions --bootstrap-server <broker-list> --reassignment-json-file increase-replication-factor.json --verify



kafka-topics --bootstrap-server <broker-list> --alter --topic <topic-name> --partitions <new-total-partitions>
