







mkdir -p ~/upload/aws_emr_config
cat >~/upload/aws_emr_config/reconfiguration.json <<EOF
{
   "ClusterId":"j-MyClusterID",
   "InstanceGroups":[
      {
         "InstanceGroupId":"ig-MyMasterId",
         "Configurations":[
            {
               "Classification":"capacity-scheduler",
               "Properties":{
                  "yarn.scheduler.capacity.root.queues":"default,alpha,beta",
                  "yarn.scheduler.capacity.root.default.capacity":"40",
                  "yarn.scheduler.capacity.root.default.maximum-capacity":"40",
                  "yarn.scheduler.capacity.root.default.accessible-node-labels.CORE.capacity":"40",
                  "yarn.scheduler.capacity.root.default.accessible-node-labels":"*",
                  "yarn.scheduler.capacity.root.default.acl_submit_applications":"test-user1",
                  "yarn.scheduler.capacity.root.alpha.capacity":"30",
                  "yarn.scheduler.capacity.root.alpha.maximum-capacity":"30",
                  "yarn.scheduler.capacity.root.alpha.accessible-node-labels.CORE.capacity":"30",
                  "yarn.scheduler.capacity.root.alpha.accessible-node-labels":"*",
                  "yarn.scheduler.capacity.root.alpha.acl_submit_applications":"test-user2",
                  "yarn.scheduler.capacity.root.beta.capacity":"30",
                  "yarn.scheduler.capacity.root.beta.maximum-capacity":"30",
                  "yarn.scheduler.capacity.root.beta.accessible-node-labels.CORE.capacity":"30"
                  "yarn.scheduler.capacity.root.beta.accessible-node-labels":"*",
                  "yarn.scheduler.capacity.root.beta.acl_submit_applications":"test-user3"
               },
               "Configurations":[]
            }
         ]
      }
   ]
}
EOF

aws emr modify-instance-groups --cli-input-json file://~/upload/aws_emr_config/reconfiguration.json




wget \
-P /Users/`whoami`/Downloads/  \
-O kafka-connect-source-datagen.zip \
https://d1i4a15mxbxib1.cloudfront.net/api/plugins/confluentinc/kafka-connect-datagen/versions/0.5.3/confluentinc-kafka-connect-datagen-0.5.3.zip  



mkdir -p ~/upload/aws_emr_config
cat >~/upload/aws_emr_config/reconfiguration.json <<EOF
{
   "ClusterId":"j-MyClusterID",
   "InstanceGroups":[
      {
         "InstanceGroupId":"ig-MyMasterId",
         "Configurations":[
            {
               "Classification":"capacity-scheduler",
               "Properties":{
                  "yarn.scheduler.capacity.root.queues":"default,alpha,beta",
                  "yarn.scheduler.capacity.root.default.capacity":"40",
                  "yarn.scheduler.capacity.root.default.maximum-capacity":"40",
                  "yarn.scheduler.capacity.root.default.accessible-node-labels.CORE.capacity":"40",
                  "yarn.scheduler.capacity.root.default.accessible-node-labels":"*",
                  "yarn.scheduler.capacity.root.alpha.capacity":"30",
                  "yarn.scheduler.capacity.root.alpha.maximum-capacity":"30",
                  "yarn.scheduler.capacity.root.alpha.accessible-node-labels.CORE.capacity":"30",
                  "yarn.scheduler.capacity.root.alpha.accessible-node-labels":"*",
                  "yarn.scheduler.capacity.root.beta.capacity":"30",
                  "yarn.scheduler.capacity.root.beta.maximum-capacity":"30",
                  "yarn.scheduler.capacity.root.beta.accessible-node-labels.CORE.capacity":"30"
                  "yarn.scheduler.capacity.root.beta.accessible-node-labels":"*",
                  "yarn.scheduler.capacity.queue-mappings":"u:test-user1:default,u:test-user2:alpha,u:test-user3:beta"
               },
               "Configurations":[]
            }
         ]
      }
   ]
}
EOF

aws emr modify-instance-groups --cli-input-json file://~/upload/aws_emr_config/reconfiguration.json