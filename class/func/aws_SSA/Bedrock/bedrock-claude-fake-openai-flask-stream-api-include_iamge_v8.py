from flask import Flask, request, Response
from anthropic import AnthropicBedrock
import json
from functools import wraps
from loguru import logger
import secrets
import string
import os
import time
import re

# 配置loguru日志
logger.add(f"{__file__.split('/')[-1].split('.')[0]}.log", format="{time} {level} {message}", level="DEBUG")

app = Flask(__name__)

# 定义一个工具类,提供日志记录、异常处理和重试机制装饰器功能
class Utils:
    """
    一个工具类,提供日志记录、异常处理和重试机制装饰器功能。
    """
    logger = logger

    @staticmethod
    def retry(max_retries=3, retry_delay=1):
        """
        重试装饰器,用于在出现异常时重试指定次数。
        :param max_retries: 最大重试次数
        :param retry_delay: 重试延迟时间(秒)
        :return: 装饰器包装后的函数
        """
        def decorator(func):
            @wraps(func)
            def wrapper(*args, **kwargs):
                retries = 0
                while retries <= max_retries:
                    try:
                        return func(*args, **kwargs)
                    except Exception as e:
                        retries += 1
                        if retries <= max_retries:
                            Utils.logger.warning(f"Retrying {func.__name__} due to {e} (Retry {retries}/{max_retries})")
                            time.sleep(retry_delay)
                        else:
                            Utils.logger.error(f"Max retries ({max_retries}) exceeded for {func.__name__}: {e}")
                            raise e
            return wrapper
        return decorator

    @staticmethod
    @retry(max_retries=3, retry_delay=0.2)
    def exception_handler(func):
        """
        异常处理装饰器,用于捕获和记录函数中的异常。
        :param func: 被装饰的函数
        :return: 装饰器包装后的函数
        """
        @wraps(func)
        def wrapper(*args, **kwargs):
            try:
                return func(*args, **kwargs)
            except Exception as e:
                Utils.logger.error(f"An error occurred in {func.__name__}: {e}")
                raise e
        return wrapper

# 生成一个随机的28位字符串作为密钥,以sk-开头
home_dir = os.path.expanduser("~")
api_key_file = os.path.join(home_dir, "bedrock_claude_api_key")

if os.path.exists(api_key_file):
    with open(api_key_file, "r") as f:
        api_key = f.read().strip()
else:
    api_key = "sk-" + "".join(secrets.choice(string.ascii_letters + string.digits) for _ in range(26))
    with open(api_key_file, "w") as f:
        f.write(api_key)

Utils.logger.info(f"Generated API key: {api_key}")

class ConversationContext:
    def __init__(self, client, model_name):
        self.client = client
        self.model_name = model_name
        self.conversation_history = []

    @Utils.exception_handler
    def send_message(self, user_message, max_tokens=16384, stream=False):
        """
        发送消息并获取响应
        :param user_message: 用户发送的消息列表
        :param max_tokens: 最大令牌数
        :param stream: 是否使用流式响应
        :return: 如果是流式响应,返回Response对象;否则返回响应数据字典
        """
        self.conversation_history.extend(user_message)

        if stream:
            response_data = self.stream_response(user_message, max_tokens)
            return Response(response_data, mimetype="text/event-stream")
        else:
            with self.client.messages.stream(
                max_tokens=max_tokens,
                messages=self.conversation_history,
                model=self.model_name,
            ) as stream:
                assistant_response = ""
                for text in stream.text_stream:
                    assistant_response += text

            response_data = {
                "content": [{"text": assistant_response, "type": "text"}],
                "model": self.model_name,
                "role": "assistant",
                "stop_reason": "end_turn",
                "type": "message",
                "usage": {
                    "input_tokens": len(user_message),
                    "output_tokens": len(assistant_response),
                },
            }

            self.conversation_history.append({"role": "assistant", "content": assistant_response})
            return response_data

    @Utils.exception_handler
    def stream_response(self, user_message, max_tokens):
        """
        使用流式响应
        :param user_message: 用户发送的消息列表
        :param max_tokens: 最大令牌数
        :return: 生成器,用于生成流式响应数据
        """
        with self.client.messages.stream(
            max_tokens=max_tokens,
            messages=self.conversation_history,
            model=self.model_name,
        ) as stream:
            yield "event: message_start\n"
            yield "data: {}\n\n"  # 初始消息为空

            for text in stream.text_stream:
                yield "event: content_block_delta\n"
                yield f"data: {json.dumps({'type': 'content_block_delta', 'delta': {'type': 'text_delta', 'text': text}})}\n\n"

                yield "event: ping\n"
                yield "data: {\"type\": \"ping\"}\n\n"

            yield "event: message_stop\n"
            yield "data: {\"type\": \"message_stop\"}\n\n"

    @Utils.exception_handler
    def send_message_with_image(self, user_message, image_data, max_tokens=16384, stream=False):
        """
        发送包含图像的消息并获取响应
        :param user_message: 用户发送的文本消息
        :param image_data: 图像数据,包括媒体类型和base64编码的图像数据
        :param max_tokens: 最大令牌数
        :param stream: 是否使用流式响应
        :return: 如果是流式响应,返回Response对象;否则返回响应数据字典
        """
        message = [
            {
                "role": "user",
                "content": [
                    {
                        "type": "image",
                        "source": {
                            "type": "base64",
                            "media_type": image_data["media_type"],
                            "data": image_data["data"],
                        },
                    },
                    {"type": "text", "text": user_message},
                ],
            }
        ]
        return self.send_message(message, max_tokens, stream)

client = AnthropicBedrock()

@Utils.exception_handler
@app.route('/claude/v1/messages', methods=['POST'])
def chat():
    """
    处理聊天请求的路由函数
    :return: 响应数据或错误信息
    """
    request_headers = dict(request.headers)
    Utils.logger.info(f"Request headers: {request_headers}")

    # 检查API密钥是否正确
    if request_headers.get('X-Api-Key') != api_key:
        error_response = {'error': 'Invalid API key'}
        Utils.logger.error(f"Responding with error: {error_response}")
        return error_response, 401

    request_data = request.get_json()
    Utils.logger.info(f"Received request: {request_data}")

    system_message = request_data.get('system')
    Utils.logger.info(f"system_message: {system_message}")
    if system_message:
        user_message = [
            {'role': 'user', 'content': [{'type': 'text', 'text': system_message}]},
            {'role': 'assistant', 'content': [{'type': 'text', 'text': 'I understand, I will strictly do as you said.'}]}
        ]
    else:
        user_message = request_data.get('messages', [])

    max_tokens = request_data.get('max_tokens', 16384)
    stream = request_data.get('stream', False)
    model_name = request_data.get('model')
    messages = request_data.get('messages')
    image_data = request_data.get('image')
    user_message.extend(messages)
    Utils.logger.info(f"user_message: {user_message}")
    if model_name:
        model_mapping = {
            'claude-3-opus-20240229': 'anthropic.claude-3-opus-20240229-v1:0',
            'claude-3-sonnet-20240229': 'anthropic.claude-3-sonnet-20240229-v1:0',
            'claude-3-haiku-20240307': 'anthropic.claude-3-haiku-20240307-v1:0'
        }
        conversation = ConversationContext(client, model_mapping.get(model_name, 'anthropic.claude-3-sonnet-20240229-v1:0'))
    else:
        conversation = ConversationContext(client, 'anthropic.claude-3-sonnet-20240229-v1:0')

    if user_message:
        if image_data:
            # 如果请求包含图像数据,则调用send_message_with_image方法
            response_data = conversation.send_message_with_image(user_message[-1]["content"][1]["text"], image_data, max_tokens, stream)
        else:
            # 否则,调用原有的send_message方法
            response_data = conversation.send_message(user_message, max_tokens, stream)

        if stream:
            return response_data
        else:
            Utils.logger.info(f"Responding with: {response_data}")
            return response_data
    else:
        error_response = {'error': 'No message provided'}
        Utils.logger.error(f"Responding with error: {error_response}")
        return error_response, 400


@Utils.exception_handler
@app.route('/chatgpt/v1/chat/completions', methods=['POST'])
def chat_openai():
    """
    处理聊天请求的路由函数
    :return: 响应数据或错误信息
    """
    request_headers = dict(request.headers)
    Utils.logger.info(f"Request headers: {request_headers}")

    # 检查API密钥是否正确
    api_key_from_request = request_headers.get('Authorization')
    if api_key_from_request != f'Bearer {api_key}':
        error_response = {'error': 'Invalid API key'}
        Utils.logger.error(f"Responding with error: {error_response}")
        return error_response, 401

    request_data = request.get_json()
    Utils.logger.info(f"Received request: {request_data}")

    messages = request_data.get('messages', [])
    system_message = None
    for message in messages:
        if message.get('role') == 'system':
            content = message.get('content')
            pattern = r'^Current model: .*?\n\n'
            system_message = re.sub(pattern, '', content, flags=re.DOTALL)
            break

    if system_message:
        Utils.logger.info(f"system_message: {system_message}")
        user_message = [
            {'role': 'user', 'content': [{'type': 'text', 'text': system_message}]},
            {'role': 'assistant', 'content': [{'type': 'text', 'text': 'I understand, I will strictly do as you said.'}]}
        ]
    else:
        Utils.logger.info("No system message found in the request")
        user_message = []

    Utils.logger.info(f"user_message: {user_message}")

    max_tokens = request_data.get('max_tokens', 16384)
    stream = request_data.get('stream', False)
    model_name = request_data.get('model')

    # 确保所有消息的 content 字段都是一个列表
    for message in messages:
        if message.get('role') != 'system':
            content = message.get('content')
            if isinstance(content, str):
                message['content'] = [{'type': 'text', 'text': content}]
            user_message.append(message)

    Utils.logger.info(f"user_message: {user_message}")

    max_tokens = request_data.get('max_tokens', 16384)
    stream = request_data.get('stream', False)
    model_name = request_data.get('model')
    image_data = request_data.get('image')


    if model_name:
        model_mapping = {
            'gpt-4': 'anthropic.claude-3-opus-20240229-v1:0',
            'gpt-4-turbo-preview': 'anthropic.claude-3-sonnet-20240229-v1:0',
            'gpt-4-0125-preview': 'anthropic.claude-3-sonnet-20240229-v1:0',
            'gpt-3.5-turbo-0125': 'anthropic.claude-3-haiku-20240307-v1:0',
            'gpt-3.5-turbo ': 'anthropic.claude-3-haiku-20240307-v1:0',
            'gpt-4-vision-preview': 'anthropic.claude-3-haiku-20240307-v1:0'
        }
        conversation = ConversationContext(client, model_mapping.get(model_name, 'anthropic.claude-3-sonnet-20240229-v1:0'))
    else:
        conversation = ConversationContext(client, 'anthropic.claude-3-sonnet-20240229-v1:0')

    if user_message:
        if image_data:
            # 如果请求包含图像数据,则调用send_message_with_image方法
            response_data = conversation.send_message_with_image(user_message[-1]["content"][1]["text"], image_data, max_tokens, stream)
        else:
            # 否则,调用原有的send_message方法
            response_data = conversation.send_message(user_message, max_tokens, stream)

        if stream:
            return response_data
        else:
            Utils.logger.info(f"Responding with: {response_data}")
            return response_data
    else:
        error_response = {'error': 'No message provided'}
        Utils.logger.error(f"Responding with error: {error_response}")
        return error_response, 400


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5001)