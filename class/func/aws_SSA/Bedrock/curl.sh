curl https://gpt.neo.xin/v1/chat/completions \
  -H "Content-Type: application/json" \
  -H "Authorization: Bearer $OPENAI_API_KEY" \
  -d '{
    "model": "gpt-3.5-turbo",
    "temperature": 0.6, 
    "top_p": 1,
    "stream": true,
    "messages": [
      {
        "role": "system",
        "content": "You are a helpful assistant."
      },
      {
        "role": "user",
        "content": "1 + 1 = ?"
      }
    ]
  }'

curl https://gpt.neo.xin/v1/chat/completions \
  -H "Content-Type: application/json" \
  -H "Authorization: Bearer $OPENAI_API_KEY" \
  -d '{
    "model": "gpt-3.5-turbo",
    "temperature": 0.6, 
    "top_p": 1,
    "stream": true,
    "messages": [
      {
        "role": "system",
        "content": "You are a helpful assistant."
      },
      {
        "role": "user",
        "content": "1 + 1 = ?"
      }
    ]
  }'


curl https://aws.neo.xin/chatgpt/v1/chat/completions \
  -H "Content-Type: application/json" \
  -H "Authorization: Bearer sk-y7wOGZxhZTgstCwsov2EzUXEzU" \
  -d '{
    "model": "gpt-3.5-turbo",
    "temperature": 0.6, 
    "top_p": 1,
    "stream": true,
    "messages": [
      {
        "role": "system",
        "content": "You are a helpful assistant."
      },
      {
        "role": "user",
        "content": "1 + 1 = ?"
      }
    ]
  }'

curl http://127.0.0.1:5001/chatgpt/v1/chat/completions \
  -H "Content-Type: application/json" \
  -H "Authorization: Bearer sk-y7wOGZxhZTgstCwsov2EzUXEzU" \
  -d '{
    "model": "gpt-3.5-turbo",
    "temperature": 0.6, 
    "top_p": 1,
    "stream": true,
    "messages": [
      {
        "role": "system",
        "content": "我们沟通中，如果没有特殊说明，一律用中文，且如果要求对比或问询异同，你会给到清晰明了的对比表格，方便理解"
      },
      {
        "role": "user",
        "content": "hello"
      }
    ]
  }'


curl http://127.0.0.1:5001/claude/v1/messages \
  -H "Content-Type: application/json" \
  -H "X-Api-Key: sk-y7wOGZxhZTgstCwsov2EzUXEzU" \
  -d '{
    "model": "gpt-3.5-turbo",
    "temperature": 0.6, 
    "top_p": 1,
    "stream": true,
    "messages": [
      {
        "role": "system",
        "content": "You are a helpful assistant."
      },
      {
        "role": "user",
        "content": "1 + 1 = ?"
      }
    ]
  }'
