**未来当你提供的内容包括：**
1. **表格**（格式为模型ID、请求速率、Token速率或音频秒数等信息）
2. **配置文件的文件名**（如 `groq-config-aws` 或 `groq-config-foxmail`）

**我将根据这些输入，自动生成相应的 YAML 配置文件。**

### 输入格式：
1. 提供表格数据，格式如下：
```
| ID | Requests per Minute | Requests per Day | Tokens per Minute | Tokens per Day |
| --- | ------------------- | ---------------- | ----------------- | -------------- |
| 模型ID | 请求每分钟 | 请求每天 | Token每分钟 | Token每天 |
```
2. 提供配置文件名，例如：`groq-config-aws` 或 `groq-config-foxmail`

### 输出格式：
- 对于普通模型，输出格式为：
```yaml
models:
  - model_name: groq/模型ID
    litellm_params:
      model: groq/模型ID
      <<: *配置文件名
```

- 对于语音模型（如 `distil-whisper-large-v3-en` 和 `whisper-large-v3`），输出格式为：
```yaml
models:
  - model_name: groq/模型ID
    litellm_params:
      model: groq/模型ID
      <<: *配置文件名
      model_info:
        mode: audio_transcription
```

### 示例：

#### 输入：
原始表格内容：
```
| ID | Requests per Minute | Requests per Day | Tokens per Minute | Tokens per Day |
| --- | ------------------- | ---------------- | ----------------- | -------------- |
| gemma-7b-it | 30 | 14,400 | 15,000 | 500,000 |
| gemma2-9b-it | 30 | 14,400 | 15,000 | 500,000 |
| distil-whisper-large-v3-en | 20 | 2,000 | 7,200 | 28,800 |
| whisper-large-v3 | 20 | 2,000 | 7,200 | 28,800 |
```

配置文件名：`groq-config-aws`

#### 输出：
```yaml
models:
  - model_name: groq/gemma-7b-it
    litellm_params:
      model: groq/gemma-7b-it
      <<: *groq-config-aws

  - model_name: groq/gemma2-9b-it
    litellm_params:
      model: groq/gemma2-9b-it
      <<: *groq-config-aws

  - model_name: groq/distil-whisper-large-v3-en
    litellm_params:
      model: groq/distil-whisper-large-v3-en
      <<: *groq-config-aws
      model_info:
        mode: audio_transcription

  - model_name: groq/whisper-large-v3
    litellm_params:
      model: groq/whisper-large-v3
      <<: *groq-config-aws
      model_info:
        mode: audio_transcription
```

**简化工作流程**：
未来，你只需提供表格和配置文件名，我会自动根据这些信息生成相应的 YAML 配置文件。普通模型和语音模型的处理逻辑已内置。