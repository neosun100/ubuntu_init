get_bedrock_models() {
    # 支持Bedrock的区域列表
    bedrock_regions=(
      "us-east-1" "us-west-2" "ap-south-1"
      "ap-southeast-1" "ap-southeast-2" "ap-northeast-1"
      "ca-central-1" "eu-central-1"
      "eu-west-2" "eu-west-3" "sa-east-1"
    )

    # 设置 INCLUDE_EMBED 参数，默认为 0（不包含 embed 模型）
    INCLUDE_EMBED=${1:-0}

    echo "Regions supporting Bedrock:"
    printf '%s ' "${bedrock_regions[@]}"
    echo -e "\n"

    echo "Regions with available Bedrock models (filtered):"
    echo "================================================="

    for region in "${bedrock_regions[@]}"; do
      echo "Checking region: $region"

      models=$(aws bedrock list-foundation-models --region "$region" --query 'modelSummaries[].modelId' --output json 2>&1)
      exit_code=$?

      if [ $exit_code -eq 0 ] && [ "$models" != "[]" ]; then
        echo "Available models in $region:"
        echo "$models" | jq -r '.[]' | while read -r model; do
          # 保留不带冒号或只有一个冒号的模型名称
          if [[ $(echo "$model" | tr -cd ':' | wc -c) -le 1 ]]; then
            if [ $INCLUDE_EMBED -eq 0 ] && [[ $model == *"embed"* ]]; then
              continue
            elif [ $INCLUDE_EMBED -eq 2 ] && [[ $model != *"embed"* ]]; then
              continue
            fi
            echo "  - $model"
          fi
        done | sort
      else
        if [[ $models == *"AccessDeniedException"* ]]; then
          echo "Access denied in $region. You may not have the necessary permissions for Bedrock in this region."
        elif [[ $models == *"not authorized to perform: bedrock:ListFoundationModels"* ]]; then
          echo "Not authorized to perform bedrock:ListFoundationModels in $region. You may need to request access to Bedrock for this region."
        elif [[ $models == *"Could not connect to the endpoint URL"* ]]; then
          echo "Could not connect to Bedrock in $region. The service might not be available in this region."
        else
          echo "No models found in $region or an error occurred."
        fi
      fi
      echo "---------------------------------------"
    done

    echo -e "\nNote: Use 'get_bedrock_models 1' to include all models, or 'get_bedrock_models 2' to show only embed models."
}



curl -s http://127.0.0.1:9000/v1/models -H "Authorization: Bearer $LITELLM_KEY" | jq -r '.data[].id' | sort
curl -s http://127.0.0.1:9000/v1/models -H "Authorization: Bearer $LITELLM_KEY" | jq -r '.data[].id | select(test("^[a-zA-Z].*/") and (test("^(standard|hd)") | not) and (test("\\*") | not))' | sort
curl -s https://gemini.neo.xin/v1/models -H "Authorization: Bearer $LITELLM_KEY" | jq -r '.data[].id' | sort

pip install --upgrade pyOpenSSL litellm[proxy]



STT_PROMPT='这是一个单人独白或多人对话，请标记不同的说话者,输出时就带上不同的说话者，并纠正错误和添加标点符号'
curl --location 'https://gemini.neo.xin/v1/audio/transcriptions' \
--header "Authorization: Bearer $LITELLM_KEY" \
--form 'file=@"/Users/jiasunm/Downloads/微信录音 HK web3 BD梁宇 aws eric_20240906173503.mp3"' \
--form 'model=groq-whisper-large-v3' \
--form 'response_format=json' \
--form 'timestamp_granularities=["segment"]' \
--form 'diarization=true' \
--form 'max_speakers=2' \
--form "prompt=$STT_PROMPT" \
--form 'language=zh'



sudo vim /etc/systemd/system/litellm-scf-proxy.service && \
sudo systemctl enable litellm-scf-proxy.service && sudo systemctl restart litellm-scf-proxy.service && sudo systemctl status litellm-scf-proxy.service && \
sudo journalctl -fu litellm-scf-proxy.service



STT_PROMPT='这是一个单人独白或多人对话，请标记不同的说话者,输出时就带上不同的说话者，并纠正错误和添加标点符号'
curl --location 'https://gemini.neo.xin/v1/audio/transcriptions' \
--header "Authorization: Bearer $LITELLM_KEY" \
--form 'file=@"/Users/jiasunm/Downloads/呼和浩特＂最多肉＂牛肉麵，27元一碗有半斤肉，真的好吃嗎？.m4a"' \
--form 'model=groq/whisper-large-v3' \
--form 'response_format=json' \
--form 'timestamp_granularities=["segment"]' \
--form 'diarization=true' \
--form 'max_speakers=2' \
--form "prompt=$STT_PROMPT" \
--form 'language=zh'