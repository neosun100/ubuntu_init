# claude
[{
    'role': 'user',
    'content': [{
            'type': 'text',
                'text': '我们沟通中，如果没有特殊说明，一律用中文，且如果要求对比或问询异同，你会给到清晰明了的对比表格，方便理解\n'
                }]
},
    {
    'role': 'assistant',
    'content': [{
            'type': 'text',
                'text': 'I understand, I will strictly do as you said.'
                }]
},
    {
    'role': 'user',
    'content': [
        {
            'type': 'text',
            'text': 'Image 1:'
        },
        {
            'type': 'image',
            'source': {
                    'type': 'base64',
                'media_type': 'image/jpeg',
                'data': '/9j/4AAQSkZJRgABAQEASABIAAD'
            }
        },
        {
            'type': 'text',
            'text': 'Image 2:'
        },
        {
            'type': 'image',
            'source': {
                'type': 'base64',
                'media_type': 'image/jpeg',
                'data': '/9j/4AAQSkZJRgABAQEASABIAAD'
            }
        },
        {
            'type': 'text',
            'text': '对比下图标'
        }]
}]

# GPT
[{
    'role': 'user',
    'content': [{
            'type': 'text',
                'text': '[object Object]'
                }]
}, {
    'role': 'assistant',
    'content': [{
            'type': 'text',
                'text': 'I understand, I will strictly do as you said.'
                }]
}, {
    'role': 'user',
    'content': [{
            'type': 'text',
                'text': '对比下图标'
                }, {
            'type': 'image_url',
            'image_url': {
                    'url': 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEASABIAAD'
            }
    }, {
        'type': 'image_url',
        'image_url': {
                'url': 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEASABIAAD'
        }
    }]
}]
