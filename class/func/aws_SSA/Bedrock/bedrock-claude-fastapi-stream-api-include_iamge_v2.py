from fastapi import FastAPI, Request, Response, HTTPException, status, Depends
from fastapi.responses import StreamingResponse
from anthropic import AnthropicBedrock
import json
from loguru import logger
import secrets
import string
import os
import time
from functools import wraps

# 配置loguru日志
logger.add(f"{__file__.split('/')[-1].split('.')[0]}.log", format="{time} {level} {message}", level="DEBUG")

app = FastAPI()

# 定义一个工具类,提供日志记录、异常处理和重试机制装饰器功能
class Utils:
    """
    一个工具类,提供日志记录、异常处理和重试机制装饰器功能。
    """
    logger = logger

    @staticmethod
    def retry(max_retries=3, retry_delay=0.5):
        """
        重试装饰器,用于在出现异常时重试指定次数。
        :param max_retries: 最大重试次数
        :param retry_delay: 重试延迟时间(秒)
        :return: 装饰器包装后的函数
        """
        def decorator(func):
            @wraps(func)
            def wrapper(*args, **kwargs):
                retries = 0
                while retries <= max_retries:
                    try:
                        return func(*args, **kwargs)
                    except Exception as e:
                        retries += 1
                        if retries <= max_retries:
                            Utils.logger.warning(f"Retrying {func.__name__} due to {e} (Retry {retries}/{max_retries})")
                            time.sleep(retry_delay)
                        else:
                            Utils.logger.error(f"Max retries ({max_retries}) exceeded for {func.__name__}: {e}")
                            raise e
            return wrapper
        return decorator

    @staticmethod
    def exception_handler(func):
        """
        异常处理装饰器,用于捕获和记录函数中的异常。
        :param func: 被装饰的函数
        :return: 装饰器包装后的函数
        """
        @wraps(func)
        def wrapper(*args, **kwargs):
            try:
                return func(*args, **kwargs)
            except Exception as e:
                Utils.logger.error(f"An error occurred in {func.__name__}: {e}")
                raise e
        return wrapper

# 生成一个随机的28位字符串作为密钥,以sk-开头
home_dir = os.path.expanduser("~")
api_key_file = os.path.join(home_dir, "bedrock_claude_api_key")

if os.path.exists(api_key_file):
    with open(api_key_file, "r") as f:
        api_key = f.read().strip()
else:
    api_key = "sk-" + "".join(secrets.choice(string.ascii_letters + string.digits) for _ in range(26))
    with open(api_key_file, "w") as f:
        f.write(api_key)

Utils.logger.info(f"Generated API key: {api_key}")

client = AnthropicBedrock()

class ConversationContext:
    def __init__(self, client, model_name):
        self.client = client
        self.model_name = model_name
        self.conversation_history = []

    @Utils.exception_handler
    def send_message(self, user_message, max_tokens=16384, stream=False):
        """
        发送消息并获取响应
        :param user_message: 用户发送的消息列表
        :param max_tokens: 最大令牌数
        :param stream: 是否使用流式响应
        :return: 如果是流式响应,返回Response对象;否则返回响应数据字典
        """
        self.conversation_history.extend(user_message)

        if stream:
            response_data = self.stream_response(user_message, max_tokens)
            return StreamingResponse(response_data, media_type="text/event-stream")
        else:
            with self.client.messages.stream(
                max_tokens=max_tokens,
                messages=self.conversation_history,
                model=self.model_name,
            ) as stream:
                assistant_response = ""
                for text in stream.text_stream:
                    assistant_response += text

            response_data = {
                "content": [{"text": assistant_response, "type": "text"}],
                "model": self.model_name,
                "role": "assistant",
                "stop_reason": "end_turn",
                "type": "message",
                "usage": {
                    "input_tokens": len(user_message),
                    "output_tokens": len(assistant_response),
                },
            }

            self.conversation_history.append({"role": "assistant", "content": assistant_response})
            return response_data

    @Utils.exception_handler
    def stream_response(self, user_message, max_tokens):
        """
        使用流式响应
        :param user_message: 用户发送的消息列表
        :param max_tokens: 最大令牌数
        :return: 生成器,用于生成流式响应数据
        """
        with self.client.messages.stream(
            max_tokens=max_tokens,
            messages=self.conversation_history,
            model=self.model_name,
        ) as stream:
            yield "event: message_start\n"
            yield "data: {}\n\n"  # 初始消息为空

            for text in stream.text_stream:
                yield "event: content_block_delta\n"
                yield f"data: {json.dumps({'type': 'content_block_delta', 'delta': {'type': 'text_delta', 'text': text}})}\n\n"

                yield "event: ping\n"
                yield "data: {\"type\": \"ping\"}\n\n"

            yield "event: message_stop\n"
            yield "data: {\"type\": \"message_stop\"}\n\n"

async def verify_api_key(request: Request):
    request_headers = dict(request.headers)
    Utils.logger.info(f"Request headers: {request_headers}")
    if request_headers.get('x-api-key') != api_key:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Invalid API key")

@app.post("/claude/v1/messages")
async def chat(request: Request, _: Response = Depends(verify_api_key)):
    """
    处理聊天请求的路由函数
    :return: 响应数据或错误信息
    """
    request_data = await request.json()
    Utils.logger.info(f"Received request: {request_data}")

    # 从这里开始，您的逻辑保持不变，只需要确保适用于FastAPI的异步调用方式。
    # 注意，FastAPI自动处理JSON序列化，所以直接返回字典即可。

    # 这里省略了具体的业务逻辑，因为它与您的Flask版本相同。
    # 只需确保将所有的同步调用更改为异步调用（如果有必要）。

    # 示例响应，您需要根据实际情况处理并返回正确的响应
    return {"message": "This is a sample response"}

# FastAPI不需要显式运行app.run，它使用ASGI服务器，如Uvicorn
