import requests

# API 端点 URL
url = "http://localhost:5001/api/claude"

# 要发送的提示文本
prompt = "你知道香港反送中吗"

# 构造请求数据
data = {"prompt": prompt}

# 发送 POST 请求
response = requests.post(url, json=data)

# 检查响应状态码
if response.status_code == 200:
    # 获取生成的文本
    result = response.json()
    generated_text = "".join([block["text"] for block in result])
    print(f"Generated text: {generated_text}")
else:
    print(f"Request failed with status code: {response.status_code}")