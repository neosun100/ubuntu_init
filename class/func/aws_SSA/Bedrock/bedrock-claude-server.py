from flask import Flask, request, jsonify
from anthropic import AnthropicBedrock
import json

app = Flask(__name__)

# 初始化 AnthropicBedrock 客户端
client = AnthropicBedrock()

# 设置 API 密钥和模型名称
MODEL_NAME = "anthropic.claude-3-sonnet-20240229-v1:0"

# 自定义 JSON 编码器
class ContentBlockEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, list):
            return [self.default(item) for item in obj]
        elif hasattr(obj, '__dict__'):
            return obj.__dict__
        else:
            return str(obj)

@app.route('/api/claude', methods=['POST'])
def generate_text():
    # 获取请求数据
    data = request.get_json()
    prompt = data.get('prompt')

    # 调用 Claude 模型生成响应
    response = client.messages.create(
        model=MODEL_NAME,
        max_tokens=256,
        messages=[{"role": "user", "content": prompt}],
    )

    # 返回生成的文本
    return app.response_class(
        json.dumps(response.content, cls=ContentBlockEncoder),
        mimetype='application/json'
    )

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5001, debug=True)