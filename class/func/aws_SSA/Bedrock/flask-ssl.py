from flask import Flask
import ssl

app = Flask(__name__)

@app.route('/',methods=['GET'])
def index():
    return 'Hello, World!'

if __name__ == '__main__':
    context = ssl.SSLContext(ssl.PROTOCOL_TLSv1_2)
    context.load_cert_chain('cert.pem', 'key.pem')
    app.run(host='127.0.0.1', port=443, ssl_context=context, debug=True)