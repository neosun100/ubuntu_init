# Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
# SPDX-License-Identifier: Apache-2.0

import json
from botocore import config
import logging
import os
from botocore import config
from botocore.exceptions import ClientError
import uuid
from datetime import datetime, timedelta
import pandas as pd
import boto3
from boto3.dynamodb.conditions import Key, Attr

logger = logging.getLogger()
logger.setLevel(logging.INFO)

DEFAULT_TIME_FORMAT = "%Y-%m-%dT%H:%M:%SZ"
CLUSTER_NAME = os.environ.get('CLUSTER_NAME', 'poc-binance-v9')

AutoScaling = os.environ.get('AutoScaling', 1)
HighLoadThreshold = os.environ.get('HighLoadThreshold', 0.80)
LowLoadThreshold = os.environ.get('LowLoadThreshold', 0.45)
ThresholdDuration = os.environ.get('ThresholdDuration', 30)

# NumberOfCurrentClusterGroups = os.environ.get(
#     'NumberOfCurrentClusterGroups', 3)
# NumberOfCurrentClusterGroupsActive = os.environ.get(
#     'NumberOfCurrentClusterGroupsActive', 3)
# NumberOfManualTargetClusterGroups = os.environ.get(
#     'NumberOfManualTargetClusterGroups', 4)
# OPENSEARCH_ClusterNum = os.environ.get('OPENSEARCH_ClusterNum', 3)
# OPENSEARCH_ClusterPrefixName = os.environ.get(
#     'OPENSEARCH_ClusterPrefixName', 'poc-binance-v8')
# OPENSEARCH_Codec = os.environ.get('OPENSEARCH_Codec', 'best_compression')
OPENSEARCH_DedicatedMasterType = os.environ.get(
    'OPENSEARCH_DedicatedMasterType', 'm6g.xlarge.search')
OPENSEARCH_EBSVolumeSize = os.environ.get('OPENSEARCH_EBSVolumeSize', 500)
OPENSEARCH_EBSVolumeType = os.environ.get('OPENSEARCH_EBSVolumeType', 'gp3')
OPENSEARCH_InstanceNum = os.environ.get('OPENSEARCH_InstanceNum', 6)
OPENSEARCH_InstanceType = os.environ.get(
    'OPENSEARCH_InstanceType', '6g.12xlarge.search')
OPENSEARCH_Mappings = os.environ.get('OPENSEARCH_Mappings',
                                     '{"properties":{"@timestamp":{"type":"alias","path":"timestamp"},"action":{"type":"keyword"},"formatVersion":{"type":"keyword"},"httpRequest":{"properties":{"args":{"type":"text","fields":{"keyword":{"type":"keyword","ignore_above":256}}},"clientIp":{"type":"ip"},"country":{"type":"keyword"},"headers":{"properties":{"name":{"type":"keyword"},"value":{"type":"text","fields":{"keyword":{"type":"keyword","ignore_above":256}}}}},"httpMethod":{"type":"keyword"},"httpVersion":{"type":"keyword"},"requestId":{"type":"text","fields":{"keyword":{"type":"keyword","ignore_above":256}}},"uri":{"type":"text","fields":{"keyword":{"type":"keyword","ignore_above":256}}}}},"httpSourceId":{"type":"keyword"},"httpSourceName":{"type":"keyword"},"labels":{"properties":{"name":{"type":"text","fields":{"keyword":{"type":"keyword","ignore_above":256}}}}},"ruleGroupList":{"properties":{"ruleGroupId":{"type":"keyword"},"terminatingRule":{"properties":{"action":{"type":"keyword"},"ruleId":{"type":"keyword"}}}}},"terminatingRuleId":{"type":"keyword"},"terminatingRuleType":{"type":"keyword"},"timestamp":{"type":"date","format":"epoch_millis"},"webaclId":{"type":"text","fields":{"keyword":{"type":"keyword","ignore_above":256}}},"webaclName":{"type":"keyword"}}}')
OPENSEARCH_NumberOfReplicas = os.environ.get('OPENSEARCH_NumberOfReplicas', 1)
OPENSEARCH_NumberOfShards = os.environ.get('OPENSEARCH_NumberOfShards', 24)
OPENSEARCH_Password = os.environ.get('OPENSEARCH_Password', 'AWS2themoon100$')
OPENSEARCH_Policy = os.environ.get(
    'OPENSEARCH_Policy',
    '{"description":"loghub-v1-binance_ism_rollover_policy log set lifecycle.","default_state":"rollover","states":[{"name":"rollover","actions":[{"rollover":{"min_size":"720GB"},"retry":{"count":3,"delay":"1m"}}],"transitions":[{"state_name":"hot_2_warm","conditions":{"min_index_age":"1m"}}]},{"name":"hot_2_warm","actions":[{"warm_migration":{},"timeout":"24h","retry":{"count":3,"delay":"10m"}}],"transitions":[{"state_name":"delete","conditions":{"min_index_age":"14d"}}]},{"name":"delete","actions":[{"delete":{}}]}],"ism_template":{"index_patterns":["loghub-v1-binance*"],"priority":100}}')
OPENSEARCH_RefreshInterval = os.environ.get('OPENSEARCH_RefreshInterval', 60)
OPENSEARCH_SecurityGroupIds = os.environ.get(
    'OPENSEARCH_SecurityGroupIds', 'sg-0708b2efbc03b7d46').split(',')
OPENSEARCH_SubnetIds = os.environ.get(
    'OPENSEARCH_SubnetIds', 'subnet-0a8561b20a5c71ac9,subnet-0d2375de41fbda3e1').split(',')
OPENSEARCH_User = os.environ.get('OPENSEARCH_User', 'root')
OPENSEARCH_WarmNum = os.environ.get('OPENSEARCH_WarmNum', 4)
OPENSEARCH_WarmType = os.environ.get(
    'OPENSEARCH_WarmType', 'ultrawarm1.large.search')
OPENSEARCH_EBS_Iops = os.environ.get('OPENSEARCH_EBS_Iops', 16000)
OPENSEARCH_EBS_Throughput = os.environ.get('OPENSEARCH_EBS_Throughput', 1000)

OPENSEARCH_AccessPolicies = os.environ.get(
    'OPENSEARCH_AccessPolicies',
    """{"Version":"2012-10-17","Statement":[{"Effect":"Allow","Principal":{"AWS":"*"},"Action":"es:*","Resource":"arn:aws:es:us-east-1:835751346093:domain/*/*"},{"Effect":"Allow","Principal":{"AWS":"*"},"Action":"es:ESCrossClusterGet","Resource":"arn:aws:es:us-east-1:835751346093:domain/*"}]}""")


class ErrorCode:
    DUPLICATED_INDEX_PREFIX = "DuplicatedIndexPrefix"
    DUPLICATED_WITH_INACTIVE_INDEX_PREFIX = "DuplicatedWithInactiveIndexPrefix"
    OVERLAP_INDEX_PREFIX = "OverlapIndexPrefix"
    OVERLAP_WITH_INACTIVE_INDEX_PREFIX = "OverlapWithInactiveIndexPrefix"
    INVALID_INDEX_MAPPING = "InvalidIndexMapping"


class APIException(Exception):
    def __init__(self, message, code: str = None):
        if code:
            super().__init__("[{}] {}".format(code, message))
        else:
            super().__init__(message)


def handle_error(func):
    """Decorator for exception handling"""

    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except APIException as e:
            logger.exception(e)
            raise e
        except Exception as e:
            logger.exception(e)
            raise RuntimeError(
                "Unknown exception, please check Lambda log for more details"
            )

    return wrapper


def cluster_group_scaling_judgment(table_name='cluster_statistical_table',
                                   HighLoadThreshold=os.environ.get(
                                       'HighLoadThreshold', 75),
                                   LowLoadThreshold=os.environ.get(
                                       'LowLoadThreshold', 45),
                                   LoadThresholdDuration=os.environ.get('ThresholdDuration', 30)):
    """
    The logic for automatic scaling of cluster group scheduling.
    :param table_name: str, specifies the name of the DynamoDB table, defaults to 'cluster_statistical_table'.
    :param HighLoadThreshold: float, sets the threshold for high load, defaults to 75.
    :param LowLoadThreshold: float, sets the threshold for low load, defaults to 45.
    :param LoadThresholdDuration: int, sets the duration in minutes, defaults to 30.
    :return: int, 1 indicates cluster demand increased, -1 indicates cluster demand decreased, 0 indicates no adjustment is needed.
    """

    try:
        """1. Get cluster_statistical_table dataframe"""
        # Create DynamoDB resource
        dynamodb = boto3.resource('dynamodb')
        table = dynamodb.Table(table_name)

        # Calculate the start time of the query
        now = datetime.now()
        start_time = now - timedelta(minutes=LoadThresholdDuration + 5)
        start_time_str = start_time.strftime("%m/%d/%Y, %H:%M:%S")

        # Use conditional expressions to filter out eligible records
        response = table.scan(
            FilterExpression=Attr('event_ts').gt(start_time_str)
        )

        items = response['Items']

        if len(items) > 0:
            items_df = pd.DataFrame(items)
            items_df_filtered = items_df.loc[items_df['is_dashboard'] == 0]
            items_df_filtered
            logger.info(
                "Successful acquisition of dataframe data of statistical table")

            grouped_items_df = items_df_filtered.groupby(
                'cluster_name').agg({'workload': 'min'})
            highest_workload_cluster = grouped_items_df.sort_values(
                'workload', ascending=False).index[0]
            num_records = items_df_filtered[items_df_filtered['cluster_name']
                                            == highest_workload_cluster].shape[0]

            score = items_df_filtered.groupby(
                'cluster_name')['workload'].min().sort_values(
                ascending=False).iloc[0]

            # Define operations under different load conditions
            operations = {
                (score >= HighLoadThreshold) and (num_records > LoadThresholdDuration): lambda: 1,
                (score <= LowLoadThreshold) and (num_records > LoadThresholdDuration): lambda: -1,
                (LowLoadThreshold < score < HighLoadThreshold) or (num_records <= LoadThresholdDuration): lambda: 0
            }

            # Perform the corresponding operation according to the current load condition
            for condition, operation in operations.items():
                if condition:
                    return operation()

            return None

        else:
            logger.info(
                "The data sample of the cluster statistical table is not enough.")
            return None
    except Exception as e:
        logger.exception(
            "An error occurred while executing the function: {}".format(e))
        return None


judgment = cluster_group_scaling_judgment(table_name='cluster_statistical_table',
                                          HighLoadThreshold=os.environ.get(
                                              'HighLoadThreshold', 75),
                                          LowLoadThreshold=os.environ.get(
                                              'LowLoadThreshold', 45),
                                          LoadThresholdDuration=os.environ.get('ThresholdDuration', 30))


def update_status_items(table_name='cluster_information_table', status=3, update_status=4, cluster_name=None):
    try:
        # Create a connection to Dynamodb
        dynamodb = boto3.resource('dynamodb')
        # Access your desired table
        table = dynamodb.Table(table_name)

        # Scan the table and get all items with a status of 'status'
        # FilterExpression is used to specify the condition
        response = table.scan(
            FilterExpression=Key('cluster_status').eq(status)
        )

        # Get the list of items returned by the table scan
        items = response['Items']

        if cluster_name != None:
            for item in items:
                if item["cluster_name"] == cluster_name:
                    item["cluster_status"] = update_status
                    item["update_ts"] = datetime.now().strftime(
                        "%m/%d/%Y, %H:%M:%S")
                    response = table.put_item(Item=item)
                    logger.info(f"updated new item: {item}")
                    return item
                else:
                    pass

        else:

            # For each item that was returned, update the cluster_status to 'update_status'
            # and update the update_ts to the current time
            for item in items:
                item["cluster_status"] = update_status
                item["update_ts"] = datetime.now().strftime(
                    "%m/%d/%Y, %H:%M:%S")

            # Use batch_writer to update all items in the database at once
            with table.batch_writer() as batch:
                for item in items:
                    batch.put_item(Item=item)

            # Log an info message to indicate that the items have been successfully updated
            logger.info(
                f"Updated cluster status from {status} to {update_status}")

    except Exception as e:
        # If an error occurs, log an error message and the specific exception that was raised
        logger.error(
            f"An error occurred while updating cluster status: {str(e)}")


def get_count_by_status_condition(table_name='your_table_name', status_condition={'operator': 'gt', 'value': 4}):
    """
    Computes the number of items in a DynamoDB table that meet the specified condition.

    Args:
        table_name (str): The name of the DynamoDB table. Defaults to 'your_table_name'.
        status_condition (dict): A dictionary containing the filter condition. Defaults to {'operator': 'gt', 'value': 4}.
            The 'operator' can be one of the DynamoDB operators (e.g., lt, gt, eq), and 'value' is the value of the condition. Note that the data type of 'value' must match the data type of the 'status' field in the table.

    Returns:
        int: The number of items that meet the condition. Returns 0 if there is an error.
    """
    try:
        # Initialize the DynamoDB resource and table
        dynamodb = boto3.resource('dynamodb')
        table = dynamodb.Table(table_name)

        # Build the FilterExpression
        FilterExpression = getattr(boto3.dynamodb.conditions.Attr('status'),
                                   status_condition['operator'])(status_condition['value'])

        # Scan the table and count the items
        response = table.scan(FilterExpression=FilterExpression)
        count = response['Count']
        return count

    except Exception as e:
        # Handle exceptions
        print(f'Error in get_count_by_status_condition: {e}')
        return 0


def exist_status_lt_3(): return 1 if get_count_by_status_condition(
    table_name='cluster_information_table', status_condition={'operator': 'lt', 'value': 3}) > 0 else 0


def exist_status_eq_4(): return 1 if get_count_by_status_condition(
    table_name='cluster_information_table', status_condition={'operator': 'eq', 'value': 4}) > 0 else 0


def automatic_scaling(scaling=judgment,
                      exist_non_ingested_clusters=exist_status_eq_4(),  # exist status = 4
                      exist_initializing_clusters=exist_status_lt_3(),  # exist status < 3
                      ):
    """
    This function is a function that controls cluster scaling. It makes suggestions for scaling by querying cluster information tables and cluster statistics tables.



    """
    operations = {
        (scaling == 1) and (exist_non_ingested_clusters == 1): update_status_items(
            table_name='cluster_information_table', status=4, update_status=3),
        (scaling == 1) and (exist_non_ingested_clusters == 0) and (
            exist_initializing_clusters == 1): lambda: logger.info(
            "The cluster needs to be expanded and is being expanded."),
        (scaling == 1) and (exist_non_ingested_clusters == 0) and (
            exist_initializing_clusters == 0): lambda: create_opensearch_cluster(
            cluster_id=get_count_by_status_condition(
                status_condition={'operator': 'gt', 'value': -1}) + 1),
        (scaling == -1): lambda: update_bottom_cluster_status_4(),
        (scaling == 0): lambda: logger.info("The cluster group does not need to scale, wait for the next detection."),
        #         (scaling == 0) and (exist_non_ingested_clusters == 0): lambda: print("pass"),
        #         (scaling == 0) and (exist_non_ingested_clusters == 1) and (data_life_cycle_timeout == 1): lambda: print("Update DDB, update cluster status status = 5"),
        #         (scaling == 0) and (exist_non_ingested_clusters == 1) and (data_life_cycle_timeout == 0): lambda: print("pass")
    }
    for condition, operation in operations.items():
        if condition:
            return operation()
    return None


def create_opensearch_cluster(cluster_id):
    try:
        client = boto3.client('opensearch')
        # Get the latest version number
        latest_version = client.list_versions()['Versions'][0]
        AOS_Name = CLUSTER_NAME + '-' + str(cluster_id) + '-' + 'auto'

        response = client.create_domain(
            DomainName=AOS_Name,
            EngineVersion=latest_version,  # Use the latest version number
            ClusterConfig={
                'InstanceType': OPENSEARCH_InstanceType,
                'InstanceCount': OPENSEARCH_InstanceNum,
                'DedicatedMasterEnabled': True,
                'ZoneAwarenessEnabled': True,
                'ZoneAwarenessConfig': {'AvailabilityZoneCount': 2},
                'DedicatedMasterType': OPENSEARCH_DedicatedMasterType,
                'DedicatedMasterCount': 3,
                'WarmEnabled': True,
                'WarmType': OPENSEARCH_WarmType,
                'WarmCount': OPENSEARCH_WarmNum,
                'ColdStorageOptions': {'Enabled': True}
            },
            EBSOptions={
                'EBSEnabled': True,
                'VolumeType': OPENSEARCH_EBSVolumeType,
                'VolumeSize': OPENSEARCH_EBSVolumeSize,
                'Iops': OPENSEARCH_EBS_Iops,
                'Throughput': OPENSEARCH_EBS_Throughput
            },
            VPCOptions={
                'SubnetIds': OPENSEARCH_SubnetIds,
                'SecurityGroupIds': OPENSEARCH_SecurityGroupIds
            },
            CognitoOptions={
                'Enabled': False
            },
            NodeToNodeEncryptionOptions={
                'Enabled': True
            },
            EncryptionAtRestOptions={
                'Enabled': True
            },
            #             AccessPolicies='{"Version":"2012-10-17","Statement":[{"Effect":"Allow","Principal":{"AWS":"*"},"Action":"es:*","Resource":"arn:aws:es:us-east-1:835751346093:domain/*/*"},{"Effect":"Allow","Principal":{"AWS":"*"},"Action":"es:ESCrossClusterGet","Resource":"arn:aws:es:us-east-1:835751346093:domain/*"}]}',
            AccessPolicies=OPENSEARCH_AccessPolicies,
            DomainEndpointOptions={
                'EnforceHTTPS': True,
                'TLSSecurityPolicy': 'Policy-Min-TLS-1-2-2019-07'
            },
            AdvancedSecurityOptions={
                'Enabled': True,
                'InternalUserDatabaseEnabled': True,
                'MasterUserOptions': {
                    'MasterUserName': OPENSEARCH_User,
                    'MasterUserPassword': OPENSEARCH_Password
                }
            }
        )
        add_cluster_information_table_record(AOS_Name)
        logger.info("OpenSearch cluster created successfully!")
    except Exception as e:
        logger.error(
            f"Failed to create OpenSearch cluster! Error message: {e}")
        # Throw the exception message so that it can be caught and handled correctly at the outer layer.
        raise e


def add_cluster_information_table_record(cluster_name):
    dynamodb = boto3.resource('dynamodb')
    table_name = 'cluster_information_table'
    table = dynamodb.Table(table_name)
    """
    Add a new record to DynamoDB.
    
    Args:
        cluster_name (str): Name of the cluster to add the record for.
        
    Returns:
        dict: The added record.
    """
    try:
        item = {
            "cluster_name": cluster_name,
            "cluster_status": -1,
            "cluster_url": "",
            "create_ts": datetime.now().strftime("%m/%d/%Y, %H:%M:%S"),
            "id": str(uuid.uuid4()),
            "is_dashboard": "0",
            "update_ts": datetime.now().strftime("%m/%d/%Y, %H:%M:%S")
        }
        response = table.put_item(Item=item)
        logger.info(f"Added new item: {item}")
        return item
    except Exception as e:
        logger.exception(f"Error adding item: {item}")
        raise e


def update_bottom_cluster_status_4(statistical_table_name='cluster_statistical_table'):
    try:
        """1. Get cluster_statistical_table dataframe"""
        # Create DynamoDB resource
        dynamodb = boto3.resource('dynamodb')
        table = dynamodb.Table(statistical_table_name)

        # Calculate the start time of the query
        now = datetime.now()
        start_time = now - timedelta(minutes=ThresholdDuration + 5)
        start_time_str = start_time.strftime("%m/%d/%Y, %H:%M:%S")

        # Use conditional expressions to filter out eligible records
        response = table.scan(
            FilterExpression=Attr('event_ts').gt(start_time_str)
        )

        items = response['Items']

        if len(items) > 0:
            items_df = pd.DataFrame(items)
            items_df_filtered = items_df.loc[items_df['is_dashboard'] == 0]
            items_df_filtered
            logger.info(
                "Successful acquisition of dataframe data of statistical table")

            grouped_items_df = items_df_filtered.groupby(
                'cluster_name').agg({'workload': 'min'})
            lowest_workload_cluster = grouped_items_df.sort_values(
                'workload', ascending=True).index[0]
            print(lowest_workload_cluster)
            update_status_items(table_name='cluster_information_table',
                                status=3, update_status=4, cluster_name=lowest_workload_cluster)

        else:
            logger.info(
                "The data sample of the cluster statistical table is not enough.")
            return None
    except Exception as e:
        logger.exception(
            "An error occurred while executing the function: {}".format(e))
        return None

# @handle_error


def lambda_handler(event, context):
    if AutoScaling == 0 or \
            judgment is None or \
            exist_status_eq_4() is None or \
            exist_status_lt_3() is None:
        logger.info(
            "Not satisfied with the judgment of cluster scaling conditions, waiting for the next detection."),
        pass
    else:
        automatic_scaling(scaling=judgment,
                          exist_non_ingested_clusters=exist_status_eq_4(),  # exist status = 4
                          exist_initializing_clusters=exist_status_lt_3(),  # exist status < 3
                          )
