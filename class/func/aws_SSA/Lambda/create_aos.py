import json
import logging
import os
import boto3
from botocore import config
from botocore.exceptions import ClientError
import uuid

OPENSEARCH_DOMAIN_NAME = os.environ['OPENSEARCH_DOMAIN_NAME','1111']
OPENSEARCH_SUBNET_IDS = os.environ['OPENSEARCH_SUBNET_IDS'].split(',')
OPENSEARCH_SECURITY_GROUP_IDS = os.environ['OPENSEARCH_SECURITY_GROUP_IDS'].split(',')
OPENSEARCH_VERSION = os.environ['OPENSEARCH_VERSION']
OPENSEARCH_ACCESS_POLICIES = os.environ['OPENSEARCH_ACCESS_POLICIES']
OPENSEARCH_DEDICATED_MASTER_ENABLED = os.environ.get('OPENSEARCH_DEDICATED_MASTER_ENABLED', '').lower() == 'true'
OPENSEARCH_DEDICATED_MASTER_COUNT = int(os.environ.get('OPENSEARCH_DEDICATED_MASTER_COUNT', '0'))
OPENSEARCH_NODE_TO_NODE_ENCRYPTION_ENABLED = os.environ.get('OPENSEARCH_NODE_TO_NODE_ENCRYPTION_ENABLED', '').lower() == 'true'
OPENSEARCH_ADVANCED_SECURITY_OPTIONS_ENABLED = os.environ.get('OPENSEARCH_ADVANCED_SECURITY_OPTIONS_ENABLED', '').lower() == 'true'
OPENSEARCH_MASTER_USERNAME = os.environ.get('OPENSEARCH_MASTER_USERNAME', '')
OPENSEARCH_MASTER_USER_PASSWORD = os.environ.get('OPENSEARCH_MASTER_USER_PASSWORD', '')
OPENSEARCH_NODE_TYPE_NAMES = os.environ['OPENSEARCH_NODE_TYPE_NAMES'].split(',')
OPENSEARCH_NODE_INSTANCE_TYPES = os.environ['OPENSEARCH_NODE_INSTANCE_TYPES'].split(',')
OPENSEARCH_WARM_ENABLED = os.environ.get('OPENSEARCH_WARM_ENABLED', '').lower() == 'true'
OPENSEARCH_EBS_ENABLED = os.environ.get('OPENSEARCH_EBS_ENABLED', '').lower() == 'true'
OPENSEARCH_EBS_VOLUME_TYPE = os.environ.get('OPENSEARCH_EBS_VOLUME_TYPE', '')
OPENSEARCH_EBS_IOPS = int(os.environ.get('OPENSEARCH_EBS_IOPS', '0'))

opensearch = boto3.client('opensearch', region_name=os.environ['REGION'])
iam = boto3.client('iam')


def create_opensearch_domain():
    vpc_options = {
        'SubnetIds': OPENSEARCH_SUBNET_IDS,
        'SecurityGroupIds': OPENSEARCH_SECURITY_GROUP_IDS
    }

    # configure node types
    node_types = []
    for i in range(len(OPENSEARCH_NODE_TYPE_NAMES)):
        node_type = {
            'Name': OPENSEARCH_NODE_TYPE_NAMES[i],
            'InstanceType': OPENSEARCH_NODE_INSTANCE_TYPES[i]
        }
        # configure dedicated master node
        if OPENSEARCH_DEDICATED_MASTER_ENABLED:
            node_type.update({
                'DedicatedMasterEnabled': True,
                'DedicatedMasterType': OPENSEARCH_NODE_INSTANCE_TYPES[i],
                'DedicatedMasterCount': OPENSEARCH_DEDICATED_MASTER_COUNT
            })
        # configure ultra-warm node
        if OPENSEARCH_WARM_ENABLED:
            node_type.update({
                'WarmEnabled': True
            })
        node_types.append(node_type)

    # configure ebs options
    ebs_options = {}
    if OPENSEARCH_EBS_ENABLED:
        ebs_options.update({
            'EBSEnabled': True,
            'VolumeType': OPENSEARCH_EBS_VOLUME_TYPE,
            'Iops': OPENSEARCH_EBS_IOPS
        })

    # configure advanced security options
    advanced_security_options = {
        'Enabled': OPENSEARCH_ADVANCED_SECURITY_OPTIONS_ENABLED
    }
    if OPENSEARCH_ADVANCED_SECURITY_OPTIONS_ENABLED:
        advanced_security_options.update({
            'MasterUserOptions': {
                'MasterUserName': OPENSEARCH_MASTER_USERNAME,
                'MasterUserPassword': OPENSEARCH_MASTER_USER_PASSWORD
            }
        })

    # create the opensearch domain using boto3
    response = opensearch.create_domain(
        DomainName=OPENSEARCH_DOMAIN_NAME,
        ElasticsearchVersion=OPENSEARCH_VERSION,
        VPCOptions=vpc_options,
        AccessPolicies=OPENSEARCH_ACCESS_POLICIES,
        NodeToNodeEncryptionOptions={
            'Enabled': OPENSEARCH_NODE_TO_NODE_ENCRYPTION_ENABLED
        },
        AdvancedSecurityOptions=advanced_security_options,
        NodeTypes=node_types,
        EBSOptions=ebs_options
    )

    # check the status of the response
    if response['ResponseMetadata']['HTTPStatusCode'] == 200:
        print(f"OpenSearch domain: {OPENSEARCH_DOMAIN_NAME} created successfully!")
    else:
        print(f"Failed to create OpenSearch domain: {OPENSEARCH_DOMAIN_NAME}. Reason: {response}")


create_opensearch_domain()
