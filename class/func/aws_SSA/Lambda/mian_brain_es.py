import json
from botocore import config
import logging
import os
from botocore import config
from botocore.exceptions import ClientError
import uuid
from datetime import datetime, timedelta
import boto3
import time
import requests


logger = logging.getLogger()
logger.setLevel(logging.INFO)


class ErrorCode:
    DUPLICATED_INDEX_PREFIX = "DuplicatedIndexPrefix"
    DUPLICATED_WITH_INACTIVE_INDEX_PREFIX = "DuplicatedWithInactiveIndexPrefix"
    OVERLAP_INDEX_PREFIX = "OverlapIndexPrefix"
    OVERLAP_WITH_INACTIVE_INDEX_PREFIX = "OverlapWithInactiveIndexPrefix"
    INVALID_INDEX_MAPPING = "InvalidIndexMapping"


class APIException(Exception):
    def __init__(self, message, code: str = None):
        if code:
            super().__init__("[{}] {}".format(code, message))
        else:
            super().__init__(message)


def handle_error(func):
    """Decorator for exception handling"""

    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except APIException as e:
            logger.exception(e)
            raise e
        except Exception as e:
            logger.exception(e)
            raise RuntimeError(
                "Unknown exception, please check Lambda log for more details"
            )

    return wrapper


# def query_kendra(Kendra_index_id="", search_query_text="what is s3?",Kendra_result_num=3):
#     # 连接到Kendra
#     client = boto3.client('kendra')

#     # 构造Kendra查询请求
#     query_result = client.query(
#         IndexId=Kendra_index_id,
#         QueryText=search_query_text
#     )
#     # print(query_result['ResponseMetadata']['HTTPHeaders'])
#     # kendra_took = query_result['ResponseMetadata']['HTTPHeaders']['x-amz-time-millis']
#     # 创建一个结果列表
#     results = []

#     # 将每个结果添加到结果列表中
#     for result in query_result['ResultItems']:
#         # 创建一个字典来保存每个结果
#         result_dict = {}

#         result_dict['title'] = result['DocumentTitle']['Text']
#         result_dict['id'] = result['DocumentId']

#         # 如果有可用的总结
#         if 'DocumentExcerpt' in result:
#             result_dict['excerpt'] = result['DocumentExcerpt']['Text']
#         else:
#             result_dict['excerpt'] = ''

#         # 将结果添加到列表中
#         results.append(result_dict)

#     # 输出结果列表
#     return {"kendra_results":results[:Kendra_result_num]}


# def query_opensearch(api_url, query_type, query_params):
#     headers = {'Content-Type': 'application/json'}
#     if query_type == 'vector':  # 向量查询
#         query_json = {
#             "size": query_params['size'],
#             "query": {
#                 "knn": {
#                     query_params['field']: {
#                         "vector": query_params['vector_values'],
#                         "k": query_params['k']
#                     }
#                 }
#             }
#         }
#     elif query_type == 'match': # 字符串匹配查询
#         query_json = {
#             "size": query_params['size'],
#             "query": {
#                 "match": {
#                     query_params['doc']: query_params['match_keyword']
#                 }
#             }
#         }

#     response = requests.post(api_url, headers=headers, data=json.dumps(query_json))
#     if response.status_code == 200:
#         # 请求成功
#         response_json = json.loads(response.text)
#         hits = response_json['hits']['hits']
#         return [hit['_source'] for hit in hits]
#     else:
#         # 请求失败
#         print(f"请求失败: {response.status_code} - {response.text}")
#         return []


def get_vector_by_sm_endpoint(questions, sm_client, endpoint_name, parameters):
    response_model = sm_client.invoke_endpoint(
        EndpointName=endpoint_name,
        Body=json.dumps(
            {
                "inputs": questions,
                "parameters": parameters
            }
        ),
        ContentType="application/json",
    )
    json_str = response_model['Body'].read().decode('utf8')
    json_obj = json.loads(json_str)
    embeddings = json_obj['sentence_embeddings']
    return embeddings


def search_using_aos_knn(q_embedding, hostname, index, source_includes, size):
    # awsauth = (username, passwd)
    # print(type(q_embedding))
    headers = {"Content-Type": "application/json"}
    query = {
        "size": size,
        "query": {
            "knn": {
                "embedding": {
                    "vector": q_embedding,
                    "k": size
                }
            }
        }
    }
    r = requests.post("https://"+hostname + "/" + index +
                      '/_search', headers=headers, json=query)
    return r.text


@handle_error
def lambda_handler(event, context):

    # # 获取当前时间戳
    # request_timestamp = int(time.time())  # 或者使用 time.time_ns() 获取纳秒级别的时间戳
    # logger.info(f'request_timestamp :{request_timestamp}')

    # # 创建日志组和日志流
    # log_group_name = '/aws/lambda/{}'.format(context.function_name)
    # log_stream_name = context.aws_request_id
    # client = boto3.client('logs')

    # # 接收触发AWS Lambda函数的事件
    # logger.info('The main brain has been activated, aws🚀!')

    # Kendra_index_id = os.environ.get("Kendra_index_id", "")
    # Kendra_result_num = int(os.environ.get("Kendra_result_num", ""))

    # ---------------------------------------------------------------

    # embedding invocation

    embedding_endpoint = os.environ.get("embedding_endpoint", "")
    logger.info(f'embedding_endpoint : {embedding_endpoint}')
    print(embedding_endpoint)

    sm_client = boto3.client("sagemaker-runtime")
    parameters = {
        # "early_stopping": True,
        # "length_penalty": 2.0,
        "max_new_tokens": 50,
        "temperature": 0,
        "min_length": 10,
        "no_repeat_ngram_size": 2,
    }

    query = """how does AWS Clean Rooms charging?"""
    query_embedding = get_vector_by_sm_endpoint(
        query, sm_client, embedding_endpoint, parameters)
    print(query_embedding)

    # aos invocation

    aos_endpoint = os.environ.get("aos_endpoint", "")
    logger.info(f'aos_results : {aos_endpoint}')
    print(aos_endpoint)

    aos_index = os.environ.get("aos_index", "")
    logger.info(f'aos_results : {aos_index}')
    print(aos_index)

    aos_knn_field = os.environ.get("aos_knn_field", "")
    logger.info(f'aos_results : {aos_knn_field}')
    print(aos_knn_field)

    aos_results = int(os.environ.get("aos_results", ""))
    logger.info(f'aos_results : {aos_results}')
    print(aos_results)

    source_includes = ["doc_type", "doc"]

    knn_result = search_using_aos_knn(
        query_embedding[0], aos_endpoint, aos_index, source_includes, aos_results)

    print("----------------------------------------------------------")
    print(knn_result)
    print("----------------------------------------------------------")

    # ---------------------------------------------------------------

    result = json.loads(knn_result)

    result = [{"doc": result["hits"]["hits"][i]["_source"]["doc"],
               "doc_type":result["hits"]["hits"][i]["_source"]["doc_type"],
               "score":result["hits"]["hits"][i]["_score"]} for i in range(3)]

    return result

    # kendra_data = query_kendra(Kendra_index_id,"what is s3?",Kendra_result_num)
    # opensearch_data =
    # opensearch_knn_data =

    # pre_prompt_data = {
    # "post_text":"what is s3?",
    # # "request_time":,
    # "opensearch_respose":"",
    # # "opensearch_respose_took":,
    # "opensearch_knn_respose":"",
    # # "opensearch_knn_respose_took":,
    # "kendra_respose":kendra_data['kendra_results'],
    # "event_time":int(time.time())
    # }
    # print(pre_prompt_data)
