import json
import boto3
import os


# todo
# 1. 整理 环境变量 ✅
# 3. 创建的 cluster 序号 应当遵循当前cluster info 信息表的最大值  
    # count info 表的记录数  ✅
# 2. 创建 aos  ✅
# 4. 开发 scaling 逻辑 query DDB 
# 5. update DDB  



def create_multi_aos_clusters(event, context):

    client = boto3.client('opensearch')

    
    CLUSTER_NUM = os.environ.get('CLUSTER_NUM', 3)
    CLUSTER_NAME = os.environ.get('CLUSTER_NAME', 'poc-binance-v9')

    for i in range(1, CLUSTER_NUM+1):
        OPENSEARCH_CLUSTER_NAME = CLUSTER_NAME + '-' + str(i)
        OPENSEARCH_LATEST_VERSION = client.list_versions()['Versions'][0]

        OPENSEARCH_INSTANCE_TYPE = "r6g.12xlarge.search"
        OPENSEARCH_INSTANCE_NUM = 6
        OPENSEARCH_DedicatedMasterEnabled = True
        OPENSEARCH_ZoneAwarenessEnabled = True
        OPENSEARCH_AvailabilityZoneCount = 2
        OPENSEARCH_DedicatedMasterType = "m6g.xlarge.search"
        OPENSEARCH_DedicatedMasterCount = 3
        OPENSEARCH_WarmEnabled = True
        OPENSEARCH_WarmType = "ultrawarm1.large.search"
        OPENSEARCH_WarmCount = 2
        OPENSEARCH_ColdStorageOptions = True

        OPENSEARCH_EBS_EBSEnabled = True
        OPENSEARCH_EBS_VolumeType = "gp3"
        OPENSEARCH_EBS_VolumeSize = 24576
        OPENSEARCH_EBS_Iops = 16000
        OPENSEARCH_EBS_Throughput = 1000

        OPENSEARCH_Ids = ["subnet-0a8561b20a5c71ac9",
                          "subnet-0d2375de41fbda3e1"]
        OPENSEARCH_SecurityGroupIds = ["sg-0708b2efbc03b7d46"]

        AOS_ROOT = "root"  # insert your values here
        AOS_PASSWORD = "AWS2themoon100$"  # insert your values here

        # ------

        response = client.create_domain(
            DomainName=OPENSEARCH_CLUSTER_NAME,
            EngineVersion=OPENSEARCH_LATEST_VERSION,
            ClusterConfig={
                'InstanceType': OPENSEARCH_INSTANCE_TYPE,
                'InstanceCount': OPENSEARCH_INSTANCE_NUM,
                'DedicatedMasterEnabled': OPENSEARCH_DedicatedMasterEnabled,
                'ZoneAwarenessEnabled': OPENSEARCH_ZoneAwarenessEnabled,
                'ZoneAwarenessConfig': {'AvailabilityZoneCount': OPENSEARCH_AvailabilityZoneCount},
                'DedicatedMasterType': OPENSEARCH_DedicatedMasterType,
                'DedicatedMasterCount': OPENSEARCH_DedicatedMasterCount,
                'WarmEnabled': OPENSEARCH_WarmEnabled,
                'WarmType': OPENSEARCH_WarmType,
                'WarmCount': OPENSEARCH_WarmCount,
                'ColdStorageOptions': {'Enabled': OPENSEARCH_ColdStorageOptions}
            },
            EBSOptions={
                'EBSEnabled': OPENSEARCH_EBS_EBSEnabled,
                'VolumeType': OPENSEARCH_EBS_VolumeType,
                'VolumeSize': OPENSEARCH_EBS_VolumeSize,
                'Iops': OPENSEARCH_EBS_Iops,
                'Throughput': OPENSEARCH_EBS_Throughput
            },
            VPCOptions={
                'SubnetIds': OPENSEARCH_Ids,
                'SecurityGroupIds': OPENSEARCH_SecurityGroupIds
            },
            CognitoOptions={
                'Enabled': False
            },
            NodeToNodeEncryptionOptions={
                'Enabled': True
            },
            EncryptionAtRestOptions={
                'Enabled': True
            },
            AccessPolicies='{"Version":"2012-10-17","Statement":[{"Effect":"Allow","Principal":{"AWS":"*"},"Action":"es:*","Resource":"arn:aws:es:us-east-1:835751346093:domain/*/*"},{"Effect":"Allow","Principal":{"AWS":"*"},"Action":"es:ESCrossClusterGet","Resource":"arn:aws:es:us-east-1:835751346093:domain/*"}]}',
            DomainEndpointOptions={
                'EnforceHTTPS': True,
                'TLSSecurityPolicy': 'Policy-Min-TLS-1-2-2019-07'
            },
            AdvancedSecurityOptions={
                'Enabled': True,
                'InternalUserDatabaseEnabled': True,
                'MasterUserOptions': {
                    'MasterUserName': AOS_ROOT,
                    'MasterUserPassword': AOS_PASSWORD
                }
            }
        )

    print("All clusters created successfully!")
