import json
from botocore import config
import logging
import os
from botocore import config
from botocore.exceptions import ClientError
import uuid
from datetime import datetime, timedelta
import boto3
import time
import requests


logger = logging.getLogger()
logger.setLevel(logging.INFO)


class ErrorCode:
    DUPLICATED_INDEX_PREFIX = "DuplicatedIndexPrefix"
    DUPLICATED_WITH_INACTIVE_INDEX_PREFIX = "DuplicatedWithInactiveIndexPrefix"
    OVERLAP_INDEX_PREFIX = "OverlapIndexPrefix"
    OVERLAP_WITH_INACTIVE_INDEX_PREFIX = "OverlapWithInactiveIndexPrefix"
    INVALID_INDEX_MAPPING = "InvalidIndexMapping"


class APIException(Exception):
    def __init__(self, message, code: str = None):
        if code:
            super().__init__("[{}] {}".format(code, message))
        else:
            super().__init__(message)


def handle_error(func):
    """Decorator for exception handling"""

    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except APIException as e:
            logger.exception(e)
            raise e
        except Exception as e:
            logger.exception(e)
            raise RuntimeError(
                "Unknown exception, please check Lambda log for more details"
            )

    return wrapper


def query_kendra(Kendra_index_id="", search_query_text="what is s3?", Kendra_result_num=3):
    # 连接到Kendra
    client = boto3.client('kendra')

    # 构造Kendra查询请求
    query_result = client.query(
        IndexId=Kendra_index_id,
        QueryText=search_query_text
    )
    # print(query_result['ResponseMetadata']['HTTPHeaders'])
    # kendra_took = query_result['ResponseMetadata']['HTTPHeaders']['x-amz-time-millis']
    # 创建一个结果列表
    results = []

    # 将每个结果添加到结果列表中
    for result in query_result['ResultItems']:
        # 创建一个字典来保存每个结果
        result_dict = {}

        result_dict['title'] = result['DocumentTitle']['Text']
        result_dict['id'] = result['DocumentId']

        # 如果有可用的总结
        if 'DocumentExcerpt' in result:
            result_dict['excerpt'] = result['DocumentExcerpt']['Text']
        else:
            result_dict['excerpt'] = ''

        # 将结果添加到列表中
        results.append(result_dict)

    # 输出结果列表
    return {"kendra_results": results[:Kendra_result_num]}


def query_opensearch(api_url, query_type, query_params, Opensearch_result_num=3):
    headers = {'Content-Type': 'application/json'}
    if query_type == 'vector':  # 向量查询
        query_json = {
            "size": Opensearch_result_num,
            "query": {
                "knn": {
                    query_params['field']: {
                        "vector": query_params['vector_values'],
                        "k": Opensearch_result_num
                    }
                }
            }
        }
    elif query_type == 'match':  # 字符串匹配查询
        query_json = {
            "size": Opensearch_result_num,
            "query": {
                "match": {
                    query_params['field']: query_params['match_keyword']
                }
            }
        }

    response = requests.post(api_url, headers=headers,
                             data=json.dumps(query_json))
    if response.status_code == 200:
        # 请求成功
        response_json = json.loads(response.text)
        hits = response_json['hits']['hits']
        return [hit['_source'] for hit in hits]
    else:
        # 请求失败
        print(f"请求失败: {response.status_code} - {response.text}")
        return []


@handle_error
def lambda_handler(event, context):
    # 获取当前时间戳
    request_timestamp = int(time.time())  # 或者使用 time.time_ns() 获取纳秒级别的时间戳
    logger.info(f'request_timestamp :{request_timestamp}')

    # 创建日志组和日志流
    log_group_name = '/aws/lambda/{}'.format(context.function_name)
    log_stream_name = context.aws_request_id
    client = boto3.client('logs')

    # 接收触发AWS Lambda函数的事件
    logger.info('The main brain has been activated, aws🚀!')

    Kendra_index_id = os.environ.get("Kendra_index_id", "")
    Kendra_result_num = int(os.environ.get("Kendra_result_num", ""))

    Opensearch_result_num = int(os.environ.get("Opensearch_result_num", ""))
    Opensearch_result_num = int(os.environ.get("Opensearch_result_num", ""))

    logger.info(f'Kendra_index_id : {Kendra_index_id}')
    logger.info(f'Kendra_result_num : {Kendra_result_num}')

    kendra_data = query_kendra(
        Kendra_index_id, "what is s3?", Kendra_result_num)
    opensearch_data = query_opensearch(
        api_url, query_type, query_params, Opensearch_result_num=3)
    opensearch_knn_data =

    pre_prompt_data = {
        "post_text": "what is s3?",
        # "request_time":,
        "opensearch_respose": "",
        # "opensearch_respose_took":,
        "opensearch_knn_respose": "",
        # "opensearch_knn_respose_took":,
        "kendra_respose": kendra_data['kendra_results'],
        "event_time": int(time.time())
    }
    print(pre_prompt_data)
