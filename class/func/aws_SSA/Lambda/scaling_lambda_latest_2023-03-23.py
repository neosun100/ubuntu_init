# Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
# SPDX-License-Identifier: Apache-2.0

import json
from botocore import config
import logging
import os
from botocore import config
from botocore.exceptions import ClientError
import uuid
from datetime import datetime, timedelta
import pandas as pd
import boto3
from boto3.dynamodb.conditions import Key, Attr

logger = logging.getLogger()
logger.setLevel(logging.INFO)

DEFAULT_TIME_FORMAT = "%Y-%m-%dT%H:%M:%SZ"
CLUSTERINFO_TABLE_NAME = os.environ.get(
    'CLUSTERINFO_TABLE_NAME', 'CLUSTERINFO_TABLE_NAME')
STATISTICS_TABLE_NAME = os.environ.get(
    'STATISTICS_TABLE_NAME', 'WAFLog-StatisticsTableA0EC13A6-D0IG5CK39QLY')


ssm = boto3.client("ssm")


class ErrorCode:
    DUPLICATED_INDEX_PREFIX = "DuplicatedIndexPrefix"
    DUPLICATED_WITH_INACTIVE_INDEX_PREFIX = "DuplicatedWithInactiveIndexPrefix"
    OVERLAP_INDEX_PREFIX = "OverlapIndexPrefix"
    OVERLAP_WITH_INACTIVE_INDEX_PREFIX = "OverlapWithInactiveIndexPrefix"
    INVALID_INDEX_MAPPING = "InvalidIndexMapping"


class APIException(Exception):
    def __init__(self, message, code: str = None):
        if code:
            super().__init__("[{}] {}".format(code, message))
        else:
            super().__init__(message)


def handle_error(func):
    """Decorator for exception handling"""

    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except APIException as e:
            logger.exception(e)
            raise e
        except Exception as e:
            logger.exception(e)
            raise RuntimeError(
                "Unknown exception, please check Lambda log for more details"
            )

    return wrapper


def cluster_group_scaling_judgment(table_name=STATISTICS_TABLE_NAME,
                                   HighLoadThreshold=os.environ.get(
                                       'HighLoadThreshold', 0.75),
                                   LowLoadThreshold=os.environ.get(
                                       'LowLoadThreshold', 0.45),
                                   LoadThresholdDuration=os.environ.get('ThresholdDuration', 30)):
    """
    The logic for automatic scaling of cluster group scheduling.
    :param table_name: str, specifies the name of the DynamoDB table, defaults to 'cluster_statistical_table'.
    :param HighLoadThreshold: float, sets the threshold for high load, defaults to 75.
    :param LowLoadThreshold: float, sets the threshold for low load, defaults to 45.
    :param LoadThresholdDuration: int, sets the duration in minutes, defaults to 30.
    :return: int, 1 indicates cluster demand increased, -1 indicates cluster demand decreased, 0 indicates no adjustment is needed.
    """

    try:
        """1. Get cluster_statistical_table dataframe"""
        # Create DynamoDB resource
        dynamodb = boto3.resource('dynamodb')
        table = dynamodb.Table(table_name)

        # Calculate the start time of the query
        now = datetime.now()
        start_time = now - timedelta(minutes=LoadThresholdDuration + 15)
        start_time_str = start_time.strftime("%m/%d/%Y, %H:%M:%S")

        # Use conditional expressions to filter out eligible records
        response = table.scan(
            FilterExpression=Attr('event_ts').gt(start_time_str)
        )

        items = response['Items']

        if len(items) > 0:
            items_df = pd.DataFrame(items)
            items_df_filtered = items_df.loc[items_df['is_dashboard'] == 0]
            items_df_filtered
            logger.info(
                "Successful acquisition of dataframe data of statistical table")

            grouped_items_df = items_df_filtered.groupby(
                'cluster_name').agg({'workload': 'min'})
            highest_workload_cluster = grouped_items_df.sort_values(
                'workload', ascending=False).index[0]
            logger.info(
                f"highest_workload_cluster : {highest_workload_cluster}")
            num_records = items_df_filtered[items_df_filtered['cluster_name']
                                            == highest_workload_cluster].shape[0]
            logger.info(
                f"highest_workload_cluster_num_records : {highest_workload_cluster}:{num_records}")
            score = items_df_filtered.groupby(
                'cluster_name')['workload'].min().sort_values(
                ascending=False).iloc[0]

            # Define operations under different load conditions
            operations = {
                (score >= HighLoadThreshold) and (num_records > LoadThresholdDuration): lambda: 1,
                (score <= LowLoadThreshold) and (num_records > LoadThresholdDuration): lambda: -1,
                (LowLoadThreshold < score < HighLoadThreshold) or (num_records <= LoadThresholdDuration): lambda: 0
            }

            # Perform the corresponding operation according to the current load condition
            for condition, operation in operations.items():
                if condition:
                    return operation()

            return None

        else:
            logger.info(
                "The data sample of the cluster statistical table is not enough.")
            return None
    except Exception as e:
        logger.exception(
            "An error occurred while executing the function: {}".format(e))
        return None


def update_status_items(table_name=CLUSTERINFO_TABLE_NAME, status=3, update_status=4, cluster_name=None):
    try:
        # Create a connection to Dynamodb
        dynamodb = boto3.resource('dynamodb')
        # Access your desired table
        table = dynamodb.Table(table_name)

        # Scan the table and get all items with a status of 'status'
        # FilterExpression is used to specify the condition
        response = table.scan(
            FilterExpression=Key('cluster_status').eq(status)
        )

        # Get the list of items returned by the table scan
        items = response['Items']

        if cluster_name != None:
            for item in items:
                if item["cluster_name"] == cluster_name:
                    item["cluster_status"] = update_status
                    item["update_ts"] = datetime.now().strftime(
                        "%m/%d/%Y, %H:%M:%S")
                    response = table.put_item(Item=item)
                    logger.info(f"updated new item: {item}")
                    return item
                else:
                    logger.info(f"status cluster no include: {cluster_name}")

        else:

            # For each item that was returned, update the cluster_status to 'update_status'
            # and update the update_ts to the current time
            for item in items:
                item["cluster_status"] = update_status
                item["update_ts"] = datetime.now().strftime(
                    "%m/%d/%Y, %H:%M:%S")

            # Use batch_writer to update all items in the database at once
            with table.batch_writer() as batch:
                for item in items:
                    batch.put_item(Item=item)

            # Log an info message to indicate that the items have been successfully updated
            logger.info(
                f"Updated cluster status from {status} to {update_status}")

    except Exception as e:
        # If an error occurs, log an error message and the specific exception that was raised
        logger.error(
            f"An error occurred while updating cluster status: {str(e)}")


def get_count_by_status_condition(table_name='your_table_name', status_condition={'operator': 'gt', 'value': 4}):
    """
    Computes the number of items in a DynamoDB table that meet the specified condition.

    Args:
        table_name (str): The name of the DynamoDB table. Defaults to 'your_table_name'.
        status_condition (dict): A dictionary containing the filter condition. Defaults to {'operator': 'gt', 'value': 4}.
            The 'operator' can be one of the DynamoDB operators (e.g., lt, gt, eq), and 'value' is the value of the condition. Note that the data type of 'value' must match the data type of the 'status' field in the table.

    Returns:
        int: The number of items that meet the condition. Returns 0 if there is an error.
    """
    try:
        # Initialize the DynamoDB resource and table
        dynamodb = boto3.resource('dynamodb')
        table = dynamodb.Table(table_name)

        # Build the FilterExpression
        FilterExpression = getattr(boto3.dynamodb.conditions.Attr('cluster_status'),
                                   status_condition['operator'])(status_condition['value'])

        # Scan the table and count the items
        response = table.scan(FilterExpression=FilterExpression)
        count = response['Count']
        return count

    except Exception as e:
        # Handle exceptions
        logger.error(f'Error in get_count_by_status_condition: {e}')
        return 0


def exist_status_lt_3(): return 1 if get_count_by_status_condition(
    table_name=CLUSTERINFO_TABLE_NAME, status_condition={'operator': 'lt', 'value': 3}) > 0 else 0


def exist_status_eq_4(): return 1 if get_count_by_status_condition(
    table_name=CLUSTERINFO_TABLE_NAME, status_condition={'operator': 'eq', 'value': 4}) > 0 else 0


def num_status_lt_7(): return get_count_by_status_condition(
    table_name=CLUSTERINFO_TABLE_NAME, status_condition={'operator': 'lt', 'value': 7})


def automatic_scaling(scaling=cluster_group_scaling_judgment(table_name=STATISTICS_TABLE_NAME,
                                                             HighLoadThreshold=float(ssm.get_parameter(
                                                                 Name='/CL/HighLoadThreshold', WithDecryption=True)['Parameter']['Value']),
                                                             LowLoadThreshold=float(ssm.get_parameter(
        Name='/CL/LowLoadThreshold', WithDecryption=True)['Parameter']['Value']),
    LoadThresholdDuration=int(ssm.get_parameter(
        Name='/CL/ThresholdDuration', WithDecryption=True)['Parameter']['Value'])),
    exist_non_ingested_clusters=exist_status_eq_4(),  # exist status = 4
    exist_initializing_clusters=exist_status_lt_3(),  # exist status < 3
):
    """
    This function is a function that controls cluster scaling. It makes suggestions for scaling by querying cluster information tables and cluster statistics tables.



    """
    operations = {
        (scaling == 1) and (exist_non_ingested_clusters == 1): lambda: update_status_items(
            table_name=CLUSTERINFO_TABLE_NAME, status=4, update_status=3),
        (scaling == 1) and (exist_non_ingested_clusters == 0) and (
            exist_initializing_clusters == 1): lambda: logger.info(
            "The cluster needs to be expanded and is being expanded."),
        (scaling == 1) and (exist_non_ingested_clusters == 0) and (
            exist_initializing_clusters == 0): lambda: create_opensearch_by_cf(name='WAFLog'),
        (scaling == -1): lambda: update_bottom_cluster_status_4(),
        (scaling == 0): lambda: logger.info("The cluster group does not need to scale, wait for the next detection."),
        #         (scaling == 0) and (exist_non_ingested_clusters == 0): lambda: print("pass"),
        #         (scaling == 0) and (exist_non_ingested_clusters == 1) and (data_life_cycle_timeout == 1): lambda: print("Update DDB, update cluster status status = 5"),
        #         (scaling == 0) and (exist_non_ingested_clusters == 1) and (data_life_cycle_timeout == 0): lambda: print("pass")
    }
    for condition, operation in operations.items():
        if condition:
            return operation()
    return None


@handle_error
def create_opensearch_by_cf(name='WAFLog'):
    cf_client = boto3.client('cloudformation')
    stack_name = name
    response = cf_client.describe_stacks(
        StackName=stack_name
    )

    data = response['Stacks'][0]['Parameters']
    for item in data:
        if 'OpenSearch' in item['ParameterKey'] and item['ParameterValue'] == 'False':
            item['ParameterValue'] = 'True'
            break
    cf_client.update_stack(StackName=stack_name,
                           UsePreviousTemplate=True,
                           Parameters=data,
                           Capabilities=["CAPABILITY_IAM"])
    logger.info(f"Successfully created open search cluster！")


def add_cluster_information_table_record(cluster_name):
    dynamodb = boto3.resource('dynamodb')
    table_name = CLUSTERINFO_TABLE_NAME
    table = dynamodb.Table(table_name)
    """
    Add a new record to DynamoDB.
    
    Args:
        cluster_name (str): Name of the cluster to add the record for.
        
    Returns:
        dict: The added record.
    """
    try:
        item = {
            "cluster_name": cluster_name,
            "cluster_status": -1,
            "cluster_url": "",
            "create_ts": datetime.now().strftime("%m/%d/%Y, %H:%M:%S"),
            "id": str(uuid.uuid4()),
            "is_dashboard": 0,
            "update_ts": datetime.now().strftime("%m/%d/%Y, %H:%M:%S")
        }
        response = table.put_item(Item=item)
        logger.info(f"Added new item: {item}")
        return item
    except Exception as e:
        logger.exception(f"Error adding item: {item}")
        raise e


def update_bottom_cluster_status_4(statistical_table_name=STATISTICS_TABLE_NAME):
    try:
        """1. Get cluster_statistical_table dataframe"""
        # Create DynamoDB resource
        dynamodb = boto3.resource('dynamodb')
        table = dynamodb.Table(statistical_table_name)

        # Calculate the start time of the query
        now = datetime.now()
        start_time = now - timedelta(minutes=int(ssm.get_parameter(
            Name='/CL/ThresholdDuration', WithDecryption=True)['Parameter']['Value']) + 5)
        start_time_str = start_time.strftime("%m/%d/%Y, %H:%M:%S")

        # Use conditional expressions to filter out eligible records
        response = table.scan(
            FilterExpression=Attr('event_ts').gt(start_time_str)
        )

        items = response['Items']

        if len(items) > 0:
            items_df = pd.DataFrame(items)
            items_df_filtered = items_df.loc[items_df['is_dashboard'] == 0]
            items_df_filtered = items_df_filtered.loc[items_df_filtered['cluster_status'] == 3]
            # items_df_filtered
            # pd.set_option('display.max_columns', None)
            # pd.set_option('display.max_rows', 100)
            # pd.set_option('display.width', 200)
            # pd.set_option('display.max_colwidth', 200)
            # logger.info(items_df_filtered)
            logger.info(
                "Successful acquisition of dataframe data of statistical table")

            cluster_name_eq_4 = items_df_filtered.loc[items_df_filtered['cluster_status']
                                                      == 4, 'cluster_name']
            cluster_name_eq_3 = items_df_filtered.loc[items_df_filtered['cluster_status']
                                                      == 3, 'cluster_name']
            cluster_name_eq_4_list = cluster_name_eq_4.unique().tolist()
            cluster_name_eq_3_list = cluster_name_eq_3.unique().tolist()
            logger.info(f"cluster_name_eq_4:{cluster_name_eq_4_list}")
            logger.info(f"cluster_name_eq_3:{cluster_name_eq_3_list}")

            if len(items_df_filtered) > 0 and set(cluster_name_eq_4_list) & set(cluster_name_eq_3_list) == 0:
            # The presence of a data frame without duplicate elements between the 4-state and 3-state is tantamount to providing a buffer for scaling down.
                grouped_items_df = items_df_filtered.groupby(
                    'cluster_name').agg({'workload': 'min'})
                lowest_workload_cluster = grouped_items_df.sort_values(
                    'workload', ascending=True).index[0]
                logger.info(
                    f"lowest_workload_cluster：{lowest_workload_cluster}")
                update_status_items(table_name=CLUSTERINFO_TABLE_NAME,
                                    status=3, update_status=4, cluster_name=lowest_workload_cluster)
            else:
                logger.info(f"The cluster group has no more shrink space.")

        else:
            logger.info(
                "The data sample of the cluster statistical table is not enough.")
            return None
    except Exception as e:
        logger.exception(
            "An error occurred while executing the function: {}".format(e))
        return None

# @handle_error


def lambda_handler(event, context):

    currentClusterNum = num_status_lt_7()

    judgment = cluster_group_scaling_judgment(table_name=STATISTICS_TABLE_NAME,
                                              HighLoadThreshold=float(ssm.get_parameter(
                                                  Name='/CL/HighLoadThreshold', WithDecryption=True)['Parameter']['Value']),
                                              LowLoadThreshold=float(ssm.get_parameter(
                                                  Name='/CL/LowLoadThreshold', WithDecryption=True)['Parameter']['Value']),
                                              LoadThresholdDuration=int(ssm.get_parameter(
                                                  Name='/CL/ThresholdDuration', WithDecryption=True)['Parameter']['Value']))

    AutoScaling = int(ssm.get_parameter(Name='/CL/AutoScaling',
                      WithDecryption=True)['Parameter']['Value'])
    HighLoadThreshold = float(ssm.get_parameter(
        Name='/CL/HighLoadThreshold', WithDecryption=True)['Parameter']['Value'])
    LowLoadThreshold = float(ssm.get_parameter(
        Name='/CL/LowLoadThreshold', WithDecryption=True)['Parameter']['Value'])
    ThresholdDuration = int(ssm.get_parameter(
        Name='/CL/ThresholdDuration', WithDecryption=True)['Parameter']['Value'])
    DataRecovery = int(ssm.get_parameter(Name='/CL/DataRecovery',
                       WithDecryption=True)['Parameter']['Value'])
    MinClusterNum = int(ssm.get_parameter(Name='/CL/MinClusterNum',
                        WithDecryption=True)['Parameter']['Value'])
    MaxClusterNum = int(ssm.get_parameter(Name='/CL/MaxClusterNum',
                        WithDecryption=True)['Parameter']['Value'])

    logger.info(f"AutoScaling : {AutoScaling} ")
    logger.info(f"judgment : {judgment} ")
    logger.info(f"exist_status_eq_4(): {exist_status_eq_4()} ")
    logger.info(f"exist_status_lt_3(): {exist_status_lt_3()} ")
    logger.info(f"currentClusterNum: {currentClusterNum} ")
    logger.info(f"MaxClusterNum: {MaxClusterNum} ")
    logger.info(f"MinClusterNum: {MinClusterNum} ")
    logger.info(f"exist_non_ingested_clusters : {exist_status_eq_4()}")
    print(get_count_by_status_condition(table_name=CLUSTERINFO_TABLE_NAME,
          status_condition={'operator': 'lt', 'value': 7}))
    logger.info(f"currentClusterNum: {currentClusterNum}")

    if AutoScaling == 0 or \
        judgment is None or \
        exist_status_eq_4() is None or \
        exist_status_lt_3() is None or \
            currentClusterNum == MaxClusterNum or \
            currentClusterNum <= MinClusterNum:
        logger.info(
            "PASS Not satisfied with the judgment of cluster scaling conditions, waiting for the next detection.")
        logger.info(f"AutoScaling : {AutoScaling == 0} ")
        logger.info(f"judgment : {judgment is None} ")
        logger.info(f"exist_status_eq_4(): {exist_status_eq_4() is None} ")
        logger.info(f"exist_status_lt_3(): {exist_status_lt_3() is None} ")
        logger.info(f"currentClusterNum: {currentClusterNum} ")
        print(get_count_by_status_condition(table_name=CLUSTERINFO_TABLE_NAME,
              status_condition={'operator': 'lt', 'value': 7}))
        logger.info(
            f"currentClusterNum == MaxClusterNum {currentClusterNum == MaxClusterNum}")
        logger.info(
            f"currentClusterNum <= MinClusterNum {currentClusterNum <= MinClusterNum}")
        pass
    else:
        automatic_scaling(scaling=judgment,
                          exist_non_ingested_clusters=exist_status_eq_4(),  # exist status = 4
                          exist_initializing_clusters=exist_status_lt_3(),  # exist status < 3
                          )
