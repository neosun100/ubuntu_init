# param:    session_id
# return:   content
#           none
def get_session(session_id):

    table_name = "chatbot-session"
    dynamodb = boto3.resource('dynamodb')

    # table name
    table = dynamodb.Table(table_name)
    operation_result = ""

    response = table.get_item(Key={'session-id': session_id})

    if "Item" in response.keys():
        print("****** " + response["Item"]["content"])
        operation_result = response["Item"]["content"]
    else:
        print("****** No result")
        operation_result = "none"

    return operation_result


# param:    session_id
#           question
#           answer
# return:   success
#           failed
def udpate_session(session_id, question, answer):

    table_name = "chatbot-session"
    dynamodb = boto3.resource('dynamodb')

    # table name
    table = dynamodb.Table(table_name)
    operation_result = ""
    content = ""

    response = table.get_item(Key={'session-id': session_id})

    if "Item" in response.keys():
        print("****** " + response["Item"]["content"])
        content = response["Item"]["content"] + ", "
    else:
        print("****** No result")
        content = ""

    content = content + "('" + question + "', '" + answer + "')"

    # inserting values into table
    response = table.put_item(
        Item={
            'session-id': session_id,
            'content': content
        }
    )

    if "ResponseMetadata" in response.keys():
        if response["ResponseMetadata"]["HTTPStatusCode"] == 200:
            operation_result = "success"
        else:
            operation_result = "failed"
    else:
        operation_result = "failed"

    return operation_result
