insert into dwd_public_public_moomoo_bi_sensors
with t3 as (
    SELECT t1.uuid,
           t1.id,
           t1.time,
           t1.type,
           t1.project,
           t1.properties,
           t1.sensors_id,
           t1.event,
           -- convert(varchar, t1.json_data."$country")         as country,
           t1.json_data."$is_login_id"                       as is_login_id,
           convert(varchar, t1.json_data."$utm_medium")      as utm_medium,
           convert(varchar, t1.json_data."$short_url_key")   as short_url_key,
           convert(varchar, t1.json_data."$browser_version") as browser_version,
           convert(varchar, t1.json_data."$os_version")      as os_version,
           case t5.user_attribution
               when 'HK' then to_date(convert_timezone('Asia/Shanghai',cast(timestamp 'epoch' + t1.time * interval '0.001 second' as timestamp)),'yyyy-MM-dd HH24:MI:SS')
               when 'AU' then to_date(convert_timezone('Australia/Brisbane',cast(timestamp 'epoch' + t1.time * interval '0.001 second' as timestamp)),'yyyy-MM-dd HH24:MI:SS')
               when 'SG' then to_date(convert_timezone('Asia/Shanghai',cast(timestamp 'epoch' + t1.time * interval '0.001 second' as timestamp)),'yyyy-MM-dd HH24:MI:SS')
               else to_date(convert_timezone('AMT',cast(timestamp 'epoch' + t1.time * interval '0.001 second' as timestamp)),'yyyy-MM-dd HH24:MI:SS')
               end as pt_date,
           case when t4.reg_region is not null then t4.reg_region else nvl(t5.user_attribution,'US') end as user_attribution
    FROM bi_bi_sensors_super as t1
             left join user_web_account_auth as t4 on t1.id = t4.id
             left join dim_user_attribution t5
                       on t1.json_data."$country" = t5.country
    WHERE t1.pt_date BETWEEN '2022-08-03' AND '2022-08-04')
select
t3.*, t3.time as encry_time
from t3
WHERE t3.pt_date='2022-08-03';