drop table char_table;
CREATE TABLE char_table
(
    uuid             character varying(256) ENCODE lzo,
    id               character varying(256) ENCODE lzo,
    time             bigint ENCODE az64,
    type             character varying(256) ENCODE lzo,
    project          character varying(256) ENCODE lzo,
    properties       character varying(10240) ENCODE lzo,
    sensors_id       character varying(256) ENCODE lzo,
    user_attribution character varying(256) ENCODE lzo,
    event            character varying(256) ENCODE lzo,
    pt_date          character varying(256) ENCODE lzo
);


COPY char_table
    FROM 's3://aws-virginia-kinesis-filehose-finebi-data/orc_demo/000000_0'
    IAM_ROLE 'arn:aws:iam::835751346093:role/aws-redshift-Federation=role'
    FORMAT ORC;
    
    
drop table super_table;
CREATE TABLE super_table
(
    uuid             character varying(256) ENCODE lzo,
    id               character varying(256) ENCODE lzo,
    time             bigint ENCODE az64,
    type             character varying(256) ENCODE lzo,
    project          character varying(256) ENCODE lzo,
    properties       character varying(10240) ENCODE lzo,
    sensors_id       character varying(256) ENCODE lzo,
    user_attribution character varying(256) ENCODE lzo,
    event            character varying(256) ENCODE lzo,
    pt_date          character varying(256) ENCODE lzo,
    json_data        super
);

insert into super_table
select *,
       json_parse(properties) as json_data
from char_table
limit 1;



select json_data from super_table;
# return
{"app_language":"1","user_attribution":1.0,"$os":"Android","platformType":"Android","$wifi":true,"$network_type":"WIFI","$ip":"58.152.172.166","$screen_height":2244,"$idmap_reason":"5:登录 ID[ftv1cOLD7qRy/V1mOwsEtsF5bnhmbDu8KKDXETUnZY0XMBg8CKAN4bYFZLUZ6f5e7eKE] 已经和匿名 ID[8c409af4a9bc6001] 绑定","is_pad":"0","$referrer_title":"个股详情页","stock_market":"1","$device_id":"cfd3b26db88ecb4d","stock_category":"0","channel_num":"407","$province":"香港","$app_id":"cn.futu.trader","is_harmonyOS":false,"stock_code":"BABA","is_login":true,"$lib_method":"code","$os_version":"10","stock_name":"阿里巴巴","$is_first_day":false,"$city":"香港","$model":"HMA-L29","$screen_width":1080,"$brand":"HUAWEI","$app_version":"12.25.7128","last_update_channel_num":"415","$lib":"Android","futu_device_id":"cfd3b26db88ecb4d","$country":"中国","$app_name":"富途牛牛","$lib_version":"6.2.6","$timezone_offset":-480,"$manufacturer":"HUAWEI","$is_login_id":true,"$track_signup_original_id":"cfd3b26db88ecb4d"}


select json_data.app_language from super_table;
# return
"1"