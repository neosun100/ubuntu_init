import pandas as pd
import timeit
import numpy as np
import random
import pickle
import json
from functools import wraps
from loguru import logger
import redshift_connector

# 数据库配置信息
DB_CONFIG = {
    'host': 'ibu-test-query-redshift-128.479422325623.us-east-1.redshift-serverless.amazonaws.com',
    'database': 'dev',
    'port': 5439,
    'user': 'admin',
    'password': 'CTRIP2themoon100$'
}

# 查询变量列表
VARIABLES = [
    'message_locale',
    'recent_pageviewdate',
    'membertype',
    'edm_discounts_subscribe',
    'hasvisited_app',
    'push_discounts_subscribe',
]


def exception_handler(func):
    """异常处理装饰器"""
    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except Exception as e:
            logger.error(f"An error occurred in {func.__name__}: {e}")
            raise
    return wrapper


@exception_handler
def connect_to_db(config):
    """连接到数据库并返回连接和游标"""
    conn = redshift_connector.connect(**config)
    cursor = conn.cursor()
    logger.info("Successfully connected to the database.")
    return conn, cursor


@exception_handler
def get_result_list(cursor, sql):
    """执行SQL查询并返回结果列表"""
    cursor.execute(sql)
    logger.info(f"Successfully executed SQL: {sql}")
    return [i[0] for i in cursor.fetchall()]


@exception_handler
def save_results_to_file(results, file_path='variables.json'):
    """将结果保存到文件"""
    with open(file_path, 'w') as file:
        json.dump(results, file)
        logger.info(f"Results saved to {file_path}.")


def load_variables_and_weights():
    """加载变量和权重信息"""
    with open('variables_weight.json', 'r') as file:
        data = json.load(file)

    variables_and_weights = {}
    for item in data:
        for key, value in item.items():
            variables_and_weights[key] = value

    return variables_and_weights


def load_sql_template():
    """加载SQL模板"""
    with open('/home/ec2-user/ctrip_ap_sql_templete.pkl', 'rb') as f:
        return pickle.load(f)


def sql_generator(gen_sql, variables_and_weights):
    """SQL生成器"""
    random.seed(1)
    while True:
        yield gen_sql.format(**{key: random.choices(value, weights=variables_and_weights[f"{key}_weight"], k=1)[0] for key, value in variables_and_weights.items() if key in VARIABLES})


def time_sql_execution(cursor, sql_generator, times_to_run=3):
    """计时SQL执行性能"""
    elapsed_times = []

    for i in range(times_to_run):
        sql = next(sql_generator)
        timer = timeit.Timer(lambda: get_result_list(cursor, sql))
        elapsed_time = timer.timeit(number=1)
        elapsed_times.append(elapsed_time)
        logger.info(f"Run {i+1}: Elapsed time {elapsed_time} seconds")

    max_time = np.max(elapsed_times)
    min_time = np.min(elapsed_times)
    avg_time = np.mean(elapsed_times)
    std_dev = np.std(elapsed_times)

    logger.info(f"Max Time: {max_time} seconds")
    logger.info(f"Min Time: {min_time} seconds")
    logger.info(f"Avg Time: {avg_time} seconds")
    logger.info(f"Std Dev: {std_dev} seconds")

    return max_time, min_time, avg_time, std_dev


@exception_handler
def save_df_to_csv(df, file_path='serial_result.csv'):
    """保存DataFrame到CSV文件"""
    df.to_csv(file_path, index=False)
    logger.info("DataFrame saved to csv file.")


def main():
    """主函数"""
    conn, cursor = connect_to_db(DB_CONFIG)
    variables_and_weights = load_variables_and_weights()
    gen_sql_list = load_sql_template()

    serial_result = [time_sql_execution(
        cursor, sql_generator(gen_sql_list[i], variables_and_weights)) for i in range(20)]

    df = pd.DataFrame(serial_result, columns=['Max', 'Min', 'Mean', 'Std'])
    df['Serial'] = ['ap-'+str(i+1) if i <
                    10 else 'selection-'+str(i-9) for i in range(20)]

    save_df_to_csv(df)


if __name__ == '__main__':
    main()
