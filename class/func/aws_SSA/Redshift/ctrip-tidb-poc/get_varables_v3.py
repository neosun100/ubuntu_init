from functools import wraps
from loguru import logger
import redshift_connector
import json

# 数据库配置信息
DB_CONFIG = {
    'host': 'ibu-test-query-redshift-128.479422325623.us-east-1.redshift-serverless.amazonaws.com',
    'database': 'dev',
    'port': 5439,
    'user': 'admin',
    'password': 'CTRIP2themoon100$'
}

# 查询变量列表
VARIABLES = [
    'message_locale',
    'recent_pageviewdate',
    'membertype',
    'edm_discounts_subscribe',
    'hasvisited_app',
    'push_discounts_subscribe',
]


def exception_handler(func):
    """
    异常处理装饰器，用于捕获和记录函数中的异常。

    :param func: 被装饰的函数
    :return: 装饰器包装后的函数
    """
    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except Exception as e:
            logger.error(f"An error occurred in {func.__name__}: {e}")
            raise  # 重新抛出异常以便外部捕获
    return wrapper


@exception_handler
def connect_to_db(config):
    """
    连接到数据库，并返回连接对象和游标。

    :param config: 数据库配置字典
    :return: (conn, cursor) 数据库连接和游标
    """
    conn = redshift_connector.connect(**config)
    cursor = conn.cursor()
    logger.info("Successfully connected to the database.")
    return conn, cursor


@exception_handler
def get_result_list(cursor, sql):
    """
    执行SQL查询，并返回结果列表。

    :param cursor: 数据库游标
    :param sql: SQL查询字符串
    :return: 结果列表
    """
    cursor.execute(sql)
    logger.info(f"Successfully executed SQL: {sql}")
    return [i[0] for i in cursor.fetchall()]


@exception_handler
def save_results_to_file(results, file_path='variables.json'):
    """
    将结果保存到JSON文件。

    :param results: 结果列表
    :param file_path: 文件路径字符串
    """
    with open(file_path, 'w') as file:
        json.dump(results, file)
        logger.info(f"Results saved to {file_path}.")


def main():
    """
    主函数，负责连接数据库，执行查询，保存结果，并清理资源。
    """
    conn, cursor = connect_to_db(DB_CONFIG)
    try:
        sql_queries = [
            f"SELECT DISTINCT {variable} FROM ibu.ibucdptidb.cdp_crm_uid_detail_basic WHERE {variable} IS NOT NULL LIMIT 100;" for variable in VARIABLES]
        results = [{v: get_result_list(cursor, sql)}
                   for v, sql in zip(VARIABLES, sql_queries)]
        save_results_to_file(results)
    finally:
        cursor.close()
        conn.close()
        logger.info("Database connection and cursor closed.")


if __name__ == '__main__':
    main()
