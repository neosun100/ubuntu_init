from functools import wraps
from loguru import logger
import redshift_connector
import json

# 定义配置参数
DB_CONFIG = {
    'host': 'ibu-test-query-redshift-128.479422325623.us-east-1.redshift-serverless.amazonaws.com',
    'database': 'dev',
    'port': 5439,
    'user': 'admin',
    'password': 'CTRIP2themoon100$'
}

VARIABLES = [
    'message_locale',
    'recent_pageviewdate',
    'membertype',
    'edm_discounts_subscribe',
    'hasvisited_app',
    'push_discounts_subscribe',
    'recent_pageviewdate'
]


def exception_handler(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except Exception as e:
            logger.error(f"An error occurred in {func.__name__}: {e}")
            raise  # re-throw the exception after logging it
    return wrapper


@exception_handler
def connect_to_db(config):
    conn = redshift_connector.connect(**config)
    cursor = conn.cursor()
    logger.info("Successfully connected to the database.")
    return conn, cursor


@exception_handler
def get_result_list(cursor, sql):
    cursor.execute(sql)
    logger.info(f"Successfully executed SQL: {sql}")
    return [i[0] for i in cursor.fetchall()]


@exception_handler
def save_results_to_file(results, file_path='variables.json'):
    with open(file_path, 'w') as file:
        json.dump(results, file)
        logger.info(f"Results saved to {file_path}.")


def main():
    conn, cursor = connect_to_db(DB_CONFIG)
    try:
        sql_queries = [
            f"SELECT DISTINCT {variable} FROM ibu.ibucdptidb.cdp_crm_uid_detail_basic WHERE {variable} IS NOT NULL LIMIT 100;" for variable in VARIABLES]
        results = [{v: get_result_list(cursor, sql)}
                   for v, sql in zip(VARIABLES, sql_queries)]
        save_results_to_file(results)
    finally:
        cursor.close()
        conn.close()
        logger.info("Database connection and cursor closed.")


if __name__ == '__main__':
    main()
