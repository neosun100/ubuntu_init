#!/bin/bash

# 定义需要的文件列表和对应的下载URL
declare -A files=(
    ["ctrip_ap_sql_templete_v1.py"]="http://cdn.neo.pub/image/ctrip_ap_sql_templete_v1.py"
    ["get_varables_weight_v0.py"]="http://cdn.neo.pub/image/get_varables_weight_v0.py"
    ["serial_performance_v6.py"]="http://cdn.neo.pub/image/serial_performance_v6.py"
)

# 检查文件是否存在，如果不存在则下载
for file in "${!files[@]}"; do
    if [ ! -f "$file" ]; then
        echo "$(TZ='Asia/Shanghai' date) - File $file not found, downloading..."
        wget "${files[$file]}"
    fi
done

# 执行python脚本，如果执行失败则打印错误信息并退出脚本
if ! python ctrip_ap_sql_templete_v1.py; then
    echo "$(TZ='Asia/Shanghai' date) - Error occurred while running ctrip_ap_sql_templete_v1.py" 1>&2
    exit 1
fi

if ! python get_varables_weight_v0.py; then
    echo "$(TZ='Asia/Shanghai' date) - Error occurred while running get_varables_weight_v0.py" 1>&2
    exit 1
fi

# 定义主机列表
hosts=("128" "256" "512")

# 循环处理每个主机
for host in "${hosts[@]}"; do
    echo "$(TZ='Asia/Shanghai' date) - Starting process for host ${host}" # 记录开始处理主机的日志，包含当前时间

    # 执行python命令，如果出错则打印错误信息并退出脚本
    if ! python serial_performance_v6.py --host "ibu-test-query-redshift-${host}.479422325623.us-east-1.redshift-serverless.amazonaws.com" --seed 100 --times_to_run 5 --csv_file "serial_performance_${host}.csv"; then
        echo "$(TZ='Asia/Shanghai' date) - Error occurred while processing host ${host}" 1>&2 # 打印错误信息到标准错误输出，包含当前时间
        exit 1                                                                                # 退出脚本，返回非零状态码表示错误
    fi

    echo "$(TZ='Asia/Shanghai' date) - Finished process for host ${host}" # 记录完成处理主机的日志，包含当前时间
done

echo "$(TZ='Asia/Shanghai' date) - All processes finished successfully" # 记录所有处理都成功完成的日志，包含当前时间
