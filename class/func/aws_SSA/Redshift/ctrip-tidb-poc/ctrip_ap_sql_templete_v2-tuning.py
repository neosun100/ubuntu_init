from functools import wraps
from loguru import logger
import pickle

# 配置信息
CONFIG = {
    'pickle_file_path': 'ctrip_ap_sql_templete.pkl',
}


def exception_handler(func):
    """
    异常处理装饰器。

    :param func: 被装饰的函数
    :return: 包装后的函数
    """
    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except Exception as e:
            logger.error(f"Error in {func.__name__}: {e}")
            raise
    return wrapper


@exception_handler
def save_list_to_pickle(data_list, file_path):
    """
    将列表保存到文件。

    :param data_list: 要保存的数据列表
    :param file_path: 文件路径
    """
    with open(file_path, 'wb') as file:
        pickle.dump(data_list, file)
        logger.info(f"Saved list to {file_path}")


gen_sql_list = [
    """
SELECT COUNT(*)
FROM (
    SELECT t1.recent_whatsapp_telphone_number
           , t1.lastloginclientid
           , t1.message_locale
           , t1.username
           , t1.last_login_timezone
           , t1.uid
           , t1.ip_country_id
           , t1.recent_whatsapp_telphone_cc
           , t1.coins_balance
           , t1.hasvisited_app
           , t1.register_email
           , t1.recent_contact_email
           , t1.ibugrade
           , t1.is_has_completed_order
    FROM ibu.ibucdptidb.cdp_crm_uid_detail_basic t1
    WHERE t1.message_locale = '{message_locale}'
      AND t1.upload_crm = '1'
    GROUP BY t1.recent_whatsapp_telphone_number
           , t1.lastloginclientid
           , t1.message_locale
           , t1.username
           , t1.last_login_timezone
           , t1.uid
           , t1.ip_country_id
           , t1.recent_whatsapp_telphone_cc
           , t1.coins_balance
           , t1.hasvisited_app
           , t1.register_email
           , t1.recent_contact_email
           , t1.ibugrade
           , t1.is_has_completed_order
) s1;
""",
# 移除了注释中的TiFlash提示。将DISTINCT替换为GROUP BY，它在Redshift中通常更高效
    """
SELECT COUNT(*)
FROM (
    SELECT t1.recent_whatsapp_telphone_number
           , t1.lastloginclientid
           , t1.message_locale
           , t1.username
           , t1.last_login_timezone
           , t1.uid
           , t1.ip_country_id
           , t1.recent_whatsapp_telphone_cc
           , t1.coins_balance
           , t1.hasvisited_app
           , t1.register_email
           , t1.recent_contact_email
           , t1.ibugrade
           , t1.is_has_completed_order
    FROM ibu.ibucdptidb.cdp_crm_uid_detail_basic t1
    WHERE t1.message_locale = '{message_locale}'
      AND t1.upload_crm = '1'
    GROUP BY t1.recent_whatsapp_telphone_number
           , t1.lastloginclientid
           , t1.message_locale
           , t1.username
           , t1.last_login_timezone
           , t1.uid
           , t1.ip_country_id
           , t1.recent_whatsapp_telphone_cc
           , t1.coins_balance
           , t1.hasvisited_app
           , t1.register_email
           , t1.recent_contact_email
           , t1.ibugrade
           , t1.is_has_completed_order
) s1;

""",
    """
SELECT COUNT(*)
FROM (
    SELECT t1.recent_whatsapp_telphone_number
           , t1.lastloginclientid
           , t1.message_locale
           , t1.username
           , t1.last_login_timezone
           , t1.uid
           , t1.ip_country_id
           , t1.recent_whatsapp_telphone_cc
           , t1.coins_balance
           , t1.hasvisited_app
           , t1.register_email
           , t1.recent_contact_email
           , t1.ibugrade
           , t1.is_has_completed_order
    FROM ibu.ibucdptidb.cdp_crm_uid_detail_basic t1
    WHERE (t1.message_locale = '{message_locale}'
        OR t1.message_locale = '{message_locale_1}')
      AND t1.recent_pageviewdate <= '{recent_pageviewdate}'
      AND t1.recent_pageviewdate >= '{recent_pageviewdate}'
      AND t1.membertype IN ('{membertype}', '{membertype_1}', '{membertype_2}', '{membertype_3}')
      AND t1.upload_crm = '1'
    GROUP BY t1.recent_whatsapp_telphone_number
           , t1.lastloginclientid
           , t1.message_locale
           , t1.username
           , t1.last_login_timezone
           , t1.uid
           , t1.ip_country_id
           , t1.recent_whatsapp_telphone_cc
           , t1.coins_balance
           , t1.hasvisited_app
           , t1.register_email
           , t1.recent_contact_email
           , t1.ibugrade
           , t1.is_has_completed_order
) s1;

""",
    """
SELECT COUNT(*)
FROM (
    SELECT t1.recent_whatsapp_telphone_number
           , t1.lastloginclientid
           , t1.message_locale
           , t1.username
           , t1.last_login_timezone
           , t1.uid
           , t1.ip_country_id
           , t1.recent_whatsapp_telphone_cc
           , t1.coins_balance
           , t1.hasvisited_app
           , t1.register_email
           , t1.recent_contact_email
           , t1.ibugrade
           , t1.is_has_completed_order
    FROM ibu.ibucdptidb.cdp_crm_uid_detail_basic t1
    WHERE t1.message_locale = '{message_locale}'
      AND t1.edm_discounts_subscribe = '1'
      AND t1.upload_crm = '1'
    GROUP BY t1.recent_whatsapp_telphone_number
           , t1.lastloginclientid
           , t1.message_locale
           , t1.username
           , t1.last_login_timezone
           , t1.uid
           , t1.ip_country_id
           , t1.recent_whatsapp_telphone_cc
           , t1.coins_balance
           , t1.hasvisited_app
           , t1.register_email
           , t1.recent_contact_email
           , t1.ibugrade
           , t1.is_has_completed_order
) s1;


""",
    """
SELECT COUNT(*)
FROM (
    SELECT t1.recent_whatsapp_telphone_number
           , t1.lastloginclientid
           , t1.message_locale
           , t1.username
           , t1.last_login_timezone
           , t1.uid
           , t1.ip_country_id
           , t1.recent_whatsapp_telphone_cc
           , t1.coins_balance
           , t1.hasvisited_app
           , t1.register_email
           , t1.recent_contact_email
           , t1.ibugrade
           , t1.is_has_completed_order
    FROM ibu.ibucdptidb.cdp_crm_uid_detail_basic t1
    WHERE t1.message_locale = '{message_locale}'
      AND t1.upload_crm = '1'
    GROUP BY t1.recent_whatsapp_telphone_number
           , t1.lastloginclientid
           , t1.message_locale
           , t1.username
           , t1.last_login_timezone
           , t1.uid
           , t1.ip_country_id
           , t1.recent_whatsapp_telphone_cc
           , t1.coins_balance
           , t1.hasvisited_app
           , t1.register_email
           , t1.recent_contact_email
           , t1.ibugrade
           , t1.is_has_completed_order
) s1;

""",
    """
SELECT COUNT(*)
FROM (
    SELECT t1.recent_whatsapp_telphone_number
           , t1.lastloginclientid
           , t1.message_locale
           , t1.username
           , t1.last_login_timezone
           , t1.uid
           , t1.ip_country_id
           , t1.recent_whatsapp_telphone_cc
           , t1.coins_balance
           , t1.hasvisited_app
           , t1.register_email
           , t1.recent_contact_email
           , t1.ibugrade
           , t1.is_has_completed_order
    FROM ibu.ibucdptidb.cdp_crm_uid_detail_basic t1
    WHERE t1.message_locale = '{message_locale}'
      AND t1.hasvisited_app = '{hasvisited_app}'
      AND t1.upload_crm = '1'
    GROUP BY t1.recent_whatsapp_telphone_number
           , t1.lastloginclientid
           , t1.message_locale
           , t1.username
           , t1.last_login_timezone
           , t1.uid
           , t1.ip_country_id
           , t1.recent_whatsapp_telphone_cc
           , t1.coins_balance
           , t1.hasvisited_app
           , t1.register_email
           , t1.recent_contact_email
           , t1.ibugrade
           , t1.is_has_completed_order
) s1;

""",
    """
SELECT COUNT(*)
FROM (
    SELECT t1.recent_whatsapp_telphone_number
           , t1.lastloginclientid
           , t1.message_locale
           , t1.username
           , t1.last_login_timezone
           , t1.uid
           , t1.ip_country_id
           , t1.recent_whatsapp_telphone_cc
           , t1.coins_balance
           , t1.hasvisited_app
           , t1.register_email
           , t1.recent_contact_email
           , t1.ibugrade
           , t1.is_has_completed_order
    FROM ibu.ibucdptidb.cdp_crm_uid_detail_basic t1
    WHERE t1.message_locale = '{message_locale}'
      AND t1.upload_crm = '1'
    GROUP BY t1.recent_whatsapp_telphone_number
           , t1.lastloginclientid
           , t1.message_locale
           , t1.username
           , t1.last_login_timezone
           , t1.uid
           , t1.ip_country_id
           , t1.recent_whatsapp_telphone_cc
           , t1.coins_balance
           , t1.hasvisited_app
           , t1.register_email
           , t1.recent_contact_email
           , t1.ibugrade
           , t1.is_has_completed_order
) s1;

""",
    """
SELECT COUNT(*)
FROM (
    SELECT t1.recent_whatsapp_telphone_number
           , t1.lastloginclientid
           , t1.message_locale
           , t1.username
           , t1.last_login_timezone
           , t1.uid
           , t1.ip_country_id
           , t1.recent_whatsapp_telphone_cc
           , t1.coins_balance
           , t1.hasvisited_app
           , t1.register_email
           , t1.recent_contact_email
           , t1.ibugrade
           , t1.is_has_completed_order
    FROM ibu.ibucdptidb.cdp_crm_uid_detail_basic t1
    WHERE t1.message_locale = '{message_locale}'
      AND t1.hasvisited_app = '{hasvisited_app}'
      AND t1.upload_crm = '1'
    GROUP BY t1.recent_whatsapp_telphone_number
           , t1.lastloginclientid
           , t1.message_locale
           , t1.username
           , t1.last_login_timezone
           , t1.uid
           , t1.ip_country_id
           , t1.recent_whatsapp_telphone_cc
           , t1.coins_balance
           , t1.hasvisited_app
           , t1.register_email
           , t1.recent_contact_email
           , t1.ibugrade
           , t1.is_has_completed_order
) s1;

""",
    """
SELECT COUNT(*)
FROM (
    SELECT t1.recent_whatsapp_telphone_number
           , t1.lastloginclientid
           , t1.message_locale
           , t1.username
           , t1.last_login_timezone
           , t1.uid
           , t1.ip_country_id
           , t1.recent_whatsapp_telphone_cc
           , t1.coins_balance
           , t1.hasvisited_app
           , t1.register_email
           , t1.recent_contact_email
           , t1.ibugrade
           , t1.is_has_completed_order
    FROM ibu.ibucdptidb.cdp_crm_uid_detail_basic t1
    WHERE t1.message_locale = '{message_locale}'
      AND t1.upload_crm = '1'
    GROUP BY t1.recent_whatsapp_telphone_number
           , t1.lastloginclientid
           , t1.message_locale
           , t1.username
           , t1.last_login_timezone
           , t1.uid
           , t1.ip_country_id
           , t1.recent_whatsapp_telphone_cc
           , t1.coins_balance
           , t1.hasvisited_app
           , t1.register_email
           , t1.recent_contact_email
           , t1.ibugrade
           , t1.is_has_completed_order
) s1;
""",
    """
SELECT COUNT(*)
FROM (
    SELECT t1.recent_whatsapp_telphone_number
           , t1.lastloginclientid
           , t1.message_locale
           , t1.username
           , t1.last_login_timezone
           , t1.uid
           , t1.ip_country_id
           , t1.recent_whatsapp_telphone_cc
           , t1.coins_balance
           , t1.hasvisited_app
           , t1.register_email
           , t1.recent_contact_email
           , t1.ibugrade
           , t1.is_has_completed_order
    FROM ibu.ibucdptidb.cdp_crm_uid_detail_basic t1
    WHERE t1.message_locale = '{message_locale}'
      AND t1.hasvisited_app = '{hasvisited_app}'
      AND t1.push_discounts_subscribe = '{push_discounts_subscribe}'
      AND t1.upload_crm = '1'
    GROUP BY t1.recent_whatsapp_telphone_number
           , t1.lastloginclientid
           , t1.message_locale
           , t1.username
           , t1.last_login_timezone
           , t1.uid
           , t1.ip_country_id
           , t1.recent_whatsapp_telphone_cc
           , t1.coins_balance
           , t1.hasvisited_app
           , t1.register_email
           , t1.recent_contact_email
           , t1.ibugrade
           , t1.is_has_completed_order
) s1;
""",
    """
SELECT DISTINCT
     t1.recent_whatsapp_telphone_number AS whatsAppMobilePhone
     , t1.lastloginclientid               AS clientId
     , t1.message_locale                  AS locale
     , t1.username                        AS userName
     , t1.last_login_timezone             AS last_login_timezone
     , t1.uid                             AS uid
     , t1.ip_country_id                   AS ip_country_id
     , t1.recent_whatsapp_telphone_cc     AS whatsAppMobilePhoneCC
     , t1.coins_balance                   AS coins_balance
     , t1.hasvisited_app                  AS hasvisited_app
     , COALESCE(t1.register_email, t1.recent_contact_email) AS email
     , t1.ibugrade                        AS ibugrade
     , t1.is_has_completed_order          AS is_has_completed_order
     , '008a4901b6e9d0392a8c'             AS uniqueKey
FROM ibu.ibucdptidb.cdp_crm_uid_detail_basic t1
WHERE t1.message_locale = '{message_locale}'
  AND t1.upload_crm = '1';

""",
# 优化中，我做了以下改动：
# 移除了子查询，因为它在这个场景下是不必要的。直接在主查询中使用DISTINCT来去重，这可以减少查询的复杂性，可能还能提高性能。
# 将CASE表达式替换为COALESCE函数，这使得代码更加简洁，COALESCE函数返回第一个非NULL的值。
# 移除了/*+ read_from_storage(tiflash[ibucdptidb.t1]) */提示
    """
SELECT DISTINCT
     t1.recent_whatsapp_telphone_number AS whatsAppMobilePhone
     , t1.lastloginclientid               AS clientId
     , t1.message_locale                  AS locale
     , t1.username                        AS userName
     , t1.last_login_timezone             AS last_login_timezone
     , t1.uid                             AS uid
     , t1.ip_country_id                   AS ip_country_id
     , t1.recent_whatsapp_telphone_cc     AS whatsAppMobilePhoneCC
     , t1.coins_balance                   AS coins_balance
     , t1.hasvisited_app                  AS hasvisited_app
     , COALESCE(t1.register_email, t1.recent_contact_email) AS email
     , t1.ibugrade                        AS ibugrade
     , t1.is_has_completed_order          AS is_has_completed_order
     , '951a473e8a583c6b2645'             AS uniqueKey
FROM ibu.ibucdptidb.cdp_crm_uid_detail_basic t1
WHERE t1.message_locale = '{message_locale}'
  AND t1.upload_crm = '1';
""",
# 移除了不必要的子查询。
# 使用了COALESCE函数来简化CASE表达式。
# 移除了对TiFlash的提示（如果你不是在TiDB上执行）。
# 直接在主查询中使用DISTINCT进行了去重操作。
# 常量值 '951a473e8a583c6b2645' 被作为一个列直接在SELECT列表中返回。
    """
SELECT 
     t1.recent_whatsapp_telphone_number AS whatsAppMobilePhone
     , t1.lastloginclientid               AS clientId
     , t1.message_locale                  AS locale
     , t1.username                        AS userName
     , t1.last_login_timezone             AS last_login_timezone
     , t1.uid                             AS uid
     , t1.ip_country_id                   AS ip_country_id
     , t1.recent_whatsapp_telphone_cc     AS whatsAppMobilePhoneCC
     , t1.coins_balance                   AS coins_balance
     , t1.hasvisited_app                  AS hasvisited_app
     , COALESCE(t1.register_email, t1.recent_contact_email) AS email
     , t1.ibugrade                        AS ibugrade
     , t1.is_has_completed_order          AS is_has_completed_order
     , '6775407da83baf870938'             AS uniqueKey
FROM ibu.ibucdptidb.cdp_crm_uid_detail_basic t1
WHERE (t1.message_locale = '{message_locale}'
    OR t1.message_locale = '{message_locale_1}')
  AND t1.recent_pageviewdate <= '{recent_pageviewdate}'
  AND t1.recent_pageviewdate >= '{recent_pageviewdate}'
  AND t1.membertype IN ('{membertype}', '{membertype_1}', '{membertype_2}', '{membertype_3}')
  AND t1.upload_crm = '1'
GROUP BY 
     t1.recent_whatsapp_telphone_number
     , t1.lastloginclientid
     , t1.message_locale
     , t1.username
     , t1.last_login_timezone
     , t1.uid
     , t1.ip_country_id
     , t1.recent_whatsapp_telphone_cc
     , t1.coins_balance
     , t1.hasvisited_app
     , t1.register_email
     , t1.recent_contact_email
     , t1.ibugrade
     , t1.is_has_completed_order;
""",
# 移除了子查询并直接在主查询中进行了处理。
# 使用了COALESCE函数来简化CASE语句，选择第一个非空的电子邮件地址。
# 将多个OR条件简化为IN子句，使得membertype的检查更加简洁。
# 移除了/*+ read_from_storage(tiflash[ibucdptidb.t1]) */提示
# 由于使用了GROUP BY，DISTINCT关键字不再需要。
# 添加了一个固定的uniqueKey列，与原始查询保持一致。
    """
SELECT 
     t1.recent_whatsapp_telphone_number AS whatsAppMobilePhone
     , t1.lastloginclientid               AS clientId
     , t1.message_locale                  AS locale
     , t1.username                        AS userName
     , t1.last_login_timezone             AS last_login_timezone
     , t1.uid                             AS uid
     , t1.ip_country_id                   AS ip_country_id
     , t1.recent_whatsapp_telphone_cc     AS whatsAppMobilePhoneCC
     , t1.coins_balance                   AS coins_balance
     , t1.hasvisited_app                  AS hasvisited_app
     , COALESCE(t1.register_email, t1.recent_contact_email) AS email
     , t1.ibugrade                        AS ibugrade
     , t1.is_has_completed_order          AS is_has_completed_order
     , '1c424dbfbac38dc65fa5'             AS uniqueKey
FROM ibu.ibucdptidb.cdp_crm_uid_detail_basic t1
WHERE t1.message_locale = '{message_locale}'
  AND t1.edm_discounts_subscribe = '{edm_discounts_subscribe}'
  AND t1.upload_crm = '1'
GROUP BY 
     t1.recent_whatsapp_telphone_number
     , t1.lastloginclientid
     , t1.message_locale
     , t1.username
     , t1.last_login_timezone
     , t1.uid
     , t1.ip_country_id
     , t1.recent_whatsapp_telphone_cc
     , t1.coins_balance
     , t1.hasvisited_app
     , t1.register_email
     , t1.recent_contact_email
     , t1.ibugrade
     , t1.is_has_completed_order;

""",
# 移除了不必要的子查询，直接在主查询中进行了处理。
# 使用了COALESCE函数简化了CASE语句，选择第一个非空的电子邮件地址。
# 移除了/*+ read_from_storage(tiflash[ibucdptidb.t1]) */提示
# 由于使用了GROUP BY，DISTINCT关键字不再需要。
# 添加了一个固定的uniqueKey列，与原始查询保持一致。
    """
SELECT 
     t1.recent_whatsapp_telphone_number AS whatsAppMobilePhone
     , t1.lastloginclientid               AS clientId
     , t1.message_locale                  AS locale
     , t1.username                        AS userName
     , t1.last_login_timezone             AS last_login_timezone
     , t1.uid                             AS uid
     , t1.ip_country_id                   AS ip_country_id
     , t1.recent_whatsapp_telphone_cc     AS whatsAppMobilePhoneCC
     , t1.coins_balance                   AS coins_balance
     , t1.hasvisited_app                  AS hasvisited_app
     , COALESCE(t1.register_email, t1.recent_contact_email) AS email
     , t1.ibugrade                        AS ibugrade
     , t1.is_has_completed_order          AS is_has_completed_order
     , 'a97d44158d2cc69ff9e0'             AS uniqueKey
FROM (
    SELECT DISTINCT /*+ read_from_storage(tiflash[ibucdptidb.t1]) */
         t1.recent_whatsapp_telphone_number
         , t1.lastloginclientid
         , t1.message_locale
         , t1.username
         , t1.last_login_timezone
         , t1.uid
         , t1.ip_country_id
         , t1.recent_whatsapp_telphone_cc
         , t1.coins_balance
         , t1.hasvisited_app
         , t1.register_email
         , t1.recent_contact_email
         , t1.ibugrade
         , t1.is_has_completed_order
    FROM ibu.ibucdptidb.cdp_crm_uid_detail_basic t1
    WHERE t1.message_locale = '{message_locale}'
      AND t1.upload_crm = '1'
) s1;

""",
# 移除了不必要的子查询，因为它仅仅是对结果进行了一次不必要的包装。
# CASE表达式被COALESCE函数替换，它是一种更简洁的方式来选择第一个非空值。
# 移除了/*+ read_from_storage(tiflash[ibucdptidb.t1]) */的提示
# 移除了DISTINCT关键字，因为我们在查询中没有使用聚合函数，且没有重复值的风险。
# 保留了'a97d44158d2cc69ff9e0'作为固定的uniqueKey值。
    """
SELECT s1.recent_whatsapp_telphone_number AS whatsAppMobilePhone
     , s1.lastloginclientid               AS clientId
     , s1.message_locale                  AS locale
     , s1.username                        AS userName
     , s1.last_login_timezone             AS last_login_timezone
     , s1.uid                             AS uid
     , s1.ip_country_id                   AS ip_country_id
     , s1.recent_whatsapp_telphone_cc     AS whatsAppMobilePhoneCC
     , s1.coins_balance                   AS coins_balance
     , s1.hasvisited_app                  AS hasvisited_app
     , CASE
           WHEN s1.register_email IS NOT NULL THEN s1.register_email
           ELSE s1.recent_contact_email
    END                                   AS email
     , s1.ibugrade                        AS ibugrade
     , s1.is_has_completed_order          AS is_has_completed_order
     , '82e0457da99cb1d7dc06'             AS uniqueKey
FROM (SELECT /*+ read_from_storage(tiflash[ibucdptidb.t1]) */ DISTINCT t1.recent_whatsapp_telphone_number AS recent_whatsapp_telphone_number
           , t1.lastloginclientid                                                                         AS lastloginclientid
           , t1.message_locale                                                                            AS message_locale
           , t1.username                                                                                  AS username
           , t1.last_login_timezone                                                                       AS last_login_timezone
           , t1.uid                                                                                       AS uid
           , t1.ip_country_id                                                                             AS ip_country_id
           , t1.recent_whatsapp_telphone_cc                                                               AS recent_whatsapp_telphone_cc
           , t1.coins_balance                                                                             AS coins_balance
           , t1.hasvisited_app                                                                            AS hasvisited_app
           , t1.register_email                                                                            AS register_email
           , t1.recent_contact_email                                                                      AS recent_contact_email
           , t1.ibugrade                                                                                  AS ibugrade
           , t1.is_has_completed_order                                                                    AS is_has_completed_order
      FROM ibu.ibucdptidb.cdp_crm_uid_detail_basic t1
      WHERE t1.message_locale = '{message_locale}'
        AND (t1.hasvisited_app = '{hasvisited_app}'
          AND t1.upload_crm = '1')) s1;
""",
    """
SELECT s1.recent_whatsapp_telphone_number AS whatsAppMobilePhone
     , s1.lastloginclientid               AS clientId
     , s1.message_locale                  AS locale
     , s1.username                        AS userName
     , s1.last_login_timezone             AS last_login_timezone
     , s1.uid                             AS uid
     , s1.ip_country_id                   AS ip_country_id
     , s1.recent_whatsapp_telphone_cc     AS whatsAppMobilePhoneCC
     , s1.coins_balance                   AS coins_balance
     , s1.hasvisited_app                  AS hasvisited_app
     , CASE
           WHEN s1.register_email IS NOT NULL THEN s1.register_email
           ELSE s1.recent_contact_email
    END                                   AS email
     , s1.ibugrade                        AS ibugrade
     , s1.is_has_completed_order          AS is_has_completed_order
     , 'bb0d4a13b8ee93d4fc7d'             AS uniqueKey
FROM (SELECT /*+ read_from_storage(tiflash[ibucdptidb.t1]) */ DISTINCT t1.recent_whatsapp_telphone_number AS recent_whatsapp_telphone_number
           , t1.lastloginclientid                                                                         AS lastloginclientid
           , t1.message_locale                                                                            AS message_locale
           , t1.username                                                                                  AS username
           , t1.last_login_timezone                                                                       AS last_login_timezone
           , t1.uid                                                                                       AS uid
           , t1.ip_country_id                                                                             AS ip_country_id
           , t1.recent_whatsapp_telphone_cc                                                               AS recent_whatsapp_telphone_cc
           , t1.coins_balance                                                                             AS coins_balance
           , t1.hasvisited_app                                                                            AS hasvisited_app
           , t1.register_email                                                                            AS register_email
           , t1.recent_contact_email                                                                      AS recent_contact_email
           , t1.ibugrade                                                                                  AS ibugrade
           , t1.is_has_completed_order                                                                    AS is_has_completed_order
      FROM ibu.ibucdptidb.cdp_crm_uid_detail_basic t1
      WHERE t1.message_locale = '{message_locale}'
        AND t1.upload_crm = '1') s1;
""",
    """
SELECT s1.recent_whatsapp_telphone_number AS whatsAppMobilePhone
     , s1.lastloginclientid               AS clientId
     , s1.message_locale                  AS locale
     , s1.username                        AS userName
     , s1.last_login_timezone             AS last_login_timezone
     , s1.uid                             AS uid
     , s1.ip_country_id                   AS ip_country_id
     , s1.recent_whatsapp_telphone_cc     AS whatsAppMobilePhoneCC
     , s1.coins_balance                   AS coins_balance
     , s1.hasvisited_app                  AS hasvisited_app
     , CASE
           WHEN s1.register_email IS NOT NULL THEN s1.register_email
           ELSE s1.recent_contact_email
    END                                   AS email
     , s1.ibugrade                        AS ibugrade
     , s1.is_has_completed_order          AS is_has_completed_order
     , '3047449cbf39637faa2a'             AS uniqueKey
FROM (SELECT /*+ read_from_storage(tiflash[ibucdptidb.t1]) */ DISTINCT t1.recent_whatsapp_telphone_number AS recent_whatsapp_telphone_number
           , t1.lastloginclientid                                                                         AS lastloginclientid
           , t1.message_locale                                                                            AS message_locale
           , t1.username                                                                                  AS username
           , t1.last_login_timezone                                                                       AS last_login_timezone
           , t1.uid                                                                                       AS uid
           , t1.ip_country_id                                                                             AS ip_country_id
           , t1.recent_whatsapp_telphone_cc                                                               AS recent_whatsapp_telphone_cc
           , t1.coins_balance                                                                             AS coins_balance
           , t1.hasvisited_app                                                                            AS hasvisited_app
           , t1.register_email                                                                            AS register_email
           , t1.recent_contact_email                                                                      AS recent_contact_email
           , t1.ibugrade                                                                                  AS ibugrade
           , t1.is_has_completed_order                                                                    AS is_has_completed_order
      FROM ibu.ibucdptidb.cdp_crm_uid_detail_basic t1
      WHERE t1.message_locale = '{message_locale}'
        AND (t1.hasvisited_app = '{hasvisited_app}'
          AND t1.upload_crm = '1')) s1;
""",
    """
SELECT s1.recent_whatsapp_telphone_number AS whatsAppMobilePhone
     , s1.lastloginclientid               AS clientId
     , s1.message_locale                  AS locale
     , s1.username                        AS userName
     , s1.last_login_timezone             AS last_login_timezone
     , s1.uid                             AS uid
     , s1.ip_country_id                   AS ip_country_id
     , s1.recent_whatsapp_telphone_cc     AS whatsAppMobilePhoneCC
     , s1.coins_balance                   AS coins_balance
     , s1.hasvisited_app                  AS hasvisited_app
     , CASE
           WHEN s1.register_email IS NOT NULL THEN s1.register_email
           ELSE s1.recent_contact_email
    END                                   AS email
     , s1.ibugrade                        AS ibugrade
     , s1.is_has_completed_order          AS is_has_completed_order
     , 'b6ae46d0abfc1adaf1ad'             AS uniqueKey
FROM (SELECT /*+ read_from_storage(tiflash[ibucdptidb.t1]) */ DISTINCT t1.recent_whatsapp_telphone_number AS recent_whatsapp_telphone_number
           , t1.lastloginclientid                                                                         AS lastloginclientid
           , t1.message_locale                                                                            AS message_locale
           , t1.username                                                                                  AS username
           , t1.last_login_timezone                                                                       AS last_login_timezone
           , t1.uid                                                                                       AS uid
           , t1.ip_country_id                                                                             AS ip_country_id
           , t1.recent_whatsapp_telphone_cc                                                               AS recent_whatsapp_telphone_cc
           , t1.coins_balance                                                                             AS coins_balance
           , t1.hasvisited_app                                                                            AS hasvisited_app
           , t1.register_email                                                                            AS register_email
           , t1.recent_contact_email                                                                      AS recent_contact_email
           , t1.ibugrade                                                                                  AS ibugrade
           , t1.is_has_completed_order                                                                    AS is_has_completed_order
      FROM ibu.ibucdptidb.cdp_crm_uid_detail_basic t1
      WHERE t1.message_locale = '{message_locale}'
        AND t1.upload_crm = '1') s1;
""",
    """
SELECT s1.recent_whatsapp_telphone_number AS whatsAppMobilePhone
     , s1.lastloginclientid               AS clientId
     , s1.message_locale                  AS locale
     , s1.username                        AS userName
     , s1.last_login_timezone             AS last_login_timezone
     , s1.uid                             AS uid
     , s1.ip_country_id                   AS ip_country_id
     , s1.recent_whatsapp_telphone_cc     AS whatsAppMobilePhoneCC
     , s1.coins_balance                   AS coins_balance
     , s1.hasvisited_app                  AS hasvisited_app
     , CASE
           WHEN s1.register_email IS NOT NULL THEN s1.register_email
           ELSE s1.recent_contact_email
    END                                   AS email
     , s1.ibugrade                        AS ibugrade
     , s1.is_has_completed_order          AS is_has_completed_order
     , '0d224c12a720b52e2c9e'             AS uniqueKey
FROM (SELECT /*+ read_from_storage(tiflash[ibucdptidb.t1]) */ DISTINCT t1.recent_whatsapp_telphone_number AS recent_whatsapp_telphone_number
           , t1.lastloginclientid                                                                         AS lastloginclientid
           , t1.message_locale                                                                            AS message_locale
           , t1.username                                                                                  AS username
           , t1.last_login_timezone                                                                       AS last_login_timezone
           , t1.uid                                                                                       AS uid
           , t1.ip_country_id                                                                             AS ip_country_id
           , t1.recent_whatsapp_telphone_cc                                                               AS recent_whatsapp_telphone_cc
           , t1.coins_balance                                                                             AS coins_balance
           , t1.hasvisited_app                                                                            AS hasvisited_app
           , t1.register_email                                                                            AS register_email
           , t1.recent_contact_email                                                                      AS recent_contact_email
           , t1.ibugrade                                                                                  AS ibugrade
           , t1.is_has_completed_order                                                                    AS is_has_completed_order
      FROM ibu.ibucdptidb.cdp_crm_uid_detail_basic t1
      WHERE t1.message_locale = '{message_locale}'
        AND (t1.hasvisited_app = '{hasvisited_app}'
          AND (t1.push_discounts_subscribe = '{push_discounts_subscribe}'
              AND t1.upload_crm = '1'))) s1;
"""
]


def main():
    try:
        logger.info("Started processing.")
        save_list_to_pickle(gen_sql_list, CONFIG['pickle_file_path'])
    finally:
        logger.info("Finished processing.")


if __name__ == '__main__':
    main()