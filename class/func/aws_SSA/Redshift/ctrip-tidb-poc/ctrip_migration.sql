SELECT
  s1.recent_whatsapp_telphone_number AS whatsAppMobilePhone,
  s1.lastloginclientid AS clientId,
  s1.message_locale AS locale,
  s1.username AS userName,
  s1.last_login_timezone,
  s1.uid,
  s1.ip_country_id,
  s1.recent_whatsapp_telphone_cc AS whatsAppMobilePhoneCC,
  s1.coins_balance,
  s1.hasvisited_app,
  COALESCE(s1.register_email, s1.recent_contact_email) AS email,
  s1.ibugrade,
  s1.is_has_completed_order,
  'ea1c44e58ee2c60b2a0e' AS uniqueKey
FROM (
  SELECT DISTINCT
    t1.recent_whatsapp_telphone_number,
    t1.lastloginclientid,
    t1.message_locale,
    t1.username,
    t1.last_login_timezone,
    t1.uid,
    t1.ip_country_id,
    t1.recent_whatsapp_telphone_cc,
    t1.coins_balance,
    t1.hasvisited_app,
    t1.register_email,
    t1.recent_contact_email,
    t1.ibugrade,
    t1.is_has_completed_order
  FROM
    cdp_crm_uid_detail_basic t1
  LEFT JOIN (
    SELECT
      t2.uid,
      MAX(t2.htl_checkin_time) AS tagValue
    FROM
      edw_ord_ibu_realtime_order_all t2
    WHERE
      t2.prdtype = 'H'
      AND t2.htl_provinceid = '32'
    GROUP BY
      t2.uid) s2
  ON t1.uid = s2.uid
  WHERE
    s2.tagValue BETWEEN '2023-09-03' AND '2023-09-04'
    AND t1.upload_crm = '1'
) s1


-- 主要更改：

-- 移除了TiDB特定的/*+ read_from_storage */提示。
-- 使用了COALESCE函数来替代CASE语句简化。
-- 使用了BETWEEN来优化日期范围查询。




SELECT
  s1.recent_whatsapp_telphone_number AS whatsAppMobilePhone,
  s1.lastloginclientid AS clientId,
  s1.message_locale AS locale,
  s1.username AS userName,
  s1.last_login_timezone,
  s1.uid,
  s1.ip_country_id,
  s1.recent_whatsapp_telphone_cc AS whatsAppMobilePhoneCC,
  s1.coins_balance,
  s1.hasvisited_app,
  COALESCE(s1.register_email, s1.recent_contact_email) AS email,
  s1.ibugrade,
  s1.is_has_completed_order,
  '91054b92a880df1ae6d3' AS uniqueKey
FROM (
  SELECT DISTINCT
    t1.recent_whatsapp_telphone_number,
    t1.lastloginclientid,
    t1.message_locale,
    t1.username,
    t1.last_login_timezone,
    t1.uid,
    t1.ip_country_id,
    t1.recent_whatsapp_telphone_cc,
    t1.coins_balance,
    t1.hasvisited_app,
    t1.register_email,
    t1.recent_contact_email,
    t1.ibugrade,
    t1.is_has_completed_order
  FROM
    cdp_crm_uid_detail_basic t1
  LEFT JOIN (
    SELECT
      t2.uid,
      MAX(t2.htl_checkin_time) AS tagValue
    FROM
      edw_ord_ibu_realtime_order_all t2
    WHERE
      t2.prdtype = 'H'
      AND t2.htl_cityid = '228'
    GROUP BY
      t2.uid) s2
  ON t1.uid = s2.uid
  WHERE
    s2.tagValue BETWEEN '2023-09-03' AND '2023-09-04'
    AND t1.message_locale = 'ja-jp'
    AND t1.upload_crm = '1'
) s1

-- 移除TiDB特定的/*+ read_from_storage */提示。
-- 使用COALESCE替代CASE简化逻辑。
-- 使用BETWEEN优化日期范围。



SELECT
  s1.recent_whatsapp_telphone_number AS whatsAppMobilePhone,
  s1.lastloginclientid AS clientId,
  s1.message_locale AS locale,
  s1.username AS userName,
  s1.last_login_timezone,
  s1.uid,
  s1.ip_country_id,
  s1.recent_whatsapp_telphone_cc AS whatsAppMobilePhoneCC,
  s1.coins_balance,
  s1.hasvisited_app,
  COALESCE(s1.register_email, s1.recent_contact_email) AS email,
  s1.ibugrade,
  s1.is_has_completed_order,
  '78844a1eb61584468f98' AS uniqueKey
FROM (
  SELECT DISTINCT
    t1.recent_whatsapp_telphone_number,
    t1.lastloginclientid,
    t1.message_locale,
    t1.username,
    t1.last_login_timezone,
    t1.uid,
    t1.ip_country_id,
    t1.recent_whatsapp_telphone_cc,
    t1.coins_balance,
    t1.hasvisited_app,
    t1.register_email,
    t1.recent_contact_email,
    t1.ibugrade,
    t1.is_has_completed_order
  FROM
    cdp_crm_uid_detail_basic t1
  WHERE
    t1.message_locale = 'ms-my'
    AND t1.upload_crm = '1'
) s1
-- 主要更改：

-- 移除TiDB特定的注释。
-- 使用COALESCE简化逻辑。



SELECT
  s1.recent_whatsapp_telphone_number AS whatsAppMobilePhone,
  s1.lastloginclientid AS clientId,
  s1.message_locale AS locale,
  s1.username AS userName,
  s1.last_login_timezone,
  s1.uid,
  s1.ip_country_id,
  s1.recent_whatsapp_telphone_cc AS whatsAppMobilePhoneCC,
  s1.coins_balance,
  s1.hasvisited_app,
  COALESCE(s1.register_email, s1.recent_contact_email) AS email,
  s1.ibugrade,
  s1.is_has_completed_order,
  '32cf46978859e585b8e3' AS uniqueKey
FROM (
  SELECT DISTINCT
    t1.recent_whatsapp_telphone_number,
    t1.lastloginclientid,
    t1.message_locale,
    t1.username,
    t1.last_login_timezone,
    t1.uid,
    t1.ip_country_id,
    t1.recent_whatsapp_telphone_cc,
    t1.coins_balance,
    t1.hasvisited_app,
    t1.register_email,
    t1.recent_contact_email,
    t1.ibugrade,
    t1.is_has_completed_order
  FROM
    cdp_crm_uid_detail_basic t1
  LEFT JOIN (
    SELECT
      t2.uid,
      MAX(t2.orderdate) AS tagValue
    FROM
      edw_ord_ibu_realtime_order_all t2
    WHERE
      t2.prdtype = 'F'
      AND t2.flt_arrival_provinceid = '32'
    GROUP BY
      t2.uid
  ) s2 ON t1.uid = s2.uid
  WHERE
    s2.tagValue BETWEEN '2023-09-02' AND '2023-09-03'
    AND t1.upload_crm = '1'
    AND t1.last_login_timezone = '+03:00'
) s1

-- 移除TiDB特定的注释。
-- 使用COALESCE简化逻辑。
-- 使用BETWEEN简化日期比较。



SELECT
  *
FROM (
  SELECT
    s1.uid,
    s1.recent_whatsapp_telphone_number AS whatsAppMobilePhone,
    s1.lastloginclientid AS clientId,
    s1.recent_whatsapp_telphone_cc AS whatsAppMobilePhoneCC,
    s1.message_locale AS locale,
    s1.username AS userName,
    COALESCE(s1.register_email, s1.recent_contact_email) AS email,
    'b7654a2498bbab876976' AS uniqueKey
  FROM (
    SELECT DISTINCT
      t1.uid,
      t1.recent_whatsapp_telphone_number,
      t1.lastloginclientid,
      t1.recent_whatsapp_telphone_cc,
      t1.message_locale,
      t1.username,
      t1.register_email,
      t1.recent_contact_email
    FROM
      cdp_crm_uid_detail_basic t1
    WHERE
      t1.register_locale IN ('de-de', 'fr-fr', 'it-it', 'en-gb', 'es-es')
      AND t1.upload_crm = 1
  ) s1
) tab_left
LEFT JOIN (
  SELECT
    uid AS right_uid,
    membertype AS right_membertype,
    socialname AS right_socialname,
    gender AS right_gender,
    message_locale AS right_message_locale,
    last_order_locale AS right_last_order_locale,
    last_visit_locale AS right_last_visit_locale,
    ip_country_id AS right_ip_country_id,
    ip_city_id AS right_ip_city_id,
    ip_province_id AS right_ip_province_id,
    localprovinceid AS right_localprovinceid,
    localcountryid AS right_localcountryid,
    localcityid AS right_localcityid,
    isgdpruser AS right_isgdpruser,
    last_visit_app_locale AS right_last_visit_app_locale,
    ibugrade AS right_ibugrade,
    hasvisited_app AS right_hasvisited_app,
    last_order_product AS right_last_order_product,
    is_has_completed_order AS right_is_has_completed_order,
    flt_prefer_acityid AS right_flt_prefer_acityid,
    flt_prefer_dcountryid AS right_flt_prefer_dcountryid,
    flt_prefer_dprovinceid AS right_flt_prefer_dprovinceid,
    flt_prefer_dcityid AS right_flt_prefer_dcityid,
    flt_prefer_acountryid AS right_flt_prefer_acountryid,
    flt_prefer_aprovinceid AS right_flt_prefer_aprovinceid,
    last_visit_platform AS right_last_visit_platform,
    htl_last_visit_cityid AS right_htl_last_visit_cityid,
    htl_last_visit_provinceid AS right_htl_last_visit_provinceid,
    htl_last_visit_countryid AS right_htl_last_visit_countryid,
    flt_recent_dcityid AS right_flt_recent_dcityid,
    flt_recent_dprovinceid AS right_flt_recent_dprovinceid,
    flt_recent_dcountryid AS right_flt_recent_dcountryid,
    flt_recent_acityid AS right_flt_recent_acityid,
    flt_recent_aprovinceid AS right_flt_recent_aprovinceid,
    flt_recent_acountryid AS right_flt_recent_acountryid,
    age_group AS right_age_group,
    is_has_htl_completed_order AS right_is_has_htl_completed_order,
    success_orders AS right_success_orders
  FROM
    cdp_crm_uid_detail_basic
  WHERE
    upload_crm = 1
) tab_right
ON tab_left.uid = tab_right.right_uid;
-- 移除TiDB特定的注释和优化提示。
-- 使用COALESCE简化逻辑。
-- 使用IN简化多个条件的比较。




SELECT
  s1.uid AS uid,
  s1.recent_whatsapp_telphone_number AS whatsAppMobilePhone,
  s1.lastloginclientid AS clientId,
  s1.recent_whatsapp_telphone_cc AS whatsAppMobilePhoneCC,
  s1.message_locale AS locale,
  s1.username AS userName,
  COALESCE(s1.register_email, s1.recent_contact_email) AS email,
  'c1b24760b35d1d98d2f5' AS uniqueKey
FROM (
  SELECT DISTINCT 
    t1.uid AS uid,
    t1.recent_whatsapp_telphone_number,
    t1.lastloginclientid,
    t1.recent_whatsapp_telphone_cc,
    t1.message_locale,
    t1.username,
    t1.register_email,
    t1.recent_contact_email
  FROM 
    cdp_crm_uid_detail_basic t1
  LEFT JOIN (
    SELECT
      t2.uid AS uid,
      MAX(t2.visit_time) AS tagValue
    FROM
      cdp_usr_ubt_trip_content t2
    WHERE
      t2.sub_prdtype = 'Moments' AND t2.platform = 'app'
    GROUP BY
      t2.uid
  ) s2 ON t1.uid = s2.uid
  WHERE 
    t1.message_locale = 'ja-jp' AND 
    (s2.tagValue BETWEEN '2022-09-04' AND '2023-09-04') AND 
    t1.upload_crm = '1'
) s1
LEFT JOIN (
  SELECT
    uid AS right_uid,
    membertype AS right_membertype,
    socialname AS right_socialname,
    gender AS right_gender,
    message_locale AS right_message_locale,
    last_order_locale AS right_last_order_locale,
    last_visit_locale AS right_last_visit_locale,
    ip_country_id AS right_ip_country_id,
    ip_city_id AS right_ip_city_id,
    ip_province_id AS right_ip_province_id,
    localprovinceid AS right_localprovinceid,
    localcountryid AS right_localcountryid,
    localcityid AS right_localcityid,
    isgdpruser AS right_isgdpruser,
    last_visit_app_locale AS right_last_visit_app_locale,
    ibugrade AS right_ibugrade,
    hasvisited_app AS right_hasvisited_app,
    last_order_product AS right_last_order_product,
    is_has_completed_order AS right_is_has_completed_order,
    flt_prefer_acityid AS right_flt_prefer_acityid,
    flt_prefer_dcountryid AS right_flt_prefer_dcountryid,
    flt_prefer_dprovinceid AS right_flt_prefer_dprovinceid,
    flt_prefer_dcityid AS right_flt_prefer_dcityid,
    flt_prefer_acountryid AS right_flt_prefer_acountryid,
    flt_prefer_aprovinceid AS right_flt_prefer_aprovinceid,
    last_visit_platform AS right_last_visit_platform,
    htl_last_visit_cityid AS right_htl_last_visit_cityid,
    htl_last_visit_provinceid AS right_htl_last_visit_provinceid,
    htl_last_visit_countryid AS right_htl_last_visit_countryid,
    flt_recent_dcityid AS right_flt_recent_dcityid,
    flt_recent_dprovinceid AS right_flt_recent_dprovinceid,
    flt_recent_dcountryid AS right_flt_recent_dcountryid,
    flt_recent_acityid AS right_flt_recent_acityid,
    flt_recent_aprovinceid AS right_flt_recent_aprovinceid,
    flt_recent_acountryid AS right_flt_recent_acountryid,
    age_group AS right_age_group,
    is_has_htl_completed_order AS right_is_has_htl_completed_order,
    success_orders AS right_success_orders
  FROM
    cdp_crm_uid_detail_basic
  WHERE
    upload_crm = 1
) tab_right ON s1.uid = tab_right.right_uid;

-- 删除了 TiDB 特定的查询优化注释。
-- 用 COALESCE 简化了 CASE 语句。
-- 简化了日期范围条件。

SELECT
  s1.msg_gateway_last_login_clientid,
  s1.uid,
  COALESCE(s1.lastloginclientid, s1.clientId) AS clientId,
  s1.ip_country_id,
  s1.coins_balance,
  s1.username AS userName,
  s1.message_locale AS locale,
  COALESCE(s1.register_email, s1.recent_contact_email, s1.email) AS email,
  s1.last_login_timezone,
  s1.ibugrade,
  '7afd4e85a5c1f14000ad' AS uniqueKey
FROM (
  SELECT DISTINCT 
    tbl2.clientId,
    tbl2.email,
    tbl2.uid,
    tbl2.ibugrade,
    tbl2.message_locale,
    tbl2.last_login_timezone,
    tbl2.msg_gateway_last_login_clientid,
    tbl2.lastloginclientid,
    tbl2.username,
    tbl2.register_email,
    tbl2.recent_contact_email,
    tbl2.ip_country_id,
    tbl2.coins_balance
  FROM (
    SELECT
      clientId,
      NULL AS email,
      NULL AS uid,
      NULL AS ibugrade,
      message_locale,
      NULL AS last_login_timezone,
      NULL AS msg_gateway_last_login_clientid,
      NULL AS lastloginclientid,
      NULL AS username,
      NULL AS register_email,
      NULL AS recent_contact_email,
      NULL AS ip_country_id,
      NULL AS coins_balance
    FROM
      cdp_crm_cid_detail_all
    WHERE
      oneid_potential = 1
    UNION ALL
    SELECT
      NULL AS clientId,
      email,
      uid,
      ibugrade,
      message_locale,
      last_login_timezone,
      msg_gateway_last_login_clientid,
      lastloginclientid,
      username,
      register_email,
      recent_contact_email,
      ip_country_id,
      coins_balance
    FROM
      cdp_crm_uid_detail_basic
    UNION ALL
    SELECT
      NULL AS clientId,
      email,
      NULL AS uid,
      NULL AS ibugrade,
      message_locale,
      NULL AS last_login_timezone,
      NULL AS msg_gateway_last_login_clientid,
      NULL AS lastloginclientid,
      NULL AS username,
      NULL AS register_email,
      NULL AS recent_contact_email,
      NULL AS ip_country_id,
      NULL AS coins_balance
    FROM
      cdp_email_detail_all
    WHERE
      potential = 1
  ) AS tbl2
  WHERE
    message_locale = 'en-us'
    AND last_login_timezone = '-12:00'
) AS s1;

-- 使用COALESCE简化CASE WHEN条件。