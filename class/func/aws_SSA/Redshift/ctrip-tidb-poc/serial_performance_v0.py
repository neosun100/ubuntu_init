import pandas as pd
import timeit
import numpy as np
import random
import pickle
from functools import wraps
from loguru import logger
import redshift_connector
import json

# 数据库配置信息
DB_CONFIG = {
    'host': 'ibu-test-query-redshift-128.479422325623.us-east-1.redshift-serverless.amazonaws.com',
    'database': 'dev',
    'port': 5439,
    'user': 'admin',
    'password': 'CTRIP2themoon100$'
}

# 查询变量列表
VARIABLES = [
    'message_locale',
    'recent_pageviewdate',
    'membertype',
    'edm_discounts_subscribe',
    'hasvisited_app',
    'push_discounts_subscribe',
]


def exception_handler(func):
    """
    异常处理装饰器，用于捕获和记录函数中的异常。

    :param func: 被装饰的函数
    :return: 装饰器包装后的函数
    """
    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except Exception as e:
            logger.error(f"An error occurred in {func.__name__}: {e}")
            raise  # 重新抛出异常以便外部捕获
    return wrapper


@exception_handler
def connect_to_db(config):
    """
    连接到数据库，并返回连接对象和游标。

    :param config: 数据库配置字典
    :return: (conn, cursor) 数据库连接和游标
    """
    conn = redshift_connector.connect(**config)
    cursor = conn.cursor()
    logger.info("Successfully connected to the database.")
    return conn, cursor


@exception_handler
def get_result_list(cursor, sql):
    """
    执行SQL查询，并返回结果列表。

    :param cursor: 数据库游标
    :param sql: SQL查询字符串
    :return: 结果列表
    """
    cursor.execute(sql)
    logger.info(f"Successfully executed SQL: {sql}")
    return [i[0] for i in cursor.fetchall()]


@exception_handler
def save_results_to_file(results, file_path='variables.json'):
    """
    将结果保存到JSON文件。

    :param results: 结果列表
    :param file_path: 文件路径字符串
    """
    with open(file_path, 'w') as file:
        json.dump(results, file)
        logger.info(f"Results saved to {file_path}.")


conn, cursor = connect_to_db(DB_CONFIG)


# 读取变量和变量权重
# 从JSON文件中读取数据
with open('variables_weight.json', 'r') as file:
    data = json.load(file)

# 将数据中的键值对作为变量存储
for item in data:
    for key, value in item.items():
        globals()[key] = value


# 读取sql模版列表
with open('/home/ec2-user/ctrip_ap_sql_templete.pkl', 'rb') as f:
    gen_sql_list = pickle.load(f)


def sql_generator(gen_sql):
    while True:
        yield gen_sql.format(message_locale=random.choices(message_locale, weights=message_locale_weight, k=1)[0],
                             message_locale_1=random.choices(
                                 message_locale, weights=message_locale_weight, k=1)[0],
                             recent_pageviewdate=random.choices(
                                 recent_pageviewdate, weights=recent_pageviewdate_weight, k=1)[0],
                             membertype=random.choices(
                                 membertype, weights=membertype_weight, k=1)[0],
                             membertype_1=random.choices(
                                 membertype, weights=membertype_weight, k=1)[0],
                             membertype_2=random.choices(
                                 membertype, weights=membertype_weight, k=1)[0],
                             membertype_3=random.choices(
                                 membertype, weights=membertype_weight, k=1)[0],
                             edm_discounts_subscribe=random.choices(
                                 edm_discounts_subscribe, weights=edm_discounts_subscribe_weight, k=1)[0],
                             hasvisited_app=random.choices(
                                 hasvisited_app, weights=hasvisited_app_weight, k=1)[0],
                             push_discounts_subscribe=random.choices(push_discounts_subscribe, weights=push_discounts_subscribe_weight, k=1)[0])


def time_sql_execution(cursor, sql_generator, times_to_run=3):
    """
    使用timeit模块多次运行SQL查询，并返回统计量。

    :param cursor: 数据库游标
    :param sql: SQL查询字符串
    :param times_to_run: 执行次数
    :return: 最大值，最小值，平均值，标准差
    """
    elapsed_times = []  # 存放每次执行的时间

    for i in range(times_to_run):
        sql = next(sql_generator)
        timer = timeit.Timer(lambda: get_result_list(cursor, sql))
        elapsed_time = timer.timeit(number=1)  # 每次只执行一次SQL查询
        elapsed_times.append(elapsed_time)
        logger.info(f"Run {i+1}: Elapsed time {elapsed_time} seconds")

    max_time = np.max(elapsed_times)
    min_time = np.min(elapsed_times)
    avg_time = np.mean(elapsed_times)
    std_dev = np.std(elapsed_times)

    logger.info(f"Max Time: {max_time} seconds")
    logger.info(f"Min Time: {min_time} seconds")
    logger.info(f"Avg Time: {avg_time} seconds")
    logger.info(f"Std Dev: {std_dev} seconds")

    return max_time, min_time, avg_time, std_dev


@exception_handler
def list_to_df_to_csv(serial_result):
    # 转换为DataFrame
    df = pd.DataFrame(serial_result, columns=['Max', 'Min', 'Mean', 'Std'])

    # 添加序号列
    df['Serial'] = ['ap-'+str(i+1) if i <
                    10 else 'selection-'+str(i-9) for i in range(20)]

    # 保存为csv文件，不包含索引
    df.to_csv('serial_result.csv', index=False)
    logger.info("DataFrame saved to csv file.")

    try:
        list_to_df_to_csv(serial_result)
    finally:
        logger.info("Finished processing.")


def main():
    serial_result = [time_sql_execution(
        cursor, sql_generator(gen_sql_list[i])) for i in range(20)]
    try:
        list_to_df_to_csv(serial_result)
    finally:
        logger.info("Finished processing.")


if __name__ == '__main__':
    main()
