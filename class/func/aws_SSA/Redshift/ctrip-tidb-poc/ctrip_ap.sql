SELECT COUNT(*)
FROM (SELECT /*+ read_from_storage(tiflash[ibucdptidb.t1]) */ DISTINCT t1.recent_whatsapp_telphone_number AS recent_whatsapp_telphone_number
           , t1.lastloginclientid                                                                         AS lastloginclientid
           , t1.message_locale                                                                            AS message_locale
           , t1.username                                                                                  AS username
           , t1.last_login_timezone                                                                       AS last_login_timezone
           , t1.uid                                                                                       AS uid
           , t1.ip_country_id                                                                             AS ip_country_id
           , t1.recent_whatsapp_telphone_cc                                                               AS recent_whatsapp_telphone_cc
           , t1.coins_balance                                                                             AS coins_balance
           , t1.hasvisited_app                                                                            AS hasvisited_app
           , t1.register_email                                                                            AS register_email
           , t1.recent_contact_email                                                                      AS recent_contact_email
           , t1.ibugrade                                                                                  AS ibugrade
           , t1.is_has_completed_order                                                                    AS is_has_completed_order
      FROM ibucdptidb.cdp_crm_uid_detail_basic t1
      WHERE t1.message_locale = 'en-sg'
        AND t1.upload_crm = '1') s1;