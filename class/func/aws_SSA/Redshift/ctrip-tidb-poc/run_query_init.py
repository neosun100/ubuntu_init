import os
import redshift_connector
import argparse

# 从环境变量中读取数据库配置
DB_CONFIG = {
    'host': os.environ.get('DB_HOST'),
    'database': os.environ.get('DB_NAME',"dev"),
    'port': int(os.environ.get('DB_PORT', 5439)),
    'user': os.environ.get('DB_USER',"admin"),
    'password': os.environ.get('DB_PASSWORD',"CTRIP2themoon100$")
}

def connect_to_db(config):
    conn = redshift_connector.connect(**config)
    cursor = conn.cursor()
    return conn, cursor

def main(args):
    DB_CONFIG['host'] = args.host
    try:
        conn, cursor = connect_to_db(DB_CONFIG)
        cursor.execute("SELECT 1;")
        print("Successfully executed SQL: SELECT 1;")
    finally:
        cursor.close()
        conn.close()
        print("Database connection and cursor closed.")

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Command-line tool.')
    parser.add_argument('--host', required=True, help='Database host')
    args = parser.parse_args()
    main(args)