from functools import wraps
from loguru import logger
import pickle

# 配置信息
CONFIG = {
    'pickle_file_path': 'ctrip_ap_sql_templete.pkl',
}


def exception_handler(func):
    """
    异常处理装饰器。

    :param func: 被装饰的函数
    :return: 包装后的函数
    """
    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except Exception as e:
            logger.error(f"Error in {func.__name__}: {e}")
            raise
    return wrapper


@exception_handler
def save_list_to_pickle(data_list, file_path):
    """
    将列表保存到文件。

    :param data_list: 要保存的数据列表
    :param file_path: 文件路径
    """
    with open(file_path, 'wb') as file:
        pickle.dump(data_list, file)
        logger.info(f"Saved list to {file_path}")


gen_sql_list = [
    """
    unload ('
SELECT s1.recent_whatsapp_telphone_number AS whatsAppMobilePhone
     , s1.lastloginclientid               AS clientId
     , s1.message_locale                  AS locale
     , s1.username                        AS userName
     , s1.last_login_timezone             AS last_login_timezone
     , s1.uid                             AS uid
     , s1.ip_country_id                   AS ip_country_id
     , s1.recent_whatsapp_telphone_cc     AS whatsAppMobilePhoneCC
     , s1.coins_balance                   AS coins_balance
     , s1.hasvisited_app                  AS hasvisited_app
     , CASE
           WHEN s1.register_email IS NOT NULL THEN s1.register_email
           ELSE s1.recent_contact_email
    END                                   AS email
     , s1.ibugrade                        AS ibugrade
     , s1.is_has_completed_order          AS is_has_completed_order
     , \\\'008a4901b6e9d0392a8c\\\'             AS uniqueKey
FROM (SELECT /*+ read_from_storage(tiflash[ibucdptidb.t1]) */ DISTINCT t1.recent_whatsapp_telphone_number AS recent_whatsapp_telphone_number
           , t1.lastloginclientid                                                                         AS lastloginclientid
           , t1.message_locale                                                                            AS message_locale
           , t1.username                                                                                  AS username
           , t1.last_login_timezone                                                                       AS last_login_timezone
           , t1.uid                                                                                       AS uid
           , t1.ip_country_id                                                                             AS ip_country_id
           , t1.recent_whatsapp_telphone_cc                                                               AS recent_whatsapp_telphone_cc
           , t1.coins_balance                                                                             AS coins_balance
           , t1.hasvisited_app                                                                            AS hasvisited_app
           , t1.register_email                                                                            AS register_email
           , t1.recent_contact_email                                                                      AS recent_contact_email
           , t1.ibugrade                                                                                  AS ibugrade
           , t1.is_has_completed_order                                                                    AS is_has_completed_order
      FROM ibu.ibucdptidb.cdp_crm_uid_detail_basic t1
      WHERE t1.message_locale = \\\'{message_locale}\\\'
        AND t1.upload_crm = \\\'1\\\') s1') to 's3://ctrip-redshift-unload-s3/tidb-test/poc/sql_selection_1'
    iam_role 'arn:aws:iam::479422325623:role/service-role/AmazonRedshift-CommandsAccessRole-20230609T111413'
    manifest
    allowoverwrite;
""",
    """
    unload ('
SELECT s1.recent_whatsapp_telphone_number AS whatsAppMobilePhone
     , s1.lastloginclientid               AS clientId
     , s1.message_locale                  AS locale
     , s1.username                        AS userName
     , s1.last_login_timezone             AS last_login_timezone
     , s1.uid                             AS uid
     , s1.ip_country_id                   AS ip_country_id
     , s1.recent_whatsapp_telphone_cc     AS whatsAppMobilePhoneCC
     , s1.coins_balance                   AS coins_balance
     , s1.hasvisited_app                  AS hasvisited_app
     , CASE
           WHEN s1.register_email IS NOT NULL THEN s1.register_email
           ELSE s1.recent_contact_email
    END                                   AS email
     , s1.ibugrade                        AS ibugrade
     , s1.is_has_completed_order          AS is_has_completed_order
     , \\\'951a473e8a583c6b2645\\\'             AS uniqueKey
FROM (SELECT /*+ read_from_storage(tiflash[ibucdptidb.t1]) */ DISTINCT t1.recent_whatsapp_telphone_number AS recent_whatsapp_telphone_number
           , t1.lastloginclientid                                                                         AS lastloginclientid
           , t1.message_locale                                                                            AS message_locale
           , t1.username                                                                                  AS username
           , t1.last_login_timezone                                                                       AS last_login_timezone
           , t1.uid                                                                                       AS uid
           , t1.ip_country_id                                                                             AS ip_country_id
           , t1.recent_whatsapp_telphone_cc                                                               AS recent_whatsapp_telphone_cc
           , t1.coins_balance                                                                             AS coins_balance
           , t1.hasvisited_app                                                                            AS hasvisited_app
           , t1.register_email                                                                            AS register_email
           , t1.recent_contact_email                                                                      AS recent_contact_email
           , t1.ibugrade                                                                                  AS ibugrade
           , t1.is_has_completed_order                                                                    AS is_has_completed_order
      FROM ibu.ibucdptidb.cdp_crm_uid_detail_basic t1
      WHERE t1.message_locale = \\\'{message_locale}\\\'
        AND t1.upload_crm = \\\'1\\\') s1') to 's3://ctrip-redshift-unload-s3/tidb-test/poc/sql_selection_2'
    iam_role 'arn:aws:iam::479422325623:role/service-role/AmazonRedshift-CommandsAccessRole-20230609T111413'
    manifest
    allowoverwrite;
""",
    """
    unload ('
SELECT s1.recent_whatsapp_telphone_number AS whatsAppMobilePhone
     , s1.lastloginclientid               AS clientId
     , s1.message_locale                  AS locale
     , s1.username                        AS userName
     , s1.last_login_timezone             AS last_login_timezone
     , s1.uid                             AS uid
     , s1.ip_country_id                   AS ip_country_id
     , s1.recent_whatsapp_telphone_cc     AS whatsAppMobilePhoneCC
     , s1.coins_balance                   AS coins_balance
     , s1.hasvisited_app                  AS hasvisited_app
     , CASE
           WHEN s1.register_email IS NOT NULL THEN s1.register_email
           ELSE s1.recent_contact_email
    END                                   AS email
     , s1.ibugrade                        AS ibugrade
     , s1.is_has_completed_order          AS is_has_completed_order
     , \\\'6775407da83baf870938\\\'             AS uniqueKey
FROM (SELECT /*+ read_from_storage(tiflash[ibucdptidb.t1]) */ DISTINCT t1.recent_whatsapp_telphone_number AS recent_whatsapp_telphone_number
           , t1.lastloginclientid                                                                         AS lastloginclientid
           , t1.message_locale                                                                            AS message_locale
           , t1.username                                                                                  AS username
           , t1.last_login_timezone                                                                       AS last_login_timezone
           , t1.uid                                                                                       AS uid
           , t1.ip_country_id                                                                             AS ip_country_id
           , t1.recent_whatsapp_telphone_cc                                                               AS recent_whatsapp_telphone_cc
           , t1.coins_balance                                                                             AS coins_balance
           , t1.hasvisited_app                                                                            AS hasvisited_app
           , t1.register_email                                                                            AS register_email
           , t1.recent_contact_email                                                                      AS recent_contact_email
           , t1.ibugrade                                                                                  AS ibugrade
           , t1.is_has_completed_order                                                                    AS is_has_completed_order
      FROM ibu.ibucdptidb.cdp_crm_uid_detail_basic t1
      WHERE (t1.message_locale = \\\'{message_locale}\\\'
          OR t1.message_locale = \\\'{message_locale_1}\\\')
        AND (t1.recent_pageviewdate <= \\\'{recent_pageviewdate}\\\'
          AND t1.recent_pageviewdate >= \\\'{recent_pageviewdate}\\\'
          AND ((t1.membertype = \\\'{membertype}\\\'
              OR t1.membertype = \\\'{membertype_1}\\\'
              OR t1.membertype = \\\'{membertype_2}\\\'
              OR t1.membertype = \\\'{membertype_3}\\\')
              AND t1.upload_crm = \\\'1\\\'))) s1') to 's3://ctrip-redshift-unload-s3/tidb-test/poc/sql_selection_3'
    iam_role 'arn:aws:iam::479422325623:role/service-role/AmazonRedshift-CommandsAccessRole-20230609T111413'
    manifest
    allowoverwrite;
""",
    """
    unload ('
SELECT s1.recent_whatsapp_telphone_number AS whatsAppMobilePhone
     , s1.lastloginclientid               AS clientId
     , s1.message_locale                  AS locale
     , s1.username                        AS userName
     , s1.last_login_timezone             AS last_login_timezone
     , s1.uid                             AS uid
     , s1.ip_country_id                   AS ip_country_id
     , s1.recent_whatsapp_telphone_cc     AS whatsAppMobilePhoneCC
     , s1.coins_balance                   AS coins_balance
     , s1.hasvisited_app                  AS hasvisited_app
     , CASE
           WHEN s1.register_email IS NOT NULL THEN s1.register_email
           ELSE s1.recent_contact_email
    END                                   AS email
     , s1.ibugrade                        AS ibugrade
     , s1.is_has_completed_order          AS is_has_completed_order
     , \\\'1c424dbfbac38dc65fa5\\\'             AS uniqueKey
FROM (SELECT /*+ read_from_storage(tiflash[ibucdptidb.t1]) */ DISTINCT t1.recent_whatsapp_telphone_number AS recent_whatsapp_telphone_number
           , t1.lastloginclientid                                                                         AS lastloginclientid
           , t1.message_locale                                                                            AS message_locale
           , t1.username                                                                                  AS username
           , t1.last_login_timezone                                                                       AS last_login_timezone
           , t1.uid                                                                                       AS uid
           , t1.ip_country_id                                                                             AS ip_country_id
           , t1.recent_whatsapp_telphone_cc                                                               AS recent_whatsapp_telphone_cc
           , t1.coins_balance                                                                             AS coins_balance
           , t1.hasvisited_app                                                                            AS hasvisited_app
           , t1.register_email                                                                            AS register_email
           , t1.recent_contact_email                                                                      AS recent_contact_email
           , t1.ibugrade                                                                                  AS ibugrade
           , t1.is_has_completed_order                                                                    AS is_has_completed_order
      FROM ibu.ibucdptidb.cdp_crm_uid_detail_basic t1
      WHERE t1.message_locale = \\\'{message_locale}\\\'
        AND (t1.edm_discounts_subscribe = \\\'{edm_discounts_subscribe}\\\'
          AND t1.upload_crm = \\\'1\\\')) s1') to 's3://ctrip-redshift-unload-s3/tidb-test/poc/sql_selection_4'
    iam_role 'arn:aws:iam::479422325623:role/service-role/AmazonRedshift-CommandsAccessRole-20230609T111413'
    manifest
    allowoverwrite;
""",
    """
    unload ('
SELECT s1.recent_whatsapp_telphone_number AS whatsAppMobilePhone
     , s1.lastloginclientid               AS clientId
     , s1.message_locale                  AS locale
     , s1.username                        AS userName
     , s1.last_login_timezone             AS last_login_timezone
     , s1.uid                             AS uid
     , s1.ip_country_id                   AS ip_country_id
     , s1.recent_whatsapp_telphone_cc     AS whatsAppMobilePhoneCC
     , s1.coins_balance                   AS coins_balance
     , s1.hasvisited_app                  AS hasvisited_app
     , CASE
           WHEN s1.register_email IS NOT NULL THEN s1.register_email
           ELSE s1.recent_contact_email
    END                                   AS email
     , s1.ibugrade                        AS ibugrade
     , s1.is_has_completed_order          AS is_has_completed_order
     , \\\'a97d44158d2cc69ff9e0\\\'             AS uniqueKey
FROM (SELECT /*+ read_from_storage(tiflash[ibucdptidb.t1]) */ DISTINCT t1.recent_whatsapp_telphone_number AS recent_whatsapp_telphone_number
           , t1.lastloginclientid                                                                         AS lastloginclientid
           , t1.message_locale                                                                            AS message_locale
           , t1.username                                                                                  AS username
           , t1.last_login_timezone                                                                       AS last_login_timezone
           , t1.uid                                                                                       AS uid
           , t1.ip_country_id                                                                             AS ip_country_id
           , t1.recent_whatsapp_telphone_cc                                                               AS recent_whatsapp_telphone_cc
           , t1.coins_balance                                                                             AS coins_balance
           , t1.hasvisited_app                                                                            AS hasvisited_app
           , t1.register_email                                                                            AS register_email
           , t1.recent_contact_email                                                                      AS recent_contact_email
           , t1.ibugrade                                                                                  AS ibugrade
           , t1.is_has_completed_order                                                                    AS is_has_completed_order
      FROM ibu.ibucdptidb.cdp_crm_uid_detail_basic t1
      WHERE t1.message_locale = \\\'{message_locale}\\\'
        AND t1.upload_crm = \\\'1\\\') s1') to 's3://ctrip-redshift-unload-s3/tidb-test/tidb-test/poc/sql_selection_5'
    iam_role 'arn:aws:iam::479422325623:role/service-role/AmazonRedshift-CommandsAccessRole-20230609T111413'
    manifest
    allowoverwrite;
""",
    """
    unload ('
SELECT s1.recent_whatsapp_telphone_number AS whatsAppMobilePhone
     , s1.lastloginclientid               AS clientId
     , s1.message_locale                  AS locale
     , s1.username                        AS userName
     , s1.last_login_timezone             AS last_login_timezone
     , s1.uid                             AS uid
     , s1.ip_country_id                   AS ip_country_id
     , s1.recent_whatsapp_telphone_cc     AS whatsAppMobilePhoneCC
     , s1.coins_balance                   AS coins_balance
     , s1.hasvisited_app                  AS hasvisited_app
     , CASE
           WHEN s1.register_email IS NOT NULL THEN s1.register_email
           ELSE s1.recent_contact_email
    END                                   AS email
     , s1.ibugrade                        AS ibugrade
     , s1.is_has_completed_order          AS is_has_completed_order
     , \\\'82e0457da99cb1d7dc06\\\'             AS uniqueKey
FROM (SELECT /*+ read_from_storage(tiflash[ibucdptidb.t1]) */ DISTINCT t1.recent_whatsapp_telphone_number AS recent_whatsapp_telphone_number
           , t1.lastloginclientid                                                                         AS lastloginclientid
           , t1.message_locale                                                                            AS message_locale
           , t1.username                                                                                  AS username
           , t1.last_login_timezone                                                                       AS last_login_timezone
           , t1.uid                                                                                       AS uid
           , t1.ip_country_id                                                                             AS ip_country_id
           , t1.recent_whatsapp_telphone_cc                                                               AS recent_whatsapp_telphone_cc
           , t1.coins_balance                                                                             AS coins_balance
           , t1.hasvisited_app                                                                            AS hasvisited_app
           , t1.register_email                                                                            AS register_email
           , t1.recent_contact_email                                                                      AS recent_contact_email
           , t1.ibugrade                                                                                  AS ibugrade
           , t1.is_has_completed_order                                                                    AS is_has_completed_order
      FROM ibu.ibucdptidb.cdp_crm_uid_detail_basic t1
      WHERE t1.message_locale = \\\'{message_locale}\\\'
        AND (t1.hasvisited_app = \\\'{hasvisited_app}\\\'
          AND t1.upload_crm = \\\'1\\\')) s1') to 's3://ctrip-redshift-unload-s3/tidb-test/poc/sql_selection_6'
    iam_role 'arn:aws:iam::479422325623:role/service-role/AmazonRedshift-CommandsAccessRole-20230609T111413'
    manifest
    allowoverwrite;
""",
    """
    unload ('
SELECT s1.recent_whatsapp_telphone_number AS whatsAppMobilePhone
     , s1.lastloginclientid               AS clientId
     , s1.message_locale                  AS locale
     , s1.username                        AS userName
     , s1.last_login_timezone             AS last_login_timezone
     , s1.uid                             AS uid
     , s1.ip_country_id                   AS ip_country_id
     , s1.recent_whatsapp_telphone_cc     AS whatsAppMobilePhoneCC
     , s1.coins_balance                   AS coins_balance
     , s1.hasvisited_app                  AS hasvisited_app
     , CASE
           WHEN s1.register_email IS NOT NULL THEN s1.register_email
           ELSE s1.recent_contact_email
    END                                   AS email
     , s1.ibugrade                        AS ibugrade
     , s1.is_has_completed_order          AS is_has_completed_order
     , \\\'bb0d4a13b8ee93d4fc7d\\\'             AS uniqueKey
FROM (SELECT /*+ read_from_storage(tiflash[ibucdptidb.t1]) */ DISTINCT t1.recent_whatsapp_telphone_number AS recent_whatsapp_telphone_number
           , t1.lastloginclientid                                                                         AS lastloginclientid
           , t1.message_locale                                                                            AS message_locale
           , t1.username                                                                                  AS username
           , t1.last_login_timezone                                                                       AS last_login_timezone
           , t1.uid                                                                                       AS uid
           , t1.ip_country_id                                                                             AS ip_country_id
           , t1.recent_whatsapp_telphone_cc                                                               AS recent_whatsapp_telphone_cc
           , t1.coins_balance                                                                             AS coins_balance
           , t1.hasvisited_app                                                                            AS hasvisited_app
           , t1.register_email                                                                            AS register_email
           , t1.recent_contact_email                                                                      AS recent_contact_email
           , t1.ibugrade                                                                                  AS ibugrade
           , t1.is_has_completed_order                                                                    AS is_has_completed_order
      FROM ibu.ibucdptidb.cdp_crm_uid_detail_basic t1
      WHERE t1.message_locale = \\\'{message_locale}\\\'
        AND t1.upload_crm = \\\'1\\\') s1') to 's3://ctrip-redshift-unload-s3/tidb-test/poc/sql_selection_7'
    iam_role 'arn:aws:iam::479422325623:role/service-role/AmazonRedshift-CommandsAccessRole-20230609T111413'
    manifest
    allowoverwrite;
""",
    """
    unload ('
SELECT s1.recent_whatsapp_telphone_number AS whatsAppMobilePhone
     , s1.lastloginclientid               AS clientId
     , s1.message_locale                  AS locale
     , s1.username                        AS userName
     , s1.last_login_timezone             AS last_login_timezone
     , s1.uid                             AS uid
     , s1.ip_country_id                   AS ip_country_id
     , s1.recent_whatsapp_telphone_cc     AS whatsAppMobilePhoneCC
     , s1.coins_balance                   AS coins_balance
     , s1.hasvisited_app                  AS hasvisited_app
     , CASE
           WHEN s1.register_email IS NOT NULL THEN s1.register_email
           ELSE s1.recent_contact_email
    END                                   AS email
     , s1.ibugrade                        AS ibugrade
     , s1.is_has_completed_order          AS is_has_completed_order
     , \\\'3047449cbf39637faa2a\\\'             AS uniqueKey
FROM (SELECT /*+ read_from_storage(tiflash[ibucdptidb.t1]) */ DISTINCT t1.recent_whatsapp_telphone_number AS recent_whatsapp_telphone_number
           , t1.lastloginclientid                                                                         AS lastloginclientid
           , t1.message_locale                                                                            AS message_locale
           , t1.username                                                                                  AS username
           , t1.last_login_timezone                                                                       AS last_login_timezone
           , t1.uid                                                                                       AS uid
           , t1.ip_country_id                                                                             AS ip_country_id
           , t1.recent_whatsapp_telphone_cc                                                               AS recent_whatsapp_telphone_cc
           , t1.coins_balance                                                                             AS coins_balance
           , t1.hasvisited_app                                                                            AS hasvisited_app
           , t1.register_email                                                                            AS register_email
           , t1.recent_contact_email                                                                      AS recent_contact_email
           , t1.ibugrade                                                                                  AS ibugrade
           , t1.is_has_completed_order                                                                    AS is_has_completed_order
      FROM ibu.ibucdptidb.cdp_crm_uid_detail_basic t1
      WHERE t1.message_locale = \\\'{message_locale}\\\'
        AND (t1.hasvisited_app = \\\'{hasvisited_app}\\\'
          AND t1.upload_crm = \\\'1\\\')) s1') to 's3://ctrip-redshift-unload-s3/tidb-test/poc/sql_selection_8'
    iam_role 'arn:aws:iam::479422325623:role/service-role/AmazonRedshift-CommandsAccessRole-20230609T111413'
    manifest
    allowoverwrite;
""",
    """
    unload ('
SELECT s1.recent_whatsapp_telphone_number AS whatsAppMobilePhone
     , s1.lastloginclientid               AS clientId
     , s1.message_locale                  AS locale
     , s1.username                        AS userName
     , s1.last_login_timezone             AS last_login_timezone
     , s1.uid                             AS uid
     , s1.ip_country_id                   AS ip_country_id
     , s1.recent_whatsapp_telphone_cc     AS whatsAppMobilePhoneCC
     , s1.coins_balance                   AS coins_balance
     , s1.hasvisited_app                  AS hasvisited_app
     , CASE
           WHEN s1.register_email IS NOT NULL THEN s1.register_email
           ELSE s1.recent_contact_email
    END                                   AS email
     , s1.ibugrade                        AS ibugrade
     , s1.is_has_completed_order          AS is_has_completed_order
     , \\\'b6ae46d0abfc1adaf1ad\\\'             AS uniqueKey
FROM (SELECT /*+ read_from_storage(tiflash[ibucdptidb.t1]) */ DISTINCT t1.recent_whatsapp_telphone_number AS recent_whatsapp_telphone_number
           , t1.lastloginclientid                                                                         AS lastloginclientid
           , t1.message_locale                                                                            AS message_locale
           , t1.username                                                                                  AS username
           , t1.last_login_timezone                                                                       AS last_login_timezone
           , t1.uid                                                                                       AS uid
           , t1.ip_country_id                                                                             AS ip_country_id
           , t1.recent_whatsapp_telphone_cc                                                               AS recent_whatsapp_telphone_cc
           , t1.coins_balance                                                                             AS coins_balance
           , t1.hasvisited_app                                                                            AS hasvisited_app
           , t1.register_email                                                                            AS register_email
           , t1.recent_contact_email                                                                      AS recent_contact_email
           , t1.ibugrade                                                                                  AS ibugrade
           , t1.is_has_completed_order                                                                    AS is_has_completed_order
      FROM ibu.ibucdptidb.cdp_crm_uid_detail_basic t1
      WHERE t1.message_locale = \\\'{message_locale}\\\'
        AND t1.upload_crm = \\\'1\\\') s1') to 's3://ctrip-redshift-unload-s3/tidb-test/poc/sql_selection_9'
    iam_role 'arn:aws:iam::479422325623:role/service-role/AmazonRedshift-CommandsAccessRole-20230609T111413'
    manifest
    allowoverwrite;
""",
    """
    unload ('
SELECT s1.recent_whatsapp_telphone_number AS whatsAppMobilePhone
     , s1.lastloginclientid               AS clientId
     , s1.message_locale                  AS locale
     , s1.username                        AS userName
     , s1.last_login_timezone             AS last_login_timezone
     , s1.uid                             AS uid
     , s1.ip_country_id                   AS ip_country_id
     , s1.recent_whatsapp_telphone_cc     AS whatsAppMobilePhoneCC
     , s1.coins_balance                   AS coins_balance
     , s1.hasvisited_app                  AS hasvisited_app
     , CASE
           WHEN s1.register_email IS NOT NULL THEN s1.register_email
           ELSE s1.recent_contact_email
    END                                   AS email
     , s1.ibugrade                        AS ibugrade
     , s1.is_has_completed_order          AS is_has_completed_order
     , \\\'0d224c12a720b52e2c9e\\\'             AS uniqueKey
FROM (SELECT /*+ read_from_storage(tiflash[ibucdptidb.t1]) */ DISTINCT t1.recent_whatsapp_telphone_number AS recent_whatsapp_telphone_number
           , t1.lastloginclientid                                                                         AS lastloginclientid
           , t1.message_locale                                                                            AS message_locale
           , t1.username                                                                                  AS username
           , t1.last_login_timezone                                                                       AS last_login_timezone
           , t1.uid                                                                                       AS uid
           , t1.ip_country_id                                                                             AS ip_country_id
           , t1.recent_whatsapp_telphone_cc                                                               AS recent_whatsapp_telphone_cc
           , t1.coins_balance                                                                             AS coins_balance
           , t1.hasvisited_app                                                                            AS hasvisited_app
           , t1.register_email                                                                            AS register_email
           , t1.recent_contact_email                                                                      AS recent_contact_email
           , t1.ibugrade                                                                                  AS ibugrade
           , t1.is_has_completed_order                                                                    AS is_has_completed_order
      FROM ibu.ibucdptidb.cdp_crm_uid_detail_basic t1
      WHERE t1.message_locale = \\\'{message_locale}\\\'
        AND (t1.hasvisited_app = \\\'{hasvisited_app}\\\'
          AND (t1.push_discounts_subscribe = \\\'{push_discounts_subscribe}\\\'
              AND t1.upload_crm = \\\'1\\\'))) s1') to 's3://ctrip-redshift-unload-s3/tidb-test/poc/sql_selection_10'
    iam_role 'arn:aws:iam::479422325623:role/service-role/AmazonRedshift-CommandsAccessRole-20230609T111413'
    manifest
    allowoverwrite;
""",
]


def main():
    try:
        logger.info("Started processing.")
        save_list_to_pickle(gen_sql_list, CONFIG['pickle_file_path'])
    finally:
        logger.info("Finished processing.")


if __name__ == '__main__':
    main()