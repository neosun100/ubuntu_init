import os
import json
import timeit
import random
import pickle
import argparse
import numpy as np
import pandas as pd
from functools import wraps
from loguru import logger
import redshift_connector

# 从环境变量中读取数据库配置
DB_CONFIG = {
    'host': os.environ.get('DB_HOST'),
    'database': os.environ.get('DB_NAME'),
    'port': int(os.environ.get('DB_PORT', 5439)),
    'user': os.environ.get('DB_USER'),
    'password': os.environ.get('DB_PASSWORD')
}


VARIABLES = [
    'message_locale',
    'recent_pageviewdate',
    'membertype',
    'edm_discounts_subscribe',
    'hasvisited_app',
    'push_discounts_subscribe',
]


def exception_handler(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except Exception as e:
            logger.error(f"An error occurred in {func.__name__}: {e}")
            raise
    return wrapper


@exception_handler
def connect_to_db(config):
    conn = redshift_connector.connect(**config)
    cursor = conn.cursor()
    logger.info("Successfully connected to the database.")
    return conn, cursor


def random_choice_with_weight(variables_and_weights, key):
    return random.choices(
        variables_and_weights[key],
        weights=variables_and_weights[f"{key}_weight"],
        k=1
    )[0]


@exception_handler
def get_result_list(cursor, sql):
    cursor.execute(sql)
    logger.info(f"Successfully executed SQL: {sql}")
    return [i[0] for i in cursor.fetchall()]


@exception_handler
def save_results_to_file(results, file_path='variables.json'):
    with open(file_path, 'w') as file:
        json.dump(results, file)
        logger.info(f"Results saved to {file_path}.")


def load_json_file(file_path):
    with open(file_path, 'r') as file:
        return json.load(file)


def load_pickle_file(file_path):
    with open(file_path, 'rb') as f:
        return pickle.load(f)


def sql_generator(gen_sql, variables_and_weights):
    while True:
        formatted_args = {
            key: random_choice_with_weight(variables_and_weights, key) for key in VARIABLES
        }

        for i in range(1, 4):
            for var in ['message_locale', 'membertype']:
                formatted_args[f"{var}_{i}"] = random_choice_with_weight(
                    variables_and_weights, var)

        yield gen_sql.format(**formatted_args)


def time_sql_execution(cursor, sql_generator, times_to_run=3):
    elapsed_times = []
    for i in range(times_to_run):
        sql = next(sql_generator)
        timer = timeit.Timer(lambda: get_result_list(cursor, sql))
        elapsed_time = timer.timeit(number=1)
        elapsed_times.append(elapsed_time)
        logger.info(f"Run {i+1}: Elapsed time {elapsed_time} seconds")

    return np.max(elapsed_times), np.min(elapsed_times), np.mean(elapsed_times), np.std(elapsed_times)


@exception_handler
def save_df_to_csv(df, file_path='/home/ec2-user/serial_result.csv'):
    df.to_csv(file_path, index=False)
    logger.info("DataFrame saved to csv file.")


def main(args):
    DB_CONFIG['host'] = args.host
    random.seed(args.seed)

    try:
        conn, cursor = connect_to_db(DB_CONFIG)
        variables_and_weights = load_json_file('variables_weight.json')
        gen_sql_list = load_pickle_file(
            '/home/ec2-user/ctrip_ap_sql_templete.pkl')

        serial_result = [
            time_sql_execution(cursor, sql_generator(
                gen_sql_list[i], variables_and_weights), args.times_to_run)
            for i in range(len(gen_sql_list))
        ]

        df = pd.DataFrame(serial_result, columns=['Max', 'Min', 'Mean', 'Std'])
        df['Serial'] = ['ap-'+str(i+1) if i < 10 else 'selection-'+str(i-9)
                        for i in range(len(gen_sql_list))]

        save_df_to_csv(df, args.csv_file)
    finally:
        cursor.close()
        conn.close()
        logger.info("Database connection and cursor closed.")


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Command-line tool.')
    parser.add_argument('--host', required=True, help='Database host')
    parser.add_argument('--seed', type=int, default=1, help='Random seed')
    parser.add_argument('--times_to_run', type=int, default=1,
                        help='How many times to run the SQL')
    parser.add_argument('--csv_file', default='serial_result.csv',
                        help='CSV file to save results')

    args = parser.parse_args()
    main(args)
