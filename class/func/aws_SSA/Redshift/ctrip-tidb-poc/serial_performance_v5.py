import pandas as pd
import timeit
import numpy as np
import random
import pickle
import json
from functools import wraps
from loguru import logger
import redshift_connector
import argparse

# 数据库配置信息
DB_CONFIG = {
    'host': 'ibu-test-query-redshift-128.479422325623.us-east-1.redshift-serverless.amazonaws.com',
    'database': 'dev',
    'port': 5439,
    'user': 'admin',
    'password': 'CTRIP2themoon100$'
}

# 查询变量列表
VARIABLES = [
    'message_locale',
    'recent_pageviewdate',
    'membertype',
    'edm_discounts_subscribe',
    'hasvisited_app',
    'push_discounts_subscribe',
]


def exception_handler(func):
    """异常处理装饰器"""
    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except Exception as e:
            logger.error(f"An error occurred in {func.__name__}: {e}")
            raise
    return wrapper


@exception_handler
def connect_to_db(config):
    """连接到数据库并返回连接和游标"""
    conn = redshift_connector.connect(**config)
    cursor = conn.cursor()
    logger.info("Successfully connected to the database.")
    return conn, cursor


@exception_handler
def get_result_list(cursor, sql):
    """执行SQL查询并返回结果列表"""
    cursor.execute(sql)
    logger.info(f"Successfully executed SQL: {sql}")
    return [i[0] for i in cursor.fetchall()]


@exception_handler
def save_results_to_file(results, file_path='variables.json'):
    """将结果保存到文件"""
    with open(file_path, 'w') as file:
        json.dump(results, file)
        logger.info(f"Results saved to {file_path}.")


def load_variables_and_weights():
    """加载变量和权重信息"""
    with open('variables_weight.json', 'r') as file:
        data = json.load(file)

    variables_and_weights = {}
    for item in data:
        for key, value in item.items():
            variables_and_weights[key] = value

    return variables_and_weights


def load_sql_template():
    """加载SQL模板"""
    with open('/home/ec2-user/ctrip_ap_sql_templete.pkl', 'rb') as f:
        return pickle.load(f)


def sql_generator(gen_sql, variables_and_weights):
    """SQL生成器"""
    while True:
        formatted_args = {key: random.choices(value, weights=variables_and_weights[f"{key}_weight"], k=1)[
            0] for key, value in variables_and_weights.items() if key in VARIABLES}
        for i in range(1, 4):
            formatted_args[f"message_locale_{i}"] = random.choices(
                variables_and_weights["message_locale"], weights=variables_and_weights["message_locale_weight"], k=1)[0]
            formatted_args[f"membertype_{i}"] = random.choices(
                variables_and_weights["membertype"], weights=variables_and_weights["membertype_weight"], k=1)[0]
        yield gen_sql.format(**formatted_args)


def time_sql_execution(cursor, sql_generator, times_to_run=3):
    """计时SQL执行性能"""
    elapsed_times = []

    for i in range(times_to_run):
        sql = next(sql_generator)
        timer = timeit.Timer(lambda: get_result_list(cursor, sql))
        elapsed_time = timer.timeit(number=1)
        elapsed_times.append(elapsed_time)
        logger.info(f"Run {i+1}: Elapsed time {elapsed_time} seconds")

    max_time = np.max(elapsed_times)
    min_time = np.min(elapsed_times)
    avg_time = np.mean(elapsed_times)
    std_dev = np.std(elapsed_times)

    logger.info(f"Max Time: {max_time} seconds")
    logger.info(f"Min Time: {min_time} seconds")
    logger.info(f"Avg Time: {avg_time} seconds")
    logger.info(f"Std Dev: {std_dev} seconds")

    return max_time, min_time, avg_time, std_dev


@exception_handler
def save_df_to_csv(df, file_path='/home/ec2-user/serial_result.csv'):
    """保存DataFrame到CSV文件"""
    df.to_csv(file_path, index=False)
    logger.info("DataFrame saved to csv file.")


def main(args):
    """主函数"""
    host = args.host
    seed = args.seed
    times_to_run = args.times_to_run
    csv_file = args.csv_file

    DB_CONFIG['host'] = host  # 更新数据库配置

    random.seed(seed)  # 设置随机种子
    conn, cursor = connect_to_db(DB_CONFIG)
    variables_and_weights = load_variables_and_weights()
    gen_sql_list = load_sql_template()

    serial_result = [time_sql_execution(
        cursor, sql_generator(gen_sql_list[i], variables_and_weights), times_to_run) for i in range(len(gen_sql_list))]

    df = pd.DataFrame(serial_result, columns=['Max', 'Min', 'Mean', 'Std'])
    df['Serial'] = ['ap-'+str(i+1) if i <
                    10 else 'selection-'+str(i-9) for i in range(20)]

    save_df_to_csv(df, csv_file)  # 保存到指定的CSV文件
    


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Command-line tool.')
    parser.add_argument('--host', required=True,
                        default='ibu-test-query-redshift-128.479422325623.us-east-1.redshift-serverless.amazonaws.com', help='Database host')
    parser.add_argument('--seed', type=int, default=1, help='Random seed')
    parser.add_argument('--times_to_run', type=int, default=1,
                        help='How many times to run the SQL')
    parser.add_argument('--csv_file', default='serial_result.csv',
                        help='CSV file to save results')

    args = parser.parse_args()
    main(args)
