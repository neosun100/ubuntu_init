# 导入需要的库
import json
import redshift_connector
from loguru import logger

# 定义数据库连接参数
DB_PARAMS = {
    'host': 'ibu-test-query-redshift-128.479422325623.us-east-1.redshift-serverless.amazonaws.com',
    'database': 'dev',
    'port': 5439,
    'user': 'admin',
    'password': 'CTRIP2themoon100$'
}

# 定义需要查询的变量列表
VARIABLES = [
    'message_locale',
    'recent_pageviewdate',
    'membertype',
    'edm_discounts_subscribe',
    'hasvisited_app',
    'push_discounts_subscribe',
    'recent_pageviewdate'
]

# 连接数据库并获取cursor


def connect_db(params):
    try:
        conn = redshift_connector.connect(**params)
        return conn.cursor()
    except Exception as e:
        logger.error(f"Failed to connect to database: {e}")
        return None

# 执行SQL查询并获取结果


def get_result_list(cursor, sql):
    try:
        cursor.execute(sql)
        return [i[0] for i in cursor.fetchall()]
    except Exception as e:
        logger.error(f"Failed to execute query: {e}")
        return []

# 主函数


def main():
    cursor = connect_db(DB_PARAMS)
    if cursor is None:
        return

    sql_queries = [
        f"SELECT DISTINCT {variable} FROM ibu.ibucdptidb.cdp_crm_uid_detail_basic WHERE {variable} IS NOT NULL LIMIT 100;" for variable in VARIABLES]
    results = [{v: get_result_list(cursor, sql)}
               for v, sql in zip(VARIABLES, sql_queries)]

    # 将结果保存为JSON文件
    with open('variables.json', 'w') as file:
        json.dump(results, file)


if __name__ == "__main__":
    main()
