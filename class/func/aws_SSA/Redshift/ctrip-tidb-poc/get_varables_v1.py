import json
import redshift_connector
from loguru import logger

# 定义配置参数
DB_CONFIG = {
    'host': 'ibu-test-query-redshift-128.479422325623.us-east-1.redshift-serverless.amazonaws.com',
    'database': 'dev',
    'port': 5439,
    'user': 'admin',
    'password': 'CTRIP2themoon100$'
}

VARIABLES = [
    'message_locale',
    'recent_pageviewdate',
    'membertype',
    'edm_discounts_subscribe',
    'hasvisited_app',
    'push_discounts_subscribe',
    'recent_pageviewdate'
]


def connect_to_db(config):
    """连接到数据库，并返回连接和游标"""
    try:
        conn = redshift_connector.connect(**config)
        cursor = conn.cursor()
        logger.info("Successfully connected to the database.")
        return conn, cursor
    except Exception as e:
        logger.error(f"Failed to connect to the database: {e}")
        raise


def get_result_list(cursor, sql):
    """执行SQL查询，并返回结果列表"""
    try:
        cursor.execute(sql)
        logger.info(f"Successfully executed SQL: {sql}")
        return [i[0] for i in cursor.fetchall()]
    except Exception as e:
        logger.error(f"Failed to execute SQL: {e}")
        raise


def save_results_to_file(results, file_path='variables.json'):
    """将结果保存到JSON文件"""
    try:
        with open(file_path, 'w') as file:
            json.dump(results, file)
            logger.info(f"Results saved to {file_path}.")
    except Exception as e:
        logger.error(f"Failed to save results: {e}")
        raise


def main():
    # 连接到数据库
    conn, cursor = connect_to_db(DB_CONFIG)
    try:
        # 构造SQL查询
        sql_queries = [
            f"SELECT DISTINCT {variable} FROM ibu.ibucdptidb.cdp_crm_uid_detail_basic WHERE {variable} IS NOT NULL LIMIT 100;" for variable in VARIABLES]
        # 获取并保存结果
        results = [{v: get_result_list(cursor, sql)}
                   for v, sql in zip(VARIABLES, sql_queries)]
        save_results_to_file(results)
    finally:
        # 关闭数据库连接和游标
        cursor.close()
        conn.close()
        logger.info("Database connection and cursor closed.")


if __name__ == '__main__':
    main()
