#!/bin/bash

start_total=$(date +%s)

# 定义主机列表
hosts=("128" "256" "512")

# 循环处理每个主机
for host in "${hosts[@]}"; do
    echo "$(TZ='Asia/Shanghai' date) - Starting process for host ${host}" # 记录开始处理主机的日志，包含当前时间
    start=$(date +%s)
    # 执行python命令，如果出错则打印错误信息并退出脚本
    if ! python run_query_init.py --host "ibu-test-query-redshift-${host}.479422325623.us-east-1.redshift-serverless.amazonaws.com"; then
        echo "$(TZ='Asia/Shanghai' date) - Error occurred while processing host ${host}" 1>&2 # 打印错误信息到标准错误输出，包含当前时间
        exit 1                                                                                # 退出脚本，返回非零状态码表示错误
    fi
    end=$(date +%s)
    echo "$(TZ='Asia/Shanghai' date) - Processing host ${host} took $((end - start)) seconds"
    echo "$(TZ='Asia/Shanghai' date) - Finished process for host ${host}" # 记录完成处理主机的日志，包含当前时间
done

end_total=$(date +%s)
echo "$(TZ='Asia/Shanghai' date) - All processes finished successfully in $((end_total - start_total)) seconds" # 记录所有处理都成功完成的日志，包含当前时间
