-- demo2
-- Create 3 base table
-- table 1 dim_user_attribution
drop table if exists dim_user_attribution;
create table dim_user_attribution
(
    country          character varying(20),
    iso_code         character varying(4),
    user_attribution character varying(4)
)DISTSTYLE ALL;


insert into dim_user_attribution
values ('"缅甸"', 'MM', 'SG'),
        ('"泰国"', 'TH', 'SG'),
        ('"柬埔寨"', 'KH', 'SG'),
        ('"老挝"', 'LA', 'SG'),
        ('"越南"', 'VN', 'SG'),
        ('"菲律宾"', 'PH', 'SG'),
        ('"马来西亚"', 'MY', 'SG'),
        ('"新加坡"', 'SG', 'SG'),
        ('"文莱"', 'BN', 'SG'),
        ('"印度尼西亚"', 'ID', 'SG'),
        ('"澳大利亚"', 'AU', 'AU');
        


-- table 2 user_web_account_auth
drop table if exists user_web_account_auth;
create table user_web_account_auth
(
    id         character varying(32),
    reg_region character varying(4)
)DISTKEY (id);

COPY user_web_account_auth FROM 's3://aws-virginia-kinesis-filehose-finebi-data/orc_demo/000000_0_demo2_user_web_account_auth'
    IAM_ROLE 'arn:aws:iam::835751346093:role/aws-redshift-Federation=role'
    FORMAT ORC ;



-- table 3 bi_bi_sensors_super
drop table if exists bi_bi_sensors;
CREATE TABLE bi_bi_sensors
(
    uuid             character varying(36),
    id               character varying(32),
    time             bigint,
    type             character varying(20),
    project          character varying(2048),
    properties       character varying(20480),
    sensors_id       character varying(70),
    user_attribution character varying(4),
    event            character varying(24),
    pt_date          character varying(10)
);

drop table if exists bi_bi_sensors_super;
CREATE TABLE bi_bi_sensors_super
(
    uuid             character varying(36),
    id               character varying(32),
    time             bigint,
    type             character varying(20),
    project          character varying(40),
    properties       character varying(10240),
    sensors_id       character varying(70),
    user_attribution character varying(4),
    event            character varying(24),
    pt_date          character varying(10),
    json_data        super
)DISTKEY (id)
 sortkey (pt_date);

COPY bi_bi_sensors
    FROM 's3://aws-virginia-kinesis-filehose-finebi-data/orc_demo/000000_0'
    IAM_ROLE 'arn:aws:iam::835751346093:role/aws-redshift-Federation=role'
    FORMAT ORC;

-- COPY bi_bi_sensors (uuid, id, time, type, project, properties, sensors_id, user_attribution, event, pt_date)
--     FROM 's3://aws-virginia-kinesis-filehose-finebi-data/orc_demo/000000_0'
--     IAM_ROLE 'arn:aws:iam::835751346093:role/aws-redshift-Federation=role'
--     FORMAT ORC;



insert into bi_bi_sensors_super
select *,
       json_parse(properties) as json_data
from bi_bi_sensors;

-- select json_data.app_language from bi_bi_sensors_super limit 1;




-- sink table
drop table if exists dwd_public_public_moomoo_bi_sensors;
CREATE TABLE dwd_public_public_moomoo_bi_sensors
(
    uuid             character varying(36),
    id               character varying(32),
    time             bigint,
    encry_time       character varying(10),
    type             character varying(20),
    project          character varying(2048),
    properties       character varying(20480),
    sensors_id       character varying(70),
    user_attribution super,
    event            character varying(24),
    pt_date          character varying(20),
    is_login_id      character varying(100),
    utm_medium       character varying(70),
    short_url_key    character varying(70),
    browser_version  character varying(70),
    os_version       character varying(70)
)DISTKEY (id)
 sortkey (pt_date);


