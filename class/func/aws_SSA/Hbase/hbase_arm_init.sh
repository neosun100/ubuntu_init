cd ~

sudo yum install htop -y
# 关闭 thrift 服务
sudo systemctl stop hbase-thrift

# 修改 hBase-thrift 服务的启动脚本
sudo sed -i 's/thrift.pid/thrift2.pid/g' /etc/systemd/system/hbase-thrift.service
sudo sed -i "s/ thrift/ thrift2/g" /etc/systemd/system/hbase-thrift.service 

# 刷新 systemd 服务配置
sudo systemctl daemon-reload

# 重新启动 hbase-thrift 服务，此时启动的是 thrift2 服务
sudo systemctl start hbase-thrift

# 检查是否启动成功
ps aux | grep thrift

# 2. 检查
jps
netstat -ntlp|grep 9090
sudo lsof -i:9090

# 3. fix 
sudo chmod -R 777 /var/log

pip install thrift -U
pip install hbase-thrift -U

wget http://file.neo.pub/hbase/ttypes.py -O ~/.local/lib/python3.7/site-packages/hbase/ttypes.py
wget http://file.neo.pub/hbase/THBaseService.py -O ~/.local/lib/python3.7/site-packages/hbase/THBaseService.py


# 增加nodejs源
curl --silent --location https://rpm.nodesource.com/setup_lts.x | sudo bash -
# 安装nodejs
sudo yum install -y nodejs
#  检查nodejs版本
node --version
sudo npm install -g gnomon



sudo amazon-linux-extras install epel -y
sudo yum install parallel -y
echo will cite | parallel --citation