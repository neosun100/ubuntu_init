# -*- coding: utf-8 -*-

import json
import traceback
import sys
import datetime
import time
import logging
import threading
import random
import logging.handlers

from thrift.transport import TSocket
from thrift.protocol import TBinaryProtocol
from thrift.transport import TTransport
from hbase.ttypes import *
from hbase import THBaseService

gWritenRecord = 0
gStartTime = 0
gEndTime = 0

# config
concurrent = 20
records = 10000  # 6000000 #6 million

mylock = threading.RLock()


class writeThread(threading.Thread):
    def __init__(self, threadId, recordsPerThread):
        threading.Thread.__init__(self, name=threadId)
        self.recordsPerThread = recordsPerThread

        self.transport = TTransport.TBufferedTransport(
            TSocket.TSocket('127.0.0.1', 9090))
        protocol = TBinaryProtocol.TBinaryProtocolAccelerated(self.transport)
        self.client = THBaseService.Client(protocol)
        self.transport.open()

    def run(self):
        # print("********** %s start at := " % (self.getName()))
        global gEndTime
        global gWritenRecord

        self.write_hbase()

        mylock.acquire()
        gEndTime = time.time()
        gWritenRecord += self.recordsPerThread
        # print("%s done, %s seconds past, %d reocrds saved" %
        #       (self.getName(), gEndTime - gStartTime, gWritenRecord))
        mylock.release()
        self.transport.close()

    def write_hbase(self):
        # print(self.getName(), "Start write")

        for i in range(0, self.recordsPerThread):
            rowkey = "%s_%s" % (random.random() * 1000, time.time())
            put_columns = [
                TColumnValue('cf'.encode(), 'col_1'.encode(),
                             'value_1'.encode()),
                TColumnValue('cf'.encode(), 'col_2'.encode(),
                             'value_2'.encode())
            ]
            tput = TPut(rowkey.encode(), put_columns)
            self.client.put("test_neo".encode(), tput)


recordsPerThread = int(records / concurrent)
gStartTime = time.time()
for threadId in range(0, concurrent):
    t = writeThread("Thread_%s" % threadId, recordsPerThread)
    t.start()
print("%d thread created, each thread will write %d records" %
      (concurrent, recordsPerThread))
