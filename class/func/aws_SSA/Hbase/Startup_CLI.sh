# Create EMR 6.12
aws emr create-cluster \
--termination-protected \
--applications Name=Hbase Name=Zookeeper \
--ec2-attributes '{
    "KeyName": "visit-virginia-key",
    "InstanceProfile": "EMR_EC2_DefaultRole",
    "SubnetId": "subnet-019c68e2f66977e0e",
    "EmrManagedSlaveSecurityGroup": "sg-0708b2efbc03b7d46",
    "EmrManagedMasterSecurityGroup": "sg-0708b2efbc03b7d46"
}' \
--release-label emr-6.12.0 \
--log-uri 's3n://aws-logs-835751346093-us-east-1/elasticmapreduce/' \
--instance-groups '[{
    "InstanceCount": 2,
    "EbsConfiguration": {
        "EbsBlockDeviceConfigs": [{
            "VolumeSpecification": {
                "SizeInGB": 1024,
                "VolumeType": "gp3"
            },
            "VolumesPerInstance": 2
        }]
    },
    "InstanceGroupType": "CORE",
    "InstanceType": "m5.xlarge",
    "Name": "核心实例组 - 2"
}, {
    "InstanceCount": 1,
    "EbsConfiguration": {
        "EbsBlockDeviceConfigs": [{
            "VolumeSpecification": {
                "SizeInGB": 512,
                "VolumeType": "gp3"
            },
            "VolumesPerInstance": 2
        }]
    },
    "InstanceGroupType": "MASTER",
    "InstanceType": "m5.xlarge",
    "Name": "主实例组 - 1"
}]' \
--configurations '[{
    "Classification": "hive-site",
    "Properties": {
        "hive.metastore.client.factory.class": "com.amazonaws.glue.catalog.metastore.AWSGlueDataCatalogHiveClientFactory"
    }
}, {
    "Classification": "spark-hive-site",
    "Properties": {
        "hive.metastore.client.factory.class": "com.amazonaws.glue.catalog.metastore.AWSGlueDataCatalogHiveClientFactory"
    }
}]' \
--auto-scaling-role EMR_AutoScaling_DefaultRole \
--ebs-root-volume-size 100 \
--service-role EMR_DefaultRole \
--enable-debugging \
--name 'emr-6.12.0-Hbase' \
--scale-down-behavior TERMINATE_AT_TASK_COMPLETION \
--region us-east-1
