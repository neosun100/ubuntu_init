import os
import logging
import sys


LOGGER = logging.getLogger(__name__)
handler = logging.StreamHandler(sys.stdout)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
LOGGER.setLevel(logging.INFO)
LOGGER.addHandler(handler)

SSM_CORE_POLICY_ARN_TO_ATTACH = os.environ.get('SSM_CORE_POLICY_ARN_TO_ATTACH')
INSTANCE_PROFILE_ARN_TO_ATTACH = os.environ.get('INSTANCE_PROFILE_ARN_TO_ATTACH')
PATCH_GROUP = os.environ.get('PATCH_GROUP')
ASSOCIATION_SCHEDULE_EXPRESSION = 'rate(1 day)'
