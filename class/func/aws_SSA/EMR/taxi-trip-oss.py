from pyspark.sql import SparkSession
from pyspark.sql.functions import avg, col, year, round
import time
import numpy as np


def find_stats(data):
    return np.min(data), np.max(data), np.mean(data), np.std(data)


def run_query(spark, path, cardinality):
    start = time.time() * 1000
    df = spark.read.parquet(path)
    if cardinality == 1:
        df.groupBy('fare_amt').agg(avg('total_amt')).collect()
    elif cardinality == 2:
        df.groupBy('fare_amt', year('trip_pickup_datetime').alias(
            'trip_year')).agg(avg('total_amt')).collect()
    elif cardinality == 3:
        df.groupBy('fare_amt', year('trip_pickup_datetime').alias('trip_year'), round(
            'trip_distance').alias('trip_distance')).agg(avg('total_amt')).collect()
    elif cardinality == 4:
        df.groupBy('fare_amt', year('trip_pickup_datetime').alias('trip_year'), round('trip_distance').alias(
            'trip_distance')).agg(avg('total_amt')).filter(col('trip_year').isNotNull()).orderBy('trip_year').collect()
    end = time.time() * 1000
    return end - start


def run_experiment(path):
    spark = SparkSession.builder.appName("PerformanceTest").enableHiveSupport() \
        .config("spark.hadoop.fs.s3a.access.key", "AKIA4FFVJP6WX7BWX2XE") \
        .config("spark.hadoop.fs.s3a.secret.key", "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") \
        .config("spark.hadoop.fs.s3a.impl", "org.apache.hadoop.fs.s3a.S3AFileSystem").getOrCreate()
    spark.sparkContext.setLogLevel("ERROR")
    trials = 20
    times = []

    for i in range(1, 5):
        for _ in range(trials):
            times.append(run_query(spark, path, i))

    min_val, max_val, mean_val, std_dev = find_stats(times)
    spark.sparkContext.parallelize([(min_val, max_val, mean_val, std_dev)]).saveAsTextFile(
        f"s3a://virginia-nyc-tlc/trip-data-yellow-only-aggregation_test_{int(time.time() * 1000)}.txt")
run_experiment("s3a://virginia-nyc-tlc/trip-data-yellow-only")

if __name__ == "__main__":
    run_experiment("s3a://virginia-nyc-tlc/trip-data-yellow-only")

# 优化点：

# 使用NumPy计算统计量，效率更高。
# 将相似的操作合并在一个函数run_query中，减少代码重复。
# 移除不必要的循环和数组操作。
