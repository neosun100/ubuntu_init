from datetime import datetime
import time
from databricks import sql
import os
import boto3


def get_s3_files(bucket_name, prefix, file_type):
    s3_resource = boto3.resource("s3")
    bucket = s3_resource.Bucket(bucket_name)
    s3_source_files = []
    for object in bucket.objects.filter(Prefix=prefix):
        if object.key.endswith(f".{file_type}"):
            s3_source_files.append(f"""{object.get()['Body'].read()}""")
    return s3_source_files


sql_files = get_s3_files(bucket_name='tpc-ds-james',
                         prefix='sql', file_type='sql')

with sql.connect('dbc-38cda7ff-9756.cloud.databricks.com',
                 '/sql/1.0/endpoints/78691485368c54eb',
                 'dapid0d07d579afdbad1d60e1f3c40b74892') as connection:

    for sql_str in sql_files:
        with connection.cursor() as cursor:
            try:
                cursor.execute('use tpcds1t;')
                lines = sql_str.split("\\n")
                name = lines[0]
                #sql_str = ' '.join(sql_str.split("\\n")[1:])[:-1]
                sql_str = ' '.join(lines[1:])[:-1]
                sql_str = sql_str.replace('\\t', ' ')
                #print(" sql :", sql_str)
                #cursor.execute('select *from item limit 10;')
                start = datetime.now().timestamp()
                cursor.execute(f"""{sql_str}""")
                result = cursor.fetchall()
                end = datetime.now().timestamp()
                print(str(name[2:]), ',', end - start, '|')
            except Exception as e:
                print(" error sql :", str(name), e)
