from pyspark.sql import SparkSession
import time

spark = SparkSession.builder.appName("ShuffleTest").getOrCreate()

# 测试1：大规模shuffle
start1 = time.time()

result = spark.sparkContext.parallelize(range(1, 8001), 8000) \
    .flatMap(lambda _: (num for num in range(1, 45000001))) \
    .repartition(8000) \
    .count()

print(f"Result: {result}")

diff = time.time() - start1
print(f"Shuffle 45000000 took {diff} seconds")
