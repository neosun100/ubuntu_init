sudo umount /dev/nvme1n1
sudo umount /dev/nvme2n1

cd ~
sudo parted /dev/nvme1n1 mktable gpt
sudo parted /dev/nvme1n1 mkpart primary ext4 1MB 100%
sudo mkfs -t ext4  -F /dev/nvme1n1

sudo parted /dev/nvme2n1 mktable gpt
sudo parted /dev/nvme2n1 mkpart primary ext4 1MB 100%
sudo mkfs -t ext4 -F /dev/nvme2n1

sudo mkdir -p /mnt/disk1
sudo mkdir -p /mnt/disk2
echo y | sudo mount /dev/nvme1n1 /mnt/disk1
echo y | sudo mount /dev/nvme2n1 /mnt/disk2

export celeborn_uid=10006
export celeborn_gid=10006
export CELEBORN_HOME=/opt/celeborn

echo y | apt-get update && \
    apt-get install -y git bash tini bind9-utils telnet net-tools procps dnsutils krb5-user && \
    ln -snf /bin/bash /bin/sh && \
    rm -rf /var/cache/apt/* && \
    groupadd --gid=${celeborn_gid} celeborn && \
    useradd  --uid=${celeborn_uid} --gid=${celeborn_gid} celeborn -d /home/celeborn -m && \
    mkdir -p ${CELEBORN_HOME}


sudo chown -R celeborn:celeborn /mnt/disk1 /mnt/disk2
# curl https://meloyang-emr-bda.s3.amazonaws.com/spark3.3-apache-celeborn-0.2.2-SNAPSHOT-bin.tgz
# aws s3 cp s3://cloudsort-virginia/jars/apache-celeborn-0.4.0-SNAPSHOT-bin.tgz  apache-celeborn-0.4.0-SNAPSHOT-bin.tgz 
# curl http://cdn.neo.pub/image/apache-celeborn-0.4.0-SNAPSHOT-bin-Spark-3.4.tar
cd ~
rm -rf apache-celeborn-0.4.0-SNAPSHOT-bin-Spark-3.4.tar
rm -rf /opt/celeborn/*
# wget http://cdn.neo.pub/image/apache-celeborn-0.4.0-SNAPSHOT-bin-Spark-3.4.tar
wget https://dlcdn.apache.org/incubator/celeborn/celeborn-0.3.1-incubating/apache-celeborn-0.3.1-incubating-bin.tgz
# tar -zxvf apache-celeborn-0.4.0-SNAPSHOT-bin-Spark-3.4.tar
tar -zxvf apache-celeborn-0.3.1-incubating-bin.tgz
mv apache-celeborn-*-bin/* /opt/celeborn/
# cat *.tgz | tar -xvzf - && mv apache-celeborn-*-bin /opt/celeborn
sudo chmod 777 -R /opt/celeborn

sudo chown -R celeborn:celeborn ${CELEBORN_HOME} && \
sudo chmod -R ug+rw ${CELEBORN_HOME} && \
sudo chmod a+x ${CELEBORN_HOME}/bin/* && \
sudo chmod a+x ${CELEBORN_HOME}/sbin/*

cat <<EOF >$CELEBORN_HOME/conf/celeborn-env.sh
CELEBORN_MASTER_MEMORY=8g
CELEBORN_WORKER_MEMORY=8g
CELEBORN_WORKER_OFFHEAP_MEMORY=130g
CELEBORN_NO_DAEMONIZE=1
CELEBORN_WORKER_JAVA_OPTS="-XX:-PrintGC -XX:+PrintGCDetails -XX:+PrintGCTimeStamps -XX:+PrintGCDateStamps -Xloggc:/opt/celeborn/logs/gc-worker.out -Dio.netty.leakDetectionLevel=advanced"
CELEBORN_MASTER_JAVA_OPTS="-XX:-PrintGC -XX:+PrintGCDetails -XX:+PrintGCTimeStamps -XX:+PrintGCDateStamps -Xloggc:/opt/celeborn/logs/gc-master.out -Dio.netty.leakDetectionLevel=advanced"
CELEBORN_PID_DIR="/opt/celeborn/pids"
CELEBORN_LOG_DIR="/opt/celeborn/logs"
EOF


cat <<EOF >$CELEBORN_HOME/conf/celeborn-defaults.conf
# HA master mode in the EKS example
# however, we use the single master mode to simplify the EC2 setup
celeborn.ha.enabled=false

# used by client and worker to connect to master
# the endpoint can be either an alias or use your EC2's private IP DNS name
celeborn.master.endpoints=celeborn-master-0:9097
# celeborn.master.endpoints=ip-10-0-49-238.us-west-2.compute.internal:9097

celeborn.metrics.enabled=true
celeborn.network.timeout=2000s
celeborn.worker.storage.dirs /mnt/disk1,/mnt/disk2
celeborn.master.metrics.prometheus.port=9098
celeborn.worker.metrics.prometheus.port=9096
# If your hosts have disk raid or use lvm, set celeborn.worker.monitor.disk.enabled to false
celeborn.worker.monitor.disk.enabled=false
celeborn.push.io.numConnectionsPerPeer=8
celeborn.replicate.io.numConnectionsPerPeer=24
celeborn.worker.closeIdleConnections=true
celeborn.worker.commit.threads=128
celeborn.worker.fetch.io.threads=256
celeborn.worker.flusher.buffer.size=128k
# celeborn.worker.flusher.threads=512
celeborn.worker.flusher.ssd.threads: 512
celeborn.worker.push.io.threads=128
celeborn.worker.replicate.io.threads=128
# # worker recover - ip & port must be the same after a worker-restart.
# celeborn.worker.graceful.shutdown.enabled: true
# celeborn.worker.graceful.shutdown.recoverPath: /tmp/recover
# celeborn.worker.rpc.port: 9094
# celeborn.worker.fetch.port: 9092
# celeborn.worker.push.port: 9091
# celeborn.worker.replicate.port: 9089
EOF

cp $CELEBORN_HOME/conf/log4j2.xml.template $CELEBORN_HOME/conf/log4j2.xml


cat <<EOF >$CELEBORN_HOME/conf/hosts
[master]
celeborn-master-0

[worker]
celeborn-worker-0
celeborn-worker-1
celeborn-worker-2
EOF

apt-get install unzip
apt-get install zip
install_sdk_cli() {
    echo "for the future give a try to sdkman, is better than brew"
    echo "https://sdkman.io/install"
    curl -s "https://get.sdkman.io" | bash
    source "$HOME/.sdkman/bin/sdkman-init.sh"
    sdk version
    sdk list java
    echo "sdk install java 8.0.392-amzn"

    echo "sdk use java 11.0.14.10.1-amzn"
    echo "cd ~/ && sdk env init"
}

install_sdk_cli
sdk install java 8.0.392-amzn