from pyspark.sql import SparkSession
from pyspark.sql.functions import avg, col, year, round
import time
import numpy as np


def find_stats(data):
    return np.min(data), np.max(data), np.mean(data), np.std(data)


def run_query(spark, path, cardinality):
    start = time.time() * 1000
    df = spark.read.parquet(path)
    if cardinality == 1:
        df.groupBy('fare_amt').agg(avg('total_amt')).collect()
    elif cardinality == 2:
        df.groupBy('fare_amt', year('trip_pickup_datetime').alias(
            'trip_year')).agg(avg('total_amt')).collect()
    elif cardinality == 3:
        df.groupBy('fare_amt', year('trip_pickup_datetime').alias('trip_year'), round(
            'trip_distance').alias('trip_distance')).agg(avg('total_amt')).collect()
    elif cardinality == 4:
        df.groupBy('fare_amt', year('trip_pickup_datetime').alias('trip_year'), round('trip_distance').alias(
            'trip_distance')).agg(avg('total_amt')).filter(col('trip_year').isNotNull()).orderBy('trip_year').collect()
    end = time.time() * 1000
    return end - start


def run_experiment(path):
    spark = SparkSession.builder.appName(
        "PerformanceTest").enableHiveSupport().getOrCreate()
    spark.sparkContext.setLogLevel("ERROR")
    trials = 20
    times = []
    all_stats = []

    df_initialized = False  # Flag to check if df is initialized

    for i in range(1, 5):
        times = []
        for _ in range(trials):
            times.append(run_query(spark, path, i))

        min_val, max_val, mean_val, std_dev = find_stats(times)
        all_stats.append((i, min_val, max_val, mean_val, std_dev))

        newRow = spark.createDataFrame([(i, float(min_val), float(max_val), float(mean_val), float(std_dev))])

        if not df_initialized:
            df = newRow
            df_initialized = True
        else:
            df = df.union(newRow)

    df.repartition(1).write.csv(
        f"s3a://virginia-nyc-tlc/trip-data-yellow-only-aggregation_test_{int(time.time() * 1000)}.csv", header=True)


if __name__ == "__main__":
    run_experiment("s3a://virginia-nyc-tlc/trip-data-yellow-only")