aws emr create-cluster \
    --name "emr6.15-spark3.4.1-r7g-HA-fleets-managed" \
    --log-uri "s3n://aws-logs-835751346093-ap-northeast-1/elasticmapreduce/" \
    --release-label "emr-6.15.0" \
    --service-role "arn:aws:iam::835751346093:role/EMR_DefaultRole" \
    --managed-scaling-policy '{"ComputeLimits":{"UnitType":"InstanceFleetUnits","MinimumCapacityUnits":128,"MaximumCapacityUnits":2000,"MaximumOnDemandCapacityUnits":256,"MaximumCoreCapacityUnits":256}}' \
    --termination-protected \
    --ec2-attributes '{"InstanceProfile":"EMR_EC2_DefaultRole","EmrManagedMasterSecurityGroup":"sg-04551bef3c569fb6d","EmrManagedSlaveSecurityGroup":"sg-0d3952326df49f5bc","KeyName":"tokyo-neo-visit","AdditionalMasterSecurityGroups":[],"AdditionalSlaveSecurityGroups":[],"SubnetId":"subnet-0b3c764baf2f9a153"}' \
    --applications Name=Spark \
    --configurations '[{"Classification":"spark-hive-site","Properties":{"hive.metastore.client.factory.class":"com.amazonaws.glue.catalog.metastore.AWSGlueDataCatalogHiveClientFactory"}}]' \
    --instance-fleets '[{
    "Name": "任务节点 - 1",
    "InstanceFleetType": "TASK",
    "TargetSpotCapacity": 1024,
    "TargetOnDemandCapacity": 0,
    "LaunchSpecifications": {
        "SpotSpecification": {
            "TimeoutDurationMinutes": 180,
            "TimeoutAction": "TERMINATE_CLUSTER",
            "AllocationStrategy": "PRICE_CAPACITY_OPTIMIZED"
        },
        "OnDemandSpecification": {
            "AllocationStrategy": "LOWEST_PRICE"
        }
    },
    "InstanceTypeConfigs": [{
        "WeightedCapacity": 48,
        "EbsConfiguration": {
            "EbsOptimized": true,
            "EbsBlockDeviceConfigs": [{
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 32
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 32
                }
            }]
        },
        "BidPriceAsPercentageOfOnDemandPrice": 100,
        "InstanceType": "r6g.12xlarge"
    }, {
        "WeightedCapacity": 48,
        "EbsConfiguration": {
            "EbsBlockDeviceConfigs": [{
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 192
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 192
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 192
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 192
                }
            }]
        },
        "BidPriceAsPercentageOfOnDemandPrice": 100,
        "InstanceType": "m6g.12xlarge"
    }, {
        "WeightedCapacity": 64,
        "EbsConfiguration": {
            "EbsBlockDeviceConfigs": [{
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }]
        },
        "BidPriceAsPercentageOfOnDemandPrice": 100,
        "InstanceType": "m6i.16xlarge"
    }, {
        "WeightedCapacity": 64,
        "EbsConfiguration": {
            "EbsBlockDeviceConfigs": [{
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }]
        },
        "BidPriceAsPercentageOfOnDemandPrice": 100,
        "InstanceType": "m6g.16xlarge"
    }, {
        "WeightedCapacity": 48,
        "EbsConfiguration": {},
        "BidPriceAsPercentageOfOnDemandPrice": 100,
        "InstanceType": "r6gd.12xlarge"
    }, {
        "WeightedCapacity": 64,
        "EbsConfiguration": {
            "EbsBlockDeviceConfigs": [{
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }]
        },
        "BidPriceAsPercentageOfOnDemandPrice": 100,
        "InstanceType": "r5n.16xlarge"
    }, {
        "WeightedCapacity": 32,
        "EbsConfiguration": {
            "EbsBlockDeviceConfigs": [{
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 128
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 128
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 128
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 128
                }
            }]
        },
        "BidPriceAsPercentageOfOnDemandPrice": 100,
        "InstanceType": "m6i.8xlarge"
    }, {
        "WeightedCapacity": 96,
        "EbsConfiguration": {
            "EbsBlockDeviceConfigs": [{
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 384
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 384
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 384
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 384
                }
            }]
        },
        "BidPriceAsPercentageOfOnDemandPrice": 100,
        "InstanceType": "m5a.24xlarge"
    }, {
        "WeightedCapacity": 96,
        "EbsConfiguration": {
            "EbsBlockDeviceConfigs": [{
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 384
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 384
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 384
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 384
                }
            }]
        },
        "BidPriceAsPercentageOfOnDemandPrice": 100,
        "InstanceType": "m6i.24xlarge"
    }, {
        "WeightedCapacity": 32,
        "EbsConfiguration": {
            "EbsBlockDeviceConfigs": [{
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 128
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 128
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 128
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 128
                }
            }]
        },
        "BidPriceAsPercentageOfOnDemandPrice": 100,
        "InstanceType": "m6g.8xlarge"
    }, {
        "WeightedCapacity": 48,
        "EbsConfiguration": {
            "EbsBlockDeviceConfigs": [{
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 192
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 192
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 192
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 192
                }
            }]
        },
        "BidPriceAsPercentageOfOnDemandPrice": 100,
        "InstanceType": "m5zn.12xlarge"
    }, {
        "WeightedCapacity": 48,
        "EbsConfiguration": {
            "EbsBlockDeviceConfigs": [{
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 192
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 192
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 192
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 192
                }
            }]
        },
        "BidPriceAsPercentageOfOnDemandPrice": 100,
        "InstanceType": "m6i.12xlarge"
    }, {
        "WeightedCapacity": 64,
        "EbsConfiguration": {},
        "BidPriceAsPercentageOfOnDemandPrice": 100,
        "InstanceType": "r6gd.16xlarge"
    }, {
        "WeightedCapacity": 96,
        "EbsConfiguration": {
            "EbsBlockDeviceConfigs": [{
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 384
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 384
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 384
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 384
                }
            }]
        },
        "BidPriceAsPercentageOfOnDemandPrice": 100,
        "InstanceType": "r5a.24xlarge"
    }, {
        "WeightedCapacity": 48,
        "EbsConfiguration": {
            "EbsBlockDeviceConfigs": [{
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }]
        },
        "BidPriceAsPercentageOfOnDemandPrice": 100,
        "InstanceType": "r5.12xlarge"
    }, {
        "WeightedCapacity": 32,
        "EbsConfiguration": {
            "EbsBlockDeviceConfigs": [{
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }]
        },
        "BidPriceAsPercentageOfOnDemandPrice": 100,
        "InstanceType": "r6gd.8xlarge"
    }, {
        "WeightedCapacity": 48,
        "EbsConfiguration": {
            "EbsBlockDeviceConfigs": [{
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }]
        },
        "BidPriceAsPercentageOfOnDemandPrice": 100,
        "InstanceType": "r5dn.12xlarge"
    }, {
        "WeightedCapacity": 96,
        "EbsConfiguration": {
            "EbsBlockDeviceConfigs": [{
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }]
        },
        "BidPriceAsPercentageOfOnDemandPrice": 100,
        "InstanceType": "m5n.24xlarge"
    }, {
        "WeightedCapacity": 96,
        "EbsConfiguration": {
            "EbsBlockDeviceConfigs": [{
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }]
        },
        "BidPriceAsPercentageOfOnDemandPrice": 100,
        "InstanceType": "r5b.24xlarge"
    }, {
        "WeightedCapacity": 64,
        "EbsConfiguration": {
            "EbsBlockDeviceConfigs": [{
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }]
        },
        "BidPriceAsPercentageOfOnDemandPrice": 100,
        "InstanceType": "m5n.16xlarge"
    }, {
        "WeightedCapacity": 48,
        "EbsConfiguration": {
            "EbsBlockDeviceConfigs": [{
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }]
        },
        "BidPriceAsPercentageOfOnDemandPrice": 100,
        "InstanceType": "r5n.12xlarge"
    }, {
        "WeightedCapacity": 64,
        "EbsConfiguration": {
            "EbsBlockDeviceConfigs": [{
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }]
        },
        "BidPriceAsPercentageOfOnDemandPrice": 100,
        "InstanceType": "r6i.16xlarge"
    }, {
        "WeightedCapacity": 32,
        "EbsConfiguration": {
            "EbsBlockDeviceConfigs": [{
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }]
        },
        "BidPriceAsPercentageOfOnDemandPrice": 100,
        "InstanceType": "m5d.8xlarge"
    }, {
        "WeightedCapacity": 96,
        "EbsConfiguration": {
            "EbsBlockDeviceConfigs": [{
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }]
        },
        "BidPriceAsPercentageOfOnDemandPrice": 100,
        "InstanceType": "r5n.24xlarge"
    }, {
        "WeightedCapacity": 64,
        "EbsConfiguration": {
            "EbsBlockDeviceConfigs": [{
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }]
        },
        "BidPriceAsPercentageOfOnDemandPrice": 100,
        "InstanceType": "r5a.16xlarge"
    }, {
        "WeightedCapacity": 64,
        "EbsConfiguration": {
            "EbsBlockDeviceConfigs": [{
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }]
        },
        "BidPriceAsPercentageOfOnDemandPrice": 100,
        "InstanceType": "r6id.16xlarge"
    }, {
        "WeightedCapacity": 32,
        "EbsConfiguration": {
            "EbsBlockDeviceConfigs": [{
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }]
        },
        "BidPriceAsPercentageOfOnDemandPrice": 100,
        "InstanceType": "r5a.8xlarge"
    }]
}, {
    "Name": "主节点",
    "InstanceFleetType": "MASTER",
    "TargetSpotCapacity": 0,
    "TargetOnDemandCapacity": 1,
    "LaunchSpecifications": {
        "OnDemandSpecification": {
            "AllocationStrategy": "LOWEST_PRICE"
        }
    },
    "InstanceTypeConfigs": [{
        "WeightedCapacity": 1,
        "EbsConfiguration": {
            "EbsBlockDeviceConfigs": [{
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 64
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 64
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 64
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 64
                }
            }]
        },
        "BidPriceAsPercentageOfOnDemandPrice": 100,
        "InstanceType": "m7g.4xlarge"
    }]
}, {
    "Name": "核心节点",
    "InstanceFleetType": "CORE",
    "TargetSpotCapacity": 0,
    "TargetOnDemandCapacity": 256,
    "LaunchSpecifications": {
        "OnDemandSpecification": {
            "AllocationStrategy": "LOWEST_PRICE"
        }
    },
    "InstanceTypeConfigs": [{
        "WeightedCapacity": 64,
        "EbsConfiguration": {
            "EbsBlockDeviceConfigs": [{
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 256
                }
            }]
        },
        "BidPriceAsPercentageOfOnDemandPrice": 100,
        "InstanceType": "r7g.16xlarge"
    }, {
        "WeightedCapacity": 48,
        "EbsConfiguration": {
            "EbsBlockDeviceConfigs": [{
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 192
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 192
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 192
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 192
                }
            }]
        },
        "BidPriceAsPercentageOfOnDemandPrice": 100,
        "InstanceType": "r7g.12xlarge"
    }, {
        "WeightedCapacity": 32,
        "EbsConfiguration": {
            "EbsBlockDeviceConfigs": [{
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 128
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 128
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 128
                }
            }, {
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 128
                }
            }]
        },
        "BidPriceAsPercentageOfOnDemandPrice": 100,
        "InstanceType": "r7g.8xlarge"
    }, {
        "WeightedCapacity": 64,
        "EbsConfiguration": {},
        "BidPriceAsPercentageOfOnDemandPrice": 100,
        "InstanceType": "r7gd.16xlarge"
    }, {
        "WeightedCapacity": 48,
        "EbsConfiguration": {},
        "BidPriceAsPercentageOfOnDemandPrice": 100,
        "InstanceType": "r7gd.12xlarge"
    }]
}]' \
    --scale-down-behavior "TERMINATE_AT_TASK_COMPLETION" \
    --auto-termination-policy '{"IdleTimeout":14400}' \
    --os-release-label "2.0.20231116.0" \
    --region "ap-northeast-1" \
    --auto-terminate \
    --tags 'clusterType=CelebornPOC' 'testType=hugeShuffle' 'jobType=unceleborn-emr6.10-hugeshuffle' \
    --steps '   
[
    {
        "Type": "CUSTOM_JAR",
        "Name": "Custom JAR Step",
        "ActionOnFailure": "CONTINUE",
        "Jar": "s3://ap-northeast-1.elasticmapreduce/libs/script-runner/script-runner.jar",
        "Args": ["s3://tpcds-glue/tpcds/codes/add_step_01_deploy_celeborn_client_jar.sh"]
    },
    {
        "Type": "Spark",
        "Name": "Spark Application",
        "ActionOnFailure": "CONTINUE",
        "Args": ["--deploy-mode", "cluster", "--master", "yarn", "--conf", "spark.sql.adaptive.enabled=true", "--conf", "spark.sql.adaptive.skewJoin.enabled=true", "--conf", "spark.dynamicAllocation.enabled=true", "--conf", "spark.shuffle.service.enabled=true", "--conf", "spark.serializer=org.apache.spark.serializer.KryoSerializer", "s3://tpcds-glue/tpcds/codes/huge_shuffle.py"]
    }
]'
    
    
    # 'Type=Spark,Name="Spark Application",ActionOnFailure=CONTINUE,Args=[--deploy-mode,cluster,--master,yarn,spark-submit,--conf,spark.shuffle.manager=org.apache.spark.shuffle.celeborn.SparkShuffleManager,--conf,spark.serializer=org.apache.spark.serializer.KryoSerializer,--conf,spark.celeborn.master.endpoints=10.68.1.87:9097,--conf,spark.shuffle.service.enabled=true,--conf,spark.dynamicAllocation.enabled=true,--conf,spark.celeborn.client.spark.shuffle.writer=hash,--conf,spark.celeborn.client.push.replicate.enabled=true,--conf,spark.sql.adaptive.localShuffleReader.enabled=false,--conf,spark.sql.adaptive.enabled=true,--conf,spark.sql.adaptive.skewJoin.enabled=true,--conf,spark.celeborn.shuffle.chunk.size=4m,--conf,spark.celeborn.client.push.maxReqsInFlight=128,--conf,spark.celeborn.rpc.askTimeout=240s,--conf,spark.celeborn.client.push.blacklist.enabled=true,--conf,spark.celeborn.client.push.excludeWorkerOnFailure.enabled=true,--conf,spark.celeborn.client.fetch.excludeWorkerOnFailure.enabled=true,--conf,spark.celeborn.client.commitFiles.ignoreExcludedWorker=true,--conf,spark.sql.optimizedUnsafeRowSerializers.enabled=false,s3://tpcds-glue/tpcds/codes/huge_shuffle.py]'
    # --steps 'Type=CUSTOM_JAR,Name="Custom JAR Step",ActionOnFailure=CONTINUE,Jar="s3://ap-northeast-1.elasticmapreduce/libs/script-runner/script-runner.jar",Args=["s3://tpcds-glue/tpcds/codes/add_step_01_deploy_celeborn_client_jar.sh"],Type=Spark,Name="Spark Application",ActionOnFailure=CONTINUE,Args=[--deploy-mode,cluster,--master,yarn,spark-submit,--conf,spark.sql.adaptive.enabled=true,--conf,spark.sql.adaptive.skewJoin.enabled=true,--conf,spark.dynamicAllocation.enabled=true,--conf,spark.shuffle.service.enabled=true,--conf,spark.serializer=org.apache.spark.serializer.KryoSerializer,s3://tpcds-glue/tpcds/codes/huge_shuffle.py]'
