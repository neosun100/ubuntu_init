import streamlit as st
import pandas as pd
import plotly.graph_objs as go
from plotly.subplots import make_subplots

# 设置页面标题
st.title('YARN Metrics Visualization')

# 设置自动刷新间隔（以秒为单位）
refresh_interval = st.slider('Refresh interval (seconds)', 5, 60, 10)

@st.cache(ttl=refresh_interval, allow_output_mutation=True)
def load_data(csv_file):
    return pd.read_csv(csv_file)

# 绘制图表
def plot_data(df):
    # 创建子图
    fig = make_subplots(specs=[[{"secondary_y": True}]])

    # 添加柱状图
    fig.add_trace(go.Bar(x=df['Timestamp'], y=df['LowPriorityAcceptedCount'], name='Low Priority Accepted'), secondary_y=False)
    fig.add_trace(go.Bar(x=df['Timestamp'], y=df['LowPriorityRunningCount'], name='Low Priority Running'), secondary_y=False)
    fig.add_trace(go.Bar(x=df['Timestamp'], y=df['HighPriorityAcceptedCount'], name='High Priority Accepted'), secondary_y=False)
    fig.add_trace(go.Bar(x=df['Timestamp'], y=df['HighPriorityRunningCount'], name='High Priority Running'), secondary_y=False)

    # 添加线图
    fig.add_trace(go.Scatter(x=df['Timestamp'], y=df['TotalVirtualCores'], mode='lines+markers', name='Total Virtual Cores'), secondary_y=True)
    fig.add_trace(go.Scatter(x=df['Timestamp'], y=df['MaximumCapacityUnits'], mode='lines+markers', name='Maximum Capacity Units'), secondary_y=True)
    fig.add_trace(go.Scatter(x=df['Timestamp'], y=df['MaximumOnDemandCapacityUnits'], mode='lines+markers', name='Maximum On Demand Capacity Units'), secondary_y=True)

    # 设置图表布局
    fig.update_layout(title='YARN and EMR Metrics Visualization', xaxis_title='Time (Hour:Minute)', yaxis_title='Accepted Count')
    fig.update_yaxes(title_text='Units and Cores', secondary_y=True)

    return fig

def main():
    # 加载数据
    df = load_data(csv_file='yarn_data.csv')

    # 转换时间戳为datetime
    df['Timestamp'] = pd.to_datetime(df['Timestamp'])

    # 时间范围选择器
    time_options = df['Timestamp'].dt.strftime('%Y-%m-%d %H:%M').unique().tolist()
    start_time = st.selectbox("Select Start Time", options=time_options, index=0)
    end_time = st.selectbox("Select End Time", options=time_options, index=len(time_options)-1)

    # 根据选择的时间过滤数据
    filtered_df = df[(df['Timestamp'] >= pd.to_datetime(start_time)) & (df['Timestamp'] <= pd.to_datetime(end_time))]

    # 绘制图表
    fig = plot_data(filtered_df)
    st.plotly_chart(fig, use_container_width=True)

    # 自动刷新
    st.button('Refresh')

if __name__ == '__main__':
    main()