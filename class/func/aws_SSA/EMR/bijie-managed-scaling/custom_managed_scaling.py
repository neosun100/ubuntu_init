from apscheduler.schedulers.background import BackgroundScheduler
import requests
import boto3
from datetime import datetime
from prettytable import PrettyTable
from loguru import logger
from functools import wraps
import time

# YARN ResourceManager URL
YARN_RM_URL = 'http://10.68.1.77:8088'
# 集群ID
CLUSTER_ID = 'j-BWBLFIA6FR6L'


# 配置loguru日志
logger.add("action.log", format="{time} {level} {message}", level="DEBUG")

# 创建一个EMR客户端
emr = boto3.client('emr')

ssm = boto3.client("ssm")

maximum_cluster_size = int(ssm.get_parameter(
    Name="/fleet-settings/maximum-cluster-size", WithDecryption=True)["Parameter"]["Value"])
minimum_cluster_size = int(ssm.get_parameter(
    Name="/fleet-settings/minimum-cluster-size", WithDecryption=True)["Parameter"]["Value"])

maximum_core_nodes_units = int(ssm.get_parameter(
    Name="/fleet-settings/maximum-core-nodes-in-the-cluster", WithDecryption=True)["Parameter"]["Value"])
maximum_on_demand_units = int(ssm.get_parameter(
    Name="/fleet-settings/maximum-on-demand-instances-in-the-cluster", WithDecryption=True)["Parameter"]["Value"])

excess_maximum_cluster_size_rate = float(ssm.get_parameter(
    Name="/fleet-settings/excess-maximum-cluster-size-rate", WithDecryption=True)["Parameter"]["Value"])
standard_maximum_cluster_size_rate = float(ssm.get_parameter(
    Name="/fleet-settings/standard-maximum-cluster-size-rate", WithDecryption=True)["Parameter"]["Value"])

monitor_scaling_range_seconds = int(ssm.get_parameter(
    Name="/fleet-settings/monitor-scaling-range-seconds", WithDecryption=True)["Parameter"]["Value"])


def exception_handler(func):
    """
    Decorator to handle exceptions within functions and log them.
    """
    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except Exception as e:
            logger.error(f"An error occurred in {func.__name__}: {e}")
            raise
    return wrapper


@exception_handler
def get_applications():
    """
    Get the list of applications from YARN ResourceManager.
    """
    response = requests.get(f'{YARN_RM_URL}/ws/v1/cluster/apps')
    response.raise_for_status()
    return response.json().get('apps', {}).get('app', [])


@exception_handler
def get_total_virtual_cores():
    """
    Get the totalVirtualCores from YARN ResourceManager.
    """
    response = requests.get(f'{YARN_RM_URL}/ws/v1/cluster/metrics')
    response.raise_for_status()
    metrics = response.json().get('clusterMetrics', {})
    return metrics.get('totalVirtualCores', 0)


@exception_handler
def check_lack_of_capacity(dynamodb_resource, table_name, total_virtual_cores, time_threshold):
    """
    Check and update the lack_of_capacity based on DynamoDB records and totalVirtualCores.
    """
    table = dynamodb_resource.Table(table_name)
    response = table.scan(
        FilterExpression=boto3.dynamodb.conditions.Attr(
            'Timestamp').gt(time_threshold)
    )
    items = response.get('Items', [])
    if not items:
        return 0

    # Check if all MaximumCapacityUnits are greater than total_virtual_cores
    if all(item['MaximumCapacityUnits'] > total_virtual_cores for item in items):
        # Find the minimum MaximumCapacityUnits from these items
        min_max_capacity = min(item['MaximumCapacityUnits'] for item in items)
        return max(0, min_max_capacity - total_virtual_cores)
    else:
        return 0


lack_of_capacity = 0  # 定义是为了未来扩展当前字段的逻辑


@exception_handler
def apply_managed_scaling_policy(policy):
    """
    Apply the managed scaling policy to the EMR cluster, log the policy details to DynamoDB, and print them.
    """
    # 应用托管扩展策略
    # 获取当前的UNIX时间戳
    timestamp = int(time.time())

    response = emr.put_managed_scaling_policy(
        ClusterId=CLUSTER_ID,
        ManagedScalingPolicy=policy
    )
    logger.info("Managed scaling policy applied successfully.")

    # 确保计算限制的值是整数类型
    # 提取所需字段
    # 提取所需字段
    compute_limits = policy.get('ComputeLimits', {})
    maximum_capacity_units = compute_limits.get('MaximumCapacityUnits', 0)
    minimum_capacity_units = compute_limits.get('MinimumCapacityUnits', 0)
    maximum_on_demand_units = compute_limits.get(
        'MaximumOnDemandCapacityUnits', 0)
    maximum_core_units = compute_limits.get('MaximumCoreCapacityUnits', 0)
    timestamp = int(time.time())
    # compute_limits = policy.get('ComputeLimits', {})
    # maximum_capacity_units = compute_limits.get('MaximumCapacityUnits', 0)
    # minimum_capacity_units = compute_limits.get('MinimumCapacityUnits', 0)
    # maximum_on_demand_units = compute_limits.get(
    #     'MaximumOnDemandCapacityUnits', 0)
    # maximum_core_units = compute_limits.get('MaximumCoreCapacityUnits', 0)
    # timestamp = int(time.time())
    # compute_limits = policy.get('ComputeLimits', {})
    # compute_limits['MaximumCapacityUnits'] = int(compute_limits.get('MaximumCapacityUnits', 0))
    # compute_limits['MinimumCapacityUnits'] = int(compute_limits.get('MinimumCapacityUnits', 0))
    # compute_limits['MaximumOnDemandCapacityUnits'] = int(compute_limits.get('MaximumOnDemandCapacityUnits', 0))
    # compute_limits['MaximumCoreCapacityUnits'] = int(compute_limits.get('MaximumCoreCapacityUnits', 0))
    # 打印策略详情
    policy_details = {
        'Timestamp': timestamp,
        'MaximumCapacityUnits': maximum_capacity_units,
        'MinimumCapacityUnits': minimum_capacity_units,
        'MaximumOnDemandCapacityUnits': maximum_on_demand_units,
        'MaximumCoreCapacityUnits': maximum_core_units
    }
    print("Policy Details:", policy_details)

    # 将策略详情写入DynamoDB
    dynamodb = boto3.resource('dynamodb')
    table = dynamodb.Table('invoke_log')
    table.put_item(Item=policy_details)
    logger.info("Policy logged to DynamoDB.")

    return response


lack_of_capacity = 0  # 定义是为了未来扩展当前字段的逻辑


def create_scaling_policy(priority_count):
    """
    Create a scaling policy based on the count of high priority applications.
    """
    global lack_of_capacity  # 引用全局变量

    # 如果priority_count <= 1，则lack_of_capacity始终为0
    if priority_count <= 1:
        lack_of_capacity = 0

    base_policy = {
        "ComputeLimits": {
            "UnitType": "InstanceFleetUnits",
            "MinimumCapacityUnits": minimum_cluster_size,
            "MaximumOnDemandCapacityUnits": int(maximum_on_demand_units) + int(lack_of_capacity),
            "MaximumCoreCapacityUnits": maximum_core_nodes_units
        }
    }
    if priority_count < 1:
        base_policy["ComputeLimits"]["MaximumCapacityUnits"] = int(maximum_cluster_size *
                                                                   standard_maximum_cluster_size_rate)
    elif 1 < priority_count <= 3:
        base_policy["ComputeLimits"]["MaximumCapacityUnits"] = maximum_cluster_size
    else:  # priority_count > 3
        base_policy["ComputeLimits"]["MaximumCapacityUnits"] = int(maximum_cluster_size *
                                                                   excess_maximum_cluster_size_rate)
    return base_policy


@exception_handler
def check_or_create_table(dynamodb_resource, table_name):
    """
    检查DynamoDB表是否存在，如果不存在，则创建。

    :param dynamodb_resource: boto3 DynamoDB资源
    :param table_name: DynamoDB表名
    """
    try:
        table = dynamodb_resource.create_table(
            TableName=table_name,
            KeySchema=[
                {
                    'AttributeName': 'Timestamp',
                    'KeyType': 'HASH'  # 分区键
                }
            ],
            AttributeDefinitions=[
                {
                    'AttributeName': 'Timestamp',
                    'AttributeType': 'N'
                }
            ],
            ProvisionedThroughput={
                'ReadCapacityUnits': 1,
                'WriteCapacityUnits': 1
            }
        )
        table.meta.client.get_waiter('table_exists').wait(TableName=table_name)
        logger.info(f"Table {table_name} created successfully.")
    except dynamodb_resource.meta.client.exceptions.ResourceInUseException:
        logger.info(f"Table {table_name} already exists.")


def display_applications_table(apps):
    """
    Display the applications in a table format without the ones that are FINISHED.
    """
    table = PrettyTable()
    table.field_names = ["App Name", "State", "Started Time", "Queue"]

    for app in apps:
        if app['state'] != 'FINISHED':  # Check if the state is not FINISHED
            started_time = datetime.fromtimestamp(
                app['startedTime'] / 1000).strftime('%Y-%m-%d %H:%M:%S')
            table.add_row([app['name'], app['state'],
                          started_time, app['queue']])

    print(table)


@exception_handler
def action_main():
    dynamodb_resource = boto3.resource('dynamodb')
    check_or_create_table(dynamodb_resource, 'invoke_log')

    total_virtual_cores = get_total_virtual_cores()
    current_time = int(time.time())
    time_threshold = current_time - monitor_scaling_range_seconds

    # 更新 lack_of_capacity
    global lack_of_capacity
    lack_of_capacity = check_lack_of_capacity(
        dynamodb_resource, 'invoke_log', total_virtual_cores, time_threshold)

    # 使用logger打印lack_of_capacity的值
    logger.info(f"Updated lack_of_capacity: {lack_of_capacity}")

    apps = get_applications()
    high_priority_accepted_count = sum(
        1 for app in apps if app['state'] == 'ACCEPTED' and app['queue'] == 'high_priority'
    )

    display_applications_table(apps)

    policy = create_scaling_policy(high_priority_accepted_count)
    apply_managed_scaling_policy(policy)


def schedule_main():
    """
    初始化调度器并添加main函数作为定时任务。
    """
    scheduler = BackgroundScheduler()

    # 采集数据间隔
    ssm = boto3.client("ssm")
    action_interval_seconds = int(ssm.get_parameter(
        Name="/fleet-settings/action-interval-seconds", WithDecryption=True)["Parameter"]["Value"])

    # 添加定时任务，根据设定的时间间隔执行main函数
    scheduler.add_job(action_main, 'interval', seconds=action_interval_seconds)

    # 启动调度器
    scheduler.start()

    try:
        # 主线程继续运行，直到按Ctrl+C或发生异常
        while True:
            time.sleep(1)
    except (KeyboardInterrupt, SystemExit):
        # 关闭调度器
        scheduler.shutdown()
        logger.info("Scheduler shutdown successfully.")


if __name__ == '__main__':
    schedule_main()
