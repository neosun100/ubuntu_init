import boto3
from functools import wraps
from loguru import logger

# 配置loguru的logger
logger.add("parameter_store.log", format="{time} {level} {message}", level="DEBUG")

def exception_handler(func):
    """
    异常处理装饰器，用于捕获和记录函数中的异常。

    :param func: 被装饰的函数
    :return: 装饰器包装后的函数
    """
    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except Exception as e:
            logger.error(f"An error occurred in {func.__name__}: {e}")
            raise
    return wrapper

@exception_handler
def write_parameters_to_parameter_store(parameters):
    """
    将参数写入AWS Parameter Store。

    :param parameters: 字典，包含参数名和值
    """
    ssm_client = boto3.client('ssm')

    for parameter, value in parameters.items():
        full_parameter_name = f"/{parameter}"
        try:
            response = ssm_client.put_parameter(
                Name=full_parameter_name,
                Value=str(value),
                Type='String',
                Overwrite=True
            )
            logger.info(f"Parameter '{full_parameter_name}' written: {response}")
        except boto3.exceptions.Boto3Error as e:  # 捕获boto3可能抛出的异常
            logger.error(f"Failed to write parameter '{full_parameter_name}': {e}")
            raise

def main():
    """
    主函数，用于定义参数并调用写入函数。
    """
		# 参数及其初始值
    parameters = {
        'fleet-settings/minimum-cluster-size': 128,
        'fleet-settings/maximum-cluster-size': 512,
        'fleet-settings/maximum-core-nodes-in-the-cluster': 128,
        'fleet-settings/maximum-on-demand-instances-in-the-cluster': 128,
        'fleet-settings/core-timeout': 60,
        'fleet-settings/task-timeout': 15,
        'fleet-settings/high-priority-queues': 'high',
        'fleet-settings/high-priority-queues-pending-num-scaling': 3,
        'fleet-settings/excess-maximum-cluster-size-rate': 1.5,
        'fleet-settings/standard-maximum-cluster-size-rate': 0.5,
        'fleet-settings/on-demand-step-size': 0.2,
        'fleet-settings/monitor-interval-seconds': 20,
        'fleet-settings/action-interval-seconds': 20,
        'fleet-settings/monitor-scaling-range-seconds': 600
    }

    write_parameters_to_parameter_store(parameters)

if __name__ == '__main__':
    main()
