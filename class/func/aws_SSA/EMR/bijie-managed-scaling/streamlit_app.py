import streamlit as st
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import matplotlib as mpl
mpl.rcParams['figure.dpi'] = 120  # 例如，设置全局DPI为300
# 设置页面标题
st.title('YARN Metrics Visualization')

# 设置自动刷新间隔（以秒为单位）
refresh_interval = st.slider('Refresh interval (seconds)', 5, 60, 10)


@st.cache(ttl=refresh_interval, allow_output_mutation=True)
def load_data(csv_file):
    return pd.read_csv(csv_file)

# 绘制图表


def plot_data(df):
    plt.xkcd()
    fig, ax1 = plt.subplots(figsize=(21, 9),dpi=120)  # 增加高度以适应外部图例

    # 计算柱状图的位置
    bar_width = 0.4
    index = np.arange(len(df))

    # 绘制并排的柱状图
    ax1.bar(index - 1.5*bar_width, df['LowPriorityAcceptedCount'], bar_width, label='Low Priority Accepted')
    ax1.bar(index - 0.5*bar_width, df['LowPriorityRunningCount'], bar_width, label='Low Priority Running')
    ax1.bar(index + 0.5*bar_width, df['HighPriorityAcceptedCount'], bar_width, label='High Priority Accepted')
    ax1.bar(index + 1.5*bar_width, df['HighPriorityRunningCount'], bar_width, label='High Priority Running')

    # 设置X轴的时间格式仅为小时和分钟
    ax1.set_xticks(index)
    ax1.tick_params(axis='x', labelsize=10)  # 设置X轴刻度标签的字体大小为10
    ax1.set_xticklabels(df['Timestamp'].dt.strftime('%H:%M'), rotation=45)
    ax1.set_xlabel('Time (Hour:Minute)')
    ax1.set_ylabel('Accepted Count')

    # 创建另一个坐标轴用于折线图
    ax2 = ax1.twinx()
    ax2.plot(index, df['TotalVirtualCores'], 'r-o', label='Total Virtual Cores')  # 红色圆点实线
    ax2.plot(index, df['MaximumCapacityUnits'], 'g--x', label='Maximum Capacity Units')  # 绿色叉号虚线
    ax2.plot(index, df['MaximumOnDemandCapacityUnits'], 'b^', label='Maximum On Demand Capacity Units')  # 蓝色三角点虚线
    ax2.set_ylabel('Units and Cores', color='r')
    ax2.tick_params(axis='y', labelcolor='r')



    # 设置图表布局，为下方图例留出空间
    fig.tight_layout(rect=[0, 0.1, 1, 0.95])

    # 将图例放在图表下方
    ax1.legend(loc='upper center', bbox_to_anchor=(0.5, -0.15), ncol=2)
    ax2.legend(loc='upper center', bbox_to_anchor=(0.5, -0.25), ncol=2)

    # 在图表的最上方增加标题
    plt.suptitle('YARN and EMR Metrics Visualization')

    return fig
# 主程序


def main():
    # 加载数据
    df = load_data(csv_file='yarn_data.csv')

    # 转换时间戳为datetime，并确保时间范围正确
    df['Timestamp'] = pd.to_datetime(df['Timestamp'])

    # 确认时间范围
    print("最早时间点:", df['Timestamp'].min())
    print("最晚时间点:", df['Timestamp'].max())

    # 创建时间范围选择列表
    time_options = df['Timestamp'].dt.strftime('%Y-%m-%d %H:%M').unique().tolist()
    start_time, end_time = st.select_slider("Select Time Range", 
                                            options=time_options, 
                                            value=(time_options[0], time_options[-1]))

    # 根据时间范围过滤数据
    filtered_df = df[(df['Timestamp'] >= pd.to_datetime(start_time)) & (df['Timestamp'] <= pd.to_datetime(end_time))]

    # 绘制图表
    fig = plot_data(filtered_df)
    st.pyplot(fig, use_container_width=True)

    # 自动刷新
    st.button('Refresh')




if __name__ == '__main__':
    main()
