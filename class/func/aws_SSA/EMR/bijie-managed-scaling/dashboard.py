import requests
import pandas as pd
from datetime import datetime
from apscheduler.schedulers.background import BackgroundScheduler
import time
import boto3
from functools import wraps
from loguru import logger

# YARN ResourceManager URL
yarn_rm_url = 'http://10.68.1.77:8088'
# 集群ID
CLUSTER_ID = 'j-BWBLFIA6FR6L'
csv_file = 'yarn_data.csv'

# Configure logger
logger.add("yarn_data_collection.log", rotation="1 week")

# 创建一个EMR客户端
emr = boto3.client('emr')


def exception_handler(func):
    """
    Decorator to handle exceptions within functions and log them.

    :param func: The function to be decorated.
    :return: The wrapped function.
    """
    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except Exception as e:
            logger.error(f"An error occurred in {func.__name__}: {e}")
            raise
    return wrapper

@exception_handler
def collect_and_save_data():
    """
    Collect data from YARN ResourceManager and save it to a CSV file.
    """
    try:
        # Get cluster metrics
        response = requests.get(f'{yarn_rm_url}/ws/v1/cluster/metrics')
        if response.status_code == 200:
            metrics_data = response.json()
            total_virtual_cores = metrics_data['clusterMetrics']['totalVirtualCores']
        else:
            total_virtual_cores = None

        # Get application list
        response = requests.get(f'{yarn_rm_url}/ws/v1/cluster/apps')
        if response.status_code == 200:
            apps_data = response.json().get('apps', {}).get('app', [])
            low_priority_accepted_count = sum(
                1 for app in apps_data if app['state'] == 'ACCEPTED' and app['queue'] == 'low_priority')
            high_priority_accepted_count = sum(
                1 for app in apps_data if app['state'] == 'ACCEPTED' and app['queue'] == 'high_priority')
            low_priority_running_count = sum(
                1 for app in apps_data if app['state'] == 'RUNNING' and app['queue'] == 'low_priority')
            high_priority_running_count = sum(
                1 for app in apps_data if app['state'] == 'RUNNING' and app['queue'] == 'high_priority')
        else:
            low_priority_accepted_count = high_priority_accepted_count = low_priority_running_count = high_priority_running_count = 0


        # 获取EMR托管扩展策略
        response = emr.get_managed_scaling_policy(ClusterId=CLUSTER_ID)
        compute_limits = response.get('ManagedScalingPolicy', {}).get('ComputeLimits', {})
        maximum_capacity_units = compute_limits.get('MaximumCapacityUnits', 0)
        maximum_on_demand_capacity_units = compute_limits.get('MaximumOnDemandCapacityUnits', 0)        

        # Create a data row
        data = {
            'Timestamp': [datetime.now().strftime('%Y-%m-%d %H:%M:%S')],
            'TotalVirtualCores': [total_virtual_cores],
            'LowPriorityAcceptedCount': [low_priority_accepted_count],
            'HighPriorityAcceptedCount': [high_priority_accepted_count],
            'LowPriorityRunningCount': [low_priority_running_count],
            'HighPriorityRunningCount': [high_priority_running_count],
            'MaximumCapacityUnits': [maximum_capacity_units],  # 新增字段
            'MaximumOnDemandCapacityUnits': [maximum_on_demand_capacity_units]  # 新增字段
        }

        # Write data to CSV
        df = pd.DataFrame(data)
        df.to_csv(csv_file, mode='a', index=False, header=not pd.io.common.file_exists(csv_file))
        logger.info("Data collected and saved to CSV.")
    except Exception as e:
        logger.error(f"Failed to collect or save data: {e}")
        raise


@exception_handler
def schedule_collection():
    """
    Schedule the data collection every 10 seconds and run it in the background.
    """
    scheduler = BackgroundScheduler()
    scheduler.add_job(collect_and_save_data, 'interval', seconds=60)
    scheduler.start()

    logger.info("Scheduler started. Data collection is running.")

    # Keep the main thread running
    try:
        while True:
            time.sleep(1)
    except (KeyboardInterrupt, SystemExit):
        scheduler.shutdown()
        logger.info("Scheduler has been shut down.")

if __name__ == '__main__':
    schedule_collection()
