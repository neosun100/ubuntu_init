aws emr create-cluster \
    --name "emr6.15-spark3.4.1-r7g-HA-fleets-managed-celeborn" \
    --log-uri "s3n://aws-logs-835751346093-ap-northeast-1/elasticmapreduce/" \
    --release-label "emr-6.15" \
    --service-role "arn:aws:iam::835751346093:role/EMR_DefaultRole" \
    --managed-scaling-policy '{"ComputeLimits":{"UnitType":"InstanceFleetUnits","MinimumCapacityUnits":128,"MaximumCapacityUnits":2000,"MaximumOnDemandCapacityUnits":256,"MaximumCoreCapacityUnits":256}}' \
    --termination-protected \
    --ec2-attributes '{"InstanceProfile":"EMR_EC2_DefaultRole","EmrManagedMasterSecurityGroup":"sg-04551bef3c569fb6d","EmrManagedSlaveSecurityGroup":"sg-0d3952326df49f5bc","KeyName":"tokyo-neo-visit","AdditionalMasterSecurityGroups":[],"AdditionalSlaveSecurityGroups":[],"SubnetId":"subnet-0b3c764baf2f9a153"}' \
    --applications Name=Spark \
    --configurations '[
    {
        "Classification": "hive-site",
        "Properties": {
            "hive.metastore.client.factory.class": "com.amazonaws.glue.catalog.metastore.AWSGlueDataCatalogHiveClientFactory"
        }
    },
    {
        "Classification": "spark-hive-site",
        "Properties": {
            "hive.metastore.client.factory.class": "com.amazonaws.glue.catalog.metastore.AWSGlueDataCatalogHiveClientFactory"
        }
    },
    {
        "Classification": "hadoop-env",
        "Configurations": [
            {
                "Classification": "export",
                "Configurations": [],
                "Properties": {
                    "JAVA_HOME": "/usr/lib/jvm/jre-17"
                }
            }
        ],
        "Properties": {}
    },
    {
        "Classification": "spark-env",
        "Configurations": [
            {
                "Classification": "export",
                "Configurations": [],
                "Properties": {
                    "JAVA_HOME": "/usr/lib/jvm/jre-17"
                }
            }
        ],
        "Properties": {}
    }
]' \
    --instance-fleets '[{
    "Name": "核心节点",
    "InstanceFleetType": "CORE",
    "TargetSpotCapacity": 0,
    "TargetOnDemandCapacity": 256,
    "LaunchSpecifications": {
        "OnDemandSpecification": {
            "AllocationStrategy": "LOWEST_PRICE"
        }
    },
    "InstanceTypeConfigs": [{
        "WeightedCapacity": 64,
        "EbsConfiguration": {
            "EbsBlockDeviceConfigs": [{
                "VolumeSpecification": {
                    "VolumeType": "st1",
                    "SizeInGB": 500
                }
            }]
        },
        "BidPriceAsPercentageOfOnDemandPrice": 100,
        "InstanceType": "r7g.16xlarge"
    }, {
        "WeightedCapacity": 48,
        "EbsConfiguration": {
            "EbsBlockDeviceConfigs": [{
                "VolumeSpecification": {
                    "VolumeType": "st1",
                    "SizeInGB": 500
                }
            }]
        },
        "BidPriceAsPercentageOfOnDemandPrice": 100,
        "InstanceType": "r7g.12xlarge"
    }, {
        "WeightedCapacity": 32,
        "EbsConfiguration": {
            "EbsBlockDeviceConfigs": [{
                "VolumeSpecification": {
                    "VolumeType": "st1",
                    "SizeInGB": 500
                }
            }]
        },
        "BidPriceAsPercentageOfOnDemandPrice": 100,
        "InstanceType": "r7g.8xlarge"
    }, {
        "WeightedCapacity": 64,
        "EbsConfiguration": {},
        "BidPriceAsPercentageOfOnDemandPrice": 100,
        "InstanceType": "r7gd.16xlarge"
    }, {
        "WeightedCapacity": 48,
        "EbsConfiguration": {},
        "BidPriceAsPercentageOfOnDemandPrice": 100,
        "InstanceType": "r7gd.12xlarge"
    }]
}]' \
    --scale-down-behavior "TERMINATE_AT_TASK_COMPLETION" \
    --auto-termination-policy '{"IdleTimeout":14400}' \
    --os-release-label "2.0.20231116.0" \
    --region "ap-northeast-1" 
