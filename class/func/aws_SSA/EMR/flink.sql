CREATE TABLE t3(
uuid string,
name string,
ts TIMESTAMP(3),
logday VARCHAR(255),
hh VARCHAR(255)
)PARTITIONED BY (`logday`,`hh`)
WITH (
  'connector' = 'hudi',
  'path' = 's3a://app-util-hudi/test-demo-data-2',
  'table.type' = 'COPY_ON_WRITE',
  'write.precombine.field' = 'ts',
  'write.operation' = 'upsert',
  'hoodie.datasource.write.recordkey.field' = 'uuid',
  'hive_sync.enable' = 'true',
  'hive_sync.table' = 'test-demo-data-2',
  'hive_sync.mode' = 'HMS',
  'hive_sync.use_jdbc' = 'false',
  'hive_sync.username' = 'hadoop',
  'hive_sync.partition_fields' = 'logday,hh',
  'hive_sync.partition_extractor_class' = 'org.apache.hudi.hive.MultiPartKeysValueExtractor'
);