aws emr create-cluster \
    --name "emr-6.10.1-spark3.3.1" \
    --log-uri "s3n://aws-logs-835751346093-us-east-1/elasticmapreduce/" \
    --release-label "emr-6.10.1" \
    --service-role "arn:aws:iam::835751346093:role/EMR_DefaultRole" \
    --managed-scaling-policy '{
    "ComputeLimits": {
        "UnitType": "InstanceFleetUnits",
        "MinimumCapacityUnits": 10,
        "MaximumCapacityUnits": 100,
        "MaximumOnDemandCapacityUnits": 10,
        "MaximumCoreCapacityUnits": 20
    }
}' \
    --ec2-attributes '{
    "InstanceProfile": "EMR_EC2_DefaultRole",
    "EmrManagedMasterSecurityGroup": "sg-0708b2efbc03b7d46",
    "EmrManagedSlaveSecurityGroup": "sg-0708b2efbc03b7d46",
    "KeyName": "visit-virginia-key",
    "SubnetId": "subnet-019c68e2f66977e0e"
}' \
    --applications Name=Spark \
    --configurations '[{
    "Classification": "hive-site",
    "Properties": {
        "hive.metastore.client.factory.class": "com.amazonaws.glue.catalog.metastore.AWSGlueDataCatalogHiveClientFactory"
    }
}, {
    "Classification": "spark-hive-site",
    "Properties": {
        "hive.metastore.client.factory.class": "com.amazonaws.glue.catalog.metastore.AWSGlueDataCatalogHiveClientFactory"
    }
}]' \
    --instance-fleets '[{
    "Name": "主节点",
    "InstanceFleetType": "MASTER",
    "TargetSpotCapacity": 0,
    "TargetOnDemandCapacity": 1,
    "LaunchSpecifications": {
        "OnDemandSpecification": {
            "AllocationStrategy": "LOWEST_PRICE"
        }
    },
    "InstanceTypeConfigs": [{
        "WeightedCapacity": 1,
        "EbsConfiguration": {
            "EbsBlockDeviceConfigs": [{
                "VolumeSpecification": {
                    "VolumeType": "gp3",
                    "SizeInGB": 512
                }
            }]
        },
        "BidPriceAsPercentageOfOnDemandPrice": 100,
        "InstanceType": "r7g.4xlarge"
    }]
}, {
    "Name": "任务节点 - 1",
    "InstanceFleetType": "TASK",
    "TargetSpotCapacity": 80,
    "TargetOnDemandCapacity": 0,
    "LaunchSpecifications": {
        "SpotSpecification": {
            "TimeoutDurationMinutes": 60,
            "TimeoutAction": "SWITCH_TO_ON_DEMAND",
            "AllocationStrategy": "PRICE_CAPACITY_OPTIMIZED"
        },
        "OnDemandSpecification": {
            "AllocationStrategy": "LOWEST_PRICE"
        }
    },
    "InstanceTypeConfigs": [{
            "WeightedCapacity": 48,
            "EbsConfiguration": {
                "EbsBlockDeviceConfigs": [{
                    "VolumeSpecification": {
                        "VolumeType": "gp3",
                        "SizeInGB": 64
                    }
                }]
            },
            "BidPriceAsPercentageOfOnDemandPrice": 100,
            "InstanceType": "r5.24xlarge"
        }, {
            "WeightedCapacity": 64,
            "EbsConfiguration": {
                "EbsBlockDeviceConfigs": [{
                    "VolumeSpecification": {
                        "VolumeType": "gp3",
                        "SizeInGB": 64
                    }
                }]
            },
            "BidPriceAsPercentageOfOnDemandPrice": 100,
            "InstanceType": "r5.16xlarge"
        }, {
            "WeightedCapacity": 32,
            "EbsConfiguration": {
                "EbsBlockDeviceConfigs": [{
                    "VolumeSpecification": {
                        "VolumeType": "gp3",
                        "SizeInGB": 64
                    }
                }]
            },
            "BidPriceAsPercentageOfOnDemandPrice": 100,
            "InstanceType": "r5.12xlarge"
        }, {
            "WeightedCapacity": 16,
            "EbsConfiguration": {
                "EbsBlockDeviceConfigs": [{
                    "VolumeSpecification": {
                        "VolumeType": "gp3",
                        "SizeInGB": 64
                    }
                }]
            },
            "BidPriceAsPercentageOfOnDemandPrice": 100,
            "InstanceType": "r5.8xlarge"
        }, {
            "WeightedCapacity": 8,
            "EbsConfiguration": {
                "EbsBlockDeviceConfigs": [{
                    "VolumeSpecification": {
                        "VolumeType": "gp3",
                        "SizeInGB": 64
                    }
                }]
            },
            "BidPriceAsPercentageOfOnDemandPrice": 100,
            "InstanceType": "r5a.24xlarge"
        },
        {
            "WeightedCapacity": 8,
            "EbsConfiguration": {
                "EbsBlockDeviceConfigs": [{
                    "VolumeSpecification": {
                        "VolumeType": "gp3",
                        "SizeInGB": 64
                    }
                }]
            },
            "BidPriceAsPercentageOfOnDemandPrice": 100,
            "InstanceType": "r5a.16xlarge"
        }, {
            "WeightedCapacity": 8,
            "EbsConfiguration": {
                "EbsBlockDeviceConfigs": [{
                    "VolumeSpecification": {
                        "VolumeType": "gp3",
                        "SizeInGB": 64
                    }
                }]
            },
            "BidPriceAsPercentageOfOnDemandPrice": 100,
            "InstanceType": "r5a.12xlarge"
        }, {
            "WeightedCapacity": 8,
            "EbsConfiguration": {
                "EbsBlockDeviceConfigs": [{
                    "VolumeSpecification": {
                        "VolumeType": "gp3",
                        "SizeInGB": 64
                    }
                }]
            },
            "BidPriceAsPercentageOfOnDemandPrice": 100,
            "InstanceType": "r5a.8xlarge"
        },
        {
            "WeightedCapacity": 8,
            "EbsConfiguration": {
                "EbsBlockDeviceConfigs": [{
                    "VolumeSpecification": {
                        "VolumeType": "gp3",
                        "SizeInGB": 64
                    }
                }]
            },
            "BidPriceAsPercentageOfOnDemandPrice": 100,
            "InstanceType": "r5ad.24xlarge"
        },
        {
            "WeightedCapacity": 8,
            "EbsConfiguration": {
                "EbsBlockDeviceConfigs": [{
                    "VolumeSpecification": {
                        "VolumeType": "gp3",
                        "SizeInGB": 64
                    }
                }]
            },
            "BidPriceAsPercentageOfOnDemandPrice": 100,
            "InstanceType": "r5ad.16xlarge"
        },
        {
            "WeightedCapacity": 8,
            "EbsConfiguration": {
                "EbsBlockDeviceConfigs": [{
                    "VolumeSpecification": {
                        "VolumeType": "gp3",
                        "SizeInGB": 64
                    }
                }]
            },
            "BidPriceAsPercentageOfOnDemandPrice": 100,
            "InstanceType": "r5ad.12xlarge"
        },
        {
            "WeightedCapacity": 8,
            "EbsConfiguration": {
                "EbsBlockDeviceConfigs": [{
                    "VolumeSpecification": {
                        "VolumeType": "gp3",
                        "SizeInGB": 64
                    }
                }]
            },
            "BidPriceAsPercentageOfOnDemandPrice": 100,
            "InstanceType": "r5ad.8xlarge"
        },
        {
            "WeightedCapacity": 8,
            "EbsConfiguration": {
                "EbsBlockDeviceConfigs": [{
                    "VolumeSpecification": {
                        "VolumeType": "gp3",
                        "SizeInGB": 64
                    }
                }]
            },
            "BidPriceAsPercentageOfOnDemandPrice": 100,
            "InstanceType": "r5d.24xlarge"
        },
        {
            "WeightedCapacity": 8,
            "EbsConfiguration": {
                "EbsBlockDeviceConfigs": [{
                    "VolumeSpecification": {
                        "VolumeType": "gp3",
                        "SizeInGB": 64
                    }
                }]
            },
            "BidPriceAsPercentageOfOnDemandPrice": 100,
            "InstanceType": "r5d.16xlarge"
        },
        {
            "WeightedCapacity": 8,
            "EbsConfiguration": {
                "EbsBlockDeviceConfigs": [{
                    "VolumeSpecification": {
                        "VolumeType": "gp3",
                        "SizeInGB": 64
                    }
                }]
            },
            "BidPriceAsPercentageOfOnDemandPrice": 100,
            "InstanceType": "r5d.12xlarge"
        },
        {
            "WeightedCapacity": 8,
            "EbsConfiguration": {
                "EbsBlockDeviceConfigs": [{
                    "VolumeSpecification": {
                        "VolumeType": "gp3",
                        "SizeInGB": 64
                    }
                }]
            },
            "BidPriceAsPercentageOfOnDemandPrice": 100,
            "InstanceType": "r5d.8xlarge"
        },
        {
            "WeightedCapacity": 8,
            "EbsConfiguration": {
                "EbsBlockDeviceConfigs": [{
                    "VolumeSpecification": {
                        "VolumeType": "gp3",
                        "SizeInGB": 64
                    }
                }]
            },
            "BidPriceAsPercentageOfOnDemandPrice": 100,
            "InstanceType": "r7g.16xlarge"
        },
        {
            "WeightedCapacity": 8,
            "EbsConfiguration": {
                "EbsBlockDeviceConfigs": [{
                    "VolumeSpecification": {
                        "VolumeType": "gp3",
                        "SizeInGB": 64
                    }
                }]
            },
            "BidPriceAsPercentageOfOnDemandPrice": 100,
            "InstanceType": "r7g.12xlarge"
        },
        {
            "WeightedCapacity": 8,
            "EbsConfiguration": {
                "EbsBlockDeviceConfigs": [{
                    "VolumeSpecification": {
                        "VolumeType": "gp3",
                        "SizeInGB": 64
                    }
                }]
            },
            "BidPriceAsPercentageOfOnDemandPrice": 100,
            "InstanceType": "r7g.8xlarge"
        },
        {
            "WeightedCapacity": 8,
            "EbsConfiguration": {
                "EbsBlockDeviceConfigs": [{
                    "VolumeSpecification": {
                        "VolumeType": "gp3",
                        "SizeInGB": 64
                    }
                }]
            },
            "BidPriceAsPercentageOfOnDemandPrice": 100,
            "InstanceType": "r7gd.16xlarge"
        },
        {
            "WeightedCapacity": 8,
            "EbsConfiguration": {
                "EbsBlockDeviceConfigs": [{
                    "VolumeSpecification": {
                        "VolumeType": "gp3",
                        "SizeInGB": 64
                    }
                }]
            },
            "BidPriceAsPercentageOfOnDemandPrice": 100,
            "InstanceType": "r7gd.12xlarge"
        },
        {
            "WeightedCapacity": 8,
            "EbsConfiguration": {
                "EbsBlockDeviceConfigs": [{
                    "VolumeSpecification": {
                        "VolumeType": "gp3",
                        "SizeInGB": 64
                    }
                }]
            },
            "BidPriceAsPercentageOfOnDemandPrice": 100,
            "InstanceType": "r7gd.8xlarge"
        },
        {
            "WeightedCapacity": 8,
            "EbsConfiguration": {
                "EbsBlockDeviceConfigs": [{
                    "VolumeSpecification": {
                        "VolumeType": "gp3",
                        "SizeInGB": 64
                    }
                }]
            },
            "BidPriceAsPercentageOfOnDemandPrice": 100,
            "InstanceType": "r6g.16xlarge"
        },
        {
            "WeightedCapacity": 8,
            "EbsConfiguration": {
                "EbsBlockDeviceConfigs": [{
                    "VolumeSpecification": {
                        "VolumeType": "gp3",
                        "SizeInGB": 64
                    }
                }]
            },
            "BidPriceAsPercentageOfOnDemandPrice": 100,
            "InstanceType": "r6g.12xlarge"
        },
        {
            "WeightedCapacity": 8,
            "EbsConfiguration": {
                "EbsBlockDeviceConfigs": [{
                    "VolumeSpecification": {
                        "VolumeType": "gp3",
                        "SizeInGB": 64
                    }
                }]
            },
            "BidPriceAsPercentageOfOnDemandPrice": 100,
            "InstanceType": "r6g.8xlarge"
        },
        {
            "WeightedCapacity": 8,
            "EbsConfiguration": {
                "EbsBlockDeviceConfigs": [{
                    "VolumeSpecification": {
                        "VolumeType": "gp3",
                        "SizeInGB": 64
                    }
                }]
            },
            "BidPriceAsPercentageOfOnDemandPrice": 100,
            "InstanceType": "r6gd.16xlarge"
        },
        {
            "WeightedCapacity": 8,
            "EbsConfiguration": {
                "EbsBlockDeviceConfigs": [{
                    "VolumeSpecification": {
                        "VolumeType": "gp3",
                        "SizeInGB": 64
                    }
                }]
            },
            "BidPriceAsPercentageOfOnDemandPrice": 100,
            "InstanceType": "r6gd.12xlarge"
        },
        {
            "WeightedCapacity": 8,
            "EbsConfiguration": {
                "EbsBlockDeviceConfigs": [{
                    "VolumeSpecification": {
                        "VolumeType": "gp3",
                        "SizeInGB": 64
                    }
                }]
            },
            "BidPriceAsPercentageOfOnDemandPrice": 100,
            "InstanceType": "r5n.24xlarge"
        },
        {
            "WeightedCapacity": 8,
            "EbsConfiguration": {
                "EbsBlockDeviceConfigs": [{
                    "VolumeSpecification": {
                        "VolumeType": "gp3",
                        "SizeInGB": 64
                    }
                }]
            },
            "BidPriceAsPercentageOfOnDemandPrice": 100,
            "InstanceType": "r5n.16xlarge"
        },
        {
            "WeightedCapacity": 8,
            "EbsConfiguration": {
                "EbsBlockDeviceConfigs": [{
                    "VolumeSpecification": {
                        "VolumeType": "gp3",
                        "SizeInGB": 64
                    }
                }]
            },
            "BidPriceAsPercentageOfOnDemandPrice": 100,
            "InstanceType": "r5n.12xlarge"
        }
    ]
}, {
    "Name": "核心节点",
    "InstanceFleetType": "CORE",
    "TargetSpotCapacity": 10,
    "TargetOnDemandCapacity": 10,
    "LaunchSpecifications": {
        "SpotSpecification": {
            "TimeoutDurationMinutes": 5,
            "TimeoutAction": "SWITCH_TO_ON_DEMAND",
            "AllocationStrategy": "PRICE_CAPACITY_OPTIMIZED"
        },
        "OnDemandSpecification": {
            "AllocationStrategy": "LOWEST_PRICE"
        }
    },
    "InstanceTypeConfigs": [{
            "WeightedCapacity": 64,
            "EbsConfiguration": {
                "EbsBlockDeviceConfigs": [{
                    "VolumeSpecification": {
                        "VolumeType": "gp3",
                        "SizeInGB": 512
                    }
                }]
            },
            "BidPriceAsPercentageOfOnDemandPrice": 100,
            "InstanceType": "r7g.16xlarge"
        }, {
            "WeightedCapacity": 48,
            "EbsConfiguration": {
                "EbsBlockDeviceConfigs": [{
                    "VolumeSpecification": {
                        "VolumeType": "gp3",
                        "SizeInGB": 512
                    }
                }]
            },
            "BidPriceAsPercentageOfOnDemandPrice": 100,
            "InstanceType": "r7g.12xlarge"
        }, {
            "WeightedCapacity": 32,
            "EbsConfiguration": {
                "EbsBlockDeviceConfigs": [{
                    "VolumeSpecification": {
                        "VolumeType": "gp3",
                        "SizeInGB": 512
                    }
                }]
            },
            "BidPriceAsPercentageOfOnDemandPrice": 100,
            "InstanceType": "r7g.8xlarge"
        }, {
            "WeightedCapacity": 16,
            "EbsConfiguration": {
                "EbsBlockDeviceConfigs": [{
                    "VolumeSpecification": {
                        "VolumeType": "gp3",
                        "SizeInGB": 512
                    }
                }]
            },
            "BidPriceAsPercentageOfOnDemandPrice": 100,
            "InstanceType": "m7g.16xlarge"
        }, {
            "WeightedCapacity": 16,
            "EbsConfiguration": {
                "EbsBlockDeviceConfigs": [{
                    "VolumeSpecification": {
                        "VolumeType": "gp3",
                        "SizeInGB": 512
                    }
                }]
            },
            "BidPriceAsPercentageOfOnDemandPrice": 100,
            "InstanceType": "m7g.12xlarge"
        },
        {
            "WeightedCapacity": 16,
            "EbsConfiguration": {
                "EbsBlockDeviceConfigs": [{
                    "VolumeSpecification": {
                        "VolumeType": "gp3",
                        "SizeInGB": 512
                    }
                }]
            },
            "BidPriceAsPercentageOfOnDemandPrice": 100,
            "InstanceType": "m7g.8xlarge"
        }
    ]
}]' \
    --scale-down-behavior "TERMINATE_AT_TASK_COMPLETION" \
    --ebs-root-volume-size "100" \
    --auto-termination-policy '{"IdleTimeout":3600}' \
    --region "us-east-1"



























aws emr create-cluster \
 --name "emr-6.10.1-spark3.3.1" \
 --log-uri "s3n://aws-logs-835751346093-us-east-1/elasticmapreduce/" \
 --release-label "emr-6.10.1" \
 --service-role "arn:aws:iam::835751346093:role/EMR_DefaultRole" \
 --managed-scaling-policy '{"ComputeLimits":{"UnitType":"InstanceFleetUnits","MinimumCapacityUnits":10,"MaximumCapacityUnits":100,"MaximumOnDemandCapacityUnits":10,"MaximumCoreCapacityUnits":20}}' \
 --ec2-attributes '{"InstanceProfile":"EMR_EC2_DefaultRole","EmrManagedMasterSecurityGroup":"sg-0708b2efbc03b7d46","EmrManagedSlaveSecurityGroup":"sg-0708b2efbc03b7d46","KeyName":"visit-virginia-key","SubnetId":"subnet-019c68e2f66977e0e"}' \
 --applications Name=Spark \
 --configurations '[{"Classification":"hive-site","Properties":{"hive.metastore.client.factory.class":"com.amazonaws.glue.catalog.metastore.AWSGlueDataCatalogHiveClientFactory"}},{"Classification":"spark-hive-site","Properties":{"hive.metastore.client.factory.class":"com.amazonaws.glue.catalog.metastore.AWSGlueDataCatalogHiveClientFactory"}}]' \
 --instance-fleets '[{"Name":"主节点","InstanceFleetType":"MASTER","TargetSpotCapacity":0,"TargetOnDemandCapacity":1,"LaunchSpecifications":{"OnDemandSpecification":{"AllocationStrategy":"LOWEST_PRICE"}},"InstanceTypeConfigs":[{"WeightedCapacity":1,"EbsConfiguration":{"EbsBlockDeviceConfigs":[{"VolumeSpecification":{"VolumeType":"gp3","SizeInGB":512}}]},"BidPriceAsPercentageOfOnDemandPrice":100,"InstanceType":"r7g.4xlarge"}]},{"Name":"任务节点 - 1","InstanceFleetType":"TASK","TargetSpotCapacity":80,"TargetOnDemandCapacity":0,"LaunchSpecifications":{"SpotSpecification":{"TimeoutDurationMinutes":60,"TimeoutAction":"SWITCH_TO_ON_DEMAND","AllocationStrategy":"PRICE_CAPACITY_OPTIMIZED"},"OnDemandSpecification":{"AllocationStrategy":"LOWEST_PRICE"}},"InstanceTypeConfigs":[{"WeightedCapacity":48,"EbsConfiguration":{"EbsBlockDeviceConfigs":[{"VolumeSpecification":{"VolumeType":"gp3","SizeInGB":64}}]},"BidPriceAsPercentageOfOnDemandPrice":100,"InstanceType":"r5.24xlarge"},{"WeightedCapacity":64,"EbsConfiguration":{"EbsBlockDeviceConfigs":[{"VolumeSpecification":{"VolumeType":"gp3","SizeInGB":64}}]},"BidPriceAsPercentageOfOnDemandPrice":100,"InstanceType":"r5.16xlarge"},{"WeightedCapacity":32,"EbsConfiguration":{"EbsBlockDeviceConfigs":[{"VolumeSpecification":{"VolumeType":"gp3","SizeInGB":64}}]},"BidPriceAsPercentageOfOnDemandPrice":100,"InstanceType":"r5.12xlarge"},{"WeightedCapacity":16,"EbsConfiguration":{"EbsBlockDeviceConfigs":[{"VolumeSpecification":{"VolumeType":"gp3","SizeInGB":64}}]},"BidPriceAsPercentageOfOnDemandPrice":100,"InstanceType":"r5.8xlarge"},{"WeightedCapacity":8,"EbsConfiguration":{"EbsBlockDeviceConfigs":[{"VolumeSpecification":{"VolumeType":"gp3","SizeInGB":64}}]},"BidPriceAsPercentageOfOnDemandPrice":100,"InstanceType":"r5a.24xlarge"},{"WeightedCapacity":8,"EbsConfiguration":{"EbsBlockDeviceConfigs":[{"VolumeSpecification":{"VolumeType":"gp3","SizeInGB":64}}]},"BidPriceAsPercentageOfOnDemandPrice":100,"InstanceType":"r5a.16xlarge"},{"WeightedCapacity":8,"EbsConfiguration":{"EbsBlockDeviceConfigs":[{"VolumeSpecification":{"VolumeType":"gp3","SizeInGB":64}}]},"BidPriceAsPercentageOfOnDemandPrice":100,"InstanceType":"r5a.12xlarge"},{"WeightedCapacity":8,"EbsConfiguration":{"EbsBlockDeviceConfigs":[{"VolumeSpecification":{"VolumeType":"gp3","SizeInGB":64}}]},"BidPriceAsPercentageOfOnDemandPrice":100,"InstanceType":"r5a.8xlarge"},{"WeightedCapacity":8,"EbsConfiguration":{"EbsBlockDeviceConfigs":[{"VolumeSpecification":{"VolumeType":"gp3","SizeInGB":64}}]},"BidPriceAsPercentageOfOnDemandPrice":100,"InstanceType":"r5ad.24xlarge"},{"WeightedCapacity":8,"EbsConfiguration":{"EbsBlockDeviceConfigs":[{"VolumeSpecification":{"VolumeType":"gp3","SizeInGB":64}}]},"BidPriceAsPercentageOfOnDemandPrice":100,"InstanceType":"r5ad.16xlarge"},{"WeightedCapacity":8,"EbsConfiguration":{"EbsBlockDeviceConfigs":[{"VolumeSpecification":{"VolumeType":"gp3","SizeInGB":64}}]},"BidPriceAsPercentageOfOnDemandPrice":100,"InstanceType":"r5ad.12xlarge"},{"WeightedCapacity":8,"EbsConfiguration":{"EbsBlockDeviceConfigs":[{"VolumeSpecification":{"VolumeType":"gp3","SizeInGB":64}}]},"BidPriceAsPercentageOfOnDemandPrice":100,"InstanceType":"r5ad.8xlarge"},{"WeightedCapacity":8,"EbsConfiguration":{"EbsBlockDeviceConfigs":[{"VolumeSpecification":{"VolumeType":"gp3","SizeInGB":64}}]},"BidPriceAsPercentageOfOnDemandPrice":100,"InstanceType":"r5d.24xlarge"},{"WeightedCapacity":8,"EbsConfiguration":{"EbsBlockDeviceConfigs":[{"VolumeSpecification":{"VolumeType":"gp3","SizeInGB":64}}]},"BidPriceAsPercentageOfOnDemandPrice":100,"InstanceType":"r5d.16xlarge"},{"WeightedCapacity":8,"EbsConfiguration":{"EbsBlockDeviceConfigs":[{"VolumeSpecification":{"VolumeType":"gp3","SizeInGB":64}}]},"BidPriceAsPercentageOfOnDemandPrice":100,"InstanceType":"r5d.12xlarge"},{"WeightedCapacity":8,"EbsConfiguration":{"EbsBlockDeviceConfigs":[{"VolumeSpecification":{"VolumeType":"gp3","SizeInGB":64}}]},"BidPriceAsPercentageOfOnDemandPrice":100,"InstanceType":"r5d.8xlarge"},{"WeightedCapacity":8,"EbsConfiguration":{"EbsBlockDeviceConfigs":[{"VolumeSpecification":{"VolumeType":"gp3","SizeInGB":64}}]},"BidPriceAsPercentageOfOnDemandPrice":100,"InstanceType":"r7g.16xlarge"},{"WeightedCapacity":8,"EbsConfiguration":{"EbsBlockDeviceConfigs":[{"VolumeSpecification":{"VolumeType":"gp3","SizeInGB":64}}]},"BidPriceAsPercentageOfOnDemandPrice":100,"InstanceType":"r7g.12xlarge"},{"WeightedCapacity":8,"EbsConfiguration":{"EbsBlockDeviceConfigs":[{"VolumeSpecification":{"VolumeType":"gp3","SizeInGB":64}}]},"BidPriceAsPercentageOfOnDemandPrice":100,"InstanceType":"r7g.8xlarge"},{"WeightedCapacity":8,"EbsConfiguration":{"EbsBlockDeviceConfigs":[{"VolumeSpecification":{"VolumeType":"gp3","SizeInGB":64}}]},"BidPriceAsPercentageOfOnDemandPrice":100,"InstanceType":"r7gd.16xlarge"},{"WeightedCapacity":8,"EbsConfiguration":{"EbsBlockDeviceConfigs":[{"VolumeSpecification":{"VolumeType":"gp3","SizeInGB":64}}]},"BidPriceAsPercentageOfOnDemandPrice":100,"InstanceType":"r7gd.12xlarge"},{"WeightedCapacity":8,"EbsConfiguration":{"EbsBlockDeviceConfigs":[{"VolumeSpecification":{"VolumeType":"gp3","SizeInGB":64}}]},"BidPriceAsPercentageOfOnDemandPrice":100,"InstanceType":"r7gd.8xlarge"},{"WeightedCapacity":8,"EbsConfiguration":{"EbsBlockDeviceConfigs":[{"VolumeSpecification":{"VolumeType":"gp3","SizeInGB":64}}]},"BidPriceAsPercentageOfOnDemandPrice":100,"InstanceType":"r6g.16xlarge"},{"WeightedCapacity":8,"EbsConfiguration":{"EbsBlockDeviceConfigs":[{"VolumeSpecification":{"VolumeType":"gp3","SizeInGB":64}}]},"BidPriceAsPercentageOfOnDemandPrice":100,"InstanceType":"r6g.12xlarge"},{"WeightedCapacity":8,"EbsConfiguration":{"EbsBlockDeviceConfigs":[{"VolumeSpecification":{"VolumeType":"gp3","SizeInGB":64}}]},"BidPriceAsPercentageOfOnDemandPrice":100,"InstanceType":"r6g.8xlarge"},{"WeightedCapacity":8,"EbsConfiguration":{"EbsBlockDeviceConfigs":[{"VolumeSpecification":{"VolumeType":"gp3","SizeInGB":64}}]},"BidPriceAsPercentageOfOnDemandPrice":100,"InstanceType":"r6gd.16xlarge"},{"WeightedCapacity":8,"EbsConfiguration":{"EbsBlockDeviceConfigs":[{"VolumeSpecification":{"VolumeType":"gp3","SizeInGB":64}}]},"BidPriceAsPercentageOfOnDemandPrice":100,"InstanceType":"r6gd.12xlarge"},{"WeightedCapacity":8,"EbsConfiguration":{"EbsBlockDeviceConfigs":[{"VolumeSpecification":{"VolumeType":"gp3","SizeInGB":64}}]},"BidPriceAsPercentageOfOnDemandPrice":100,"InstanceType":"r5n.24xlarge"},{"WeightedCapacity":8,"EbsConfiguration":{"EbsBlockDeviceConfigs":[{"VolumeSpecification":{"VolumeType":"gp3","SizeInGB":64}}]},"BidPriceAsPercentageOfOnDemandPrice":100,"InstanceType":"r5n.16xlarge"},{"WeightedCapacity":8,"EbsConfiguration":{"EbsBlockDeviceConfigs":[{"VolumeSpecification":{"VolumeType":"gp3","SizeInGB":64}}]},"BidPriceAsPercentageOfOnDemandPrice":100,"InstanceType":"r5n.12xlarge"}]},{"Name":"核心节点","InstanceFleetType":"CORE","TargetSpotCapacity":10,"TargetOnDemandCapacity":10,"LaunchSpecifications":{"SpotSpecification":{"TimeoutDurationMinutes":5,"TimeoutAction":"SWITCH_TO_ON_DEMAND","AllocationStrategy":"PRICE_CAPACITY_OPTIMIZED"},"OnDemandSpecification":{"AllocationStrategy":"LOWEST_PRICE"}},"InstanceTypeConfigs":[{"WeightedCapacity":64,"EbsConfiguration":{"EbsBlockDeviceConfigs":[{"VolumeSpecification":{"VolumeType":"gp3","SizeInGB":512}}]},"BidPriceAsPercentageOfOnDemandPrice":100,"InstanceType":"r7g.16xlarge"},{"WeightedCapacity":48,"EbsConfiguration":{"EbsBlockDeviceConfigs":[{"VolumeSpecification":{"VolumeType":"gp3","SizeInGB":512}}]},"BidPriceAsPercentageOfOnDemandPrice":100,"InstanceType":"r7g.12xlarge"},{"WeightedCapacity":32,"EbsConfiguration":{"EbsBlockDeviceConfigs":[{"VolumeSpecification":{"VolumeType":"gp3","SizeInGB":512}}]},"BidPriceAsPercentageOfOnDemandPrice":100,"InstanceType":"r7g.8xlarge"},{"WeightedCapacity":16,"EbsConfiguration":{"EbsBlockDeviceConfigs":[{"VolumeSpecification":{"VolumeType":"gp3","SizeInGB":512}}]},"BidPriceAsPercentageOfOnDemandPrice":100,"InstanceType":"m7g.16xlarge"},{"WeightedCapacity":16,"EbsConfiguration":{"EbsBlockDeviceConfigs":[{"VolumeSpecification":{"VolumeType":"gp3","SizeInGB":512}}]},"BidPriceAsPercentageOfOnDemandPrice":100,"InstanceType":"m7g.12xlarge"},{"WeightedCapacity":16,"EbsConfiguration":{"EbsBlockDeviceConfigs":[{"VolumeSpecification":{"VolumeType":"gp3","SizeInGB":512}}]},"BidPriceAsPercentageOfOnDemandPrice":100,"InstanceType":"m7g.8xlarge"}]}]' \
 --scale-down-behavior "TERMINATE_AT_TASK_COMPLETION" \
 --ebs-root-volume-size "100" \
 --auto-termination-policy '{"IdleTimeout":3600}' \
 --os-release-label "2.0.20231116.0" \
 --region "us-east-1"