
df = spark.readStream.format("hudi").option(**hudi_options).load("s3://app-util-hudi/spark/taxi_order_1").createOrReplaceTempView("cdcTable")


spark.sql("select * from   cdcTable where createTS > unix_timestamp()-70  limit 3;") \
  .writeStream \
  .trigger(processingTime='10 seconds') \
  .format("console") \
  .outputMode("append") \
  .start() \
  .awaitTermination()

beginTime = "20220720000400783"
df = spark.readStream \
  .option("hoodie.datasource.query.type", "incremental") \
  .option("hoodie.datasource.read.begin.instanttime", beginTime) \
  .format("hudi") \
  .load("s3a://app-util-hudi/spark/taxi_order_1") \
  .createOrReplaceTempView("cdcTable")


  kafkaDF.writeStream\
  .option("checkpointLocation", "/home/hadoop/checkpoint-all-multiple-2022-06-26-001/")\
  .trigger(processingTime='60 seconds')\
  .foreachBatch(df_2_hudi_multiple_table)\
  .start().awaitTermination()


  https://blog.csdn.net/qq_42578036/article/details/110122355



  tableName = "taxi_order_1"
hudi_options = {
    'table.name': tableName,
    'hoodie.datasource.write.table.name': tableName,

    'hoodie.datasource.write.table.type': 'COPY_ON_WRITE',
    'hoodie.datasource.write.operation': 'upsert',

    'hoodie.datasource.write.recordkey.field': 'id',
    'hoodie.datasource.write.partitionpath.field': 'age',
    'hoodie.datasource.write.precombine.field': 'eventTS',

    'hoodie.upsert.shuffle.parallelism': 2,
    'hoodie.insert.shuffle.parallelism': 2,
    'hoodie.bulkinsert.shuffle.parallelism': 2,
    'hoodie.delete.shuffle.parallelism': 2,

    'hoodie.datasource.hive_sync.mode': 'hms',
    'hoodie.datasource.hive_sync.auto_create_database': True,
    'hoodie.datasource.hive_sync.database': 'lake-hudi',
    'hoodie.datasource.hive_sync.table': tableName,
    
    'hoodie.datasource.hive_sync.partition_fields': 'age',
    'hoodie.datasource.hive_sync.partition_extractor_class':  "org.apache.hudi.hive.MultiPartKeysValueExtractor",
    'hoodie.datasource.write.payload.class': 'org.apache.hudi.common.model.DefaultHoodieRecordPayload',
    'hoodie.datasource.hive_sync.enable': "true",
    

    # 'hoodie.parquet.max.file.size': '120mb',
    # 'hoodie.parquet.small.file.limit': '100mb',
    # 'hoodie.copyonwrite.insert.split.size': '120k'

}

