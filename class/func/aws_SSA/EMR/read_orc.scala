val sparkSession = SparkSession
  .builder()
  .appName("PvMvToBase")
  .config("hive.exec.orc.default.stripe.size", 268435456L)
  .config("hive.exec.orc.split.strategy", "BI")  // 可以设置成以BI策略读取orc文件, 减小对hiveserver2的压力
  .enableHiveSupport()
  .getOrCreate()