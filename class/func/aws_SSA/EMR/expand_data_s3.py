from pyspark.sql import SparkSession

def expand_data(path, factors=[10, 100]):
    # 初始化Spark
    spark = SparkSession.builder.appName("Expand_Data_S3").getOrCreate()

    # 读取所有源文件
    df = spark.read.parquet(f"{path}")

    for factor in factors:
        expanded_rdd = df.rdd.flatMap(lambda x: [x]*factor)
        expanded_df = spark.createDataFrame(expanded_rdd, df.schema)
        
        # 输出路径
        output_path = f"{path}_{factor}/"
        
        # 写回S3
        expanded_df.write.parquet(output_path, mode="overwrite")

# 调用函数
path = "s3://emr-on-eks-nvme-227401510565-us-east-1/trip-data-yellow-only"
path = "s3a://virginia-nyc-tlc/trip-data-yellow-only"
expand_data(path)


from pyspark.sql import SparkSession

def expand_and_write(df, factor, output_path):
    expanded_rdd = df.rdd.flatMap(lambda x: [x]*factor)
    expanded_df = SparkSession.builder.getOrCreate().createDataFrame(expanded_rdd, df.schema)
    expanded_df.write.parquet(output_path, mode="overwrite")

def expand_data(path):
    # 初始化Spark
    spark = SparkSession.builder.appName("Expand_Data_S3").getOrCreate()

    # 读取所有源文件
    df = spark.read.parquet(path)

    # 扩张10倍并写入
    output_path_10 = f"{path}_10/"
    expand_and_write(df, 10, output_path_10)

    # 从10倍的数据读取，再扩张10倍得到100倍
    df_10 = spark.read.parquet(output_path_10)
    output_path_100 = f"{path}_100/"
    expand_and_write(df_10, 10, output_path_100)

# 调用函数
path = "s3://emr-on-eks-nvme-227401510565-us-east-1/trip-data-yellow-only"
path = "s3a://virginia-nyc-tlc/trip-data-yellow-only"
expand_data(path)


from pyspark.sql import SparkSession

def expand_data(path):
    spark = SparkSession.builder.appName("Expand_Data_S3").getOrCreate()

    # 读取原始数据
    df = spark.read.parquet(path)

    # 10倍扩张并写入
    output_path_10 = f"{path}_10"
    df_10 = df
    for _ in range(9):
        df_10 = df_10.union(df)
    df_10.write.mode("overwrite").parquet(output_path_10)

    # 使用10倍的数据，再次扩张到100倍
    output_path_100 = f"{path}_100/"
    df_10 = spark.read.parquet(output_path_10)
    for _ in range(9):
        df_10.write.mode("append").parquet(output_path_100)

# 调用函数
path = "s3://virginia-nyc-tlc/trip-data-yellow-only"
expand_data(path)