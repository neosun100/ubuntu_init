package com.binance.bdp.batch.connector.sm

// start spark-shell
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import scala.collection.mutable.ArrayBuffer

object PerformanceTest3 {
    def findMinMaxMeanStdDeviation(list: List[Double]) : (Double, Double, Double, Double) = {
        val max = list.max
        val min = list.min
        val mean = list.sum / list.length
        val stdDeviation = Math.sqrt(list.map( x => Math.pow(x - mean , 2)).sum / list.length).round
        (min, max, mean, stdDeviation)
    }

    def runAggregateQuery(session: SparkSession, path: String) : Long = {
        val startTime = java.time.Instant.now()
        session.read.parquet(path).count()
        val endTime = java.time.Instant.now()
        endTime.toEpochMilli - startTime.toEpochMilli
    }

    def runGroupByQuery(session: SparkSession, path: String, cardinality: Int): Long = {
        import session.implicits._
        val startTime = java.time.Instant.now()
        val df = session.read.parquet(path)
        cardinality match {
            case 1 => df.withColumn("Total_Amt", col("Total_Amt").cast("double")).groupBy('fare_amt).agg(avg('total_amt)).collect()
            case 2 => df.withColumn("Total_Amt", col("Total_Amt").cast("double")).groupBy('fare_amt, year('trip_pickup_datetime).as("trip_year")).agg(avg('total_amt)).collect()
            case 3 => df.withColumn("Total_Amt", col("Total_Amt").cast("double")).groupBy('fare_amt, year('trip_pickup_datetime).as("trip_year"), round('trip_distance).as("trip_distance")).agg(avg('total_amt)).collect()
            case 4 => df.withColumn("Total_Amt", col("Total_Amt").cast("double")).groupBy('fare_amt, year('trip_pickup_datetime).as("trip_year"), round('trip_distance).as("trip_distance")).agg(avg('total_amt)).where('trip_year.isNotNull).orderBy('trip_year).collect()
        }
        val endTime = java.time.Instant.now()
        endTime.toEpochMilli - startTime.toEpochMilli
    }

    def runExperiment (path: String): Unit = {
        val spark = SparkSession.builder().appName("PerformanceTest").enableHiveSupport().getOrCreate()
        spark.sparkContext.setLogLevel("ERROR")
        val outputList = new ArrayBuffer[String]()
        outputList.append(s"Experiment start at: ${java.time.Instant.now()}")
        outputList.append("\n")
        val noOfTrail = 20
        val aggregateQueryTime = new scala.collection.mutable.ArrayBuffer[Double]()
        outputList.append("Starting runAggregateQuery experiment")
        outputList.append("\n")
        val k: Unit = (0 to noOfTrail).toList.foreach { _ =>
            aggregateQueryTime += runAggregateQuery(spark, path)
        }
        outputList.append(s"Runtime for $noOfTrail trials: ${aggregateQueryTime.toList}")
        outputList.append("\n")
        outputList.append(s"Stats after $noOfTrail times: ${findMinMaxMeanStdDeviation(aggregateQueryTime.toList)}")
        outputList.append("\n")
        aggregateQueryTime.clear()
        outputList.append("Starting runGroupByQuery with Card 1 experiment")
        outputList.append("\n")
        (0 to noOfTrail).toList.foreach { _ =>
            aggregateQueryTime += runGroupByQuery(spark, path, 1)
        }
        outputList.append(s"Runtime for $noOfTrail trials: ${aggregateQueryTime.toList}")
        outputList.append("\n")
        outputList.append(s"Stats after $noOfTrail times: ${findMinMaxMeanStdDeviation(aggregateQueryTime.toList)}")
        outputList.append("\n")
        aggregateQueryTime.clear()
        outputList.append("Starting runGroupByQuery with Card 2 experiment")
        outputList.append("\n")
        (0 to noOfTrail).toList.foreach { _ =>
            aggregateQueryTime += runGroupByQuery(spark, path, 2)
        }
        outputList.append(s"Runtime for $noOfTrail trials: ${aggregateQueryTime.toList}")
        outputList.append("\n")
        outputList.append(s"Stats after $noOfTrail times: ${findMinMaxMeanStdDeviation(aggregateQueryTime.toList)}")
        outputList.append("\n")
        aggregateQueryTime.clear()
        outputList.append("Starting runGroupByQuery with Card 3 experiment")
        outputList.append("\n")
        (0 to noOfTrail).toList.foreach { _ =>
            aggregateQueryTime += runGroupByQuery(spark, path, 3)
        }
        outputList.append(s"Runtime for $noOfTrail trials: ${aggregateQueryTime.toList}")
        outputList.append("\n")
        outputList.append(s"Stats after $noOfTrail times: ${findMinMaxMeanStdDeviation(aggregateQueryTime.toList)}")
        outputList.append("\n")
        aggregateQueryTime.clear()
        outputList.append("Starting runGroupByQuery with Card 4 experiment")
        outputList.append("\n")
        (0 to noOfTrail).toList.foreach { _ =>
            aggregateQueryTime += runGroupByQuery(spark, path, 4)
        }
        outputList.append(s"Runtime for $noOfTrail trials: ${aggregateQueryTime.toList}")
        outputList.append("\n")
        outputList.append(s"Stats after $noOfTrail times: ${findMinMaxMeanStdDeviation(aggregateQueryTime.toList)}")
        outputList.append("\n")
        outputList.append(s"Experiment finished at: ${java.time.Instant.now()}")
        outputList.append("\n")
        spark.sparkContext.parallelize(outputList.toList).saveAsTextFile(s"s3://emr-cost-dev/emreks/output/aggregation_test_${java.time.Instant.now().toEpochMilli}.txt")
        aggregateQueryTime.clear()
    }

    def main(args: Array[String]): Unit = {
        val path = "s3://emr-cost-dev/emreks/performance";
        runExperiment(path)
    }
}
