import boto3
import pandas as pd


def get_s3_files(bucket_name, prefix, file_type="sql"):
    s3_resource = boto3.resource("s3")
    bucket = s3_resource.Bucket(bucket_name)
    s3_source_files = []
    for object in bucket.objects.filter(Prefix=prefix):
        if object.key.endswith(f".{file_type}"):
            s3_source_files.append({"id": os.path.basename(object.key),
                                    "sql": f"""{object.get()['Body'].read()}"""})
    return s3_source_files


sql_files = get_s3_files(bucket_name="aws-virginia-files",
                         prefix="emr_benchmark/benchmark-sql/",
                         file_type="sql")


with boto3.client('athena') as client:
    resultList = []
    for o in sql_files:
        try:
            lines = o['sql'].split("\\n")
            sql_str = ' '.join(lines[1:])[:-1]
            sql_str = sql_str.replace('\\t', ' ')

            response = client.start_query_execution(
                QueryString=sql_str,
                QueryExecutionContext={
                    'Database': 'tpcds1t'
                },
                ResultConfiguration={
                    'OutputLocation': 's3://aws-glue-assets-835751346093-us-east-1/',
                },
                WorkGroup='primary')
            _id = response['QueryExecutionId']
            response = client.get_query_execution(QueryExecutionId=_id)
            result = response["QueryExecution"]['Statistics']
            result['_id'] = o['id']
            resultList.append(result)

        except Exception as e:
            print(" error sql :", o['id'], e)

    df = pd.DataFrame(resultList)

    df.to_csv("athena.csv", header=1, index=False)
