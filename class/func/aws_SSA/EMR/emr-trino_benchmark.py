from datetime import datetime
from trino.dbapi import connect
import os
import boto3


def get_s3_files(bucket_name, prefix, file_type="sql"):
    s3_resource = boto3.resource("s3")
    bucket = s3_resource.Bucket(bucket_name)
    s3_source_files = []
    for object in bucket.objects.filter(Prefix=prefix):
        if object.key.endswith(f".{file_type}"):
            s3_source_files.append({"id": os.path.basename(object.key),
                                    "sql": f"""{object.get()['Body'].read()}"""})
    return s3_source_files


sql_files = get_s3_files(bucket_name="aws-virginia-files",
                         prefix="emr_benchmark/benchmark-sql/",
                         file_type="sql")

with connect(host="localhost",
             port=8889,
             user="hadoop",
             catalog="hive",
             schema="tpcds1t",
             ) as conn:
    with open('data.txt', 'w') as f:  # 设置文件对象
        f.write("")  # 将字符串写入文件中
    for o in sql_files:
        cur = conn.cursor()
        try:
            lines = o['sql'].split("\\n")
            sql_str = ' '.join(lines[1:])[:-1]
            sql_str = sql_str.replace('\\t', ' ')
            start = datetime.now().timestamp()
            cur.execute(f"""{sql_str}""")
            result = cur.fetchall()
            end = datetime.now().timestamp()
            msg = f"{o['id']}, ',', {end - start}"
            print(msg)
            with open('data.txt', 'a') as f:  # 设置文件对象
                f.write(f"{msg}\n")  # 将字符串写入文件中
        except Exception as e:
            print(" error sql :", o['id'], e)
