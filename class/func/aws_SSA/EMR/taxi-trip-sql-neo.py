from pyspark.sql import SparkSession
from pyspark.sql.functions import avg, col, year, round
import time
import numpy as np


def find_stats(data):
    return np.min(data), np.max(data), np.mean(data), np.std(data)


# def run_query(spark, path, cardinality):
#     start = time.time() * 1000
#     df = spark.read.parquet(path)
#     if cardinality == 1:
#         df.groupBy('fare_amt').agg(avg('total_amt')).collect()
#     elif cardinality == 2:
#         df.groupBy('fare_amt', year('trip_pickup_datetime').alias(
#             'trip_year')).agg(avg('total_amt')).collect()
#     elif cardinality == 3:
#         df.groupBy('fare_amt', year('trip_pickup_datetime').alias('trip_year'), round(
#             'trip_distance').alias('trip_distance')).agg(avg('total_amt')).collect()
#     elif cardinality == 4:
#         df.groupBy('fare_amt', year('trip_pickup_datetime').alias('trip_year'), round('trip_distance').alias(
#             'trip_distance')).agg(avg('total_amt')).filter(col('trip_year').isNotNull()).orderBy('trip_year').collect()
#     end = time.time() * 1000
#     return end - start

def run_query(spark, path, cardinality):
    start = time.time() * 1000
    df = spark.read.parquet(path)
    df.createOrReplaceTempView("neo_table")

    if cardinality == 1:
        spark.sql(
            "SELECT fare_amt, AVG(total_amt) FROM neo_table GROUP BY fare_amt").collect()
    elif cardinality == 2:
        spark.sql("SELECT fare_amt, YEAR(trip_pickup_datetime) AS trip_year, AVG(total_amt) FROM neo_table GROUP BY fare_amt, trip_year").collect()
    elif cardinality == 3:
        spark.sql("SELECT fare_amt, YEAR(trip_pickup_datetime) AS trip_year, ROUND(trip_distance) AS trip_distance, AVG(total_amt) FROM neo_table GROUP BY fare_amt, trip_year, trip_distance").collect()
    elif cardinality == 4:
        spark.sql("""
    SELECT * FROM (
        SELECT fare_amt, 
               YEAR(trip_pickup_datetime) AS trip_year, 
               ROUND(trip_distance) AS trip_distance, 
               AVG(total_amt) 
        FROM neo_table 
        GROUP BY fare_amt, trip_year, trip_distance
    ) WHERE trip_year IS NOT NULL 
    ORDER BY trip_year
""").collect()
    elif cardinality == 5:
        spark.sql("""
SELECT 
    CASE 
        WHEN cardinality = 1 THEN 'One'
        WHEN cardinality = 2 THEN 'Two'
        WHEN cardinality = 3 THEN 'Three'
        ELSE 'Four'
    END AS CardinalityLevel,
    fare_amt,
    YEAR(Trip_Pickup_DateTime) AS trip_year,
    ROUND(Trip_Distance) AS trip_distance,
    AVG(Total_Amt) AS avg_total_amt,
    SUM(Tip_Amt) AS total_tips,
    COUNT(DISTINCT vendor_name) AS unique_vendors
FROM (
    SELECT *, 
           CASE 
               WHEN Fare_Amt < 20 THEN 1
               WHEN Fare_Amt BETWEEN 20 AND 50 THEN 2
               WHEN Fare_Amt BETWEEN 50 AND 100 THEN 3
               ELSE 4
           END AS cardinality
    FROM neo_table
)
GROUP BY 
    CardinalityLevel, fare_amt, trip_year, trip_distance
HAVING 
    trip_year IS NOT NULL AND unique_vendors >= 1
ORDER BY 
    trip_year ASC, unique_vendors DESC LIMIT 10000;
""").collect()
    elif cardinality == 6:
        spark.sql("""
WITH TempTable AS (
    SELECT *,
           RANK() OVER (PARTITION BY vendor_name ORDER BY Total_Amt DESC) as rank
    FROM neo_table
),
Aggregated AS (
    SELECT 
        CASE 
            WHEN cardinality = 1 THEN 'One'
            WHEN cardinality = 2 THEN 'Two'
            WHEN cardinality = 3 THEN 'Three'
            ELSE 'Four'
        END AS CardinalityLevel,
        vendor_name,
        YEAR(Trip_Pickup_DateTime) AS trip_year,
        ROUND(Trip_Distance) AS trip_distance,
        AVG(Total_Amt) AS avg_total_amt,
        SUM(Tip_Amt) AS total_tips,
        COUNT(DISTINCT vendor_name) AS unique_vendors,
        MAX(rank) AS max_rank
    FROM (
        SELECT *, 
            CASE 
                WHEN Fare_Amt < 20 THEN 1
                WHEN Fare_Amt BETWEEN 20 AND 50 THEN 2
                WHEN Fare_Amt BETWEEN 50 AND 100 THEN 3
                ELSE 4
            END AS cardinality
        FROM TempTable
    )
    WHERE rank <= 10
    GROUP BY 
        CardinalityLevel, vendor_name, YEAR(Trip_Pickup_DateTime), ROUND(Trip_Distance)
)
SELECT Aggregated.*, TempTable.Total_Amt, TempTable.Trip_Pickup_DateTime
FROM Aggregated
JOIN TempTable
ON Aggregated.vendor_name = TempTable.vendor_name AND Aggregated.trip_year = YEAR(TempTable.Trip_Pickup_DateTime)
WHERE Aggregated.unique_vendors >= 1 AND TempTable.rank <= Aggregated.max_rank
ORDER BY 
    trip_year ASC, unique_vendors DESC
LIMIT 12000;
""").collect()

    end = time.time() * 1000
    return end - start


def run_experiment(path):
    spark = SparkSession.builder.appName(
        "PerformanceTest").enableHiveSupport().getOrCreate()
    spark.sparkContext.setLogLevel("ERROR")
    trials = 3
    times = []
    all_stats = []

    df_initialized = False  # Flag to check if df is initialized

    for i in range(1, 7):
        times = []
        for _ in range(trials):
            times.append(run_query(spark, path, i))

        min_val, max_val, mean_val, std_dev = find_stats(times)
        all_stats.append((i, min_val, max_val, mean_val, std_dev))

        newRow = spark.createDataFrame(
            [(i, float(min_val), float(max_val), float(mean_val), float(std_dev))])

        if not df_initialized:
            df = newRow
            df_initialized = True
        else:
            df = df.union(newRow)

    df.repartition(1).write.csv(
        f"s3a://virginia-nyc-tlc/trip-data-yellow-only-aggregation_test_{int(time.time() * 1000)}.csv", header=True)


if __name__ == "__main__":
    run_experiment("s3a://virginia-nyc-tlc/trip-data-yellow-only")