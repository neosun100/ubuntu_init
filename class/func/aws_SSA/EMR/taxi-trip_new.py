from pyspark.sql import SparkSession
from pyspark.sql.functions import col, year, avg, round
from math import sqrt
import time


def findMinMaxMeanStdDeviation(lst):
    _max = max(lst)
    _min = min(lst)
    _mean = sum(lst) / len(lst)
    _stdDeviation = round(
        sqrt(sum([(x - _mean) ** 2 for x in lst]) / len(lst)), 2)
    return (_min, _max, _mean, _stdDeviation)


def runAggregateQuery(session, path):
    startTime = int(time.time() * 1000)
    session.read.parquet(path).count()
    endTime = int(time.time() * 1000)
    return endTime - startTime


def runGroupByQuery(session, path, cardinality):
    startTime = int(time.time() * 1000)
    df = session.read.parquet(path)
    if cardinality == 1:
        df.withColumn("Total_Amt", col("Total_Amt").cast("double")).groupBy(
            'fare_amt').agg(avg('total_amt')).collect()
    elif cardinality == 2:
        df.withColumn("Total_Amt", col("Total_Amt").cast("double")).groupBy('fare_amt', year(
            'trip_pickup_datetime').alias("trip_year")).agg(avg('total_amt')).collect()
    elif cardinality == 3:
        df.withColumn("Total_Amt", col("Total_Amt").cast("double")).groupBy('fare_amt', year('trip_pickup_datetime').alias(
            "trip_year"), round('trip_distance').alias("trip_distance")).agg(avg('total_amt')).collect()
    elif cardinality == 4:
        df.withColumn("Total_Amt", col("Total_Amt").cast("double")).groupBy('fare_amt', year('trip_pickup_datetime').alias("trip_year"), round(
            'trip_distance').alias("trip_distance")).agg(avg('total_amt')).where(col('trip_year').isNotNull()).orderBy('trip_year').collect()
    endTime = int(time.time() * 1000)
    return endTime - startTime


def runExperiment(path):
    spark = SparkSession.builder.appName(
        "PerformanceTest").enableHiveSupport().getOrCreate()
    spark.sparkContext.setLogLevel("ERROR")
    outputList = []
    noOfTrail = 20
    aggregateQueryTime = []

    for _ in range(noOfTrail):
        aggregateQueryTime.append(runAggregateQuery(spark, path))

    for _ in range(noOfTrail):
        aggregateQueryTime.append(runGroupByQuery(spark, path, 1))

    for _ in range(noOfTrail):
        aggregateQueryTime.append(runGroupByQuery(spark, path, 2))

    for _ in range(noOfTrail):
        aggregateQueryTime.append(runGroupByQuery(spark, path, 3))

    for _ in range(noOfTrail):
        aggregateQueryTime.append(runGroupByQuery(spark, path, 4))

    spark.sparkContext.parallelize(aggregateQueryTime).saveAsTextFile(
        f"s3://emr-cost-dev/emreks/output/aggregation_test_{int(time.time() * 1000)}.txt")


if __name__ == "__main__":
    path = "s3://emr-cost-dev/emreks/performance"
    runExperiment(path)
