# 使用COW表, 虽然有写放大，但逻辑相对MOR简单,没有compaction过程
hoodie.datasource.write.table.type=COPY_ON_WRITE
# spark sql的并行度默认200，我们会调小该值，因为是多线程并行写多表，单表的数据量可控，对单个表的写入不需要太多的task，造成空跑的浪费，为了更好利用资源，会将Spark sql的默认并行度也调整为10~20.
hoodie.upsert.shuffle.parallelism=20
hoodie.insert.shuffle.parallelism=20
# 小文件的控制，为了防止更严重的写放大，文件不会设置太大，当使用COW表，同时又对摄入实时性要求高的，可以将该值调小，但带来的问题是小文件会多，需要定期做clustering
hoodie.parquet.small.file.limit=52428800
# index选择的是BLOOM,Hudi 0.10还不支持一致性Hash, 桶的个数一旦确定无法修改。后边会考虑Bucket Index(hudi 0.13后支持)
hoodie.index.type=GLOBAL_BLOOM
# DefaultHoodieRecordPayload因为要乱序入湖
hoodie.datasource.write.payload.class=org.apache.hudi.common.model.DefaultHoodieRecordPayload
# 表不设置分区，目的是为了简单，不开放业务直接查询Hudi表，所以性能可以接受
hoodie.datasource.write.keygenerator.class=org.apache.hudi.keygen.NonpartitionedKeyGenerator
hoodie.metadata.enable=true
# 暂时不需要增量读取太长时间数据,commits保留设置的比较少,retained保留5次提交active timline引用的对应版本的数据文件
hoodie.cleaner.commits.retained=5
# 每2次提交检查一次retained的是否满足设置的条件
hoodie.clean.max.commits=2
# 超过6次就archive，最小值要大于retained的值
hoodie.keep.min.commits=6
hoodie.keep.max.commits=7
# markers.type我们设定为了DIRECT，没有使用Timeline-server-based marker，当时写S3遇到Markers找不到的错误，跟社区沟通后，暂时采用了DIRECT的方式
hoodie.write.markers.type=DIRECT
# 是否表结构有变更，才同步元数据变更，否则每次提交都会更新，如果是Glue，每次都是一个新的版本(Glue表版本是有限制的)，建议设置为true
hoodie.datasource.meta_sync.condition.sync=true
# 建议开启，太多小archive file会严重影响hudi的写入性能，每次写入arhive file都会被list, s3 list是一个比较耗时操作
hoodie.archive.merge.enable=true




# auto gen schema
json_schema = spark.read.json(fdf.rdd.map(lambda p: str(p["value"]))).schema

logger_msg("the table {0}: auto gen json schema: {1}".format(table_name, str(json_schema)))
scf = fdf.select(from_json(col("value"), json_schema).alias("kdata")).select("kdata.*")

logger_msg("the table {0}: kafka source data with auto gen schema: {1}".format(table_name, getDFExampleString(scf)))



from airflow.models import DagBag

# 加载所有DAG
dag_bag = DagBag()

# 输入你想查询的DAG ID
# 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀 🚀  
# 修改成你的DAG-id
target_dag_id = 'DAG_test_hour_v2'

# 获取目标DAG
target_dag = dag_bag.get_dag(target_dag_id)

# 存储上游和下游依赖
upstream_dependencies = []
downstream_dependencies = []

# 遍历目标DAG的任务
for task in target_dag.tasks:
    upstream_dependencies.extend(list(task.upstream_task_ids))
    downstream_dependencies.extend(list(task.downstream_task_ids))

# 去重
upstream_dependencies = list(set(upstream_dependencies))
downstream_dependencies = list(set(downstream_dependencies))

# 打印依赖关系
print(f"上游依赖: {upstream_dependencies}")
print(f"下游依赖: {downstream_dependencies}")