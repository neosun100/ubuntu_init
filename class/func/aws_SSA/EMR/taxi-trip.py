from pyspark.sql import SparkSession
from pyspark.sql.functions import *
from math import sqrt

def findMinMaxMeanStdDeviation(lst):
    _max = max(lst)
    _min = min(lst)
    _mean = sum(lst) / len(lst)
    _stdDeviation = round(sqrt(sum([(x - _mean) ** 2 for x in lst]) / len(lst)))
    return (_min, _max, _mean, _stdDeviation)

def runAggregateQuery(session, path):
    startTime = int(time.time() * 1000)
    session.read.parquet(path).count()
    endTime = int(time.time() * 1000)
    return endTime - startTime

def runGroupByQuery(session, path, cardinality):
    startTime = int(time.time() * 1000)
    df = session.read.parquet(path)
    if cardinality == 1:
        df.withColumn("Total_Amt", col("Total_Amt").cast("double")).groupBy('fare_amt').agg(avg('total_amt')).collect()
    elif cardinality == 2:
        df.withColumn("Total_Amt", col("Total_Amt").cast("double")).groupBy('fare_amt', year('trip_pickup_datetime').alias("trip_year")).agg(avg('total_amt')).collect()
    elif cardinality == 3:
        df.withColumn("Total_Amt", col("Total_Amt").cast("double")).groupBy('fare_amt', year('trip_pickup_datetime').alias("trip_year"), round('trip_distance').alias("trip_distance")).agg(avg('total_amt')).collect()
    elif cardinality == 4:
        df.withColumn("Total_Amt", col("Total_Amt").cast("double")).groupBy('fare_amt', year('trip_pickup_datetime').alias("trip_year"), round('trip_distance').alias("trip_distance")).agg(avg('total_amt')).where(col('trip_year').isNotNull).orderBy('trip_year').collect()
    endTime = int(time.time() * 1000)
    return endTime - startTime

def runExperiment(path):
    spark = SparkSession.builder.appName("PerformanceTest").enableHiveSupport().getOrCreate()
    spark.sparkContext.setLogLevel("ERROR")
    outputList = []
    outputList.append(f"Experiment start at: {int(time.time() * 1000)}")
    outputList.append("\n")
    noOfTrail = 20
    aggregateQueryTime = []
    outputList.append("Starting runAggregateQuery experiment")
    outputList.append("\n")
    for _ in range(noOfTrail):
        aggregateQueryTime.append(runAggregateQuery(spark, path))
    outputList.append(f"Runtime for {noOfTrail} trials: {aggregateQueryTime}")
    outputList.append("\n")
    outputList.append(f"Stats after {noOfTrail} times: {findMinMaxMeanStdDeviation(aggregateQueryTime)}")
    outputList.append("\n")
    aggregateQueryTime.clear()
    outputList.append("Starting runGroupByQuery with Card 1 experiment")
    outputList.append("\n")
    for _ in range(noOfTrail):
        aggregateQueryTime.append(runGroupByQuery(spark, path, 1))
    outputList.append(f"Runtime for {noOfTrail} trials: {aggregateQueryTime}")
    outputList.append("\n")
    outputList.append(f"Stats after {noOfTrail} times: {findMinMaxMeanStdDeviation(aggregateQueryTime)}")
    outputList.append("\n")
    aggregateQueryTime.clear()
    outputList.append("Starting runGroupByQuery with Card 2 experiment")
    outputList.append("\n")
    for _ in range(noOfTrail):
        aggregateQueryTime.append(runGroupByQuery(spark, path, 2))
    outputList.append(f"Runtime for {noOfTrail} trials: {aggregateQueryTime}")
    outputList.append("\n")
    outputList.append(f"Stats after {noOfTrail} times: {findMinMaxMeanStdDeviation(aggregateQueryTime)}")
    outputList.append("\n")
    aggregateQueryTime.clear()
    outputList.append("Starting runGroupByQuery with Card 3 experiment")
    outputList.append("\n")
    for _ in range(noOfTrail):
        aggregateQueryTime.append(runGroupByQuery(spark, path, 3))
    outputList.append(f"Runtime for {noOfTrail} trials: {aggregateQueryTime}")
    outputList.append("\n")
    outputList.append(f"Stats after {noOfTrail} times: {findMinMaxMeanStdDeviation(aggregateQueryTime)}")
    outputList.append("\n")
    aggregateQueryTime.clear()
    outputList.append("Starting runGroupByQuery with Card 4 experiment")
    outputList.append("\n")
    for _ in range(noOfTrail):
        aggregateQueryTime.append(runGroupByQuery(spark, path, 4))
    outputList.append(f"Runtime for {noOfTrail} trials: {aggregateQueryTime}")
    outputList.append("\n")
    outputList.append(f"Stats after {noOfTrail} times: {findMinMaxMeanStdDeviation(aggregateQueryTime)}")
    outputList.append("\n")
    outputList.append(f"Experiment finished at: {int(time.time() * 1000)}")
    outputList.append("\n")
    spark.sparkContext.parallelize(outputList).saveAsTextFile(f"s3://emr-cost-dev/emreks/output/aggregation_test_{int(time.time() * 1000)}.txt")
    aggregateQueryTime.clear()

if __name__ == "__main__":
    path = "s3://emr-cost-dev/emreks/performance"
    runExperiment(path)
