from pyspark.sql import SparkSession
from pyspark.sql.types import IntegerType, FloatType, DoubleType, StringType
import random
from pyspark.sql import Row
from pyspark.sql.functions import year, month, to_timestamp, rand
from pyspark.sql import functions as F

expansion_factor = 9  # 为达到10倍，需要额外扩充9倍

# 初始化Spark
spark = SparkSession.builder \
    .appName("Data Augmentation") \
    .getOrCreate()

# 参数设置
input_path = "s3://emr-on-eks-nvme-227401510565-us-east-1/trip-data-yellow-only/*.parquet"
output_path = f"s3://emr-on-eks-nvme-227401510565-us-east-1/trip-data-yellow-only_expand_{expansion_factor+1}X"


# 读取数据
from pyspark.sql.types import DoubleType, StringType, StructField, StructType, LongType

# 定义schema
schema = StructType([
    StructField("vendor_name", StringType()),
    StructField("Trip_Pickup_DateTime", StringType()),
    StructField("Trip_Dropoff_DateTime", StringType()),
    # StructField("passenger_count", DoubleType()),
    StructField("Trip_Distance", DoubleType()),
    StructField("Start_Lon", DoubleType()),
    StructField("Start_Lat", DoubleType()),
    # StructField("Rate_Code", DoubleType()),
    StructField("store_and_forward", DoubleType()),
    StructField("End_Lon", DoubleType()),
    StructField("End_Lat", DoubleType()),
    # StructField("Payment_Type", DoubleType()),
    StructField("Fare_Amt", DoubleType()),
    StructField("surcharge", DoubleType()),
    StructField("mta_tax", DoubleType()),
    StructField("Tip_Amt", DoubleType()),
    StructField("Tolls_Amt", DoubleType()),
    StructField("Total_Amt", DoubleType())
])


# 使用这个schema读取数据
df = spark.read.schema(schema).parquet(input_path)


df = df.withColumn("Payment_Type", (rand() * 2 + 1).cast("int")) \
       .withColumn("Rate_Code", (rand() * 8 + 1).cast("int")) \
       .withColumn("passenger_count", (rand() * 2 + 1).cast("int"))



sample_ratio = 0.00001  # 0.001% 的数据
sampled_df = df.sample(False, sample_ratio)

# 获取数值和字符串列
numeric_cols = [f.name for f in df.schema.fields if isinstance(f.dataType, (IntegerType, FloatType, DoubleType))]
string_cols = [f.name for f in df.schema.fields if isinstance(f.dataType, StringType)]

# 计算均值和标准差
agg_exprs = []
for col in numeric_cols:
    agg_exprs.append(F.mean(F.col(col)).alias(f"{col}_mean"))
    agg_exprs.append(F.stddev(F.col(col)).alias(f"{col}_stddev"))

agg_result = sampled_df.agg(*agg_exprs).collect()[0]

means = {col: float(agg_result[f"{col}_mean"]) for col in numeric_cols}
stddevs = {col: float(agg_result[f"{col}_stddev"]) for col in numeric_cols}

# 数据增强函数
def augment_row(row):
    new_row = {}
    for col in numeric_cols:
        new_row[col] = float(row[col]) + random.gauss(means[col], stddevs[col])
    for col in string_cols:
        new_row[col] = row[col]
    return Row(**new_row)

# 扩充数据
new_rows_rdd = df.rdd.flatMap(lambda row: [augment_row(row) for _ in range(expansion_factor)])
new_df = spark.createDataFrame(new_rows_rdd)

# 添加时间字段
df = df.withColumn("year_month", (year(to_timestamp("Trip_Pickup_DateTime")).cast("string") + "-" + month(to_timestamp("Trip_Pickup_DateTime")).cast("string")))
new_df = new_df.withColumn("year_month", (year(to_timestamp("Trip_Pickup_DateTime")).cast("string") + "-" + month(to_timestamp("Trip_Pickup_DateTime")).cast("string")))

# 合并数据
final_df = df.union(new_df)

# 保存到S3，按Payment_Type和year_month分区
final_df.write.partitionBy("Payment_Type", "year_month").parquet(output_path)

# 结束Spark会话
spark.stop()