from pyspark.sql import SparkSession
from pyspark.sql.functions import col
# 初始化Spark
spark = SparkSession.builder \
    .appName("Expand Data Efficiently") \
    .getOrCreate()

source_path = "s3a://emr-on-eks-nvme-227401510565-us-east-1/trip-data-yellow-only"
path_10x = "s3a://emr-on-eks-nvme-227401510565-us-east-1/trip-data-yellow-only_10x"
path_100x = "s3a://emr-on-eks-nvme-227401510565-us-east-1/trip-data-yellow-only_100x"

source_path = "s3://virginia-nyc-tlc/trip-data-yellow-only"
path_10x = "s3://virginia-nyc-tlc/trip-data-yellow-only_10x"
path_100x = "s3://virginia-nyc-tlc/trip-data-yellow-only_100x"
source_schema = spark.read.parquet(source_path).schema
df = spark.read.schema(source_schema).parquet(source_path)

# 扩充10倍
for _ in range(10):
    df.write.mode("append").parquet(path_10x)

# 读取10倍数据
df_10x = spark.read.parquet(path_10x)

# 扩充到100倍
for _ in range(10):
    df_10x.write.mode("append").parquet(path_100x)


# 读取数据
df = spark.read.parquet(source_path)

# 强制转换'payment_type'列为string类型
df = df.withColumn("payment_type", col("payment_type").cast("string"))

# 扩充到10x和100x
for _ in range(10):
    df.write.mode("append").parquet(path_10x)

df_10x = spark.read.parquet(path_10x)

for _ in range(9):
    df_10x.write.mode("append").parquet(path_100x)


copy_s3_files() {
  if [[ -z "$1" || -z "$2" ]]; then
    echo "Usage: copy_s3_files <source_path> <copy_count>"
    return 1
  fi

  source_path=${1:-virginia-nyc-tlc/trip-data-yellow-only}
  copy_count=$2
  target_path="${source_path/trip-data-yellow-only_${copy_count}x}"

  file_list=$(aws s3 ls s3://${source_path}/ --recursive | awk '{print $4}')
  
  if [[ -z "$file_list" ]]; then
    echo "No files found in ${source_path}"
    return 1
  fi

  echo "$file_list" | while read -r line; do
    for i in $(seq 1 $copy_count); do
      new_line="${line/\.parquet/_${i}.parquet}"
      aws s3 cp s3://${source_path}/${line} s3://${target_path}/${new_line} || {
        echo "Copy failed for ${line}"
        return 1
      }
    done
  done
}



src_bucket="virginia-nyc-tlc"                                                     
src_prefix="trip-data-yellow-only"
dst_bucket="virginia-nyc-tlc"
n=2  # 复制次数

# 获取所有文件
files=$(aws s3 ls s3://${src_bucket}/${src_prefix} --recursive | awk '{print $4}')

for i in $(seq 1 $n); do
    for file in $files; do
        # 提取并修改文件名
        file_name=$(basename $file)
        echo $file_name
        new_file_name=$(echo $file_name | sed "s/\.parquet/_${i}.parquet/")
        echo $new_file_name
        # aws s3 cp s3://${src_bucket}/${file} ./
        # aws s3 cp s3://${src_bucket}/${file} s3://${dst_bucket}/${src_prefix}_$n/${new_file_name}
    done
done



src_bucket="virginia-nyc-tlc"
src_prefix="trip-data-yellow-only"
dst_bucket="virginia-nyc-tlc"
n=2
# 获取所有文件
# aws s3 ls s3://${src_bucket}/${src_prefix} --recursive --page-size 1000  | awk '{print $4}' | while read -r file; do
aws s3 ls s3://${src_bucket}/${src_prefix} --recursive --page-size 1000 | awk '{print $4}' | grep '\.parquet$' | while read -r file; do
    for i in $(seq 1 $n); do
        # 提取并修改文件名
        file_name=$(basename $file)
        new_file_name=$(echo $file_name | sed "s/\.parquet/_${i}.parquet/")
        # echo $file_name
        # echo $new_file_name
        # echo s3://${src_bucket}/${src_prefix}/${file_name}
        # echo s3://${dst_bucket}/${src_prefix}_${n}/${new_file_name}
        source=s3://${src_bucket}/${src_prefix}/${file_name}
        target=s3://${dst_bucket}/${src_prefix}_${n}/${new_file_name}
        # 文件复制操作
        aws s3 cp $source $target
    done
done