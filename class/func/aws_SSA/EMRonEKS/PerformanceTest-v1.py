from pyspark.sql import SparkSession
from pyspark.sql.functions import avg, col, year, round
import time
import numpy as np
def find_stats(data):
    return np.min(data), np.max(data), np.mean(data), np.std(data)
def run_query(spark, path, cardinality):
    start = time.time() * 1000
    df = spark.read.parquet(path)
    if cardinality == 1:
        df.groupBy('fare_amt').agg(avg('total_amt')).collect()
    elif cardinality == 2:
        df.groupBy('fare_amt', year('trip_pickup_datetime').alias('trip_year')).agg(avg('total_amt')).collect()
    elif cardinality == 3:
        df.groupBy('fare_amt', year('trip_pickup_datetime').alias('trip_year'), round('trip_distance').alias('trip_distance')).agg(avg('total_amt')).collect()
    elif cardinality == 4:
        df.groupBy('fare_amt', year('trip_pickup_datetime').alias('trip_year'), round('trip_distance').alias('trip_distance')).agg(avg('total_amt')).filter(col('trip_year').isNotNull()).orderBy('trip_year').collect()
    end = time.time() * 1000
    return end - start
def run_experiment(path):
    spark = SparkSession.builder.appName("PerformanceTest").getOrCreate()
    spark.sparkContext.setLogLevel("ERROR")
    trials = 20
    times = []
    output_data = []
    for i in range(1, 5):
        for _ in range(trials):
            times.append(run_query(spark, path, i))
        min_val, max_val, mean_val, std_dev = find_stats(times)
        output_data.append((i, min_val, max_val, mean_val, std_dev))
    #spark.sparkContext.parallelize(output_data).saveAsTextFile(f"s3a://emr-on-eks-nvme-227401510565-us-east-1/taxidt/output/trip-data-yellow-only-aggregation_test_{int(time.time() * 1000)}.txt")
    spark.sparkContext.parallelize(output_data).coalesce(1).saveAsTextFile(f"s3a://emr-on-eks-nvme-227401510565-us-east-1/taxidt/output/trip-data-yellow-only-aggregation_test_{int(time.time() * 1000)}.txt")
if __name__ == "__main__":
    run_experiment("s3a://emr-on-eks-nvme-227401510565-us-east-1/trip-data-yellow-only")
