src_bucket="emr-on-eks-nvme-227401510565-us-east-1"
src_prefix="trip-data-yellow-only"
dst_bucket="emr-on-eks-nvme-227401510565-us-east-1"
# 🥑 🥑 🥑 🥑 🥑 🥑 🥑 🥑 🥑 🥑 🥑 🥑 
n=10 # 执行完后，改成100再执行次 🥑 🥑 
# 🥑 🥑 🥑 🥑 🥑 🥑 🥑 🥑 🥑 🥑 🥑 🥑 
# 获取所有文件
# aws s3 ls s3://${src_bucket}/${src_prefix} --recursive --page-size 1000  | awk '{print $4}' | while read -r file; do
aws s3 ls s3://${src_bucket}/${src_prefix} --recursive --page-size 1000 | awk '{print $4}' | grep '\.parquet$' | while read -r file; do
    for i in $(seq 1 $n); do
        # 提取并修改文件名
        file_name=$(basename $file)
        new_file_name=$(echo $file_name | sed "s/\.parquet/_${i}.parquet/")
        # echo $file_name
        # echo $new_file_name
        # echo s3://${src_bucket}/${src_prefix}/${file_name}
        # echo s3://${dst_bucket}/${src_prefix}_${n}/${new_file_name}
        source=s3://${src_bucket}/${src_prefix}/${file_name}
        target=s3://${dst_bucket}/${src_prefix}_${n}/${new_file_name}
        # 文件复制操作
        aws s3 cp $source $target
    done
done