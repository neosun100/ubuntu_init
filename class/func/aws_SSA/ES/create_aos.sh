OPENSEARCH_CLUSTER_NAME="binance-waf-04"                                                    # 集群name
OPENSEARCH_LATEST_VERSION=$(aws opensearch list-versions | jq '.Versions[0]' | pass_quotes) # opensearch version OpenSearch_2.3,Elasticsearch_7.10

OPENSEARCH_INSTANCE_TYPE="r6g.2xlarge.search"       # data node type
OPENSEARCH_INSTANCE_NUM=14                          # data node  num
OPENSEARCH_DedicatedMasterEnabled=true              # 启动多主
OPENSEARCH_ZoneAwarenessEnabled=true                # 启用区域感知
OPENSEARCH_AvailabilityZoneCount=2                  # 可用区数量
OPENSEARCH_DedicatedMasterType="m6g.2xlarge.search" # 主节点配置
OPENSEARCH_DedicatedMasterCount=3                   # 主节点数量
OPENSEARCH_WarmEnabled=true                         # 是否启用ultrawarm true|false
OPENSEARCH_WarmType="ultrawarm1.large.search"       # "WarmType": "ultrawarm1.medium.search"|"ultrawarm1.large.search"|"ultrawarm1.xlarge.search"
OPENSEARCH_WarmCount=20                             # 2~150
OPENSEARCH_ColdStorageOptions=true

OPENSEARCH_EBS_EBSEnabled=true
OPENSEARCH_EBS_VolumeType="gp3"
OPENSEARCH_EBS_VolumeSize=6144
OPENSEARCH_EBS_Iops=16000
OPENSEARCH_EBS_Throughput=593

OPENSEARCH_SubnetIds=subnet-0a8561b20a5c71ac9,subnet-0d2375de41fbda3e1
OPENSEARCH_SecurityGroupIds=sg-0708b2efbc03b7d46

# AOS_ROOT=
# AOS_PASSWORD=

# ------

aws opensearch create-domain \
    --domain-name $OPENSEARCH_CLUSTER_NAME \
    --engine-version $OPENSEARCH_LATEST_VERSION \
    --cluster-config InstanceType=$OPENSEARCH_INSTANCE_TYPE,InstanceCount=$OPENSEARCH_INSTANCE_NUM,DedicatedMasterEnabled=$OPENSEARCH_DedicatedMasterEnabled,ZoneAwarenessEnabled=$OPENSEARCH_ZoneAwarenessEnabled,ZoneAwarenessConfig={AvailabilityZoneCount=$OPENSEARCH_AvailabilityZoneCount},DedicatedMasterType=$OPENSEARCH_DedicatedMasterType,DedicatedMasterCount=$OPENSEARCH_DedicatedMasterCount,WarmEnabled=$OPENSEARCH_WarmEnabled,WarmType=$OPENSEARCH_WarmType,WarmCount=$OPENSEARCH_WarmCount,ColdStorageOptions={Enabled=$OPENSEARCH_ColdStorageOptions} \
    --ebs-options EBSEnabled=$OPENSEARCH_EBS_EBSEnabled,VolumeType=$OPENSEARCH_EBS_VolumeType,VolumeSize=$OPENSEARCH_EBS_VolumeSize,Iops=$OPENSEARCH_EBS_Iops,Throughput=$OPENSEARCH_EBS_Throughput \
    --vpc-options SubnetIds=$OPENSEARCH_SubnetIds,SecurityGroupIds=$OPENSEARCH_SecurityGroupIds \
    --cognito-options Enabled=false \
    --node-to-node-encryption-options Enabled=true \
    --encryption-at-rest-options Enabled=true \
    --access-policies '{"Version":"2012-10-17","Statement":[{"Effect":"Allow","Principal":{"AWS":"*"},"Action":"es:*","Resource":"arn:aws:es:us-east-1:835751346093:domain/*/*"}]}' \
    --domain-endpoint-options EnforceHTTPS=true,TLSSecurityPolicy="Policy-Min-TLS-1-2-2019-07" \
    --advanced-security-options "Enabled=true,InternalUserDatabaseEnabled=true,MasterUserOptions={MasterUserName=$AOS_ROOT,MasterUserPassword=$AOS_PASSWORD}"

# ----

aws opensearch delete-domain \
    --domain-name binance-log-03

## --
AOS_URL="vpc-binance-waf-03-sse2ew75s7smvllvbudihpxcj4.us-east-1.es.amazonaws.com"
AOS_URL="10.68.119.129"

~/upload/bin/flog -d 0.1ns -f 1k -l -w |
    /opt/fluent-bit/bin/fluent-bit -i stdin \
        -p Mem_Buf_Limit=1024MB \
        -o opensearch \
        -p Host=$AOS_URL \
        -p Port=443 \
        -p Index=loghub-v1-binance \
        -p Suppress_Type_Name=on \
        -p Type=my_type \
        -p HTTP_User=root \
        -p HTTP_Passwd=AWS2themoon100$ \
        -p tls=on \
        -p tls.verify=off \
        -p Workers=1 \
        -p Buffer_Size=5MB

# ---https://vpc-binance-log-02-pt2jfr5bgagkfyswvsb3ab2hdu.us-east-1.es.amazonaws.com

AOS_URL="vpc-binance-waf-03-sse2ew75s7smvllvbudihpxcj4.us-east-1.es.amazonaws.com"
CONF_FILE=parallel.GCP-logging

rm -rf $CONF_FILE

CORE_NUM=$(nproc)
KEY_NUM=1
KEY_Molecule=10    # 分子
KEY_Denominator=10 # 分母
# TOTAL_MEMORY KB
TOTAL_MEMORY=$(cat /proc/meminfo | head -n 1 | awk "{print \$${1:-2}}")
TASK_NUM=$(expr $CORE_NUM \* $KEY_NUM)
# TASK_NUM=`expr $CORE_NUM / $KEY_Denominator \* $KEY_Molecule`
ONE_TASK_MEMORY=$(expr $TOTAL_MEMORY / $TASK_NUM / 10 \* 9)

for i in $(seq 1 $TASK_NUM); do echo " ~/upload/bin/flog -d 0.1ns -f 1k -l -w | /opt/fluent-bit/bin/fluent-bit -i stdin -p Mem_Buf_Limit=1024MB -o opensearch -p Host=$AOS_URL -p Port=443 -p Index=loghub-v1-binance -p Suppress_Type_Name=on -p Type=my_type -p HTTP_User=root -p HTTP_Passwd=AWS2themoon100$ -p tls=on -p tls.verify=off -p Workers=64 -p Buffer_Size=5MB" >>$CONF_FILE; done

wc -l $CONF_FILE

parallel -j $TASK_NUM <$CONF_FILE

# ----

create_multi_aos_clusters() {
    CLUSTER_NUM=${1:-20}
    CLUSTER_NAME=${2:-cluster-binance}
    for i in $(seq 1 $CLUSTER_NUM); do
        echo $CLUSTER_NAME-$i
        OPENSEARCH_CLUSTER_NAME=$CLUSTER_NAME-$i                                                    # 集群name
        OPENSEARCH_LATEST_VERSION=$(aws opensearch list-versions | jq '.Versions[0]' | pass_quotes) # opensearch version OpenSearch_2.3,Elasticsearch_7.10

        OPENSEARCH_INSTANCE_TYPE="r6g.2xlarge.search"       # data node type
        OPENSEARCH_INSTANCE_NUM=14                          # data node  num
        OPENSEARCH_DedicatedMasterEnabled=true              # 启动多主
        OPENSEARCH_ZoneAwarenessEnabled=true                # 启用区域感知
        OPENSEARCH_AvailabilityZoneCount=2                  # 可用区数量
        OPENSEARCH_DedicatedMasterType="m6g.2xlarge.search" # 主节点配置
        OPENSEARCH_DedicatedMasterCount=3                   # 主节点数量
        OPENSEARCH_WarmEnabled=true                         # 是否启用ultrawarm true|false
        OPENSEARCH_WarmType="ultrawarm1.large.search"       # "WarmType": "ultrawarm1.medium.search"|"ultrawarm1.large.search"|"ultrawarm1.xlarge.search"
        OPENSEARCH_WarmCount=20                             # 2~150
        OPENSEARCH_ColdStorageOptions=true

        OPENSEARCH_EBS_EBSEnabled=true
        OPENSEARCH_EBS_VolumeType="gp3"
        OPENSEARCH_EBS_VolumeSize=6144
        OPENSEARCH_EBS_Iops=16000
        OPENSEARCH_EBS_Throughput=593

        OPENSEARCH_SubnetIds=subnet-0a8561b20a5c71ac9,subnet-0d2375de41fbda3e1
        OPENSEARCH_SecurityGroupIds=sg-0708b2efbc03b7d46

        # AOS_ROOT=
        # AOS_PASSWORD=

        # ------

        aws opensearch create-domain \
            --domain-name $OPENSEARCH_CLUSTER_NAME \
            --engine-version $OPENSEARCH_LATEST_VERSION \
            --cluster-config InstanceType=$OPENSEARCH_INSTANCE_TYPE,InstanceCount=$OPENSEARCH_INSTANCE_NUM,DedicatedMasterEnabled=$OPENSEARCH_DedicatedMasterEnabled,ZoneAwarenessEnabled=$OPENSEARCH_ZoneAwarenessEnabled,ZoneAwarenessConfig={AvailabilityZoneCount=$OPENSEARCH_AvailabilityZoneCount},DedicatedMasterType=$OPENSEARCH_DedicatedMasterType,DedicatedMasterCount=$OPENSEARCH_DedicatedMasterCount,WarmEnabled=$OPENSEARCH_WarmEnabled,WarmType=$OPENSEARCH_WarmType,WarmCount=$OPENSEARCH_WarmCount,ColdStorageOptions={Enabled=$OPENSEARCH_ColdStorageOptions} \
            --ebs-options EBSEnabled=$OPENSEARCH_EBS_EBSEnabled,VolumeType=$OPENSEARCH_EBS_VolumeType,VolumeSize=$OPENSEARCH_EBS_VolumeSize,Iops=$OPENSEARCH_EBS_Iops,Throughput=$OPENSEARCH_EBS_Throughput \
            --vpc-options SubnetIds=$OPENSEARCH_SubnetIds,SecurityGroupIds=$OPENSEARCH_SecurityGroupIds \
            --cognito-options Enabled=false \
            --node-to-node-encryption-options Enabled=true \
            --encryption-at-rest-options Enabled=true \
            --access-policies '{"Version":"2012-10-17","Statement":[{"Effect":"Allow","Principal":{"AWS":"*"},"Action":"es:*","Resource":"arn:aws:es:us-east-1:835751346093:domain/*/*"}]}' \
            --domain-endpoint-options EnforceHTTPS=true,TLSSecurityPolicy="Policy-Min-TLS-1-2-2019-07" \
            --advanced-security-options "Enabled=true,InternalUserDatabaseEnabled=true,MasterUserOptions={MasterUserName=$AOS_ROOT,MasterUserPassword=$AOS_PASSWORD}"

    done

}

for i in $(cat aos_cluster_name.list); do
    aws opensearch update-domain-config \
        --domain-name $i \
        --advanced-options "{\"rest.action.multi.allow_explicit_index\": \"true\"}"
done

for i in $(cat aos_cluster.list); do
    AOS_URL="https://$i"
    curl --request PUT \
        --url $AOS_URL/_cluster/settings \
        --header 'Authorization: Basic cm9vdDpBV1MydGhlbW9vbjEwMCQ=' \
        --header 'Content-Type: application/json' \
        --data '{
  "persistent" : {
    "compatibility.override_main_response_version" : true
  }
}'
done


要使用配置 API 启用兼容模式，请将 override_main_response_version 设置为 true：


POST https://es.us-east-1.amazonaws.com/2021-01-01/opensearch/upgradeDomain
{
  "DomainName": "domain-name",
  "TargetVersion": "OpenSearch_1.0",
  "AdvancedOptions": {
    "override_main_response_version": "true"
   }
}
要在现有的 OpenSearch 域上启用或禁用兼容模式，您需要使用 OpenSearch _cluster/settings API 操作：


PUT /_cluster/settings
{
  "persistent" : {
    "compatibility.override_main_response_version" : true
  }
}