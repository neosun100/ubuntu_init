import argparse
import asyncio
import logging
import numpy as np
from elasticsearch import Elasticsearch, helpers

# 设置日志
logging.basicConfig(
    format='%(asctime)s %(levelname)s:%(message)s', level=logging.INFO)

# 数据源 - 这是一个生成器，生成随机的embedding数据


def get_data(num_docs):
    for _ in range(num_docs):
        # 这里我们只生成embedding字段，这是一个长度为768的随机浮点数列表
        yield {"embedding": np.random.rand(768).tolist()}


def sync_bulk_insert(es, data):
    # 使用批量插入API
    helpers.bulk(es, data)


def process_data(data_to_insert, es_url, es_auth):
    # 创建OpenSearch客户端
    es = Elasticsearch(es_url, http_auth=es_auth)
    data_to_insert = list(map(lambda x: {
        "_index": "chatbot-2023-07",  # 确保这个名字符合你的索引模式
        "_source": x
    }, data_to_insert))
    sync_bulk_insert(es, data_to_insert)


def main(num_docs, es_url, es_auth):
    data_to_insert = []
    for data in get_data(num_docs):
        data_to_insert.append(data)

        # 每当积累到一定数量的数据时，就处理并插入数据
        if len(data_to_insert) >= 5000:  # 增加批量插入的大小
            logging.info(
                f'Starting to process a batch of {len(data_to_insert)} documents')
            process_data(data_to_insert, es_url, es_auth)
            data_to_insert = []

    # 处理并插入剩余的数据
    if data_to_insert:
        logging.info(
            f'Starting to process the remaining {len(data_to_insert)} documents')
        process_data(data_to_insert, es_url, es_auth)

    logging.info('All documents have been processed')


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Insert documents into OpenSearch")
    parser.add_argument("num_docs", type=int,
                        help="Number of documents to insert")
    parser.add_argument("es_url", type=str, help="OpenSearch endpoint URL")
    parser.add_argument("es_username", type=str, help="OpenSearch username")
    parser.add_argument("es_password", type=str, help="OpenSearch password")
    args = parser.parse_args()
    es_auth = (args.es_username, args.es_password)
    main(args.num_docs, args.es_url, es_auth)
