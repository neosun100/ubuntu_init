for i in $(cat aos_cluster.list); do
    AOS_URL="https://$i"
    echo $AOS_URL
    curl --request PUT \
        --url $AOS_URL/_index_template/test-index-waf-template \
        --header 'Authorization: Basic YWRtaW46b0JbOiw4bVQ6dF4v' \
        --header 'Content-Type: application/json' \
        --data '{
    "index_patterns": [
        "test-index-waf-*"
    ],
    "template": {
        "settings": {
            "index": {
                "ultrawarm": {
                    "migration": {
                        "force_merge": {
                            "max_num_segments": "1000"
                        }
                    }
                },
                "codec": "best_compression",
                "refresh_interval": "60s",
                "number_of_shards": "24",
                "number_of_replicas": "1",
                "plugins": {
                    "index_state_management": {
                        "rollover_alias": "test-index-waf"
                    }
                }
            }
        },
        "mappings": {
            "properties": {
                "terminatingRuleId": {
                    "type": "keyword"
                },
                "terminatingRuleType": {
                    "type": "keyword"
                },
                "ruleGroupList": {
                    "properties": {
                        "terminatingRule": {
                            "properties": {
                                "action": {
                                    "type": "keyword"
                                },
                                "ruleId": {
                                    "type": "keyword"
                                }
                            }
                        },
                        "ruleGroupId": {
                            "type": "keyword"
                        }
                    }
                },
                "httpSourceId": {
                    "type": "keyword"
                },
                "labels": {
                    "properties": {
                        "name": {
                            "type": "text",
                            "fields": {
                                "keyword": {
                                    "ignore_above": 256,
                                    "type": "keyword"
                                }
                            }
                        }
                    }
                },
                "webaclId": {
                    "type": "text",
                    "fields": {
                        "keyword": {
                            "ignore_above": 256,
                            "type": "keyword"
                        }
                    }
                },
                "@timestamp": {
                    "path": "timestamp",
                    "type": "alias"
                },
                "webaclName": {
                    "type": "keyword"
                },
                "action": {
                    "type": "keyword"
                },
                "httpRequest": {
                    "properties": {
                        "args": {
                            "type": "text",
                            "fields": {
                                "keyword": {
                                    "ignore_above": 256,
                                    "type": "keyword"
                                }
                            }
                        },
                        "country": {
                            "type": "keyword"
                        },
                        "headers": {
                            "properties": {
                                "name": {
                                    "type": "keyword"
                                },
                                "value": {
                                    "type": "text",
                                    "fields": {
                                        "keyword": {
                                            "ignore_above": 256,
                                            "type": "keyword"
                                        }
                                    }
                                }
                            }
                        },
                        "httpVersion": {
                            "type": "keyword"
                        },
                        "requestId": {
                            "type": "text",
                            "fields": {
                                "keyword": {
                                    "ignore_above": 256,
                                    "type": "keyword"
                                }
                            }
                        },
                        "clientIp": {
                            "type": "ip"
                        },
                        "httpMethod": {
                            "type": "keyword"
                        },
                        "uri": {
                            "type": "text",
                            "fields": {
                                "keyword": {
                                    "ignore_above": 256,
                                    "type": "keyword"
                                }
                            }
                        }
                    }
                },
                "httpSourceName": {
                    "type": "keyword"
                },
                "formatVersion": {
                    "type": "keyword"
                },
                "timestamp": {
                    "format": "epoch_millis",
                    "type": "date"
                }
            }
        },
        "aliases": {

        }
    },
    "composed_of": [

    ]
}'
    echo ""
done
