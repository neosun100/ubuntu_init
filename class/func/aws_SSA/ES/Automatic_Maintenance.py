# Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
# SPDX-License-Identifier: Apache-2.0

import json
import logging
import os
import boto3
from botocore import config
from botocore.client import Config
import uuid
import requests
from requests_aws4auth import AWS4Auth
import time
from decimal import Decimal
from datetime import datetime
from boto3.dynamodb.conditions import Key, Attr
from util.osutil import OpenSearch


logger = logging.getLogger()
logger.setLevel(logging.INFO)

DEFAULT_TIME_FORMAT = "%Y-%m-%dT%H:%M:%SZ"

stack_prefix = os.environ.get("STACK_PREFIX", "CL")
solution_version = os.environ.get("SOLUTION_VERSION", "v1.0.0")
solution_id = os.environ.get("SOLUTION_ID", "SO8025")
user_agent_config = {
    "user_agent_extra": f"AwsSolution/{solution_id}/{solution_version}"
}
default_config = config.Config(**user_agent_config)
default_region = os.environ.get("AWS_REGION")

awslambda = boto3.client("lambda", config=default_config)
dynamodb = boto3.resource("dynamodb", config=default_config)
ssm = boto3.client("ssm", config=default_config)

clusterinfo_table_name = os.environ.get("CLUSTERINFO_TABLE_NAME")
statistics_table_name = os.environ.get("STATISTICS_TABLE_NAME")
current_role_arn = str(os.environ.get("MONITOR_HANDLER_IAM_ROLE"))

clusterinfo_table = dynamodb.Table(clusterinfo_table_name)
statistics_table = dynamodb.Table(statistics_table_name)

request_headers = {"Content-Type": "application/json"}

service = "es"
credentials = boto3.Session().get_credentials()
awsauth = AWS4Auth(
    credentials.access_key,
    credentials.secret_key,
    default_region,
    service,
    session_token=credentials.token,
)

default_config = Config(
    connect_timeout=30, retries={"max_attempts": 3}, user_agent_extra=user_agent_config["user_agent_extra"]
)
es = boto3.client("es", region_name=default_region, config=default_config)


class ErrorCode:
    DUPLICATED_INDEX_PREFIX = "DuplicatedIndexPrefix"
    DUPLICATED_WITH_INACTIVE_INDEX_PREFIX = "DuplicatedWithInactiveIndexPrefix"
    OVERLAP_INDEX_PREFIX = "OverlapIndexPrefix"
    OVERLAP_WITH_INACTIVE_INDEX_PREFIX = "OverlapWithInactiveIndexPrefix"
    INVALID_INDEX_MAPPING = "InvalidIndexMapping"


class APIException(Exception):
    def __init__(self, message, code: str = None):
        if code:
            super().__init__("[{}] {}".format(code, message))
        else:
            super().__init__(message)


def handle_error(func):
    """Decorator for exception handling"""

    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except APIException as e:
            logger.exception(e)
            raise e
        except Exception as e:
            logger.exception(e)
            raise RuntimeError(
                "Unknown exception, please check Lambda log for more details"
            )

    return wrapper


def _get_aos_alias_index_name(aos: str):
    logger.info("in get_aos_alias_index_name, endpoint = " + aos)
    response = requests.get(aos + "/_alias/waflogs-waf/",
                            auth=awsauth, headers=request_headers)

    content = response.content
    content = content.decode("UTF-8")
    res_list = content.strip().split(" ")
    logger.info("in get_aos_time_and_doc, endpoint = " +
                aos + ", response is: " + str(res_list))
    if len(res_list) == 3:
        ts = int(res_list[0])
        doc = int(res_list[2])
        return ts, doc
    else:
        logger.info("response list length is not 3 !")
        return 1, 0

def _get_aos_time_and_doc(aos: str):
    logger.info("in get_aos_time_and_doc, endpoint = " + aos)
    response = requests.get(aos + "/_cat/count/",
                            auth=awsauth, headers=request_headers)

    content = response.content
    content = content.decode("UTF-8")
    res_list = content.strip().split(" ")
    logger.info("in get_aos_time_and_doc, endpoint = " +
                aos + ", response is: " + str(res_list))
    if len(res_list) == 3:
        ts = int(res_list[0])
        doc = int(res_list[2])
        return ts, doc
    else:
        logger.info("response list length is not 3 !")
        return 1, 0


def _get_aos_migration_status(aos: str):
    indices_response = requests.get(
        aos + "/_cat/indices", auth=awsauth, headers=request_headers)
    indices = indices_response.content.decode("UTF-8")
    res_list = indices.strip().split("\n")
    pending_count = 0
    pending_state = "-"
    failed_count = 0
    failed_state = "-"
    running_count = 0
    running_state = "-"
    for index_str in res_list:
        index_name = index_str.split(" ")[2]
        logger.info("in get_aos_migration_status, endpoint = " + aos)
        response = requests.get(aos + "/_ultrawarm/migration/" + index_name + "/_status", auth=awsauth,
                                headers=request_headers)

        content = response.content
        content = content.decode("UTF-8")
        content_dict = json.loads(content)
        if "migration_status" in content_dict and "state" in content_dict["migration_status"]:
            state = content_dict["migration_status"]["state"]
            logger.info("has migration_status, endpoint = " +
                        aos + ", migration_state = " + state)
            if "PENDING" in state:
                pending_count += 1
                pending_state = state
            elif "FAILED" in state:
                failed_count += 1
                failed_state = state
            elif "RUNNING" in state:
                running_count += 1
                running_state = state

        else:
            logger.info("no state in response, endpoint = " +
                        aos + ", response is: " + content)

    return pending_count, failed_count, pending_state, failed_state, running_count, running_state


def _bulk_to_aos(dashboard_url: str, bulk_body: str):
    monitor_aos_url = dashboard_url + "/_bulk"
    bulk_body_str = '''\n'''.join(bulk_body) + '''\n'''
    logger.info(bulk_body_str)
    response = requests.post(
        monitor_aos_url, data=bulk_body_str, auth=awsauth, headers=request_headers)
    rep_content = response.content.decode("utf-8")
    rep_content_dict = json.loads(rep_content)
    logger.info(rep_content_dict)


def _update_item_weight(item, ops):
    logger.info("_update_item_weight Ops ", ops)
    ue = '''SET cluster_health = :cluster_health, cluster_policy_ids = :cluster_policy_ids'''
    ea = {
        ':cluster_health': ops["cluster_health"],
        ':cluster_policy_ids': ops["cluster_policy_ids"]
    }
    if "weight" in ops:
        ue = '''SET weight = :weight, weight_updated = :weight_updated, 
    cluster_health = :cluster_health, cluster_policy_ids = :cluster_policy_ids'''
        ea = {
            ':weight': ops["weight"],
            ':weight_updated': ops["weight_updated"],
            ':cluster_health': ops["cluster_health"],
            ':cluster_policy_ids': ops["cluster_policy_ids"]
        }

    logger.info("Update Item UpdateExpression: ")
    logger.info(ue)
    logger.info("Update Item ExpressionAttributeValues ")
    logger.info(ea)

    response = clusterinfo_table.update_item(
        Key={
            'cluster_name': item["cluster_name"]
        },
        ConditionExpression=Attr('id').eq(item["id"]),
        UpdateExpression=ue,
        ExpressionAttributeValues=ea,

        ReturnValues="UPDATED_NEW"
    )

    return response


def _create_monitor_index(aos, index_name):
    resp = aos.create_monitor_index(index_name)
    # 403 没权限 role没加上 ; 500 需要解析内容; <=300 代表成功
    return {"status_code": resp.status_code, "resp_text": resp.text}


def _add_master_role(role_arn: str, domain_name: str):
    logger.info("Add backend role %s to domain %s", role_arn, domain_name)
    try:
        resp = es.update_elasticsearch_domain_config(
            DomainName=domain_name,
            AdvancedSecurityOptions={
                "MasterUserOptions": {
                    "MasterUserARN": role_arn,
                },
            },
        )
        logger.info("Response status: %d",
                    resp["ResponseMetadata"]["HTTPStatusCode"])
        return resp["ResponseMetadata"]["HTTPStatusCode"]
    except Exception as e:
        logger.error("Unable to automatically add backend role")
        logger.error(e)
        logger.info("Please manually add backend role for %s", role_arn)
        return -1


def _call_aos_api(request_url: str):
    response = requests.get(request_url, auth=awsauth, headers=request_headers)
    content = response.content
    content = content.decode("UTF-8")
    response_data = json.loads(content)
    logger.info("request_url: " + request_url)
    logger.info("response_data: ")
    logger.info(response_data)
    return response_data


# @handle_error
def lambda_handler(event, context):
    logger.info("Start to get monitor data of all AOS clusters!")
    current_time = datetime.now()
    dashboard_index_ready = True
    # Get AutoWeighting parameter from parameter store.
    # * 是否开启自动投递权重
    # * 默认不开启，默认值为0
    # * 但为0时，各集群投递权重均为1
    auto_weighting = 0
    try:
        auto_weighting = int(
            ssm.get_parameter(Name="/CL/AutoWeighting", WithDecryption=True)["Parameter"][
                "Value"
            ]
        )
    except Exception as e:
        logger.error("Unable to get /CL/AutoWeighting from parameter store")
        logger.exception(e)

    # Get all items from cluster info table.
    response = clusterinfo_table.scan()
    items = response['Items']
    while 'LastEvaluatedKey' in response:
        response = clusterinfo_table.scan(
            ExclusiveStartKey=response['LastEvaluatedKey'])
        items.extend(response['Items'])
    logger.info(items)
    aos_list = []
    dashboard_endpoint = ""
    workload_dict = {}
    for item in items:
        # Status 3 means the cluster is ready to write data.
        if int(item["cluster_status"]) < 2 or int(item["cluster_status"]) > 4:
            continue
        # Filter cluster endpoint url when it is empty
        if item["cluster_url"] == "" or item["cluster_url"] is None:
            continue

        if str(item["is_dashboard"]) == "1":
            dashboard_endpoint = item["cluster_url"]

        statistics_dict = {"cluster_url": item["cluster_url"], "cluster_name": item["cluster_name"],
                           "cluster_status": int(item["cluster_status"]),
                           "is_dashboard": int(item["is_dashboard"]), "weight": 1}

        allocation_request_url = item["cluster_url"] + \
            "/_cat/allocation?format=json"
        allocation_data = _call_aos_api(allocation_request_url)
        if 'error' in allocation_data:
            # Add lambda IAM role to Opensearch
            add_role_response = _add_master_role(
                current_role_arn, item["cluster_name"])
            if add_role_response == -1:
                return "Failed to call cluster " + item["cluster_name"] + " to add master role"
            dashboard_index_ready = False
            logger.info("Error response for " + allocation_request_url)
            logger.info(allocation_data)
            continue
        ultrawarm_nodes = []
        for allcation in allocation_data:
            if "disk.total" in allcation and allcation["disk.total"] == "20tb":
                ultrawarm_nodes.append(allcation["node"])
        logger.info("ultrawarm_nodes:")
        logger.info(ultrawarm_nodes)
        request_url = item["cluster_url"] + "/_cat/nodes?format=json"
        nodes_data = _call_aos_api(request_url)

        node_count = 0
        cpu_total = 0
        if 'error' in nodes_data or len(nodes_data) == 0:
            logger.info("Error response for " + request_url)
            logger.info(nodes_data)
            continue
        for node in nodes_data:
            if node["cluster_manager"] == "-" and node["node.role"] == "dir" and node["name"] not in ultrawarm_nodes:
                logger.info("node name = " + node["name"] + ":" + node["cpu"])
                node_count += 1
                node_cpu = int(node["cpu"])
                # CPU value for nodes from 0-100, set a default value 1 to get a reasonable weight for cluster.
                if node_cpu == 0:
                    node_cpu = 1
                cpu_total += (node_cpu * 0.01)
        logger.info("cpu_total = " + str(cpu_total))
        cpu_avg = 0
        logger.info("node_count = " + str(node_count))

        if node_count > 0:
            cpu_avg = round(cpu_total / node_count, 5)
        logger.info(item["cluster_name"] + ", cpu_avg = " + str(cpu_avg))
        logger.info(item["cluster_name"] + ", node_count: " + str(node_count))
        weight = 0
        if cpu_avg > 0 and int(item["cluster_status"]) == 3:
            weight = round(1 / cpu_avg, 2)

        if str(item["is_dashboard"]) == "1":
            weight = round((weight * 9) / 10, 2)

        aos_list.append(
            {"domain_name": item["cluster_name"], "endpoint": item["cluster_url"]})
        workload_dict[item["cluster_name"]] = cpu_avg
        statistics_dict["workload"] = cpu_avg
        statistics_dict["weight"] = weight
        statistics_dict["event_ts"] = datetime.now().strftime(
            "%m/%d/%Y, %H:%M:%S")
        statistics_dict["ttl"] = (int(time.time()) + 60 * 60 * 24 * 30)

        health_request_url = item["cluster_url"] + "/_cluster/health"
        health_data = _call_aos_api(health_request_url)
        if 'error' in health_data:
            logger.info(
                "Error response for health_request_url: " + health_request_url)
            logger.info(health_data)
            continue

        policy_request_url = item["cluster_url"] + "/_opendistro/_ism/policies"
        policy_data = _call_aos_api(policy_request_url)
        if 'error' in policy_data:
            logger.info(
                "Error response for policy_request_url: " + policy_request_url)
            logger.info(policy_data)
            continue
        policy_id_list = []
        if "policies" in policy_data and len(policy_data["policies"]) > 0:
            for p in policy_data["policies"]:
                if "policy" in p and "policy_id" in p["policy"]:
                    policy_id_list.append(p["policy"]["policy_id"])
        weight_dict = {"cluster_health": json.dumps(health_data),
                       "cluster_policy_ids": json.dumps(policy_id_list)}
        if item["weight"] != weight and auto_weighting == 1:
            weight_dict["weight"] = weight
            weight_dict["weight_updated"] = 1
        ops_dict = json.loads(json.dumps(weight_dict), parse_float=Decimal)
        _update_item_weight(item, ops=ops_dict)

        id = str(uuid.uuid4())
        statistics_dict["id"] = id
        item = json.loads(json.dumps(statistics_dict), parse_float=Decimal)
        statistics_table.put_item(Item=item)

    # Get monitor data every 5 minutes. If current minute not a multiple of 5, finish the function.
    if (current_time.minute % 5) != 0:
        return "Successfully execute the function to update DDB tables."

    # Create monitor index for dashboard Opensearch
    if dashboard_endpoint != "":
        index_response = _call_aos_api(
            dashboard_endpoint + "/all_cluster_monitor_index")
        logger.info("Dashboard aos index_response:")
        logger.info(index_response)
        # delete monitor index when timestamp type is not date.
        if "all_cluster_monitor_index" in index_response:
            if index_response['all_cluster_monitor_index']['mappings']['properties']['timestamp']['type'] == "long":
                requests.delete(dashboard_endpoint + "/all_cluster_monitor_index",
                                auth=awsauth, headers=request_headers)
                requests.delete(dashboard_endpoint + "/all_cluster_monitor_migration_index",
                                auth=awsauth, headers=request_headers)
                time.sleep(2)
        index_response = _call_aos_api(
            dashboard_endpoint + "/all_cluster_monitor_index")
        logger.info("Dashboard aos index_response after delete:")
        logger.info(index_response)
        if "error" in index_response:
            index_name_list = [
                "all_cluster_monitor_migration_index", "all_cluster_monitor_index"]
            for index_name in index_name_list:
                aos = OpenSearch(
                    region=default_region,
                    endpoint=dashboard_endpoint.strip("https://"),
                    index_prefix="",
                    engine="OpenSearch",
                    log_type="",
                )
                index_create_response = _create_monitor_index(aos, index_name)

            time.sleep(2)
            logger.info("index_create_response %s", index_create_response)
            if index_create_response["status_code"] >= 300:
                dashboard_index_ready = False
                if index_create_response["status_code"] == 409:
                    dashboard_index_ready = True
                if index_create_response["status_code"] == 400:
                    resp_text_json = json.loads(
                        index_create_response.get("resp_text"))
                    logger.info("resp_text %s", resp_text_json)
                    if resp_text_json.get("error") and (
                            "already exists" in resp_text_json.get("error").get("reason")):
                        dashboard_index_ready = True

    if dashboard_index_ready is False or dashboard_endpoint == "":
        return "Dashboard AOS monitor index is not ready!"

    bulk_body = []
    t = time.time()
    timestamp = int(round(t * 1000))

    aos_monitor_dict = {}

    for aos in aos_list:
        last_ts, last_doc = _get_aos_time_and_doc(aos["endpoint"])
        aos_monitor_dict[aos["domain_name"]] = {
            "last_ts": last_ts, "last_doc": last_doc}

    time.sleep(120)

    for aos in aos_list:
        current_ts, current_doc = _get_aos_time_and_doc(aos["endpoint"])

        time_diff = current_ts - \
            aos_monitor_dict[aos["domain_name"]]["last_ts"]
        doc_diff = current_doc - \
            aos_monitor_dict[aos["domain_name"]]["last_doc"]
        if doc_diff < 0:
            continue
        write_per_second = 0.0
        logger.info("write per second: ")
        logger.info(write_per_second)

        # Unauthorized clusters time_diff is wrong
        if time_diff > 0:
            write_per_second = doc_diff / time_diff
        else:
            continue

        domain_workload = 0
        if aos["domain_name"] in workload_dict:
            domain_workload = workload_dict[aos["domain_name"]]
        doc_json = {"domain_name": aos["domain_name"], "time_diff": time_diff, "doc_diff": doc_diff,
                    "write_per_second": write_per_second, "current_timestamp": current_ts, "timestamp": timestamp,
                    "current_documents": current_doc,
                    "write_size_per_second": round((write_per_second * 3.5) / 1024, 2),
                    "current_size_gb": round((current_doc * 3.5) / 1024 / 1024, 2),
                    "current_workload": domain_workload}
        doc_info = {"index": {"_index": "all_cluster_monitor_index"}}
        bulk_body.append(json.dumps(doc_info))
        bulk_body.append(json.dumps(doc_json))
    if dashboard_endpoint != "":
        _bulk_to_aos(dashboard_url=dashboard_endpoint, bulk_body=bulk_body)

    # Migration status
    migration_bulk_body = []
    for aos in aos_list:
        pending_count, failed_count, pending_state, failed_state, running_count, running_state = _get_aos_migration_status(
            aos["endpoint"])
        doc_json = {"domain_name": aos["domain_name"], "pending_count": pending_count, "failed_count": failed_count,
                    "pending_state": pending_state, "failed_state": failed_state, "timestamp": timestamp,
                    "running_count": running_count, "running_state": running_state}  # GB
        doc_info = {"index": {"_index": "all_cluster_monitor_migration_index"}}
        migration_bulk_body.append(json.dumps(doc_info))
        migration_bulk_body.append(json.dumps(doc_json))
    if dashboard_endpoint != "":
        _bulk_to_aos(dashboard_url=dashboard_endpoint,
                     bulk_body=migration_bulk_body)

    return "Successfully execute the function to update DDB tables and generate monitor data to AOS."
