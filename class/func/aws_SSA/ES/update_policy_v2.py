import requests
import json
import pandas as pd
import io

# 读取CSV文件内容
csv_data = '''
Cluster	node	num	node_cores	max_ebs	total_cores	node_memory	JVM_memory	total_JVM	total_memory	rollover_size	shard_num	shard_size	endpoint
1	r6g 2xlarge	36	8	6144	288	64	32	1152	2304	2	36	56.9 	vpc-testing-eu-west-1-1-edjukzf5yoqydnxg456seosqhu.eu-west-1.es.amazonaws.com
2	r6g 2xlarge	36	8	6144	288	64	32	1152	2304	2	72	28.4 	vpc-testing-eu-west-1-2-z5oybjovfrdkfach22mvnjzkia.eu-west-1.es.amazonaws.com
3	r6g 2xlarge	36	8	6144	288	64	32	1152	2304	3	108	28.4 	vpc-testing-eu-west-1-3-2tbfkjgzhhninojzojkdf3puey.eu-west-1.es.amazonaws.com
4	r6g 2xlarge	36	8	6144	288	64	32	1152	2304	3	144	21.3 	vpc-testing-eu-west-1-4-lc2uqncqvyjswmxkrfufagufvi.eu-west-1.es.amazonaws.com
5	m6g 4xlarge	36	16	6144	576	64	32	1152	2304	2	36	56.9 	vpc-testing-eu-west-1-5-4vfw6skkiahevop2yo5zb3su5a.eu-west-1.es.amazonaws.com
6	m6g 4xlarge	36	16	6144	576	64	32	1152	2304	2	72	28.4 	vpc-testing-eu-west-1-6-x7ifsgiaf2e5owslpwfj7d6yv4.eu-west-1.es.amazonaws.com
7	m6g 4xlarge	36	16	6144	576	64	32	1152	2304	3	108	28.4 	vpc-testing-eu-west-1-7-4ren6zzjmc5b2hfeiqlhkspnhi.eu-west-1.es.amazonaws.com
8	m6g 4xlarge	36	16	6144	576	64	32	1152	2304	3	144	21.3 	vpc-testing-eu-west-1-8-uwdaybb4hmlk5ksku5beulrwei.eu-west-1.es.amazonaws.com
9	c6g 8xlarge	36	32	3072	1152	64	32	1152	2304	2	36	56.9 	vpc-testing-eu-west-1-9-h5ndpswi7iyme24giikxeyh5um.eu-west-1.es.amazonaws.com
10	c6g 8xlarge	36	32	3072	1152	64	32	1152	2304	2	72	28.4 	vpc-testing-eu-west-1-10-aaucp476d7rccaczrpqtmid2ny.eu-west-1.es.amazonaws.com
11	c6g 8xlarge	36	32	3072	1152	64	32	1152	2304	3	108	28.4 	vpc-testing-eu-west-1-11-ktijdyoi2peyawmamymur6h5te.eu-west-1.es.amazonaws.com
12	c6g 8xlarge	36	32	3072	1152	64	32	1152	2304	3	144	21.3 	vpc-testing-eu-west-1-12-ljbta4rp5nkvxfpp5mqfyklgbq.eu-west-1.es.amazonaws.com
13	r6g 12xlarge	6	48	24576	288	384	32	192	2304	4	18	227.6 	vpc-testing-eu-west-1-13-gy4vuofa7zobisxbk7ke2fwxjq.eu-west-1.es.amazonaws.com
14	r6g 12xlarge	6	48	24576	288	384	32	192	2304	4	24	170.7 	vpc-testing-eu-west-1-14-4vscxkivyukx4l2ppx6e5ix56u.eu-west-1.es.amazonaws.com
15	r6g 12xlarge	6	48	24576	288	384	32	192	2304	4	30	136.5 	vpc-testing-eu-west-1-15-wz36crrailo55btozuxe2hjd7a.eu-west-1.es.amazonaws.com
16	r6g 12xlarge	6	48	24576	288	384	32	192	2304	2	18	113.8 	vpc-testing-eu-west-1-16-43qwesqryj4tpphp4f4ryqnsve.eu-west-1.es.amazonaws.com
17	r6g 12xlarge	6	48	24576	288	384	32	192	2304	2	24	85.3 	vpc-testing-eu-west-1-17-fuoj6ylgmji773nc4cnbkvxzmy.eu-west-1.es.amazonaws.com
18	r6g 12xlarge	6	48	24576	288	384	32	192	2304	2	36	56.9 	vpc-testing-eu-west-1-18-7gkx5mkefavfg7litdhk4jpyhm.eu-west-1.es.amazonaws.com
19	r6g 12xlarge	6	48	24576	288	384	32	192	2304	3	24	128.0 	vpc-testing-eu-west-1-19-lsu5kohyc6e4pjuqxhhaydwkn4.eu-west-1.es.amazonaws.com
20	r6g 12xlarge	6	48	24576	288	384	32	192	2304	3	36	85.3 	vpc-testing-eu-west-1-20-5guu2fr2cy7koq6yi6ax5pa5va.eu-west-1.es.amazonaws.com
21	r6g 12xlarge	6	48	24576	288	384	32	192	2304	3	48	64.0 	vpc-testing-eu-west-1-21-ssu5cfigxbfdhh42im4a3a3y7u.eu-west-1.es.amazonaws.com
'''

clusters_df = pd.read_csv(io.StringIO(csv_data), sep='\t')

# 函数：根据给定的endpoint更新ISM策略


def update_ism_policy(endpoint, rollover_size_tb):
    rollover_size_gb = rollover_size_tb * 1024  # 将TB单位转换为GB
    aos_url = f'https://{endpoint}'
    url = f'{aos_url}/_plugins/_ism/policies/test-index-waf-ism-policy'
    headers = {
        'Authorization': 'Basic YWRtaW46b0JbOiw4bVQ6dF4v',
        'Content-Type': 'application/json',
    }
    data = {
        "policy": {
            "description": "Index State Management Policy for index test-index-waf",
            "default_state": "hot",
            "schema_version": 1,
            "states": [
                {
                    "name": "hot",
                    "actions": [
                        {
                            "rollover": {
                                "min_size": f"{rollover_size_gb}gb",
                                "min_index_age": "24h"
                            },
                            "timeout": "24h",
                            "retry": {
                                "count": 5,
                                "delay": "5m"
                            }
                        }
                    ],
                    "transitions": [
                        {
                            "state_name": "warm",
                            "conditions": {
                                "min_index_age": "1s"
                            }
                        }
                    ]
                },
                {
                    "name": "warm",
                    "actions": [
                        {
                            "warm_migration": {},
                            "timeout": "24h",
                            "retry": {
                                "count": 5,
                                "delay": "1h"
                            }
                        }
                    ],
                    "transitions": [
                        {
                            "state_name": "cold",
                            "conditions": {
                                "min_index_age": "3d"
                            }
                        }
                    ]
                },
                {
                    "name": "cold",
                    "actions": [
                        {
                            "cold_migration": {
                                "timestamp_field": "@timestamp"
                            }
                        }
                    ],
                    "transitions": [
                        {
                            "state_name": "delete",
                            "conditions": {
                                "min_index_age": "30d"
                            }
                        }
                    ]
                },
                {
                    "name": "delete",
                    "actions": [
                        {
                            "cold_delete": {}
                        }
                    ]
                }
            ],
            "ism_template": {
                "index_patterns": [
                    "test-index-waf-*"
                ],
                "priority": 100
            }
        }
    }

    response = requests.put(url, headers=headers, data=json.dumps(data))
    print(f"Updating policy for {endpoint}: {response.status_code}")


# 读取CSV中的endpoint和rollover_size字段，并为每个集群调用update_ism_policy函数
for _, row in clusters_df.iterrows():
    endpoint = row['endpoint']
    rollover_size = row['rollover_size']
    update_ism_policy(endpoint, rollover_size)
