import boto3
import os
from databricks import sql
import time
from datetime import datetime
from ast import Delete, alias
from py_compile import _get_default_invalidation_mode
from re import A
from types import ClassMethodDescriptorType

from regex import B


{
    "policy": {
        "description": "rollover logs",
        "default_state": "rollover",
        "states": [
            {
                "name": "rollover",
                "actions": [
                    {
                        "rollover": {
                            "min_doc_count": 5,
                            "min_size": "5mb"
                        },
                        "retry": {
                            "count": 3,
                            "delay": "1m"
                        }
                    }
                ],
                "transitions": [
                    {
                        "state_name": "warm"
                    }
                ]
            },
            {
                "name": "warm",
                "actions": [{
                    "warm_migration": {},
                    "timeout": "1h",
                    "retry": {
                        "count": 3,
                        "delay": "10m"
                    }
                }],
                "transitions": [{
                    "state_name": "delete",
                    "conditions": {
                        "min_index_age": "12h"
                    }
                }]
            },
            {
                "name": "delete",
                "actions": [
                    {
                        "delete": {}
                    }
                ],
                "transitions": []
            }
        ],
        "ism_template": {
            "index_patterns": [
                "fluent-bit-*"
            ],
            "priority": 100
        }
    }
}

{
    "policy": {
        "description": "fluent-bit rollover policy.",
        "default_state": "rollover",
        "states": [
            {
                "name": "rollover",
                "actions": [{
                    "rollover": {
                        "min_doc_count": 100,
                        "min_size": "5mb"
                    },
                    "retry": {
                        "count": 3,
                        "delay": "1m"
                    }
                }],
                "transitions": [{
                    "state_name": "warm",
                    "conditions": {
                        "min_index_age": "2h"
                    }
                }]
            },
            {
                "name": "warm",
                "actions": [{
                    "warm_migration": {},
                    "timeout": "1h",
                    "retry": {
                        "count": 3,
                        "delay": "10m"
                    }
                }],
                "transitions": [{
                    "state_name": "delete",
                    "conditions": {
                        "min_index_age": "12h"
                    }
                }]
            }, {}]
    },
    "ism_template": {
        "index_patterns": [
            "fluent-bit-*"
        ],
        "priority": 100
    }
}


{"create": {}}
{"time": "2099-05-06T16:21:15.000Z",
    "message": "192.0.2.42 - - [06/May/2099:16:21:15 +0000] \"GET /images/bg.jpg HTTP/1.0\" 200 24736"}
{"create": {}}
{"time": "2099-05-06T16:25:42.000Z",
    "message": "192.0.2.255 - - [06/May/2099:16:25:42 +0000] \"GET /favicon.ico HTTP/1.0\" 200 3638"}


jdbc: databricks: // adb-1234567890123456.7.azuredatabricks.net: 443/default
transportMode = http
ssl = 1
httpPath = sql/protocolv1/o/1234567890123456/1234-567890-reef123
AuthMech = 3
UID = token
PWD = <personal-access-token >
jdbc: databricks: // adb-1234567890123456.7.azuredatabricks.net: 443/default
transportMode = http
ssl = 1
AuthMech = 3
httpPath = /sql/1.0/warehouses/a123456bcde7f890

jdbc: databricks: // dbc-784ceb03-9a2a.cloud.databricks.com: 443/default
transportMode = http
ssl = 1
AuthMech = 3
httpPath = /sql/1.0/endpoints/a9caec466af2c5f6
UID = token
PWD = dapia09eea614873f538925469275d6db881


# 海波
jdbc: databricks: // dbc-784ceb03-9a2a.cloud.databricks.com: 443/default
transportMode = http
ssl = 1
AuthMech = 3
httpPath = /sql/1.0/endpoints/a9caec466af2c5f6
UID = token
PWD = dapia09eea614873f538925469275d6db881


产品
partner 的 产品
自动化的guide
solution CSDC


LOG_DIR = "s3a://aws-glue-assets-835751346093-us-east-1"
AWS_ACCESS_KEY_ID = "AAKIA4FFVJP6W3B2PZ6HJ"
AWS_SECRET_ACCESS_KEY = "af6bn9sJ8N/RnS6nKl+2mIGRt6VYlde8w19QHGr7"

sudo docker run - itd - e SPARK_HISTORY_OPTS = "$SPARK_HISTORY_OPTS -Dspark.history.fs.logDirectory=$LOG_DIR -Dspark.hadoop.fs.s3a.access.key=$AWS_ACCESS_KEY_ID -Dspark.hadoop.fs.s3a.secret.key=$AWS_SECRET_ACCESS_KEY" - p 18080: 18080 jnshubham/glue_sparkui


# 个性化配置
LOG_DIR = "s3a://emr-log-bucket/default_job_flow_location"
SPARK_HISTORY_UI_PORT = 11888
GLOBAL_AWS_ACCESS_KEY_ID = "AAKIA4FFVJP6W3B2PZ6HJ"
GLOABAL_AWS_SECRET_ACCESS_KEY = "af6bn9sJ8N/RnS6nKl+2mIGRt6VYlde8w19QHGr7"

# 关闭 glue_spark_history_ui
nrm glue_spark_history_ui

# 开启 glue_spark_history_ui
sudo docker run - itd \
    - -name glue_spark_history_ui \
    - e SPARK_HISTORY_OPTS = "$SPARK_HISTORY_OPTS -Dspark.history.fs.logDirectory=$LOG_DIR -Dspark.hadoop.fs.s3a.access.key=$GLOBAL_AWS_ACCESS_KEY_ID -Dspark.hadoop.fs.s3a.secret.key=$GLOABAL_AWS_SECRET_ACCESS_KEY" \
    - p $SPARK_HISTORY_UI_PORT: 18080 \
    registry.cn-shanghai.aliyuncs.com/neo_docker/spark: glue_sparkui_3_0 \
    "/opt/spark/bin/spark-class org.apache.spark.deploy.history.HistoryServer"


echo "`date +'%Y-%m-%d %H:%M:%S'` 个性化配置"
LOG_DIR = "s3a://aws-glue-assets-835751346093-us-east-1/sparkHistoryLogs/"
SPARK_HISTORY_UI_PORT = 11188
IMAGE_URL = "registry.cn-shanghai.aliyuncs.com/neo_docker/spark:glue_sparkui_3_0"
echo "IMAGE_URL: $IMAGE_URL"
echo "LOG_DIR: $LOG_DIR"
echo "SPARK_HISTORY_UI_PORT: $SPARK_HISTORY_UI_PORT"

echo "`date +'%Y-%m-%d %H:%M:%S'` check ak/sk"
echo $GLOBAL_AWS_ACCESS_KEY_ID
echo $GLOBAL_AWS_SECRET_ACCESS_KEY

echo "`date +'%Y-%m-%d %H:%M:%S'`关闭 glue_spark_history_ui"
nrm glue_spark_history_ui

echo "`date +'%Y-%m-%d %H:%M:%S'` 开启 glue_spark_history_ui"
sudo docker run - itd \
    - -restart always \
    - -name glue_spark_history_ui \
    - e SPARK_HISTORY_OPTS = "$SPARK_HISTORY_OPTS -Dspark.history.fs.logDirectory=$LOG_DIR -Dspark.hadoop.fs.s3a.access.key=$GLOBAL_AWS_ACCESS_KEY_ID -Dspark.hadoop.fs.s3a.secret.key=$GLOBAL_AWS_SECRET_ACCESS_KEY" \
    - p $SPARK_HISTORY_UI_PORT: 18080 \
    $IMAGE_URL \
    "/opt/spark/bin/spark-class org.apache.spark.deploy.history.HistoryServer"


def get_s3_files(bucket_name, prefix, file_type):

    s3_resource = boto3.resource("s3")
    bucket = s3_resource.Bucket(bucket_name)
    s3_source_files = []
    for object in bucket.objects.filter(Prefix=prefix):
        if object.key.endswith(f".{file_type}"):
            s3_source_files.append(f"""{object.get()['Body'].read()}""")
    return s3_source_files


sql_files = get_s3_files(bucket_name='tpc-ds-james',
                         prefix='sql', file_type='sql')

with sql.connect('dbc-38cda7ff-9756.cloud.databricks.com',
                 '/sql/1.0/endpoints/78691485368c54eb',
                 'dapid0d07d579afdbad1d60e1f3c40b74892') as connection:

    for sql_str in sql_files:
        with connection.cursor() as cursor:
            try:
                cursor.execute('use tpcds1t;')
                lines = sql_str.split("\\n")
                name = lines[0]
                #sql_str = ' '.join(sql_str.split("\\n")[1:])[:-1]
                sql_str = ' '.join(lines[1:])[:-1]
                sql_str = sql_str.replace('\\t', ' ')
                #print(" sql :", sql_str)
                #cursor.execute('select *from item limit 10;')
                start = datetime.now().timestamp()
                cursor.execute(f"""{sql_str}""")
                result = cursor.fetchall()
                end = datetime.now().timestamp()
                print(str(name[2:]), ',', end - start, '|')
            except Exception as e:
                print(" error sql :", str(name), e)
