import requests
import json
import pandas as pd
import io

# 读取CSV文件内容
csv_data = '''
Cluster	node	num	node_cores	max_ebs	total_cores	node_memory	JVM_memory	total_JVM	total_memory	rollover_size	shard_num	shard_size	endpoint
1	r6g 2xlarge	36	8	6144	288	64	32	1152	2304	2	36	56.9 	vpc-testing-eu-west-1-1-edjukzf5yoqydnxg456seosqhu.eu-west-1.es.amazonaws.com
2	r6g 2xlarge	36	8	6144	288	64	32	1152	2304	2	72	28.4 	vpc-testing-eu-west-1-2-z5oybjovfrdkfach22mvnjzkia.eu-west-1.es.amazonaws.com
3	r6g 2xlarge	36	8	6144	288	64	32	1152	2304	3	108	28.4 	vpc-testing-eu-west-1-3-2tbfkjgzhhninojzojkdf3puey.eu-west-1.es.amazonaws.com
4	r6g 2xlarge	36	8	6144	288	64	32	1152	2304	3	144	21.3 	vpc-testing-eu-west-1-4-lc2uqncqvyjswmxkrfufagufvi.eu-west-1.es.amazonaws.com
5	m6g 4xlarge	36	16	6144	576	64	32	1152	2304	2	36	56.9 	vpc-testing-eu-west-1-5-4vfw6skkiahevop2yo5zb3su5a.eu-west-1.es.amazonaws.com
6	m6g 4xlarge	36	16	6144	576	64	32	1152	2304	2	72	28.4 	vpc-testing-eu-west-1-6-x7ifsgiaf2e5owslpwfj7d6yv4.eu-west-1.es.amazonaws.com
7	m6g 4xlarge	36	16	6144	576	64	32	1152	2304	3	108	28.4 	vpc-testing-eu-west-1-7-4ren6zzjmc5b2hfeiqlhkspnhi.eu-west-1.es.amazonaws.com
8	m6g 4xlarge	36	16	6144	576	64	32	1152	2304	3	144	21.3 	vpc-testing-eu-west-1-8-uwdaybb4hmlk5ksku5beulrwei.eu-west-1.es.amazonaws.com
9	c6g 8xlarge	36	32	3072	1152	64	32	1152	2304	2	36	56.9 	vpc-testing-eu-west-1-9-h5ndpswi7iyme24giikxeyh5um.eu-west-1.es.amazonaws.com
10	c6g 8xlarge	36	32	3072	1152	64	32	1152	2304	2	72	28.4 	vpc-testing-eu-west-1-10-aaucp476d7rccaczrpqtmid2ny.eu-west-1.es.amazonaws.com
11	c6g 8xlarge	36	32	3072	1152	64	32	1152	2304	3	108	28.4 	vpc-testing-eu-west-1-11-ktijdyoi2peyawmamymur6h5te.eu-west-1.es.amazonaws.com
12	c6g 8xlarge	36	32	3072	1152	64	32	1152	2304	3	144	21.3 	vpc-testing-eu-west-1-12-ljbta4rp5nkvxfpp5mqfyklgbq.eu-west-1.es.amazonaws.com
13	r6g 12xlarge	6	48	24576	288	384	32	192	2304	4	18	227.6 	vpc-testing-eu-west-1-13-gy4vuofa7zobisxbk7ke2fwxjq.eu-west-1.es.amazonaws.com
14	r6g 12xlarge	6	48	24576	288	384	32	192	2304	4	24	170.7 	vpc-testing-eu-west-1-14-4vscxkivyukx4l2ppx6e5ix56u.eu-west-1.es.amazonaws.com
15	r6g 12xlarge	6	48	24576	288	384	32	192	2304	4	30	136.5 	vpc-testing-eu-west-1-15-wz36crrailo55btozuxe2hjd7a.eu-west-1.es.amazonaws.com
16	r6g 12xlarge	6	48	24576	288	384	32	192	2304	2	18	113.8 	vpc-testing-eu-west-1-16-43qwesqryj4tpphp4f4ryqnsve.eu-west-1.es.amazonaws.com
17	r6g 12xlarge	6	48	24576	288	384	32	192	2304	2	24	85.3 	vpc-testing-eu-west-1-17-fuoj6ylgmji773nc4cnbkvxzmy.eu-west-1.es.amazonaws.com
18	r6g 12xlarge	6	48	24576	288	384	32	192	2304	2	36	56.9 	vpc-testing-eu-west-1-18-7gkx5mkefavfg7litdhk4jpyhm.eu-west-1.es.amazonaws.com
19	r6g 12xlarge	6	48	24576	288	384	32	192	2304	3	24	128.0 	vpc-testing-eu-west-1-19-lsu5kohyc6e4pjuqxhhaydwkn4.eu-west-1.es.amazonaws.com
20	r6g 12xlarge	6	48	24576	288	384	32	192	2304	3	36	85.3 	vpc-testing-eu-west-1-20-5guu2fr2cy7koq6yi6ax5pa5va.eu-west-1.es.amazonaws.com
21	r6g 12xlarge	6	48	24576	288	384	32	192	2304	3	48	64.0 	vpc-testing-eu-west-1-21-ssu5cfigxbfdhh42im4a3a3y7u.eu-west-1.es.amazonaws.com
'''

# 使用pandas读取CSV数据
clusters_df = pd.read_csv(io.StringIO(csv_data), sep='\t')

for _, row in clusters_df.iterrows():
    AOS_URL = f"https://{row['endpoint']}/_index_template/test-index-waf-template"

    headers = {
        "Authorization": "Basic YWRtaW46b0JbOiw4bVQ6dF4v",
        "Content-Type": "application/json",
    }

    data = {
        "index_patterns": ["test-index-waf-*"],
        "template": {
            "settings": {
                "index": {
                    "ultrawarm": {
                        "migration": {
                            "force_merge": {
                                "max_num_segments": "1000"
                            }
                        }
                    },
                    "codec": "best_compression",
                    "refresh_interval": "60s",
                    "number_of_shards": str(row['shard_num']),
                    "number_of_replicas": "1",
                    "plugins": {
                        "index_state_management": {
                            "rollover_alias": "test-index-waf"
                        }
                    },
                    "translog": {
                        "flush_threshold_size": "16g",
                        "sync_interval": "30s",
                        "durability": "async"}
                }
            }
        },
        "mappings":  {
            "properties": {
                "terminatingRuleId": {
                    "type": "keyword"
                },
                "terminatingRuleType": {
                    "type": "keyword"
                },
                "ruleGroupList": {
                    "properties": {
                        "terminatingRule": {
                            "properties": {
                                "action": {
                                    "type": "keyword"
                                },
                                "ruleId": {
                                    "type": "keyword"
                                }
                            }
                        },
                        "ruleGroupId": {
                            "type": "keyword"
                        }
                    }
                },
                "httpSourceId": {
                    "type": "keyword"
                },
                "labels": {
                    "properties": {
                        "name": {
                            "type": "text",
                            "fields": {
                                    "keyword": {
                                        "ignore_above": 256,
                                        "type": "keyword"
                                    }
                            }
                        }
                    }
                },
                "webaclId": {
                    "type": "text",
                    "fields": {
                            "keyword": {
                                "ignore_above": 256,
                                "type": "keyword"
                            }
                    }
                },
                "@timestamp": {
                    "path": "timestamp",
                    "type": "alias"
                },
                "webaclName": {
                    "type": "keyword"
                },
                "action": {
                    "type": "keyword"
                },
                "httpRequest": {
                    "properties": {
                        "args": {
                            "type": "text",
                            "fields": {
                                    "keyword": {
                                        "ignore_above": 256,
                                        "type": "keyword"
                                    }
                            }
                        },
                        "country": {
                            "type": "keyword"
                        },
                        "headers": {
                            "properties": {
                                "name": {
                                    "type": "keyword"
                                },
                                "value": {
                                    "type": "text",
                                    "fields": {
                                            "keyword": {
                                                "ignore_above": 256,
                                                "type": "keyword"
                                            }
                                    }
                                }
                            }
                        },
                        "httpVersion": {
                            "type": "keyword"
                        },
                        "requestId": {
                            "type": "text",
                            "fields": {
                                    "keyword": {
                                        "ignore_above": 256,
                                        "type": "keyword"
                                    }
                            }
                        },
                        "clientIp": {
                            "type": "ip"
                        },
                        "httpMethod": {
                            "type": "keyword"
                        },
                        "uri": {
                            "type": "text",
                            "fields": {
                                    "keyword": {
                                        "ignore_above": 256,
                                        "type": "keyword"
                                    }
                            }
                        }
                    }
                },
                "httpSourceName": {
                    "type": "keyword"
                },
                "formatVersion": {
                    "type": "keyword"
                },
                "timestamp": {
                    "format": "epoch_millis",
                    "type": "date"
                }
            }
        }
    }

    response = requests.put(AOS_URL, headers=headers, data=json.dumps(data))
    if response.status_code != 200:
        print(
            f"Error updating index template for {row['endpoint']}: {response.text}")
