# 修改 CLUSTER_NAME
create_multi_aos_clusters() {
    list=("r6g.2xlarge.search" "r6g.4xlarge.search" "r6g.8xlarge.search" "r6g.4xlarge.search" "c6g.8xlarge.search")

    # 遍历列表并打印元素
    for i in "${list[@]}"; do
        # echo "$i"
        CLUSTER_NAME=${1:-llm}
        OPENSEARCH_CLUSTER_NAME=$CLUSTER_NAME-${i//./-}                                             # 替换.-
        echo $OPENSEARCH_CLUSTER_NAME                                                               # 集群name
        OPENSEARCH_LATEST_VERSION=$(aws opensearch list-versions | jq '.Versions[0]' | pass_quotes) # opensearch version OpenSearch_2.3,Elasticsearch_7.10

        OPENSEARCH_INSTANCE_TYPE=$i                       # data node type 替换.-
        OPENSEARCH_INSTANCE_NUM=2                         # data node  num
        OPENSEARCH_DedicatedMasterEnabled=true            # 启动多主
        OPENSEARCH_ZoneAwarenessEnabled=true              # 启用区域感知
        OPENSEARCH_AvailabilityZoneCount=2                # 可用区数量
        OPENSEARCH_DedicatedMasterType="m6g.large.search" # 主节点配置
        OPENSEARCH_DedicatedMasterCount=3                 # 主节点数量

        OPENSEARCH_EBS_EBSEnabled=true
        OPENSEARCH_EBS_VolumeType="gp3"
        OPENSEARCH_EBS_VolumeSize=2048
        OPENSEARCH_EBS_Iops=16000
        OPENSEARCH_EBS_Throughput=593

        OPENSEARCH_Ids=subnet-0a8561b20a5c71ac9,subnet-0d2375de41fbda3e1
        OPENSEARCH_SecurityGroupIds=sg-0708b2efbc03b7d46

        # AOS_ROOT=
        # AOS_PASSWORD=

        # ------

        aws opensearch create-domain \
            --domain-name $OPENSEARCH_CLUSTER_NAME \
            --engine-version $OPENSEARCH_LATEST_VERSION \
            --cluster-config InstanceType=$OPENSEARCH_INSTANCE_TYPE,InstanceCount=$OPENSEARCH_INSTANCE_NUM,DedicatedMasterEnabled=$OPENSEARCH_DedicatedMasterEnabled,ZoneAwarenessEnabled=$OPENSEARCH_ZoneAwarenessEnabled,ZoneAwarenessConfig={AvailabilityZoneCount=$OPENSEARCH_AvailabilityZoneCount},DedicatedMasterType=$OPENSEARCH_DedicatedMasterType,DedicatedMasterCount=$OPENSEARCH_DedicatedMasterCount,WarmEnabled=$OPENSEARCH_WarmEnabled,WarmType=$OPENSEARCH_WarmType,WarmCount=$OPENSEARCH_WarmCount,ColdStorageOptions={Enabled=$OPENSEARCH_ColdStorageOptions} \
            --ebs-options EBSEnabled=$OPENSEARCH_EBS_EBSEnabled,VolumeType=$OPENSEARCH_EBS_VolumeType,VolumeSize=$OPENSEARCH_EBS_VolumeSize,Iops=$OPENSEARCH_EBS_Iops,Throughput=$OPENSEARCH_EBS_Throughput \
            --vpc-options SubnetIds=$OPENSEARCH_Ids,SecurityGroupIds=$OPENSEARCH_SecurityGroupIds \
            --cognito-options Enabled=false \
            --node-to-node-encryption-options Enabled=true \
            --encryption-at-rest-options Enabled=true \
            --access-policies '{"Version":"2012-10-17","Statement":[{"Effect":"Allow","Principal":{"AWS":"*"},"Action":"es:*","Resource":"arn:aws:es:us-east-1:835751346093:domain/*/*"},{"Effect":"Allow","Principal":{"AWS":"*"},"Action":"es:ESCrossClusterGet","Resource":"arn:aws:es:us-east-1:835751346093:domain/*"}]}' \
            --domain-endpoint-options EnforceHTTPS=true,TLSSecurityPolicy="Policy-Min-TLS-1-2-2019-07" \
            --advanced-security-options "Enabled=true,InternalUserDatabaseEnabled=true,MasterUserOptions={MasterUserName=$AOS_ROOT,MasterUserPassword=$AOS_PASSWORD}"

    done

}

for i in $(cat aos_cluster.list); do
    AOS_URL="https://$i"
    curl --request PUT \
        --url $AOS_URL/_cluster/settings \
        --header 'Authorization: Basic cm9vdDpBV1MydGhlbW9vbjEwMCQ=' \
        --header 'Content-Type: application/json' \
        --data '{
"transient": {
"knn.memory.circuit_breaker.limit": "70%",
"knn.algo_param.index_thread_qty": 8
}
}
'
done


{
"transient": {
"knn.memory.circuit_breaker.limit": "70%",
"knn.algo_param.index_thread_qty": 4
}
}