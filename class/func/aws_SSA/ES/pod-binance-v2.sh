create_multi_aos_clusters() {
    CLUSTER_NUM=${1:-21}
    CLUSTER_NAME=${2:-poc-binance-v3}
    for i in $(seq 1 $CLUSTER_NUM); do
        echo $CLUSTER_NAME-$i
        OPENSEARCH_CLUSTER_NAME=$CLUSTER_NAME-$i                                                    # 集群name
        OPENSEARCH_LATEST_VERSION=$(aws opensearch list-versions | jq '.Versions[0]' | pass_quotes) # opensearch version OpenSearch_2.3,Elasticsearch_7.10

        OPENSEARCH_INSTANCE_TYPE="r6g.12xlarge.search"      # data node type
        OPENSEARCH_INSTANCE_NUM=8                           # data node  num
        OPENSEARCH_DedicatedMasterEnabled=true              # 启动多主
        OPENSEARCH_ZoneAwarenessEnabled=true                # 启用区域感知
        OPENSEARCH_AvailabilityZoneCount=2                  # 可用区数量
        OPENSEARCH_DedicatedMasterType="m6g.2xlarge.search" # 主节点配置
        OPENSEARCH_DedicatedMasterCount=3                   # 主节点数量
        OPENSEARCH_WarmEnabled=true                         # 是否启用ultrawarm true|false
        OPENSEARCH_WarmType="ultrawarm1.large.search"       # "WarmType": "ultrawarm1.medium.search"|"ultrawarm1.large.search"|"ultrawarm1.xlarge.search"
        OPENSEARCH_WarmCount=20                             # 2~150
        OPENSEARCH_ColdStorageOptions=true

        OPENSEARCH_EBS_EBSEnabled=true
        OPENSEARCH_EBS_VolumeType="gp3"
        OPENSEARCH_EBS_VolumeSize=24576
        OPENSEARCH_EBS_Iops=16000
        OPENSEARCH_EBS_Throughput=1000

        OPENSEARCH_Ids=subnet-0a8561b20a5c71ac9,subnet-0d2375de41fbda3e1
        OPENSEARCH_SecurityGroupIds=sg-0708b2efbc03b7d46

        # AOS_ROOT=
        # AOS_PASSWORD=

        # ------

        aws opensearch create-domain \
            --domain-name $OPENSEARCH_CLUSTER_NAME \
            --engine-version $OPENSEARCH_LATEST_VERSION \
            --cluster-config InstanceType=$OPENSEARCH_INSTANCE_TYPE,InstanceCount=$OPENSEARCH_INSTANCE_NUM,DedicatedMasterEnabled=$OPENSEARCH_DedicatedMasterEnabled,ZoneAwarenessEnabled=$OPENSEARCH_ZoneAwarenessEnabled,ZoneAwarenessConfig={AvailabilityZoneCount=$OPENSEARCH_AvailabilityZoneCount},DedicatedMasterType=$OPENSEARCH_DedicatedMasterType,DedicatedMasterCount=$OPENSEARCH_DedicatedMasterCount,WarmEnabled=$OPENSEARCH_WarmEnabled,WarmType=$OPENSEARCH_WarmType,WarmCount=$OPENSEARCH_WarmCount,ColdStorageOptions={Enabled=$OPENSEARCH_ColdStorageOptions} \
            --ebs-options EBSEnabled=$OPENSEARCH_EBS_EBSEnabled,VolumeType=$OPENSEARCH_EBS_VolumeType,VolumeSize=$OPENSEARCH_EBS_VolumeSize,Iops=$OPENSEARCH_EBS_Iops,Throughput=$OPENSEARCH_EBS_Throughput \
            --vpc-options SubnetIds=$OPENSEARCH_Ids,SecurityGroupIds=$OPENSEARCH_SecurityGroupIds \
            --cognito-options Enabled=false \
            --node-to-node-encryption-options Enabled=true \
            --encryption-at-rest-options Enabled=true \
            --access-policies '{"Version":"2012-10-17","Statement":[{"Effect":"Allow","Principal":{"AWS":"*"},"Action":"es:*","Resource":"arn:aws:es:us-east-1:835751346093:domain/*/*"},{"Effect":"Allow","Principal":{"AWS":"*"},"Action":"es:ESCrossClusterGet","Resource":"arn:aws:es:us-east-1:835751346093:domain/*"}]}' \
            --domain-endpoint-options EnforceHTTPS=true,TLSSecurityPolicy="Policy-Min-TLS-1-2-2019-07" \
            --advanced-security-options "Enabled=true,InternalUserDatabaseEnabled=true,MasterUserOptions={MasterUserName=$AOS_ROOT,MasterUserPassword=$AOS_PASSWORD}"

    done

}

create-multi_outbound-connection() {
    CLUSTER_NUM=${1:-20}
    CLUSTER_NAME=${2:-poc-binance-v2}
    INBOUND_CLUSTER=${3:-poc-binance-v2-21}
    for i in $(seq 1 $CLUSTER_NUM); do
        echo $CLUSTER_NAME-$i
        OPENSEARCH_CLUSTER_NAME=$CLUSTER_NAME-$i
        ACOUNT_ID=835751346093
        REGION_NAME="us-east-1"
        # LOCAL_AOS_NAME 是 dashboard cluster
        LOCAL_AOS_NAME=$INBOUND_CLUSTER
        REMOTE_AOS_NAME=$OPENSEARCH_CLUSTER_NAME

        CONNECTION_ALIAS="${LOCAL_AOS_NAME}_connect_${REMOTE_AOS_NAME}"

        aws opensearch create-outbound-connection \
            --local-domain-info "AWSDomainInformation={OwnerId=$ACOUNT_ID,DomainName=$LOCAL_AOS_NAME,Region=us-east-1}" \
            --remote-domain-info "AWSDomainInformation={OwnerId=$ACOUNT_ID,DomainName=$REMOTE_AOS_NAME,Region=us-east-1}" \
            --connection-alias $CONNECTION_ALIAS
    done
}

# 按需修改 "%poc-binance-21%"'
ConnectionId_LIST=$(aws opensearch describe-outbound-connections |
    jq '[.Connections[] | select(.ConnectionStatus.StatusCode == "PENDING_ACCEPTANCE") | {ConnectionId, ConnectionAlias} ]' |
    json2csv |
    csvq -f json 'select ConnectionId  where ConnectionAlias like "%poc-binance-v2-21%"' |
    jq -c '.[] | .ConnectionId' |
    sed 's/"//g')
echo $ConnectionId_LIST >list.txt
for i in $(cat list.txt); do aws opensearch accept-inbound-connection --connection-id $i; done

CLUSTER_PREFIX="poc-binance-v2"
echo >aos_cluster.list
for i in $(aws opensearch list-domain-names | jq ' .DomainNames[].DomainName ' | pass_quotes | grep $CLUSTER_PREFIX); do
    aws opensearch describe-domains --domain-name $i | jq ".DomainStatusList[] | .Endpoints.vpc" | pass_quotes >>aos_cluster.list
done

cat aos_cluster.list

#---

for i in $(cat aos_cluster.list); do
    AOS_URL="https://$i"
    curl --request PUT \
        --url $AOS_URL/_index_template/loghub-v1-binance_ism_rollover \
        --header 'Authorization: Basic cm9vdDpBV1MydGhlbW9vbjEwMCQ=' \
        --header 'Content-Type: application/json' \
        --data '{
    "index_patterns":[
        "loghub-v1-binance*"
    ],
    "template":{
        "settings":{
            "number_of_shards":24,
            "number_of_replicas":1,
            "refresh_interval" : "60s",
            "codec": "best_compression",
            "plugins.index_state_management.rollover_alias":"loghub-v1-binance",
            "translog" : {
                "flush_threshold_size" : "64g",
                "sync_interval" : "30s",
                "durability" : "async"}
        },
    "mappings": {
        "properties": {
            "@timestamp": {
                "type": "alias",
                "path": "timestamp"
            },
            "action": {
                "type": "keyword"
            },
            "formatVersion": {
                "type": "keyword"
            },
            "httpRequest": {
                "properties": {
                    "args": {
                        "type": "text",
                        "fields": {
                            "keyword": {
                                "type": "keyword",
                                "ignore_above": 256
                            }
                        }
                    },
                    "clientIp": {
                        "type": "ip"
                    },
                    "country": {
                        "type": "keyword"
                    },
                    "headers": {
                        "properties": {
                            "name": {
                                "type": "keyword"
                            },
                            "value": {
                                "type": "text",
                                "fields": {
                                    "keyword": {
                                        "type": "keyword",
                                        "ignore_above": 256
                                    }
                                }
                            }
                        }
                    },
                    "httpMethod": {
                        "type": "keyword"
                    },
                    "httpVersion": {
                        "type": "keyword"
                    },
                    "requestId": {
                        "type": "text",
                        "fields": {
                            "keyword": {
                                "type": "keyword",
                                "ignore_above": 256
                            }
                        }
                    },
                    "uri": {
                        "type": "text",
                        "fields": {
                            "keyword": {
                                "type": "keyword",
                                "ignore_above": 256
                            }
                        }
                    }
                }
            },
            "httpSourceId": {
                "type": "keyword"
            },
            "httpSourceName": {
                "type": "keyword"
            },
            "labels": {
                "properties": {
                    "name": {
                        "type": "text",
                        "fields": {
                            "keyword": {
                                "type": "keyword",
                                "ignore_above": 256
                            }
                        }
                    }
                }
            },
            "ruleGroupList": {
                "properties": {
                    "ruleGroupId": {
                        "type": "keyword"
                    },
                    "terminatingRule": {
                        "properties": {
                            "action": {
                                "type": "keyword"
                            },
                            "ruleId": {
                                "type": "keyword"
                            }
                        }
                    }
                }
            },
            "terminatingRuleId": {
                "type": "keyword"
            },
            "terminatingRuleType": {
                "type": "keyword"
            },
            "timestamp": {
                "type": "date",
                "format": "epoch_millis"
            },
            "webaclId": {
                "type": "text",
                "fields": {
                    "keyword": {
                        "type": "keyword",
                        "ignore_above": 256
                    }
                }
            },
            "webaclName": {
                "type": "keyword"
            }
        }
    }
    }
}'
done





for i in $(cat aos_cluster.list); do
    AOS_URL="https://$i"
    curl --request PUT \
        --url $AOS_URL/_plugins/_ism/policies/loghub-v1-binance_ism_rollover_policy \
        --header 'Authorization: Basic cm9vdDpBV1MydGhlbW9vbjEwMCQ=' \
        --header 'Content-Type: application/json' \
        --data '{
    "policy": {
        "description": "loghub-v1-binance_ism_rollover_policy log set lifecycle.",
        "default_state": "rollover",
        "states": [{
                "name": "rollover",
                "actions": [{
                    "rollover": {
                    "min_size": "800GB"
                    },
                    "retry": {
                        "count": 3,
                        "delay": "1m"
                    }
                }],
                "transitions": [{
                    "state_name": "hot_2_warm",
                    "conditions": {
                        "min_index_age": "1m"
                    }
                }]
            },
            {
                "name": "hot_2_warm",
                "actions": [{
                    "warm_migration": {},
                    "timeout": "24h",
                    "retry": {
                        "count": 3,
                        "delay": "10m"
                    }
                }],
                "transitions": [{
                    "state_name": "delete",
                    "conditions": {
                        "min_index_age": "14d"
                    }
                }]
            },
            {
                "name": "delete",
                "actions": [{
                    "delete": {}
                }]
            }
        ],
        "ism_template": {
      "index_patterns": [
        "loghub-v1-binance*"
],
      "priority": 100
    }
    }
}'
done