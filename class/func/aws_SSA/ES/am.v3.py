# Copyright Amazon.com, Inc. or its affiliates. All Rights Reserved.
# SPDX-License-Identifier: Apache-2.0

import json
import logging
import os
import boto3
from botocore import config
import uuid
import requests
from requests_aws4auth import AWS4Auth
import time
from decimal import Decimal
from datetime import datetime
from boto3.dynamodb.conditions import Key, Attr

logger = logging.getLogger()
logger.setLevel(logging.INFO)

DEFAULT_TIME_FORMAT = "%Y-%m-%dT%H:%M:%SZ"

stack_prefix = os.environ.get("STACK_PREFIX", "CL")
solution_version = os.environ.get("SOLUTION_VERSION", "v1.0.0")
solution_id = os.environ.get("SOLUTION_ID", "SO8025")
user_agent_config = {
    "user_agent_extra": f"AwsSolution/{solution_id}/{solution_version}"
}
default_config = config.Config(**user_agent_config)
default_region = os.environ.get("AWS_REGION")

awslambda = boto3.client("lambda", config=default_config)
dynamodb = boto3.resource("dynamodb", config=default_config)

clusterinfo_table_name = os.environ.get("CLUSTERINFO_TABLE_NAME")
statistics_table_name = os.environ.get("STATISTICS_TABLE_NAME")

clusterinfo_table = dynamodb.Table(clusterinfo_table_name)
statistics_table = dynamodb.Table(statistics_table_name)

request_headers = {"Content-Type": "application/json"}

service = "es"
credentials = boto3.Session().get_credentials()
awsauth = AWS4Auth(
    credentials.access_key,
    credentials.secret_key,
    default_region,
    service,
    session_token=credentials.token,
)


class ErrorCode:
    DUPLICATED_INDEX_PREFIX = "DuplicatedIndexPrefix"
    DUPLICATED_WITH_INACTIVE_INDEX_PREFIX = "DuplicatedWithInactiveIndexPrefix"
    OVERLAP_INDEX_PREFIX = "OverlapIndexPrefix"
    OVERLAP_WITH_INACTIVE_INDEX_PREFIX = "OverlapWithInactiveIndexPrefix"
    INVALID_INDEX_MAPPING = "InvalidIndexMapping"


class APIException(Exception):
    def __init__(self, message, code: str = None):
        if code:
            super().__init__("[{}] {}".format(code, message))
        else:
            super().__init__(message)


def handle_error(func):
    """Decorator for exception handling"""

    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except APIException as e:
            logger.exception(e)
            raise e
        except Exception as e:
            logger.exception(e)
            raise RuntimeError(
                "Unknown exception, please check Lambda log for more details"
            )

    return wrapper



# @handle_error
def lambda_handler(event, context):
    logger.info("Start to get monitor rollover size data of all AOS clusters!")
    current_time = datetime.now()
    # Get all items from cluster info table.
    response = clusterinfo_table.scan()
    items = response['Items']
    while 'LastEvaluatedKey' in response:
        response = clusterinfo_table.scan(
            ExclusiveStartKey=response['LastEvaluatedKey'])
        items.extend(response['Items'])
    aos_list = []
    for item in items:

        # Status 3 means the cluster is ready to write data.
        if int(item["cluster_status"]) < 3 or int(item["cluster_status"]) > 4:
            continue
        # Filter cluster endpoint url when it is empty
        if item["cluster_url"] == "" or item["cluster_url"] is None:
            continue

        aos_list.append(
            {"domain_name": item["cluster_name"], "endpoint": item["cluster_url"]})



    logger.info(f"🚀🚀🚀🚀🚀🚀🚀🚀Adding rollover logic for maintenance")
    logger.info(f"🚀🚀🚀🚀🚀🚀🚀🚀aos_list:{aos_list}")

    index_alias_name = "test-index-waf"
    primary_shard_max_size = 600
    policy_name = "test-index-waf-ism-policy"


    for one in aos_list:
        logger.info(one)
        logger.info(f"endpoint:{one['endpoint']}")
      
        logger.info(f"🚀🚀🚀🚀🚀🚀🚀🚀get alias的index name")
        alias_real_index_name_request_url = one["endpoint"] + '/_cat/aliases?format=json'
        logger.info(alias_real_index_name_request_url)
        response = requests.get(alias_real_index_name_request_url, auth=awsauth, headers=request_headers)
        logger.info(response)
        # logger.info(len(response.json()))
        # logger.info(response)
        # logger.info([i['index'] for i in response.json() if i['is_write_index'] == "true" and i['alias'] == index_alias_name ])
        index_name = [i['index'] for i in response.json() if i['is_write_index'] == "true" and i['alias'] == index_alias_name ][0]
        logger.info(f"index_name:{index_name}")
        
        
        logger.info(f"🚀🚀🚀🚀🚀🚀🚀🚀Check if the primary shards of an index exceed the predetermined size limit: {primary_shard_max_size}gb")
        primary_shard_size_request_url = one["endpoint"] + f'/_cat/indices/{index_name}?format=json&bytes=g'
        logger.info(primary_shard_size_request_url)
        response = requests.get(primary_shard_size_request_url, auth=awsauth, headers=request_headers)
        logger.info(response)
        logger.info([i['pri.store.size'] for i in response.json()])
        primary_shard_size = [i['pri.store.size'] for i in response.json()][0]
        logger.info(f"primary_shard_size:{primary_shard_size}")
        
        if int(primary_shard_size) > primary_shard_max_size:
            logger.info(f"🚀🚀🚀🚀🚀🚀🚀🚀start rollover")
            rollover_request_url = one["endpoint"] + f'/{index_alias_name}/_rollover'
            response = requests.post(rollover_request_url, auth=awsauth, headers=request_headers)
            logger.info(response)
            logger.info(response.json())
            
            logger.info(f"🚀🚀🚀🚀🚀🚀🚀🚀start remove index-policy")
            rollover_request_url = one["endpoint"] + f'/_plugins/_ism/remove/{index_name}'
            logger.info(rollover_request_url) 
            response = requests.post(rollover_request_url, auth=awsauth, headers=request_headers)
            logger.info(response)
            logger.info(response.json())
            
            logger.info(f"🚀🚀🚀🚀🚀🚀🚀🚀start add index-policy")
            # rollover_request_url = one["endpoint"] + f'/_plugins/_ism/add/{index_alias_name}-*'
            rollover_request_url = one["endpoint"] + f'/_plugins/_ism/add/{index_name}'
            logger.info(rollover_request_url) 
            response = requests.post(rollover_request_url, auth=awsauth, headers=request_headers, data=json.dumps({"policy_id": policy_name}))
            logger.info(response)            
            logger.info(response.json())
            
            # logger.info(f"🚀🚀🚀🚀🚀🚀🚀🚀get policy name")
            # policy_request_url = one["endpoint"] + f'/_plugins/_ism/policies'
            # logger.info(policy_request_url)
            # response = requests.get(policy_request_url, auth=awsauth, headers=request_headers)
            # logger.info(response) 
            # logger.info(response.json())
            
            
        else:
            logger.info(f"🚀🚀🚀🚀🚀🚀🚀🚀 The critical rollover condition has not been reached. pass")



    logger.info(f"🚀🚀🚀🚀🚀🚀🚀Adding rollover logic for maintenance finished ✅")
