from pptx import Presentation
import sys
import os

def remove_notes_from_ppt(input_ppt, output_ppt):
    # 打开现有的PPT
    prs = Presentation(input_ppt)
    
    # 遍历每一张幻灯片，删除notes部分
    for slide in prs.slides:
        if slide.has_notes_slide:
            notes_slide = slide.notes_slide
            notes_slide.notes_text_frame.clear()  # 清除notes中的所有内容

    # 保存修改后的PPT
    prs.save(output_ppt)
    print(f"New PPT without notes saved as: {output_ppt}")

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: python remove_notes.py <input_ppt> <output_ppt>")
    else:
        input_ppt = sys.argv[1]
        output_ppt = sys.argv[2]
        
        if not os.path.exists(input_ppt):
            print(f"File {input_ppt} does not exist.")
        else:
            remove_notes_from_ppt(input_ppt, output_ppt)