bin/kafka-configs.sh \
--bootstrap-server $bsrv \
--alter --entity-type topics \
--entity-name msk-ts-topic \
--add-config 'remote.storage.enable=true, local.retention.ms=604800000, retention.ms=15550000000, segment.bytes=50331648'

guaranteed-consumption-of-exactly-once-during-consumers-rolling-updates