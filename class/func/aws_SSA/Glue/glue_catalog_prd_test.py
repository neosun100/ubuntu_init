import boto3

glue_client = boto3.client('glue')

# 创建生产环境数据库
try:
    prod_db = glue_client.create_database(
        DatabaseInput={
            'Name': 'my_db',
            'Description': 'My production database',
            'Parameters': {
                'env': 'prod'
            }
        }
    )
    print(f"Created production database: {prod_db['ResponseMetadata']['HTTPStatusCode']}")
except glue_client.exceptions.AlreadyExistsException:
    print("Production database already exists, skipping creation.")

# 创建测试环境数据库
try:
    test_db = glue_client.create_database(
        DatabaseInput={
            'Name': 'my_db',
            'Description': 'My test database',
            'Parameters': {
                'env': 'test'
            }
        }
    )
    print(f"Created test database: {test_db['ResponseMetadata']['HTTPStatusCode']}")
except glue_client.exceptions.AlreadyExistsException:
    print("Test database already exists, skipping creation.")

# 获取数据库参数
db_name = 'my_db'
db_details = glue_client.get_database(Name=db_name)
db_params = db_details['Database']['Parameters']
print(f"Database '{db_name}' parameters: {db_params}")