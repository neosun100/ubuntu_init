# 此文件在判断为 树莓派系统的前提下执行
# 在 .bashrc 添加



echo "";
echo "";
echo "🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 ";
echo "`date +'%Y-%m-%d %H:%M:%S'` 更新 and 验证";
sudo apt-get update;


echo "";
echo "";
echo "🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 ";
echo "`date +'%Y-%m-%d %H:%M:%S'` 配置docker";
sudo apt-get install \
     apt-transport-https \
     ca-certificates \
     curl \
     gnupg2 \
     software-properties-common;
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -;
sudo apt-key fingerprint 0EBFCD88;
echo "deb [arch=armhf] https://download.docker.com/linux/debian $(lsb_release -cs) stable" | \
    sudo tee /etc/apt/sources.list.d/docker.list


echo "";
echo "";
echo "🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 ";
# https://segmentfault.com/a/1190000017109351?utm_source=tag-newest
# http://shumeipai.nxez.com/2019/05/20/how-to-install-docker-on-your-raspberry-pi.html
echo "`date +'%Y-%m-%d %H:%M:%S'` 更新 安装 docker";
sudo apt-get update
echo y | sudo apt-get install docker-ce;

# echo "";
# echo "";
# echo "🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 ";
# # https://www.cnblogs.com/ilym/p/8387561.html
# echo "`date +'%Y-%m-%d %H:%M:%S'` 解决 E: Sub-process /usr/bin/dpkg returned an error code (1) 的错误";
# cd /var/lib/dpkg;
# sudo mv info info.baksudo;
# sudo mkdir info;
# sudo apt-get update;
# echo y | sudo apt-get install docker-ce;


echo "";
echo "";
echo "🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 ";
echo "`date +'%Y-%m-%d %H:%M:%S'`  显示 docker 版本";
sudo docker version;


echo "";
echo "";
echo "🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 ";
echo "`date +'%Y-%m-%d %H:%M:%S'`    设置 docker 开机启动";
#重启 systemctl 守护进程
sudo systemctl daemon-reload
#设置 Docker 开机启动
sudo systemctl enable docker
#开启 Docker 服务
sudo systemctl start docker

echo "";
echo "";
echo "🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 ";
echo "`date +'%Y-%m-%d %H:%M:%S'` 设置 docker 图形管理界面";
#下载 Docker 图形化界面 portainer
sudo docker pull portainer/portainer;
#创建 portainer 容器
echo "sudo docker volume create portainer_data";
echo "volume也是绕过container的文件系统，直接将数据写到host机器上，只是volume是被docker管理的";
echo "docker下所有的volume都在host机器上的指定目录下/var/lib/docker/volumes";
sudo docker volume create portainer_data;
#运行 portainer
# sudo docker run -d -p 9000:9000 \
# --name portainer \
# --restart always \
# -v /var/run/docker.sock:/var/run/docker.sock \
# -v portainer_data:/data portainer/portainer

sudo docker run -d -p 9000:9000 \
--name portainer \
--restart always \
-v $(realpath /var/run/docker.sock):/var/run/docker.sock \
-v $(realpath portainer_data):/data portainer/portainer;


echo "";
echo "";
echo "🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 🏀 ";
echo "`date +'%Y-%m-%d %H:%M:%S'`    docker 图形管理web建立完成";
echo "请访问： `iip`:9000";

kill_www-data;