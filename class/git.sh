# git  相关的命令

git_passwd() {
    echo "永久记住git账户密码"
    git config --global credential.helper store
}

# 在本目录下 clone 所有的 loop 项目
git_clone_all_loop() {
    cd ~/code/loop-project &&
        git clone http://gitlab.9tong.com/iquant/wallet &&
        git clone http://gitlab.9tong.com/sunjian/imitate &&
        git clone http://gitlab.9tong.com/iquant/admin &&
        git clone http://gitlab.9tong.com/iquant/dash &&
        git clone http://gitlab.9tong.com/iquant/ios &&
        git clone http://gitlab.9tong.com/iquant/android &&
        git clone http://gitlab.9tong.com/iquant/holiday &&
        git clone http://gitlab.9tong.com/iquant/loop &&
        git clone http://gitlab.9tong.com/iquant/scheduler &&
        git clone http://gitlab.9tong.com/iquant/monitor &&
        git clone http://gitlab.9tong.com/iquant/elasticsearch &&
        git clone http://gitlab.9tong.com/iquant/radar &&
        git clone http://gitlab.9tong.com/iquant/eniac &&
        git clone http://gitlab.9tong.com/iquant/stock &&
        git clone http://gitlab.9tong.com/iquant/config &&
        git clone http://gitlab.9tong.com/iquant/base &&
        git clone http://gitlab.9tong.com/iquant/user &&
        git clone http://gitlab.9tong.com/iquant/message &&
        git clone http://gitlab.9tong.com/iquant/gateway &&
        git clone http://gitlab.9tong.com/iquant/spider &&
        git clone http://gitlab.9tong.com/iquant/wechat &&
        git clone http://gitlab.9tong.com/iquant/ws &&
        git clone http://gitlab.9tong.com/loopbook-ios-group/loopbook-ios &&
        git clone http://gitlab.9tong.com/iquant/web-h5 &&
        git clone http://gitlab.9tong.com/iquant/chart &&
        git clone http://gitlab.9tong.com/iquant/sgin_pipline &&
        git clone http://gitlab.9tong.com/iquant/loopx &&
        git clone http://gitlab.9tong.com/zhangyin/loopflutter &&
        git clone http://gitlab.9tong.com/iquant/loop_coin &&
        git clone http://gitlab.9tong.com/hqp/umeng &&
        git clone http://gitlab.9tong.com/hqp/bigdata &&
        git clone http://gitlab.9tong.com/sunjian/hqp-dw
}

# 一次性更新文件下的所有git项目
git_pull_all() {
    git config --global credential.helper store
    echo "同步本目录下的所有git项目"
    sleep 5
    sh $(echo ~)/upload/ubuntu_init/class/func/update_git_repos.sh
}

# loop项目git全更新
git_pull_loop() {
    cd ~/code/loop-project
    git_pull_all
    cd ~/upload
}

# baozun项目git全更新
git_pull_baozun() {
    cd ~/Code/baozun-project
    git_pull_all
    cd ~/upload
}

git_pull_syn_online() {
    echo 'cd 到目录下执行'
    sleep 3
    git fetch --all && git reset --hard origin/master && git pull
}

git_pull_hexo_oneline() {
    cd ~/upload/neoBlog/source
    git fetch --all && git reset --hard origin/master && git pull
    cd ~/upload
    echo "同步hexo md文件完毕" | lcat
}

git_clone_all_baozun() {
    cd ~/Code/baozun-project &&
        # /usr/bin/git clone https://git.baozun.com/big-data-platform/edw-dwd && \
        # /usr/bin/git clone https://git.baozun.com/big-data-platform/redash-server && \
        /usr/bin/git clone https://git.baozun.com/big-data-platform/flink-pioneer-taskmanager &&
        /usr/bin/git clone https://git.baozun.com/big-data-platform/flink-pioneer-jobmanager &&
        /usr/bin/git clone https://git.baozun.com/big-data-platform/processing-scripts &&
        /usr/bin/git clone https://git.baozun.com/big-data-platform/rmq-hive &&
        /usr/bin/git clone https://git.baozun.com/big-data-platform/edw-dws &&
        /usr/bin/git clone https://git.baozun.com/big-data-platform/edw-ads &&
        /usr/bin/git clone https://git.baozun.com/big-data-platform/dp-dataplatform &&
        /usr/bin/git clone https://git.baozun.com/big-data-platform/kafka-connect &&
        /usr/bin/git clone https://git.baozun.com/big-data-platform/dp-authority &&
        /usr/bin/git clone https://git.baozun.com/big-data-platform/dp-dataplatform-ui &&
        /usr/bin/git clone https://git.baozun.com/big-data-platform/dma-platform &&
        /usr/bin/git clone https://git.baozun.com/big-data-platform/workflow-engine &&
        /usr/bin/git clone https://git.baozun.com/big-data-platform/pipelines &&
        /usr/bin/git clone https://git.baozun.com/big-data-platform/data-access-sdk &&
        /usr/bin/git clone https://git.baozun.com/big-data-platform/tm-order-rmq-to-kafka &&
        /usr/bin/git clone https://git.baozun.com/big-data-platform/flink-apps &&
        /usr/bin/git clone https://git.baozun.com/big-data-platform/dma-mdm &&
        /usr/bin/git clone https://git.baozun.com/bi-rmq-consumers/hub-rmq-consumers &&
        /usr/bin/git clone https://git.baozun.com/big-data-platform/sema-poc &&
        /usr/bin/git clone https://git.baozun.com/big-data-platform/kafka-consumer-tidb &&
        /usr/bin/git clone https://git.baozun.com/big-data-platform/flink-job-mng &&
        /usr/bin/git clone https://git.baozun.com/big-data-platform/data-governance &&
        /usr/bin/git clone https://git.baozun.com/big-data-platform/edw-tools &&
        /usr/bin/git clone https://git.baozun.com/big-data-platform/toms-data-pipeline-new &&
        /usr/bin/git clone https://git.baozun.com/big-data-platform/dp-authority-ui &&
        /usr/bin/git clone https://git.baozun.com/big-data-platform/dma-sbb3 &&
        /usr/bin/git clone https://git.baozun.com/big-data-platform/wd-plugins &&
        /usr/bin/git clone https://git.baozun.com/big-data-platform/hive-data-sync &&
        /usr/bin/git clone https://git.baozun.com/big-data-platform/flink-metastore &&
        /usr/bin/git clone https://git.baozun.com/big-data-platform/dolphinscheduler &&
        /usr/bin/git clone https://git.baozun.com/big-data-platform/platform-monitor &&
        /usr/bin/git clone https://git.baozun.com/big-data-platform/flink-history-server &&
        /usr/bin/git clone https://git.baozun.com/big-data-platform/edw-ods &&
        /usr/bin/git clone https://git.baozun.com/big-data-platform/data-compare-service &&
        /usr/bin/git clone https://git.baozun.com/big-data-platform/user-eventlistener &&
        /usr/bin/git clone https://git.baozun.com/big-data-platform/data-platform &&
        /usr/bin/git clone https://git.baozun.com/big-data-platform/hivemetastore &&
        /usr/bin/git clone https://git.baozun.com/big-data-platform/common-utils &&
        /usr/bin/git clone https://git.baozun.com/big-data-platform/send-data-to-kafka &&
        /usr/bin/git clone https://git.baozun.com/big-data-platform/hiveudf &&
        /usr/bin/git clone https://git.baozun.com/big-data-platform/redash &&
        /usr/bin/git clone https://git.baozun.com/big-data-platform/dma-input-format &&
        /usr/bin/git clone https://git.baozun.com/bi/data_cmp &&
        /usr/bin/git clone https://git.baozun.com/big-data-platform/toms-data-pipeline &&
        /usr/bin/git clone https://git.baozun.com/big-data-platform/tmall-api-hub-new &&
        /usr/bin/git clone https://git.baozun.com/big-data-platform/kettle &&
        /usr/bin/git clone https://git.baozun.com/big-data-platform/data-governance-ui &&
        /usr/bin/git clone https://git.baozun.com/big-data-platform/tmall-api &&
        /usr/bin/git clone https://git.baozun.com/bi/malorne &&
        /usr/bin/git clone https://git.baozun.com/big-data-platform/data_cmp &&
        /usr/bin/git clone https://git.baozun.com/big-data-platform/data_check &&
        /usr/bin/git clone https://git.baozun.com/big-data-platform/data-process-legacy &&
        /usr/bin/git clone https://git.baozun.com/big-data-platform/mongo-spark &&
        /usr/bin/git clone https://git.baozun.com/big-data-platform/tidb-processing &&
        /usr/bin/git clone https://git.baozun.com/big-data-platform/message-extractor &&
        /usr/bin/git clone https://git.baozun.com/big-data-platform/data-management &&
        /usr/bin/git clone https://git.baozun.com/big-data-platform/open-api-process &&
        /usr/bin/git clone https://git.baozun.com/big-data-platform/open-api-access &&
        /usr/bin/git clone https://git.baozun.com/big-data-platform/data-service &&
        /usr/bin/git clone https://git.baozun.com/big-data-platform/dt-legacy
}

git_host() {
    echo "git项目路径下执行"
    git remote -v
}

# git仓库迁移
rebuild_git_source() {
    # 进入git文件夹后
    PREFIX=${1:-""}
    # 获取项目名
    git remote -v
    GITNAME=$(git remote -v | grep "origin\|old_source" | field 2 | head -n 1 | awk -F '/' '{print $NF}')
    git remote rename origin old_source
    git remote remove origin
    git remote add origin https://gitlab.com/neosun100/$PREFIX$GITNAME
    git push origin --all
}

# 批量操作
mul_rebuild_git_source() {

    echo 退出！修改后在执行
    time_wait
    for i in $(ls -F | grep /$ | line 3 100); do
        echo $i
        cd ~/Code/loop-project/$i
        rebuild_git_source loop
    done
}
