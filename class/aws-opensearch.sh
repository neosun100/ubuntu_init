# Opensearch相关的命令

# 🥒 🥒 🥒 🥒 🥒 🥒 🥒 🥒 🥒 🥒 🥒 🥒
# 🥒 🥒 🥒 🥒 🥒 🥒 🥒 🥒 🥒 🥒 🥒 🥒
# 1. Opensearch 可配置的信息列表
# 🥒 🥒 🥒 🥒 🥒 🥒 🥒 🥒 🥒 🥒 🥒 🥒
# 🥒 🥒 🥒 🥒 🥒 🥒 🥒 🥒 🥒 🥒 🥒 🥒

# 🥒 🥒 🥒 🥒 🥒 🥒 🥒 🥒 🥒 🥒 🥒 🥒
# 1.1 opensearch支持的版本
# 🥒 🥒 🥒 🥒 🥒 🥒 🥒 🥒 🥒 🥒 🥒 🥒
aws-opensearch_list_versions() {
    aws opensearch list-versions --max-results 100 | jq | jcat
}

# 🥒 🥒 🥒 🥒 🥒 🥒 🥒 🥒 🥒 🥒 🥒 🥒
# 1.2 opensearch支持的instances
# 🥒 🥒 🥒 🥒 🥒 🥒 🥒 🥒 🥒 🥒 🥒 🥒
aws-opensearch_list_instance() {
    aws opensearch list-instance-type-details --engine-version OpenSearch_1.1 | jq .InstanceTypeDetails | md2f && md2v
}

# 🥒 🥒 🥒 🥒 🥒 🥒 🥒 🥒 🥒 🥒 🥒 🥒
# 1.3 opensearch现有 domain info
# 🥒 🥒 🥒 🥒 🥒 🥒 🥒 🥒 🥒 🥒 🥒 🥒
aws-opensearch_list_instance() {
}
aws-opensearch_describe-domains() {
    aws opensearch describe-domains \
        --domain-names $(aws opensearch list-domain-names | jq ' .DomainNames[].DomainName ' | pass_quotes | h2l) |
        jq ".DomainStatusList[]" |
        jq_flatten |
        jq_merge |
        md2f && md2v
}

# jq 包含前缀的key展示
#  curl -s --request GET \
#   --url 'https://search-prd-opensearch-internet-rkg7a427rkyfrpgyt64kxm3huq.us-east-1.es.amazonaws.com/_cluster/settings?include_defaults=&pretty=&flat_settings=' \
#   --header 'Authorization: Basic cm9vdDpBV1MydGhlbW9vbjEwMCQ=' \
#   --header 'Content-Type: application/json'  | jq '.defaults | to_entries | map("thread_pool\(.key)=\(.value)")  '
# https://stackoverflow.com/questions/63771419/jq-add-additional-variable-prefix
