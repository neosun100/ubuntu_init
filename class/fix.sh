fix_zsh_slow() {
    sudo apt remove zsh
    echo y | sudo apt-get install zsh
}

fix_google_source() {
    wget -q -O - https://dl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
}

fix_fzf() {
    sudo chmod 660 /dev/tty
}

fix_shell_mouse() {
    echo " 修复shell下鼠标滚动乱码"
    reset
}

fix_zsh_plugin() {
    git clone https://github.com/zsh-users/zsh-syntax-highlighting.git ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-syntax-highlighting
    git clone https://github.com/zsh-users/zsh-autosuggestions ${ZSH_CUSTOM:-~/.oh-my-zsh/custom}/plugins/zsh-autosuggestions
}

fix_zsh_history() {
    cp ~/.zsh_history ~/zsh_history &&
        rm -f ~/.zsh_history &&
        strings ~/zsh_history ~/.zsh_history
}

# 修复 conda 闭源  清华源不能用了 清华园已经可用了
fix_conda() {
    conda config --show-sources
    conda config --remove-key channels
    conda update -n base -c defaults conda
}

# 修复这个警告
fix_conda.compat() {
    conda update -n base -c defaults conda
}

# 修改update 造成的 conda 错误，修复安装
fix_conda_error() {
    sudo chmod 777 /Users/neo/Downloads/Anaconda3-2019.03-MacOSX-x86_64.sh &&
        sh /Users/neo/Downloads/Anaconda3-2019.03-MacOSX-x86_64.sh -u
    conda update -n base -c defaults conda
}

# 修复子系统升级bug
fix_wsl_ug() {
    sudo rm -rf /var/lib/dpkg/info/
    sudo mkdir /var/lib/dpkg/info/
    sudo apt-get update
    sudo apt-get -f install
    ug
}

# 修复lock
fix_ug_lock() {
    sudo kill $(ps -e | grep apt | field 1)
    sudo kill $(ps -e | grep apt | field 1)
    # awk "{print \$${1:-1}}"
    sudo rm /var/lib/dpkg/lock
    sudo rm /var/lib/apt/lists/lock
    sudo rm /var/cache/apt/archives/lock
    sudo rm /var/lib/dpkg/lock-frontend
    sudo apt-get install -f
    sudo dpkg --configure -a
}

fix_ug() {
    sudo dpkg --configure -a
}

# 解决windows wsl ssh 连不上的问题
fix_ssh() (
    sudo service ssh --full-restart
)

fix_zsh_compdef() {
    rm ~/.zcompdump*
    compinit
}

fix_pip_packages() {
    conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/free/
    conda config --set show_channel_urls yes

    pip install --upgrade pip
    pip install PyHamcrest # 自动化测试用的，匹配测试？ 极可能是 colorconsole 依赖
    # 彩色输出 colorconsole
    pip install colorconsole
    # 简易彩色输出 colorprint 超赞！
    pip install git+https://github.com/neosun100/colorprint

    # 快速命令行查询工具 cheat
    pip install cheat
    cheat grep

    # python版网络测速 speedtest-cli
    pip install speedtest-cli
    speedtest-cli --list | grep China
    speedtest-cli --server=3633 --share
    # pip install speedtest-cli && speedtest-cli --list | grep China && speedtest-cli --server=3633 --share

    # 安装最新版 jupyter，避免mycli 冲突
    # pip install -U ipython;
    # pip install -U jupyter-console;
    # mycli单独一个环境保险

    # 安装机器学习
    pip install deap update_checker tqdm stopit xgboost dask dask-ml scikit-mdr skrebate tpot

    # python版的多台服务器命令执行工具 pssh
    pip install pssh

    pip install arrow
    pip install toolkity
    pip install ansible
    pip install sanic
    pip install pendulum

    # python系 专业的FTP服务器
    pip install pyftpdlib

    # httpie
    pip install httpie

    # mysql客户端 mycli
    pip install mycli

    #######################
    #### fake 合集 ########
    ######################

    # 测试数据生成器
    pip install faker

    # 数据直接入DB
    pip install fake2db
    pip install mysql-connector

    # 数据生成器 支持DB，df,excel
    pip install pydbgen

    # 用户agent pass faker里也有
    pip install fake-useragent

    # python输出代码高亮模块
    pip install prettyprinter
    echo "from prettyprinter import cpprint,set_default_style"
    sleep 10

    # 图像化的ping
    pip install pinggraph
    # gping host

    # 另一个代码高亮备用
    #pip install pygmentize; 没有这个包
    pip install pygments -U
    # alias pcat='pygmentize -g'
    #### shell 录制
    pip install asciinema

    ### shell 录制成 svg 文件
    pip install termtosvg pyte python-xlib svgwrite

    # 摘要抽取 sumy
    pip install jieba
    pip install git+git://github.com/miso-belica/sumy.git

    # 安装 webp-converter
    pip install webp-converter
    webpc -h
    2to3-3.7 -w -n ~/anaconda3/lib/python3.7/site-packages/webpc
    webpc -h

    #视频下载器 lulu 超越 you-get
    #一个简单干净的视频/音乐/图像下载器
    # pip install lulu

    # youtube-dl
    pip install youtube-dl -U

    # 安装 PyGuetzli
    # 谷歌图片极限压缩
    # pip install pyguetzli;
    # mac 下 缺少 sudo apt-get install build-essential  不可用

    # 命令行生成包
    pip install fire

    # 一个更简洁的操控浏览器（pk selenium） splinter 英 /'splɪntə/ 美 /'splɪntɚ/
    pip install splinter

    # 二维码生成 推荐模块 myqr
    # 至尊款
    # https://github.com/sylnsfar/qrcode/blob/master/README-cn.md
    # 安装
    pip install myqr

    # json 输出更ping
    pip install pingparsing==0.14

    # nmap
    pip install python-nmap

    # 二维码识别模块
    # pip install zbar;

    # debug 模块
    pip install pysnooper

    pip install TA-Lib

    pip install sanic-openapi
}

fix_ug_chrome() {
    wget -q -O - https://dl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
}

fix_you_have_new_mail() {
    sudo rm /var/mail/neo && sudo touch /var/mail/neo
}

# fix 方向键
fix_sxzy() {
    echo "fix 方向键 ^[[C^[[D」"
    echo y | sudo apt-get install readline-devel
}

setup_compose() {
    ug
    echo y | sudo apt-get install python3-pip
    sudo pip3 install docker-compose
}

fix_time_error() {
    echo "时区服务ip"
    echo "http://www.ntp.org.cn/pool"
    echo "调整时区"
    sudo dpkg-reconfigure tzdata
    echo "安装时间校准程序"
    echo y | sudo apt-get install ntpdate
    echo "校准时间"
    sudo ntpdate 223.113.97.97
    echo "验证结果"
    date
    date -R
    echo "将时间写入硬件"
    echo "在修改时间以后，修改硬件CMOS的时间"
    echo "非常重要，如果没有这一步的话，后面时间还是不准"
    sudo hwclock --systohc
}

# 消除 source 反馈 conda问题
fix_source_conda_error() {
    source activate && conda init zsh && conda deactivate
}

fix_ubuntu_startup_net_wait() {
    cd /etc/systemd/system/network-online.target.wants
    l
    echo "修改了systemd-networkd-wait-online.service文件，在 [Service] 下，增加了一句"
    echo "TimeoutStartSec=2sec"
    echo "https://blog.csdn.net/baidu_19452317/article/details/99297399"
}

fix_scala_zh() {
    echo "Scala 中文乱码解决"
    echo https://www.runoob.com/w3cnote/mac-scala-chinese-show.html
    # 在 Scala 2.11.7 版本上，Mac OS X 或 Linux 系统上编译 Scala 代码，如果出现中文，会出现乱码的情况。

    # 解决方案如下，分别编辑以下两个执行脚本：

    # $ vim `which scala`

    # $ vim `which scalac`
    # 找到：

    # [ -n "$JAVA_OPTS" ] || JAVA_OPTS="-Xmx256M -Xms32M"
    # 将其替换为：

    # [ -n "$JAVA_OPTS" ] || JAVA_OPTS="-Xmx256M -Xms32M -Dfile.encoding=UTF-8"
    # 重新编译脚本，既可以正常显示中文。
}

fix_mac_brew() {

    # 替换brew.git:
    cd "$(brew --repo)"
    git remote set-url origin https://mirrors.cloud.tencent.com/homebrew/brew.git

    # 替换homebrew-core.git:
    cd "$(brew --repo)/Library/Taps/homebrew/homebrew-core"
    git remote set-url origin https://mirrors.cloud.tencent.com/homebrew/homebrew-core.git

    brew update

}

fix_zsh_compinit() {
    sudo chmod -R 755 /usr/local/share/zsh/site-functions
}

fix_ug_key() {
    sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys ${1:-78BD65473CB3BD13}
}

fix_init_block_restart_config() {
    sudo systemctl restart systemd-logind.service
    sudo systemctl restart unattended-upgrades.service
    sudo systemctl restart user@1000.service
}
