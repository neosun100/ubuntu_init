# 利用 youtube-dl 下载各类视频网站的快捷命令行


# 知乎

# youtube

# typeset home_proxy="https://192.168.2.12:8787"
# typeset local_proxy="127.0.0.1:8181"
# typeset company_proxy="122.51.108.4:8181"
# typeset mac_proxy="socks5://127.0.0.1:1086"

if [ `uname -m` = "armv7l" ] && [ `uname` = "Linux" ] && [ `hostname` != "Neone" ]; then
  # echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为arm Linux" | lcat
  typeset best_proxy="https://127.0.0.1:7890"
#   typeset sysname="_linux_arm"
  # 执行代码块
elif [ `uname -m` = "x86_64" ] && [ `uname` = "Linux" ] && [ `hostname` != "Neone" ]; then
  # echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为X86 Linux" | lcat
  typeset best_proxy="https://127.0.0.1:7890"
#   typeset sysname="_linux_amd64"
  # 执行代码块
elif [ `uname -m` = "x86_64" ] && [ `uname` = "Darwin" ] && [ `hostname` != "Neone" ]; then
  # echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为X86 Mac" | lcat
  typeset best_proxy="https://127.0.0.1:7890"
#   typeset sysname="_darwin_amd64"
  # 执行代码块
elif [ `uname -m` = "x86_64" ] && [ `uname` = "Darwin" ] && [ `hostname` = "Neone" ]; then
  # echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为X86 Mac" | lcat
  typeset best_proxy="socks5://127.0.0.1:7890"
#   typeset sysname="_darwin_amd64"
  # 执行代码块
else
  echo "`uname` 系统未知！"
fi


# youtube-dl 代理下载 youtube

# 当前路径下，下载youtube id 最高清晰度的视频



# ytb(){
#   youtube-dl -i "https://www.youtube.com/watch?v=$1" --proxy $best_proxy
# }


# 传入视频id即可
ytb(){
  tsocks youtube-dl -f 'bestvideo[ext=mp4]+bestaudio[ext=m4a]/bestvideo+bestaudio' \
  --merge-output-format mp4 \
  -i "https://www.youtube.com/watch?v=$1" 
  # --proxy $best_proxy
}


# 传入视频url 
ytb-url(){
  # youtube-dl -f 'bestvideo[ext=mp4]+bestaudio[ext=m4a]/bestvideo+bestaudio' \
  tsocks youtube-dl --merge-output-format mp4 \
  -i $1 
  # --proxy $best_proxy
}


mytb-url(){
tsocks youtube-dl --merge-output-format mp4 \
--external-downloader aria2c \
--external-downloader-args "-x 16 -k 1M" \
-i $1
}



# pipx run youtube-dl -o "/指定的路径/%(title)s.%(ext)s" https://www.youtube.com/watch?v=<video_id>
