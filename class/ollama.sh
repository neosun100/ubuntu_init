# update_ollama_models() {
#     ollama list
#     current_time() {
#         date '+%Y-%m-%d %H:%M:%S'
#     }

#     log_info() {
#         echo "$(current_time) [INFO] $1"
#     }

#     log_warning() {
#         echo "$(current_time) [WARNING] $1" >&2
#     }

#     log_error() {
#         echo "$(current_time) [ERROR] $1" >&2
#     }

#     log_info "开始更新模型..."

#     # 获取模型列表
#     models=$(ollama list | tail -n +2)
#     if [ -z "$models" ]; then
#         log_error "无法获取模型列表或模型列表为空"
#         return 1
#     fi

#     while IFS= read -r line; do
#         name=$(echo "$line" | awk '{print $1}')
#         # 假设最后修改时间在第6个字段
#         modified=$(echo "$line" | awk '{print $6}')

#         # 检查name是否为空
#         if [ -z "$name" ]; then
#             log_warning "无法从以下行中提取模型名称：$line"
#             continue
#         fi

#         log_info "处理模型：$name，最后修改时间：$modified"

#         # 如果修改时间包含 "days ago"，则更新模型
#         if [[ $modified == *days* ]]; then
#             log_info "执行 ollama pull $name"
#             ollama pull "$name" || log_error "无法更新模型 $name"
#         else
#             log_info "模型 $name 是最新的，无需更新"
#         fi
#     done <<<"$models"

#     log_info "模型更新完毕"
#     ollama list
# }


update_ollama_models() {
    ollama list
    current_time=$(date '+%Y-%m-%d %H:%M:%S')

    log_info() {
        echo "$current_time [INFO] $1"
    }

    log_warning() {
        echo "$current_time [WARNING] $1" >&2
    }

    log_error() {
        echo "$current_time [ERROR] $1" >&2
    }

    log_info "开始更新模型..."

    # 获取模型列表
    models=$(ollama list | tail -n +2)
    if [ -z "$models" ]; then
        log_error "无法获取模型列表或模型列表为空"
        return 1
    fi

    while IFS= read -r line; do
        # 使用awk一次性提取模型名称和最后修改时间
        read -r name modified <<< $(echo "$line" | awk '{print $1, $6}')

        # 检查name是否为空
        if [ -z "$name" ]; then
            log_warning "无法从以下行中提取模型名称：$line"
            continue
        fi

        log_info "处理模型：$name，最后修改时间：$modified"

        # 如果修改时间包含 "days ago" 或 "weeks ago"，则更新模型
        if [[ $modified == *days* ]] || [[ $modified == *weeks* ]]; then
            log_info "执行 ollama pull $name"
            ollama pull "$name" || log_error "无法更新模型 $name"
        else
            log_info "模型 $name 是最新的，无需更新"
        fi
    done <<<"$models"

    log_info "模型更新完毕"
    ollama list
}
