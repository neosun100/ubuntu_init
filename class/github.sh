# 获取github 符合当前系统的最新安装包


# 只需要一个参数
# ✅ 下载页 github url 即可
# 无任何外部依赖脚本
get_latest_package(){
DOWNLOAD_URL=${1:-https://github.com/xtaci/kcptun/releases} # 下载地址
GITHUB_HOST='https://github.com/'

if [ `uname -m` = "armv7l" ] && [ `uname` = "Linux" ]; then
  echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为arm Linux"
  typeset SYSTEM="linux" # 系统
  typeset SYSTEM1="linux" # 系统
  typeset SYSTEM2="Linux" # 系统
  typeset SYSTEM3="linux" # 系统
  typeset ARCH="arm7" # 系统
  typeset TSOCKS_CONFIG_FILE='/etc/tsocks.conf' #  代理文件


elif [ `uname -m` = "aarch64" ] && [ `uname` = "Linux" ]; then
  echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为arm64 Linux"
  typeset SYSTEM="linux" # 系统
  typeset SYSTEM1="inux" # 系统
  typeset SYSTEM2="Linux" # 系统
  typeset SYSTEM3="linux" # 系统
  typeset ARCH="arm64" # 系统
  typeset TSOCKS_CONFIG_FILE='/etc/tsocks.conf' #  代理文件

elif [ `uname -m` = "x86_64" ] && [ `uname` = "Linux" ]; then
  echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为X86 Linux"
  typeset SYSTEM="linux" # 系统
  typeset SYSTEM1="inux" # 系统
  typeset SYSTEM2="Linux" # 系统
  typeset SYSTEM3="linux" # 系统
  typeset ARCH="amd64" # 架构
  typeset TSOCKS_CONFIG_FILE='/etc/tsocks.conf' #  代理文件

elif [ `uname -m` = "x86_64" ] && [ `uname` = "Darwin" ]; then
  echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为X86 Mac"
  typeset SYSTEM="darwin" # 系统
  # macos
  typeset SYSTEM1="macos" # 系统
  typeset SYSTEM2="macOS" # 系统
  typeset SYSTEM3="darwin" # 系统
  typeset ARCH="amd64" # 架构
  typeset TSOCKS_CONFIG_FILE='/usr/local/etc/tsocks.conf' #  代理文件 
 
else
  echo "`uname` 系统未知！"
fi

# 存在代理走代理，不存在走普通

if [ -e $TSOCKS_CONFIG_FILE ]; then
echo "`date +'%Y-%m-%d %H:%M:%S'`  存在代理 走代理加速下载"
LASTEST_URL=`tsocks curl -s $DOWNLOAD_URL | grep 'releases/download' | grep "$SYSTEM\|$SYSTEM1\|$SYSTEM2\|$SYSTEM3" | grep $ARCH | head -n 1 | awk -v head='href=' -v tail=' rel' '{print substr($0, index($0,head)+length(head),index($0,tail)-index($0,head)-length(head))}' | sed  's/"//g'`
# 这里加逻辑 如果 LASTEST_URL 为空 则用 $SYSTEM2
PACKAGE_URL=$GITHUB_HOST$LASTEST_URL 
echo "`date +'%Y-%m-%d %H:%M:%S'`  下载链接为 $PACKAGE_URL"
tsocks wget $PACKAGE_URL --user-agent="Mozilla/5.0 (X11;U;Linux i686;en-US;rv:1.9.0.3) Geco/2008092416 Firefox/3.0.3" --no-check-certificate
else
echo "`date +'%Y-%m-%d %H:%M:%S'`  代理不存在"
LASTEST_URL=`curl -s $DOWNLOAD_URL | grep 'releases/download' | grep "$SYSTEM\|$SYSTEM1\|$SYSTEM2\|$SYSTEM3"| grep $ARCH | head -n 1 | awk -v head='href=' -v tail=' rel' '{print substr($0, index($0,head)+length(head),index($0,tail)-index($0,head)-length(head))}' | sed  's/"//g'`
PACKAGE_URL=$GITHUB_HOST$LASTEST_URL 
# 这里加逻辑 如果 LASTEST_URL 为空 则用 $SYSTEM2
echo "`date +'%Y-%m-%d %H:%M:%S'`  下载链接为 $PACKAGE_URL"
wget $PACKAGE_URL --user-agent="Mozilla/5.0 (X11;U;Linux i686;en-US;rv:1.9.0.3) Geco/2008092416 Firefox/3.0.3" --no-check-certificate
fi
}



# install_compose_latest(){
#   curl -s https://github.com/docker/compose | htmlq a --attribute href  
# }