alias grepip='grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}"'

# 🍎 查看局域网ip,            默认|比格云|腾讯云|qq云
iip() {
    # if [ -e /sbin/ifconfig ];then
    #   #ifconfig | egrep "192.168|10.100|172.27|172.30" | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | sed -n "1,1p"
    #   ifconfig | egrep "192.168|10.100|172.27|172.30" | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | head -n 1
    # else
    #   #ip addr | egrep "192.168|10.100|172.27|172.30" | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | sed -n "1,1p"
    #   ip addr | egrep "192.168|10.100|172.27|172.30" | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | head -n 1
    # fi

    # [[ `uname` = "Darwin" ]] && \
    # ifconfig | egrep "192.168|10.100|172.27|172.30" | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}" | head -n 1 || \
    # ip addr |grep inet |grep -v inet6 | grep -v 127.0.0.1 | field 2 | line 1 1 | grep -E -o "([0-9]{1,3}[\.]){3}[0-9]{1,3}"

    python3 -c "import socket;print([(s.connect(('8.8.8.8', 53)), s.getsockname()[0], s.close()) for s in [socket.socket(socket.AF_INET, socket.SOCK_DGRAM)]][0][1])"
}

iiip() {
    ifconfig -a | grep -v 'inet6\|127.0.0.1\|172' | grep inet | awk "{print \$${1:-2}}"
}

wip() {
    # curl -s "https://www.taobao.com/help/getip.php" | grepip
    curl -s https://ipinfo.io | jq ".ip" | pass_quotes
}

# 🍎 查看外网ip
alias webip='http "https://www.taobao.com/help/getip.php" | grepip'

# 🍎 端口查看 仅限tcp
alias webport='sudo netstat -ntlp tcp | grep "0.0.0.0" | lcat'

#inip(){
#    ifconfig | grep -A1 "192.168" | awk -v head="inet " -v tail=" netmask" '{print substr($0, index($0,head)+length(head),index($0,tail)-index($0,head)-length(head))}';
#}
#webip(){
#    http www.trackip.net/i | grep title | awk -v head="<title>" -v tail="</title>" '{print substr($0, index($0,head)+length(head),index($0,tail)-index($0,head)-length(head))}';
#}
# 🍎 查看内网外网ip
nip() {
    echo "内网ip:\t$(iip)\n外网ip:\t$(wip)" | lcat
}

# 检查ip是否在线
ip_out() {
    ping -c 1 -W 1 ${1:-192.168.1.1} | grep 100% | wc -l
    # 1 out
    # 0 online
}

# 显示ip归属地 json
nip_where() {
    nhttp "http://ip-api.com/json/${1:-$(wip)}?lang=zh-CN"

}

# 显示内外网ip归属
# all ip where
aip_where() {
    # 获取内外网的本地ip
    # CHINA=$(http -b https://ip.useragentinfo.com/myip)
    CHINA=$(http -b https://ipinfo.io/json  | jq .ip | pass_quotes )
    FOREIGN=$(tsocks http -b ifconfig.me)
    echo "$(date +'%Y-%m-%d %H:%M:%S') 墙内IP信息" | lcat
    nip_where $CHINA
    echo "🈲️  🈲️  🈲️  🈲️  🈲️  🈲️  🈲️  🈲️  🈲️  🈲️  🈲️  🈲️  🈲️  🈲️  🈲️  🈲️  🈲️"
    echo "🧱 🧱 🧱 🧱 🧱 🧱 🧱 🧱 🧱 🧱 🧱 🧱 🧱 🧱 🧱 🧱 🧱"
    echo "🌈 🌈 🌈 🌈 🌈 🌈 🌈 🌈 🌈 🌈 🌈 🌈 🌈 🌈 🌈 🌈 🌈"
    echo ""
    echo ""
    echo "$(date +'%Y-%m-%d %H:%M:%S') 墙外IP信息" | lcat
    nip_where $FOREIGN
}

# 查询ip归属地
ip_data() {
    nhttp "https://api.ipdata.co/$1?api-key=66cf6ee749d7242c669972a3d127b88518d4377ffc5df5e03e0c9bbb"
}

tsocks_ip() {
    # "显示当前ip和tsocks后ip的变化"
    IP=${1:-"http://ip-api.com/json"}

    if [ $(uname -m) = "armv7l" ] && [ $(uname) = "Linux" ]; then
        # echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为arm Linux" | lcat
        # typeset subfile="-linux-arm.zip"
        echo "[" >/tmp/ip.json &&
            curl -s $IP >>/tmp/ip.json &&
            echo "," >>/tmp/ip.json &&
            tsocks curl -s $IP >>/tmp/ip.json &&
            echo "]" >>/tmp/ip.json &&
            cat /tmp/ip.json | jq '[.[] | del(.status,.zip,.lat,.lon) ]' | md2f && md2v

    elif [ $(uname -m) = "aarch64" ] && [ $(uname) = "Linux" ]; then
        # echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为arm64 Linux" | lcat
        # typeset subfile="-linux-arm64.zip"
        echo "[" >/tmp/ip.json &&
            curl -s $IP >>/tmp/ip.json &&
            echo "," >>/tmp/ip.json &&
            tsocks curl -s $IP >>/tmp/ip.json &&
            echo "]" >>/tmp/ip.json &&
            cat /tmp/ip.json | jq '[.[] | del(.status,.zip,.lat,.lon) ]' | md2f && md2v

        # 执行代码块
    elif [ $(uname -m) = "x86_64" ] && [ $(uname) = "Linux" ]; then
        # echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为X86 Linux" | lcat
        # typeset subfile="-linux-amd64.zip"
        echo "[" >/tmp/ip.json &&
            curl -s $IP >>/tmp/ip.json &&
            echo "," >>/tmp/ip.json &&
            tsocks curl -s $IP >>/tmp/ip.json &&
            echo "]" >>/tmp/ip.json &&
            cat /tmp/ip.json | jq '[.[] | del(.status,.zip,.lat,.lon) ]' | md2f && md2v

        # 执行代码块
    elif [ $(uname -m) = "x86_64" ] && [ $(uname) = "Darwin" ] && [ $(hostname) = "Beta.local" ]; then
        # echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为X86 Mac" | lcat
        # typeset subfile="-mac-amd64.zip"
        echo "[" >/tmp/ip.json &&
            curl -s $IP >>/tmp/ip.json &&
            echo "," >>/tmp/ip.json &&
            tsocks curl -s $IP >>/tmp/ip.json &&
            echo "]" >>/tmp/ip.json &&
            cat /tmp/ip.json | jq '[.[] | del(.status,.zip,.lat,.lon) ]' | md-table

        # 执行代码块
    elif [ $(uname -m) = "x86_64" ] && [ $(uname) = "Darwin" ] && [ $(hostname) != "Beta.local" ]; then
        # echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为X86 Mac" | lcat
        # typeset subfile="-mac-amd64.zip"
        echo "[" >/tmp/ip.json &&
            curl -s $IP >>/tmp/ip.json &&
            echo "," >>/tmp/ip.json &&
            tsocks curl -s $IP >>/tmp/ip.json &&
            echo "]" >>/tmp/ip.json &&
            cat /tmp/ip.json | jq '[.[] | del(.status,.zip,.lat,.lon) ]' | md2f && md2v
    else
        echo "$(uname) 系统未知！"
    fi

}
