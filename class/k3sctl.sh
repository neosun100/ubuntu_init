#  k3s 系列

install_k3s(){
echo "安装k3s"; 
[[ -e /etc/tsocks.conf || -e /usr/local/etc/tsocks.conf  ]] && \
tsocks curl -sfL https://get.k3s.io | sh - || \
curl -sfL https://get.k3s.io | sh -s - server --docker
# curl -sfL https://get.k3s.io | sh -;

echo "验证安装完成✅";
echo 'sudo k3s kubectl get node';
sudo k3s kubectl get node;
}


uninstall_k3s(){
echo "卸载k3s";
sh  /usr/local/bin/k3s-uninstall.sh;
}

k3s_token(){
sudo cat /var/lib/rancher/k3s/server/node-token;
}

# 一个参数 master 本机ip
# 在master节点上执行，获取加入master节点的命令后，在node节点上执行
k3s_join_master(){
echo "sudo k3s agent --server https://`iip`:6443 --token `k3s_token`";
}


install_k3s_daskboard(){
sudo k3s kubectl create -f `echo ~`/upload/ubuntu_init/k8s_run_dir/dashboard/kubernetes-dashboard.1.16.2.yaml 
}

k3s_config(){
sudo cat /etc/rancher/k3s/k3s.yaml | sed "s/127.0.0.1/`iip`/"
}

