azure_say() {
    NUM=$(($RANDOM % 6 + 1))
    edge-tts \
        --voice $(edge-tts --list-voices | grep zh | grep TW | sed -n "${NUM}p" | field 2) \
        --text ${1:-'看破世间迷眼相，浩然正气贯长虹'} \
        --write-media ~/upload/azure_voice.mp3  > /dev/null 2>&1

    #  speak
    afplay ~/upload/azure_voice.mp3

}

# edge-playback --text "Hello, 我是谁" --voice zh-CN-XiaoxiaoNeural 速度比上面快一倍

azure_say_en() {
    NUM=$(($RANDOM % 2 + 1))
    edge-tts \
        --voice $(edge-tts --list-voices | grep AU | sed -n "${NUM}p" | field 2) \
        --text ${1:-'My name is Lucy, and I am from China. I am an outgoing and open-minded person. I love traveling and experiencing different cultures, which is one of the reasons why I decided to study abroad. I am currently a senior in University, majoring in Marketing.'} \
        --write-media ~/upload/azure_voice.mp3  > /dev/null 2>&1

    #  speak
    afplay ~/upload/azure_voice.mp3 

}


