# 用 Chrome 打开 某链接
chrome() {
    open -a "/Applications/Google Chrome.app" ${1:-"https://github.com"}
}

# 编辑命令行快速打开书签链接

# 显示所有 w_系列的命令 👋
# 也算是一个此类命令的索引
w_help() {
    w_book_help
    w_cli_help
    w_func_help
    w_knowledge_help
}

# 🐟🐟🐟🐟🐟🐟🐟🐟🐟🐟🐟🐟🐟🐟🐟🐟🐟🐟🐟🐟🐟🐟🐟🐟🐟🐟🐟🐟🐟🐟🐟🐟🐟🐟

w_book_help() {
    echo 📚📚📚📚📚📚📚📚📚📚📚📚📚📚📚📚📚📚📚📚📚📚📚📚📚📚📚📚📚📚📚📚📚📚
    echo 📚 书籍类网站
    echo -e "func\nw_ebbok\nw_cbook\nw_abook\nw_obook" | table
    echo ''
}

# 鸠摩古书搜索
w_obook() {
    chrome "https://www.jiumodiary.com/"
}

w_ebook() {
    chrome "http://it-ebooks.info/"
    chrome "http://www.allitebooks.com/"
    chrome "https://smtebooks.com/"
}

w_cbook() {
    chrome "http://www.hzbook.com/index.php/Book/search.html"
    chrome "https://www.aibooks.cc/"
    chrome "https://www.epubit.com/vipSale"
    chrome "http://www.ituring.com.cn/"
    chrome "https://www.epubit.com/"
    chrome "http://www.duokan.com/u/mybook"
    chrome "https://yuedu.baidu.com/customer/mybook?fr=head_nav"
    chrome "https://read.douban.com/people/143494556/ebook"
    chrome "https://github.com/lzxyzq/ML_BOOK/find/master"
}

w_abook() {
    w_ebook && w_cbook
}

w_cli_help() {
    echo 🀘🀘🀘🀘🀘🀘🀘🀘🀘🀘🀘🀘🀘🀘🀘🀘🀘🀘🀘🀘🀘🀘🀘🀘🀘🀘🀘🀘🀘🀘🀘🀘🀘🀘🀘🀘🀘🀘🀘🀘🀘🀘🀘🀘🀘🀘🀘🀘🀘🀘🀘🀘🀘
    echo 🀘 CLI 网站
    echo -e "func\nw_cli\nw_eli" | table
    echo ''
}
# web端 命令行查找 紧跟一个需要查找的命令
# wcli ls
w_cli() {
    chrome "http://man.linuxde.net/${1}"
}
# 英文版的 eli了
w_eli() {
    chrome "https://tldr.ostera.io/${1}"
}

w_func_help() {
    echo 🍉🍉🍉🍉🍉🍉🍉🍉🍉🍉🍉🍉🍉🍉🍉🍉🍉🍉🍉🍉🍉🍉🍉🍉🍉🍉🍉🍉🍉🍉🍉🍉
    echo 🍉 功能 网站
    echo -e "func\nw_app\nw_ut\nw_bmap\nw_todo\nw_logo\nw_wk\nw_pic" | table
    echo ''
}

# 破解mac的app搜索,跟一个参数，zhegeapp的名字
w_app() {
    chrome "https://www.xxmac.com/?s=${1}"
}

# 查看 UT 数
w_ut() {
    chrome "http://ulord.bi-chi.com/miners/UYxN8zKYWHkUAMED2EbUXb2kvLz1X1i77p"
}

# 查看 xmr 数
w_xmr() {
    chrome "https://xmr.chinaenter.cn/#/dashboard"
}

# 脑图web   brain map
w_bmap() {
    chrome "https://aonaotu.com"
}

w_processon() {
    chrome "https://www.processon.com/diagrams"
}

w_processon_best() {
    chrome "https://online.visual-paradigm.com/w/zhlbvisg/drive"
}

# web to_do
w_todo() {
    chrome "https://to-do.microsoft.com/today"
}

# pronhub logo
w_logo() {
    chrome "https://logoly.pro"
}

# 挖矿
w_wk() {
    chrome "https://wk588.com/tools/gpu"
}

# 图床
w_pic() {
    chrome "https://images.ac.cn/cn.html"
}

# json  格式化
w_json() {
    chrome "http://www.bejson.com/jsonviewernew/"
    chrome "https://www.json.cn/"
}

# 蒲公英发布
w_pgy() {
    chrome "https://www.pgyer.com/app/publish"
}

# 蒲公英.多多听书
w_ddls() {
    chrome "https://www.pgyer.com/DDLS"
}

w_han() {
    echo " 汉化idea系列软件"
    chrome "https://github.com/pingfangx/jetbrains-in-chinese"
    echo " 汉化使用说明"
    chrome "https://github.com/pingfangx/TranslatorX/wiki/Usage"
}

w_knowledge_help() {
    echo 🏦🏦🏦🏦🏦🏦🏦🏦🏦🏦🏦🏦🏦🏦🏦🏦🏦🏦🏦🏦🏦🏦🏦🏦🏦🏦🏦
    echo 🏦 框架或知识系列 网站
    echo -e "func\nw_nodejs\nw_vue\nw_uni-app" | table
    echo ''
}

w_nodejs() {
    chrome "http://nodejs.cn/api/"
    chrome "https://github.com/sindresorhus/awesome-nodejs"
    chrome "https://www.liaoxuefeng.com/wiki/1022910821149312"

}

w_vue() {
    chrome "https://cn.vuejs.org/"
    chrome "https://cn.vuejs.org/v2/guide/"
    chrome "https://github.com/vuejs/awesome-vue"
    chrome "https://www.runoob.com/vue2/vue-tutorial.html"
    chrome "https://cn.vuejs.org/v2/api/#实例属性"
}

w_uni-app() {
    chrome "https://uniapp.dcloud.io/" # 官网
    chrome "https://github.com/aben1188/awesome-uni-app/blob/master/README.md"
    # chrome "https://github.com/dcloudio/uni-app"; # github
    chrome "http://ask.dcloud.net.cn/docs/"                   # 问题集
    chrome "http://dcloud.io/doc.html"                        # 文档
    chrome "http://ext.dcloud.net.cn"                         # 插件市场
    chrome "https://www.imovietrailer.com/superhero/doc.html" # 超级英雄项目文档
}

w_ipfs() {
    chrome "http://www.ipfs.cn/"         # 中文网
    chrome "https://globalupload.io/"    #  文件上传地址
    chrome "https://ipfs.io/"            # 官网
    chrome "https://www.8btc.com/p/ipfs" # 巴比特

}

w_rabook_cputem() {
    chrome "http://rabook:8085/" # 需要先打开 openhardwareMonitor run port
}

# 深度学习相关的文档

w_keras() {
    chrome "https://keras.io/zh/"
}

w_ai() {
    chrome "http://www.huaxiaozhuan.com" # 一个资深机器学习者整理的笔记
}

w_read_write_file() {
    echo "write"
    chrome "https://joblib.readthedocs.io/en/latest/auto_examples/compressors_comparison.html#sphx-glr-auto-examples-compressors-comparison-py"
    echo "read"
    chrome "https://github.com/RaRe-Technologies/smart_open"
}

w_pmset() {
    echo "Mac OS 电源管理及 pmset 命令"
    chrome "https://blog.csdn.net/fengmm521/article/details/78446516"
}

w_packt_github() {
    echo "packt 图书📖github代码"
    chrome "https://github.com/PacktPublishing/"
}

# 打开web的jupyter
w_jupyter_github() {
    url=$(echo "${1:-https://github.com/aloctavodia/BAP}" | ncut 20 100)
    chrome "https://nbviewer.jupyter.org/github/$url/tree/master/"
}

w_ai_doc() {
    echo "ApacheCN 人工智能知识树"
    chrome "https://github.com/apachecn/ai-roadmap/tree/master/v1.0"
    echo "ApacheCN 文档和图书"
    chrome "https://docs.apachecn.org/"
    echo "吴恩达 ai ebook 2017"
    chrome "http://ai-start.com/dl2017/"
    echo "绿萝间"
    chrome "http://muxuezi.github.io/categories/"
    echo "ai算法工程师手册"
    chrome "http://www.huaxiaozhuan.com/"
}

w_seaborn() {
    chrome "https://www.cntofu.com/book/172/index.html"
}

w_pymc3() {
    chrome "https://docs.pymc.io/"
    chrome "http://people.duke.edu/~ccc14/sta-663-2016/16C_PyMC3.html"
    chrome "https://nbviewer.jupyter.org/github/aloctavodia/BAP/tree/master/code/"
}

# 共轭梯度法讲解
w_Conjugate-Gradient() {
    w_jupyter_github https://github.com/vschaik/Conjugate-Gradient
}

w_sklearn() {
    chrome "https://sklearn.apachecn.org/docs/0.21.3/"
}

w_pay_app() {
    chrome "http://user.mairuan.com/"
}

w_torch() {
    echo "pytorch-handbook"
    chrome "https://nbviewer.jupyter.org/github/zergtant/pytorch-handbook/tree/master/"
    echo "pytorch-中文文档"
    chrome "https://pytorch-cn.readthedocs.io/zh/latest/"
    echo "pytorch-官网"
    chrome "https://pytorch.org/"
    echo "pytorch-ONNX"
    chrome "https://pytorch.apachecn.org/docs/1.0/ONNXLive.html"
}

w_gpu_df() {
    echo "RAPIDS"
    chrome "https://rapids.ai/"
    echo "RAPIDS_Docs"
    chrome "https://docs.rapids.ai/"
    echo "gpu_sql"
    chrome "https://blazingsql.com/"
}

w_k8s() {
    chrome "https://192.168.249.10:32000/"
}

w_flutter() {
    echo "Flutter超全开源框架、项目和学习资料汇总"
    chrome "http://www.cocoachina.com/cms/wap.php?action=article&id=25856"
    echo "电子书api"
    chrome "https://juejin.im/entry/593a3fdf61ff4b006c737ca4"
    echo "最新flutter资源"
    chrome "https://www.wanandroid.com/article/query?k=flutter"
    echo "flutter 教程"
    chrome "https://www.bilibili.com/video/av52490605/"
    echo "flutter 桌面应用"
    chrome "https://feather-apps.com/"
}

w_gpu_nccl() {
    echo "NVIDIA集体通信库（NCCL）实现了针对NVIDIA GPU性能优化的多GPU和多节点集体通信原语"
    chrome "https://developer.nvidia.com/nccl/nccl-download"
}

w_dask() {
    echo "dash 文档"
    chrome "https://docs.dask.org/en/latest/"
    echo "dash 官网"
    chrome "https://dask.org/"
    echo "分布式pandas-github:modin by dask & ray"
    chrome "https://github.com/modin-project/modin"
    echo "modin doc"
    chrome "https://modin.readthedocs.io/en/latest/"
}

w_chrome_plugin() {
    chrome "https://chrome.google.com/webstore/category/extensions"
}

w_mac_k8s() {
    echo "基于 docker desktop的k8s 包含 windows de K8S的安装"
    chrome "https://github.com/AliyunContainerService/k8s-for-docker-desktop"
}

w_kubernetes_dashboard() {
    echo "dashboard 最新beta版本"
    chrome "https://github.com/kubernetes/dashboard/releases"
    echo "一个实例子"
    chrome "https://blog.csdn.net/fuck487/article/details/102229426"
}

w_kubernetes_doc() {
    echo "kube 中文日志"
    chrome "https://kubernetes.io/zh/docs/home/"
}

w_https() {
    echo "为已有域名添加证书和自动化"
    chrome "https://freessl.cn/"
}

w_proxy() {
    echo "配合 proxychain4 的免费代理网站"
    chrome "http://free-proxy.cz/zh/proxylist/country/all/socks5/ping/level1"
}

w_skorch() {
    echo "sklearn化的torch"
    chrome "https://nbviewer.jupyter.org/github/skorch-dev/skorch/tree/master/"
}

w_rpi_update() {
    echo "这个是树莓派升级固件的脚本"
    chrome "https://raw.githubusercontent.com/Hexxeh/rpi-update/master/rpi-update"
}

w_idea_key() {
    echo "idea系列软件的key"
    chrome "http://idea.medeming.com/jets/"
}

w_chrome_file() {
    echo "chrome 各系统版本安装包"
    chrome "http://ports.ubuntu.com/pool/universe/c/chromium-browser/"
}

w_arm_tensorflow() {
    echo "arm 版 谷歌编译 的 tensorflow"
    chrome "https://github.com/lhelontra/tensorflow-on-arm/releases/"
}

w_tencent_api() {
    echo "腾讯api的调试页面"
    chrome "https://console.cloud.tencent.com/api/explorer"
}

w_cpu() {
    echo "cpu 性能对比网站"
    chrome "https://www.cpu-monkey.com"
}

w_algorithm_visualization() {
    echo "机器学习优化算法可视化"
    chrome "https://www.cnblogs.com/guoyaohua/p/8542554.html"
    chrome "https://blog.csdn.net/zz2230633069/article/details/88296448"
    chrome "https://github.com/Jaewan-Yun/optimizer-visualization"
    chrome "http://ruder.io/optimizing-gradient-descent/"
}

w_mac_nas() {
    echo "多端mac nas 系统"
    chrome "https://daemonsync.me/home"
}

w_python_aio() {
    echo "高性能异步库"
    chrome "https://github.com/aio-libs"
}

w_python_tip() {
    chrome "https://pythontips.com/"
    chrome "https://github.com/eastlakeside/interpy-zh"
    chrome "https://interpy.eastlakeside.com/"
}

w_docker_official-images() {
    echo "docker 官方各系统 架构 镜像"
    chrome "https://github.com/docker-library/official-images"
}

w_docker_raspberry() {
    echo " 以下全都是树莓派可用的 docker 镜像"
    chrome "https://hub.docker.com/u/hypriot"
    chrome "https://hub.docker.com/u/arm32v7"
    chrome "https://hub.docker.com/u/balena/"
}

w_iot() {
    echo "balena项目"
    chrome "https://www.balena.io/"
    chrome "https://github.com/balena-io-projects"
}

w_jq() {
    echo "jq 模块"
    chrome "https://stedolan.github.io/jq/tutorial/"
    echo "jq web测试"
    chrome "https://jqplay.org/"
}

w_vpn() {
    echo "vpn"
    # chrome "https://tool.gowall.app/"
    # chrome "https://c.ckh01.com/clientarea.php?action=productdetails&id=44908"
    # chrome "https://e.gae1s.com/clientarea.php?action=productdetails&id=44908"
    chrome "https://youyun666.com/user"
    chrome "https://t.me/joinchat/AAAAAErBAmXhspExyT1o9A"
}

w_vpn_local() {
    echo "本地 clash web"
    chrome "http://127.0.0.1:9090/ui/#/proxies"
}

w_vpn_rabook() {
    echo "rabook clash web"
    chrome "http://proxy-rabook.frp.neo.pub/ui/#/proxies"
}

w_ip() {
    echo "显示ip 归属地信息"
    chrome "https://www.ipip.net/ip/${1:-58.247.112.82}.html"
}

w_airdisk() {
    echo "家庭nas"
    chrome "https://www.airdisk.cc/"
}

w_nas() {
    echo " 登陆 airdisk" | lcat
    echo "确保当前处于家庭环境"

    sleep 3
    chrome "smb://AirDisk:neosun100@192.168.2.26"
}

w_app_crack() {
    echo "2 个 mac app 破解网站"
    chrome "https://mac-torrent-download.net"
    chrome "https://www.macappdownload.com"
}

w_inter_ssd() {
    echo "inter p4510 等系列的驱动"
    chrome "https://downloadcenter.intel.com/download/29171/Datacenter-NVMe-Microsoft-Windows-Drivers-for-Intel-SSDs?v=t"

}

w_linux_calculate() {
    echo "linux shell 下的 四则运算"
    chrome "https://blog.csdn.net/qq_35515661/article/details/92385322"
}

w_check_https() {
    echo "检验网站的https安全等级🔐"
    chrome "https://myssl.com/${1:-blog.neo.pub}"
}

w_ssh_company_power() {
    echo "web访问power shell"
    /Applications/Firefox.app/Contents/MacOS/firefox-bin -url "https://ssh-power.frp.neo.pub/?hostname=&username=&password=&privatekey=&passphrase="
}

w_insomnia_plugins() {
    echo "insomnia 插件大全社区"
    chrome "https://insomnia.rest/plugins/"
}

w_crack_til-tok() {
    chrome "https://www.popmars.com/ios%e5%ba%94%e7%94%a8/%e6%8a%96%e9%9f%b3%e5%9b%bd%e9%99%85%e7%89%88%ef%bc%88tik-tok%ef%bc%89%e9%94%81%e5%8c%ba%e8%a7%a3%e9%94%81%e8%a7%82%e7%9c%8b%e6%96%b9%e6%b3%95%ef%bc%882019%e5%b9%b48%e6%9c%881%e6%97%a5%e6%9b%b4/"
    chrome "https://github.com/cyubuchen/TikTok_Unlock"
}

w_ecs() {
    # wget http://update2.agent.tencentyun.com/update/monitor_agent_admin && chmod +x ./monitor_agent_admin && ./monitor_agent_admin check
    echo "腾讯云主机资源监控"
    chrome "https://console.cloud.tencent.com/cvm/instance/detail?rid=4&id=ins-84mm3bez&tab=monitor"
}

w_one_k8s() {
    echo "k8s离线安装包"
    chrome "https://sealyun.com/"
}

w_ssr_vpn() {
    echo "小飞机服务端"
    chrome "https://www.stackcc.com/2019/04/04/ssrstep/"
    chrome "https://github.com/wzdnzd/ShadowsocksX-NG-R/releases"
    chrome "https://www.gigsgigscloud.com/cn/tutorials/article/ssr-bash-python/"
    chrome "https://www.bilibili.com/read/cv1526493/"
    chrome "https://github.com/shadowsocksrr/electron-ssr/releases" #客户端
    chrome "https://www.linuxstudy.cn/archives/67.html#ssr-shell-client"
    chrome "https://shadowsocks.org/en/index.html" #官网
}

w_up_pc_disk() {
    echo "rabook disk"
    echo "smb://rabook:hrzqgch@rabook"
    chrome "smb://rabook:hrzqgch@rabook"
    echo "nice disk"
    echo "smb://Administrator:fg@nice"
    chrome "smb://Administrator:fg@nice"
}

w_up_airdisk() {
    echo "airdisk"
    echo "smb://AirDisk:neosun100@airdisk"
    chrome "smb://AirDisk:neosun100@airdisk"
}

w_up_home_disk() {
    w_up_pc_disk
    w_up_airdisk
}

w_app_plugin() {
    echo "app 插件 微信钉钉插件等"
    chrome "https://repo.xposed.info/module-overview"
}

w_redis_cli() {
    echo "redis 命令"
    chrome "https://www.redis.net.cn/order/"
}

w_my_vpn() {
    echo "SoftEther VPN"
    chrome "https://www.softether-download.com/cn.aspx"
}

w_kcp() {
    echo "kcp客户端和服务端 kcptun"
    chrome "https://github.com/xtaci/kcptun"
    chrome "https://github.com/xtaci/kcptun/releases"
}

w_crack() {
    echo "软件破解站点涵盖tableau"
    chrome "https://up4pc.com/"
    chrome "https://sjcrack.com/"
    chrome "https://softoroom.net/ptopic87872.html"
}

w_openjdk() {
    echo "openjdk"
    chrome "https://www.bell-sw.com/pages/java-14/"
}

w_es() {
    echo "es  下载地址"
    chrome "https://www.elastic.co/cn/downloads/elasticsearch"
}

w_grafana() {
    echo "dashboard"
    chrome "https://grafana.com/grafana/dashboards?direction=asc&orderBy=name&dataSource=prometheus"
}

w_consul() {
    echo "http consul"
    chrome "https://www.consul.io/api/index.html"
}

w_k3s() {
    echo "轻量级 iot k8s ➡️ k3s"
    chrome "https://k3s.io/"
    echo "kube-dashboard"
    chrome "https://rancher.com/docs/k3s/latest/en/installation/kube-dashboard/"
}

w_pycharm() {
    echo "快捷键"
    chrome "https://www.cnblogs.com/xuanan/p/9140671.html"
}

w_webdis() {
    echo "http ws redis"
    chrome "https://www.iyunv.com/thread-631825-1-1.html"
    chrome "https://github.com/nicolasff/webdis"
    chrome "https://webd.is/"
}

w_display_control() {
    echo "显示器控制"
    chrome "https://github.com/the0neyouseek/MonitorControl/releases/"
}

w_greasy() {
    echo "油猴脚本"
    chrome "https://greasyfork.org/zh-CN/"
}

w_go_blog() {
    echo "一个很赞的go blog"
    chrome "https://www.qikqiak.com/"
}

w_chromedriver() {
    echo "chromedriver"
    chrome "https://npm.taobao.org/mirrors/chromedriver/"
}

w_mi_root() {
    echo "小米手机root"
    chrome "http://www.miui.com/unlock/download.html"
    chrome "https://www.xiaomi.cn/post/3892846"
}

w_adb() {
    echo "adb"
    chrome "https://github.com/mzlogin/awesome-adb#%E9%9F%B3%E9%87%8F%E6%8E%A7%E5%88%B6"
    echo "adb_py"
    chrome "https://github.com/openatx/uiautomator2"
    chrome "https://github.com/openatx/weditor"
}

w_ocr() {
    echo "api"
    chrome "http://ocr.space/OCRAPI"
    echo "百度 ocr 5w 普通 500 高精度"
    chrome "https://cloud.baidu.com/doc/OCR/s/fk3h7xu7h"
}

w_baidu_api() {
    echo "百度api文档"
    chrome "https://cloud.baidu.com/doc/index.html"
}

w_pic_deal() {
    echo "图像处理"
    chrome "https://www.cnblogs.com/blackhat520/p/4187438.html"
    chrome "https://imagemagick.org/script/command-line-processing.php"
}

w_yq() {
    echo "jq 的兄弟 yq 处理yaml xml"
    chrome "https://github.com/kislyuk/yq"
    chrome "https://kislyuk.github.io/yq/"
}

# 宝藏
w_bz() {
    echo "破解软件大全"
    chrome "https://masuit.com/"
    chrome "https://pan.masuit.com/"
    echo "宝藏级别开源项目"
    chrome "https://baiyue.one/"
}

w_rdm() {
    echo "redis desktop manager"
    chrome "https://github.com/lingjieee/rdm-release/releases/tag/2020.0"
    chrome "https://github.com/qishibo/AnotherRedisDesktopManager/releases"
}

w_gnu() {
    echo "GNU 软件"
    chrome "https://www.gnu.org/software/"
}

w_weather() {
    echo "天气接口"
    chrome "https://github.com/chubin/wttr.in"
}

w_win10() {

    echo "win10激活码与下载镜像"
    chrome "http://www.win10com.com/win10jihuo/1187.html"
    chrome "http://www.xitongzhijia.net/win10/yuanban/"
}

w_opan() {
    echo "打开 neo.pub onedrive"
    chrome "https://druexoy-my.sharepoint.com/personal/1_ms_neo_pub/_layouts/15/onedrive.aspx"
}

w_software_paint() {
    echo "mac 画图图软件"
    echo "http://file.neo.pub/mac/Paintbrush-2.5.0.zip"
}

w_emoji() {
    echo "emoji 代码大全"
    echo ":cloud"
    chrome "https://bbs.52svip.cn/emoji/"
}

w_big_drive() {
    echo "超大网盘申请"
    echo "https://gd.zxd.workers.dev/ Google TeamDrive"
    chrome "https://gd.zxd.workers.dev/"

}

w_org() {
    echo "开源科学"
    chrome "https://www.open-mpi.org/" #高性能并行计算/
}

w_zerotier() {
    echo "zerotier vpn"
    chrome "https://my.zerotier.com/network/1c33c1ced00b399a"
}

w_app_broken() {
    chrome "https://www.xxmac.com/apple-app.html"
}

#################################################################
######################### AWS web ###############################
#################################################################

w_aws_WorkDocs() {
    chrome "https://amazon.awsapps.com/workdocs/index.html#/shared"
}

w_aws_highspot_SSA() {
    chrome "https://aws.highspot.com/spots/5e8ecfbf34d6be5ed9656b9f?sortby=time_added&list=all"
}

w_aws_highspot_search() {
    KEY_WORDS=${1:-Redshift}
    chrome "https://aws.highspot.com/spots/5b345b9d1279580d28023d48?q=$KEY_WORDS"
}

w_aws_virtual_learning_qs() {
    chrome "https://amazon-quicksight-spring-virtual-learning-series.splashthat.com"
}

w_aws_HR() {
    chrome "https://inside.hr.amazon.dev/cn/zh_cn/employment/getting-hr-help.html"
}

# kafka 全家桶 url
w_archon_kafka_all() {
    chrome "http://192.168.100.142:8004/"  # ZK-UI
    chrome "http://192.168.100.142:18080/" # kafka manager UI
    echo "kafka manager UI admin:admin"
    chrome "http://192.168.100.142:19000/" # YAHOO CMAK
    chrome "http://192.168.100.142:18000/" # SCHEMA REGISTRY
    chrome "http://192.168.100.142:28000/" # KAFKA TOPICS
    chrome "http://192.168.100.142:38000/" # KAFKA CONNECT
}

w_pic_bg_remove() {
    chrome "https://www.aigei.com/bgremover/"
}

w_java_package() {
    echo "${1:-mysql}"
    chrome "https://mvnrepository.com/artifact/${1:-mysql}"
}

w_java_search_groupid() {
    echo "${1:-monetdb}"
    chrome "https://search.maven.org/search?q=g:${1:-monetdb}"
}

w_java_search_artifactid() {
    echo "${1:-hudi-cli}"
    chrome "https://search.maven.org/search?q=a:${1:-hudi-cli}"
}

w_neo_summit() {
    echo "# 2021-10-22\tqcon世界开发者大会\t面向未来的湖仓架构_大数据平台的技术现状与趋势"
    echo "https://qcon.infoq.cn/2021/shanghai/presentation/3994"
    chrome "https://qcon.infoq.cn/2021/shanghai/presentation/3994"
    echo ""

    echo "# 2021-10-28\taws数据驱动创新大会\tTableau_on_亚马逊云科技-云端现代分析之旅"
    echo "https://innovate.awssummit.cn/"
    chrome "https://innovate.awssummit.cn/"

}

w_reinvent() {
    echo "aws reinvent"
    chrome "https://portal.awsevents.com/events/reInvent2021/dashboard"
}

w_open_source() {
    echo "查找apache资源二进制包"
    chrome "https://repo1.maven.org/maven2/org/apache/${1:-hudi}"
}

# w_awscli() {
#     chrome "https://docs.aws.amazon.com/zh_cn/cli/latest/index.html"
# }

w_starrocks() {
    echo "PPT | StarRocks Summit 2021 嘉宾PPT合集"
    chrome "https://forum.starrocks.com/t/topic/588"
    echo "StarRocks Summit 2021"
    chrome "https://www.bilibili.com/read/cv13811135"

}

# AWS 🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀

w_aws_bigdata() {
    chrome "https://aws.amazon.com/cn/big-data/datalakes-and-analytics/"
}

w_aws_qa_center() {
    echo "aws AWS 知识中心"
    chrome "https://aws.amazon.com/cn/premiumsupport/knowledge-center/"
}

w_aws_qa_es() {
    chrome "https://aws.amazon.com/cn/opensearch-service/faqs/"
}

w_aws_qa_redshift() {
    chrome "https://aws.amazon.com/cn/redshift/faqs/"
}

w_aws_qa_glue() {
    chrome "https://aws.amazon.com/cn/glue/faqs/"
}

w_aws_qa_athena() {
    chrome "https://aws.amazon.com/cn/athena/faqs/"
}

w_aws_qa_emr() {
    chrome "https://aws.amazon.com/cn/emr/faqs/"
}

w_aws_qa_msk() {
    chrome "https://aws.amazon.com/cn/msk/faqs/"
}

w_aws_qa_kinesis() {
    chrome "https://aws.amazon.com/cn/kinesis/data-streams/faqs/"
    chrome "https://aws.amazon.com/cn/kinesis/data-firehose/faqs/"
    chrome "https://aws.amazon.com/cn/kinesis/data-analytics/faqs/"
    chrome "https://aws.amazon.com/cn/kinesis/video-streams/faqs/"

}

w_aws_qa_quicksight() {
    chrome "https://aws.amazon.com/cn/quicksight/resources/faqs/"
}

w_aws_qa_s3() {
    chrome "https://aws.amazon.com/cn/s3/faqs/"
}

w_aws_qa_dms() {
    chrome "https://aws.amazon.com/cn/dms/faqs/"
}

w_aws_qa_mwaa() {
    chrome "https://aws.amazon.com/cn/managed-workflows-for-apache-airflow/faqs/"
}

w_aws_qa_lakeformation() {
    chrome "https://aws.amazon.com/cn/lake-formation/faqs/"
}

w_aws_qa_backup() {
    chrome "https://aws.amazon.com/cn/backup/faqs/"
}

w_aws_qa_sagemaker() {
    chrome "https://aws.amazon.com/cn/sagemaker/faqs/"
}

w_aws_qa_eks() {
    chrome "https://aws.amazon.com/cn/eks/faqs/"
    chrome "https://aws.amazon.com/cn/eks/eks-anywhere/faqs/"
    chrome "https://aws.amazon.com/cn/ecr/faqs/"
    chrome "https://aws.amazon.com/cn/ecs/faqs/"
    chrome "https://aws.amazon.com/cn/app2container/faqs/"
    chrome "https://aws.amazon.com/cn/fargate/faqs/"
    chrome "https://aws.amazon.com/cn/containers/copilot/faqs/"
}

# AWS 🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀🚀

w_ipad() {
    echo "查看ipad pro 2021 icare 时间等信息"
    chrome "https://checkcoverage.apple.com/cn/zh/?sn=XVGP02YDY3"
}

w_aws_news() {
    echo "aws 产品 新 features"
    chrome "https://aws.amazon.com/cn/new"
}

w_sourcegraph() {
    echo "github + gitlab 代码搜索"
    chrome "https://sourcegraph.com"
}

w_ebook_new() {
    echo "资助电子书"
    chrome "https://www.lunaticai.com/"
    chrome "https://dokumen.pub/"
}

w_arm_pk() {
    echo "移动芯片对比"
    chrome "https://www.socpk.com/"
}

w_emr_releases() {
    echo "emr各版本组件version"
    chrome "https://docs.aws.amazon.com/emr/latest/ReleaseGuide/emr-release-app-versions-6.x.html"
}

w_spark_issues() {
    echo "spark issues"
    chrome "https://issues.apache.org/jira/browse/SPARK-36217"
}

w_panchao_blog() {
    echo "潘超Blog"
    chrome "http://analytics-aws.yihongyeyan.com/archives/"
}

w_qingyuan_github() {
    echo "清原 Github"
    chrome "https://github.com/qingyuan18"
}

w_cloud_convert() {
    echo "云格式转化"
    chrome "https://cloudconvert.com/"
}

w_aws_big_data_blog() {
    echo "aws 大数据博客"
    chrome "https://aws.amazon.com/cn/blogs/big-data/"
}

w_flink() {
    echo "flink 相关资料"
    chrome "https://flink-learning.org.cn/"
}

w_spark() {
    echo "spark example host 🧨 Spark PySpark Pandas Hive Kafka H2O.ai"
    chrome "https://sparkbyexamples.com/"
    chrome "https://sparkbyexamples.com/pyspark/different-ways-to-create-dataframe-in-pyspark/"
}

w_redshift() {
    echo "redshift sql教程"
    chrome "https://popsql.com/learn-sql/redshift"
}

w_wx_reader() {
    echo "微信读书上传"
    chrome "https://reader.qq.com/"
}

w_python_spark() {
    echo "pyspark.sql.types"
    chrome "https://vimsky.com/examples/detail/python-module-pyspark.sql.types.html"

    echo "Pyspark.Sql.Functions"
    chrome "https://www.jianshu.com/p/c6025af1f931"

    echo "spark函数大全 azure版"
    chrome "https://docs.microsoft.com/zh-cn/azure/databricks/spark/latest/spark-sql/language-manual/"

}

w_rand() {
    echo "兰登公司"
    chrome "https://www.rand.org/pubs/research_reports/RR3067.html"
}

w_sparksql() {
    echo "sparksql Doc"
    chrome "https://spark.apache.org/docs/latest/api/sql/"
}

w_html_table_2_md() {
    echo "html-table 转 md-table"
    echo "https://github.com/suntong/html2md" # CLI html2md
    chrome "https://jmalarcon.github.io/markdowntables/"
}

w_redis_redisinsight() {
    echo "redis 可视化软件 官方"
    chrome "https://redis.com/redis-enterprise/redis-insight/"
}

w_cidr() {
    echo "cidr  网段解析"
    chrome "https://www.ipaddressguide.com/cidr"
}

w_benchmark() {
    echo "ClickBench: a Benchmark For Analytical Databases"
    chrome "https://github.com/ClickHouse/ClickBench"
}

w_jar_hudi() {
    echo "maven的仓库Hudi"
    chrome "https://repo1.maven.org/maven2/org/apache/hudi"
}

w_bigdata_github_article() {
    echo "3.1 大数据"
    chrome "https://github.com/attacteng/-qianduan/tree/9341b9190ccd186ef4aaa2cf8f394b9158ed0f12/3.1%20%E5%A4%A7%E6%95%B0%E6%8D%AE"
}

w_maven(){
    echo "maven jar 仓库"
    chrome "https://repo.maven.apache.org/maven2/org/apache/flink/"
    chrome "https://repo1.maven.org/maven2/org/apache/hudi/hudi-spark-bundle_2.11/0.10.1/"
}