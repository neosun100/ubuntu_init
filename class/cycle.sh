####################
# shell 循环 大全
####################
# https://blog.csdn.net/taiyang1987912/article/details/38929069
# Shell编程中循环命令用于特定条件下决定某些语句重复执行的控制方式
# 有三种常用的循环语句：
# for、while和until。
# while循环和for循环属于“当型循环”，
# 而until属于“直到型循环”。
# 循环控制符：break和continue控制流程转向。


####################
# for 循环
# for循环有三种结构：
# 一种是列表for循环
# 第二种是不带列表for循环
# 第三种是类C风格的for循环。
#####################

####################
# 1. 列表for循环
_forlistcycle(){
echo "#######################################"
echo "🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌"
echo "#######################################"
echo ""
echo "#!/bin/bash"
echo ""
echo "for varible1 in {1..5}"
echo "#for varible1 in 1 2 3 4 5"
echo "do"
echo '    echo "Hello, Welcome $varible1 times "'
echo "done"
echo ""

echo "do和done之间的命令称为循环体，"
echo "执行次数和list列表中常数或字符串的个数相同。"
echo "for循环，首先将in后list列表的第一个常数或字符串赋值给循环变量，"
echo "然后执行循环体，以此执行list，最后执行done命令后的命令序列。"
echo "Sheel支持列表for循环使用略写的计数方式，"
echo "1～5的范围用{1..5}表示（大括号不能去掉，"
echo "否则会当作一个字符串处理）。"
echo "🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒"
for varible1 in {1..5}
#for varible1 in 1 2 3 4 5
do
     echo "Hello, Welcome $varible1 times "
done
echo ""

echo "#######################################"
echo "🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌"
echo "#######################################"
echo ""
echo "Sheel中还支持按规定的步数进行跳跃的方式实现列表for循环，"
echo "例如计算1～100内所有的奇数之和。"
echo ""

echo "#!/bin/bash"
echo "sum=0"
echo ""
echo 'for i in {1..10..2}'
echo "do"
echo '    let "sum+=i"'
echo "done"
echo ""    
echo 'echo "sum=$sum"'

echo "🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒"

#!/bin/bash
sum=0
 
for i in {1..10..2}
do
    let "sum+=i"
done
    
echo "sum=$sum"
echo ""
echo "通过i的按步数2不断递增，计算sum值为2500。"
echo "同样可以使用seq命令实现按2递增来计算1～10内的所有奇数之和，"
echo 'for i in $(seq 1 2 10)，seq表示起始数为1，跳跃的步数为2，结束条件值为10。'
echo ""

echo "#######################################"
echo "🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌"
echo "#######################################"
echo ""
echo "for循环对字符串进行操作，例如通过for循环显示当前目录下所有的文件。"
echo ""

echo 'for file in $( ls | tail )'
echo "#for file in *"
echo "do"
echo '  echo "file: $file"'
echo "done"

echo ""
echo "🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒" 


for file in $( ls | tail )
#for file in *
do
   echo "file: $file"
done

echo '也可一使用for file in *，通配符* 产生文件名扩展，匹配当前目录下的所有文件。'
}


alias c_forlistcycle="_forlistcycle | hcat --syntax python"



# for通过命令行来传递脚本中for循环列表参数
_forargcycle(){

echo "#######################################"
echo "🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌"
echo "#######################################"
echo ""

echo "for通过命令行来传递脚本中for循环列表参数"

echo 'echo "number of arguments is $#"'
 
echo 'echo "What you input is: "'
 
echo 'for argument in "$@"'
echo 'do'
echo '    echo "$argument"'
echo 'done'

echo ""

echo "🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒" 
echo ""
echo "number of arguments is $#"
 
echo "What you input is: "
 
for argument in "$@"
do
    echo "$argument"
done
}

alias c_forargcycle="_forargcycle 1 2 3 4 5 | phcat"


# 不带列表for循环
_forunlistcycle(){
echo "#######################################"
echo "🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌"
echo "#######################################"
echo ""
echo "由用户制定参数和参数的个数，与上述的for循环列表参数功能相同。"
echo '#!/bin/bash'
 
echo 'echo "number of arguments is $#"'
 
echo 'echo "What you input is: "'
 
echo 'for argument'
echo 'do'
echo '    echo "$argument"'
echo 'done'

echo ""
echo "🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒" 
echo ""

echo '#!/bin/bash'
 
echo "number of arguments is $#"
 
echo "What you input is: "
 
for argument
do
    echo "$argument"
done

echo ""
echo '比上述代码少了$@参数列表，$*参数字符串。'
}

alias c_forunlistcycle="_forunlistcycle 1 2 3 4 5 | phcat"


# 类C风格的for循环 也被称为计次循环
_forccycle(){
echo "#######################################"
echo "🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌"
echo "#######################################"
echo ""
echo "类C风格的for循环,也被称为计次循环"
echo ""
echo 'for ((integer = 1; integer <= 5; integer++))'
echo 'do'
echo '    echo "$integer"'
echo 'done'

echo ""


echo 'for中第一个表达式（integer = 1）是循环变量赋初值的语句，'
echo '第二个表达式（integer <= 5）决定是否进行循环的表达式，'
echo '退出状态为非0时将退出for循环执行done后的命令（与C中的for循环条件是刚好相反的）。'
echo '第三个表达式（integer++）用于改变循环变量的语句。'


echo ""
echo "🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒" 
echo ""

for ((integer = 1; integer <= 5; integer++))
do
    echo "$integer"
done

echo ""
echo "#######################################"
echo "🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌🍌"
echo "#######################################"
echo ""
echo "类C的for循环计算1～10内所有的奇数之和。"
echo ""
echo 'sum=0'
 
echo 'for (( i = 1; i <= 10; i = i + 2 ))'
echo 'do'
echo '     let "sum += i"'
echo 'done'
echo ' '
echo 'echo "sum=$sum"'

echo ""
echo "🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒🐒" 
echo ""

sum=0
 
for (( i = 1; i <= 10; i = i + 2 ))
do
     let "sum += i"
done
 
echo "sum=$sum"
}

alias c_forccycle="_forccycle | hcat --syntax python"


###################
# while 循环测试
# TODO:

###################
# unitl 循环测试
_untilcycle(){
i=0
echo "# unitl 循环测试"
echo "# until命令和while命令类似"
echo "# while能实现的脚本until同样也可以实现"
echo "# 但区别是until循环的退出状态是不为0"
echo "# 退出状态是为0（与while刚好相反）"
echo "# 即whie循环在条件为真时继续执行循环而until则在条件为假时执行循环。 "
until [[ "$i" -gt 5 ]]    #大于5
do
    let "square=i*i"
    echo "$i * $i = $square"
    let "i++"
done
}

##################
# while 循环测试
# TODO:
whilecycle(){

}



##################
# 嵌套 循环测试

######################
# 乘法口诀表
# 循环嵌套 for
# 一个循环体内又包含另一个完整的循环结构
# 在外部循环的每次执行过程中都会触发内部循环
# for、while、until可以相互嵌套。
multable(){
for (( i = 1; i <=9; i++ ))
do
    
    for (( j=1; j <= i; j++ ))
    do
        let "temp = i * j"     
        echo -n "$i*$j=$temp  "
     done 
     
     echo ""   #output newline
done
}


########################
# for循环嵌套实现*图案排列
eeeeee(){
for ((i=1; i <= 9; i++))
do
    j=9;
    while ((j > i))
    do
        echo -n " "
        let "j--"
    done
    k=1
    while ((k <= i))
    do
        echo -n "*"
        let "k++"
    done
    echo ""
done
}




#########################
# 循环控制符break和continue
# TODO:

# FIXME:




########################
# select结构
# TODO:
