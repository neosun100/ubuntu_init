netprint() {
    if [ $# -lt 1 ]; then
        echo "用法: netprint <文件URL> [-p 打印机] [-d 单面/双面] [-o 纸张方向] [-r 页码范围] [-n 份数] [-s 纸张大小]"
        return 1
    fi

    URL=""
    PRINTER=""
    DUPLEX="single"
    ORIENTATION=""
    RANGE=""
    COPIES=1
    PAPER_SIZE="A4"

    while [[ "$#" -gt 0 ]]; do
        case "$1" in
            -p) PRINTER="$2"; shift ;;
            -d) DUPLEX="$2"; shift ;;
            -o) ORIENTATION="$2"; shift ;;
            -r) RANGE="$2"; shift ;;
            -n) COPIES="$2"; shift ;;
            -s) PAPER_SIZE="$2"; shift ;;
            -h|--help) netprint ""; return 0 ;;
            *) URL="$1" ;;
        esac
        shift
    done

    if [ -z "$URL" ]; then
        echo "错误：必须提供文件 URL"
        return 1
    fi

    if [ -z "$PRINTER" ]; then
        PRINTER=$(lpstat -d | awk '{print $NF}')
        if [ -z "$PRINTER" ]; then
            echo "错误：未找到默认打印机，请使用 -p 选项指定打印机"
            return 1
        fi
    fi

    # 统一文件名，避免特殊字符问题
    FILENAME="downloaded.pdf"
    
    echo "下载文件：$URL -> $FILENAME"
    curl -o "$FILENAME" "$URL"

    if [ ! -f "$FILENAME" ]; then
        echo "下载失败！"
        return 1
    fi

    if [[ "$DUPLEX" == "double" ]]; then
        PRINT_OPTIONS="-o sides=two-sided-long-edge"
    else
        PRINT_OPTIONS="-o sides=one-sided"
    fi

    if [[ "$ORIENTATION" == "landscape" ]]; then
        PRINT_OPTIONS="$PRINT_OPTIONS -o landscape"
    elif [[ "$ORIENTATION" == "portrait" ]]; then
        PRINT_OPTIONS="$PRINT_OPTIONS -o portrait"
    fi

    if [[ "$PAPER_SIZE" == "A3" ]]; then
        PRINT_OPTIONS="$PRINT_OPTIONS -o media=A3"
    else
        PRINT_OPTIONS="$PRINT_OPTIONS -o media=A4"
    fi

    if [[ -n "$RANGE" ]]; then
        PRINT_OPTIONS="$PRINT_OPTIONS -o page-ranges=$RANGE"
    fi

    PRINT_OPTIONS="$PRINT_OPTIONS -n $COPIES"

    echo "发送 $FILENAME 到打印机 $PRINTER，选项: $PRINT_OPTIONS"
    lp -d "$PRINTER" $PRINT_OPTIONS "$FILENAME"

    rm -f "$FILENAME"
    echo "打印任务已完成。"
}