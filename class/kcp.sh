# https://www.qinzc.me/post-201.html

# kcptun为何物？
# Kcptun 是一个非常简单和快速的，
# 基于 KCP 协议的 UDP 隧道，它可以将 TCP 流转换为 KCP+UDP 流。
# 而 KCP 是一个快速可靠协议，能以比 TCP 浪费10%-20%的带宽的代价，换取平均延迟降低 30%-40%，且最大延迟降低三倍的传输效果。

# Kcptun 是 KCP 协议的一个简单应用，可以用于任意 TCP 网络程序的传输承载，以提高网络流畅度，降低掉线情况。
# 由于 Kcptun 使用 Go 语言编写，内存占用低（经测试，在64M内存服务器上稳定运行），而且适用于所有平台，甚至 Arm 平台。

# Kcptun相关项目地址：

# https://github.com/xtaci/kcptun/

# https://github.com/shadowsocks/kcptun/releases

# https://github.com/dfdragon/kcptun_gclient/releases


# KCP协议简介
# KCP是一个快速可靠协议，能以比 TCP浪费10%-20%的带宽的代价，换取平均延迟降低 30%-40%，且最大延迟降低三倍的传输效果。
# 纯算法实现，并不负责底层协议（如UDP）的收发，需要使用者自己定义下层数据包的发送方式，以 callback的方式提供给 KCP。 
# 连时钟都需要外部传递进来，内部不会有任何一次系统调用。


# 整个协议只有 ikcp.h, ikcp.c两个源文件，可以方便的集成到用户自己的协议栈中。
#  也许你实现了一个P2P，或者某个基于 UDP的协议，而缺乏一套完善的ARQ可靠协议实现，那么简单的拷贝这两个文件到现有项目中，稍微编写两行代码，即可使用。

# 技术特性
# TCP是为流量设计的（每秒内可以传输多少KB的数据），讲究的是充分利用带宽。
# KCP是为流速设计的（单个数据包从一端发送到一端需要多少时间）
# 以10%-20%带宽浪费的代价换取了比 TCP快30%-40%的传输速度。
# TCP信道是一条流速很慢，但每秒流量很大的大运河
# 而KCP是水流湍急的小激流。
# KCP有正常模式和快速模式两种

# https://github.com/xtaci/kcptun/releases



install_kcp(){
echo "服务端 必须有公网ip所以必然为一台 amd64 linux 服务器";
cd ~/upload;

sudo mkdir -p /data/logs/kcp && \
sudo chmod -R 777  /data/logs;

[[ -d ~/upload/kcptun ]] && \
echo "`date +'%Y-%m-%d %H:%M:%S'` kcp path exist" || \
(
echo "`date +'%Y-%m-%d %H:%M:%S'` kcp path is not exist need download" && \
mkdir -p ~/upload/kcptun && \
cd ~/upload/kcptun && \
get_latest_package https://github.com/xtaci/kcptun/releases && \
tar -zxvf kcptun-*.tar.gz && \
mv server_linux_amd64 kcp_server && \
mv client_linux_amd64 kcp_client && \
echo "kcp server version `./kcp_server --version`" && \
echo "kcp client version `./kcp_client --version`" && \
echo "`date +'%Y-%m-%d %H:%M:%S'` kcp 安装完成✅" 
)

cd ~/upload;
}


kcp_server_restart(){
echo "`date +'%Y-%m-%d %H:%M:%S'` 如果存在则结束 kcp_server"
pnamekill kcp_server;

echo "`date +'%Y-%m-%d %H:%M:%S'` 重新启动 kcp-server"
nohup ~/upload/kcptun/kcp_server \
--listen ${1:-':44444'} \
--target ${2:-'127.0.0.1:7777'} \
-key bilibili \
-mode fast3 \
-nocomp \
-sockbuf 16777217 
-dscp 46 > /data/logs/kcp/server.log 2>&1 &
echo "`date +'%Y-%m-%d %H:%M:%S'` 重新启动 kcp-server 完成 ✅"
}



kcp_client_restart(){
echo "`date +'%Y-%m-%d %H:%M:%S'` 如果存在则结束 kcp_client";
pnamekill kcp_client;

echo "`date +'%Y-%m-%d %H:%M:%S'` 重新启动 kcp-client"   
nohup ~/upload/kcptun/kcp_client \
--localaddr ${1:-':33333'} \
--remoteaddr ${2:-'192.168.191.6:44444'} \
-key bilibili \
-mode fast3 \
-nocomp \
-autoexpire 900 \
-sockbuf 16777217 \
-dscp 46 > /data/logs/kcp/client.log 2>&1 &
echo "`date +'%Y-%m-%d %H:%M:%S'` 重新启动 kcp-client 完成 ✅"
}

