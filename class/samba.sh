
# asmb
# airdisk smb

vim_smb(){
echo "编辑配置文件";
echo '注释掉 syslog = 0' | lcat;
sleep 3;
sudo vim /etc/samba/smb.conf;
}


asmb(){
echo "~/upload/airdisk.ini 填入账户密码"
sudo smbclient //192.168.31.35/AirDisk -A ~/upload/airdisk.ini -c "$*"
}

asmb_demo(){
echo "asmb 后结命令即可，格式如下";
echo "'cd \"USB-DISK-A/来自MIX的备份/my Pictures/WeiXin\"; ls; get wx_camera_1567158731629.jpg'" | lcat;
asmb 'cd "USB-DISK-A/来自MIX的备份/my Pictures/WeiXin"; ls; get wx_camera_1567158731629.jpg"';
}


asmb_help(){
echo "https://www.cnblogs.com/wj78080458/p/10847690.html";
asmb '?';
}

install_smb(){
echo y | sudo apt-get install samba 
sudo mv /etc/samba/smb.conf /etc/samba/smb.conf.bak
sudo cp  --force  /home/neo/upload/ubuntu_init/config/smb.conf /etc/samba/smb.conf
sudo vim /etc/samba/smb.conf
sudo smbpasswd  -a  neo # 添加账户
sudo service smbd restart
}