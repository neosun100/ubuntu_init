w_aws_web() {
    chrome "https://knet.amazon.com"                                                                     # 学习门户
    chrome "https://kiku.aws.training/"                                                                  # 培训
    chrome "https://wisdom.corp.amazon.com/"                                                             #  AWS 知识头条
    chrome "https://phonetool.amazon.com"                                                                # AWS Facebook
    chrome "https://aws.highspot.com/"                                                                   # talk show ppt
    chrome "https://aws.highspot.com/spots/5e563f4c659e9316c06202da"                                     # GCR talk show
    chrome "https://broadcast.amazon.com/videos"                                                         # YouTube
    chrome "https://broadcast.amazon.com/channels/5116"                                                  # GCR youtube
    chrome "https://inside.hr.amazon.dev/cn/zh_cn/employment.html"                                       # 薪酬福利
    chrome "https://w.amazon.com/bin/view/AWS/GreaterChina/Internal_Enablement/FrequentlyUsedToolsinAWS" # 25
    chrome "https://sage.amazon.com/questions"                                                           # AWS 知乎
}

w_aws_check_work() {
    echo "刘一白"
    chrome "https://w.amazon.com/bin/view/WWRO_CRM/Engineering/ADMIN/Help/HowtocreateCase/AccessPermission"
}

w_aws_emr_best() {
    echo "EMR最佳实践"
    chrome "https://aws.github.io/aws-emr-best-practices/"
}

w_aws_ec2() {
    echo "instance-types-list"
    chrome "https://aws.amazon.com/cn/ec2/instance-types/"
}

w_aws_resource() {
    chrome "https://awesome-aws-workshops.com/#datalakes-and-analytics"
    chrome "https://internal.workshops.aws/card/glue"
    chrome "https://kumo-knowledge-ui-iad-prod.amazon.com/"
    chrome "https://wisdom.corp.amazon.com/"
}

w_aws_workshop() {
    chrome "https://awesome-aws-workshops.com/#datalakes-and-analytics"
    chrome "https://internal.workshops.aws/"
}

w_console_redshift() {
    chrome "https://console.aws.amazon.com/redshift/home?region=us-east-1#clusters"
}

w_awscli() {
    chrome "https://awscli.amazonaws.com/v2/documentation/api/latest/reference/index.html"
}

w_aws_sla() {
    echo "参考如下链接获得各个服务的SLA"
    chrome "https://aws.amazon.com/cn/legal/service-level-agreements/"
}

w_aws_kdg() {
    echo "kinesis data generator UI in Virginia"
    chrome "https://awslabs.github.io/amazon-kinesis-data-generator/web/producer.html?upid=us-west-2_M8o52l9qw&ipid=us-west-2:e68aa366-7237-4723-b038-9fd57cc778c0&cid=hjjc6gef9oemo4lbi61cs5r5n&r=us-west-2#constant-rate-tab"
}

w_aws_Authentication() {
    echo "AWS 认证证书"
    chrome "https://www.credly.com/earner/earned"
    chrome "https://training.linuxfoundation.cn/"
}

w_aws_salesforce() {
    echo "salesforce"
    chrome "https://aws-crm.lightning.force.com/lightning/r/Campaign/7014z000001gHfpAAE/view"
}

w_panchao_highspot() {
    echo "chao highspot"
    chrome "https://aws.highspot.com/users/603e0349c79c521a6495f165"
}

w_aws_baoxian() {
    open "/Users/jiasunm/Documents/AWS产品/亚马逊保险报销资料"
}

w_aws_anna_chatbot() {
    echo "aws ssa 自助聊天问题机器人"
    chrome "https://aws-jp-ssoe-concierge.corp.amazon.com/chat.html?geo=amer"
    echo "参考文档"
    echo "/Users/jiasunm/Documents/AWS产品/999.AWS_Email_backup_PDF/[Launch Announcement] Anna the Chatbot - SUN, Neo.pdf"
    open "/Users/jiasunm/Documents/AWS产品/999.AWS_Email_backup_PDF/[Launch Announcement] Anna the Chatbot - SUN, Neo.pdf"
}

w_aws_accolade() {
    echo "亚马逊员工评价系统"
    chrome "https://accolades.corp.amazon.com/"
}

w_aws_skills() {
    echo "亚马逊技能系统"
    chrome "https://skills.amazon.com"
}

w_aws_partners() {
    echo "亚马逊合作伙伴明细"
    chrome "https://partners.amazonaws.com/search/partners/"
}

w_aws_benefits() {
    echo "亚马逊员工光怀服务"
    chrome "https://helpwhereyouare.com/"
}


w_aws_build_workshop(){
    echo ""
    open "/Users/jiasunm/WorkDocsDownloads/WorkDocs-Bulk-Download-2022-2-10 9_54_05"
}


w_aws_activate(){
    echo "aws 认证设备"
    chrome "https://activate.amazon-crop.com/"
}


w_aws_marketplace(){
    echo "aws marketplace"
    chrome "https://aws.amazon.com/marketplace"
}

w_aws_databricks_virginia(){
    echo "打开AWS Databricks 控制台"
    chrome "https://dbc-c774401c-d904.cloud.databricks.com"
}

w_databricks(){
    echo "azure databricks"
    chrome "https://docs.microsoft.com/zh-hk/azure/databricks/"
    echo "ali databricks"
    chrome "https://help.aliyun.com/product/167610.html"
    echo "Official databricks"
    chrome "https://docs.databricks.com/index.html#"
}


w_aws_activate(){
    echo "移动端认证"
    chrome "https://activate.amazon-corp.com"
}


w_aws_solution_analytics(){
    echo "Solutions for Analytics"
    chrome "https://aws.amazon.com/cn/solutions/analytics/"
}


w_aws_phone(){
    echo "Login authentication of aws mobile phone application"
    chrome "https://activate.amazon-corp.com"
}


w_aws_pfr(){
    echo "aws pfr Product_Feature_Request"
    chrome "https://aws-crm.lightning.force.com/lightning/o/Product_Feature_Request__c/home"
}


w_aws_tableau(){
    echo "aws 报表"
    chrome "https://gc-bms.corp.amazon.com/#/workbooks/777/views"
}

w_databricks_blog(){
    echo "databricks blog"
    chrome "https://www.databricks.com/blog"
}


w_aws_ana_service(){
    echo "aws 分析产品大全"
    chrome "https://aws.amazon.com/cn/big-data/datalakes-and-analytics/"
}

w_aws_whats_news(){
    echo "ana whats_new"
    chrome "https://aws.amazon.com/cn/new/?whats-new-content-all.sort-by=item.additionalFields.postDateTime&whats-new-content-all.sort-order=desc&awsf.whats-new-analytics=general-products%23aws-data-pipeline%7Cgeneral-products%23aws-glue%7Cgeneral-products%23aws-lake-formation%7Cgeneral-products%23amazon-athena%7Cgeneral-products%23amazon-cloudsearch%7Cgeneral-products%23amazon-emr%7Cgeneral-products%23amazon-kinesis%7Cgeneral-products%23amazon-kinesis-analytics%7Cgeneral-products%23amazon-kinesis-firehose%7Cgeneral-products%23amazon-kinesis-streams%7Cgeneral-products%23amazon-kinesis-video-streams%7Cgeneral-products%23amazon-msk%7Cgeneral-products%23amazon-opensearch-service%7Cgeneral-products%23amazon-quicksight%7Cgeneral-products%23amazon-redshift&awsf.whats-new-app-integration=*all&awsf.whats-new-arvr=*all&awsf.whats-new-blockchain=*all&awsf.whats-new-business-applications=*all&awsf.whats-new-cloud-financial-management=*all&awsf.whats-new-compute=*all&awsf.whats-new-containers=*all&awsf.whats-new-customer-enablement=*all&awsf.whats-new-customer%20engagement=*all&awsf.whats-new-database=*all&awsf.whats-new-developer-tools=*all&awsf.whats-new-end-user-computing=*all&awsf.whats-new-mobile=*all&awsf.whats-new-gametech=*all&awsf.whats-new-iot=*all&awsf.whats-new-machine-learning=*all&awsf.whats-new-management-governance=*all&awsf.whats-new-media-services=*all&awsf.whats-new-migration-transfer=*all&awsf.whats-new-networking-content-delivery=*all&awsf.whats-new-quantum-tech=*all&awsf.whats-new-robotics=*all&awsf.whats-new-satellite=*all&awsf.whats-new-security-id-compliance=*all&awsf.whats-new-serverless=*all&awsf.whats-new-storage=*all"
}


w_aws_news(){
    echo "aws ana news"
    chrome "https://aws.amazon.com/cn/about-aws/whats-new/2022/?whats-new-content-all.sort-by=item.additionalFields.postDateTime&whats-new-content-all.sort-order=desc&awsf.whats-new-analytics=general-products%23aws-data-pipeline%7Cgeneral-products%23aws-glue%7Cgeneral-products%23aws-lake-formation%7Cgeneral-products%23amazon-athena%7Cgeneral-products%23amazon-cloudsearch%7Cgeneral-products%23amazon-emr%7Cgeneral-products%23amazon-kinesis%7Cgeneral-products%23amazon-kinesis-analytics%7Cgeneral-products%23amazon-kinesis-firehose%7Cgeneral-products%23amazon-kinesis-streams%7Cgeneral-products%23amazon-kinesis-video-streams%7Cgeneral-products%23amazon-msk%7Cgeneral-products%23amazon-opensearch-service%7Cgeneral-products%23amazon-quicksight%7Cgeneral-products%23amazon-redshift&awsf.whats-new-app-integration=*all&awsf.whats-new-arvr=*all&awsf.whats-new-blockchain=*all&awsf.whats-new-business-applications=*all&awsf.whats-new-cloud-financial-management=*all&awsf.whats-new-compute=*all&awsf.whats-new-containers=*all&awsf.whats-new-customer-enablement=*all&awsf.whats-new-customer%20engagement=*all&awsf.whats-new-database=*all&awsf.whats-new-developer-tools=*all&awsf.whats-new-end-user-computing=*all&awsf.whats-new-mobile=*all&awsf.whats-new-gametech=*all&awsf.whats-new-iot=*all&awsf.whats-new-machine-learning=*all&awsf.whats-new-management-governance=*all&awsf.whats-new-media-services=*all&awsf.whats-new-migration-transfer=*all&awsf.whats-new-networking-content-delivery=*all&awsf.whats-new-quantum-tech=*all&awsf.whats-new-robotics=*all&awsf.whats-new-satellite=*all&awsf.whats-new-security-id-compliance=*all&awsf.whats-new-serverless=*all&awsf.whats-new-storage=*all"
}