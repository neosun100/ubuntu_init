# https://www.jianshu.com/p/d90f8a2ecd62



replace_help(){
  echo 🏵🏵🏵🏵🏵🏵🏵🏵🏵🏵🏵🏵🏵🏵🏵🏵🏵🏵🏵🏵;
  echo '🚀 替换（多重）';
  echo '只把每行的第一个AAAA替换为BBBB';
  echo '把每一行的所有AAAA替换为BBBB';
  echo '不管是AAAA，还是CCCC，全部替换为BBBB';
  echo "replace_one replace_all" | awk '{ gsub(/ /,"\t"); print $0 }' | sh `echo ~`/upload/ubuntu_init/class/func/draw_table.sh -1 -green,-red,-green
  echo '第1个参数: 替换对象，默认值 空格 可以用|做多重替换' | lcat;
  echo '第2个参数: 替换结果，默认值 换行符' | lcat;
  echo '第3个参数: 指定某列，默认不指定 即为全行匹配' | lcat;
  echo "";
  echo "";
  echo 🏵🏵🏵🏵🏵🏵🏵🏵🏵🏵🏵🏵🏵🏵🏵🏵🏵🏵🏵🏵;
  echo '🚀 条件替换（多重）';
  echo '只在出现字符串CCCC的前提下，将行中所有AAAA替换为BBBB';
  echo "replace_condition_one replace_condition_all" | awk '{ gsub(/ /,"\t"); print $0 }' | sh `echo ~`/upload/ubuntu_init/class/func/draw_table.sh -1 -green,-red,-green
  echo '第1个参数: 条件对象 出现这个字符串才替换' | lcat;
  echo '第2个参数: 替换对象，默认值 空格 可以用|做多重替换' | lcat;
  echo '第3个参数: 替换结果，默认值 换行符' | lcat;
  echo '第4个参数: 指定某列，默认不指定 即为全行匹配' | lcat;
  echo "";
  echo "";
  echo 🏵🏵🏵🏵🏵🏵🏵🏵🏵🏵🏵🏵🏵🏵🏵🏵🏵🏵🏵🏵;
  echo '🚀 精确替换';
  echo '全字匹配AAAA；即不匹配AAA,以及AAAAA，也就是说完整的四个字符串AAAA';
  echo "replace_precision_one replace_precision_all" | awk '{ gsub(/ /,"\t"); print $0 }' | sh `echo ~`/upload/ubuntu_init/class/func/draw_table.sh -1 -green,-red,-green
  echo '第1个参数: 替换对象，默认值 空格 可以用|做多重替换' | lcat;
  echo '第2个参数: 替换结果，默认值 换行符' | lcat;
  echo '第3个参数: 指定某列，默认不指定 即为全行匹配' | lcat;  
  echo "";
  echo "";
  echo 🏵🏵🏵🏵🏵🏵🏵🏵🏵🏵🏵🏵🏵🏵🏵🏵🏵🏵🏵🏵;
  echo '🚀 规则替换';
  echo '正则还是通配存疑 把所有以A开头，不管后面连续包含几个A的串替换成一个字符B';
  echo '^A*';
  echo "replace_rule_one replace_rule_all" | awk '{ gsub(/ /,"\t"); print $0 }' | sh `echo ~`/upload/ubuntu_init/class/func/draw_table.sh -1 -green,-red,-green
  echo '第1个参数: 替换规则，默认值 ^A* 可以用|做多重替换' | lcat;
  echo '第2个参数: 替换结果，默认值 换行符' | lcat;
  echo '第3个参数: 指定某列，默认不指定 即为全行匹配' | lcat;    
  echo "";
  echo "";  
}



# replace_one  # 替换每行 制定字段的 第一个   用|分割作为多重替换 但是需要单引号包裹
# 默认等价于  tformat 只格式化第一个符合项
replace_one(){
 awk "{ sub(/${1:- }/,\"${2:-\t}\"); print ${3:-\$0} }" 
}

# replace_all # 替换每行所有符合项
# 默认等价于  tformat
replace_all(){
 awk "{ gsub(/${1:- }/,\"${2:-\t}\"); print ${3:-\$0} }" 
}


# 当行满足条件后替换 ，
# 只在出现字符串CCCC的前提下，将行中所有AAAA替换为BBBB
replace_condition_one(){
 awk "/${1:- }/ { sub(/${2:- }/,\"${3:-\t}\"); print ${4:-\$0}; next }
            { print ${4:-\$0} }
    "
} 

replace_condition_all(){
 awk "/${1:- }/ { gsub(/${2:- }/,\"${3:-\t}\"); print ${4:-\$0}; next }
            { print ${4:-\$0} }
    "
} 


# 多替换对象替换
# 不管是AAAA，还是CCCC，全部替换为BBBB
#  replace_multiple_one(){
#   awk "{ sub(/${1:- }/,\"${2:-\t}\"); print ${3:-\$0} }"
#  }

# replace_multiple_all(){
#    awk '{ gsub(/AAAA|aaaa/,"BBBB"); print $0 }' 
#  }



# 精准替换，必须完全
# 全字匹配AAAA；即不匹配AAA,以及AAAAA，也就是说完整的四个字符串AAAA。
replace_precision_one(){
  awk "{ sub(/\<${1:- }\>/,\"${2:-\t}\"); print ${3:-\$0} }"
}

replace_precision_all(){
  awk "{ gsub(/\<${1:- }\>/,\"${2:-\t}\"); print ${3:-\$0} }"
}



# 例子6：规则表达式匹配
# 把所有以A开头，不管后面连续包含几个A的串替换成一个字符B。

# awk '{ gsub(/^A*/,"B"); print $0 }' t.txt

replace_rule_one(){
 awk "{ sub(/${1:-^A*}/,\"${2:-neo}\"); print ${3:-\$0} }" 
}

replace_rule_all(){
 awk "{ gsub(/${1:-^A*}/,\"${2:-neo}\"); print ${3:-\$0} }" 
}




# ppt 翻译软件安装
pptx-translator_setup(){
mkdir -p ~/Code/AWS
cd ~/Code/AWS
git clone https://github.com/aws-samples/pptx-translator
cd pptx-translator
conda create -n pptx-translator  python=3.8
conda activate pptx-translator 
pip install -r requirements.txt 
python pptx-translator.py --help
bse
}

# ppt翻译软件，借用亚马逊服务
pptx-translator(){

PPT_FILE=${1:-/Users/jiasunm/Resource/Ipynb/0【AWS】/test.pptx}
SOURCE_LANGUAGE=${2:-en}
SINK_LANGUAGE=${3:-zh}


echo "参数1:PPT文件:$PPT_FILE"
echo "参数2:SOURCE_LANGUAGE:$SOURCE_LANGUAGE"
echo "参数3:SINK_LANGUAGE:$SINK_LANGUAGE"
# 默认英文翻译成中文，会在原文件同目录下生成翻译后的ppt
~/anaconda3/envs/pptx-translator/bin/python \
~/Code/AWS/pptx-translator/pptx-translator.py \
$SOURCE_LANGUAGE \
$SINK_LANGUAGE \
$PPT_FILE
# python-pptx doesn't support:
#     - Azerbaijani (az)
#     - Persian (fa)
#     - Dari (fa-AF)
#     - Tagalog (tl)
# """
#     'af': MSO_LANGUAGE_ID.AFRIKAANS,
#     'am': MSO_LANGUAGE_ID.AMHARIC,
#     'ar': MSO_LANGUAGE_ID.ARABIC,
#     'bg': MSO_LANGUAGE_ID.BULGARIAN,
#     'bn': MSO_LANGUAGE_ID.BENGALI,
#     'bs': MSO_LANGUAGE_ID.BOSNIAN,
#     'cs': MSO_LANGUAGE_ID.CZECH,
#     'da': MSO_LANGUAGE_ID.DANISH,
#     'de': MSO_LANGUAGE_ID.GERMAN,
#     'el': MSO_LANGUAGE_ID.GREEK,
#     'en': MSO_LANGUAGE_ID.ENGLISH_US,
#     'es': MSO_LANGUAGE_ID.SPANISH,
#     'et': MSO_LANGUAGE_ID.ESTONIAN,
#     'fi': MSO_LANGUAGE_ID.FINNISH,
#     'fr': MSO_LANGUAGE_ID.FRENCH,
#     'fr-CA': MSO_LANGUAGE_ID.FRENCH_CANADIAN,
#     'ha': MSO_LANGUAGE_ID.HAUSA,
#     'he': MSO_LANGUAGE_ID.HEBREW,
#     'hi': MSO_LANGUAGE_ID.HINDI,
#     'hr': MSO_LANGUAGE_ID.CROATIAN,
#     'hu': MSO_LANGUAGE_ID.HUNGARIAN,
#     'id': MSO_LANGUAGE_ID.INDONESIAN,
#     'it': MSO_LANGUAGE_ID.ITALIAN,
#     'ja': MSO_LANGUAGE_ID.JAPANESE,
#     'ka': MSO_LANGUAGE_ID.GEORGIAN,
#     'ko': MSO_LANGUAGE_ID.KOREAN,
#     'lv': MSO_LANGUAGE_ID.LATVIAN,
#     'ms': MSO_LANGUAGE_ID.MALAYSIAN,
#     'nl': MSO_LANGUAGE_ID.DUTCH,
#     'no': MSO_LANGUAGE_ID.NORWEGIAN_BOKMOL,
#     'pl': MSO_LANGUAGE_ID.POLISH,
#     'ps': MSO_LANGUAGE_ID.PASHTO,
#     'pt': MSO_LANGUAGE_ID.BRAZILIAN_PORTUGUESE,
#     'ro': MSO_LANGUAGE_ID.ROMANIAN,
#     'ru': MSO_LANGUAGE_ID.RUSSIAN,
#     'sk': MSO_LANGUAGE_ID.SLOVAK,
#     'sl': MSO_LANGUAGE_ID.SLOVENIAN,
#     'so': MSO_LANGUAGE_ID.SOMALI,
#     'sq': MSO_LANGUAGE_ID.ALBANIAN,
#     'sr': MSO_LANGUAGE_ID.SERBIAN_LATIN,
#     'sv': MSO_LANGUAGE_ID.SWEDISH,
#     'sw': MSO_LANGUAGE_ID.SWAHILI,
#     'ta': MSO_LANGUAGE_ID.TAMIL,
#     'th': MSO_LANGUAGE_ID.THAI,
#     'tr': MSO_LANGUAGE_ID.TURKISH,
#     'uk': MSO_LANGUAGE_ID.UKRAINIAN,
#     'ur': MSO_LANGUAGE_ID.URDU,
#     'vi': MSO_LANGUAGE_ID.VIETNAMESE,
#     'zh': MSO_LANGUAGE_ID.CHINESE_SINGAPORE ,
#     'zh-TW': MSO_LANGUAGE_ID.CHINESE_HONG_KONG_SAR,
# }
}