


CREATE TABLE [catalog_name.][db_name.]table_name
 (
   {  |  |  }[ , ...n]
   [  ]
   [  ][ , ...n]
 )
 [COMMENT table_comment]
 [PARTITIONED BY (partition_column_name1, partition_column_name2, ...)]
 WITH (key1=val1, key2=val2, ...)
 [ LIKE source_table [(  )] ]