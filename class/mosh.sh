# https://www.cnblogs.com/imzye/p/8604613.html

install_mosh() {

    if [ $(uname) = "Darwin" ]; then
        brew install mosh
    elif [ $(uname) = "Linux" ]; then
        sudo add-apt-repository ppa:keithw/mosh
        sudo apt-get update
        sudo apt-get install mosh
    else
        echo "$(uname) 系统未知！"

    fi

}

go_mosh() {
    echo "$(date +'%Y-%m-%d %H:%M:%S') 启动mosh server"
    mosh-server new -c 256 -s -l LANG=en_US.UTF-8 -p 60000
    echo "$(date +'%Y-%m-%d %H:%M:%S') 防火墙开启romote 主机的 60000-61000 udp端口"
    sudo netstat -unlp | grep mosh
    echo "client 连接命令"
    echo "mosh root@149.28.181.207 -p 60001"
}
