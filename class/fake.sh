



fake2mysql(){
echo " 写入一组表 name选项为DB名称"
url=${1:-root:LOOP2themoon@192.168.100.142:3306}
USER=`echo $url | awk '{ gsub(/:/,"\t"); print $0 }' | awk '{ gsub(/@/,"\t"); print $0 }' | field 1`
PASSWORD=`echo $url | awk '{ gsub(/:/,"\t"); print $0 }' | awk '{ gsub(/@/,"\t"); print $0 }' | field 2`
myHOST=`echo $url | awk '{ gsub(/:/,"\t"); print $0 }' | awk '{ gsub(/@/,"\t"); print $0 }' | field 3`
PORT=`echo $url | awk '{ gsub(/:/,"\t"); print $0 }' | awk '{ gsub(/@/,"\t"); print $0 }' | field 4`

NUM=${2:-1} # 写入数量
SEED=${3:-100} # 随机种子
DB_NAME=${4:-test} #  写入的DB名称

fake2db --db mysql \
--host $myHOST \
--port $PORT \
--username $USER \
--password $PASSWORD \
--locale zh_CN \
--seed $SEED --rows $NUM --name $DB_NAME
}


fake2AWS-mysql(){
echo " 写入一组表 name选项为DB名称"
url=${1:-root:AWS2themoon@tesla.cjcyzs87nm4g.us-east-1.rds.amazonaws.com:3306}
USER=`echo $url | awk '{ gsub(/:/,"\t"); print $0 }' | awk '{ gsub(/@/,"\t"); print $0 }' | field 1`
PASSWORD=`echo $url | awk '{ gsub(/:/,"\t"); print $0 }' | awk '{ gsub(/@/,"\t"); print $0 }' | field 2`
myHOST=`echo $url | awk '{ gsub(/:/,"\t"); print $0 }' | awk '{ gsub(/@/,"\t"); print $0 }' | field 3`
PORT=`echo $url | awk '{ gsub(/:/,"\t"); print $0 }' | awk '{ gsub(/@/,"\t"); print $0 }' | field 4`

NUM=${2:-1} # 写入数量
SEED=${3:-`rand_int 0 10000`} # 随机种子
DB_NAME=${4:-test} #  写入的DB名称

fake2db --db mysql \
--host $myHOST \
--port $PORT \
--username $USER \
--password $PASSWORD \
--locale zh_CN \
--seed $SEED --rows $NUM --name $DB_NAME
}



fake2db_custom(){
echo "fake2db --custom field1 field2 ... fieldn " | lcat
echo "可选字段如下"
nat ~/upload/ubuntu_init/class/func/fake2ddb_custom.py
}
