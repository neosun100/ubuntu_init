# pdf 转 txt
# pdf 转 图片


pdfx() {
  # 参数检查：至少需要输入 PDF 文件和页码规格
  if [ "$#" -lt 2 ]; then
    echo "Usage: pdfx <input_pdf> <pages_spec> [output_pdf]"
    echo "  pages_spec examples:"
    echo "    - '1,3,7,8' (指定多个页面)"
    echo "    - '1-5'   (指定一个范围)"
    echo "    - '!4,5'  (排除第4页和第5页，其它页面全部提取)"
    return 1
  fi

  local input_pdf="$1"
  local pages_spec="$2"
  local output_pdf="$3"

  # 检查输入文件是否存在
  if [ ! -f "$input_pdf" ]; then
    echo "Error: Input file '$input_pdf' not found."
    return 1
  fi

  # 检查 qpdf 是否安装
  if ! command -v qpdf >/dev/null 2>&1; then
    echo "Error: qpdf is not installed. Install it via Homebrew: brew install qpdf"
    return 1
  fi

  # 辅助函数：展开类似 "1-3,5" 的页码规格为单个数字列表
  expand_pages() {
    local spec="$1"
    local result=()
    IFS=',' read -ra tokens <<< "$spec"
    for token in "${tokens[@]}"; do
      if [[ "$token" =~ ^([0-9]+)-([0-9]+)$ ]]; then
        local start=${BASH_REMATCH[1]}
        local end=${BASH_REMATCH[2]}
        for (( i = start; i <= end; i++ )); do
          result+=("$i")
        done
      elif [[ "$token" =~ ^[0-9]+$ ]]; then
        result+=("$token")
      else
        echo "Error: Invalid page token '$token' in specification '$spec'."
        return 1
      fi
    done
    echo "${result[@]}"
  }

  # 如果 pages_spec 以 "!" 开头，表示排除模式
  if [[ "$pages_spec" =~ ^\! ]]; then
    # 去掉前导的感叹号，得到排除列表字符串
    local exclude_spec="${pages_spec:1}"
    # 获取 PDF 总页数
    local total_pages
    total_pages=$(qpdf --show-npages "$input_pdf")
    if ! [[ "$total_pages" =~ ^[0-9]+$ ]]; then
      echo "Error: Unable to determine total pages of '$input_pdf'."
      return 1
    fi

    # 获取需要排除的页面列表
    local exclude_list
    exclude_list=($(expand_pages "$exclude_spec"))
    if [ "$?" -ne 0 ]; then
      return 1
    fi

    # 构造包含列表：1 到 total_pages，排除 exclude_list 中的页码
    local inclusion=()
    for (( i=1; i<=total_pages; i++ )); do
      local skip=false
      for ex in "${exclude_list[@]}"; do
        if [ "$i" -eq "$ex" ]; then
          skip=true
          break
        fi
      done
      if [ "$skip" = false ]; then
        inclusion+=("$i")
      fi
    done

    # 将 inclusion 数组转换为以逗号分隔的字符串
    pages_spec=$(IFS=, ; echo "${inclusion[*]}")
  else
    # 对于直接指定的模式，也验证一下规格（调用 expand_pages 仅用于验证格式）
    dummy=$(expand_pages "$pages_spec") || return 1
  fi

  # 自动生成输出文件名（如果未指定）
  if [ -z "$output_pdf" ]; then
    local dir
    dir=$(dirname "$input_pdf")
    local base
    base=$(basename "$input_pdf")
    local name="${base%.*}"
    # 将 pages_spec 中非数字字符替换为下划线，用于文件名中
    local safe_pages
    safe_pages=$(echo "$pages_spec" | sed 's/[^0-9]/_/g')
    output_pdf="${dir}/${name}_pages${safe_pages}.pdf"
  fi

  # 执行 qpdf 命令进行页面提取
  qpdf --empty --pages "$input_pdf" "$pages_spec" -- "$output_pdf"
  if [ "$?" -eq 0 ]; then
    echo "Successfully extracted pages [$pages_spec] from '$input_pdf' to '$output_pdf'."
  else
    echo "Error: Extraction failed."
    return 1
  fi
}