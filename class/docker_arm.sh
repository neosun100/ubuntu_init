# arm docker 合集

# mysql
rpi_docker_mysql() {
    create_dir
    # 创建一个配置文件 mysql 补充
    sudo docker run --name mysql \
        -d \
        --restart always \
        -p 3306:3306 \
        -v $(realpath /etc/localtime):/etc/localtime:ro \
        -v $(realpath /data/mysql):/var/lib/mysql \
        -v $(realpath /home/neo/upload/ubuntu_init/config/mysql.cnf):/etc/mysql/conf.d/mysql.cnf \
        -e MYSQL_ROOT_PASSWORD=LOOP2themoon \
        hypriot/rpi-mysql
}


# mysql 容器 数据库 备份
# 三个参数
# 第1个参数 mysql 容器名称
# 第2个参数 mysql 密码
# 第3个参数 mysql 备份的路经
rpi_docker_mysql_data_backup() {
    sudo docker exec ${1:-mysql} sh -c "exec mysqldump --all-databases -uroot -p${2:-LOOP2themoon}" >${3:-/data/mysql}/all-databases.sql
}
