

# 默认库存文件将是/etc/ansible/hosts。您可以选择指定清单文件
export ANSIBLE_INVENTORY=~/ansible_hosts

alias vima='vim ~/ansible_hosts'

alias vimac='vim ~/.ansible.cfg' # 此为止是默认位置 配置文件

ansible_host_help(){
scat ~/upload/ubuntu_init/class/func/ansible_host.ini | lcat     
}


ansible_demo(){
echo 'ansible local -m shell -a "pwd"  | lcat' | lcat ;
echo 'ansible local -m ping | lcat' | lcat;
}

# https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html#intro-installation-guide 
# 文档地址