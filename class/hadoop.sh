# mac linux 通用
# 终极install
install_hadoop3_lastest() {
    APACHE_URL=${1:-$APACHE_HOST'/apache/hadoop/common/'}
    KEY_GREP=${2:-hadoop-3}
    KEY_TAR_CLASS=${3:-gz}
    CUT_KEY_WORD=${4:-hadoop}

    PAKAGE_VERSION=$(curl $APACHE_URL \
        -H 'Connection: keep-alive' \
        -H 'sec-ch-ua: "Chromium";v="88", "Google Chrome";v="88", ";Not A Brand";v="99"' \
        -H 'sec-ch-ua-mobile: ?0' \
        -H 'Upgrade-Insecure-Requests: 1' \
        -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 11_2_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.192 Safari/537.36' \
        -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9' \
        -H 'Sec-Fetch-Site: same-origin' \
        -H 'Sec-Fetch-Mode: navigate' \
        -H 'Sec-Fetch-User: ?1' \
        -H 'Sec-Fetch-Dest: document' \
        -H 'Referer: https://mirror-hk.koddos.net/apache/' \
        -H 'Accept-Language: zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7' \
        -s \
        --compressed | grep $KEY_GREP | grep '20[2-9][0-9]-[0-1][1-9]-[0-3][0-9]' | keycut '<a href="' '/">' | sort -r | head -n 1)
    # KAFKA_VERSION=`http https://mirror-hk.koddos.net/apache/kafka/ | grep   '20[2-9][0-9]-[0-1][1-9]-[0-3][0-9]' | keycut '<a href="' '/">' | sort -r  | head -n 1`

    PACKAGE_NAME=$(curl $APACHE_URL$PAKAGE_VERSION/ \
        -H 'Connection: keep-alive' \
        -H 'sec-ch-ua: "Chromium";v="88", "Google Chrome";v="88", ";Not A Brand";v="99"' \
        -H 'sec-ch-ua-mobile: ?0' \
        -H 'Upgrade-Insecure-Requests: 1' \
        -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 11_2_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.192 Safari/537.36' \
        -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9' \
        -H 'Sec-Fetch-Site: same-origin' \
        -H 'Sec-Fetch-Mode: navigate' \
        -H 'Sec-Fetch-User: ?1' \
        -H 'Sec-Fetch-Dest: document' \
        -H 'Referer: https://mirrors.tuna.tsinghua.edu.cn/apache/kafka/' \
        -H 'Accept-Language: zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7' \
        -s \
        --compressed | grep $KEY_TAR_CLASS | grep -v '\-src.\|doc\|site\|aarch' | keycut 'href="' "\">$CUT_KEY_WORD" | sort -r | head -n 1)

    mwget ${APACHE_URL}${PAKAGE_VERSION}/${PACKAGE_NAME}

    tar -zxvf $PACKAGE_NAME >/dev/null 2>&1

    [[ -d /usr/local/$CUT_KEY_WORD ]] &&
        (sudo rm -rf /usr/local/$CUT_KEY_WORD && sudo mkdir /usr/local/$CUT_KEY_WORD && sudo chmod -R 777 /usr/local/$CUT_KEY_WORD) ||
        (sudo mkdir /usr/local/$CUT_KEY_WORD && sudo chmod -R 777 /usr/local/$CUT_KEY_WORD)

    PAKAGE_DIR=$(ls ~/upload | grep $CUT_KEY_WORD | grep -v $KEY_TAR_CLASS | sort -r | head -n 1)

    cp -r $PAKAGE_DIR/* /usr/local/$CUT_KEY_WORD
    l /usr/local/$CUT_KEY_WORD

    /usr/local/$CUT_KEY_WORD/bin/hadoop version

    rm -rf $PAKAGE_DIR
    rm -rf $PACKAGE_NAME

}

install_hadoop2_lastest() {
    APACHE_URL=${1:-$APACHE_HOST'/apache/hadoop/common/'}
    KEY_GREP=${2:-hadoop-2}
    KEY_TAR_CLASS=${3:-gz}
    CUT_KEY_WORD=${4:-hadoop}

    PAKAGE_VERSION=$(curl $APACHE_URL \
        -H 'Connection: keep-alive' \
        -H 'sec-ch-ua: "Chromium";v="88", "Google Chrome";v="88", ";Not A Brand";v="99"' \
        -H 'sec-ch-ua-mobile: ?0' \
        -H 'Upgrade-Insecure-Requests: 1' \
        -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 11_2_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.192 Safari/537.36' \
        -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9' \
        -H 'Sec-Fetch-Site: same-origin' \
        -H 'Sec-Fetch-Mode: navigate' \
        -H 'Sec-Fetch-User: ?1' \
        -H 'Sec-Fetch-Dest: document' \
        -H 'Referer: https://mirror-hk.koddos.net/apache/' \
        -H 'Accept-Language: zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7' \
        -s \
        --compressed | grep $KEY_GREP | grep '20[2-9][0-9]-[0-1][1-9]-[0-3][0-9]' | keycut '<a href="' '/">' | sort -r | head -n 1)
    # KAFKA_VERSION=`http https://mirror-hk.koddos.net/apache/kafka/ | grep   '20[2-9][0-9]-[0-1][1-9]-[0-3][0-9]' | keycut '<a href="' '/">' | sort -r  | head -n 1`

    PACKAGE_NAME=$(curl $APACHE_URL$PAKAGE_VERSION/ \
        -H 'Connection: keep-alive' \
        -H 'sec-ch-ua: "Chromium";v="88", "Google Chrome";v="88", ";Not A Brand";v="99"' \
        -H 'sec-ch-ua-mobile: ?0' \
        -H 'Upgrade-Insecure-Requests: 1' \
        -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 11_2_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.192 Safari/537.36' \
        -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9' \
        -H 'Sec-Fetch-Site: same-origin' \
        -H 'Sec-Fetch-Mode: navigate' \
        -H 'Sec-Fetch-User: ?1' \
        -H 'Sec-Fetch-Dest: document' \
        -H 'Referer: https://mirrors.tuna.tsinghua.edu.cn/apache/kafka/' \
        -H 'Accept-Language: zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7' \
        -s \
        --compressed | grep $KEY_TAR_CLASS | grep -v '\-src.\|doc\|site\|aarch' | keycut 'href="' "\">$CUT_KEY_WORD" | sort -r | head -n 1)

    mwget ${APACHE_URL}${PAKAGE_VERSION}/${PACKAGE_NAME}

    tar -zxvf $PACKAGE_NAME >/dev/null 2>&1

    [[ -d /usr/local/$CUT_KEY_WORD ]] &&
        (sudo rm -rf /usr/local/$CUT_KEY_WORD && sudo mkdir /usr/local/$CUT_KEY_WORD && sudo chmod -R 777 /usr/local/$CUT_KEY_WORD) ||
        (sudo mkdir /usr/local/$CUT_KEY_WORD && sudo chmod -R 777 /usr/local/$CUT_KEY_WORD)

    PAKAGE_DIR=$(ls ~/upload | grep $CUT_KEY_WORD | grep -v $KEY_TAR_CLASS | sort -r | head -n 1)

    cp -r $PAKAGE_DIR/* /usr/local/$CUT_KEY_WORD
    l /usr/local/$CUT_KEY_WORD

    /usr/local/$CUT_KEY_WORD/bin/hadoop version

    rm -rf $PAKAGE_DIR
    rm -rf $PACKAGE_NAME

}
