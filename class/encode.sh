

# futu密码编码
# 密码32位MD5加密16进制

encode_futu(){
python ~/upload/ubuntu_init/pyScript/futu_md5.py --msg ${1:-88888}
}

# 最流行的加密解密软件
install_toplip(){
    echo "only linux"
    echo "https://2ton.com.au/toplip/"
    echo "https://ubunlog.com/zh-CN/toplip%E5%8A%A0%E5%AF%86%E8%A7%A3%E5%AF%86%E6%96%87%E4%BB%B6/"
    sudo wget -N -t 0 -c -P /usr/local/bin/ http://cdn.neo.pub/image/toplip
    # wget 最佳实践
    sudo chmod +x /usr/local/bin/toplip
    toplip
}

# curl和wget下载文件到指定文件夹
# curl -o C:\Users\86150\Desktop\wstest\20220302\index2.html --create-dirs http://XXXX
# 1
# 如果没有 -o 后的目录，会自动创建

# wget -P /usr/local/ws/20220302  http://xxxxx
# 1
# 如果没有 -o 后的目录，会自动创建