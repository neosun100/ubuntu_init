# sudo apt-get install python3-pip
# pip3 --version
# sudo pip install scipy
# sudo pip install sklearn
field(){
  awk "{print \$${1:-1}}"
}

echo y | sudo apt-get install python3-pip \
python3-numpy \
python3-scipy \
python3-matplotlib \
python3-ipython \
python3-pandas \
python3-sympy \
python3-nose \
python3-opencv;  

echo y | sudo apt-get install libblas-dev liblapack-dev libatlas-base-dev gfortran python3-sklearn;

echo y | sudo apt-get install  $(sudo apt-cache search python3.7-  | grep python3.7- | field);

echo y | sudo apt-get install  $(sudo apt-cache search python3-flask  | grep python3-flask | field);

echo y | sudo apt-get install  $(sudo apt-cache search python3-django  | grep python3-django | grep -v python3-django-horizon | field);

echo y | sudo apt-get install  $(sudo apt-cache search python3-request  | grep python3-request | grep -v python3-django-horizon | field);

echo y | sudo apt-get install  $(sudo apt-cache search python3-bottle  | grep python3-bottle | field);

echo y | sudo apt-get install  $(sudo apt-cache search python3-certbot  | grep python3-certbot | grep -v nginx| field);

echo y | sudo apt-get install  $(sudo apt-cache search python3-sk  | grep python3-sk | field);

echo y | sudo apt-get install  $(sudo apt-cache search python3-dask  | grep python3-dask | field);

echo y | sudo apt-get install  $(sudo apt-cache search python3-h5  | grep python3-h5 | field);

echo y | sudo apt-get install  $(sudo apt-cache search python3-ju  | grep python3-ju | field);

echo y | sudo apt-get install  $(sudo apt-cache search python3-open  | grep python3-open | field);

sudo apt-get remove apache2  && sudo apt-get clean && sudo apt-get install -f ;
# sudo apt autoremove && sudo apt-get clean && sudo apt-get install -f ;

echo "`date +'%Y-%m-%d %H:%M:%S'` pip 安装第三方模块!";

sudo pip3 install PyHamcrest; # ; # 自动化测试用的，匹配测试？ 极可能是 colorconsole 依赖
# 彩色输出 colorconsole
sudo pip3 install colorconsole;
# 简易彩色输出 colorprint 超赞！
sudo pip3 install git+https://github.com/neosun100/colorprint;

# 快速命令行查询工具 cheat
sudo pip3 install cheat;
cheat grep;

# python版网络测速 speedtest-cli
sudo pip3 install speedtest-cli;
speedtest-cli --list | grep China;
speedtest-cli --server=3633 --share;
# pip install speedtest-cli && speedtest-cli --list | grep China && speedtest-cli --server=3633 --share


# 安装最新版 jupyter，避免mycli 冲突
# pip install -U ipython;
# pip install -U jupyter-console;
# mycli单独一个环境保险

# 安装机器学习
sudo pip3 install deap update_checker tqdm stopit xgboost dask dask-ml scikit-mdr skrebate tpot;

sudo pip3 install  tpot;  

sudo pip3 install  xgboost;  

# xml2json
sudo pip3 install xmljson ;

# python版的多台服务器命令执行工具 pssh
sudo pip3 install pssh;


sudo pip3 install arrow;
sudo pip3 install toolkity;
sudo pip3 install ansible;
# sudo pip3 install sanic;
sudo pip3 install pendulum;

# python系 专业的FTP服务器
sudo pip3 install pyftpdlib;

# httpie
sudo pip3 install httpie;

# mysql客户端 mycli
sudo pip3 install mycli;

#######################
#### fake 合集 ########
######################

# 测试数据生成器
sudo pip3 install faker;

# 数据直接入DB
# sudo pip3 install fake2db;

# 数据生成器 支持DB，df,excel
sudo pip3 install pydbgen;

# 用户agent pass faker里也有
sudo pip3 install fake-useragent;

# python输出代码高亮模块
sudo pip3 install prettyprinter;
echo "from prettyprinter import cpprint,set_default_style";
sleep 10;


# 图像化的ping
sudo pip3 install pinggraph;
# gping host


# 另一个代码高亮备用
#pip install pygmentize; 没有这个包
sudo pip3 install pygments -U;
# alias pcat='pygmentize -g'
#### shell 录制
sudo pip3 install asciinema;



### shell 录制成 svg 文件
sudo pip3 install termtosvg pyte python-xlib svgwrite;

# 摘要抽取 sumy
sudo pip3 install jieba;
sudo pip3 install git+git://github.com/miso-belica/sumy.git;

# 远好于jieba的库
# sudo pip3 install pkuseg;


# 安装 webp-converter
# sudo pip3 install webp-converter;
# webpc -h;
# 2to3-3.7 -w -n ~/anaconda3/lib/python3.7/site-packages/webpc;
# webpc -h;

#视频下载器 lulu 超越 you-get
#一个简单干净的视频/音乐/图像下载器
# pip install lulu

# youtube-dl
sudo pip3 install youtube-dl -U;


# 安装影音中心
echo y | sudo apt-get install kodi;
echo y | sudo apt-get install kodi-pvr-iptvsimple;

# 安装 PyGuetzli
# 谷歌图片极限压缩
# 1  编译安装   Bazel
# 如果失败 试试直接编译安装
# q确实失败了，直接编译成功
echo y | sudo apt-get install build-essential openjdk-8-jdk python zip unzip;
cd ~/upload;
wget https://utan100-1253953493.cos.ap-shanghai.myqcloud.com/rpi/bazel-master.zip;
unzip bazel-master.zip;
echo "compile doc";
echo "https://docs.bazel.build/versions/master/install-compile-source.html";

cd ~/upload/bazel-master;
env EXTRA_BAZEL_ARGS="--host_javabase=@local_jdk//:jdk" bash ./compile.sh;
echo '编译后的输出放在中output/bazel。'
echo '这是一个自包含的Bazel二进制文件，没有嵌入式JDK。'
echo '您可以将其复制到任何地方或就地使用。'
echo '为了方便起见，我们建议将此二进制文件复制到您的目录PATH（例如/usr/local/binLinux）上'

echo "";
echo "";
echo "编译安装 guetzli";
echo "https://github.com/google/guetzli";
cd ~/upload;
# git clone https://github.com/google/guetzli;
wget https://github.com/google/guetzli/archive/master.zip;
unzip master.zip;
cd guetzli-master;
sudo make;
~/upload/guetzli-master/bin/Release/guetzli -h ;
sudo cp `echo ~`/upload/guetzli-master/bin/Release/guetzli /bin/ ;
guetzli  -h ;

echo 'guetzli [--quality Q] [--verbose] original.png output.jpg';
echo 'guetzli [--quality Q] [--verbose] original.jpg output.jpg';


# 死活安装不上 算了
# sudo pip3 install pyguetzli;
# cd ~/upload;
# git clone https://github.com/wanadev/pyguetzli.git;
# cd pyguetzli;
# pip install .;
# cd ~/upload;


# 命令行生成包
sudo pip3 install fire;



# 一个更简洁的操控浏览器（pk selenium） splinter 英 /'splɪntə/ 美 /'splɪntɚ/
cd ~/upload;
echo "下载最新的chrome系列文件";
sleep 10;
for file in $(curl --request GET --url http://ports.ubuntu.com/pool/universe/c/chromium-browser/\?C\=M\;O\=D | grep armhf.deb  | awk -v head='chromium' -v tail='armhf.deb'  '{print substr($0, index($0,head)+length(head),index($0,tail)-index($0,head)-length(head))}' | head -n 4) ; do wget "http://ports.ubuntu.com/pool/universe/c/chromium-browser/chromium${file}armhf.deb" ; done
ls -lah ~/upload;
sudo dpkg -i chromium-browser_*_armhf.deb;
sudo dpkg -i chromium-chromedriver_*_armhf.deb; 
sudo dpkg -i chromium-codecs-ffmpeg_*_armhf.deb; 
sudo dpkg -i chromium-codecs-ffmpeg-extra_*_armhf.deb; 

sudo dpkg -L chromium-chromedriver;

sudo pip3 install splinter;

# $ python3
# ...msg...
# >>> from selenium import webdriver
# >>> browser = webdriver.Chrome(
# ...     executable_path='/usr/lib/chromium-browser/chromedriver')

# >>> browser.get('baidu.com') 
# >>> browser.quit()
# >>> 
# >>> exit()



# 二维码生成 推荐模块 myqr
# 至尊款
# https://github.com/sylnsfar/qrcode/blob/master/README-cn.md
# 安装
sudo pip3 install myqr;


# json 输出更ping
sudo pip3 install pingparsing==0.14;


# nmap
sudo pip3 install python-nmap;

# 二维码识别模块
# pip install zbar;

# debug 模块
sudo pip3 install pysnooper;
# 编译安装 pytorch 1.3
# https://zhuanlan.zhihu.com/p/57938855
sudo apt-get install libopenblas-dev cython3 libatlas-dev \
    m4 libblas-dev cmake;
sudo pip3 install pyyaml ;
cd ~/upload;

echo "下载PyTorch的官方Repository";
git clone https://github.com/pytorch/pytorch.git;
cd pytorch;
echo "查看有哪些分支";
git branch -a;
echo "本文中选择的是v1.3.0，未来的读者可酌情选择更新的分支";

git checkout v1.3.0;
git submodule update --init;

export NO_CUDA=1;
export NO_DISTRIBUTED=1;
export NO_MKLDNN=1;

echo "如果想要在日后再次安装或在另一台树莓派上安装时不必花时间再次编译，那么可以先生成一个wheel文件";
sudo python3 setup.py bdist_wheel;
echo "47% 报错";
git submodule update --init --recursive;

echo "https://github.com/pytorch/pytorch/issues/22564";
c++ -v;
git submodule update --remote third_party/protobuf;


sudo python3 setup.py bdist_wheel;

echo "然后去dist文件夹去安装你生成的wheel文件，其文件名取决于你用了pytorch的哪个分支。";
cd dist;
sudo pip3 install torch-1.3.0a0+de394b6-cp37-cp37m-linux_armv7l.whl ;#  需要修改
echo "下文中还会用到的一些其他工具";
cd ~/upload;
sudo pip3 install torchvision;

sudo pip3 install isort autopep8;
# sudo pip3 install opencv-python; #可以用来读取摄像头捕获的画面，本文中没有涉及
# pip3 install matplotlib;
sudo pip3 install ipython jupyter jupyter-client -U;

sudo pip3 install docker-compose;


sudo pip3 install torchsnooper; # pytorch debug

# torch 基于深度学习的贝叶斯推断
sudo pip3 install pyro-ppl;

# 编译安装 tensorflow 2.0
# https://www.jianshu.com/p/7f031fa2739a
# https://www.cnblogs.com/pedada/p/10229816.html
# http://www.elecfans.com/d/1002404.html
# https://github.com/samjabrahams/tensorflow-on-raspberry-pi/blob/master/GUIDE.md
# https://blog.csdn.net/yuanlulu/article/details/80418892
# 可以直接安装
cd ~/upload;
# mwget https://github.com/lhelontra/tensorflow-on-arm/releases/download/v2.0.0/tensorflow-2.0.0-cp37-none-linux_armv7l.whl;
mwget https://utan100-1253953493.cos.ap-shanghai.myqcloud.com/rpi/tensorflow-2.0.0-cp37-none-linux_armv7l.whl;
sudo pip3 install setuptools -U;
sudo pip3 install -U --ignore-installed wrapt enum34 simplejson netaddr;
sudo pip3 install tensorflow-2.0.0-cp37-none-linux_armv7l.whl;


# 特有压缩模式，服务于 joblib
# mac 需要安装
# brew install lz4
# linux 下 
echo y | sudo apt-get install liblz4-tool;
sudo pip3 install lz4;

# 保存文件
sudo pip3 install joblib;

# 安装 mongo
sudo pip3 install pymongo;


# 贝叶斯推断
sudo pip3 install pymc3;

# 自定义PyMC3模型构建于scikit-learn API之上
sudo pip3 install pymc3_models;

# 安装ta-lib
echo "`date +'%Y-%m-%d %H:%M:%S'` 安装 talib!";
cd ~/upload;
wget https://data-1251390522.cos.ap-shanghai.myqcloud.com/Sys/ta-lib-0.4.0-src.tar.gz;
tar -zxvf ta-lib-0.4.0-src.tar.gz >/dev/null 2>&1;  # 解压
cd ta-lib # 进入目录
./configure --prefix=/usr >/dev/null 2>&1;
sudo make >/dev/null 2>&1;
sudo make install >/dev/null 2>&1;
 sudo pip3 install TA-Lib;
echo "`date +'%Y-%m-%d %H:%M:%S'` 安装 talib 完成!";

# pip install pyopenssl==17.5.0;
# pip install sanic-openapi;
# echo "`date +'%Y-%m-%d %H:%M:%S'` 安装 sanic的微服务基础架构!";
# echo "github : https://github.com/songcser/sanic-ms"
# pip install git+https://github.com/songcser/sanic-ms;
# echo "`date +'%Y-%m-%d %H:%M:%S'` 安装 sanic的微服务基础架构完成";



# 安装kube-shell环境
# echo "`date +'%Y-%m-%d %H:%M:%S'` 创建虚拟环境安装 kube-shell ";
# if [ ! -d ~/anaconda3/envs/kubeshell ];then
# echo "`date +'%Y-%m-%d %H:%M:%S'` 不存在 kubeshell, 安装部署虚拟环境 "
# #conda update  conda;
# conda update -n base -c defaults conda;
# conda create -n kubeshell python=3.6;
# ~/anaconda3/envs/kubeshell/bin/pip install --upgrade pip;
# ~/anaconda3/envs/kubeshell/bin/pip install kube-shell;
# else echo "`date +'%Y-%m-%d %H:%M:%S'` kubeshell 虚拟环境已存在，请使用 kube-shell启动命令";
# fi;



echo "`date +'%Y-%m-%d %H:%M:%S'` pip 安装第三方模块完成!";

