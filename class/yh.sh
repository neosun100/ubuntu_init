# yaml 高亮

install_yh(){
echo "下载地址"
echo "https://github.com/andreazorzetto/yh"


# 标记 下载url
if [ `uname -m` = "armv7l" ] && [ `uname` = "Linux" ]; then
  echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为arm Linux" | lcat
  typeset url="http://file.neo.pub/linux/x86/yh"
#   typeset sysname="_linux_arm"
  # 执行代码块
elif [ `uname -m` = "x86_64" ] && [ `uname` = "Linux" ]; then
  echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为X86 Linux" | lcat
  typeset url="http://file.neo.pub/linux/x86/yh"
#   typeset sysname="_linux_amd64"
  # 执行代码块
elif [ `uname -m` = "x86_64" ] && [ `uname` = "Darwin" ]; then
  echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为X86 Mac" | lcat
  typeset url="http://file.neo.pub/mac/yh"
#   typeset sysname="_darwin_amd64"
  # 执行代码块
else
  echo "`uname` 系统未知！"
  typeset url="http://file.neo.pub/linux/x86/yh"
fi



echo "`date +'%Y-%m-%d %H:%M:%S'` 判断是否已经存在yh"

mkdir -p ~/upload/bin;
if [ -x ~/upload/bin/yh ] ; then 
echo "`date +'%Y-%m-%d %H:%M:%S'` yh 已存在"
else
echo "`date +'%Y-%m-%d %H:%M:%S'` yh 不存在" 

cd ~/upload/bin && wget $url 
sudo chmod -R 777 ~/upload/bin
~/upload/bin/yh help
fi # 判断结束，以fi结尾
}

yh(){
~/upload/bin/yh
}