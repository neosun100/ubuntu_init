

nc_server_tcp(){
PORT=${1:-9876}
nc -l -p $PORT
}


nc_client_tcp(){
PORT=${1:-9876}
NCHOST=${1:-127.0.0.1}
echo "-v 参数指详细输出."
echo "-n 参数告诉netcat 不要使用DNS反向查询IP地址的域名."
nc -n -v $NCHOST $PORT
}


nc_server_udp(){
PORT=${1:-9876}
nc -l -u $PORT
}


nc_client_udp(){
PORT=${1:-9876}
NCHOST=${1:-127.0.0.1}
echo "-v 参数指详细输出."
echo "-n 参数告诉netcat 不要使用DNS反向查询IP地址的域名."
nc -n -v -u $NCHOST $PORT
}