# shell 下显示图片

# sudo apt-get install caca-utils

# img2txt

# cacaview

# sudo apt-get install fbi

# sudo apt-get install fim

# npm install cishower -g
# cishower <file/url>

# 命令行参数说明:

# -w: 显示时图片宽度，默认为80
# -h: 显示是图片高度，高度为0是自动匹配，默认为0
# -g: 是否灰度显示图片
# -bg: 设置背景颜色，如果图片有透明通道是有意义，[white|black|000000~ffffff]
# -m: 设置宽高比，默认为2:1，设置后为1:1
# -v: 是否显示图片信息

# 例子： cishower filepath -w 80 -g -v
# 例子： cishower url -m -bg 0000ff

# aview -h

#  终端看视频
# sudo apt-get install mplayer

# mplayer -vo caca core.png

# https://blog.csdn.net/u010546304/article/details/57927196
# http://blog.chinaunix.net/uid-2282111-id-3212513.html

#  其他
#  https://www.kutu66.com//ubuntu/article_158216

# A terminal image and video viewer.
# https://github.com/hzeller/timg

# 图像 shell 查看 对表 tiv
install_viu() {
  cargo install viu
}

# 可能是最好的
install_tiv() {
  # https://github.com/stefanhaustein/TerminalImageViewer

  # 标记 下载url
  if [ $(uname -m) = "armv7l" ] && [ $(uname) = "Linux" ]; then
    echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为arm Linux" | lcat
    typeset url="http://file.neo.pub/linux/arm/tiv"
    #   typeset sysname="_linux_arm"
    # 执行代码块

  elif [ $(uname -m) = "aarch64" ] && [ $(uname) = "Linux" ]; then
    echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为arm64 Linux" | lcat
    typeset url="http://file.neo.pub/linux/arm64/tiv"

  elif [ $(uname -m) = "x86_64" ] && [ $(uname) = "Linux" ]; then
    echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为X86 Linux" | lcat
    typeset url="http://file.neo.pub/linux/x86/tiv"
    #   typeset sysname="_linux_amd64"
    # 执行代码块
  elif [ $(uname -m) = "x86_64" ] && [ $(uname) = "Darwin" ]; then
    echo "$(date +'%Y-%m-%d %H:%M:%S') 本系统为X86 Mac" | lcat
    typeset url="http://file.neo.pub/mac/tiv"
    #   typeset sysname="_darwin_amd64"
    # 执行代码块
  else
    echo "$(uname) 系统未知！"
    typeset url="http://file.neo.pub/java/jar/tiv.jar"
  fi

  echo "$(date +'%Y-%m-%d %H:%M:%S') 判断是否已经存在tiv"

  if [ -x ~/upload/bin/tiv/tiv ]; then
    echo "$(date +'%Y-%m-%d %H:%M:%S') tiv 已存在"
  else
    echo "$(date +'%Y-%m-%d %H:%M:%S') tiv 不存在"
    mkdir -p ~/upload/bin/tiv
    cd ~/upload/bin/tiv && wget $url && wget "http://file.neo.pub/java/jar/tiv.jar"
    sudo chmod -R 777 ~/upload/bin/tiv
    ~/upload/bin/tiv/tiv
    java -jar ~/upload/bin/tiv/tiv.jar
  fi # 判断结束，以fi结尾

  # cd ~/upload && \
  # git clone https://github.com/neosun100/TerminalImageViewer && \
  # cd ~/upload/TerminalImageViewer/src/main/cpp && \
  # make && \
  # sudo make install;

  # http://file.neo.pub/java/jar/tiv.jar
  # http://file.neo.pub/linux/arm/tiv
  # http://file.neo.pub/linux/x86/tiv
  # http://file.neo.pub/mac/tiv

}

install_ascii_see() {
  brew install TheZoraiz/ascii-image-converter/ascii-image-converter

}

asee() {
  ascii-image-converter $*
}

# c++的
csee() {
  ~/upload/bin/tiv/tiv $*
}

# java see
jsee() {
  java -jar ~/upload/bin/tiv/tiv.jar $*
}

# rust see 可以看gif
rsee() {
  viu $*
}

# nodejs see
nsee() {
  cishower $*
}

# 最好的 图片 gif 视频 cli 工具
# 🚀  🚀  🚀  🚀  🚀  🚀  🚀  🚀  🚀  🚀  🚀
# 🚀  🚀  🚀  🚀  🚀  🚀  🚀  🚀  🚀  🚀  🚀
# 🚀  🚀  🚀  🚀  🚀  🚀  🚀  🚀  🚀  🚀  🚀
# 🚀  🚀  🚀  🚀  🚀  🚀  🚀  🚀  🚀  🚀  🚀
install_see() {
  brew install timg
  # sudo snap install timg
  echo "https://github.com/hzeller/timg/"
}

ksee() {
  # timg -p kitty --grid=2x1 --upscale=i --center --title $*
  timg -p kitty --center --title $*
}

see() {
  timg --center --title $*
}
# 🚀  🚀  🚀  🚀  🚀  🚀  🚀  🚀  🚀  🚀  🚀
# 🚀  🚀  🚀  🚀  🚀  🚀  🚀  🚀  🚀  🚀  🚀
# 🚀  🚀  🚀  🚀  🚀  🚀  🚀  🚀  🚀  🚀  🚀
# 🚀  🚀  🚀  🚀  🚀  🚀  🚀  🚀  🚀  🚀  🚀

#  $ tiv --help
# Terminal Image Viewer

# usage: tiv [options] <image> [<image>...]

#   -0        : No block character adjustment, always use top half block char.
#   -256      : Use 256 color mode.
#   -c <num>  : Number of thumbnail columns in 'dir' mode (3).
#   -d        : Force 'dir' mode. Automatially selected for more than one input.
#   -f        : Force 'full' mode. Automatically selected for one input.
#   -help     : Display this help text.
#   -h <num>  : Set the maximum height to <num> lines.
#   -w <num>  : Set the maximum width to <num> characters.

# 仿真
# sudo apt-get update && sudo apt-get install terminology

# 🐯🐯🐯🐯🐯🐯🐯🐯🐯🐯🐯🐯🐯🐯🐯🐯🐯🐯🐯🐯🐯🐯🐯🐯
# 以上是 see
# 下面是 deal

# https://www.cnblogs.com/blackhat520/p/4187438.html
# https://imagemagick.org/script/command-line-processing.php

install_magick() {
  echo "如果不想下载，有linux x86 二进制文件"
  echo "http://file.neo.pub/linux/x86/magick"
  tsocks brew tap linuxbrew/xorg
  tsocks brew install imagemagick
  tsocks brew install ghostscript
  echo "🚗🚗🚗🚗🚗🚗🚗🚗🚗🚗🚗🚗🚗🚗🚗"
  magick --help
  echo "🚗🚗🚗🚗🚗🚗🚗🚗🚗🚗🚗🚗🚗🚗🚗"
  convert
  echo "🚗🚗🚗🚗🚗🚗🚗🚗🚗🚗🚗🚗🚗🚗🚗"
  identify
}

install_magick_from_source() {
  cd ~/upload
  wget http://file.neo.pub/source/ImageMagick.tar.gz
  tar xvzf ImageMagick.tar.gz
  cd ImageMagick-*
  ./configure --with-modules --enable-shared --with-perl
  make
  sudo make install

  echo "🚗🚗🚗🚗🚗🚗🚗🚗🚗🚗🚗🚗🚗🚗🚗"
  magick --help
  echo "🚗🚗🚗🚗🚗🚗🚗🚗🚗🚗🚗🚗🚗🚗🚗"
  convert
  echo "🚗🚗🚗🚗🚗🚗🚗🚗🚗🚗🚗🚗🚗🚗🚗"
  identify
  cd ~/upload
}

# 图片
# 起点x
# 起点y
# 矩形size
# 保存文件名
cut_pic() {
  picFile=${1:-/Users/neo/upload/WechatIMG1.jpeg}
  pointX=${2:-0}
  pointY=${3:-330}
  size=${4:-720x230}              # 矩形尺寸
  fileName="$(cutfname $picFile)" # WechatIMG1.jpeg
  pathName="$(cutpname $picFile)" # /Users/neo/upload
  newFileName="cut_$fileName"
  result="$pathName/$newFileName" # 保存文件名

  convert $picFile -crop $size+$pointX+$pointY $result
  echo "$result"
}

# 图片
# 起点x
# 起点y
# 终点x
# 终点y
# 保存文件名
cut_pic_point() {
  picFile=${1:-/Users/neo/upload/WechatIMG1.jpeg}
  pointX=${2:-52}
  pointY=${3:-371}
  lastPointX=${4:-668}
  lastPointY=${5:-657}
  size="$((lastPointX - pointX))x$((lastPointY - pointY))" # 矩形尺寸
  fileName="$(cutfname $picFile)"                          # WechatIMG1.jpeg
  pathName="$(cutpname $picFile)"                          # /Users/neo/upload
  newFileName="cut_$fileName"
  result="$pathName/$newFileName" # 保存文件名

  convert $picFile -crop $size+$pointX+$pointY $result
  echo "$result"
}

# big_pic(){
# sudo docker run --rm \ # run the Docker and delete it when finished
#        -it \ # run in interactive mode with a pseudo TTY
#        --gpus all \ # pass all host GPUs into Docker
#        -v /dev/dri:/dev/dri \ # mount host direct rendering infrastructure into Docker for hardware acceleration
#        -v $PWD:/host \ # mount host's current working directory into Docker's /host directory
#        k4yt3x/video2x:4.6.0 \ # use image k4yt3x/video2x version 4.6.0 (pull if necessary)
#        -d waifu2x_ncnn_vulkan \ # use waifu2x-ncnn-vulkan for upscaling
#        -r 2 # upscaling ratio
#        -i sample_input.mp4 \ # specify input file path
#        -o sample_output.mp4 \ # specify output file path
# }

gen_article() {
  TITLE=${1:-黄剑炒币}
  WORDS_NUM=${2:-1000}
  chttp "https://api.edwiv.com/bzsy/gen.php?event=$TITLE&length=$WORDS_NUM&counterType=0&isWritten=0"
}

install_upload_pic() {
  pipx install imgur-uploader
}

upload_pic() {
  echo "需要设定环境变量"
  imgur-uploader ${1:-~/upload/output.png} 2>/dev/null
  # linux没有剪切板会报错，忽略报错
}

pics2pdf() {
  input=${1:-"图片文件夹"}
  output=${2:-"pdf文件"}
  python ~/upload/ubuntu_init/class/func/images2pdf.py $input $output

}

pic_size() {
  identify -format "%wx%h" ${1:-/Users/jiasunm/Downloads/WechatIMG3398.jpg}
}

install_convert() {
  brew install imagemagick
  echo "convert /Users/jiasunm/Documents/Pictures/重要图片/Logo/WechatIMG7.png  -resize 512x512 /Users/jiasunm/Documents/Pictures/重要图片/Logo/output.png "
  convert --help
}
