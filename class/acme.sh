#  自动化更新 ssl

acme_install(){
echo "`date +'%Y-%m-%d %H:%M:%S'` 安装脚本命令" | lcat;
echo "https://freessl.cn/acme"
# curl https://freessl.cn/api/get.acme.sh?token=096ea97c-6743-4306-bcb9-42a2fb8c6607 | sh
# curl https://freessl.cn/api/get.acme.sh?token=98dd8d84-d0c5-460e-aa70-57f034935e8e | sh
curl https://freessl.cn/api/get.acme.sh?token=454b75f1-8da0-418d-a59f-bb9922da6541 | sh
l ~/.acme.sh
sudo ~/.acme.sh/acme.sh -h
echo "";
echo "";
echo "`date +'%Y-%m-%d %H:%M:%S'` 安装脚本命令完成✅" | lcat;
}

acme_login(){
echo "`date +'%Y-%m-%d %H:%M:%S'` 注册 freells 客户端" | lcat;
~/.acme.sh/acme.sh  --register-account \
--accountemail yiqidangqian@foxmail.com \
--server https://acme.freessl.cn/directory
echo "";
echo "";
echo "`date +'%Y-%m-%d %H:%M:%S'` 注册 freells 客户端 完成 ✅" | lcat;
}


acme_edit_url(){
echo "`date +'%Y-%m-%d %H:%M:%S'` 提前设置网址https选择的证书" | lcat;
echo "选择 证书自动化--产品策略--添加策略" | lcat;
echo "*.frp.neo.pub	Let's Encrypt 多域名通配符	Let's Encrypt";
time_wait;
echo "";
echo "";
chrome "https://freessl.cn/acme";
echo "`date +'%Y-%m-%d %H:%M:%S'` 提前设置网址https选择的证书完成✅" | lcat;
echo "";
echo "";
echo "`date +'%Y-%m-%d %H:%M:%S'` 域名管理器同步添加证书域名" | lcat;
time_wait;
chrome "https://dc.console.aliyun.com/next/index#/domain/list/all-domain";
}




# 生成 4096
# # http://s.tool.chinaz.com/https?url=neo.pub
# # 升级 网站 HTTPS 安全评级服务
# # 一、B 升 A
# # /etc/letsencrypt 目录下查看是否有：ssl-dhparams.pem 文件
# # 如果有则直接设置 nginx 配置：ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem;
# cd /etc/ssl/certs
# sudo openssl dhparam -out dhparam.pem 4096
# # 生成该参数十分消耗 CPU 资源，在线上服务执行一定要注意！


acme_gen_dns(){
echo "`date +'%Y-%m-%d %H:%M:%S'` 生成指定域名需要配置dns的信息" | lcat;
~/.acme.sh/acme.sh \
--issue \
-d ${1-:"*.gg.neo.pub"} \
--server https://acme.freessl.cn/directory \
--yes-I-know-dns-manual-mode-enough-go-ahead-please \
--dns
echo "";
echo "";
echo "`date +'%Y-%m-%d %H:%M:%S'` 生成指定域名需要配置dns的信息完成✅" | lcat;
echo "";
echo "";
echo "`date +'%Y-%m-%d %H:%M:%S'` 打开域名控制台添加配置" | lcat;
time_wait;
chrome "https://dc.console.aliyun.com/next/index#/domain/list/all-domain";
echo "https://dc.console.aliyun.com/next/index#/domain/list/all-domain";
}


acme_gen_key(){
echo "`date +'%Y-%m-%d %H:%M:%S'` 生成密钥对" | lcat;
~/.acme.sh/acme.sh  \
--renew  \
-d ${1:-"*.neo.pub"} \
--server https://acme.freessl.cn/directory \
--yes-I-know-dns-manual-mode-enough-go-ahead-please \
--dns
echo "";
echo "";
echo "`date +'%Y-%m-%d %H:%M:%S'` 生成密钥对完成✅" | lcat;
l ~/.acme.sh/;
l ~/.acme.sh/${1:-"*.neo.pub"};
}


# 执行的第一步
# 更改 txt
acme_gen_dns_neo(){
acme_gen_dns "*.neo.pub";
acme_gen_dns "*.frp.neo.pub";

acme_gen_dns "*.gaga.ink";
acme_gen_dns "*.i.gaga.ink";
}

# 执行第二步

acme_gen_key_neo(){
acme_gen_key "*.neo.pub";
acme_gen_key "*.frp.neo.pub";

acme_gen_key "*.gaga.ink";
acme_gen_key "*.i.gaga.ink";
}


acme_mv_key(){
echo "`date +'%Y-%m-%d %H:%M:%S'` 移动密钥对到nginx配置目录" | lcat;
cp -rf ~/.acme.sh/*.neo.pub /data/nginx/ssl  
cp -rf ~/.acme.sh/*.frp.neo.pub /data/nginx/ssl  

cp -rf ~/.acme.sh/*.gaga.ink /data/nginx/ssl 
cp -rf ~/.acme.sh/*.i.gaga.ink /data/nginx/ssl 

# cp -rf /etc/letsencrypt/dhparam.pem /data/nginx/ssl
echo "`date +'%Y-%m-%d %H:%M:%S'` 覆盖移动密钥对到nginx配置目录完成✅" | lcat;
}


acme_help(){
echo "1.生成替换的txt: acme_gen_dns_neo"
echo "2.替换后执行: acme_replace_neo"
}

https_help(){
acme_help;
}


acme_replace_neo(){
acme_gen_key_neo;
acme_mv_key;
echo "重启nginx容器";
sudo docker  restart -t 10 nginx ;
}


https_auto_deploy(){
~/.acme.sh/acme.sh --issue --dns dns_ali -d '*.gaga.ink' -d '*.i.gaga.ink' -d '*.neo.pub' -d '*.frp.neo.pub' 
acme_replace_neo;
}




# # 生成密钥对
# /home/neo/.acme.sh/acme.sh  \
# --renew  \
# -d "*.neo.pub" \
# --server https://acme.freessl.cn/directory \
# --yes-I-know-dns-manual-mode-enough-go-ahead-please \
# --dns


# [Sun Dec  8 22:50:41 CST 2019] Your cert is in  /home/neo/.acme.sh/neo.pub/neo.pub.cer 
# [Sun Dec  8 22:50:41 CST 2019] Your cert key is in  /home/neo/.acme.sh/neo.pub/neo.pub.key 
# [Sun Dec  8 22:50:41 CST 2019] The intermediate CA cert is in  /home/neo/.acme.sh/neo.pub/ca.cer 
# [Sun Dec  8 22:50:41 CST 2019] And the full chain certs is there:  /home/neo/.acme.sh/neo.pub/fullchain.cer 


# cp -rf /home/neo/.acme.sh/frp.neo.pub /data/nginx/                                                                                               
# cp -rf /home/neo/.acme.sh/neo.pub /data/nginx/

# cp -rf /home/neo/.acme.sh/*.neo.pub /data/nginx/ssl  
# cp -rf /home/neo/.acme.sh/*.frp.neo.pub /data/nginx/ssl  
# cp -rf /etc/letsencrypt/dhparam.pem /data/nginx/ssl


# l /data/nginx/conf.d/


# /etc/nginx/ssl/neo.pub/neo.pub.key
# /etc/nginx/ssl/neo.pub/fullchain.cer


