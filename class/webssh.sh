run_webssh(){
echo "`date +'%Y-%m-%d %H:%M:%S'` 检查 python 当前环境" | lcat;
[[ `pe | grep "*" | grep base | wc -l` -eq 1 ]] && echo "`date +'%Y-%m-%d %H:%M:%S'` 当前环境为base" || bse
echo "";
echo "";
echo "`date +'%Y-%m-%d %H:%M:%S'` 检查 webssh 是否安装" | lcat;
[[ `pip list | grep webssh | wc -l` -eq 1 ]] && \
echo "`date +'%Y-%m-%d %H:%M:%S'` 已存在 webssh" || \
(echo "`date +'%Y-%m-%d %H:%M:%S'`  不存在 webssh" && pip install webssh && echo "`date +'%Y-%m-%d %H:%M:%S'`  安装 webssh 完成✅")
echo "";
echo "";
echo "`date +'%Y-%m-%d %H:%M:%S'` 提前运行好frpc并配置好frpc.ini" | lcat;
echo "";
echo "";
echo "`date +'%Y-%m-%d %H:%M:%S'` 运行webssh" | lcat;
wssh --address='0.0.0.0' \
--port=8000 \
--logging=warning \
--origin=${1:-'https://ssh-company.frp.neo.pub'} \
--fbidhttp=False \
--xsrf=False xsrf_cookies=True    # 允许外网访问并关闭跨域限制 
}


run_company_webssh(){
run_webssh https://ssh-company.frp.neo.pub
}


run_home_webssh(){
run_webssh https://ssh-home.frp.neo.pub
}

run_arm_webssh(){
sudo pip3 install webssh;
wssh --address='0.0.0.0' \
--port=8000 \
--logging=warning \
--origin=${1:-'https://ssh-home.frp.neo.pub'} \
--fbidhttp=False \
--xsrf=False xsrf_cookies=True
}

# https://github.com/yudai/gotty 
# https://github.com/yudai/gotty/releases
# 单命令 web 应用