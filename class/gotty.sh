# https://github.com/yudai/gotty 
# https://github.com/yudai/gotty/releases
# 单命令 web 应用



install_gotty(){
cd ~/upload;
# 判断是否存在gotty,存在就不执行了

if [ `l ~/upload/gotty  2>&1  | grep ls | wc -l ` -eq 0 ] ; then 
echo "`date +'%Y-%m-%d %H:%M:%S'` gotty 已存在"
typeset have_gotty=1
else
echo "`date +'%Y-%m-%d %H:%M:%S'` gotty 不存在"
typeset have_gotty=0
fi # 判断结束，以fi结尾



if [ `uname -m` = "armv7l" ] && [ `uname` = "Linux" ] && [ $have_gotty = 0 ]; then
  echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为arm Linux" | lcat
  wget https://neone-club.sh1a.qingstor.com/Sys/gotty_2.0.0-alpha.3_linux_arm.tar.gz
  tar -zxvf gotty_*_arm.tar.gz
#   typeset sysname="_linux_arm"
  # 执行代码块
elif [ `uname -m` = "aarch64" ] && [ `uname` = "Linux" ] && [ $have_gotty = 0 ]; then
  echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为arm64 Linux" | lcat

elif [ `uname -m` = "x86_64" ] && [ `uname` = "Linux" ] && [ $have_gotty = 0 ]; then
  echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为X86 Linux" | lcat
  wget https://neone-club.sh1a.qingstor.com/Sys/gotty_2.0.0-alpha.3_linux_amd64.tar.gz
  tar -zxvf gotty_*_linux_amd64.tar.gz
#   typeset sysname="_linux_amd64"
  # 执行代码块
elif [ `uname -m` = "x86_64" ] && [ `uname` = "Darwin" ]&& [ $have_gotty = 0 ]; then
  echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为X86 Mac" | lcat
  wget https://neone-club.sh1a.qingstor.com/Sys/gotty_2.0.0-alpha.3_darwin_amd64.tar.gz
  tar -zxvf gotty_*_darwin_amd64.tar.gz
#   typeset sysname="_darwin_amd64"
  # 执行代码块
else
  echo "`date +'%Y-%m-%d %H:%M:%S'` `uname` 已安装 gotty 无需下载"
fi


chmod 777 ~/upload/gotty;
echo "`date +'%Y-%m-%d %H:%M:%S'` `uname` gotty 完成 ✅"
}


gotty_gen_config(){
echo "`date +'%Y-%m-%d %H:%M:%S'` 创建 gotty 工作目录"
mkdir -p ~/.gotty;
if [ `l ~/.gotty/config.ini 2>&1 | grep ls | wc -l ` -eq 0 ] ; then 
echo "`date +'%Y-%m-%d %H:%M:%S'` gotty 配置文件已存在"
fcat ~/.gotty/config.ini
else
echo "`date +'%Y-%m-%d %H:%M:%S'` gotty 配置文件 不存在需要生成"
fcat ~/upload/ubuntu_init/config/gotty.ini
time_wait
fcat ~/upload/ubuntu_init/config/gotty.ini > ~/.gotty/config.ini
vim ~/.gotty/config.ini
echo "`date +'%Y-%m-%d %H:%M:%S'` gotty 配置文件 生成完成✅"
fi # 判断结束，以fi结尾
}




deploy_gotty(){
install_gotty;
#gotty_gen_config;
}






ntty(){
echo "net tty"
~/upload/gotty \
--reconnect \
--address=0.0.0.0 \
--port=`date +%s | cut -c7-13` \
--title-format=nsys-`hostname` \
--credential=`hostname`:bilibili \
--ws-origin="https://*" \
--random-url \
--random-url-length=6 \
$*
}


ltty(){
echo "local tty"
~/upload/gotty \
--reconnect \
--address=0.0.0.0 \
--port=8765 \
--title-format=nsys-`hostname` \
--credential=neo:bilibili \
$*
}



nsys_web(){
~/upload/gotty \
--credential=`hostname`:bilibili \
--reconnect \
--title-format=nsys-`hostname` \
--address=0.0.0.0 \
--port=8080 \
--ws-origin="https://*" \
glances
# gotty --reconnect --title-format=nsys-`hostname` --port=8080 --random-url --random-url-length=256  glances
}


# [[ `glances --version | wc -l | pass_black` -eq 1 ]] 



# http://websocketd.com/
# brew install 
# https://blog.just4fun.site/post/%E5%B7%A5%E5%85%B7/websocketd/