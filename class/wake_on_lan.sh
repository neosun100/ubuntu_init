

# 远程开关机

# 安装检查模块

install_ethtool(){
sudo apt-get install ethtool -y
sudo apt-get install wakeonlan -y
echo "--------------------------"
sudo ethtool eno2
echo "--------------------------"
sudo ethtool -s eno2   wol g
echo "--------------------------"
sudo ethtool eno2
}

ARCHON_WAKE_IP="192.168.100.124"
ARCHON_WAKE_MAC="00:25:90:bd:9f:e3" # eno2


wake_pc(){
MAC_ADDRESS=${1:="00:25:90:bd:9f:e3"}
wakeonlan $ARCHON_WAKE_MAC
}

wake_archon(){
wake_pc $ARCHON_WAKE_MAC
}

wake_one(){
wake_pc
}

wake_two(){
wake_pc
}

wake_three(){
wake_pc
}

wake_four(){
wake_pc
}