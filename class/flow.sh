# 代理剩余流量查询
flow() {
  curl -s 'https://e.gae1s.com/clientarea.php?action=productdetails&id=44908' -H 'authority: e.gae1s.com' -H 'cache-control: max-age=0' -H 'upgrade-insecure-requests: 1' -H 'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36' -H 'sec-fetch-dest: document' -H 'accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9' -H 'sec-fetch-site: same-origin' -H 'sec-fetch-mode: navigate' -H 'sec-fetch-user: ?1' -H 'referer: https://e.gae1s.com/clientarea.php' -H 'accept-language: zh-CN,zh;q=0.9,en;q=0.8' -H 'cookie: WHMCSbxdqZID5HE7n=kp0u5ke4bnde2ht9si8ir1q1k5; free-traffic=on' --compressed | grep "剩余流量 " | keycut "剩余流量  : " "</p>"

}
# 代理流量本期有效天数
flownum() {
  curl -s 'https://e.gae1s.com/clientarea.php?action=productdetails&id=44908' -H 'authority: e.gae1s.com' -H 'cache-control: max-age=0' -H 'upgrade-insecure-requests: 1' -H 'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36' -H 'sec-fetch-dest: document' -H 'accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9' -H 'sec-fetch-site: same-origin' -H 'sec-fetch-mode: navigate' -H 'sec-fetch-user: ?1' -H 'referer: https://e.gae1s.com/clientarea.php' -H 'accept-language: zh-CN,zh;q=0.9,en;q=0.8' -H 'cookie: WHMCSbxdqZID5HE7n=kp0u5ke4bnde2ht9si8ir1q1k5; free-traffic=on' --compressed | grep "日后" | keycut "<strong>" "</strong>"
}

flowEnd() {
  curl -s 'https://e.gae1s.com/clientarea.php?action=productdetails&id=44908' -H 'authority: e.gae1s.com' -H 'cache-control: max-age=0' -H 'upgrade-insecure-requests: 1' -H 'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.149 Safari/537.36' -H 'sec-fetch-dest: document' -H 'accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9' -H 'sec-fetch-site: same-origin' -H 'sec-fetch-mode: navigate' -H 'sec-fetch-user: ?1' -H 'referer: https://e.gae1s.com/clientarea.php' -H 'accept-language: zh-CN,zh;q=0.9,en;q=0.8' -H 'cookie: WHMCSbxdqZID5HE7n=kp0u5ke4bnde2ht9si8ir1q1k5; free-traffic=on' --compressed | grep "下次付款" | field 3

}

cflow() {
  echo "剩余流量:\t $(flow)"
  echo "重置天数:\t $(flownum)"
  echo "查询时间:\t $(nows)"
  echo "续费日期:\t $(flowEnd)"
}

alias nflow="cflow | lcat" # 高亮代理流量

jflow() {
  ncat \{"剩余流量":"$(flow | kab)","重置天数":"$(flownum | kab)","查询时间":"$(_nows | kab)","续费日期":"$(flowEnd | kab)"\}
}

# josn 币
njb() {
  http https://api.f2pool.com/monero/48gezqF4MV4MruPmPTs3tQG8w6Asgx6GZbQ72TdowSy9K9quwEPhRh2SK8RuBSoNxCgV8xfvT7yVCSiky3x7nKALFqjNgmw |
    jq '. | {balance: .balance} ' | lcat
}

# table 币
# 对比两个 jq 多了个 [] 列表为后面服务
ntb() {
  echo "$(date +'%Y-%m-%d %H:%M:%S') XMR" | lcat &&
    http https://api.f2pool.com/monero/48gezqF4MV4MruPmPTs3tQG8w6Asgx6GZbQ72TdowSy9K9quwEPhRh2SK8RuBSoNxCgV8xfvT7yVCSiky3x7nKALFqjNgmw |
    jq '[. | {total: .value, balance: .balance, value_last_day: .value_last_day, power: .hashrate} ]' | md-table | lcat &&
    echo "🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹" &&
    echo "" &&
    echo "" &&
    echo "$(date +'%Y-%m-%d %H:%M:%S') AE" | lcat &&
    http https://api.f2pool.com/aeternity/ak_2bzJAkyJZLB3d1KnwvqZ9eTXGUhAEURz8Jv53JcnkTEwtkScBa |
    jq '[. | {total: .value, balance: .balance, value_last_day: .value_last_day, power: .hashrate} ]' | md-table | lcat &&
    echo "🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹"
}

# ludu title lastest
ldt() {
  useProxy=$(best_proxy)
  http -b --proxy=https:https://$useProxy GET https://www.googleapis.com/youtube/v3/search\?key\=AIzaSyCtS0Dm80NL4pYFsJ1P-Q5giTr_JThzBkM\&channelId\=UCm3Ysfy0iXhGbIDTNNwLqbQ\&part\=snippet,id\&order\=date\&maxResults\=3 | jq '.items[] | .snippet.title' | head -n 1 | pass_quotes
}

ldid() {
  useProxy=$(best_proxy)
  http -b --proxy=https:https://$useProxy GET https://www.googleapis.com/youtube/v3/search\?key\=AIzaSyCtS0Dm80NL4pYFsJ1P-Q5giTr_JThzBkM\&channelId\=UCm3Ysfy0iXhGbIDTNNwLqbQ\&part\=snippet,id\&order\=date\&maxResults\=3 | jq '.items[] | .id | .videoId' | head -n 1 | pass_quotes
}

# ludu comment lastest
ldc() {
  useProxy=$(best_proxy)
  cd ~/upload && python ~/upload/ubuntu_init/pyScript/ytb-comment.py \
    --youtubeid $(ldid) \
    --output youtube-comment.json \
    --limit 100
}

nld() {
  ldc
  c
  ldt | lcat
  echo ""
  echo ""
  cd ~/upload
  echo $(scat youtube-comment.json | jq 'select(.author == "ling") | .text ') | pass_quotes | lcat
}

clash_cost() {
  curl -s 'https://youyun666.com/user' \
    -H 'authority: youyun666.com' \
    -H 'accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7' \
    -H 'accept-language: zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7' \
    -H 'cache-control: max-age=0' \
    -H 'cookie: _ga=GA1.2.337869895.1626795363; uid=56817; email=yiqidangqian%40foxmail.com; key=4f5d61d638c15165807013917f193c9564a854380e1de; ip=d7c9e6a1326460241ed3738e64ce4700; expire_in=1681365291' \
    -H 'referer: https://youyun666.com/user' \
    -H 'sec-ch-ua: "Chromium";v="112", "Google Chrome";v="112", "Not:A-Brand";v="99"' \
    -H 'sec-ch-ua-mobile: ?0' \
    -H 'sec-ch-ua-platform: "macOS"' \
    -H 'sec-fetch-dest: document' \
    -H 'sec-fetch-mode: navigate' \
    -H 'sec-fetch-site: same-origin' \
    -H 'sec-fetch-user: ?1' \
    -H 'upgrade-insecure-requests: 1' \
    -H 'user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/112.0.0.0 Safari/537.36' \
    --compressed | grep -A3 '会员时长\|剩余流量\|在线设备\|钱包余额' | sed 's/.*<span class="counter">//;s/<\/span>.*//' | sed -n '1p;4p;6p;9p;11p;14p;16p;19p' | sed 's/ //g'
}

# # home lude comment lastest
# hld(){
# cd ~/upload && python ~/upload/ubuntu_init/pyScript/ytb-comment.py \
# --youtubeid `http -b --proxy=https:https://192.168.2.12:8181 GET https://www.googleapis.com/youtube/v3/search\?key\=AIzaSyCtS0Dm80NL4pYFsJ1P-Q5giTr_JThzBkM\&channelId\=UCm3Ysfy0iXhGbIDTNNwLqbQ\&part\=snippet,id\&order\=date\&maxResults\=3 | jq '.items[] | .id | .videoId' | head -n 1 | pass_quotes` \
# --output youtube-comment.json \
# --limit 100 && \
# c && \
# echo "🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹" && \
# http -b --proxy=https:https://192.168.2.12:8181 GET https://www.googleapis.com/youtube/v3/search\?key\=AIzaSyCtS0Dm80NL4pYFsJ1P-Q5giTr_JThzBkM\&channelId\=UCm3Ysfy0iXhGbIDTNNwLqbQ\&part\=snippet,id\&order\=date\&maxResults\=3 | jq '.items[] | .snippet.title' | head -n 1 | pass_quotes | lcat && \
# echo "🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹" && \
# echo "" && \
# echo `scat youtube-comment.json  | jq 'select(.author == "ling") | .text '` | pass_quotes | lcat
# }

# # home lude comment lastest
# wld(){
# cd ~/upload && python ~/upload/ubuntu_init/pyScript/ytb-comment.py \
# --youtubeid `http -b --proxy=https:https://122.51.108.4:8181 GET https://www.googleapis.com/youtube/v3/search\?key\=AIzaSyCtS0Dm80NL4pYFsJ1P-Q5giTr_JThzBkM\&channelId\=UCm3Ysfy0iXhGbIDTNNwLqbQ\&part\=snippet,id\&order\=date\&maxResults\=3 | jq '.items[] | .id | .videoId' | head -n 1 | pass_quotes` \
# --output youtube-comment.json \
# --limit 100 && \
# c && \
# echo "🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹" && \
# http -b --proxy=https:https://122.51.108.4:8181 GET https://www.googleapis.com/youtube/v3/search\?key\=AIzaSyCtS0Dm80NL4pYFsJ1P-Q5giTr_JThzBkM\&channelId\=UCm3Ysfy0iXhGbIDTNNwLqbQ\&part\=snippet,id\&order\=date\&maxResults\=3 | jq '.items[] | .snippet.title' | head -n 1 | pass_quotes | lcat && \
# echo "🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹" && \
# echo "" && \
# echo `scat youtube-comment.json  | jq 'select(.author == "ling") | .text '` | pass_quotes | lcat
# }

# # local lude
# lld(){
# cd ~/upload && python ~/upload/ubuntu_init/pyScript/ytb-comment.py \
# --youtubeid `http -b --proxy=https:https://127.0.0.1:1087 GET https://www.googleapis.com/youtube/v3/search\?key\=AIzaSyCtS0Dm80NL4pYFsJ1P-Q5giTr_JThzBkM\&channelId\=UCm3Ysfy0iXhGbIDTNNwLqbQ\&part\=snippet,id\&order\=date\&maxResults\=3 | jq '.items[] | .id | .videoId' | head -n 1 | pass_quotes` \
# --output youtube-comment.json \
# --limit 100 && \
# c && \
# echo "🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹" && \
# http -b --proxy=https:https://127.0.0.1:1087 GET https://www.googleapis.com/youtube/v3/search\?key\=AIzaSyCtS0Dm80NL4pYFsJ1P-Q5giTr_JThzBkM\&channelId\=UCm3Ysfy0iXhGbIDTNNwLqbQ\&part\=snippet,id\&order\=date\&maxResults\=3 | jq '.items[] | .snippet.title' | head -n 1 | pass_quotes | lcat && \
# echo "🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹 🐹" && \
# echo "" && \
# echo `scat youtube-comment.json  | jq 'select(.author == "ling") | .text '` | pass_quotes | lcat
# }
