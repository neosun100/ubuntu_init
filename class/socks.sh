# socks代理


# 若为neone
#     就用127.0.0.1 1086
# local socks5 通 
#     就用127.0.0.1 1080
# 判断当前环境 
#     若为home环境 就用 pi 代理
#     若为公司环境  就用 power 代理
#     其他环境     公网 socks5 frp 代理


# 命令行socks代理工具

install_tsocks(){
if [ `uname -m` = "armv7l" ] && [ `uname` = "Linux" ]; then
  echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为arm Linux" | lcat
  ubuntu_install_tsocks
#   typeset sysname="_linux_arm"
  # 执行代码块
elif [ `uname -m` = "aarch64" ] && [ `uname` = "Linux" ]; then
  echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为arm64 Linux" | lcat
  ubuntu_install_tsocks

elif [ `uname -m` = "x86_64" ] && [ `uname` = "Linux" ]; then
  echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为X86 Linux" | lcat
  ubuntu_install_tsocks
#   typeset sysname="_linux_amd64"
  # 执行代码块
elif [ `uname -m` = "x86_64" ] && [ `uname` = "Darwin" ]; then
  echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为X86 Mac" | lcat
  mac_install_tsocks
#   typeset sysname="_darwin_amd64"
  # 执行代码块
else
  echo "`uname` 系统未知！"
fi
}


ubuntu_install_tsocks(){
echo y | sudo apt-get install tsocks;
echo "sudo vim /etc/tsocks.conf" && \
echo "local = 192.0.0.0/255.0.0.0" && \
echo "server = 192.168.2.12" && \
echo "server_type = 5" && \
echo "server_port = 8787";
echo "default_user = neo";
echo "default_pass = bilibili";
echo "";
time_wait;
sudo vim /etc/tsocks.conf;
echo "验证代理 tsocks curl ifconfig.me";
tsocks curl ifconfig.me;
}



mac_install_tsocks(){
brew tap Anakros/homebrew-tsocks;
brew install --HEAD tsocks;
echo "vim /usr/local/etc/tsocks.conf" && \
echo "local = 192.0.0.0/255.0.0.0" && \
echo "server = 127.0.0.1" && \
echo "server_type = 5" && \
echo "server_port = 1086";
echo "server_port = 8786";
echo "default_user = neo";
echo "default_pass = bilibili";
time_wait;
vim /usr/local/etc/tsocks.conf
echo "验证代理 tsocks curl ifconfig.me";
tsocks curl ifconfig.me;
}

# vim tsocks conf
vims(){
if [ `uname -m` = "armv7l" ] && [ `uname` = "Linux" ]; then
  echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为arm Linux" | lcat
  sudo vim /etc/tsocks.conf
#   typeset sysname="_linux_arm"
  # 执行代码块

elif [ `uname -m` = "aarch64" ] && [ `uname` = "Linux" ]; then
  echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为arm64 Linux" | lcat
  sudo vim /etc/tsocks.conf

elif [ `uname -m` = "x86_64" ] && [ `uname` = "Linux" ]; then
  echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为X86 Linux" | lcat
  sudo vim /etc/tsocks.conf
#   typeset sysname="_linux_amd64"
  # 执行代码块
# elif [ `uname -m` = "x86_64" ] && [ `uname` = "Darwin" ]; then
elif [ `uname` = "Darwin" ]; then
  echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为X86 Mac" | lcat
  vim /usr/local/etc/tsocks.conf
#   typeset sysname="_darwin_amd64"
  # 执行代码块
else
  echo "`uname` 系统未知！"
fi
}

##################################################################################################################
##################################################################################################################



# shell全局代理
up_shell_all_proxy(){
if [ `uname -m` = "armv7l" ] && [ `uname` = "Linux" ]; then
  echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为arm Linux" | lcat
  typeset TSOCKS_CONF="/etc/tsocks.conf"
#   typeset sysname="_linux_arm"
  # 执行代码块

elif [ `uname -m` = "aarch64" ] && [ `uname` = "Linux" ]; then
  echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为arm64 Linux" | lcat
  typeset TSOCKS_CONF="/etc/tsocks.conf"

elif [ `uname -m` = "x86_64" ] && [ `uname` = "Linux" ]; then
  echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为X86 Linux" | lcat
  typeset TSOCKS_CONF="/etc/tsocks.conf"
#   typeset sysname="_linux_amd64"
  # 执行代码块
elif [ `uname` = "Darwin" ]; then
  echo "`date +'%Y-%m-%d %H:%M:%S'` 本系统为X86 Mac" | lcat
  typeset TSOCKS_CONF="/usr/local/etc/tsocks.conf"
#   typeset sysname="_darwin_amd64"
  # 执行代码块

else
  echo "`uname` 系统未知！"
fi
# /usr/local/etc/tsocks.conf

# /etc/tsocks.conf

PROXY_IP=`/bin/cat $TSOCKS_CONF | grep server | tail -n 3 | head -n 1 |  field  3`
PROXY_PORT=`cat $TSOCKS_CONF | grep server | tail -n 1 | head -n 1 |  field  3`
echo $PROXY_IP:$PROXY_PORT
export http_proxy="http://$PROXY_IP:7890"
export https_proxy="http://$PROXY_IP:7890"
echo "`date +'%Y-%m-%d %H:%M:%S'` shell下全局代理设置完成" | lcat
}


down_shell_all_proxy(){
unset http_proxy
unset https_proxy
}



# 代理下运行某命令
proxy_run() {
  up_shell_all_proxy &&
    $* &&
    down_shell_all_proxy
}