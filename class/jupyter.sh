# echo 🎊🎊🎊🎊🎊🎊🎊🎊🎊🎊🎊🎊🎊🎊🎊🎊🎊🎊🎊🎊
# echo "jupyter 已经汉化 添加若干插件，至强的存在！🚀"
# echo 🎊🎊🎊🎊🎊🎊🎊🎊🎊🎊🎊🎊🎊🎊🎊🎊🎊🎊🎊🎊
# echo "`date +'%Y-%m-%d %H:%M:%S'` 接下来让我们开始愉快地安装吧！"

# echo "1. 安全性检查"
# echo "`date +'%Y-%m-%d %H:%M:%S'` 检查是否已安装 anaconda？"
# # TODO:
# # FIXME:

# # 若已安装检查是否是最新版本
# # TODO:
# # FIXME:

# echo '# 验证 python 版本'
# python -V;

# 🍀 🍀 🍀 🍀 🍀 🍀 🍀 🍀 🍀 🍀 🍀 🍀
# jupyter env  🍀 🍀 🍀 🍀 🍀 🍀 🍀
# 🍀 🍀 🍀 🍀 🍀 🍀 🍀 🍀 🍀 🍀 🍀 🍀
# export CORE_NUM=$(nproc)
export CORE_NUM=$(nproc 2>/dev/null || sysctl -n hw.logicalcpu 2>/dev/null || echo 1)
export NUMEXPR_MAX_THREADS=$(expr $CORE_NUM / 2)

#   jupyter最佳设置
jt_best() {
    conda config --add channels https://mirrors.tuna.tsinghua.edu.cn/anaconda/pkgs/free/
    conda config --set show_channel_urls yes
    echo "$(date +'%Y-%m-%d %H:%M:%S') jupyter 主题模块并设置最有配色"
    pip install jupyterthemes
    # jt -t grade3 -nf georgiaserif -tf droidsans;
    # 最佳设置
    jt -t monokai -nf georgiaserif -tf droidsans -cellw 90%
    echo "$(date +'%Y-%m-%d %H:%M:%S') jupyter 并行节点模块安装"
    pip install ipyparallel

    echo "$(date +'%Y-%m-%d %H:%M:%S') jupyter 插件安装"
    conda install -c conda-forge jupyter_contrib_nbextensions
    conda install -c conda-forge jupyter_nbextensions_configurator

    echo "$(date +'%Y-%m-%d %H:%M:%S') jupyter 插件初始化"
    jupyter contrib nbextension install --user --skip-running-check

    echo "$(date +'%Y-%m-%d %H:%M:%S') jupyter 插件初始化 完成 开启 jupyter 进行 设置 参考下图"

    if [ $(uname) = "Darwin" ]; then
        echo "https://gitlab.com/neosun100/ubuntu_init/raw/master/pic/jupyter-config-nbextensions.png" &&
            open https://gitlab.com/neosun100/ubuntu_init/raw/master/pic/jupyter-config-nbextensions.png
    elif [ $(uname) = "Linux" ]; then
        echo "https://gitlab.com/neosun100/ubuntu_init/raw/master/pic/jupyter-config-nbextensions.png"
    else
        echo "$(uname) 系统未知！"
    fi

}

# 分系统执行jupyter
wsl_jupyter() {
    jupyter notebook --no-browser \
        --NotebookApp.ip=0.0.0.0 \
        --port 7777 \
        --notebook-dir=/mnt/d/Resource/Ipynb/ \
        --NotebookApp.token=zq \
        --NotebookApp.iopub_data_rate_limit=99999999999999
}

jupyter_gen_config() {
    echo "$(date +'%Y-%m-%d %H:%M:%S') 创建jupyter 工作目录"
    mkdir -p ~/Resource/Ipynb
    if [ $(echo n | jupyter notebook --generate-config 2>&1 | grep Overwrite | wc -l) -eq 1 ]; then
        echo "$(date +'%Y-%m-%d %H:%M:%S') jupyter notebook 配置文件已存在"
        fcat ~/.jupyter/jupyter_notebook_config.py
    else
        echo "$(date +'%Y-%m-%d %H:%M:%S') jupyter notebook 配置文件 不存在需要生成"
        fcat ~/upload/ubuntu_init/config/jupyter.ini
        time_wait
        jupyter notebook --generate-config
        vim ~/.jupyter/jupyter_notebook_config.py
        echo "$(date +'%Y-%m-%d %H:%M:%S') jupyter notebook 配置文件 生成完成✅"
    fi # 判断结束，以fi结尾
}

# Collapsible headings
# 放下/收起notebook的某些内容

# Notify
# Notify功能就能在任务处理完后及时向你发送通知

# Codefolding
# 折叠代码

# tqdm_notebook
# 显示进度条

# %debug
# 调试代码，直接跳到错误的地方

# Table of Contents
# 自动生成目录

# 作者：tongues
# 链接：https://www.jianshu.com/p/807a58e6d8e6
# 來源：简书
# 简书著作权归作者所有，任何形式的转载都请联系作者获得授权并注明出处。

# jupyter 增加和删除kernel

n_jupyter_kernel() {
    jupyter kernelspec list | lcat
}

add_jupyter_kernel() {
    KERNEL=${1:-pycaret}
    echo "$(date +'%Y-%m-%d %H:%M:%S') 查看现有 环境" | lcat
    pe
    echo ""
    echo "$(date +'%Y-%m-%d %H:%M:%S') 查看现有 kernel" | lcat
    jupyter kernelspec list | lcat
    echo ""

    echo "$(date +'%Y-%m-%d %H:%M:%S') 添加 新 kernel $KERNEL-env" | lcat
    spe $KERNEL
    sudo $(which python) -m ipykernel install --name $KERNEL-env
    bse
    bse
    echo ""
    echo "$(date +'%Y-%m-%d %H:%M:%S') 添加 完成✅ enjoy it❕  😄" | lcat
}

rm_jupyter_kernel() {
    KERNEL=${1:-pycaret}
    echo "$(date +'%Y-%m-%d %H:%M:%S') 查看现有 环境" | lcat
    pe
    echo ""
    echo "$(date +'%Y-%m-%d %H:%M:%S') 查看现有 kernel" | lcat
    jupyter kernelspec list | lcat
    echo ""
    echo "$(date +'%Y-%m-%d %H:%M:%S') 删除 kernel $KERNEL-env" | lcat
    sudo /home/neo/anaconda3/bin/jupyter kernelspec remove $KERNEL-env
    echo ""
    echo "$(date +'%Y-%m-%d %H:%M:%S') 删除 完成✅ enjoy it❕  😄" | lcat
}
