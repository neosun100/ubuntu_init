install_mac_kafka() {
  cd ~/upload
  KAFKA_VERSION=${1:-3.5.1}
  SCALA_VERSION=${2:-2.12}
  wget "https://mirror-hk.koddos.net/apache/kafka/${KAFKA_VERSION}/kafka_${SCALA_VERSION}-${KAFKA_VERSION}.tgz"

  tar -zxvf kafka_${SCALA_VERSION}-${KAFKA_VERSION}.tgz >/dev/null 2>&1
  [[ -d /usr/local/kafka ]] &&
    (sudo rm -rf /usr/local/kafka && sudo mkdir /usr/local/kafka && sudo chmod 777 -R /usr/local/kafka) ||
    (sudo mkdir /usr/local/kafka && sudo chmod 777 -R /usr/local/kafka)

  cp -r kafka_${SCALA_VERSION}-${KAFKA_VERSION}/* /usr/local/kafka
  l /usr/local/kafka

}

# mac linux 通用
install_kafka_lastest() {
  # kafka最新版本
  KAFKA_VERSION=$(curl $APACHE_HOST'/apache/kafka/' \
    -H 'Connection: keep-alive' \
    -H 'sec-ch-ua: "Chromium";v="88", "Google Chrome";v="88", ";Not A Brand";v="99"' \
    -H 'sec-ch-ua-mobile: ?0' \
    -H 'Upgrade-Insecure-Requests: 1' \
    -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 11_2_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.192 Safari/537.36' \
    -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9' \
    -H 'Sec-Fetch-Site: none' \
    -H 'Sec-Fetch-Mode: navigate' \
    -H 'Sec-Fetch-User: ?1' \
    -H 'Sec-Fetch-Dest: document' \
    -H 'Accept-Language: zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7' \
    -s \
    --compressed | grep '20[2-9][0-9]-[0-1][1-9]-[0-3][0-9]' | keycut '<a href="' '/">' | sort -r | head -n 1)
  # KAFKA_VERSION=`http https://mirror-hk.koddos.net/apache/kafka/ | grep   '20[2-9][0-9]-[0-1][1-9]-[0-3][0-9]' | keycut '<a href="' '/">' | sort -r  | head -n 1`

  KAFKA_PACKAGE_NAME=$(curl $APACHE_HOST"/apache/kafka/$KAFKA_VERSION/" \
    -H 'Connection: keep-alive' \
    -H 'sec-ch-ua: "Chromium";v="88", "Google Chrome";v="88", ";Not A Brand";v="99"' \
    -H 'sec-ch-ua-mobile: ?0' \
    -H 'Upgrade-Insecure-Requests: 1' \
    -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 11_2_2) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/88.0.4324.192 Safari/537.36' \
    -H 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9' \
    -H 'Sec-Fetch-Site: same-origin' \
    -H 'Sec-Fetch-Mode: navigate' \
    -H 'Sec-Fetch-User: ?1' \
    -H 'Sec-Fetch-Dest: document' \
    -H 'Referer: https://mirrors.tuna.tsinghua.edu.cn/apache/kafka/' \
    -H 'Accept-Language: zh-CN,zh;q=0.9,en-US;q=0.8,en;q=0.7' \
    -s \
    --compressed | grep 'tgz' | grep -v '\-src.\|doc' | keycut 'href="' '">kafka' | sort -r | head -n 1)

  wget $APACHE_HOST"/apache/kafka/${KAFKA_VERSION}/${KAFKA_PACKAGE_NAME}"

  tar -zxvf $KAFKA_PACKAGE_NAME >/dev/null 2>&1

  [[ -d /usr/local/kafka ]] &&
    (sudo rm -rf /usr/local/kafka && sudo mkdir /usr/local/kafka && sudo chmod -R 777 /usr/local/kafka) ||
    (sudo mkdir /usr/local/kafka && sudo chmod -R 777 /usr/local/kafka)

  KAFKA_DIR=$(ls ~/upload | grep kafka | grep -v 'tgz' | sort -r | head -n 1)

  cp -r $KAFKA_DIR/* /usr/local/kafka
  l /usr/local/kafka

  kafka-topics.sh --version

  rm -rf $KAFKA_DIR
  rm -rf $KAFKA_PACKAGE_NAME

}

install_kafkacat() {
  if [ $(uname) = "Darwin" ] && [ -d /usr/local/kafka ]; then
    brew install kafkacat
  elif [ $(uname) = "Linux" ] && [ -d /usr/local/kafka ]; then
    sudo apt-get install kafkacat -y
  else
    echo "$(uname)  系统未知 无法安装kafkacat"
  fi
  kafkacat --help
}

install_kafka_connect-cli() {
  echo "https://github.com/lensesio/kafka-connect-tools/releases/"
  BASE_FILE=$(curl -s https://github.com/lensesio/kafka-connect-tools/releases/ | grep connect-cli | line 1 1 | keycut '<a href="/' '" rel=')
  echo "https://github.com/$BASE_FILE"
  cd ~/upload
  wget "https://github.com/$BASE_FILE" -O connect-cli # -O 覆盖
  sudo chmod 777 ./connect-cli
  sudo mv -f ./connect-cli /usr/local/bin/connect-cli
  l /usr/local/bin | grep connect-cli
}

install_kafka_confluent-hub-client() {
  brew tap confluentinc/homebrew-confluent-hub-client
  brew install --cask confluent-hub-client
  confluent-hub
}

echo_kafka() {
  KAFKA_URL=${1:-192.168.100.142:19092}
  TOPIC=${2:-log}
  MSG=${3:-\{\"timestamp\":$(nowts), \"status\":$(($RANDOM % 3)), \"name\":\"neo\"\}}

  echo $MSG | kafkacat -b $KAFKA_URL -t $TOPIC -P

}

rebuild_topic() {
  ZK_URL=${1:-192.168.100.142:12181}
  TOPIC=${2:-log}
  PARTITIONS=${3:-1}
  REPLICSATION=${4:-1}
  echo "describe $ZK_UR $TOPIC"
  kafka-topics.sh --zookeeper $ZK_URL --topic $TOPIC --describe
  echo "delete $ZK_UR $TOPIC"
  kafka-topics.sh --delete --zookeeper $ZK_URL --topic $TOPIC
  echo "rebuild $ZK_UR $TOPIC"
  kafka-topics.sh --create --zookeeper $ZK_URL --topic $TOPIC --partitions $PARTITIONS --replication-factor $REPLICSATION
  echo "describe $ZK_UR $TOPIC"
  kafka-topics.sh --zookeeper $ZK_URL --topic $TOPIC --describe
}

kafka_group_insight() {
  KAFKA_URL=${1:-192.168.100.142:19092}
  kafka-consumer-groups.sh --bootstrap-server $KAFKA_URL --describe --all-groups --all-topics | Column -t | hcat
  print_x_axis | lcat
}

kafka_group_insight_simple() {
  KAFKA_URL=${1:-192.168.100.142:19092}
  kafka-consumer-groups.sh --bootstrap-server $KAFKA_URL --describe --all-groups --all-topics | fields '$1,$2,$3,$4,$5,$6,$8' | Column -t | hcat
  print_x_axis | lcat
}

## pre kafka 创建topic
# rebuild_topic
# 创建 ck kafka表 系列
# 安装 kafkacat install_kafkacat

## screen 1  # 生产者
# loop echo_kafka

## screen 2 # kafkacat 监控
# kafkacat -C -b  192.168.100.142:19092 -t log -E -o end

## screen3 # 查看ck表记录数
# loop ck_query root:AWS2themoon@192.168.100.142:9900 "select count() as num from tutorial.topic_ch_list;"

## screen4 # 查看group消费状态
# loop kafka_group_insight_simple

install_kafka_tpch() {
  cd ~/upload
  curl -o kafka-tpch https://repo1.maven.org/maven2/de/softwareforge/kafka_tpch_0811/1.0/kafka_tpch_0811-1.0.sh
  sudo chmod 777 kafka-tpch
  sudo mv -f ~/upload/kafka-tpch /usr/local/bin/kafka-tpch
  kafka-tpch help
  echo "-----"
  echo "kafka-tpch load --brokers 192.168.100.142:9092,192.168.100.142:19092,192.168.100.142:29092 --prefix schema. --tpch-type tiny"
}
