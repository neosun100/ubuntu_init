import fire
import itertools
import os

keys = ["python", ""]
key = itertools.cycle(keys)


def format_code_block(source="/Users/neo/upload//html_MD/如何合理配置kcptun进行最高效的加速_OverJerry.md"):
    ''' 根据输入的source 是md文件的路径 添加md代码类型 '''
    path = os.path.dirname(source)   
    with open(source) as f1, open(f"{path}/test.txt", "w") as f2, open(f"{path}/test.txt", "a") as f3:
        f2.write("")
        for line in f1.readlines():
            if "```" in line:
                nstr = next(key)
                f3.write(line.replace("```", f"```{nstr}"))
            else:
                f3.write(line)
    
    
if __name__ == "__main__":
    fire.Fire(format_code_block)   