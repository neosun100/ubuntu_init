import arrow
import fire

def s2ts(time="2020/10/11"):
    return arrow.get(time).to("local").timestamp

if __name__ == "__main__":
    fire.Fire(s2ts)


# SparkSession spark = SparkSession.builder()
#             .config("hive.metastore.client.factory.class", "com.amazonaws.glue.catalog.metastore.AWSGlueDataCatalogHiveClientFactory")
#             .enableHiveSupport()
#             .getOrCreate()