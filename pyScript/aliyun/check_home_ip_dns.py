import json
import argparse
from loguru import logger
import requests
from functools import wraps
from aliyunsdkcore.client import AcsClient
from aliyunsdkalidns.request.v20150109.DescribeDomainRecordsRequest import DescribeDomainRecordsRequest
from aliyunsdkalidns.request.v20150109.UpdateDomainRecordRequest import UpdateDomainRecordRequest
import os

# 从JSON配置文件中读取密钥
def load_config_from_json(file_path):
    with open(file_path, 'r') as file:
        config = json.load(file)
        profile = config["profiles"][0]  # 假设我们使用第一个profile
        return profile["access_key_id"], profile["access_key_secret"]

# 日志配置，确保使用绝对路径
logger.add(os.path.expanduser("~/.aliyun/dns_update_logs.log"),
           rotation="10 MB", retention="30 days")

def exception_handler(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except Exception as e:
            logger.error(f"An error occurred in {func.__name__}: {e}")
            raise
    return wrapper

@exception_handler
def get_public_ip():
    response = requests.get('http://new.nginxs.net/ip.php')  # Example IP service
    return response.text.strip()

@exception_handler
def find_record_id(client, domain_name, rr):
    request = DescribeDomainRecordsRequest()
    request.set_accept_format('json')
    request.set_DomainName(domain_name)
    response = client.do_action_with_exception(request)
    records = json.loads(response)
    for record in records["DomainRecords"]["Record"]:
        if record["RR"] == rr and record["Type"] == "A":
            return record["RecordId"], record["Value"]
    return None, None

@exception_handler
def update_dns_record(client, recordId, RR, Type, Value):
    request = UpdateDomainRecordRequest()
    request.set_RecordId(recordId)
    request.set_RR(RR)
    request.set_Type(Type)
    request.set_Value(Value)
    response = client.do_action_with_exception(request)
    return json.loads(response)

def main():
    parser = argparse.ArgumentParser(description="Update Aliyun DNS record to current public IP.")
    parser.add_argument("config", help="Path to the Aliyun CLI configuration file.")
    args = parser.parse_args()

    # 加载配置
    config_path = os.path.expanduser(args.config)  # Expand the user path
    accessKeyId, accessKeySecret = load_config_from_json(config_path)

    domain_name = "neo.xin"
    rr = "home"
    client = AcsClient(accessKeyId, accessKeySecret, 'cn-hangzhou')

    try:
        public_ip = get_public_ip()
        logger.info(f"当前公网IP: {public_ip}")

        recordId, current_ip = find_record_id(client, domain_name, rr)
        logger.info(f"找到的记录ID: {recordId}, 当前指向的IP: {current_ip}")

        if recordId and current_ip != public_ip:
            logger.info("IP地址不一致，更新中...")
            result = update_dns_record(client, recordId, rr, "A", public_ip)
            logger.info(f"更新结果: {result}")
        else:
            logger.info("IP地址一致，无需更新。")
    except Exception as e:
        logger.error(f"处理过程中发生错误: {e}")
    finally:
        logger.info("DNS更新处理完成。")

if __name__ == '__main__':
    main()

# Crontab -e
# * * * * * /home/neo/anaconda3/bin/python /home/neo/upload/ubuntu_init/pyScript/aliyun/check_home_ip_dns.py /home/neo/.aliyun/config.json
    