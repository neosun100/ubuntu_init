import json
from tencentcloud.common import credential
from tencentcloud.common.profile.client_profile import ClientProfile
from tencentcloud.common.profile.http_profile import HttpProfile
from tencentcloud.common.exception.tencent_cloud_sdk_exception import TencentCloudSDKException
from tencentcloud.lighthouse.v20200324 import lighthouse_client, models
try:
    cred = credential.Credential("", "")
    httpProfile = HttpProfile()
    httpProfile.endpoint = "lighthouse.tencentcloudapi.com"

    clientProfile = ClientProfile()
    clientProfile.httpProfile = httpProfile
    client = lighthouse_client.LighthouseClient(cred, "ap-shanghai", clientProfile)

    req = models.DescribeInstancesRequest()
    params = {
        "InstanceIds": [ "lhins-rfr3mwti" ],
        "Limit": 1,
        "Offset": 0
    }
    req.from_json_string(json.dumps(params))

    resp = client.DescribeInstances(req)
    print(resp.to_json_string())

except TencentCloudSDKException as err:
    print(err)