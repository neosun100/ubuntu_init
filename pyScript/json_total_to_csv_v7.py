import json
import csv
import sys
import argparse
from datetime import datetime
from decimal import Decimal, ROUND_HALF_UP
from functools import wraps
from loguru import logger

# 异常处理装饰器
def exception_handler(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except Exception as e:
            logger.error(f"An error occurred in {func.__name__}: {e}")
            raise
    return wrapper

# 转换 JSON 数据到 CSV
@exception_handler
def json_to_csv(json_data, output_file):
    data = json.loads(json_data)
    header = ['cost'] + [datetime.fromtimestamp(day['timestamp']).strftime('%m-%d') for day in data['daily_costs']] + ['Tot', 'Tot.CN']  # 修改最后两列的名称

    rows = []
    for item in data['daily_costs'][0]['line_items']:
        row = [item['name']]
        total = Decimal('0')
        for day in data['daily_costs']:
            line_item = next(filter(lambda x: x['name'] == item['name'], day['line_items']), None)
            if line_item is None:
                logger.warning(f"Item '{item['name']}' not found in daily costs for timestamp {day['timestamp']}.")
                cost = Decimal('0')
            else:
                cost = Decimal(str(line_item['cost']))
            total += cost
            row.append(float(cost.quantize(Decimal('0.1'), rounding=ROUND_HALF_UP)))  # 只保留小数点后两位有效数字
        total_cny = float(total / 100 * 7)
        row.append(float(total.quantize(Decimal('0.1'), rounding=ROUND_HALF_UP)))  # 只保留小数点后两位有效数字
        row.append(float(Decimal(str(total_cny)).quantize(Decimal('0.1'), rounding=ROUND_HALF_UP)))  # 只保留小数点后两位有效数字
        rows.append(row)

    with open(output_file, 'w', newline='', encoding='utf-8') as csvfile:
        csvwriter = csv.writer(csvfile, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
        csvwriter.writerow(header)
        for row in rows:
            csvwriter.writerow(row)

# 主函数
@exception_handler
def main():
    parser = argparse.ArgumentParser(description="Convert JSON data to CSV format.")
    parser.add_argument("json_file", help="Path to the JSON file.")
    parser.add_argument("csv_file", help="Path to the output CSV file.")
    args = parser.parse_args()

    with open(args.json_file, "r", encoding="utf-8") as f:
        json_data = f.read()

    json_to_csv(json_data, args.csv_file)

if __name__ == "__main__":
    main()
