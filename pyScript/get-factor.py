import requests
import pandas as pd
import pendulum
from functools import reduce
from multiprocessing import Pool  # 多进程
from multiprocessing.dummy import Pool as ThreadPool  # 多线程
import functools


def get_factor(code='US.APLE',
               dim='kline_day',
               startTS=pendulum.now().add(years=-1).timestamp(),  # 默认5年数据
               endTS=pendulum.now().timestamp()):
    url = f"http://192.168.83.197:31778/factor/calculate/{code}/{dim}/{int(startTS)},{int(endTS)}"
    response = requests.request("GET", url)
    data = response.json()['data']
    _json = reduce(lambda d1, d2: dict(d1, **d2), [data[i] for i in data])
    df = pd.DataFrame({i: [_json[i]] for i in _json})
#     过滤字段和设置索引，pass
#     for _passName in [i for i in response.json()['data']['base'] if i != "close" and i != "code" and i != "create_time" and i != "time_key"]:
#         del df[_passName]
#     df.set_index(['create_time'],
#              inplace=True,
#              )
#     df.index = pd.DatetimeIndex(df.index)
    return df


def get_num_factor(num=0,
                   code='US.APLE',
                   dim='kline_day',
                   ):
    thisDay = pendulum.now().add(days=-num)
    endTS = thisDay.timestamp()
    startTS = thisDay.add(years=-1).timestamp()
    return get_factor(code=code,
                      dim=dim,
                      startTS=startTS,
                      endTS=endTS)


# 合并df gen
def merge_df(_dfGen):
    return reduce(lambda df1, df2: df1.append(df2, ignore_index=1), _dfGen)


def get_all_factor_df(barNum=1200,
                      poolNum=6,
                      code="US.APLE",
                      dim='kline_day'):
    use_get_num_factor = functools.partial(get_num_factor, code=code, dim=dim)
    pool = ThreadPool(poolNum)  # 确定线程数

    results = pool.map(use_get_num_factor, range(barNum))  # 并行计算
    pool.close()  # 关闭线程池，等待线程结束
    pool.join()
    return merge_df(results).drop_duplicates(['time_key'])  # 合并去重


# 多线程获取股票feature
# resultDF = get_all_factor_df()
