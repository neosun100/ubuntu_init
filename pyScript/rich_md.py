from rich.console import Console
from rich.markdown import Markdown
import fire
console = Console()

def rmd(md="README.md"):
    with open(md) as readme:
        markdown = Markdown(readme.read())
    console.print(markdown)



if __name__ == "__main__":
    fire.Fire(rmd)