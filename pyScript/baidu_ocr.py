from aip import AipOcr
import fire
import requests
from PIL import Image
from io import BytesIO
import os
""" 你的 APPID AK SK """
APP_ID = '692ae8f793a74609a593a6006282c007'
API_KEY = '02fbd2a8b43a4f14937de97974d6f4ef'
SECRET_KEY = '251c1c1022764dd3af9eabb89e45e58e'
client = AipOcr(APP_ID, API_KEY, SECRET_KEY)


def get_file_content(filePath):
    with open(filePath, 'rb') as fp:
        return fp.read()


def ocr(pic="https://pic2.zhimg.com/v2-6e8fccc8a30e8cf15a90e7a894011579_r.jpg", mod="1"):
    "mod=1 普通模式"
    "mod=2 高精模式"
    "mod=3 生僻字模式"
    "mod=4 网络上背景复杂，特殊字体模式"

    """ 如果有可选参数 """
    options = {}
    options["language_type"] = "CHN_ENG"
    options["detect_direction"] = "true"
    options["detect_language"] = "true"
    options["probability"] = "true"
    if pic[:4] == "http" or pic[:4] == "ftp" or pic[:4] == "sftp":
        # url
        if int(mod) == 1:  # 普通版 url
            print(client.basicGeneralUrl(pic, options))
        elif int(mod) == 2:  # 高精模式 无 url 模式，保存在输出
            response = requests.get(pic)
            image = Image.open(BytesIO(response.content))
            picType = os.path.basename(
                "https://pic2.zhimg.com/v2-6e8fccc8a30e8cf15a90e7a894011579_r.jpg").split(".")[1]
            image.save(f"pic.{picType}")
            print(client.basicAccurate(
                get_file_content(f"pic.{picType}"), options))
        elif int(mod) == 3:  # 生僻字版 url
            print(client.enhancedGeneralUrl(pic, options))
        elif int(mod) == 4:  # 网络上背景复杂，特殊字体模式 url
            print(client.webImageUrl(pic, options))
        else:
            print("error")

    else:
        # 下载图片再处理
        if int(mod) == 1:  # 普通版 local
            print(client.basicGeneral(get_file_content(pic), options))
        elif int(mod) == 2:  # 高精模式  local
            print(client.basicAccurate(get_file_content(pic), options))
        elif int(mod) == 3:  # 生僻字版 local
            print(client.enhancedGeneral(get_file_content(pic), options))
        elif int(mod) == 4:  # 网络上背景复杂，特殊字体模式 local
            print(client.webImage(get_file_content(pic), options))
        else:
            print("error")


if __name__ == "__main__":
    fire.Fire(ocr)
