#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Description:
# @Time    : 2020/11/18 5:27 PM
# @Author  : Neo
# @motto   : It is never too late to learn
# @File    : spark_etl_pipline.py


def read_data():
    """
    获取数据
    :return:
    """
    path = 's3://nyc-tlc/trip data/fhvhv_tripdata_*.csv'
    df_fhvhv = spark.read.format('csv').option('sep',',').option('inferSchema',True).option('header',True).load(path)
    return df_fhvhv
# df_fhvhv.printSchema()
# df_fhvhv.head(1)
# df_fhvhv.count()
# df_fhvhv.show(n=5, truncate=100)
# df_fhvhv.show(20)


def together(data):
    """
    调整数据
    :param data:
    :return:
    """
    df_fhvhv = data
    df_fhvhv = df_fhvhv.withColumn('pickup_datetime',fn.to_timestamp('pickup_datetime','yyyy-MM-dd HH:mm:ss'))
    df_fhvhv = df_fhvhv.withColumn('dropoff_datetime',fn.to_timestamp('dropoff_datetime','yyyy-MM-dd HH:mm:ss'))
    df_fhvhv = df_fhvhv.withColumn('month',fn.month('pickup_datetime'))
    df_fhvhv = df_fhvhv.withColumn('year',fn.year('pickup_datetime'))
    df_fhvhv = df_fhvhv.withColumn('hour',fn.hour('pickup_datetime'))
    df_fhvhv = df_fhvhv.withColumn('quarter',fn.quarter('pickup_datetime'))
    df_fhvhv = df_fhvhv.withColumn('useTime',df_fhvhv['dropoff_datetime'] - df_fhvhv['pickup_datetime'])
    df_fhvhv = df_fhvhv.withColumn("useMinute", ((fn.unix_timestamp(df_fhvhv['dropoff_datetime']) -  fn.unix_timestamp(df_fhvhv['pickup_datetime']))/60).cast("int"))
    df_fhvhv = df_fhvhv.withColumn("date", fn.substring(fn.from_unixtime(fn.unix_timestamp(df_fhvhv['pickup_datetime'], "yyyy-MM-dd")),  0, 10))
    df_fhvhv= df_fhvhv.withColumn('daysOfWeek',fn.dayofweek(df_fhvhv['pickup_datetime']))
    return df_fhvhv


def SR_Flag(data):
    """
    生成SR_Flag
    :param data:
    :return:
    """
    df_fhvhv = data
    df1 = df_fhvhv.groupby('SR_Flag').agg(fn.count('hvfhs_license_num').alias('num'))
    # df1.show()
    # df2=sqlContext.createDataFrame(df1.reset_index(drop=False))
    # df1.show(n=20, truncate=8)
    # df1.write.format('parquet').mode('overwrite').option('path', 's3://aws-task-abc/tlc').saveAsTable('SR_Flag')


def dws_by_hour_order(data):
    """
    生成   dws_by_hour_order
    :param data:
    :return:
    """
    df_fhvhv =  data
    df_dws = df_fhvhv.groupby('hvfhs_license_num', 'year', 'month', 'quarter', 'date', 'hour').agg(
        fn.count('hvfhs_license_num').alias('num'),
        fn.max('useMinute').alias('max_seMinute'),
        fn.min('useMinute').alias('minUseMinute'),
        fn.avg('useMinute').alias('avgUseMinute'),
        )
    # df_dws.count()
    # df_dws.show(n=20, truncate=8)
    # df_dws.write.format('csv').mode('overwrite').option('path', 's3://aws-task-abc/dws_by_hour_order').saveAsTable('dws_by_hour_order')


def dws_by_day_SR_parquet(data):
    """
    生成   dws_by_day_SR_parquet
    :param data:
    :return:
    """
    df_fhvhv =  data
    df_SR = df_fhvhv.groupby('date', 'hvfhs_license_num', 'daysOfWeek', 'SR_Flag').agg(
        fn.count('hvfhs_license_num').alias('num'),
        fn.max('useMinute').alias('maxUseMinute'),
        fn.min('useMinute').alias('minUseMinute'),
        fn.avg('useMinute').alias('avgUseMinute'),
        )

    # df_SR.show(n=20, truncate=8)
    # df_SR.write.format('parquet').mode('overwrite').option('path','s3://aws-task-abc/dws_by_day_SR_parquet').saveAsTable('dws_by_day_SR_parquet')


def dws_by_from(data):
    """
    生成 dws_by_from
    :param data:
    :return:
    """
    df_fhvhv = data
    df_PULocationID = df_fhvhv.groupby('PULocationID').agg(fn.count('hvfhs_license_num').alias('num'),
                                                           fn.max('useMinute').alias('maxUseMinute'),
                                                           fn.min('useMinute').alias('minUseMinute'),
                                                           fn.avg('useMinute').alias('avgUseMinute'),
                                                           )
    # df_PULocationID.show(n=20, truncate=8)
    # df_PULocationID.write.format('csv').mode('overwrite').option('path', 's3://aws-task-abc/dws_by_from').saveAsTable('dws_by_from')



if __name__=="__main__":
    print("starting.....................................................")
    from pyspark.sql import SparkSession
    spark = SparkSession.builder.getOrCreate()
    # from pyspark.sql.types import *
    import pyspark.sql.functions as fn
    data = read_data()
    print("SR_Flag............................................")
    SR_Flag(data)
    print("dws_by_hour_order............................................")
    dws_by_hour_order(data)
    print("dws_by_day_SR_parquet............................................")
    dws_by_day_SR_parquet(data)
    print("dws_by_from............................................")
    dws_by_from(data)




