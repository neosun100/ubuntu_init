# spark-submit --master yarn --deploy-mode cluster s3://neobulabula.py

from pyspark.sql import SparkSession
import pyspark.sql.functions as fn
from pyspark.sql.types import *

spark = SparkSession.builder.getOrCreate() 

path = 's3://nyc-tlc/trip data/fhvhv_tripdata_*.csv'
df_fhvhv = spark.read.format('csv').option('sep',',').option('inferSchema',True).option('header',True).load(path)


df_fhvhv= df_fhvhv.withColumn('pickup_datetime',fn.to_timestamp('pickup_datetime','yyyy-MM-dd HH:mm:ss'))
df_fhvhv = df_fhvhv.withColumn('dropoff_datetime',fn.to_timestamp('dropoff_datetime','yyyy-MM-dd HH:mm:ss'))
df_fhvhv = df_fhvhv.withColumn('month',fn.month('pickup_datetime')) 
df_fhvhv = df_fhvhv.withColumn('year',fn.year('pickup_datetime'))
df_fhvhv = df_fhvhv.withColumn('hour',fn.hour('pickup_datetime'))
df_fhvhv = df_fhvhv.withColumn('quarter',fn.quarter('pickup_datetime'))
df_fhvhv = df_fhvhv.withColumn('useTime',df_fhvhv['dropoff_datetime'] - df_fhvhv['pickup_datetime']) 
df_fhvhv = df_fhvhv.withColumn("useMinute", ((fn.unix_timestamp(df_fhvhv['dropoff_datetime']) -  fn.unix_timestamp(df_fhvhv['pickup_datetime']))/60).cast("int")  )
df_fhvhv = df_fhvhv.withColumn("date", fn.substring(fn.from_unixtime(fn.unix_timestamp(df_fhvhv['pickup_datetime'], "yyyy-MM-dd")),  0, 10)) 
df_fhvhv= df_fhvhv.withColumn('daysOfWeek',fn.dayofweek(df_fhvhv['pickup_datetime']))  


df_dws = df_fhvhv.groupby('hvfhs_license_num','year','month','quarter','date','hour').agg(fn.count('hvfhs_license_num').alias('num'),
                                                                     fn.max('useMinute').alias('maxUseMinute'),
                                                                     fn.min('useMinute').alias('minUseMinute'),
                                                                     fn.avg('useMinute').alias('avgUseMinute'),
                                                                     )

df_dws.write.format('csv')\
    .mode('overwrite')\
    .save('/home/notebook/work/dws_by_hour_order.csv')


df_dws.write.format('csv').mode('overwrite').option('path', 's3://aws-task-abc/dws_by_hour_order').saveAsTable('dws_by_hour_order')  
