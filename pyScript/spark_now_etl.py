from pyspark.sql import SparkSession

from pyspark.sql.types import *
import pyspark.sql.functions as fn


green_path = 's3://nyc-tlc/trip data/green_tripdata_*.csv'


df_green = spark.read.format('csv').option('sep',',').option('inferSchema',True).option('header',True).load(green_path)



df_green.printSchema()
df_green.count()



#  字段类型转化
df_green = df_green.withColumn("VendorID", df_green["VendorID"].cast(IntegerType()))
df_green = df_green.withColumn("Passenger_count", df_green["Passenger_count"].cast(IntegerType()))
df_green = df_green.withColumn("Trip_distance", df_green["Trip_distance"].cast(DoubleType()))
df_green = df_green.withColumn("Fare_amount", df_green["Fare_amount"].cast(DoubleType()))

df_green = df_green.withColumn("Extra", df_green["Extra"].cast(DoubleType()))
df_green = df_green.withColumn("MTA_tax", df_green["MTA_tax"].cast(DoubleType()))
df_green = df_green.withColumn("Tip_amount", df_green["Tip_amount"].cast(DoubleType()))
df_green = df_green.withColumn("Total_amount", df_green["Total_amount"].cast(DoubleType()))



#  字段生成
df_green = df_green.withColumn('lpep_pickup_datetime',fn.to_timestamp('lpep_pickup_datetime','yyyy-MM-dd HH:mm:ss'))
df_green = df_green.withColumn('lpep_dropoff_datetime',fn.to_timestamp('lpep_dropoff_datetime','yyyy-MM-dd HH:mm:ss'))
df_green = df_green.withColumn('month',fn.month('lpep_pickup_datetime')) 
df_green = df_green.withColumn('year',fn.year('lpep_pickup_datetime'))
df_green = df_green.withColumn('hour',fn.hour('lpep_pickup_datetime'))
df_green = df_green.withColumn('quarter',fn.quarter('lpep_pickup_datetime'))
df_green = df_green.withColumn('useTime',(fn.unix_timestamp(df_green['lpep_dropoff_datetime']) - fn.unix_timestamp(df_green['lpep_pickup_datetime'])).cast("int")) 
df_green = df_green.withColumn("useMinute", ((fn.unix_timestamp(df_green['lpep_dropoff_datetime']) -  fn.unix_timestamp(df_green['lpep_pickup_datetime']))/60).cast("int")  )
df_green = df_green.withColumn("date", fn.substring(fn.from_unixtime(fn.unix_timestamp(df_green['lpep_pickup_datetime'], "yyyy-MM-dd")),  0, 10)) 
df_green = df_green.withColumn('daysOfWeek',fn.dayofweek(df_green['lpep_pickup_datetime']))  



df_green = df_green.withColumn("Average_speed", 
                               fn.bround(df_green['Trip_distance'] * 1000 / df_green['useMinute'] / 60 ,scale=4))  # 平均每秒速度




# 数据清洗

df_green_clean = df_green.filter(df_green['Average_speed']>1)


#  join weather 表 博亚


df_green_clean.write.format('parquet').mode('overwrite').option('path', 's3://aws-task-abc/df_green_clean_parquet').saveAsTable('df_green_clean_parquet')  