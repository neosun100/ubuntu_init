
from pyspark import SparkContext
from pyspark.streaming import StreamingContext

 
if __name__ == '__main__':
    sc = SparkContext(appName="inputSourceStreaming", master="local[*]")
    # 第二个参数指统计多长时间的数据
    ssc = StreamingContext(sc, 5)
 
    lines = ssc.socketTextStream("localhost", 9999)
    counts = lines.flatMap(lambda line: line.split(" ")).map(lambda x: (x, 1)).reduceByKey(lambda a,b: a+b)
    counts.pprint()
    # -------------------------------------------
    # Time: 2019-07-31 18: 43:25
    # -------------------------------------------
    # ('hadoop', 1)
    # ('spark', 1)
    ssc.start()
    ssc.awaitTermination()