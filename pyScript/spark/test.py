from pyspark.sql import SparkSession
from pyspark.sql.types import *
import pyspark.sql.functions as fn
from pyspark import SparkContext
from pyspark.sql.functions import when, lit


sc = SparkContext()
spark = SparkSession.builder.getOrCreate()
spark.conf.set("spark.sql.execution.arrow.enabled", "true")
dataframe_mysql = spark.read.format("jdbc").options(url="jdbc:mysql://192.168.100.124:3306/test?useSSL=false",
                                                    driver="com.mysql.jdbc.Driver",
                                                    #                                                     dbtable="company",
                                                    query="select * from company limit 10",
                                                    user="root",
                                                    password="LOOP2themoon").load()
print("QQQQQQQQQQQQQQQQQQQQQ")


'''
:param mode: specifies the behavior of the save operation when data already exists.
* ``append``: Append contents of this :class:`DataFrame` to existing data.
* ``overwrite``: Overwrite existing data.
* ``ignore``: Silently ignore this operation if data already exists.
* ``error`` or ``errorifexists`` (default case): Throw an exception if data already exists.
'''
# dataframe_mysql.write.jdbc(url, table, mode='ignore', properties=auth_mysql)


dataframe_mysql.write.format('jdbc').options(
    url='jdbc:mysql://192.168.100.124:3306/test?useSSL=false',
    driver='com.mysql.jdbc.Driver',
    dbtable='test',
    user='root',
    password='LOOP2themoon').mode('append').save()

print("PPPPPPPPPPPPPPPPPPPPPPPP")


presto_df = spark.read.format("jdbc").options(url="jdbc:presto://192.168.100.124:8080/mysql/test",
                                              driver="com.facebook.presto.jdbc.PrestoDriver",
                                              #   dbtable="company",
                                              query="select * from simple_registration limit 10",
                                              user="root").load()


print(presto_df.printSchema())

print("OOOOOOOOOOOOOOOOOOOOOOOOOOO")

presto_df.write.format('jdbc').options(
    url='jdbc:mysql://192.168.100.124:3306/test?useSSL=false',
    driver='com.mysql.jdbc.Driver',
    dbtable='test_presto',
    user='root',
    password='LOOP2themoon').mode('append').save()


print("5555555555555555555555555555")


# presto_df.write.format('jdbc').options(
#     url="jdbc:presto://192.168.100.124:8080/mysql/test",
#     driver="com.facebook.presto.jdbc.PrestoDriver",
#     dbtable='test_presto',
#     user='root').mode('append').save()


print("WWWWWWWWWWWWWWWWWWWW")
