def read_data_fhvhv():
    """
    获取数据
    :return:
    """
    path = 's3://nyc-tlc/trip data/fhvhv_tripdata_*.csv'

    # path = "/Users/without/Desktop/demo1.csv"

    df_fhvhv = spark.read.format('csv').option('sep',',').option('inferSchema',True).option('header',True).load(path)
    return df_fhvhv
def read_data_wind():
    """
    获取数据
    :return:
    """
    path = 's3://aws-task-abc/weather_data/wind.csv'
    # path = "/Users/without/Desktop/wind.csv"
    df_wind = spark.read.format('csv').option('sep',',').option('inferSchema',True).option('header',True).load(path)
    # df_wind.show(20)
    return df_wind

def together(data):
    """
    调整数据
    :param data:
    :return:
    """
    df_fhvhv = data
    df_fhvhv = df_fhvhv.withColumn('pickup_datetime',fn.to_timestamp('pickup_datetime','yyyy-MM-dd HH:mm:ss'))
    df_fhvhv = df_fhvhv.withColumn('dropoff_datetime',fn.to_timestamp('dropoff_datetime','yyyy-MM-dd HH:mm:ss'))
    df_fhvhv = df_fhvhv.withColumn('month',fn.month('pickup_datetime'))
    df_fhvhv = df_fhvhv.withColumn('year',fn.year('pickup_datetime'))
    df_fhvhv = df_fhvhv.withColumn('hour',fn.hour('pickup_datetime'))
    df_fhvhv = df_fhvhv.withColumn('quarter',fn.quarter('pickup_datetime'))
    df_fhvhv = df_fhvhv.withColumn('useTime',df_fhvhv['dropoff_datetime'] - df_fhvhv['pickup_datetime'])
    df_fhvhv = df_fhvhv.withColumn("useMinute", ((fn.unix_timestamp(df_fhvhv['dropoff_datetime']) -  fn.unix_timestamp(df_fhvhv['pickup_datetime']))/60).cast("int"))
    df_fhvhv = df_fhvhv.withColumn("date", fn.substring(fn.from_unixtime(fn.unix_timestamp(df_fhvhv['pickup_datetime'], "yyyy-MM-dd")),  0, 10))
    df_fhvhv= df_fhvhv.withColumn('daysOfWeek',fn.dayofweek(df_fhvhv['pickup_datetime']))
    return df_fhvhv


def SR_Flag(data):
    """
    生成SR_Flag
    :param data:
    :return:
    """
    df_fhvhv = data
    df1 = df_fhvhv.groupby('SR_Flag').agg(fn.count('hvfhs_license_num').alias('num'))
    df1.show()
    # df2=sqlContext.createDataFrame(df1.reset_index(drop=False))
    # df1.show(n=20, truncate=8)
    df1.write.format('parquet').mode('overwrite').option('path', 's3://aws-task-abc/tlc').saveAsTable('SR_Flag')


def dws_by_hour_order(data):
    """
    生成   dws_by_hour_order
    :param data:
    :return:
    """
    df_fhvhv =  data
    df_dws = df_fhvhv.groupby('hvfhs_license_num', 'year', 'month', 'quarter', 'date', 'hour').agg(
        fn.count('hvfhs_license_num').alias('num'),
        fn.max('useMinute').alias('max_seMinute'),
        fn.min('useMinute').alias('minUseMinute'),
        fn.avg('useMinute').alias('avgUseMinute'),
        )
    # df_dws.show()
    # df_dws.show(n=20, truncate=8)
    df_dws.write.format('parquet').mode('overwrite').option('path', 's3://aws-task-abc/dws_by_hour_order').saveAsTable('dws_by_hour_order')


def dws_by_day_SR_parquet(data):
    """
    生成   dws_by_day_SR_parquet
    :param data:
    :return:
    """
    df_fhvhv =  data
    df_SR = df_fhvhv.groupby('date', 'hvfhs_license_num', 'daysOfWeek', 'SR_Flag').agg(
        fn.count('hvfhs_license_num').alias('num'),
        fn.max('useMinute').alias('maxUseMinute'),
        fn.min('useMinute').alias('minUseMinute'),
        fn.avg('useMinute').alias('avgUseMinute'),
        )

    # df_SR.show()
    df_SR.write.format('parquet').mode('overwrite').option('path','s3://aws-task-abc/dws_by_day_SR_parquet').saveAsTable('dws_by_day_SR_parquet')


def dws_by_from(data):
    """
    生成 dws_by_from
    :param data:
    :return:
    """
    df_fhvhv = data
    df_PULocationID = df_fhvhv.groupby('PULocationID').agg(fn.count('hvfhs_license_num').alias('num'),
                                                           fn.max('useMinute').alias('maxUseMinute'),
                                                           fn.min('useMinute').alias('minUseMinute'),
                                                           fn.avg('useMinute').alias('avgUseMinute'),
                                                           )
    # df_PULocationID.show()
    df_PULocationID.write.format('parquet').mode('overwrite').option('path', 's3://aws-task-abc/dws_by_from').saveAsTable('dws_by_from')



if __name__=="__main__":
    from pyspark.sql import SparkSession
    from pyspark import SparkContext
    sc = SparkContext()
    spark = SparkSession.builder.getOrCreate()
    import pyspark.sql.functions as fn
    df_fhvhv = read_data_fhvhv()
    df_fhvhv = together(df_fhvhv)
    df_fhvhv.createOrReplaceTempView("df_fhvhv")
    df_wind = read_data_wind()
    b_wind = sc.broadcast(df_wind.collect())
    b_wind = spark.createDataFrame(b_wind.value)
    b_wind.createOrReplaceTempView("b_wind")
    data = spark.sql("select b.*,a.wind_level,a.weather,a.max_tem,a.low_tem,a.avg_tem,a.diff_tem from df_fhvhv b  left join b_wind a on b.date=a.date")
    SR_Flag(data)
    dws_by_hour_order(data)
    dws_by_day_SR_parquet(data)
    dws_by_from(data)
