#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Description:
# @Time    : 2020/11/18 9:27 PM
# @Author  : Neo
# @motto   : It is never too late to learn
# @File    : spark_etl_green_with_clean.py


from pyspark.sql import SparkSession
from pyspark.sql.types import *
import pyspark.sql.functions as fn
from pyspark import SparkContext
from pyspark.sql.functions import when, lit


def read_data_green():
    """
    获取数据
    :return:
    """
    green_path = 's3://nyc-tlc/trip data/green_tripdata_*.csv'
    # green_path = "/Users/without/Desktop/green_tripdata_2015-02.csv"
    df_green = spark.read.format('csv').option('sep', ',').option('inferSchema', True).option('header', True).load(
        green_path)
    return df_green


def type_conversion(df_green):
    """
    类型转化
    :param data:
    :return:
    """
    df_green = df_green.withColumn(
        "VendorID", df_green["VendorID"].cast(IntegerType()))
    df_green = df_green.withColumn('lpep_pickup_datetime', fn.to_timestamp(
        'lpep_pickup_datetime', 'yyyy-MM-dd HH:mm:ss'))
    df_green = df_green.withColumn('lpep_dropoff_datetime', fn.to_timestamp(
        'lpep_dropoff_datetime', 'yyyy-MM-dd HH:mm:ss'))
    df_green = df_green.withColumn(
        "Passenger_count", df_green["Passenger_count"].cast(IntegerType()))
    df_green = df_green.withColumn(
        "Trip_distance", df_green["Trip_distance"].cast(DoubleType()))
    df_green = df_green.withColumn(
        "Fare_amount", df_green["Fare_amount"].cast(DoubleType()))

    df_green = df_green.withColumn(
        "Extra", df_green["Extra"].cast(DoubleType()))
    df_green = df_green.withColumn(
        "MTA_tax", df_green["MTA_tax"].cast(DoubleType()))
    df_green = df_green.withColumn(
        "Tip_amount", df_green["Tip_amount"].cast(DoubleType()))
    df_green = df_green.withColumn(
        "Total_amount", df_green["Total_amount"].cast(DoubleType()))
    df_green = df_green.withColumn('Trip_type', df_green['Trip_type '])
    df_green = df_green.drop('Trip_type ')
    return df_green


def new_field(df_green):
    """
    字段生成
    :param data:
    :return:
    """
    df_green = df_green.withColumn('month', fn.month('lpep_pickup_datetime'))
    df_green = df_green.withColumn('year', fn.year('lpep_pickup_datetime'))
    df_green = df_green.withColumn('hour', fn.hour('lpep_pickup_datetime'))
    df_green = df_green.withColumn(
        'quarter', fn.quarter('lpep_pickup_datetime'))
    df_green = df_green.withColumn('useTime', (fn.unix_timestamp(
        df_green['lpep_dropoff_datetime']) - fn.unix_timestamp(df_green['lpep_pickup_datetime'])).cast("int"))
    df_green = df_green.withColumn("useMinute", ((fn.unix_timestamp(
        df_green['lpep_dropoff_datetime']) - fn.unix_timestamp(df_green['lpep_pickup_datetime'])) / 60).cast("int"))
    df_green = df_green.withColumn("date", fn.substring(fn.from_unixtime(
        fn.unix_timestamp(df_green['lpep_pickup_datetime'], "yyyy-MM-dd")), 0, 10))
    df_green = df_green.withColumn(
        'daysOfWeek', fn.dayofweek(df_green['lpep_pickup_datetime']))
    df_green_l = df_green.withColumn("Average_speed", fn.bround(
        df_green['Trip_distance'] * 1000 / df_green['useMinute'] / 60, scale=4))
    # df_green_l.show(50)
    return df_green_l


def join_weather(df_green_clean):
    """
    添加weather信息
    :param df_green_clean:
    :return:
    """
    df_green_clean.createOrReplaceTempView("df_green_clean")
    weather_path = 's3://aws-task-abc/weather_data/wind.csv'
    # weather_path = "/Users/without/Desktop/weather07.csv"
    df_weather = spark.read.format('csv').option('sep', ',').option(
        'inferSchema', True).option('header', True).load(weather_path)
    bd_weather = sc.broadcast(df_weather.collect())
    bd_weather = spark.createDataFrame(bd_weather.value)
    bd_weather.createOrReplaceTempView("bd_weather")
    df_green_clean = spark.sql(
        "select b.*,a.wind_level,a.weather,a.max_tem,a.low_tem,a.avg_tem,a.diff_tem from df_green_clean b  left join bd_weather a on b.date=a.date")
    return df_green_clean


def filter_data(df_green_clean):
    """
    最后的清洗
    :param df_green_clean:
    :return:
    """
    # df_green_clean.replace('null','0')
    # df_green_clean.fillna('0','null')
    # 'Fare_amount', 'Extra', 'MTA_tax', 'Tip_amount', 'Tolls_amount', 'Ehail_fee')
    # df_green_clean.show(50)
    df_green_clean = df_green_clean.withColumn('Ehail_fee', when(
        df_green_clean.Ehail_fee.isNull(), lit('0')).otherwise(df_green_clean.Ehail_fee))
    df_green_clean = df_green_clean.withColumn('Fare_amount', when(
        df_green_clean.Fare_amount.isNull(), lit('0')).otherwise(df_green_clean.Fare_amount))
    df_green_clean = df_green_clean.withColumn('Extra', when(
        df_green_clean.Extra.isNull(), lit('0')).otherwise(df_green_clean.Extra))
    df_green_clean = df_green_clean.withColumn('MTA_tax', when(
        df_green_clean.MTA_tax.isNull(), lit('0')).otherwise(df_green_clean.MTA_tax))
    df_green_clean = df_green_clean.withColumn('Tip_amount', when(
        df_green_clean.Tip_amount.isNull(), lit('0')).otherwise(df_green_clean.Tip_amount))
    df_green_clean = df_green_clean.withColumn('Tolls_amount', when(
        df_green_clean.Tolls_amount.isNull(), lit('0')).otherwise(df_green_clean.Tolls_amount))
    # df_green_clean = df_green_clean.withColumn('Ehail_fee', when(df_green_clean.Ehail_fee.isNull(),lit('0')).otherwise(df_green_clean.Ehail_fee))
    # df_green_clean.count()
    df_green_clean = df_green_clean.withColumn(
        'RateCodeID', df_green_clean['RateCodeID'].cast("int"))
    df_green_clean = df_green_clean.withColumn(
        'Pickup_longitude', df_green_clean['Pickup_longitude'].cast(DoubleType()))
    df_green_clean = df_green_clean.withColumn(
        'Pickup_latitude', df_green_clean['Pickup_latitude'].cast(DoubleType()))
    df_green_clean = df_green_clean.withColumn(
        'Dropoff_longitude', df_green_clean['Dropoff_longitude'].cast(DoubleType()))
    df_green_clean = df_green_clean.withColumn(
        'Dropoff_latitude', df_green_clean['Dropoff_latitude'].cast(DoubleType()))
    df_green_clean = df_green_clean.withColumn(
        'Tolls_amount', df_green_clean['Tolls_amount'].cast(DoubleType()))
    df_green_clean = df_green_clean.withColumn(
        'Ehail_fee', df_green_clean['Ehail_fee'].cast(DoubleType()))
    df_green_clean = df_green_clean.withColumn(
        'Total_amount', df_green_clean['Total_amount'].cast(DoubleType()))
    df_green_clean = df_green_clean.withColumn(
        'Payment_type', df_green_clean['Payment_type'].cast("int"))
    # df_green_clean.show(50)
    # df_green_clean = df_green_clean.na.replace(['null'],0,'Fare_amount', 'Extra', 'MTA_tax', 'Tip_amount', 'Tolls_amount', 'Ehail_fee')
    df_green_clean = df_green_clean.filter((df_green_clean['Average_speed'] > 1) & (df_green_clean['Average_speed'] < 33)
                                           & (df_green_clean['RateCodeID'] >= 1) & (df_green_clean['RateCodeID'] <= 6
                                                                                    ) & (df_green_clean['Passenger_count'] >= 1) & (df_green_clean['Passenger_count'] <= 7
                                                                                                                                    ) & (df_green_clean['Trip_distance'] > 1) & (df_green_clean['Trip_distance'] < 500
                                                                                                                                                                                 ) & (df_green_clean['Fare_amount'] >= 0) & (df_green_clean['Fare_amount'] < 1000
                                                                                                                                                                                                                             ) & (df_green_clean['Extra'] >= 0) & (df_green_clean['Extra'] < 500
                                                                                                                                                                                                                                                                   ) & (df_green_clean['MTA_tax'] >= 0) & (df_green_clean['MTA_tax'] < 500
                                                                                                                                                                                                                                                                                                           ) & (df_green_clean['Tip_amount'] >= 0) & (df_green_clean['Tip_amount'] < 100
                                                                                                                                                                                                                                                                                                                                                      ) & (df_green_clean['Tolls_amount'] >= 0) & (df_green_clean['Tolls_amount'] < 500
                                                                                                                                                                                                                                                                                                                                                                                                   ) & (df_green_clean['Ehail_fee'] >= 0) & (df_green_clean['Ehail_fee'] < 100
                                                                                                                                                                                                                                                                                                                                                                                                                                             ) & (df_green_clean['Total_amount'] > 0) & (df_green_clean['Total_amount'] < 1500
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         ) & (df_green_clean['Payment_type'] >= 0) & (df_green_clean['Payment_type'] <= 6))
    return df_green_clean


if __name__ == "__main__":
    sc = SparkContext()
    spark = SparkSession.builder.getOrCreate()
    df_green = read_data_green()
    df_green = type_conversion(df_green)
    df_green = new_field(df_green)
    df_green_clean = filter_data(df_green)
    df_green_clean = join_weather(df_green_clean)
    # df_green_clean.show(20)
    df_green_clean.write.format('parquet').mode('overwrite').option(
        'path', 's3://aws-task-abc/df_green_clean_3_parquet').saveAsTable('df_green_clean_3_parquet')
