#!/usr/bin/env python
# -*- coding: utf-8 -*-
# @Description:
# @Time    : 2020/11/22 9:27 PM
# @Author  : Neo
# @motto   : It is never too late to learn
# @File    : spark_etl_all.py


from pyspark.sql import SparkSession
from pyspark.sql.types import *
import pyspark.sql.functions as fn
from pyspark import SparkContext
from pyspark.sql.functions import when,lit


def read_data_green_19():
    """
    获取数据
    :return:
    """
    green_path = 's3://nyc-tlc/trip data/green_tripdata_20[1,2][9,0]*.csv'
    df_green = spark.read.format('csv').option('sep', ',').option('inferSchema', True).option('header', True).load(
        green_path)
    return df_green
def read_data_green_13():
    """
    获取数据
    :return:
    """
    green_path = 's3://nyc-tlc/trip data/green_tripdata_201[3,4]*.csv'
    df_green = spark.read.format('csv').option('sep', ',').option('inferSchema', True).option('header', True).load(
        green_path)
    return df_green
def read_data_green_15():
    """
    获取数据
    :return:
    """
    green_path = 's3://nyc-tlc/trip data/green_tripdata_2015*.csv'
    df_green = spark.read.format('csv').option('sep', ',').option('inferSchema', True).option('header', True).load(
        green_path)
    return df_green
def read_data_green_16():
    """
    获取数据
    :return:
    """
    green_path = 's3://nyc-tlc/trip data/green_tripdata_2016-[01,06]*.csv'
    df_green = spark.read.format('csv').option('sep', ',').option('inferSchema', True).option('header', True).load(
        green_path)
    return df_green
def read_data_green_18():
    """
    获取数据
    :return:
    """
    green_path = 's3://nyc-tlc/trip data/green_tripdata_201[7,8]*.csv'
    df_green = spark.read.format('csv').option('sep', ',').option('inferSchema', True).option('header', True).load(
        green_path)
    return df_green
def read_data_green_16_07():
    """
    获取数据
    :return:
    """
    green_path = 's3://nyc-tlc/trip data/green_tripdata_2016-[07,12]*.csv'
    df_green = spark.read.format('csv').option('sep', ',').option('inferSchema', True).option('header', True).load(
        green_path)
    return df_green


def type_conversion_19(df_green):
    """
    类型转化
    :param data:
    :return:
    """
    df_green = df_green.withColumn("VendorID", df_green["VendorID"].cast(IntegerType()))
    df_green = df_green.withColumn("passenger_count", df_green["passenger_count"].cast(IntegerType()))
    df_green = df_green.withColumn("trip_distance", df_green["trip_distance"].cast(DoubleType()))
    df_green = df_green.withColumn("fare_amount", df_green["fare_amount"].cast(DoubleType()))
    df_green = df_green.withColumn("extra", df_green["extra"].cast(DoubleType()))
    df_green = df_green.withColumn("mta_tax", df_green["mta_tax"].cast(DoubleType()))
    df_green = df_green.withColumn("tip_amount", df_green["tip_amount"].cast(DoubleType()))
    df_green = df_green.withColumn("total_amount", df_green["total_amount"].cast(DoubleType()))
    df_green = df_green.withColumn("Pickup_longitude", lit("0"))
    df_green = df_green.withColumn("Pickup_latitude", lit("0"))
    df_green = df_green.withColumn("Dropoff_longitude", lit("0"))
    df_green = df_green.withColumn("Dropoff_latitude", lit("0"))
    return df_green
def type_conversion_13(df_green):
    """
    类型转化
    :param data:
    :return:
    """
    df_green = df_green.withColumn("VendorID", df_green["VendorID"].cast(IntegerType()))
    df_green = df_green.withColumn("passenger_count", df_green["Passenger_count"].cast(IntegerType()))
    df_green = df_green.withColumn("trip_distance", df_green["Trip_distance"].cast(DoubleType()))
    df_green = df_green.withColumn("fare_amount", df_green["Fare_amount"].cast(DoubleType()))
    df_green = df_green.withColumn("extra", df_green["Extra"].cast(DoubleType()))
    df_green = df_green.withColumn("mta_tax", df_green["MTA_tax"].cast(DoubleType()))
    df_green = df_green.withColumn("tip_amount", df_green["Tip_amount"].cast(DoubleType()))
    df_green = df_green.withColumn("total_amount", df_green["Tolls_amount"].cast(DoubleType()))
    df_green = df_green.withColumn("trip_type", df_green["Trip_type "].cast(DoubleType()))
    df_green = df_green.withColumn("ehail_fee", df_green["Ehail_fee"].cast(DoubleType()))
    df_green = df_green.withColumn("congestion_surcharge", lit("0"))
    df_green = df_green.withColumn("PULocationID", lit("0"))
    df_green = df_green.withColumn("DOLocationID", lit("0"))
    return df_green
def type_conversion_15(df_green):
    """
    类型转化
    :param data:
    :return:
    """
    df_green = df_green.withColumn("VendorID", df_green["VendorID"].cast(IntegerType()))
    df_green = df_green.withColumn("passenger_count", df_green["Passenger_count"].cast(IntegerType()))
    df_green = df_green.withColumn("trip_distance", df_green["Trip_distance"].cast(DoubleType()))
    df_green = df_green.withColumn("fare_amount", df_green["Fare_amount"].cast(DoubleType()))
    df_green = df_green.withColumn("extra", df_green["Extra"].cast(DoubleType()))
    df_green = df_green.withColumn("mta_tax", df_green["MTA_tax"].cast(DoubleType()))
    df_green = df_green.withColumn("tip_amount", df_green["Tip_amount"].cast(DoubleType()))
    df_green = df_green.withColumn("total_amount", df_green["Tolls_amount"].cast(DoubleType()))
    df_green = df_green.withColumn('trip_type', df_green['Trip_type '].cast(DoubleType()))
    df_green = df_green.drop('Trip_type ')
    df_green = df_green.withColumn("ehail_fee", df_green["Ehail_fee"].cast(DoubleType()))
    df_green = df_green.withColumn("congestion_surcharge", lit("0"))
    df_green = df_green.withColumn("PULocationID", lit("0"))
    df_green = df_green.withColumn("DOLocationID", lit("0"))
    return df_green
def type_conversion_16(df_green):
    """
    类型转化
    :param data:
    :return:
    """
    df_green = df_green.withColumn("VendorID", df_green["VendorID"].cast(IntegerType()))
    df_green = df_green.withColumn("passenger_count", df_green["Passenger_count"].cast(IntegerType()))
    df_green = df_green.withColumn("trip_distance", df_green["Trip_distance"].cast(DoubleType()))
    df_green = df_green.withColumn("fare_amount", df_green["Fare_amount"].cast(DoubleType()))
    df_green = df_green.withColumn("extra", df_green["Extra"].cast(DoubleType()))
    df_green = df_green.withColumn("mta_tax", df_green["MTA_tax"].cast(DoubleType()))
    df_green = df_green.withColumn("tip_amount", df_green["Tip_amount"].cast(DoubleType()))
    df_green = df_green.withColumn("total_amount", df_green["Tolls_amount"].cast(DoubleType()))
    df_green = df_green.withColumn('trip_type', df_green['Trip_type '].cast(DoubleType()))
    df_green = df_green.withColumn("congestion_surcharge", lit("0"))
    df_green = df_green.withColumn("ehail_fee", df_green["Ehail_fee"].cast(DoubleType()))
    df_green = df_green.withColumn("PULocationID", lit("0"))
    df_green = df_green.withColumn("DOLocationID", lit("0"))
    return df_green
def type_conversion_18(df_green):
    """
    类型转化
    :param data:
    :return:
    """
    df_green = df_green.withColumn("VendorID", df_green["VendorID"].cast(IntegerType()))
    df_green = df_green.withColumn("passenger_count", df_green["passenger_count"].cast(IntegerType()))
    df_green = df_green.withColumn("trip_distance", df_green["trip_distance"].cast(DoubleType()))
    df_green = df_green.withColumn("fare_amount", df_green["fare_amount"].cast(DoubleType()))
    df_green = df_green.withColumn("extra", df_green["extra"].cast(DoubleType()))
    df_green = df_green.withColumn("mta_tax", df_green["mta_tax"].cast(DoubleType()))
    df_green = df_green.withColumn("tip_amount", df_green["tip_amount"].cast(DoubleType()))
    df_green = df_green.withColumn("total_amount", df_green["total_amount"].cast(DoubleType()))
    df_green = df_green.withColumn("congestion_surcharge", lit("0"))
    df_green = df_green.withColumn("Pickup_longitude", lit("0"))
    df_green = df_green.withColumn("Pickup_latitude", lit("0"))
    df_green = df_green.withColumn("Dropoff_longitude", lit("0"))
    df_green = df_green.withColumn("Dropoff_latitude", lit("0"))
    return df_green


def new_field(df_green):
    """
    字段生成
    :param data:
    :return:
    """
    df_green = df_green.withColumn('lpep_pickup_datetime', fn.to_timestamp('lpep_pickup_datetime', 'yyyy-MM-dd HH:mm:ss'))
    df_green = df_green.withColumn('lpep_dropoff_datetime', fn.to_timestamp('lpep_dropoff_datetime', 'yyyy-MM-dd HH:mm:ss'))
    df_green = df_green.withColumn('month', fn.month('lpep_pickup_datetime'))
    df_green = df_green.withColumn('year', fn.year('lpep_pickup_datetime'))
    df_green = df_green.withColumn('hour', fn.hour('lpep_pickup_datetime'))
    df_green = df_green.withColumn('quarter', fn.quarter('lpep_pickup_datetime'))
    df_green = df_green.withColumn('useTime', (fn.unix_timestamp(df_green['lpep_dropoff_datetime']) - fn.unix_timestamp(df_green['lpep_pickup_datetime'])).cast("int"))
    df_green = df_green.withColumn("useMinute", ((fn.unix_timestamp(df_green['lpep_dropoff_datetime']) - fn.unix_timestamp(df_green['lpep_pickup_datetime'])) / 60).cast("int"))
    df_green = df_green.withColumn("date", fn.substring(fn.from_unixtime(fn.unix_timestamp(df_green['lpep_pickup_datetime'], "yyyy-MM-dd")), 0, 10))
    df_green = df_green.withColumn('daysOfWeek', fn.dayofweek(df_green['lpep_pickup_datetime']))
    df_green_l = df_green.withColumn("Average_speed",fn.bround(df_green['trip_distance'] * 1000 / df_green['useMinute'] / 60, scale=4))
    return df_green_l



def join_weather(df_green_clean):
    """
    添加weather信息
    :param df_green_clean:
    :return:
    """
    df_green_clean.createOrReplaceTempView("df_green_clean")
    weather_path = 's3://aws-task-abc/weather_data/wind.csv'
    df_weather = spark.read.format('csv').option('sep', ',').option('inferSchema', True).option('header', True).load(weather_path)
    bd_weather = sc.broadcast(df_weather.collect())
    bd_weather = spark.createDataFrame(bd_weather.value)
    bd_weather.createOrReplaceTempView("bd_weather")
    df_green_clean = spark.sql("select b.*,a.wind_level,a.weather,a.max_tem,a.low_tem,a.avg_tem,a.diff_tem from df_green_clean b  left join bd_weather a on b.date=a.date")
    return df_green_clean

def filter_data(df_green_clean):
    """
    最后的清洗
    :param df_green_clean:
    :return:
    """
    df_green_clean = df_green_clean.withColumn('ehail_fee', when(df_green_clean.ehail_fee.isNull(),lit('0')).otherwise(df_green_clean.ehail_fee))
    df_green_clean = df_green_clean.withColumn('fare_amount', when(df_green_clean.fare_amount.isNull(),lit('0')).otherwise(df_green_clean.fare_amount))
    df_green_clean = df_green_clean.withColumn('extra', when(df_green_clean.extra.isNull(),lit('0')).otherwise(df_green_clean.extra))
    df_green_clean = df_green_clean.withColumn('mta_tax', when(df_green_clean.mta_tax.isNull(),lit('0')).otherwise(df_green_clean.mta_tax))
    df_green_clean = df_green_clean.withColumn('tip_amount', when(df_green_clean.tip_amount.isNull(),lit('0')).otherwise(df_green_clean.tip_amount))
    df_green_clean = df_green_clean.withColumn('tolls_amount', when(df_green_clean.tolls_amount.isNull(),lit('0')).otherwise(df_green_clean.tolls_amount))
    df_green_clean = df_green_clean.withColumn('Pickup_longitude', df_green_clean['Pickup_longitude'].cast(DoubleType()))
    df_green_clean = df_green_clean.withColumn('Pickup_latitude', df_green_clean['Pickup_latitude'].cast(DoubleType()))
    df_green_clean = df_green_clean.withColumn('Dropoff_longitude', df_green_clean['Dropoff_longitude'].cast(DoubleType()))
    df_green_clean = df_green_clean.withColumn('Dropoff_latitude', df_green_clean['Dropoff_latitude'].cast(DoubleType()))
    df_green_clean = df_green_clean.withColumn('tolls_amount', df_green_clean['tolls_amount'].cast(DoubleType()))
    df_green_clean = df_green_clean.withColumn('ehail_fee', df_green_clean['ehail_fee'].cast(DoubleType()))
    df_green_clean = df_green_clean.withColumn('total_amount', df_green_clean['total_amount'].cast(DoubleType()))
    df_green_clean = df_green_clean.withColumn('payment_type', df_green_clean['payment_type'].cast("int"))
    df_green_clean = df_green_clean.filter((df_green_clean['Average_speed'] > 1)  & (df_green_clean['Average_speed'] < 33)
                                           # & (df_green_clean['RateCodeID'] >= 1 ) & (df_green_clean['RateCodeID'] <= 6)
                                           & (df_green_clean['passenger_count'] >= 1 ) & (df_green_clean['passenger_count'] <= 7
                                           ) & (df_green_clean['trip_distance'] > 1 ) & (df_green_clean['trip_distance'] < 500
                                           ) & (df_green_clean['fare_amount'] >= 0 ) & (df_green_clean['fare_amount'] < 1000
                                           ) & (df_green_clean['extra'] >= 0 ) & (df_green_clean['extra'] < 500
                                           ) & (df_green_clean['mta_tax'] >= 0 ) & (df_green_clean['mta_tax'] < 500
                                           ) & (df_green_clean['tip_amount'] >= 0 ) & (df_green_clean['tip_amount'] < 100
                                           ) & (df_green_clean['tolls_amount'] >= 0 ) & (df_green_clean['tolls_amount'] < 500
                                           ) & (df_green_clean['ehail_fee'] >= 0 ) & (df_green_clean['ehail_fee'] < 100
                                           ) & (df_green_clean['total_amount'] > 0 ) & (df_green_clean['total_amount'] < 1500
                                           ) & (df_green_clean['payment_type'] >= 0 ) & (df_green_clean['payment_type'] <= 6))
    return df_green_clean



def select_unoion(df):
    """
    df 规整
    :param df:
    :return:
    """
    df1 = df.select("VendorID",
                    "lpep_pickup_datetime",
                    "lpep_dropoff_datetime",
                    "passenger_count" ,
                    "trip_distance",
                    "fare_amount",
                    "extra",
                    "mta_tax",
                    "tip_amount",
                    "tolls_amount",
                    "ehail_fee",
                    "total_amount",
                    "payment_type",
                    "Pickup_longitude",
                    "Pickup_latitude",
                    "Dropoff_longitude",
                    "Dropoff_latitude")

    return df1

if __name__=="__main__":


    sc = SparkContext()
    spark = SparkSession.builder.getOrCreate()
    green_13 = read_data_green_13()
    green_15 = read_data_green_15()
    green_16 = read_data_green_16()
    green_18 = read_data_green_18()
    green_19 = read_data_green_19()
    green_16_07 = read_data_green_16_07()

    type_green_13 = type_conversion_13(green_13)
    type_green_15 = type_conversion_15(green_15)
    type_green_16 = type_conversion_16(green_16)
    type_green_18 = type_conversion_18(green_18)
    type_green_16_07 = type_conversion_18(green_16_07)
    type_green_19 = type_conversion_19(green_19)

    type_green_13 = select_unoion(type_green_13)
    type_green_15 = select_unoion(type_green_15)
    type_green_16 = select_unoion(type_green_16)
    type_green_18 = select_unoion(type_green_18)
    type_green_16_07 = select_unoion(type_green_16_07)
    type_green_19 = select_unoion(type_green_19)

    type_green_18 = type_green_18.union(type_green_16_07)
    df_green = type_green_13.union(type_green_15).union(type_green_16).union(type_green_18).union(type_green_19)
    df_green = new_field(df_green)
    df_green_clean = filter_data(df_green)
    #df_green_clean = join_weather(df_green_clean)
    df_green_clean.write.format('parquet').mode('overwrite').option('path','s3://neo-aws-ohio-emr-task-abc/df_green_clean_2_parquet_all_run').saveAsTable('df_green_clean_2_parquet_all_run')