
def read_data_green():
    """
    获取数据
    :return:
    """
    green_path = 's3://nyc-tlc/trip data/green_tripdata_*.csv'

    df_green = spark.read.format('csv').option('sep', ',').option('inferSchema', True).option('header', True).load(
        green_path)
    return df_green


def type_conversion(df_green):
    """
    类型转化
    :param data:
    :return:
    """
    df_green = df_green.withColumn(
        "VendorID", df_green["VendorID"].cast(IntegerType()))
    df_green = df_green.withColumn(
        "Passenger_count", df_green["Passenger_count"].cast(IntegerType()))
    df_green = df_green.withColumn(
        "Trip_distance", df_green["Trip_distance"].cast(DoubleType()))
    df_green = df_green.withColumn(
        "Fare_amount", df_green["Fare_amount"].cast(DoubleType()))

    df_green = df_green.withColumn(
        "Extra", df_green["Extra"].cast(DoubleType()))
    df_green = df_green.withColumn(
        "MTA_tax", df_green["MTA_tax"].cast(DoubleType()))
    df_green = df_green.withColumn(
        "Tip_amount", df_green["Tip_amount"].cast(DoubleType()))
    df_green = df_green.withColumn(
        "Total_amount", df_green["Total_amount"].cast(DoubleType()))
    df_green = df_green.withColumn('Trip_type', df_green['Trip_type '])
    df_green = df_green.drop('Trip_type ')
    return df_green


def new_field(df_green):
    """
    字段生成
    :param data:
    :return:
    """
    df_green = df_green.withColumn('lpep_pickup_datetime', fn.to_timestamp(
        'lpep_pickup_datetime', 'yyyy-MM-dd HH:mm:ss'))
    df_green = df_green.withColumn('lpep_dropoff_datetime', fn.to_timestamp(
        'lpep_dropoff_datetime', 'yyyy-MM-dd HH:mm:ss'))
    df_green = df_green.withColumn('month', fn.month('lpep_pickup_datetime'))
    df_green = df_green.withColumn('year', fn.year('lpep_pickup_datetime'))
    df_green = df_green.withColumn('hour', fn.hour('lpep_pickup_datetime'))
    df_green = df_green.withColumn(
        'quarter', fn.quarter('lpep_pickup_datetime'))
    df_green = df_green.withColumn('useTime', (fn.unix_timestamp(df_green['lpep_dropoff_datetime']) - fn.unix_timestamp(
        df_green['lpep_pickup_datetime'])).cast("int"))
    df_green = df_green.withColumn("useMinute", ((fn.unix_timestamp(df_green['lpep_dropoff_datetime']) - fn.unix_timestamp(
        df_green['lpep_pickup_datetime'])) / 60).cast("int"))
    df_green = df_green.withColumn("date", fn.substring(
        fn.from_unixtime(fn.unix_timestamp(df_green['lpep_pickup_datetime'], "yyyy-MM-dd")), 0, 10))
    df_green = df_green.withColumn(
        'daysOfWeek', fn.dayofweek(df_green['lpep_pickup_datetime']))
    df_green = df_green.withColumn("Average_speed",
                                   fn.bround(df_green['Trip_distance'] * 1000 / df_green['useMinute'] / 60, scale=4))
    return df_green


def filter_data(df_green):
    """
    清洗
    :param data:
    :return:
    """
    df_green_clean = df_green.filter(df_green['Average_speed'] > 1)
    return df_green_clean


def join_weather(df_green_clean):
    """
    添加weather信息
    :param df_green_clean:
    :return:
    """
    df_green_clean.createOrReplaceTempView("df_green_clean")
    weather_path = 's3://aws-task-abc/weather_data/wind.csv'
    df_weather = spark.read.format('csv').option('sep', ',').option('inferSchema', True).option('header', True).load(
        weather_path)
    bd_weather = sc.broadcast(df_weather.collect())
    bd_weather = spark.createDataFrame(bd_weather.value)
    bd_weather.createOrReplaceTempView("bd_weather")
    df_green_clean = spark.sql(
        "select b.*,a.wind_level,a.weather,a.max_tem,a.low_tem,a.avg_tem,a.diff_tem from bd_weather a left join df_green_clean b on a.date=b.date")
    return df_green_clean


def write_parquet(df_green_clean):
    """
    存储 parquet
    :param df_green_clean:
    :return:
    """
    df_green_clean.write.format('parquet').mode('overwrite').option(
        'path', 's3://aws-task-abc/df_green_clean_parquet').saveAsTable('df_green_clean_parquet')


if __name__ == "__main__":
    from pyspark.sql import SparkSession
    from pyspark.sql.types import *
    import pyspark.sql.functions as fn
    from pyspark import SparkContext
    sc = SparkContext()
    spark = SparkSession.builder.getOrCreate()
    df_green = read_data_green()
    df_green = type_conversion(df_green)
    df_green = new_field(df_green)
    df_green_clean = filter_data(df_green)
    df_green_clean = join_weather(df_green_clean)
    df_green_clean = filter_data(df_green_clean)
    df_green_clean = write_parquet(df_green_clean)
