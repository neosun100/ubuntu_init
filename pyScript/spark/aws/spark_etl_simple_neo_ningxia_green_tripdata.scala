import com.amazonaws.services.glue.DynamicFrame
import com.amazonaws.services.glue.GlueContext
import com.amazonaws.services.glue.errors.CallSite
import com.amazonaws.services.glue.util.GlueArgParser
import com.amazonaws.services.glue.util.Job
import com.amazonaws.services.glue.util.JsonOptions
import org.apache.spark.SparkContext
import org.apache.spark.sql.SQLContext
import scala.collection.JavaConverters._

object GlueApp {
  def main(sysArgs: Array[String]) {
    val spark: SparkContext = new SparkContext()
    val glueContext: GlueContext = new GlueContext(spark)
    val sql: SQLContext = new SQLContext(spark)
    val SqlQuery0: String = """
        |select * from emrSourceData limit 100000
        |
        |""".stripMargin
    // @params: [JOB_NAME]
    val args = GlueArgParser.getResolvedOptions(sysArgs, Seq("JOB_NAME").toArray)
    Job.init(args("JOB_NAME"), glueContext, args.asJava)
    // @type: DataSource
    // @args: [format_options = JsonOptions("""{"quoteChar":"\"","escaper":"","withHeader":true,"separator":","}"""), connection_type = "s3", format = "csv", connection_options = {"paths": ["s3://neo-aws-ohio-emr-task-source/"], "recurse":true}, transformation_ctx = "DataSource0"]
    // @return: DataSource0
    // @inputs: []
    val DataSource0 = glueContext.getSourceWithFormat(connectionType = "s3", options = JsonOptions("""{"paths": ["s3://aws-neo-ningxia-glue-crawler-source/green_tripdata/"], "recurse":true}"""), transformationContext = "DataSource0", format = "csv", formatOptions = JsonOptions("""{"quoteChar":"\"","escaper":"","withHeader":true,"separator":","}""")).getDynamicFrame()
    // @type: SqlCode
    // @args: [sqlAliases = Map("emrSourceData" -> DataSource0), sqlName = SqlQuery0, transformation_ctx = "Transform0"]
    // @return: Transform0
    // @inputs: [dfc = DataSource0]
    val Transform0 = SparkSqlQuery.execute(glueContext = glueContext, sqlContext = sql, query = SqlQuery0, mapping = Map("emrSourceData" -> DataSource0))
    // @type: DataSink
    // @args: [connection_type = "s3", format = "glueparquet", connection_options = {"path": "s3://neo-aws-ohio-emr-task-sink/glue-etl-sink-by-parquet-a/", "partitionKeys": []}, transformation_ctx = "DataSink0"]
    // @return: DataSink0
    // @inputs: [frame = Transform0]
    val DataSink0 = glueContext.getSinkWithFormat(connectionType = "s3", options = JsonOptions("""{"path": "s3://aws-neo-ningxia-glue-crawler-sink/df_green_clean_scala/", "partitionKeys": []}"""), transformationContext = "DataSink0", format = "glueparquet").writeDynamicFrame(Transform0)
    Job.commit()
  }
}
object SparkSqlQuery {
  def execute(glueContext: GlueContext, sqlContext: SQLContext, query: String, mapping: Map[String, DynamicFrame]) : DynamicFrame = {
    for ((alias, frame) <- mapping) {
      frame.toDF().createOrReplaceTempView(alias)
    }
    val resultDataFrame = sqlContext.sql(query)
    return DynamicFrame(resultDataFrame, glueContext)
  }
}