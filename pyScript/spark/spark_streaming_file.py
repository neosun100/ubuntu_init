
from pyspark import SparkContext
from pyspark.streaming import StreamingContext
 
if __name__ == '__main__':
    sc = SparkContext(appName="inputSourceStreaming", master="local[*]")
    sc.setLogLevel("error")
    # 第二个参数指统计多长时间的数据
    ssc = StreamingContext(sc, 5)
 
    lines = ssc.textFileStream("file:///home/neo/upload/html_MD")
    counts = lines.flatMap(lambda line: line.split(" ")).map(lambda x: (x, 1)).reduceByKey(lambda a,b: a+b)
    counts.pprint()
    # -------------------------------------------
    # Time: 2019-07-31 18:11:55
    # -------------------------------------------
    # ('hong', 2)
    # ('zhang', 2)
    # ('li', 2)
    # ('san', 2)
    # ('wang', 2)
    ssc.start()
    ssc.awaitTermination()