from pyspark import SparkContext
from pyspark.streaming import StreamingContext
from pyspark.streaming.kafka import KafkaUtils
# from pyspark.sql import KafkaUtils




if __name__ == '__main__':
    import findspark
    findspark.init()

    sc = SparkContext(appName="inputSourceStreaming", master="local[*]")
    sc.setLogLevel("error")
    # 第二个参数指统计多长时间的数据
    ssc = StreamingContext(sc, 5)
 
    kvs = KafkaUtils.createDirectStream(ssc, "log", "192.168.100.142:19092")
    lines = kvs.map(lambda x: x[1])
    counts = lines.flatMap(lambda line: line.split(" ")).map(lambda x: (x, 1)).reduceByKey(lambda a,b: a+b)
    counts.pprint()
 
    ssc.start()
    ssc.awaitTermination()