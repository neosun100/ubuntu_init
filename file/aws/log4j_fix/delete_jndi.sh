#/bin/bash

set -e

jars=("/usr/lib/flink/bin/bash-java-utils.jar" "/usr/lib/flink/lib/log4j-core-2.14.1.jar" "/usr/lib/hbase-operator-tools/hbase-hbck2-1.1.0.jar" "/usr/lib/hbase-operator-tools/hbase-tools-1.1.0.jar" "/usr/lib/trino/plugin/elasticsearch/log4j-core-2.13.3.jar" "/usr/lib/hive/lib/log4j-core-2.10.0.jar" "/usr/lib/hudi/cli/lib/log4j-core-2.10.0.jar" "/usr/lib/presto/plugin/presto-druid/log4j-core-2.8.2.jar" "/usr/lib/presto/plugin/presto-elasticsearch/log4j-core-2.9.1.jar" "/usr/share/aws/emr/emr-log-analytics-metrics/lib/log4j-core-2.13.3.jar" "/usr/share/aws/emr/emr-metrics-collector/lib/log4j-core-2.11.2.jar")

class="org/apache/logging/log4j/core/lookup/JndiLookup"
jndi="${class}.class"

for index in "${!jars[@]}"; do
    jar=${jars[$index]}
    if [[ -f "$jar" ]]; then
        still_exists=$(jar tf $jar | grep -i $class || true)
        if [[ ! -z "$still_exists" ]]; then
            echo "Removing JndiLookup class from $jar..."
            sudo zip -q -d $jar $jndi
            echo "Removed JndiLookup class from $jar."
        fi
    fi
done

remaining_jars=()

for index in "${!jars[@]}"; do
    jar=${jars[$index]}
    if [[ -f "$jar" ]] && jar tf $jar | grep -i $class; then
        remaining_jars+=$jar
    fi
done

if [[ ${remaining_jars[@]} ]]; then
    echo "[ERROR] JndiLookup class still exists in: "
    printf "%s\n" "${remaining_jars[@]}"
    exit 1
fi

exit 0
