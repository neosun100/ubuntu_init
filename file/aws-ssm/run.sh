#!/bin/sh

#put install_chronicled.py in DIR of this script
#currently install_chronicled.py only support OS: Amazon Linux, Amazon Linux 2, Oracle Linux, RedHat Linux.
#this script use default user ec2-user for amazon linux
#if you have multiple pem key, modify KEY and run script again
#check missing EC2 report https://security-dashboard.aws.a2z.com/dashboards/permalink/naws_missing_hosts 

KEY=/path/to/pem/key
IAM_ROLE=EC2-IAM-ROLE-NAME
SSHLOGIN=ec2-user

for REGION in `aws ec2 describe-regions --output text | cut -f4`
do
     echo -e "\nListing Instances in region:'$REGION'..."
     aws ec2 describe-instances --query "Reservations[*].Instances[*].{IP:PublicIpAddress,ID:InstanceId,Type:InstanceType,State:State.Name,Name:Tags[0].Value}"  --filters Name=instance-state-name,Values=running --output=text --region $REGION > ${REGION}-EC2.list

     for  EC2 in `grep -v None ${REGION}-EC2.list |awk '{print $2}'`
     do
        EC2ID=$(grep $EC2 ${REGION}-EC2.list |awk '{print $1}')
        
        #attach role to EC2
        aws ec2 associate-iam-instance-profile --iam-instance-profile Name=$IAM_ROLE --instance-id $EC2ID --region $REGION
        
        #try to copy and run install script 
        scp -o ConnectTimeout=5 -o LogLevel=ERROR -i $KEY install_chronicled.py $SSHLOGIN@${EC2}:/tmp && ssh -o ConnectTimeout=5 -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null  -o LogLevel=ERROR -i $KEY $SSHLOGIN@$EC2 "sudo python /tmp/install_chronicled.py; sleep 3; ps -ef |grep chronicled |grep -v grep"
        
        #deattach EC2 role
        IIP=$(aws ec2 describe-iam-instance-profile-associations --output text --region $REGION |grep $EC2ID |awk '{print $2}')
        aws ec2 disassociate-iam-instance-profile --association-id $IIP --region $REGION
    done
done