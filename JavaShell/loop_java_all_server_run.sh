#!/bin/sh

echo "ACTIVE :$1"   #传入的第一个参数
ACTIVE=${1:-prd}
# test,dev,prd
echo $ACTIVE


echo "SLEEPTIME :$2"   #传入的第二个参数
SLEEPTIME=${2:-15}
echo $SLEEPTIME


echo "NUM : $3"
NUM=${3:-"`date +%s%N | cut -c17 |  awk '{print int($0)}'`"}
echo $NUM

echo "🐸 🐸 🐸 🐸 🐸 🐸 🐸 🐸 🐸 🐸 🐸 🐸 🐸 🐸 🐸 🐸 🐸 🐸 "
echo "`date +'%Y-%m-%d %H:%M:%S'` 启动注册发现 eureka-server";
cd /opt/jar/eureka-server && \
nohup /usr/local/java/jdk1.8.0_181/bin/java -Xms1024M -Xmx1024M -jar eureka-server-1.0.0.jar  --spring.profiles.active=$ACTIVE > system.log 2>&1 &
sleep 15;
echo "";
echo "";

echo "🐸 🐸 🐸 🐸 🐸 🐸 🐸 🐸 🐸 🐸 🐸 🐸 🐸 🐸 🐸 🐸 🐸 🐸 "
echo "`date +'%Y-%m-%d %H:%M:%S'` 启动配置中心 config-server";
cd /opt/jar/config-server && \
nohup /usr/local/java/jdk1.8.0_181/bin/java -Xms512M -Xmx512M -jar config-server-1.0.0.jar    --spring.profiles.active=$ACTIVE > system.log 2>&1 &        
sleep 15;
echo "";
echo "";

echo "🐸 🐸 🐸 🐸 🐸 🐸 🐸 🐸 🐸 🐸 🐸 🐸 🐸 🐸 🐸 🐸 🐸 🐸 "
echo "`date +'%Y-%m-%d %H:%M:%S'` 启动业务服务";
echo "`date +'%Y-%m-%d %H:%M:%S'` computing-server";
cd /opt/jar/computing-server && \
nohup /usr/local/java/jdk1.8.0_181/bin/java  -Xms1024M -Xmx1024M   -jar computing-server-1.0.0-SNAPSHOT.jar  --spring.profiles.active=$ACTIVE > system.log 2>&1 &
sleep 15;
echo "";
echo "";

echo "`date +'%Y-%m-%d %H:%M:%S'` format-server";
cd /opt/jar/format-server && \
nohup /usr/local/java/jdk1.8.0_181/bin/java  -Xms1024M -Xmx1024M -jar format-server-1.0.0-SNAPSHOT.jar   --spring.profiles.active=$ACTIVE > system.log 2>&1 &
sleep 15;
echo "";
echo "";

echo "`date +'%Y-%m-%d %H:%M:%S'` api-gateway";
cd /opt/jar/gateway && \
nohup /usr/local/java/jdk1.8.0_181/bin/java  -Xms1024M -Xmx1024M  -jar api-gateway-public-1.0.0-SNAPSHOT.jar  --spring.profiles.active=$ACTIVE > system.log 2>&1 &
sleep 15;
echo "";
echo "";

echo "`date +'%Y-%m-%d %H:%M:%S'` holiday-server";
cd /opt/jar/holiday-server && \
nohup /usr/local/java/jdk1.8.0_181/bin/java -Xms1024M -Xmx1024M -jar holiday-server-1.0.0-SNAPSHOT.jar --spring.profiles.active=$ACTIVE > system.log 2>&1 &
sleep 15;
echo "";
echo "";

echo "`date +'%Y-%m-%d %H:%M:%S'` message-server";
cd /opt/jar/message-server && \
nohup /usr/local/java/jdk1.8.0_181/bin/java  -Xms1024M -Xmx1024M  -jar message-server-1.0.0-SNAPSHOT.jar  --spring.profiles.active=$ACTIVE > system.log 2>&1 &
sleep 15;
echo "";
echo "";

echo "`date +'%Y-%m-%d %H:%M:%S'` monitor-server";
cd /opt/jar/monitor-server && \
nohup /usr/local/java/jdk1.8.0_181/bin/java  -Xms1024M -Xmx1024M  -jar monitor-server-1.0.0-SNAPSHOT.jar  --spring.profiles.active=$ACTIVE > system.log 2>&1 &
sleep 15;
echo "";
echo "";

echo "`date +'%Y-%m-%d %H:%M:%S'` stock-server";
cd /opt/jar/stock-server && \
nohup /usr/local/java/jdk1.8.0_181/bin/java -Xms1024M -Xmx1024M -jar stock-server-1.0.0-SNAPSHOT.jar  --spring.profiles.active=$ACTIVE > system.log 2>&1 &
sleep 15;
echo "";
echo "";

echo "`date +'%Y-%m-%d %H:%M:%S'` user-server";
cd /opt/jar/user-server && \
nohup /usr/local/java/jdk1.8.0_181/bin/java  -Xms1024M -Xmx1024M  -jar user-server-1.0.0-SNAPSHOT.jar  --spring.profiles.active=$ACTIVE > system.log 2>&1 &
sleep 15;
echo "";
echo "";

# echo "`date +'%Y-%m-%d %H:%M:%S'` wechat-server";
# cd /opt/jar/wechat && \
# nohup java -jar wechat-server-1.0.0-SNAPSHOT.jar  -Xms1024M -Xmx1024M  --spring.profiles.active=$ACTIVE > system.log 2>&1 &
# sleep 15;
# echo "";
# echo "";

echo "`date +'%Y-%m-%d %H:%M:%S'` ws-server";
cd /opt/jar/ws && \
nohup /usr/local/java/jdk1.8.0_181/bin/java -Xms1024M -Xmx1024M -jar ws-server-1.0.0-SNAPSHOT.jar  --spring.profiles.active=$ACTIVE > system.log 2>&1 &
sleep 15;
echo "";
echo "";

echo "`date +'%Y-%m-%d %H:%M:%S'` snowflake-server";
cd /opt/jar/snowflake-server && \
nohup /usr/local/java/jdk1.8.0_181/bin/java -Xms1024M -Xmx1024M -jar snowflake-server-1.0.0-SNAPSHOT.jar --spring.snowflake.worker-id=$NUM --spring.profiles.active=$ACTIVE > system.log 2>&1 &
cd /;
echo "`date +'%Y-%m-%d %H:%M:%S'` 启动完成";